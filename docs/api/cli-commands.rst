**********************
Command Line Interface
**********************

This document is a prettier output of the documentation produced by
the command line client's ``man`` command. You can obtain similar pages
using the following shell commands.

::

   dune-client -protocol PsBabyM1eUXZ man -verbosity 3
   dune-admin-client man -verbosity 3


.. _client_manual:

Client manual
=============

.. raw:: html
         :file: dune-client.html


.. _admin_client_manual:

Admin-client manual
===================

.. raw:: html
         :file: dune-admin-client.html


.. _signer_manual:

Signer manual
=============

.. raw:: html
         :file: dune-signer.html


.. _baker_manual:

Baker manual
============

.. raw:: html
         :file: dune-baker.html


.. _endorser_manual:

Endorser manual
===============

.. raw:: html
         :file: dune-endorser.html


.. _accuser_manual:

Accuser manual
==============

.. raw:: html
         :file: dune-accuser.html
