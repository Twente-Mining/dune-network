.. _support:

Technical Support
=================

If you need help understanding how the Dune protocol works or if you
have technical questions about the software, here are a few resources
to find answers.

- `This documentation! <https://dune.network/docs/>`_ Make sure to go
  through this technical documentation before asking elsewhere, there
  is also a searchbox on the top left corner.
- There is a sub-reddit at https://www.reddit.com/r/dune-network/ that
  is the main meeting point for the Dune community, for technical,
  economical and just random questions. They also have a nicely
  curated list of resources.
