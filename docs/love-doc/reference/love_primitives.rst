Love primitives
===============

Prefix & Infix primitives
~~~~~~~~~~~~~~~~~~~~~~~~~

There are two kinds of primitives in the language:

* **Prefix primitives** are used by putting the primitive before the
  arguments: ``prim x y z``.

* **Infix primitves** are used by putting the primitive between the
  arguments: ``x prim y``. Infix primitives are always operators
  (``+``, ``-``, etc.).

When the type of a primitive is specified, we extend the notation for
functions like this:

* ``TYPE_ARG -> TYPE_RESULT`` for a primitive with one argument
* ``TYPE_ARG1 -> TYPE_ARG2 -> TYPE_RESULT`` for a primitive with two arguments
* ``forall TYPES. ... -> TYPE_RESULT`` for a polymorphic primitive

Whereas functions can only take one argument in Liquidity/Michelson
(possibly a tuple), primitives can take multiple arguments.

Comparison between values
~~~~~~~~~~~~~~~~~~~~~~~~~

All values are not comparable. Only two values of the following types
can be compared with each other:

* ``unit``
* ``bool``
* ``int``
* ``tez``
* ``string``
* ``bytes``
* ``timestamp``
* ``keyhash``
* ``address``

* ``'a * 'b`` when ``'a`` and  ``'b`` are comparable
* ``'a option``,``'a list``, ``'a set`` when ``'a`` is comparable
* ``('a, 'b) map`` when ``'a`` and  ``'b`` are comparable
* ``{field1 : 'a; field2 : 'b; ...}`` when ``'a``, ``'b``, ... are
  comparable
* ``A of 'a | B of 'b ...`` when ``'a``, ``'b``, ... are comparable

The following comparison operators are available:

* ``=`` : equal
* ``<>`` : not-equal
* ``<`` : strictly less
* ``<=`` : less or equal
* ``>`` : strictly greater
* ``>=`` : greater or equal

These operators have type ``forall 'a. 'a -> 'a -> bool``, hence their type
must be provided with the type application ``[:TYPE]``

Example: ``=[:bool]`` defines the equality on booleans and has type
``bool -> bool -> bool``.

The function ``compare : forall 'a. 'a -> 'a -> int``
is a polymorphic function returning an integer, as follows.

``compare[:TYPE] x y``
* returns 0 if ``x`` and ``y`` are equal
* returns a strictly positive integer if ``x > y``
* returns a strictly negative integer if ``x < y``

Raising exceptions
~~~~~~~~~~~~~~~~~~

There exist two kind of exceptions in Love: user-defined exceptions
and the ``Failure`` polymorphic exception.

* User-defined exceptions are declared in contracts with the keyword ``exception``
  and a list of types. They are raised with they keywork ``raise``, and can be catched
  when encapsulated in ``try ... with EXCEPTION ->``. ``raise`` is a special primitive
  that takes an exception in argument and returns a value of type ``forall 'a, 'a``.

  .. .. tryliquidity:: tests/doc7.lov
  .. literalinclude:: tests/doc7.lov

* ``failwith : forall 'a. 'a -> (forall 'b. 'b)``: makes the current
  transaction and all its internal transactions fail. No modification
  is done to the context. The argument can be any value (often a string
  and some argument), the system will display it to explain why the
  transaction failed. The argument and return type must be specified
  as the function is polymorphic.

  .. .. tryliquidity:: tests/doc7-1.lov
  .. literalinclude:: tests/doc7-1.lov

Operations on tuples
~~~~~~~~~~~~~~~~~~~~

* Simple projection: ``t.n`` where ``n`` is a constant positive-or-nul int: returns the
  ``n``-th element of the tuple ``t``. Starts at ``0``.

  .. .. tryliquidity:: tests/doc8.lov
  .. literalinclude:: tests/doc8.lov

* Tuple projection: ``t.(n_1,n_2,...)`` where ``n_i`` is a constant positive-or-nul int for
  each ``i``: returns the tuple with the elements corresponding to ``(n_1,n_2,...)``

  .. .. tryliquidity:: tests/doc9.lov
  .. literalinclude:: tests/doc9.lov

*  Tuple update: ``t <- (EXP_1,EXP_2,...)`` returns the tuple where each element ``i`` has been
   replaced by ``EXP_i``. ``EXP_i`` can either be an expression or ``_``, which
   does not change the ``i``-th element of the tuple.

  .. .. tryliquidity:: tests/doc10.lov
  .. literalinclude:: tests/doc10.lov

Operations on numeric values
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Operations on numeric values are not polymorphic, each operator
define a specific numeric operation.

* Additions

  * ``+ : int -> int -> int``
  * ``++ : nat -> nat -> nat``
  * ``++! : nat -> int -> int``
  * ``+!+ : int -> nat -> int``
  * ``+$ : dun -> dun -> dun``
  * ``+:! : timestamp -> int -> timestamp``
  * ``+!: : int -> timestamp -> timestamp``
  * ``+:+ : timestamp -> nat -> timestamp``
  * ``++: : nat -> timestamp -> timestamp``

* Substraction

  * ``- : int -> int -> int``
  * ``-+ : nat -> nat -> int``
  * ``-+! : nat -> int -> int``
  * ``-!+ : int -> nat -> int``
  * ``-$ : dun -> dun -> dun``
  * ``-: : timestamp -> timestamp -> int``
  * ``-:! : timestamp -> int -> timestamp``
  * ``-!: : int -> timestamp -> timestamp``
  * ``-:+ : timestamp -> nat -> timestamp``
  * ``-+: : nat -> timestamp -> timestamp``

* Multiplication


  * ``* : int -> int -> int``
  * ``*+ : nat -> nat -> int``
  * ``*+! : nat -> int -> int``
  * ``*!+ : int -> nat -> int``
  * ``*$! : dun -> int -> dun``
  * ``*!$ : int -> dun -> dun``
  * ``*$+ : dun -> nat -> dun``
  * ``*+$ : nat -> dun -> dun``

* Division (returns ``None`` when the second argument is zero)

  * ``/ : int -> int -> (int * int) option``
  * ``/+ : nat -> nat -> (nat * nat) option``
  * ``/+! : nat -> int -> (int * int) option``
  * ``/!+ : int -> nat -> (int * int) option``
  * ``/$ : dun -> dun -> (int * dun) option``
  * ``/$! : dun -> int -> (dun * dun) option``
  * ``/$+ : dun -> nat -> (dun * dun) option``

NB: Arithmetic operators syntax follows a certain logic.
The first characher represents the operation, followed
by symbols representing the argument types.

  * ``!`` for ``int``
  * ``+`` for ``nat``
  * ``$`` for ``dun``
  * ``:`` for ``timestamp``
    
For example, ``+!:`` is the operator adding ``int`` s
to ``timestamp`` s.

* Bitwise operators

  * ``land | lor | lxor | lsl | lsr : int -> int -> int``
  * ``nand | nor | nxor | nlsl | nlsr : nat -> nat -> nat``

* Boolean operators (not lazy)

  * ``&&`` : boolean and
  * ``||`` : boolean or
  * ``|&`` : boolean xor
  * ``not`` : boolean negation

Translators
~~~~~~~~~~~

* ``Int.of_nat : nat -> int`` Transforms a positive integer to
  a signed integer.

  .. .. tryliquidity:: tests/doc_intnat.lov
  .. literalinclude:: tests/doc_intnat.lov

* ``Nat.of_int : int -> nat option`` Transforms an integer to
  a positive integer. If the integer in argument is negative,
  it returns ``None``.

  .. .. tryliquidity:: tests/doc_natint.lov
  .. literalinclude:: tests/doc_natint.lov

* ``Address.of_keyhash : keyhash -> address`` Transforms a keyhash to
  an address

  .. .. tryliquidity:: tests/doc_keyadd.lov
  .. literalinclude:: tests/doc_keyadd.lov

* ``Keyhash.of_address : address -> keyhash option`` Transforms an
  address to a keyhash. If the address in argument is a contract,
  it returns ``None``.

  .. .. tryliquidity:: tests/doc_addkey.lov
  .. literalinclude:: tests/doc_addkey.lov

Operations on lambdas
~~~~~~~~~~~~~~~~~~~~~

* ``( |> ) : 'a -> ('a -> 'b) -> 'b``
  Applies a function to its argument.

  .. .. tryliquidity:: tests/doc33.lov
  .. literalinclude:: tests/doc33.lov

Operation on lists
~~~~~~~~~~~~~~~~~~

Lists are immutable data structures containing values (of any type)
that can only be accessed in a sequential order. Since they are
immutable, all **modification** primitives return a new list, and the
list given in argument is unmodified.

* ``[] : forall 'a. 'a list``
  The empty list.

  .. .. tryliquidity:: tests/doc_nil.lov
  .. literalinclude:: tests/doc_nil.lov

* ``List.cons : forall 'a. 'a -> 'a list -> 'a list`` Add a new element at the head
  of the list. The previous list becomes the tail of the new list.
  It is equivalent to ``CONS`` in Michelson.

  .. .. tryliquidity:: tests/doc34.lov
  .. literalinclude:: tests/doc34.lov

* ``( :: ) : forall 'a. 'a -> 'a list -> 'a list`` Same as ``List.cons``, but is
  an infix operator. The type ``'a`` is automatically inferred and is not
  required.

  .. .. tryliquidity:: tests/doc34.lov
  .. literalinclude:: tests/doc34.lov

* ``( @ ) : forall 'a. 'a list -> 'a list -> 'a list``. Concatenate two lists into a
  single list.

  .. .. tryliquidity:: tests/doc41.lov
  .. literalinclude:: tests/doc41.lov

* ``List.rev : forall 'a. 'a list -> 'a list`` Returns the list in the reverse order.

  .. .. tryliquidity:: tests/doc35.lov
  .. literalinclude:: tests/doc35.lov

* ``List.length : forall 'a. 'a list -> nat``. Returns the length
  of the list. It is equivalent to ``SIZE`` in Michelson.

  .. .. tryliquidity:: tests/doc36.lov
  .. literalinclude:: tests/doc36.lov

* ``List.iter: forall 'a. ('a -> unit) -> 'a list -> unit``. Iter the function on
  all the elements of a list. Since no value can be returned, it can
  only be used for side effects, i.e. to fail the transaction.  It is
  equivalent to ``ITER`` in Michelson.

  .. .. tryliquidity:: tests/doc37.lov
  .. literalinclude:: tests/doc37.lov

* ``List.fold: forall 'elt. forall 'acc. ('elt -> 'acc -> 'acc) -> 'elt list
  -> 'acc ->  'acc``.
  Iter on all elements of a list, while modifying an
  accumulator. It is equivalent to ``ITER`` in Michelson.

  .. .. tryliquidity:: tests/doc38.lov
  .. literalinclude:: tests/doc38.lov

* ``List.map: forall 'a. forall 'b. ('a -> 'b) -> 'a list -> 'b list``. Returns a
  list with the result of applying the function on each element of the
  list. It is equivalent to ``MAP`` in Michelson.

  .. .. tryliquidity:: tests/doc39.lov
  .. literalinclude:: tests/doc39.lov

* ``List.map_fold: forall 'a. forall 'acc. forall 'b.
  ('a * 'acc -> 'b * 'acc) -> 'a list -> 'acc -> 'b list * 'acc``.
  Returns a list with the result of applying the
  function on each element of the list, plus an accumulator. It is
  equivalent to ``MAP`` in Michelson.

  .. .. tryliquidity:: tests/doc40.lov
  .. literalinclude:: tests/doc40.lov

The Bytes module
~~~~~~~~~~~~~~~~

* ``Bytes.pack: forall 'a. 'a -> bytes``. Serialize any data to a binary
  representation in a sequence of bytes. It is equivalent to ``PACK``
  in Michelson.

  .. .. tryliquidity:: tests/doc25.lov
  .. literalinclude:: tests/doc25.lov

* ``Bytes.unpack<:TYPE>: bytes -> TYPE option``. Deserialize a sequence of
  bytes to a value from which it was serialized. The expression should
  be annotated with the (option) type that it should return. It is
  equivalent to ``UNPACK`` in Michelson.

  .. .. tryliquidity:: tests/doc26.lov
  .. literalinclude:: tests/doc26.lov

* ``Bytes.length : bytes -> nat``. Returns the size of
  the sequence of bytes. It is equivalent to ``SIZE`` in Michelson.

  .. .. tryliquidity:: tests/doc27.lov
  .. literalinclude:: tests/doc27.lov

* ``Bytes.concat: bytes list -> bytes``. Append all the sequences of
  bytes of a list into a single sequence of bytes. It is equivalent to
  ``CONCAT`` in Michelson.

  .. .. tryliquidity:: tests/doc28.lov
  .. literalinclude:: tests/doc28.lov

* ``Bytes.slice : nat -> nat -> bytes ->
  bytes option``. Extract a sequence of bytes within another sequence
  of bytes. ``Bytes.slice start len b`` extracts the bytes subsequence
  of ``b`` starting at index ``start`` and of length ``len``. A return
  value ``None`` means that the position or length was invalid. It
  is equivalent to ``SLICE`` in Michelson.

  .. .. tryliquidity:: tests/doc29.lov
  .. literalinclude:: tests/doc29.lov

Operation on strings
~~~~~~~~~~~~~~~~~~~~

A string is a fixed sequence of characters. They are restricted to the
printable subset of 7-bit ASCII, plus some escaped characters (``\n``,
``\t``, ``\b``, ``\r``, ``\\``, ``\"``).


* ``String.length : string -> nat``.
  Returns the size of the string in characters. It is equivalent
  to ``SIZE`` in Michelson.

  .. .. tryliquidity:: tests/doc30.lov
  .. literalinclude:: tests/doc30.lov

* ``String.slice : nat -> nat -> string  -> string option``.
  ``String.slice start len s`` returns a substring
  of a string ``s`` at the given starting at position ``len`` with the
  specified length ``len``, or ``None`` if invalid. It is
  equivalent to ``SLICE`` in Michelson.

  .. .. tryliquidity:: tests/doc31.lov
  .. literalinclude:: tests/doc31.lov

* ``String.concat: string list -> string``. Append all strings of a
  list into a single string. It is equivalent to ``CONCAT`` in
  Michelson.

  .. .. tryliquidity:: tests/doc32.lov
  .. literalinclude:: tests/doc32.lov

* ``^: string -> string -> string``. Infix operator for
  concatenating two strings.

  .. .. tryliquidity:: tests/doc_concat.lov
  .. literalinclude:: tests/doc_concat.lov

The ``Set`` module
~~~~~~~~~~~~~~~~~~

Sets are immutable data structures containing unique values (a
comparable type). Since they are immutable, all **modification**
primitives return a new updated set, and the set given in argument is
unmodified.

* ``Set.empty: forall 'a. 'a set``. The empty set

  .. .. tryliquidity:: tests/doc42.lov
  .. literalinclude:: tests/doc42.lov


* ``Set.cardinal: forall 'a. 'a set -> nat``. The number of elements in a set.

  .. .. tryliquidity:: tests/doc43.lov
  .. literalinclude:: tests/doc43.lov

* ``Set.add: forall 'a. 'a -> 'a set -> 'a set`` . Add an element to a set, if
  not present.

  .. .. tryliquidity:: tests/doc44.lov
  .. literalinclude:: tests/doc44.lov

* ``Set.remove: forall 'a. 'a -> 'a set -> 'a set`` . Removes an element to a set, if
  present.

  .. .. tryliquidity:: tests/doc45.lov
  .. literalinclude:: tests/doc45.lov

* ``Set.mem: forall 'a. 'a -> 'a set -> bool``. Returns ``true`` if the element is
  in the set, ``false`` otherwise. It is equivalent to ``MEM`` in
  Michelson.

  .. .. tryliquidity:: tests/doc46.lov
  .. literalinclude:: tests/doc46.lov

* ``Set.iter: forall 'elt. ('elt -> unit) -> 'elt set -> unit``. Apply a function
  on all elements of the set. Since no value can be returned, it can
  only be used for side effects, i.e. to fail the transaction.  It is
  equivalent to ``ITER`` in Michelson.

  .. .. tryliquidity:: tests/doc47.lov
  .. literalinclude:: tests/doc47.lov

* ``Set.fold: forall 'elt. forall 'acc. ('elt -> 'acc -> 'acc) -> 'elt set
  -> 'acc ->  'acc``.
  Iter on all elements of a set, while modifying an accumulator.

  .. .. tryliquidity:: tests/doc48.lov
  .. literalinclude:: tests/doc48.lov

* ``Set.map: forall 'a. forall 'b. ('a -> 'b) -> 'a set -> 'b set``. Returns a
  list with the result of applying the function on each element of the
  list.

  .. .. tryliquidity:: tests/doc49.lov
  .. literalinclude:: tests/doc49.lov

* ``Set.map_fold: forall 'a. forall 'acc. forall 'b.
  ('a -> 'acc -> 'b * 'acc) -> 'a set -> 'acc -> 'b set * 'acc``.
  Returns a list with the result of applying the
  function on each element of the list, plus an accumulator.

  .. .. tryliquidity:: tests/doc50.lov
  .. literalinclude:: tests/doc50.lov

The ``Map`` module
~~~~~~~~~~~~~~~~~~

Maps are immutable data structures containing associations between
keys (a comparable type) and values (any type). Since they are
immutable, all **modification** primitives return a new updated map,
and the map given in argument is unmodified.

* ``Map.empty: forall 'key. forall 'elt. ('key, 'elt) map``.
  The empty map that binds elements of type ``'key`` to elements
  of type ``'elt``.

  .. .. tryliquidity:: tests/doc51.lov
  .. literalinclude:: tests/doc51.lov

* ``Map.cardinal: forall 'key. forall 'elt. ('key, 'elt) map -> nat``.
  The number of bindings registered in a map.

  .. .. tryliquidity:: tests/doc52.lov
  .. literalinclude:: tests/doc52.lov

* ``Map.add: forall 'key. forall 'elt. 'key -> 'elt -> ('key, 'elt) map -> ('key, 'elt) map`` .
  Binds a key to a value into a map, if the key is not present.

  .. .. tryliquidity:: tests/doc53.lov
  .. literalinclude:: tests/doc53.lov

* ``Map.remove:  forall 'key. forall 'elt. 'key -> 'elt -> ('key, 'elt) map -> ('key, 'elt) map`` .
  Removes a binding from a map, if present.

  .. .. tryliquidity:: tests/doc54.lov
  .. literalinclude:: tests/doc54.lov

* ``Map.mem: forall 'key. forall 'map 'key -> ('key, 'elt) map -> bool``.
  Returns ``true`` if the key belong to the map, ``false`` otherwise.

  .. .. tryliquidity:: tests/doc55.lov
  .. literalinclude:: tests/doc55.lov

* ``Map.find: forall 'key. forall 'map 'key -> ('key, 'elt) map -> 'elt option``.
  Returns ``Some elt``  if ``elt`` is bound to the key in argument, ``None [:'elt]`` otherwise.

  .. .. tryliquidity:: tests/doc56.lov
  .. literalinclude:: tests/doc56.lov

* ``Map.iter: forall 'key. forall 'elt. ('elt -> 'key -> unit) -> ('key, 'elt) map -> unit``.
  Applies a function on all elements of the set. Since no value can be returned, it can
  only be used for side effects, i.e. to fail the transaction.

  .. .. tryliquidity:: tests/doc57.lov
  .. literalinclude:: tests/doc57.lov

* ``Map.fold: forall 'key. forall 'elt. forall 'acc.
  ('key -> 'elt -> 'acc -> 'acc) -> ('key, 'elt) map -> 'acc ->  'acc``.
  Iter on all elements of a set, while modifying an accumulator.

  .. .. tryliquidity:: tests/doc58.lov
  .. literalinclude:: tests/doc58.lov

* ``Map.map:  forall 'key. forall 'elt. forall 'new_elt.
  ('key -> 'elt -> 'new_elt) -> ('key, 'elt) map -> ('key, 'new_elt) map``.
  Returns a list with the result of applying the function on each element of the
  list.

  .. .. tryliquidity:: tests/doc59.lov
  .. literalinclude:: tests/doc59.lov

* ``Map.map_fold: forall 'key. forall 'elt. forall 'acc. forall 'new_elt.
  ('key -> 'elt -> 'acc -> 'new_elt * 'acc) ->
  ('key, 'elt) map -> 'acc ->  ('key, 'new_elt) map * 'acc``.
  Returns a list with the result of applying the
  function on each element of the list, plus an accumulator.

  .. .. tryliquidity:: tests/doc60.lov
  .. literalinclude:: tests/doc60.lov

The ``BigMap`` module
~~~~~~~~~~~~~~~~~~~~~

Big maps are a specific kind of maps, optimized for storing. They can
be updated incrementally and scale to a high number of associations,
whereas standard maps will have an expensive serialization and
deserialization cost. You are limited by Michelson to one big map per
smart contract, that should appear as the first element of the
storage. Big maps cannot be iterated.

* ``BigMap.empty<:TYPE, TYPE'>: (TYPE,TYPE') bigmap``. Returns the
  value associated with a key in the map. It is equivalent to ``GET``
  in Michelson.

  .. .. tryliquidity:: tests/doc61.lov
  .. literalinclude:: tests/doc61.lov

* ``BigMap.add: forall 'key. forall 'val. 'key -> 'val -> ('key, 'val) big_map -> ('key, 'val) bigmap``
   Returns the big map in argument with the new binding ``('key, 'val)`` if ``'key`` is not binded.

  .. .. tryliquidity:: tests/doc62.lov
  .. literalinclude:: tests/doc62.lov

* ``BigMap.remove: forall 'key. forall 'val. 'key -> ('key,'val) big_map -> ('key,'val) bigmap``.
  Returns the big map without the binding ``'key``.

  .. .. tryliquidity:: tests/doc63.lov
  .. literalinclude:: tests/doc63.lov

* ``BigMap.mem: forall 'key. forall 'val. 'key -> ('key, 'val) bigmap -> bool``. Returns ``true`` if an
  association exists in the big map for the key, ``false`` otherwise.

  .. .. tryliquidity:: tests/doc64.lov
  .. literalinclude:: tests/doc64.lov

* ``BigMap.find: forall 'key. forall 'val. 'key -> ('key,'val) bigmap -> 'val option``. Returns
  ``Some elt`` if the argument is bound to ``elt`` in the big map, ``None``
  otherwise.

  .. .. tryliquidity:: tests/doc65.lov
  .. literalinclude:: tests/doc65.lov

The ``Current`` module
~~~~~~~~~~~~~~~~~~~~~~

* ``Current.balance: unit -> dun``: returns the balance of the current
  contract. The balance contains the amount of dun that was sent by
  the current operation. It is equivalent to ``BALANCE`` in Michelson.

  .. .. .. tryliquidity:: tests/doc1.lov
  .. literalinclude:: tests/doc1.lov

* ``Current.time: unit -> timestamp``: returns the timestamp of the
  block in which the transaction is included. This value is chosen by
  the baker that is including the transaction, so it should not be
  used as a reliable source of alea.  It is equivalent to ``NOW`` in
  Michelson.

  .. .. tryliquidity:: tests/doc2.lov
  .. literalinclude:: tests/doc2.lov

* ``Current.amount: unit -> dun``: returns the amount of dun
  transferred by the current operation (standard or internal
  transaction). It is equivalent to ``AMOUNT`` in Michelson.

  .. .. tryliquidity:: tests/doc3.lov
  .. literalinclude:: tests/doc3.lov


* ``Current.gas: unit -> nat``: returns the amount of gas available to
  execute the rest of the transaction. It is equivalent to
  ``STEPS_TO_QUOTA`` in Michelson.

  .. .. tryliquidity:: tests/doc4.lov
  .. literalinclude:: tests/doc4.lov

* ``Current.self: unit -> address``: returns the address of the current
  contract.

  .. .. tryliquidity:: tests/doc_self.lov
  .. literalinclude:: tests/doc_self.lov

* ``Current.source: unit -> address``: returns the address that
  initiated the current top-level transaction in the blockchain. It is
  the same one for all the transactions resulting from the top-level
  transaction, standard and internal. It is the address that paid the
  fees and storage cost, and signed the operation on the
  blockchain. It is equivalent to ``SOURCE`` in Michelson.

  .. .. tryliquidity:: tests/doc5.lov
  .. literalinclude:: tests/doc5.lov

* ``Current.sender: unit -> address``: returns the address that
  initiated the current transaction. It is the same as the source for
  the top-level transaction, but it is the originating contract for
  internal operations. It is equivalent to ``SENDER`` in Michelson.

  .. .. tryliquidity:: tests/doc6.lov
  .. literalinclude:: tests/doc6.lov

* ``Current.cycle: unit -> cycle``: returns the cycle in which the
  transaction initiating the execution of the current smart contract
  has been performed.

  .. .. tryliquidity:: tests/doc_cycle.lov
  .. literalinclude:: tests/doc_cycle.lov

* ``Current.level: unit -> address``: returns the cycle in which the
  transaction initiating the execution of the current smart contract
  has been performed.

  .. .. tryliquidity:: tests/doc_level.lov
  .. literalinclude:: tests/doc_level.lov

The ``Contract`` module
~~~~~~~~~~~~~~~~~~~~~~~

* ``Contract.self<!CONTRACT TYPE>: unit -> (instance CONTRACT TYPE) option``.
  Returns the current executing contract. If the contract does not matched the
  signature in argument, returns ``None``.
  It is equivalent to ``SELF`` in Michelson.

  .. .. tryliquidity:: tests/doc11.lov
  .. literalinclude:: tests/doc11.lov

* ``Contract.at<!CONTRACT TYPE>: address -> (instance CONTRACT TYPE) option``.
  Returns the contract associated with the address. As for ``Contract.self``, it
  must be annotated with the type of the contract.

  .. .. tryliquidity:: tests/doc12.lov
  .. literalinclude:: tests/doc12.lov

* ``Contract.call: forall 'a. 'a entrypoint -> dun -> 'a -> operation``.
  Forges an internal contract call. It is equivalent to ``TRANSFER_TOKENS`` in Michelson.
  The entry point name is optional (``default`` by default).

  .. .. tryliquidity:: tests/doc13.lov
  .. literalinclude:: tests/doc13.lov

* ``Contract.view: forall 'a. 'a view -> 'a``.
  Calls a view from a contract. Views does not initiate transaction when
  they are called, i.e. they are called in the contect of the calling
  contract, but with the storage of the target contract.
  ``Contract.view [:typ] view`` deserializes the storage, which means that
  any change of the storage *after* is not taken into account.

  .. .. tryliquidity:: tests/doc_view.lov
  .. literalinclude:: tests/doc_view.lov

* ``Contract.set_delegate: keyhash option -> operation``. Forge a
  delegation operation for the current contract. A ``None`` argument
  means that the contract should have no delegate (it falls back to
  its manager). The delegation operation will only be executed in an
  internal operation if it is returned at the end of the ``%entry``
  function. It is equivalent to ``SET_DELEGATE`` in Michelson.

  .. .. tryliquidity:: tests/doc17.lov
  .. literalinclude:: tests/doc17.lov

* ``Contract.address: UnitContract instance -> address`` . Returns the address of
  a contract. It is equivalent to ``ADDRESS`` in Michelson.

  .. .. tryliquidity:: tests/doc18.lov
  .. literalinclude:: tests/doc18.lov

* ``Contract.create: forall 'a.
  keyhash option -> dun ->
  (sig type storage val%init storage : 'a end contract) ->'a -> (operation * address)``.
  Forges an operation
  to originate a contract with code. The contract is only created when
  the operation is executed, so it must be returned by the
  transaction. Note that the code must be specified as a contract
  structure (inlined or not).
  ``Contract.create manager_opt initial_amount (contract C) arg``
  forges an an origination operation for contract `C` with manager ``manager``,
  optional delegate ``delegate``, initial
  balance ``initial_amount`` and initial storage ``C.init arg``.

  Warning : ``'a`` must be a built-in type or a public alias of a built-in type.

  .. .. tryliquidity:: tests/doc19.lov
  .. literalinclude:: tests/doc19.lov

The ``Account`` module
~~~~~~~~~~~~~~~~~~~~~~

* ``Account.transfer: address -> dun -> operation``.
  Forges an internal transaction to the implicit (_i.e._ default) account contract
  in argument.

  .. .. tryliquidity:: tests/doc14.lov
  .. literalinclude:: tests/doc14.lov

* ``Account.default: keyhash -> instance UnitContract``. Returns
  the contract associated to the given ``keyhash`` with an empty
  signature. It is equivalent to ``IMPLICIT_ACCOUNT`` in Michelson.

  .. .. tryliquidity:: tests/doc16.lov
  .. literalinclude:: tests/doc16.lov

.. * Account.get_info: address -> (int * bool * address option * address list)
..   Returns the maximum rolls the account can be delegated, if the contract delegates,
..   the address of its optional administrator and the list of white listed adresses.

..  .. tryliquidity:: tests/doc66.lov
..  .. literalinclude:: tests/doc66.lov

* ``Account.manage: address -> int option -> bool -> address option -> address list -> operation``
  Returns an operation that updates the account informations. The operation will fail if
  the operation creator is not the administrator of the adress in argument.

  .. .. tryliquidity:: tests/doc67.lov
  .. literalinclude:: tests/doc67.lov

* ``Account.balance: address -> dun``
  Returns the amount of dun at a given address.

  .. .. tryliquidity:: tests/doc_balance.lov
  .. literalinclude:: tests/doc_balance.lov

The ``Crypto`` module
~~~~~~~~~~~~~~~~~~~~~

* ``Crypto.blake2b: bytes -> bytes``. Computes the cryptographic hash of
  a bytes with the cryptographic Blake2b function. It is equivalent to
  ``BLAKE2B`` in Michelson.

  .. .. tryliquidity:: tests/doc20.lov
  .. literalinclude:: tests/doc20.lov


* ``Crypto.sha256: bytes -> bytes``. Computes the cryptographic hash
  of a bytes with the cryptographic Sha256 function. It is translated
  to ``SHA256`` in Michelson.

  .. .. tryliquidity:: tests/doc21.lov
  .. literalinclude:: tests/doc21.lov


* ``Crypto.sha512: bytes -> bytes``. Computes the cryptographic hash of
  a bytes with the cryptographic Sha512 function. It is equivalent to
  ``SHA512`` in Michelson.

  .. .. tryliquidity:: tests/doc22.lov
  .. literalinclude:: tests/doc22.lov


* ``Crypto.hash_key: key -> keyhash``. Hash a public key and encode
  the hash in B58check. It is equivalent to ``HASH_KEY`` in Michelson.

  .. .. tryliquidity:: tests/doc23.lov
  .. literalinclude:: tests/doc23.lov


* ``Crypto.check: key -> signature -> bytes -> bool``. Check that the
  signature corresponds to signing the (Blake2b hash of the) sequence
  of bytes with the public key. It is equivalent to
  ``CHECK_SIGNATURE`` in Michelson. Signatures generated by
  ``dune-client sign bytes ...`` can be checked this way.

  .. .. tryliquidity:: tests/doc24.lov
  .. literalinclude:: tests/doc24.lov
