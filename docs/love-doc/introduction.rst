
Introduction
============

The Dune network provides a statically typed low level language for
Smart Contracts, called Michelson. Although Michelson can be used
directly, the lack of variables and the use of stack-based instructions
make it impracticable to write, and even harder to read. The success
of Liquidity, a Michelson compiler with an OCaml inspired syntax,
inspired Origin Labs to develop Love.
Love (for Low Ocaml for Verification & Efficiency) is a language
for Smart Contracts at the boundary of low-level languages -- such
as Michelson -- and higher level languages -- such as OCaml & Michelson.

Love follows Michelson & Liquidity type-system, but implemented on a subset of
the OCaml syntax and is based on the system-F type system.
