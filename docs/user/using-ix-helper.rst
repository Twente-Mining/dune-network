.. _dun:

Using the ``ix`` Helper Tool
=============================

The ``ix`` tool is an helper tool to simplify the use of
``dune-node`` and ``dune-client`` (these binaries are expected to be
in the same directory, either installed by ``make install`` or sitting
in the build directory).

Basic Principles
----------------

At any time, ``ix`` has a configuration containing 3 values:

* the current network to interact with
* the current node to interact with
* the current client to interact with

A network (also called a switch) is the name of a network, either one
of the two predefined ones, ``mainnet`` and ``testnet``, or created by
the user (a word with only letters and numbers, starting with a
letter).

A node is the name of a node, either predefined (currently, only
``dunscan`` is predefined, and only for ``mainnet`` and ``testnet``
networks), or created by the user. Created nodes are named either
``node`` or ``nodeNNN`` where ``NNN`` is a number (``node`` and
``node0`` are the same one).

A client is the name of a client, either predefined (currently, only
``client`` is predefined for ``mainnet`` and ``testnet``) or
created. Clients are named either ``client`` or ``clientNNN`` where
``NNN`` is a number (``client`` and ``client0`` are the same one).

Most ``ix`` commands will call ``dune-node`` and other commands using
the current network/node/client configuration. For example::

  $ ix switch testnet
  $ ix client
  COMMAND: /usr/bin/dune-client -A testnet-node.dunscan.io -P 8734 -d /home/user/.dune-testnet-client
  [...]

When calling such commands, ``ix`` will:

* Set Dune environment variables:

  * Set ``DUNE_CONFIG`` to the current network configuration
  * Set ``DUNE_CLIENT_UNSAFE_DISABLE_DISCLAIMER`` to ``y``
  * Set ``DUNE_CONTEXT_STORAGE`` to ``ironmin`` (i.e. always use Ironmin)

* Add command specific arguments (addr/port of the node, directory of the
  node or the client configuration, etc.)

Command List
------------

Basic Commands
~~~~~~~~~~~~~~

``ix`` understands the following basic commands:

* ``ix list`` displays all the available networks (``mainnet`` and
  ``testnet`` at the beginning)::

    $ ix list
    Available switches:
    mainnet (current: dunscan.client)
    testnet

* ``ix info`` displays the configuration for the current network
* ``ix info NETWORK`` displays the configuration for network ``NETWORK``::

    $ ix info mainnet
    Switch "mainnet" (current):
      nodes: 1
        dunscan (current)
      clients: 1
        client (current)

* ``ix switch NETWORK`` switches the configuration to use ``NETWORK``::

    $ ix switch testnet
    Current switch: testnet.dunscan.client

* ``ix switch NODE`` changes the configuration of the current network
  to use node ``NODE``
* ``ix switch CLIENT`` changes the configuration of the current network
  to use node ``CLIENT``
* ``ix switch NETWORK.NODE.CLIENT`` changes the configuration to use
  network ``NETWORK``, and use ``NODE`` and ``CLIENT`` in that
  network.
* ``ix switch`` displays information on the current network::

    $ ix switch
    Current switch: mainnet
      node:   dunscan
         addr: mainnet-node.dunscan.io:8733
      client: client
      DUNE_CONFIG= mainnet
      DUNE_BINDIR= /home/lefessan/GIT/dune-network

* Proxy commands. All these commands will call the associated command
  with the current configuration (network/node/client):

  * ``ix client ARGS`` will call ``dune-client CLIENT_CONFIG_ARG ARGS``
  * ``ix admin ARGS`` will call ``dune-admin-client CLIENT_CONFIG_ARG ARGS``
  * ``ix node ARGS`` will call ``dune-node NODE_CONFIG_ARGS ARGS``
  * ``ix accuser ARGS`` will call ``dune-accuser-all CLIENT_CONFIG_ARGS ARGS``
  * ``ix baker ARGS`` will call ``dune-baker-all CLIENT_CONFIG_ARGS ARGS``
  * ``ix endorser ARGS`` will call ``dune-endorser CLIENT_CONFIG_ARGS ARGS``
  * ``ix signer ARGS`` will call ``dune-signer CLIENT_CONFIG_ARGS ARGS``

* ``ix help`` displays a basic help with most useful commands
* ``ix --version`` displays the current version of ``ix``

Using ``ix`` for RPC calls
~~~~~~~~~~~~~~~~~~~~~~~~~~~

``ix`` understands the following RPCs to trigger RPC calls to the
current configuration node:

* ``ix rpc`` open a browser on the current node webserver
* ``ix rpc PATH`` calls the RPC ``PATH`` on the current node::

    $ ix rpc /chains/main/blocks/head/hash
    Downloading http://mainnet-node.dunscan.io:8733/chains/main/blocks/head/hash ...
    "BLehTAfwSYic8SjgMZCr4m8e7RvASj5Lzaw3nNARJAgGGYKbJyL"

Some shortcuts are available for RPCs:

* ``ix rpc head`` calls ``/chains/main/blocks/head``
* ``ix rpc header`` calls ``/chains/main/blocks/head/header``
* ``ix rpc hash`` calls ``/chains/main/blocks/head/hash``
* ``ix rpc level`` calls ``/chains/main/blocks/head/helpers/current_level``
* ``ix rpc version`` calls ``/dune/version``

Also, the following command will start a browser and connect to the
node internal webserver::

  ix rpc /

The internal webserver provides a simplified block explorer and an RPC
explorer.

Creating a new node
~~~~~~~~~~~~~~~~~~~

To create a new node for the current network, use ``ix create
NODE_NAME`` where ``NODE_NAME`` is either ``node`` or ``nodeNNN``
where ``NNN`` is a number::

  $ ix create node1
  Creating node "node1"............
  COMMAND: /usr/bin/dune-node identity generate --data-dir /home/user/.dune/switches/mainnet/node1
  DUNE_CONFIG="mainnet"
  Config: Mainnet. Revision: 3
  Generating a new identity... (level: 26.00)
  Stored the new identity (idtvBfpCuttLmssbWJ27vtUXJJNwoe) into '/home/user/.dune/switches/mainnet/node1/identity.json'.
  COMMAND: /usr/bin/dune-node config init --net-addr [::]:9734 --rpc-addr 127.0.0.1:8734 --data-dir /home/user/.dune/switches/mainnet/node1
  DUNE_CONFIG="mainnet"
  Config: Mainnet. Revision: 3
  Saving node config in "/home/user/.dune/switches/mainnet/node1/config.json"
  Current switch: mainnet.node1.client

``ix`` automatically creates an identity for the node and a default
configuration.

If you are using the ``mainnet`` or ``testnet`` networks, you can
start the node from a snapshot using ``ix restore``. It will
automatically download a snapshot if you have never downloaded one, or
if it is older than 2 days::

  $ ix restore
  Downloading https://dune.network/files/snapshots/mainnet/LAST.txt ...
  Downloading latest snapshot mainnet_full_2020-03-01T00:00:01Z_LVL_BMNiKs3qATVtuQGPSeVGkmXQZULpuqVTFwqExNZP255SiHuFbRM.snapshot
  Downloading https://dune.network/files/snapshots/mainnet/mainnet_full_2020-03-01T00:00:01Z_LVL_BMNiKs3qATVtuQGPSeVGkmXQZULpuqVTFwqExNZP255SiHuFbRM.snapshot
  COMMAND: /usr/bin/dune-node snapshot import /home/user/.dune/switches/mainnet/snapshots/mainnet_full_2020-03-01T00:00:01Z_LVL_BMNiKs3qATVtuQGPSeVGkmXQZULpuqVTFwqExNZP255SiHuFbRM.snapshot --data-dir /home/user/.dune/switches/mainnet/node1
  [...]

If you already have a node running, you can use ``ix save`` to create
a local snapshot, that will be used by ``ix restore`` for other nodes::

  $ DUNE_CONFIG=node2 ix save
  COMMAND: /home/lefessan/GIT/dune-network/dune-node snapshot export /home/lefessan/.dune/switches/mainnet/snapshots --data-dir /home/lefessan/.dune/switches/mainnet/node2
  DUNE_CONFIG="mainnet"
  Config: Mainnet. Revision: 3
  [...]
  Mar  1 18:07:28 - shell.snapshots: Successful export: /home/user/.dune/switches/mainnet/snapshots/mainnet_full_2020-02-29T23:59:55Z_249228_BMNiKs3qATVtuQGPSeVGkmXQZULpuqVTFwqExNZP255SiHuFbRM.snapshot

You can then start the node using ``ix run``::

  $ ix run

You can also clean the storage of a node using ``ix reset``::

  $ ix reset

Finally, you can get rid of a created node (that is not the current
one) using ``ix remove NODE_NAME``::

  $ ix switch dunscan
  $ ix remove node1

Creating a new Network
~~~~~~~~~~~~~~~~~~~~~~

The command ``ix create NETWORK`` can be used to create a new
network, an associated node ``node`` and an associated client
``client``. As a specific case, if a network name is ``sandboxNNN``
where ``NNN`` is a number, the accounts are in the client are always
the same ones.

For example::

  $ ix create sandbox2
  Creating node "node"............
  [...]
  Creating client "client"............
  Current switch: sandbox2.node.client

The command automatically generates:

* A directory ``$(HOME)/.dune/switches/NETWORK/``
* A file ``$(HOME)/.dune/switches/NETWORK/NETWORK-config.json``
  containing the network configuration to be used to start the nodes
  and clients.
* A file ``$(HOME)/.dune/switches/NETWORK/NETWORK-parameters.json``
  containing the parameters to use for activation.

You may want to look at the network configuration and parameters files
before starting the network.

You can then start the first node using::

  $ ix run
  [...]

The node is now running, but with the genesis protocol. We must now
activate the node with ``ix activate``::

  $ ix activate
  COMMAND: /usr/bin/dune-client -A 127.0.0.1 -P 8733 -d /home/user/.dune/switches/sandbox2/client -block genesis activate protocol PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS with fitness 1 and key activator and parameters /home/user/.dune/switches/sandbox2/sandbox2-parameters.json
  [...]
  Injected BMVRt1RrYcnL

We have just injected a transition block to the new protocol.

Now, we can start the baker and endorser associated with the node
using the simple command ``ix baker``::

  $ ix bake
  COMMAND: /usr/bin/dune-baker-all -A 127.0.0.1 -P 8733 -d /home/user/.dune/switches/sandbox2/client run with local node /home/user/.dune/switches/sandbox2/node bootstrap4 bootstrap3 bootstrap2 bootstrap1
  COMMAND: /usr/bin/dune-endorser-all -A 127.0.0.1 -P 8733 -d /home/user/.dune/switches/sandbox2/client run bootstrap4 bootstrap3 bootstrap2 bootstrap1
  Baker and Endorser started. Use these commands to watch the logs:
    tail -f /home/user/.dune/switches/sandbox2/node/baker.log
    tail -f /home/user/.dune/switches/sandbox2/node/endorser.log

The command ``ix create NETWORK`` also creates several accounts:

* 4 baker accounts of ``4_000_000`` DUN, called ``bootstrap1``,
  ``bootstrap2``, ``bootstrap3`` and ``bootstrap4``.
* 1 super-admin account ``activator`` with ``100`` DUN.
* 10 user accounts of ``100`` DUN, called ``user0`` to ``user9``.

All the ``sandboxNNN`` networks use the following accounts public key
hashes:

* ``bootstrap1``: ``dn1f7ujdgCTZg7AvhTuadmVrcgUZrdrcN6jT``
* ``bootstrap2``: ``dn1QHrzv66HAT2NjFdj1sEwvMasXuWtHVqJV"``
* ``bootstrap3``: ``dn1W9Vnwqcw5kZQr4HM6nrS5qYUXAmderFyR``
* ``bootstrap4``: ``dn1QEurh3rBReQvArunakVXyTyktQX5VFSEh``
* ``activator``: ``dn1RvpNRW6hiTL8oyRbD3TegDrM8gtb5WULi``

We can create and start another node::

  $ ix create node2
  [...]
  Current switch: sandbox2.node2.client
  $ ix run

The two running nodes ``node`` and ``node2`` should immediately connect to
each other and propagate blocks between them.

All the commands ``ix run``, ``ix activate`` and ``ix bake`` can be
done in one single command::

  ix create sandbox2
  ix start

will create the ``sandbox2`` network, run a first node, activate the
network and start a baker and endorser.

It is sometimes useful to only bake blocks when new operations are
available. For that, you can start the baker in *lazy* mode, using the
``--lazy`` argument::

  ix start --lazy

or::

  ix bake --lazy

If you have started a node, you can stop it either by using CTRL-C in
its terminal, or by using::

  ix stop

You can list running nodes for the current network with::

  ix jobs
  
Accessing Web Pages
~~~~~~~~~~~~~~~~~~~

``ix`` also provides an easy way to access Dune-related webpages:

* ``ix www doc`` : open the documentation page
* ``ix www faucet`` : open the Faucet page
* Use ``ix www --list`` to list more available shortcuts

Full List of Commands
~~~~~~~~~~~~~~~~~~~~~

Use ``ix COMMAND --help`` to get more help.

Network related commands:

* ``ix info`` : Print information on a network
* ``ix create`` : Create a new resource
* ``ix switch`` : Switch to a resource network/node/client
* ``ix remove`` : Remove a switch/network
* ``ix list`` : List all networks

Running nodes and bakers:
  
* ``ix start`` : Start a network node, activate the network and bake
* ``ix stop`` : Stop the current node
* ``ix jobs`` : Check status of processes
* ``ix run`` : Start the current node
* ``ix activate`` : Activate the most recent protocol using the current node
* ``ix bake`` : Start a baker and endorser on the current node

Saving and restoring node storage:
  
* ``ix save`` : Save a snapshot of the current node
* ``ix reset`` : Clear the storage of the current node
* ``ix restore`` : Restore the current node from the last snapshot

Accessing local and remote pages:
  
* ``ix rpc`` : Query the RPC server of the current node
* ``ix wget`` : Download a webpage by URL
* ``ix www`` : Open web page solidity files
* ``ix contracts`` : Download all contracts and format them as text rows

Proxy commands:

* ``ix client`` : Proxy command for dune-client
* ``ix node`` : Proxy command for dune-node
* ``ix admin`` : Proxy command for dune-admin-client
* ``ix baker`` : Proxy command for dune-baker-all
* ``ix endorser`` : Proxy command for dune-endorser-all
* ``ix accuser`` : Proxy command for dune-accuser-all
* ``ix signer`` : Proxy command for dune-signer

Misc commands:

* ``ix rotate`` : Rotate log files

  
Environment Variables
---------------------

``ix`` uses the following environment variables:

* ``IX_SWITCH``, used to override the current configuration. The
  variable can contain a switch name and/or a node name and/or a
  client name. For example::

    IX_SWITCH=mainnet ix info
    IX_SWITCH=mainnet.node2 ix info
    IX_SWITCH=testnet.dunscan.client ix info
    IX_SWITCH=client2 ix info

* ``DUNE_SWITCH`` is used only if ``IX_SWITCH`` is not defined. It
  does the same thing;
* ``DUNE_BINDIR`` can contain the directory used to find ``dune-node``,
  ``dune-client`` and other commands;
* ``HOME`` is the user home directory. ``ix`` will not execute if this
  variable is not defined.
  
Directories and Files
---------------------

``ix`` uses the following directories (that will be created the first
time ``ix`` is run):

* ``$(HOME)/.dune/`` is the directory containing most files, including most
  node and wallet storages
* ``$(HOME)/.dune/CURRENT`` contains the name of the current switch/network
* ``$(HOME)/.dune/switches/`` contains all the available switches

Within a switch directory, the following files are used:

* ``CURRENT`` contains the current switch configuration, i.e. the current node
  and client
* ``STATE`` contains the switch description, i.e. the list of nodes
  and clients, with their parameters
* ``nodeNNN/``, where ``NNN`` is empty or a number, is the directory for
  ``nodeNNN``
* ``clientNNN/``, where ``NNN`` is empty or a number, is the directory
  for ``clientNNN``
* ``snapshots/`` contains available snapshots (saved or downloaded)
  
As an exception, ``ix`` keeps the default directories for Mainnet and
Testnet nodes and clients, i.e.:
* ``$(HOME)/.dune-node`` for the first Mainnet node
* ``$(HOME)/.dune-client`` for the first Mainnet client
* ``$(HOME)/.dune-testnet-node`` for the first Testnet node
* ``$(HOME)/.dune-testnet-node`` for the first Testnet client
*


