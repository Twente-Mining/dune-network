Baker Account Management
========================

Dune Network provides additional features for bakers to manage the
account used for baking. Some of these features, such as the white
list, can also be used for any other account. These features are all
available after the protocol upgrade to Babylon+.

Account Administrator
---------------------

Every account can have an administrator account to manage it. By
default, the administrator account is the account itself.

The command to set the admin is::

  $ dune-client dune set admin NEWADMIN of ACCOUNT with admin CURRENTADMIN

Once the operation has been included in the chain, all operations to
manage the account ACCOUNT must specify NEWADMIN as admin, and the
client must know the secrete key of NEWADMIN.

To unset the administrator account, you need to set the account back
as its own administrator::

  $ dune-client dune set admin ACCOUNT of ACCOUNT with admin CURRENTADMIN

Disabling/Enabling Delegation
-----------------------------

A baker can control whether delegations are allowed or not.
To disable delegations::
  
  $ dune-client dune set delegation false of ACCOUNT with admin CURRENTADMIN

Now, all attempts to delegate an account to this baker will fail.

To enable delegations again::
  
  $ dune-client dune set delegation true of ACCOUNT with admin CURRENTADMIN

Removing Delegations
--------------------

It is possible for a baker that wants to stop its activity to remove
all current delegations, using::
  
  $ dune-client dune clear delegations of ACCOUNT

You can now list all delegations and it should return an empty list::
  
  $ dune-client rpc get /chains/main/blocks/head/context/delegates/dn1.../delegated_contracts

Controling the Number of Rolls
------------------------------

A baker can control the maximal number of rolls owned by its baker::

  $ dune-client dune set maxrolls NUMBER of ACCOUNT with admin CURRENTADMIN

Even if its staking balance increases to a value where the number of
rolls would overflow `maxrolls`, the real number of rolls will still
remain at `maxrolls`. Any attempt to delegate an account to a baker
who has reached its maximal number of rolls will immediately fail.

It is possible to completely disable a baker with::

  $ dune-client dune set maxrolls 0 of ACCOUNT with admin CURRENTADMIN

Of course, the impact of the operation will only be noticeable 7
cycles later, since the current number of rolls in only used to
compute baking right 7 cycles later.

Finally, a baker can remove the limit::

  $ dune-client dune set maxrolls none of ACCOUNT with admin CURRENTADMIN

It is possible to check at any time the current nunber of rolls, the
maximum number of rolls and the number of rolls that could be
available with::

  $ dune-client dune get account info for ACCOUNT
  { "balance": "1159219153170", "frozen_balance": "22229000000",
    "counter": "18071", "desactivation_cycle": 110,
    "nrolls": 100, "maxrolls": 100, "nrolls_avail": 135 }

Controling Destinations of Transfers
------------------------------------

It is also possible to control the list of accounts to which a tranfer
can be done, from the baker account, using a white list. The white
list can only be used on implicit accounts (dn...), not originated
ones (KT1...). The benefit is that, if the account secret key is
compromised, the attacker cannot transfer tokens anywhere.

To set a white-list, you must first set an administrator account
(otherwise, the attacker could change the white list). Then, you can
use the following command::

  $ dune-client dune set whitelist DEST1,DEST2,... of ACCOUNT with admin CURRENTADMIN

Once a white list has been set, only DEST1, DEST2 can receive tokens
from the account (a baker can, for example, only white list its
payment account). Also, originations of contracts are forbidden.

The white list can be deleted using::

  $ dune-client dune set whitelist none of ACCOUNT with admin CURRENTADMIN

Viewing/modifying Everything at once
------------------------------------

It is possible to check the current settings of an account using::

   $ dune-client dune get account parameters for ACCOUNT
   { "admin": "dn1ax1fXs5KD3MzoV6tcAyZ6rGLMaHBTYz5f",
     "white_list": [ "dn1ax1fXs5KD3MzoV6tcAyZ6rGLMaHBTYz5f" ]
   }

It is then possible to save these values in a JSON file, modify them
and update the account with the new values using::

  $ dune-client dune change account parameters JSONFILE of ACCOUNT with admin ACCOUNT

New Michelson Instructions
--------------------------

