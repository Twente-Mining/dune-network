(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Tezos_micheline (* For Micheline_parser *)

open Dune_lang_v1_parser

let print_expr fmt = function
  | Script.Michelson_expr expr ->
      Michelson_v1_printer.print_expr fmt expr
  | Dune_code code -> (
    match Love_repr.is_code code with
    | Some code -> (
      match code with
      | Contract s ->
          Format.fprintf fmt "%a" Love_printer.Ast.print_structure s.code
      | LiveContract c ->
          Format.fprintf
            fmt
            "%a"
            Love_printer.Value.print_livestructure
            c.root_struct
      | FeeCode {fee_codes = fc; _} ->
          List.iter
            (fun (n, (v, _t)) ->
              Format.fprintf fmt "%s = %a" n Love_printer.Value.print v)
            fc )
    | None ->
        assert false )
  | Dune_expr dexpr -> (
    match Love_repr.is_const dexpr with
    | Some (Value v) ->
        Format.fprintf fmt "%a" Love_printer.Value.print v
    | Some (Type t) ->
        Format.fprintf fmt "%a" Love_type.pretty t
    | None ->
        assert false )

let print_expr_unwrapped fmt = function
  | Script.Michelson_expr expr ->
      Michelson_v1_printer.print_expr_unwrapped fmt expr
  | expr ->
      print_expr fmt expr

let print_execution_trace = Michelson_v1_printer.print_execution_trace

let print_with_injected_types type_map fmt parsed =
  match parsed.original with
  | Michelson_parsed parsed ->
      let program = Michelson_v1_printer.inject_types type_map parsed in
      Micheline_printer.print_expr fmt program
  | Dune_parsed ->
      Format.fprintf fmt "%s" parsed.source

let unparse_toplevel ?type_map expr =
  match expr with
  | Script.Michelson_expr expr ->
      let original = Michelson_v1_printer.unparse_toplevel ?type_map expr in
      {
        source = original.source;
        expanded = Script.Michelson_expr original.expanded;
        original = Michelson_parsed original;
      }
  | _ ->
      let source = Format.asprintf "%a" print_expr expr in
      {source; expanded = expr; original = Dune_parsed}

let print_big_map_diff = Michelson_v1_printer.print_big_map_diff print_expr
