(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context

(* open Tezos_micheline *)
(* open Dune_lang_more (* Michelson_v1_* = Dune_lang_v1_* *) *)
open Dune_injection

type program =
  | Program of string
  (*  Michelson_v1_parser.parsed Micheline_parser.parsing_result *)
  | Program_hash of Script_expr_hash.t
  | Contract_hash of Contract.t

module Program_or_hash : sig
  val source_param :
    ?name:string ->
    ?desc:string ->
    ('a, (#Client_context.wallet as 'wallet)) Clic.params ->
    (program -> 'a, 'wallet) Clic.params
end

module Ed25519_public_key_hash : sig
  val param :
    ?name:string ->
    ?desc:string ->
    ('a, (#Client_context.wallet as 'wallet)) Clic.params ->
    (Ed25519.Public_key_hash.t -> 'a, 'wallet) Clic.params
end

val batch_transfer :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:public_key_hash ->
  src_pk:public_key ->
  src_sk:Client_keys.sk_uri ->
  destinations:(Contract.t * Tez.t * Z.t option * string * string option) list ->
  total:Tez.t ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  ?batch_size:int ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  (Apply_results.packed_contents_result list * Contract.t list) tzresult Lwt.t

val activate_protocol_operation :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:public_key_hash ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->
  level:int32 ->
  ?protocol:Protocol_hash.t ->
  ?protocol_parameters:Dune_parameters.parameters ->
  ?fee:Tez.tez ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  unit tzresult Lwt.t

val manage_accounts_operation :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:public_key_hash ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->
  protocol_parameters:MBytes.t ->
  ?fee:Tez.tez ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  unit tzresult Lwt.t

val manage_account_operation :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:public_key_hash ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->
  ?target:Signature.Public_key_hash.t * Signature.t ->
  options:MBytes.t ->
  ?fee:Tez.tez ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  unit tzresult Lwt.t

val clear_delegations_operation :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:public_key_hash ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->
  ?fee:Tez.tez ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  unit tzresult Lwt.t

val forge_set_delegate :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?fee:Tez.t ->
  source:Signature.public_key_hash ->
  src_pk:public_key ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  public_key_hash option ->
  Kind.delegation Kind.manager forge_result tzresult Lwt.t

val forge_register_as_delegate :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?fee:Tez.t ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  public_key ->
  Kind.delegation Kind.manager forge_result tzresult Lwt.t

val forge_originate_contract :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  delegate:public_key_hash option ->
  initial_storage:string ->
  balance:Tez.t ->
  source:public_key_hash ->
  src_pk:public_key ->
  ?code_hash:Script_expr_hash.t ->
  ?code:Script.expr ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  unit ->
  (MBytes.t * Contract.t option) tzresult Lwt.t

val forge_transfer :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  source:public_key_hash ->
  src_pk:public_key ->
  destination:Contract.t ->
  ?entrypoint:string ->
  ?arg:string ->
  amount:Tez.t ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  ?collect_call:collect_call ->
  ?forge_file:string ->
  unit ->
  Kind.transaction Kind.manager forge_result tzresult Lwt.t

val forge_reveal :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  source:public_key_hash ->
  src_pk:public_key ->
  ?fee:Tez.t ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  unit ->
  Kind.reveal Kind.manager forge_result tzresult Lwt.t

val source_to_public_key :
  #Protocol_client_context.full ->
  Contract.t ->
  (Signature.public_key_hash * Signature.public_key) tzresult Lwt.t

val get_frozen_balance :
  #Protocol_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  Signature.public_key_hash ->
  Tez_repr.tez tzresult Lwt.t
