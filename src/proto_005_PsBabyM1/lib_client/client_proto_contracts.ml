(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Protocol
open Alpha_context

(* Either a dn* or KT* *)
module ContractEntity = struct
  type t = Contract.t

  let encoding = Contract.encoding

  let of_source s =
    match Contract.of_b58check s with
    | Error _ as err ->
        Lwt.return (Environment.wrap_error err)
        |> trace
             (failure
                "%S is not a correct contract identifier, or the identifier \
                 is unknown."
                s)
    | Ok s ->
        return s

  let to_source s = return (Contract.to_b58check s)

  let name = "contract"
end

module RawContractAlias = Client_aliases.Alias (ContractEntity)

module ContractAlias = struct
  open Client_aliases.Ambiguity

  let load = RawContractAlias.load

  let find_contract cctxt name =
    RawContractAlias.find_opt cctxt name >>= lists_of_option

  let find_key cctxt name =
    Client_keys.Public_key_hash.find_opt cctxt name
    >>= fun v ->
    Lwt.return
      ( match v with
      | Ok (Some v) ->
          ([Contract.implicit_contract v], [])
      | Ok None ->
          ([], [])
      | Error errs ->
          ([], errs) )

  let find cctxt name =
    find_contract cctxt name
    >>= fun lists_contract ->
    find_key cctxt name
    >>= fun lists_key -> lists_combine lists_contract lists_key

  let rev_find cctxt c =
    match Contract.is_implicit c with
    | Some hash -> (
        Client_keys.Public_key_hash.rev_find cctxt hash
        >>=? function
        | Some name -> return_some ("key:" ^ name) | None -> return_none )
    | None ->
        RawContractAlias.rev_find cctxt c

  let get_contract_alias cctxt s =
    match String.split ~limit:1 ':' s with
    | ["key"; key] ->
        find_key cctxt key
    | ["contract"; key] ->
        find_contract cctxt key
    | _ ->
        find cctxt s

  let get_contract_alias cctxt s =
    get_contract_alias cctxt s
    >>= fun v ->
    check_ambiguity
      ~kind:"contract"
      ~names:["contract"; "key"]
      ~prefixes:["contract:"; "key:"]
      s
      v

  let find_hash name = ContractEntity.of_source name >>= lists_of_error

  let get_contracts cctxt s =
    match String.split ~limit:1 ':' s with
    | ["alias"; alias] ->
        find cctxt alias
    | ["key"; text] ->
        find_key cctxt text
    | ["contract"; text] ->
        find_contract cctxt text
    | ["chash"; text] ->
        find_hash text
    | _ ->
        find_hash s
        >>= fun res1 -> find cctxt s >>= fun res2 -> lists_combine res1 res2

  let autocomplete cctxt =
    Client_keys.Public_key_hash.autocomplete cctxt
    >>=? fun keys ->
    RawContractAlias.autocomplete cctxt
    >>=? fun contracts -> return (List.map (( ^ ) "key:") keys @ contracts)

  let alias_param ?(name = "name") ?(desc = "existing contract alias") next =
    let desc =
      desc ^ "\n"
      ^ "Can be a contract alias or a key alias (autodetected in order).\n\
         Use 'key:name' to force the later."
    in
    Clic.(
      param
        ~name
        ~desc
        (parameter ~autocomplete (fun cctxt p -> get_contract_alias cctxt p))
        next)

  let get_contract cctxt s =
    get_contracts cctxt s
    >>= check_ambiguity
          ~kind:"contract"
          ~names:["contract"; "key"; "hash"]
          ~prefixes:["contract:"; "key:"; "chash:"]
          s

  let destination_parameter () =
    Clic.parameter
      ~autocomplete:(fun cctxt ->
        autocomplete cctxt
        >>=? fun list1 ->
        Client_keys.Public_key_hash.autocomplete cctxt
        >>=? fun list2 -> return (list1 @ list2))
      get_contract

  let destination_param ?(name = "dst") ?(desc = "destination contract") next =
    let desc =
      String.concat
        "\n"
        [ desc;
          "Can be an alias, a key, or a literal (autodetected in order).\n\
           Use 'text:literal', 'alias:name', 'key:name' to force." ]
    in
    Clic.param ~name ~desc (destination_parameter ()) next

  let destination_arg ?(name = "dst") ?(doc = "destination contract") () =
    let doc =
      String.concat
        "\n"
        [ doc;
          "Can be an alias, a key, or a literal (autodetected in order).\n\
           Use 'text:literal', 'alias:name', 'key:name' to force." ]
    in
    Clic.arg ~long:name ~doc ~placeholder:name (destination_parameter ())

  let name cctxt contract =
    rev_find cctxt contract
    >>=? function
    | None -> return (Contract.to_b58check contract) | Some name -> return name
end

let list_contracts cctxt =
  RawContractAlias.load cctxt
  >>=? fun raw_contracts ->
  cctxt#add_subst "contracts" (String.concat "," (List.map fst raw_contracts)) ;
  Lwt_list.map_s
    (fun (n, v) ->
      cctxt#add_subst ("contract:" ^ n) (Contract.to_b58check v) ;
      Lwt.return ("", n, v))
    raw_contracts
  >>= fun contracts ->
  Client_keys.Public_key_hash.load cctxt
  >>=? fun keys ->
  cctxt#add_subst "keys" (String.concat "," (List.map fst keys)) ;
  (* List accounts (implicit contracts of identities) *)
  map_s
    (fun (n, v) ->
      RawContractAlias.mem cctxt n
      >>=? fun mem ->
      let p = if mem then "pkh:" else "" in
      let v' = Contract.implicit_contract v in
      cctxt#add_subst ("pkh:" ^ n) (Contract.to_b58check v') ;
      return (p, n, v'))
    keys
  >>=? fun accounts -> return (contracts @ accounts)

let get_delegate cctxt ~chain ~block source =
  Alpha_services.Contract.delegate_opt cctxt (chain, block) source
