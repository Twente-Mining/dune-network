(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Clic

let batch_size_arg =
  arg
    ~long:"batch-size"
    ~short:'b'
    ~placeholder:"nb"
    ~doc:"size of batches for multiple operations operations"
    (parameter (fun _ s ->
         try return (int_of_string s) with _ -> failwith "invalid batch-size"))

let collect_switch =
  switch
    ~long:"collect-call"
    ~doc:"make a collect call, fee paid by destination"
    ()

let forge_switch =
  Clic.switch
    ~long:"forge"
    ~doc:"forge a serialized operation for later signing"
    ()

let out_arg =
  Clic.arg
    ~long:"out"
    ~placeholder:"file.out"
    ~doc:"dump result to file.out"
    (Clic.parameter (fun _ s -> return s))

let collect_fee_gas_arg =
  Clic.arg
    ~long:"collect-call-fee-gas"
    ~placeholder:"limit"
    ~doc:"Set the gas limit for executing the fee code of a collect call"
    (Clic.parameter (fun _ s ->
         try
           let v = Z.of_string s in
           assert (Compare.Z.(v >= Z.zero)) ;
           return v
         with _ -> failwith "invalid gas limit (must be a positive number)"))

let verbose_signing_switch =
  Clic.switch
    ~long:"verbose-signing"
    ~doc:"display extra information before signing"
    ()

let collect_call_switch =
  Clic.switch ~long:"collect-call" ~doc:"Run code as in a collect call" ()

let output_arg =
  default_arg
    ~doc:"write to a file"
    ~long:"output"
    ~short:'o'
    ~placeholder:"path"
    ~default:"-"
    (parameter (fun _ ->
       function
       | "-" ->
           return Format.std_formatter
       | file ->
           let ppf = Format.formatter_of_out_channel (open_out file) in
           ignore Clic.(setup_formatter ppf Plain Full) ;
           return ppf))

let value_param ~name ~desc next =
  Clic.param ~name ~desc (parameter (fun _ s -> return s)) next
