(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context

type error += Unsupported_language of string

type error += Invalid_json of string

type error += Parsing_error of string

let () =
  let open Data_encoding in
  (* Reject *)
  register_error_kind
    `Permanent
    ~id:"dune_lang_v1_parser.unsupported_language"
    ~title:"The specified language is not supported"
    ~description:"An unspecified language was specified"
    (obj1 (req "lang" string))
    (function Unsupported_language s -> Some s | _ -> None)
    (fun s -> Unsupported_language s) ;
  (* Reject *)
  register_error_kind
    `Permanent
    ~id:"dune_lang_v1_parser.invalid_json"
    ~title:"The provided JSON could not be decoded in the specified language"
    ~description:"An invalid JSON was provided for the specified language"
    (obj1 (req "lang" string))
    (function Invalid_json s -> Some s | _ -> None)
    (fun s -> Invalid_json s) ;
  (* Reject *)
  register_error_kind
    `Permanent
    ~id:"dune_lang_v1_parser.parsing_error"
    ~title:"Parsing Error"
    ~description:"The source contains a parsing error"
    (obj1 (req "source" string))
    (function Parsing_error s -> Some s | _ -> None)
    (fun s -> Parsing_error s) ;
  ()

type original = Michelson_parsed of Michelson_v1_parser.parsed | Dune_parsed

type parsed = {source : string; expanded : Script.expr; original : original}

let make_dummy src =
  let open Tezos_micheline.Micheline in
  let dummy =
    Michelson_v1_parser.
      {
        source = src;
        unexpanded = strip_locations (Seq ((), []));
        expanded = strip_locations (Seq ((), []));
        expansion_table = [];
        unexpansion_table = [];
      }
  in
  {
    source = src;
    expanded = Michelson_expr dummy.expanded;
    original = Michelson_parsed dummy;
  }

let get_header s =
  if String.length s >= 1 && s.[0] = '#' then
    let rec iter s pos len =
      if pos = len then pos
      else
        match s.[pos] with
        | '\n' | '\r' | ':' | ' ' | '\t' ->
            pos
        | _ ->
            iter s (pos + 1) len
    in
    let i = iter s 1 (String.length s) in
    if i - 1 <= 0 then (None, 0) else (Some (String.sub s 1 (i - 1)), i)
  else (None, 0)

let ok_or_nil = function
  | Ok list ->
      Lwt.return list
  | Error _ ->
      Lwt.return []

module StringMap = Map.Make (String)

let subst_in_source (cctxt : #Protocol_client_context.full) src =
  let aliases = ref StringMap.empty in
  let add a b = aliases := StringMap.add a b !aliases in
  Client_keys.Public_key_hash.load cctxt
  >>= ok_or_nil
  >>= fun pkh_aliases ->
  List.iter
    (fun (s, c) ->
      let c = Signature.Public_key_hash.to_b58check c in
      add s c ;
      add ("pkh:" ^ s) c)
    pkh_aliases ;
  Client_proto_contracts.ContractAlias.load cctxt
  >>= ok_or_nil
  >>= fun contract_aliases ->
  List.iter
    (fun (s, c) ->
      let c = Contract.to_b58check c in
      add s c ;
      add ("contract:" ^ s) c)
    contract_aliases ;
  Client_keys.Public_key.load cctxt
  >>= ok_or_nil
  >>= fun pk_aliases ->
  List.iter
    (fun (s, c) ->
      match c with
      | (_, Some c) ->
          let c = Signature.Public_key.to_b58check c in
          add ("key:" ^ s) c
      | _ ->
          ())
    pk_aliases ;
  let b = Buffer.create (String.length src) in
  let subst = ref [] in
  Buffer.add_substitute
    b
    (fun s ->
      match StringMap.find s !aliases with
      | exception Not_found ->
          Printf.sprintf "${%s}" s
      | s' ->
          subst := Printf.sprintf "%s -> %s" s s' :: !subst ;
          s')
    src ;
  if !subst <> [] then
    cctxt#message "Substitutions:\n  %s" (String.concat "\n  " !subst)
    >>= fun () -> Lwt.return (Buffer.contents b)
  else Lwt.return src

let parse_toplevel ?check src =
  match get_header src with
  | (Some "mic", _) | (None, _) ->
      let (v, errors) = Michelson_v1_parser.parse_toplevel ?check src in
      ( {
          source = v.source;
          expanded = Script.Michelson_expr v.expanded;
          original = Michelson_parsed v;
        },
        errors )
  | (Some "love", _) ->
      let code = Environment.Love_lexer.parse_top_contract src in
      let code = Script.Dune_code (Love_repr.code (Contract code)) in
      ({source = src; expanded = code; original = Dune_parsed}, [])
  | (Some "love-json", i) -> (
      let json_src =
        Data_encoding.Json.from_string
          (String.sub src i (String.length src - i))
      in
      match json_src with
      | Ok json_src ->
          let c =
            Data_encoding.Json.destruct
              Love_json_encoding.Ast.top_contract_encoding
              json_src
          in
          let code = Script.Dune_code (Love_repr.code (Contract c)) in
          ({source = src; expanded = code; original = Dune_parsed}, [])
      | _ ->
          (make_dummy src, [Invalid_json "love"]) )
  | (Some l, _) ->
      (make_dummy src, [Unsupported_language l])

let shorten maxlen src =
  let len = String.length src in
  if len > maxlen then String.sub src 0 (maxlen - 3) ^ "..." else src

let parse_toplevel ?check src =
  match parse_toplevel ?check src with
  | (s, []) ->
      (s, [])
  | (s, errors) ->
      (s, Parsing_error (shorten 100 src) :: errors)

let parse_expression ?check src =
  match get_header src with
  | (Some "mic", _) | (None, _) ->
      let (v, errors) = Michelson_v1_parser.parse_expression ?check src in
      ( {
          source = v.source;
          expanded = Script.Michelson_expr v.expanded;
          original = Michelson_parsed v;
        },
        errors )
  | (Some "love", _) ->
      let value = Environment.Love_lexer.parse_top_value src in
      let value =
        Script.Dune_expr
          (Love_repr.const
             ( match value with
             | VALUE v ->
                 Value (Love_value.value_of_value v)
             | TYPE t ->
                 Type t ))
      in
      ({source = src; expanded = value; original = Dune_parsed}, [])
  | (Some "love-json", i) -> (
      let json_src =
        Data_encoding.Json.from_string
          (String.sub src i (String.length src - i))
      in
      match json_src with
      | Ok json_src ->
          let v =
            Data_encoding.Json.destruct
              Love_json_encoding.Value.encoding
              json_src
          in
          let value = Script.Dune_expr (Love_repr.const (Value v)) in
          ({source = src; expanded = value; original = Dune_parsed}, [])
      | _ ->
          (make_dummy src, [Invalid_json "love"]) )
  | (Some l, _) ->
      (make_dummy src, [Unsupported_language l])
