(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Dune_lang_v1_parser

let print_expr fmt expr =
  match expr with
  | Script.Michelson_expr expr ->
      Michelson_v1_emacs.print_expr fmt expr
  | _ ->
      assert false

(* TODO Dune prio 10 *)

let print_type_map fmt (parsed, type_map) =
  match parsed.original with
  | Michelson_parsed parsed ->
      Michelson_v1_emacs.print_type_map fmt (parsed, type_map)
  | _ ->
      assert false

(* TODO Dune prio 10 *)

let report_errors fmt (parsed, errors) =
  match parsed.original with
  | Michelson_parsed parsed ->
      Michelson_v1_emacs.report_errors fmt (parsed, errors)
  | _ ->
      assert false

(* TODO Dune prio 10 *)
