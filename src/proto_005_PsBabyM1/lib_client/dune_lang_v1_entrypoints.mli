(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol

(** Returns [Some type] if the contract has an entrypoint of type [type]. None if it does not exists.  *)
val script_entrypoint_type :
  #Protocol_client_context.rpc_context ->
  chain:Chain_services.chain ->
  block:Block_services.block ->
  Alpha_context.Script.expr ->
  entrypoint:string ->
  Alpha_context.Script.expr option tzresult Lwt.t

(** Returns [Some type] if the script has an entrypoint of type [type]. None if it does not exists.  *)
val contract_entrypoint_type :
  #Protocol_client_context.rpc_context ->
  chain:Chain_services.chain ->
  block:Block_services.block ->
  contract:Alpha_context.Contract.t ->
  entrypoint:string ->
  Alpha_context.Script.expr option tzresult Lwt.t

val print_entrypoint_type :
  #Client_context.printer ->
  ?on_errors:(error list -> unit tzresult Lwt.t) ->
  emacs:bool ->
  ?contract:Alpha_context.Contract.t ->
  ?script_name:string ->
  entrypoint:string ->
  Alpha_context.Script.expr option tzresult ->
  unit tzresult Lwt.t

(* Dune: We have merged list_unreachables and print_unreachables to
   avoid the exposition of an intermediate type that would not make
   any sense in other languages than Michelson *)
val print_unreachables :
  #Protocol_client_context.full ->
  chain:Chain_services.chain ->
  block:Block_services.block ->
  ?on_errors:(error list -> unit tzresult Lwt.t) ->
  emacs:bool ->
  ?contract:Alpha_context.Contract.t ->
  ?script_name:string ->
  Alpha_context.Script.expr ->
  unit tzresult Lwt.t

(** List the contract entrypoints with their types.
    If their is no explicit default, th type of default entrypoint will still be given.
*)
val list_contract_entrypoints :
  #Protocol_client_context.rpc_context ->
  chain:Chain_services.chain ->
  block:Block_services.block ->
  contract:Alpha_context.Contract.t ->
  (string * Alpha_context.Script.expr) list tzresult Lwt.t

(** List the script entrypoints with their types.  *)
val list_entrypoints :
  #Protocol_client_context.rpc_context ->
  chain:Chain_services.chain ->
  block:Block_services.block ->
  Alpha_context.Script.expr ->
  (string * Alpha_context.Script.expr) list tzresult Lwt.t

(** Print the contract entrypoints with their types.  *)
val print_entrypoints_list :
  #Client_context.printer ->
  ?on_errors:(error list -> unit tzresult Lwt.t) ->
  emacs:bool ->
  ?contract:Alpha_context.Contract.t ->
  ?script_name:string ->
  (string * Alpha_context.Script.expr) list tzresult ->
  unit tzresult Lwt.t
