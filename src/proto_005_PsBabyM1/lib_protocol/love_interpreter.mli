(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_ast_types

val typecheck_code :
  Alpha_context.t ->
  AST.top_contract ->
  Alpha_context.t tzresult Lwt.t

val typecheck_data :
  Alpha_context.t ->
  Love_value.ValueSet.elt ->
  TYPE.t ->
  Alpha_context.t tzresult Lwt.t

val typecheck :
  Alpha_context.t ->
  source:Contract_repr.t ->
  payer:Contract_repr.t ->
  self:Contract_repr.t ->
  amount:Tez_repr.tez ->
  code:AST.top_contract ->
  storage:Love_value.Value.t ->
  (Alpha_context.t *
   Love_value.LiveContract.t *
   Love_value.Value.t) tzresult Lwt.t

val preprocess_already_typechecked :
  Alpha_context.t ->
  source:Contract_repr.t ->
  payer:Contract_repr.t ->
  self:Contract_repr.t ->
  amount:Tez_repr.tez ->
  code:Love_value.LiveContract.t ->
  storage:Love_value.Value.t ->
  (Alpha_context.t *
   Love_value.LiveContract.t *
   Love_value.Value.t) tzresult Lwt.t

val execute :
  Alpha_context.t ->
  source:Contract_repr.t ->
  payer:Contract_repr.t ->
  self:Contract_repr.t ->
  amount:Tez_repr.tez ->
  entrypoint:string ->
  code:Love_value.LiveContract.t ->
  storage:Love_value.Value.t ->
  parameter:Love_value.Value.t ->
  (Alpha_context.t *
   (Love_value.Value.t *
    Love_value.Op.internal_operation list *
    Love_value.Op.big_map_diff option *
    Love_value.Value.t)) tzresult Lwt.t

val execute_fee_script :
  Alpha_context.t ->
  source:Contract_repr.t ->
  payer:Contract_repr.t ->
  self:Contract_repr.t ->
  amount:Tez_repr.tez ->
  entrypoint:string ->
  fee_code:Love_value.FeeCode.t ->
  storage:Love_value.Value.t ->
  parameter:Love_value.Value.t ->
  (Alpha_context.t * (Tez_repr.tez * Z.t)) tzresult Lwt.t

val storage_big_map_diff :
  Alpha_context.t ->
  Love_value.Value.t ->
  (Alpha_context.t *
   Love_value.Value.t *
   Love_value.Op.big_map_diff option) tzresult Lwt.t

val execute_value :
  Alpha_context.t ->
  source:Contract_repr.t ->
  payer:Contract_repr.t ->
  self:Contract_repr.t ->
  val_name:string ->
  code:Love_value.LiveContract.t ->
  storage:Love_value.Value.t ->
  parameter:Love_value.Value.t option ->
  (Alpha_context.t * Love_value.Value.t * TYPE.t) tzresult Lwt.t

val get_entrypoint:
  Alpha_context.t ->
  code:Love_value.LiveContract.t ->
  entrypoint:string ->
  (Alpha_context.t * TYPE.t option) tzresult Lwt.t

val list_entrypoints:
  Alpha_context.t ->
  code:Love_value.LiveContract.t ->
  (Alpha_context.t * (string * TYPE.t) list) tzresult Lwt.t
