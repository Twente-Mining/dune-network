(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions
open Utils
open Love_runtime_ast
open Signature
open Love_ast_types

module rec Value : sig

  type ('a, 'b) local_or_global =
    | Local of 'a
    | Global of 'b

  type 'a ptr_or_inlined =
    | Pointer of string Ident.t
    | Inlined of string Ident.t * 'a

  type val_contents =
    (Value.t, Value.t Value.ptr_or_inlined) Value.local_or_global

  type env = {
    values: (string Ident.t * val_contents) list;
    structs: (string Ident.t * LiveStructure.t ptr_or_inlined) list;
    sigs: (string Ident.t * TYPE.structure_sig ptr_or_inlined) list;
    exns: (string Ident.t * string Ident.t) list;
    types: (string Ident.t * string Ident.t) list;
    tvars: (string * TYPE.t) list;
  }

  type closure = {
    call_env: env;
    lambda: lambda;
  }

  type bigmap = {
    id: Z.t option;
    diff: Value.t option ValueMap.t;
    key_type: TYPE.t;
    value_type: TYPE.t;
  }

  type t =
    (* Base *)
    | VUnit
    | VBool of bool
    | VString of string
    | VBytes of MBytes.t
    | VInt of Z.t
    | VNat of Z.t

    (* Composite *)
    | VTuple of t list
    | VConstr of string * t list
    | VRecord of (string * t) list

    (* Collections *)
    | VList of t list
    | VSet of ValueSet.t
    | VMap of t ValueMap.t
    | VBigMap of bigmap

    (* Domain-specific *)
    | VDun of Tez_repr.t
    | VKey of Public_key.t
    | VKeyHash of Public_key_hash.t
    | VSignature of Signature.t
    | VTimestamp of Script_timestamp_repr.t
    | VAddress of Contract_repr.t
    | VOperation of Op.t

    (* Structures and entry points *)
    | VPackedStructure of (string Ident.t * LiveContract.t)
    | VContractInstance of TYPE.structure_sig * Contract_repr.t
    | VEntryPoint of Contract_repr.t * string
    | VView of Contract_repr.t * string

    (* Closures *)
    | VClosure of closure

    (* Love primitive
       VPrimitive (f, [aN;...;a2;a1]) = f a1 a2 ... aN *)
    | VPrimitive of (Love_primitive.t *
                     TYPE.extended_arg * val_or_type list)

  and val_or_type =
    | V of t
    | T of TYPE.t

  val of_const: const -> t

  val none : unit -> t
  val some : t -> t

  val unrec_closure: t -> t
  val rec_closure: t -> t

  val compare: t -> t -> int
  val equal: t -> t -> bool

  val raw_compare: t -> t -> int

end = struct

  type ('a, 'b) local_or_global =
    | Local of 'a
    | Global of 'b

  type 'a ptr_or_inlined =
    | Pointer of string Ident.t
    | Inlined of string Ident.t * 'a

  type val_contents =
    (Value.t, Value.t Value.ptr_or_inlined) Value.local_or_global

  type env = {
    values: (string Ident.t * val_contents) list;
    structs: (string Ident.t * LiveStructure.t ptr_or_inlined) list;
    sigs: (string Ident.t * TYPE.structure_sig ptr_or_inlined) list;
    exns: (string Ident.t * string Ident.t) list;
    types: (string Ident.t * string Ident.t) list;
    tvars: (string * TYPE.t) list;
  }

  type closure = {
    call_env: env;
    lambda: lambda;
  }

  type bigmap = {
    id: Z.t option;
    diff: Value.t option ValueMap.t;
    key_type: TYPE.t;
    value_type: TYPE.t;
  }

  type t =
    (* Base *)
    | VUnit
    | VBool of bool
    | VString of String.t
    | VBytes of MBytes.t
    | VInt of Z.t
    | VNat of Z.t

    (* Composite *)
    | VTuple of t list
    | VConstr of string * t list
    | VRecord of (string * t) list

    (* Collections *)
    | VList of t list
    | VSet of ValueSet.t
    | VMap of t ValueMap.t
    | VBigMap of bigmap

    (* Domain-specific *)
    | VDun of Tez_repr.t
    | VKey of Public_key.t
    | VKeyHash of Public_key_hash.t
    | VSignature of Signature.t
    | VTimestamp of Script_timestamp_repr.t
    | VAddress of Contract_repr.t
    | VOperation of Op.t

    (* Structures and entry points *)
    | VPackedStructure of (string Ident.t * LiveContract.t)
    | VContractInstance of TYPE.structure_sig * Contract_repr.t
    | VEntryPoint of Contract_repr.t * string
    | VView of Contract_repr.t * string

    (* Closures *)
    | VClosure of closure

    (* Love primitive
       VPrimitive (f, [aN;...;a2;a1]) = f a1 a2 ... aN *)
    | VPrimitive of (Love_primitive.t *
                     TYPE.extended_arg * val_or_type list)

  and val_or_type =
    | V of t
    | T of TYPE.t

  let of_const = function
    | RCUnit -> VUnit
    | RCBool b -> VBool b
    | RCString s -> VString s
    | RCBytes b -> VBytes b
    | RCInt i -> VInt i
    | RCNat i -> VNat i
    | RCDun d -> VDun d
    | RCKey k -> VKey k
    | RCKeyHash kh -> VKeyHash kh
    | RCSignature s -> VSignature s
    | RCTimestamp t -> VTimestamp t
    | RCAddress a -> VAddress a

  let none () = VConstr ("None", [])
  let some v  = VConstr ("Some", [v])

  let unrec_closure v =
    match v with
    | VClosure { call_env; lambda } ->
        let is_rec = ref false in
        let values = List.map (fun id_vc ->
            match id_vc with
            | id, Local v' ->
                if v == v' then
                  begin
                    is_rec := true;
                    (Ident.create_id "@self",
                     Local (VList (List.map (fun s ->
                         VString s) (Ident.get_list id))))
                  end
                else id_vc
            | _ -> id_vc
          ) call_env.values
        in
        if !is_rec then
          VClosure { call_env = { call_env with values }; lambda }
        else v
    | _ -> v

  let rec_closure v =
    match v with
    | VClosure { call_env; lambda } ->
        let values, self = List.partition (fun (id, _c) ->
            not (String.equal (Ident.get_final id) ("@self"))
          ) call_env.values in
        begin match self with
          | [] -> v
          | [_, Local (VList id)] ->
              let id = Ident.put_list (List.map (function
                  | VString s -> s
                  | _ -> raise (InvariantBroken "Bad recusrive closure")
                ) id)
              in
              let rec c =
                VClosure { call_env =
                             { call_env with
                               values = (id, Local c) :: values };
                           lambda }
              in
              c
          | _ -> raise (InvariantBroken "Bad recursive function")
        end
    | _ -> v

  let rec compare v1 v2 =
    let compare_value_list l1 l2 = compare_list compare l1 l2 in
    let compare_field_list fl1 fl2 =
      let compare_field f1 f2 =
        if String.equal f1 f2 then 0 else raise Uncomparable in
      let fl1 = sort_assoc_list_by_key String.compare fl1 in
      let fl2 = sort_assoc_list_by_key String.compare fl2 in
      compare_list (compare_pair compare_field compare) fl1 fl2
    in
    match v1, v2 with
    | VUnit, VUnit -> 0
    | VBool b1, VBool b2 -> Compare.Bool.compare b1 b2
    | VString s1, VString s2 -> String.compare s1 s2
    | VBytes s1, VBytes s2 -> MBytes.compare s1 s2
    | VInt i1, VInt i2
    | VNat i1, VNat i2 -> Z.compare i1 i2
    | VTuple vl1, VTuple vl2 -> compare_value_list vl1 vl2
    | VConstr (c1, vl1), VConstr (c2, vl2) ->
        compare_pair String.compare compare_value_list (c1, vl1) (c2, vl2)
    | VRecord fvl1, VRecord fvl2 -> compare_field_list fvl1 fvl2
    | VList vl1, VList vl2 -> compare_value_list vl1 vl2
    | VSet vs1, VSet vs2 -> ValueSet.compare vs1 vs2
    | VMap vm1, VMap vm2 -> ValueMap.compare compare vm1 vm2
    | VDun d1, VDun d2 -> Tez_repr.compare d1 d2
    | VKey k1, VKey k2 -> Public_key.compare k1 k2
    | VKeyHash kh1, VKeyHash kh2 -> Public_key_hash.compare kh1 kh2
    | VSignature sig1, VSignature sig2 -> Signature.compare sig1 sig2
    | VTimestamp ts1, VTimestamp ts2 -> Script_timestamp_repr.compare ts1 ts2
    | VAddress a1, VAddress a2 -> Contract_repr.compare a1 a2
    | VContractInstance (_, a1), VContractInstance (_, a2) ->
        Contract_repr.compare a1 a2
    | VEntryPoint (a1, n1), VEntryPoint (a2, n2)
    | VView (a1, n1), VView (a2, n2) ->
        compare_pair Contract_repr.compare String.compare (a1, n1) (a2, n2)
    | VBigMap _, VBigMap _ -> raise Uncomparable
    | VPackedStructure _, VPackedStructure _ -> raise Uncomparable
    | VOperation _, VOperation _ -> raise Uncomparable
    | VClosure _, VClosure _ -> raise Uncomparable
    | VPrimitive _, VPrimitive _ -> raise Uncomparable

    (* Exhaustiveness *)
    | (VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _ |
       VTuple _ | VConstr _ | VRecord _ | VList _ |
       VSet _ | VMap _ | VBigMap _ | VDun _ | VKey _ | VKeyHash _ |
       VSignature _ | VTimestamp _ | VAddress _ | VOperation _ |
       VContractInstance _ | VPackedStructure _ | VEntryPoint _ | VView _ |
       VClosure _ | VPrimitive _),
      (VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _ |
       VTuple _ | VConstr _ | VRecord _ | VList _ |
       VSet _ | VMap _ | VBigMap _ | VDun _ | VKey _ | VKeyHash _ |
       VSignature _ | VTimestamp _ | VAddress _ | VOperation _ |
       VContractInstance _ | VPackedStructure _ | VEntryPoint _ | VView _ |
       VClosure _ | VPrimitive _) ->
        raise Uncomparable

  let equal v1 v2 =
    Compare.Int.(Value.compare v1 v2 = 0)

  let rec raw_compare v1 v2 =
    let compare_value_list l1 l2 = compare_list raw_compare l1 l2 in
    let compare_field_list fl1 fl2 =
      let compare_field f1 f2 =
        if String.equal f1 f2 then 0 else raise Uncomparable
      in
      let fl1 = sort_assoc_list_by_key String.compare fl1 in
      let fl2 = sort_assoc_list_by_key String.compare fl2 in
      compare_list (compare_pair compare_field compare) fl1 fl2
    in
    match unrec_closure v1, unrec_closure v2 with
    | VUnit, VUnit -> 0
    | VBool b1, VBool b2 -> Compare.Bool.compare b1 b2
    | VString s1, VString s2 -> String.compare s1 s2
    | VBytes s1, VBytes s2 -> MBytes.compare s1 s2
    | VInt i1, VInt i2
    | VNat i1, VNat i2 -> Z.compare i1 i2
    | VTuple vl1, VTuple vl2 -> compare_value_list vl1 vl2
    | VConstr (c1, vl1), VConstr (c2, vl2) ->
        compare_pair String.compare compare_value_list (c1, vl1) (c2, vl2)
    | VRecord fvl1, VRecord fvl2 -> compare_field_list fvl1 fvl2
    | VList vl1, VList vl2 -> compare_value_list vl1 vl2
    | VSet vs1, VSet vs2 ->
        compare_value_list (ValueSet.elements vs1) (ValueSet.elements vs2)
    | VMap vm1, VMap vm2 ->
        compare_list (compare_pair raw_compare raw_compare)
          (ValueMap.bindings vm1) (ValueMap.bindings vm2)
    | VDun d1, VDun d2 -> Tez_repr.compare d1 d2
    | VKey k1, VKey k2 -> Public_key.compare k1 k2
    | VKeyHash kh1, VKeyHash kh2 -> Public_key_hash.compare kh1 kh2
    | VSignature sig1, VSignature sig2 -> Signature.compare sig1 sig2
    | VTimestamp ts1, VTimestamp ts2 -> Script_timestamp_repr.compare ts1 ts2
    | VAddress a1, VAddress a2 -> Contract_repr.compare a1 a2
    | VContractInstance (_, a1), VContractInstance (_, a2) ->
        Contract_repr.compare a1 a2
    | VEntryPoint (a1, n1), VEntryPoint (a2, n2)
    | VView (a1, n1), VView (a2, n2) ->
        compare_pair Contract_repr.compare String.compare (a1, n1) (a2, n2)
    | VBigMap bm1, VBigMap bm2 ->
        compare_tuple4 (compare_option Z.compare)
          (compare_list (compare_pair raw_compare (compare_option raw_compare)))
          Love_type.compare Love_type.compare
          (bm1.id, ValueMap.bindings bm1.diff, bm1.key_type, bm1.value_type)
          (bm2.id, ValueMap.bindings bm2.diff, bm2.key_type, bm2.value_type)
    | VPackedStructure (id1, ct1), VPackedStructure (id2, ct2) ->
        compare_pair (Ident.compare String.compare)
          LiveContract.raw_compare (id1, ct1) (id2, ct2)
    | VOperation op1, VOperation op2 -> Op.raw_compare op1 op2
    | VClosure c1, VClosure c2 ->
        compare_pair raw_compare_env Love_runtime_ast.compare_lambda
          (c1.call_env, c1.lambda) (c2.call_env, c2.lambda)
    | VPrimitive (p1, xa1, al1), VPrimitive (p2, xa2, al2) ->
        compare_tuple3 Love_primitive.compare
          Love_primitive.compare_extended_arg
          (compare_list raw_compare_val_or_type) (p1, xa1, al1) (p2, xa2, al2)

    (* Exhaustiveness *)
    | VUnit, _ -> -1               | _, VUnit -> 1
    | VBool _, _ -> -1             | _, VBool _ -> 1
    | VString _, _ -> -1           | _, VString _ -> 1
    | VBytes _, _ -> -1            | _, VBytes _ -> 1
    | VInt _, _ -> -1              | _, VInt _ -> 1
    | VNat _, _ -> -1              | _, VNat _ -> 1
    | VTuple _, _ -> -1            | _, VTuple _ -> 1
    | VConstr _, _ -> -1           | _, VConstr _ -> 1
    | VRecord _, _ -> -1           | _, VRecord _ -> 1
    | VList _, _ -> -1             | _, VList _ -> 1
    | VSet _, _ -> -1              | _, VSet _ -> 1
    | VMap _, _ -> -1              | _, VMap _ -> 1
    | VBigMap _, _ -> -1           | _, VBigMap _ -> 1
    | VDun _, _ -> -1              | _, VDun _ -> 1
    | VKey _, _ -> -1              | _, VKey _ -> 1
    | VKeyHash _, _ -> -1          | _, VKeyHash _ -> 1
    | VSignature _, _ -> -1        | _, VSignature _ -> 1
    | VTimestamp _, _ -> -1        | _, VTimestamp _ -> 1
    | VAddress _, _ -> -1          | _, VAddress _ -> 1
    | VOperation _, _ -> -1        | _, VOperation _ -> 1
    | VContractInstance _, _ -> -1 | _, VContractInstance _ -> 1
    | VPackedStructure _, _ -> -1  | _, VPackedStructure _ -> 1
    | VEntryPoint _, _ -> -1       | _, VEntryPoint _ -> 1
    | VView _, _ -> -1             | _, VView _ -> 1
    | VClosure _, _ -> -1          | _, VClosure _ -> 1
  (* | VPrimitive _, _ -> -1        | _, VPrimitive _ -> 1 *)

  and raw_compare_val_or_type vt1 vt2 =
    match vt1, vt2 with
    | V v1, V v2 -> raw_compare v1 v2
    | T t1, T t2 -> Love_type.compare t1 t2
    | V _, _ -> -1 | _, V _ -> 1
  (* | T _, _ -> -1 | _, T _ -> 1 *)

  and raw_compare_env e1 e2 =
    let compare_strident = Ident.compare String.compare in
    let compare_assoc_list compfun l1 l2 =
      compare_list (compare_pair compare_strident compfun) l1 l2 in
    compare_tuple6
      (compare_assoc_list (compare_local_or_global raw_compare
                             (compare_ptr_or_inlined raw_compare)))
      (compare_assoc_list (compare_ptr_or_inlined LiveStructure.raw_compare))
      (compare_assoc_list (compare_ptr_or_inlined Love_type.compare_sig_kind))
      (compare_assoc_list compare_strident)
      (compare_assoc_list compare_strident)
      (compare_list (compare_pair String.compare Love_type.compare))
      (e1.values, e1.structs, e1.sigs, e1.exns, e1.types, e1.tvars)
      (e2.values, e2.structs, e2.sigs, e2.exns, e2.types, e2.tvars)

  and compare_local_or_global compfunloc compfunglob lg1 lg2 =
    match lg1, lg2 with
    | Local l1, Local l2 -> compfunloc l1 l2
    | Global g1, Global g2 -> compfunglob g1 g2
    | Local _, _ -> -1 | _, Local _ -> 1
  (* | Global _, _ -> -1 | _, Global _ -> 1 *)

  and compare_ptr_or_inlined :
    type a. (a -> a -> int) -> a ptr_or_inlined -> a ptr_or_inlined -> int =
    fun compfun pi1 pi2 ->
      match pi1, pi2 with
      | Pointer p1, Pointer p2 -> Ident.compare String.compare p1 p2
      | Inlined (id1, x1), Inlined (id2, x2) ->
          compare_pair (Ident.compare String.compare)
            compfun (id1, x1) (id2, x2)
      | Pointer _, _ -> -1 | _, Pointer _ -> 1
      (* | Inlined _, _ -> -1 | _, Inlined _ -> 1 *)

end

and ValueSet : S.SET with type elt = Value.t = Set.Make (Value)

and ValueMap : S.MAP with type key = Value.t = Map.Make (Value)

and Op : sig
  type manager_operation =
    | Origination of {
        delegate: Public_key_hash.t option;
        script: (Value.t * LiveContract.t); (* Initial storage + contract *)
        credit: Tez_repr.t;
        preorigination: Contract_repr.t option;
      }
    | Transaction of {
        amount: Tez_repr.t; (* The amount of the transaction *)
        parameters: Value.t option; (* An optional parameter *)
        entrypoint: string;
        destination: Contract_repr.t; (* The transaction receiver. *)
        (* collect call *)
      }
    | Delegation of Public_key_hash.t option
    | Dune_manage_account of
        { target : Public_key_hash.t option;
          maxrolls : int option option;
          admin : Contract_repr.t option option;
          white_list : Contract_repr.t list option;
          delegation : bool option }

  type internal_operation = {
    source: Contract_repr.t;
    operation: manager_operation;
    nonce: int;
  }

  type big_map_diff_item =
    | Update of {
        big_map : Z.t;
        diff_key : Value.t;
        diff_key_hash : Script_expr_hash.t;
        diff_value : Value.t option;
      }
    | Clear of Z.t
    | Copy of Z.t * Z.t
    | Alloc of {
        big_map : Z.t;
        key_type : TYPE.t;
        value_type : TYPE.t;
      }

  type big_map_diff = big_map_diff_item list

  type t = internal_operation * big_map_diff option

  val raw_compare : t -> t -> int

end = struct
  type manager_operation =
    | Origination of {
        delegate: Public_key_hash.t option;
        script: (Value.t * LiveContract.t); (* Initial storage + contract *)
        credit: Tez_repr.t;
        preorigination: Contract_repr.t option;
      }
    | Transaction of {
        amount: Tez_repr.t; (* The amount of the transaction *)
        parameters: Value.t option; (* An optional parameter *)
        entrypoint: string;
        destination: Contract_repr.t; (* The transaction receiver. *)
        (* collect call *)
      }
    | Delegation of Public_key_hash.t option
    | Dune_manage_account of
        { target : Public_key_hash.t option;
          maxrolls : int option option;
          admin : Contract_repr.t option option;
          white_list : Contract_repr.t list option;
          delegation : bool option }

  type internal_operation = {
    source: Contract_repr.t;
    operation: manager_operation;
    nonce: int;
  }

  type big_map_diff_item =
    | Update of {
        big_map : Z.t;
        diff_key : Value.t;
        diff_key_hash : Script_expr_hash.t;
        diff_value : Value.t option;
      }
    | Clear of Z.t
    | Copy of Z.t * Z.t
    | Alloc of {
        big_map : Z.t;
        key_type : TYPE.t;
        value_type : TYPE.t;
      }

  type big_map_diff = big_map_diff_item list

  type t = internal_operation * big_map_diff option

  let raw_compare_manager_operation op1 op2 =
    match op1, op2 with
    | Origination { delegate = d1; script = s1;
                    credit = c1; preorigination = p1 },
      Origination { delegate = d2; script = s2;
                    credit = c2; preorigination = p2 } ->
        compare_tuple4 (compare_option Public_key_hash.compare)
          (compare_pair Value.raw_compare LiveContract.raw_compare)
          Tez_repr.compare (compare_option Contract_repr.compare)
          (d1, s1, c1, p1) (d2, s2, c2, p2)
    | Transaction { amount = a1; parameters = p1;
                    entrypoint = e1; destination = d1 },
      Transaction { amount = a2; parameters = p2;
                    entrypoint = e2; destination = d2 } ->
        compare_tuple4 Tez_repr.compare (compare_option Value.raw_compare)
          String.compare Contract_repr.compare
          (a1, p1, e1, d1) (a2, p2, e2, d2)
    | Delegation d1, Delegation d2 ->
        compare_option Public_key_hash.compare d1 d2
    | Dune_manage_account { target = t1; maxrolls = m1; admin = a1;
                            white_list = w1; delegation = d1 },
      Dune_manage_account { target = t2; maxrolls = m2; admin = a2;
                            white_list = w2; delegation = d2 } ->
        compare_tuple5 (compare_option Public_key_hash.compare)
          (compare_option (compare_option Compare.Int.compare))
          (compare_option (compare_option Contract_repr.compare))
          (compare_option (compare_list Contract_repr.compare))
          (compare_option Compare.Bool.compare)
          (t1, m1, a1, w1, d1) (t2, m2, a2, w2, d2)
    | Origination _, _ -> -1         | _, Origination _ -> 1
    | Transaction _, _ -> -1         | _, Transaction _ -> 1
    | Delegation _, _ -> -1          | _, Delegation _ -> 1
  (* | Dune_manage_account _, _ -> -1 | _, Dune_manage_account _ -> 1 *)

  let raw_compare_internal_operation iop1 iop2 =
    compare_tuple3 Contract_repr.compare raw_compare_manager_operation
      Compare.Int.compare (iop1.source, iop1.operation, iop1.nonce)
      (iop2.source, iop2.operation, iop2.nonce)

  let raw_compare_big_map_diff_item bmdi1 bmdi2 =
    match bmdi1, bmdi2 with
    | Update { big_map = m1; diff_key = k1;
               diff_key_hash = h1; diff_value = v1 },
      Update { big_map = m2; diff_key = k2;
               diff_key_hash = h2; diff_value = v2 } ->
        compare_tuple4 Z.compare Value.raw_compare
          Script_expr_hash.compare (compare_option Value.raw_compare)
          (m1, k1, h1, v1) (m2, k2, h2, v2)
    | Clear m1, Clear m2 ->
        Z.compare m1 m2
    | Copy (ms1, md1), Copy (ms2, md2) ->
        compare_pair Z.compare Z.compare (ms1, md1) (ms2, md2)
    | Alloc { big_map = m1; key_type = kt1; value_type = vt1 },
      Alloc { big_map = m2; key_type = kt2; value_type = vt2 } ->
        compare_tuple3 Z.compare Love_type.compare
          Love_type.compare (m1, kt1, vt1) (m2, kt2, vt2)
    | Update _, _ -> -1 | _, Update _ -> 1
    | Clear _, _ -> -1  | _, Clear _ -> 1
    | Copy _, _ -> -1   | _, Copy _ -> 1
  (* | Alloc _, _ -> -1  | _, Alloc _ -> 1 *)

  let raw_compare op1 op2 =
    compare_pair raw_compare_internal_operation
      (compare_option (compare_list raw_compare_big_map_diff_item)) op1 op2

end

and LiveStructure : sig
  type init = {
    vinit_code: Value.t;
    vinit_typ: TYPE.t;
    vinit_persist: bool;
  }

  type entry = {
    ventry_code: Value.t;
    ventry_fee_code: Value.t option;
    ventry_typ: TYPE.t;
  }

  type view = {
    vview_code: Value.t;
    vview_typ: TYPE.t;
    vview_recursive: TYPE.recursive;
  }

  type value = {
    vvalue_code: Value.t;
    vvalue_typ: TYPE.t;
    vvalue_visibility: visibility;
    vvalue_recursive: TYPE.recursive;
  }

  type content =
    | VType of Love_runtime_ast.type_kind * TYPE.typedef
    | VException of TYPE.t list
    | VInit of init
    | VEntry of entry
    | VView of view
    | VValue of value
    | VStructure of LiveStructure.t
    | VSignature of TYPE.structure_sig

  and t = {
    kind : TYPE.struct_kind;
    content : (string * content) list;
  }

  val is_module : t -> bool

  val unit_void : t

  val unit_default : unit -> t

  val resolve_id_in_struct : LiveStructure.t -> string Ident.t -> content option

  val raw_compare : t -> t -> int

end = struct
  type init = {
    vinit_code: Value.t;
    vinit_typ: TYPE.t;
    vinit_persist: bool;
  }

  type entry = {
    ventry_code: Value.t;
    ventry_fee_code: Value.t option;
    ventry_typ: TYPE.t;
  }

  type view = {
    vview_code: Value.t;
    vview_typ: TYPE.t;
    vview_recursive: TYPE.recursive;
  }

  type value = {
    vvalue_code: Value.t;
    vvalue_typ: TYPE.t;
    vvalue_visibility: visibility;
    vvalue_recursive: TYPE.recursive;
  }

  type content =
    | VType of Love_runtime_ast.type_kind * TYPE.typedef
    | VException of TYPE.t list
    | VInit of init
    | VEntry of entry
    | VView of view
    | VValue of value
    | VStructure of LiveStructure.t
    | VSignature of TYPE.structure_sig

  and t = {
    kind : TYPE.struct_kind;
    content : (string * content) list;
  }

  let is_module c =
    match c.kind with
    | Module -> true
    | Contract _ -> false

  let unit_void = {
    content = [];
    kind = Contract []
  }

  let unit_default () =
    let default_entry =
      VEntry {
        ventry_code = VClosure {
            call_env = {
              values = []; structs = []; sigs = [];
              exns = []; types = []; tvars = [] };
            lambda = {
              args = [
                RPattern (RPAny, (Love_type_list.get_type "unit" []));
                RPattern (RPAny, (Love_type_list.get_type "dun" []));
                RPattern (RPAny, (Love_type_list.get_type "unit" []))
              ];
              body = RTuple [ RNil; RConst RCUnit ]
            }
          };
        ventry_fee_code = None;
        ventry_typ = Love_type_list.get_type "unit" []
      } in
    { unit_void with content = ["default", default_entry] }

  let resolve_id_in_struct (s : LiveStructure.t) (id : string Ident.t) =
    let open LiveStructure in
    List.fold_left (fun res n ->
        match res with
        | Some (VStructure s) -> List.assoc_opt n s.content
        | Some _ | None -> None
      ) (Some (VStructure s)) (Ident.get_list id)

  let rec raw_compare_content c1 c2 =
    match c1, c2 with
    | VType (tk1, td1), VType (tk2, td2) ->
        compare_pair Love_ast.compare_type_kind
          Love_type.compare_typedef (tk1, td1) (tk2, td2)
    | VException tl1, VException tl2 ->
        compare_list Love_type.compare tl1 tl2
    | VInit { vinit_code = v1; vinit_typ = t1; vinit_persist = b1 },
      VInit { vinit_code = v2; vinit_typ = t2; vinit_persist = b2 } ->
        compare_tuple3 Value.raw_compare Love_type.compare
          Compare.Bool.compare (v1, t1, b1) (v2, t2, b2)
    | VEntry { ventry_code = v1; ventry_fee_code = vo1; ventry_typ = t1 },
      VEntry { ventry_code = v2; ventry_fee_code = vo2; ventry_typ = t2 } ->
        compare_tuple3 Value.raw_compare (compare_option Value.raw_compare)
          Love_type.compare (v1, vo1, t1) (v2, vo2, t2)
    | VView { vview_code = v1; vview_typ = t1 },
      VView { vview_code = v2; vview_typ = t2 } ->
        compare_pair Value.raw_compare Love_type.compare (v1, t1) (v2, t2)
    | VValue { vvalue_code = v1; vvalue_typ = t1;
               vvalue_visibility = vs1; vvalue_recursive = vr1 },
      VValue { vvalue_code = v2; vvalue_typ = t2;
               vvalue_visibility = vs2; vvalue_recursive = vr2 } ->
        compare_tuple4 Value.raw_compare Love_type.compare
          Love_ast.compare_visibility Love_type.compare_rec
          (v1, t1, vs1, vr1) (v2, t2, vs2, vr2)
    | VStructure s1, VStructure s2 -> raw_compare s1 s2
    | VSignature s1, VSignature s2 -> Love_type.compare_sig_kind s1 s2
    | VType _, _ -> -1      | _, VType _ -> 1
    | VException _, _ -> -1 | _, VException _ -> 1
    | VInit _, _ -> -1      | _, VInit _ -> 1
    | VEntry _, _ -> -1     | _, VEntry _ -> 1
    | VView _, _ -> -1      | _, VView _ -> 1
    | VValue _, _ -> -1     | _, VValue _ -> 1
    | VStructure _, _ -> -1 | _, VStructure _ -> 1
  (* | VSignature _, _ -> -1 | _, VSignature _ -> 1 *)

  and raw_compare ls1 ls2 =
    compare_pair Love_type.compare_struct_kind
      (compare_list (compare_pair String.compare raw_compare_content))
      (ls1.kind, ls1.content) (ls2.kind, ls2.content)

end

and LiveContract : sig

  type t = {
    version : (int * int); (* (major, minor) *)
    root_struct : LiveStructure.t;
  }

  val unit_void : t

  val unit_default : unit -> t

  val raw_compare : t -> t -> int

end = struct

  type t = {
    version : (int * int); (* (major, minor) *)
    root_struct : LiveStructure.t;
  }

  let unit_void =
    { version = Options.version; root_struct = LiveStructure.unit_void }

  let unit_default () =
    { version = Options.version; root_struct = LiveStructure.unit_default () }

  let raw_compare c1 c2 =
    compare_tuple3 Compare.Int.compare
      Compare.Int.compare LiveStructure.raw_compare
      (fst c1.version, snd c1.version, c1.root_struct)
      (fst c2.version, snd c2.version, c2.root_struct)

end

and FeeCode : sig

  type t = {
    version : (int * int); (* (major, minor) *)
    root_struct : LiveStructure.t; (* Containing only actually used code/data *)
    fee_codes : (string * (Value.t * TYPE.t)) list
  }

end = FeeCode


let rec value_of_value v =
  let open Love_ast_types in
  match v with
    VALUE.VUnit -> Value.VUnit
  | VBool bool -> VBool bool
  | VString s -> VString s
  | VBytes s -> VBytes s
  | VInt z -> VInt z
  | VNat z -> VNat z

  (* Composite *)
  | VTuple list -> VTuple (List.map value_of_value list)
  | VConstr (s, list) -> VConstr (s, List.map value_of_value list)
  | VRecord list -> VRecord (List.map (
      fun (s, v) -> (s, value_of_value v) ) list)

  (* Collections *)
  | VList list -> VList (List.map value_of_value list)
  | VSet list ->
      VSet ( List.map value_of_value list |> ValueSet.of_list )
  | VMap list ->
      VMap (
        List.fold_left (fun acc (k,v) ->
            let k = value_of_value k in
            let v = value_of_value v in
            ValueMap.add k v acc)
          ValueMap.empty list )

  | VBigMap { diff ; key_type ; value_type } ->
      let diff = List.fold_left (fun acc (k,v) ->
            let k = value_of_value k in
            let v = value_of_value v in
            ValueMap.add k (Some v) acc)
          ValueMap.empty diff in
      VBigMap { id = None; diff ; key_type ; value_type }

  (* Domain-specific *)
  | VDun int64 -> VDun (Tez_repr.of_mutez_exn int64)
  | VKey pk -> VKey pk
  | VKeyHash pkh -> VKeyHash pkh
  | VSignature s -> VSignature s
  | VTimestamp z -> VTimestamp z
  | VAddress a ->
      match Contract_repr.of_b58check a with
      | Ok s -> VAddress s
      | _ -> raise (InvariantBroken ("bad contract address:" ^ a))
