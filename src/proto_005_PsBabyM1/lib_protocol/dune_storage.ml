(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Storage_functors (* For functors and Registered/Ghost *)

type error += Unsupported_revision of int * int
type error += Bad_revision_change of int * int

let () =
  register_error_kind
    `Permanent
    ~id:"protocol_revision.unsupported"
    ~title:"Unsupported protocol revision"
    ~description:"The protocol revision is not supported by this node."
    ~pp:(fun ppf (revision, max_revision) ->
        Format.fprintf ppf
          "This node does not support protocol %d (max is %d)"
          revision max_revision
      )
    Data_encoding.(obj2 (req "revision" int31) (req "max_revision" int31))
    (function Unsupported_revision (r, m) -> Some (r, m) | _ -> None)
    (fun (r, m) -> Unsupported_revision (r, m)) ;
  register_error_kind
    `Permanent
    ~id:"protocol_revision.illegal_change"
    ~title:"Bad protocol revision change"
    ~description:"The protocol revision cannot be changed to a lower value."
    ~pp:(fun ppf (revision, prev_revision) ->
        Format.fprintf ppf
          "Cannot set protocol revision to a lower value (%d, already at revision %d)"
          revision prev_revision
      )
    Data_encoding.(obj2 (req "new_revision" int31) (req "current_revision" int31))
    (function Bad_revision_change (r, p) -> Some (r, p) | _ -> None)
    (fun (r, p) -> Bad_revision_change (r, p)) ;
  ()

module Int32 = struct
  type t = Int32.t
  let encoding = Data_encoding.int32
end

(* Storage *)

module Activate_protocol = struct

  module Raw_context =
    Make_subcontext(Registered)(Raw_context)
      (struct let name = ["activate_protocol"] end)

  module Level =
    Storage_functors.Make_single_data_storage(Registered)(Raw_context)
      (struct let name = [ "level" ] end)
      (Int32)

  module Protocol =
    Storage_functors.Make_single_data_storage(Registered)(Raw_context)
      (struct let name = [ "protocol" ] end)
      (Protocol_hash)

  module Protocol_parameters =
    Storage_functors.Make_single_data_storage(Registered)(Raw_context)
      (struct let name = [ "protocol_parameters" ] end)
      (struct
        type t = Dune_parameters_repr.parameters
        let encoding = Dune_parameters_repr.parameters_encoding
      end)

end

(* Accessors *)

let get_activate_protocol_level = Activate_protocol.Level.get_option
let get_activate_protocol ctxt =
  Activate_protocol.Level.get_option ctxt >>=? function
  | None -> return_none
  | Some level ->
      Activate_protocol.Protocol.get_option ctxt >>=? fun protocol ->
      Activate_protocol.Protocol_parameters.get_option ctxt >>=?
      fun protocol_parameters ->
      return_some ( level, protocol, protocol_parameters )

let get_activate_protocol_and_cleanup ctxt =
  Activate_protocol.Level.remove ctxt >>= fun ctxt ->
  Activate_protocol.Protocol.get_option ctxt >>=? fun protocol ->
  Activate_protocol.Protocol.remove ctxt >>= fun ctxt ->
  Activate_protocol.Protocol_parameters.get_option ctxt >>=?
  fun protocol_parameters ->
  Activate_protocol.Protocol_parameters.remove ctxt >>= fun ctxt ->
  return ( ctxt, protocol, protocol_parameters )

let set_activate_protocol
    ctxt ?protocol ?protocol_parameters level =
  begin match protocol with
    | Some protocol ->
        Activate_protocol.Protocol.init_set ctxt protocol
    | None -> Activate_protocol.Protocol.remove ctxt
  end >>= fun ctxt ->
  begin match protocol_parameters with
    | None -> Activate_protocol.Protocol_parameters.remove ctxt
    | Some protocol_parameters ->
        Activate_protocol.Protocol_parameters.init_set ctxt protocol_parameters
  end >>= fun ctxt ->
  Activate_protocol.Level.init_set ctxt level

let genesis_pubkey =
  Signature.Public_key.of_b58check_exn Config.config.genesis_key

(* Deprecated: Foundation_pubkey is replaced by the superadmin flag *)
module Foundation_pubkey =
  Storage_functors.Make_single_data_storage
    (Registered)(Raw_context)
    (struct let name = [ "foundation_pubkey" ] end)
    (Signature.Public_key)

let get_foundation_pubkey t =
  Foundation_pubkey.get t >>= function
  | Ok x -> Lwt.return x
  | Error _ -> Lwt.return genesis_pubkey

let set_foundation_pubkey = Foundation_pubkey.init_set

let remove_foundation_pubkey = Foundation_pubkey.remove
(* End of deprecation zone *)

(* Management of peers_id: network permissioning *)

module String_index = struct
  type t = string
  let path_length = 1
  let to_path c l = c :: l
  let of_path = function
    | [] | _ :: _ :: _ -> None
    | [ c ] -> Some c
  let rpc_arg = RPC_arg.string
  let encoding = Data_encoding.string
  let compare = Compare.String.compare
end

let peers_hash_prefix = "\002\213\233" (* PEE(36) *)
module Peers_hash = Blake2B.Make(Base58)(struct
    let name = "Peers_hash"
    let title = "A hash of a sequence of peers"
    let b58check_prefix = peers_hash_prefix
    let size = Some 20
  end)
let () =
  Base58.check_encoded_prefix Peers_hash.b58check_encoding "PEE" 36

module Current_peers_hash =
  Make_single_data_storage
    (Registered)(Raw_context)
    (struct let name = [ "peers_hash" ] end)
    (Peers_hash)

module Peers =
    Make_data_set_storage
      (Make_subcontext(Registered)(Raw_context)
         (struct let name = ["peers"] end))
      (Storage.Make_index(String_index))

let change_peer_ids ctxt activate peer_ids =
  Current_peers_hash.get_option ctxt >>=? fun option ->
  let prev_hash = match option with
    | None -> Peers_hash.hash_bytes []
    | Some hash -> hash
  in
  fold_left_s
    (fun (ctxt, to_hash) peer_id ->
       if not ( Dune_debug.is_peer_id peer_id ) then
         Dune_misc.failwith "Bad peer_id %S" peer_id
       else
         Peers.mem ctxt peer_id >>= fun active ->
         begin
           match activate, active with
           | true, true
           | false, false ->
               Dune_misc.failwith "Peer_id %S: no change needed" peer_id
           | true, false ->
               Peers.add ctxt peer_id >>= fun ctxt ->
               return (ctxt, MBytes.of_string ( "+" ^ peer_id ) :: to_hash)
           | false, true ->
               Peers.del ctxt peer_id >>= fun ctxt ->
               return (ctxt, MBytes.of_string ( "-" ^ peer_id ) :: to_hash)
         end
    ) (ctxt, [Peers_hash.to_bytes prev_hash]) peer_ids
  >>=? fun ( ctxt, to_hash ) ->
  let hash = Peers_hash.hash_bytes to_hash in
  Current_peers_hash.init_set ctxt hash >>= return

(* End of management of peers_id *)


let patch_constants = Raw_context.patch_constants

let protocol_revision = Raw_context.protocol_revision
let set_protocol_revision t v =
  Raw_context.set_protocol_revision t v
let check_protocol_revision t =
  let revision = protocol_revision t in
  fail_when Compare.Int.(revision > Protocol.max_revision)
    (Unsupported_revision (revision, Protocol.max_revision))
let has_protocol_revision t v =
  Compare.Int.(protocol_revision t >= v)

(* Signature re-exported by Alpha_context, to avoid multiple
   redefinitions of types and values *)
module type S = sig
  type context
  type dune_parameters
  type parametric

  val get_foundation_pubkey : context -> Signature.Public_key.t Lwt.t
  val remove_foundation_pubkey : context -> context Lwt.t
  val set_foundation_pubkey : context -> Signature.Public_key.t -> context Lwt.t

  val set_activate_protocol : context ->
    ?protocol:Protocol_hash.t ->
    ?protocol_parameters:dune_parameters ->
    int32 ->
    context Lwt.t
  val get_activate_protocol_level :
    context -> int32 option tzresult Lwt.t
  val get_activate_protocol :
    context ->
    (int32 * Protocol_hash.t option * dune_parameters option)
      option tzresult Lwt.t
  val get_activate_protocol_and_cleanup :
    context ->
    (context * Protocol_hash.t option * dune_parameters option) tzresult Lwt.t

  val patch_constants : context ->
    (parametric -> parametric) -> context Lwt.t

  val protocol_revision : context -> int
  val set_protocol_revision : context -> int -> context Lwt.t
  val check_protocol_revision : context -> unit tzresult Lwt.t
  val has_protocol_revision : context -> int -> bool
  val change_peer_ids : context -> bool -> string list ->
    context tzresult Lwt.t
end
