(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Ident
open Log
open Collections
open Love_ast_types
open Love_ast_types.AST
open Love_type
open Love_ast_types.TYPE
open Love_type_utils

exception EnvironmentError of string

(** If set to true, the environment will make internal and abstract
   type definitions visible. The interpreter needs to have access to
   the type definitions. *)
(* GLOBAL_STATE *)
let internal_mode = ref false

let set_environment_mode b = internal_mode := b

module SMap =
struct
  module M = StringMap

  (** Every element is registered with a unique ID stating the order in
      which they have been registered
      (except primitives and core types, that have a special timestamp
      of 0). *)
  let counter = ref 1

  type flag = Deleted | Registered

  type 'a elts = {
    res : 'a;
    timestamp : int;
    reg_flag : flag
  }
  type 'a t = 'a elts M.t
  let add ?timestamp key res map =
    match timestamp with
      None -> (
        let timestamp = !counter in
        M.add key {res; timestamp; reg_flag = Registered} map
      )
    | Some timestamp ->
      M.add key {res; timestamp; reg_flag = Registered} map

  let iter f (m : 'a t) =
    M.iter (fun k r ->
       match r.reg_flag with
         Deleted when not (!internal_mode) -> ()
       | _ -> f k (r.res, r.timestamp)
    )
    m

  let filter f (m : 'a t) =
    M.filter (fun k r -> f k (r.res, r.timestamp))
    m

  let fold f (m : 'a t) acc =
    M.fold (fun k r acc ->
       match r.reg_flag with
         Deleted when not (!internal_mode) -> acc
       | _ -> f k (r.res, r.timestamp) acc
    )
    m
    acc

  let empty = M.empty
  let find_opt (key : string) (map : 'a t) =
    match M.find_opt key map with
    | Some {reg_flag = Deleted; _} when not !internal_mode -> None
    | Some r -> Some (r.res, r.timestamp)
    | None ->(* (
        match String.split_on_char '/' key with
          [] | [_] -> None
        | l ->
          try
            let id, key =
              let rev_l = List.rev l in
              int_of_string (List.hd rev_l),
              String.concat "" (List.rev @@ List.tl rev_l)
            in
            match M.find_opt key map with
              Some r when Compare.Int.equal r.timestamp id ->
              Some (r.res, r.timestamp)
            | _ -> None
          with
            Invalid_argument _ (* int_of_string *) -> None
                ) *) None

  let remove e (m : 'a t) =
    match find_opt e m with
        None -> m
      | Some (res, timestamp) -> M.add e {res; timestamp; reg_flag = Deleted} m

  let add_unique ?timestamp key elt map =
    if M.mem key map
    then raise (EnvironmentError ("Failure : Trying to register " ^ key ^ " twice"))
    else add ?timestamp key elt map

  let incr_timestamp () =
    counter := !counter + 1

  let get_timestamp () =
    !counter

end

module TVSet = TYPE.TypeVarSet
module TVMap = TYPE.TypeVarMap

type typ_constructor = {
    cparent : TYPE.t;
    cname : string;
    cargs : TYPE.t list;
    crec : TYPE.recursive
  }

type typ_field = {
  fparent : TYPE.t;
  fname : string;
  ftyp : TYPE.t;
  frec : TYPE.recursive
}

type alias = {
  aparams : TYPE.type_var list;
  atype : TYPE.t;
  absolute_path : TYPE.multiple_path
}

type def_loc = External | Local

(** When registering values in the environment,
    they can either be entry points, or simple values (like variables or functions).
    Internal is reserved for expressions inside the definition of an entry point
    or a value and are discarded after its typing.

    For example, in "let x () = let y = 5 in y + 2", 'y' is Internal and 'x' is a value
*)
type val_kind =
  | Entry
  | View of def_loc
  | Value of visibility
  | Init of visibility
  | Internal
  | Primitive of Love_primitive.generic_primitive

type t = {
  sc_parent : parent; (** The parent environment. *)
  sc_local : env; (** The content of the current environment *)
}

and parent =
    Root
  | SubStructure of string * t

and env =
  {
    is_module : bool;

    dependencies : (string * string) list;

    var_typ    : (val_kind * TYPE.t) SMap.t;
    (* Variable <-> (Entry type | Value type) *)

    sum_typ : typ_constructor list SMap.t;
    (* Typename <-> typ constructor infos *)

    constr_typ : typ_constructor SMap.t;
    (* Constuctor name <-> typ constructor info *)

    record_typ : typ_field list SMap.t;
    (* Typename <-> typ fields infos *)

    field_typ : typ_field SMap.t;
    (* Field <-> typ field info  *)

    signature_types : t SMap.t;
    (* Signature <-> local and parent environment *)

    contract_types : contract_type SMap.t;
    (* Contracts <-> local and parent environment *)

    exception_typ : TYPE.t list SMap.t;
    (* Exception constructor <-> Exception arguments  *)

    forall_typ : TVSet.t;
    (* Registers the set of universally quantified variables.
       If a typvar is registered twice, it raises an exception. *)

    aliases : alias SMap.t; (* error when using TNMap *)
    (* The set of aliases *)

    type_kind : (type_kind * type_var list * traits) SMap.t;
    (* Each type is mapped to its kind (internal, abstract, private or public) and
       its parameters *)

    all_exceptions : StringSet.t;
    (* Every exception defined in submodules *)

  }

and contract_type = t

type path = Path.t

type 'res request_result = {
  result : 'res; (* The request result *)
  path : path; (* The relative path of the environment in which the result has been found *)
  last_env : t; (* The environment in which the result has been found *)
  index : int; (* The eventual index of the result *)
  in_core_env : bool (* true = the element is in the core environment  *)
}

type 'res env_request = 'res request_result option


let env_error =
  let pre = fun m -> raise (EnvironmentError m) in
  Format.kasprintf pre


let str_parent =
  function
    Root -> "main"
  | SubStructure (n,_) -> n

let index_of_env env =
  match env.sc_parent with
    Root -> max_int
  | SubStructure (n, env) -> (
      match SMap.find_opt n env.sc_local.contract_types with
        Some (_,i) -> i
      | None -> (
          match SMap.find_opt n env.sc_local.signature_types with
            Some (_, i) -> i
          | None -> max_int
        )
    )

let rec root_env e =
  match e.sc_parent with
    Root -> e
  | SubStructure (n, parent_e) ->
    let new_parent =
      let sc_local = parent_e.sc_local in
      let contract_types =
        let old_structures = parent_e.sc_local.contract_types in
        SMap.add_unique n e old_structures
      in
      let new_sc_local = {sc_local with contract_types = contract_types}
      in
      {parent_e with sc_local = new_sc_local}
    in
    root_env new_parent

let env_path_str : t -> string list =
  fun (env : t) ->
  let rec loop =
    fun (res : string list) (env : t) ->
      match env.sc_parent with
        Root -> res
      | SubStructure (n, env) -> loop (n :: res) env
  in
  loop [] env

let pp_sep = (fun fmt () -> Format.fprintf fmt ", ")

let pp_typ_constr fmt tc =
  Format.fprintf fmt
    "%s of (%a) : %a"
    tc.cname
    (Format.pp_print_list ~pp_sep Love_type.pretty) tc.cargs
    Love_type.pretty tc.cparent

let pp_typ_field fmt tf =
  Format.fprintf fmt
    "(Field %s : %a) : %a"
    tf.fname
    Love_type.pretty tf.ftyp
    Love_type.pretty tf.fparent

let pp_typ_alias fmt al =
  Format.fprintf fmt
    "Params = %a, type = %a, absolute_path = %a"
    (Format.pp_print_list Love_type.pp_typvar) al.aparams
    Love_type.pretty al.atype
    Love_type.pp_multiple_path al.absolute_path

let pp_simap indent printer =
  fun fmt map ->
  SMap.iter
    (fun key (bnd,_) ->
       Format.fprintf fmt
         "%s%s <-> %a\n"
         indent
         key
         printer bnd
    )
    map

let pp_tvset =
  fun fmt set ->
  TVSet.iter
    (fun key ->
       Format.fprintf fmt
         "%a,\n"
         pp_typvar key
    )
    set

let pp_valkind fmt = function
  | Entry -> Format.fprintf fmt "Entry "
  | View Local -> Format.fprintf fmt "Local View "
  | View External -> Format.fprintf fmt "External View "
  | Value _ -> Format.fprintf fmt "Value "
  | Init _ -> Format.fprintf fmt "Init "
  | Internal -> Format.fprintf fmt "Internal "
  | Primitive _ -> Format.fprintf fmt "Primitive "

let rec pp_env ?(indent="") fmt env =
  let pp_cenv = pp_cenv ~indent:(indent ^ "  ") in
  let parent = env.sc_parent in
  let env = env.sc_local in
  Format.fprintf fmt
    "%s{Structure %s\n\
     %sIs module:%b@.\
     %sVariables:\n%a@.\
     \n\
     %sTypes:\n%a@.\
     \n\
     %sSum types:\n%a@.\
     %sConstructors:\n%a@.\
     \n\
     %sRecords:\n%a@.\
     %sFields:\n%a@.\
     \n\
     %sAliases:\n%a@.\
     \n\
     %sSubcontracts:\n%a@.\
     \n\
     %sSubsignatures:\n%a@.\
     \n\
     %sQuantified:\n%a@.\
     %s}"
    indent (str_parent parent)
    indent env.is_module
    indent (pp_simap indent (fun fmt (k, t) -> Format.fprintf fmt "%a %a" pp_valkind k Love_type.pretty t)) env.var_typ
    indent (pp_simap indent pp_typ_kind) env.type_kind
    indent (pp_simap indent (Format.pp_print_list ~pp_sep pp_typ_constr)) env.sum_typ
    indent (pp_simap indent pp_typ_constr) env.constr_typ
    indent (pp_simap indent (Format.pp_print_list ~pp_sep pp_typ_field)) env.record_typ
    indent (pp_simap indent pp_typ_field) env.field_typ
    indent (pp_simap indent pp_typ_alias) env.aliases
    indent (pp_simap indent pp_cenv) env.contract_types
    indent (pp_simap indent pp_env) env.signature_types
    indent pp_tvset env.forall_typ
    indent

and pp_typ_kind fmt (k,l, _traits) = Format.fprintf fmt
    "%a : %i parameters" Love_ast.pp_type_kind k (List.length l)

and pp_cenv ~indent fmt cenv = pp_env ~indent fmt cenv

let pp_env fmt env = pp_env fmt env

let empty_env kind =
  let is_module, dependencies =
    match kind with
      Module -> true, []
    | Contract l -> false, l
  in
  {
    is_module;
    dependencies;
    var_typ = SMap.empty;
    sum_typ = SMap.empty;
    constr_typ = SMap.empty;
    record_typ = SMap.empty;
    field_typ = SMap.empty;
    forall_typ = TVSet.empty;
    signature_types = SMap.empty;
    contract_types = SMap.empty;
    exception_typ = SMap.empty;
    type_kind = SMap.empty;
    all_exceptions = StringSet.singleton "Failure";
    aliases = SMap.empty; (* TNMap causes error *)
  }

let empty is_module () =
  let sc_local = empty_env is_module in {
    sc_local;
    sc_parent = Root
  }

(* GLOBAL_STATE *)
let core_env = ref (empty (Contract []) ())
(* Changed after initializing at the end on environment *)

let get_core_env () = !core_env

let add_var kind x t env =
  debug "[add_var] Adding %s -> %a to environment@."
    x
    Love_type.pretty t;
  match kind with
    Internal ->
      {env with var_typ = SMap.add x (kind, t) env.var_typ}
  | _ ->
    {env with var_typ = SMap.add_unique x (kind, t) env.var_typ}

let add_var ?(kind=Internal) x t env =
  SMap.incr_timestamp ();
  {env with sc_local = add_var kind x t env.sc_local}

let add_sum params cparent crlist crec env =
  let params = TVSet.of_list params in
  let name =
    match cparent with
      TUser (name, _) -> Ident.get_final name
    | _ -> raise (EnvironmentError "Add sum : Trying to register a non user type") in
  let cstrs, env =
    List.fold_left
      (fun (acc_tcstr, env) (cname,cargs) ->
         let tcstr = {cparent; cname; cargs; crec}
         in
         let () =
           List.iter
             (fun carg ->
                if TVSet.subset (fvars carg) params
                then ()
                else
                  raise (EnvironmentError "Missing polymorphic arguments in params.")
             )
             cargs
         in
         tcstr :: acc_tcstr,
         {env with constr_typ = SMap.add_unique cname tcstr env.constr_typ}
      )
      ([], env)
      crlist in
  {env with sum_typ = SMap.add_unique name cstrs env.sum_typ}

let add_sum params cparent crlist crec env =
  {env with sc_local = add_sum params cparent crlist crec env.sc_local}

let add_record params fparent flist frec env =
  let params = TVSet.of_list params in
  let name =
    match fparent with
      TUser (name, _) -> Ident.get_final name
    | _ -> raise (EnvironmentError "Add record : Trying to register a non user type") in

  let fts, env =
    List.fold_left
      (fun (acc_tcstr, env) (fname,ftyp) ->
         let tcstr = {fparent; fname; ftyp; frec}
         in
         let fvars_args = fvars ftyp
         in
         let () =
           if (not (TVSet.subset fvars_args params))
           then (
             debug "[add_record] %a is not a subset of %a."
               (fun fmt s -> TVSet.iter (fun e -> Format.fprintf fmt "%a" Love_type.pp_typvar e) s) fvars_args
               (fun fmt s -> TVSet.iter (fun e -> Format.fprintf fmt "%a" Love_type.pp_typvar e) s)  params;
             raise (EnvironmentError "Type record using unknwown type variables"))
           else ()
         in
         tcstr :: acc_tcstr, {env with field_typ = SMap.add_unique fname tcstr env.field_typ}
      )
      ([], env)
      flist in
  {env with record_typ = SMap.add_unique name fts env.record_typ}

let add_record params fparent frlist frec env =
  {env with sc_local = add_record params fparent frlist frec env.sc_local}

let add_forall tv env =
  if TVSet.mem tv env.forall_typ
  then
    raise (
      EnvironmentError (
        Format.asprintf "Typ %a already registered"
          Love_type.pp_typvar tv
      )
    )
  else {env with forall_typ = TVSet.add tv env.forall_typ}

let add_forall tv env =
  SMap.incr_timestamp ();
  {env with sc_local = add_forall tv env.sc_local}

let new_subcontract_env name is_module env =
  let sc_local =
    match is_module with
      Module -> {(empty_env is_module) with dependencies = env.sc_local.dependencies}
    | Contract l -> ( (* The dependencies of a sub contract must be included in the root
                         contract.  *)
        let () =
          List.iter
            (fun (key, kt1) ->
               match List.assoc_opt key env.sc_local.dependencies with
                 None ->
                 raise (
                   EnvironmentError (
                     Format.asprintf "Dependency %s must be declared previously" key)
                 )
               | Some kt1' ->
                 if String.equal kt1 kt1'
                 then ()
                 else
                   raise (
                     EnvironmentError (
                       Format.asprintf
                         "Dependency %s = %s is incorrect : expected %s = %s."
                         key kt1 key kt1')
                   )
            )
            l
        in
        let root = root_env env in
        let contract_types =
          List.fold_left
            (fun acc (key,_) ->
               match SMap.find_opt key root.sc_local.contract_types with
                 None ->
                 raise (EnvironmentError "Imported module not registered in root")
               | Some (s, _) ->
                 let s = {s with sc_parent = SubStructure (key, env)} in
                 SMap.add key s acc
            )
            SMap.empty
            l
        in
        {(empty_env is_module) with dependencies = l; contract_types}
      )
  in {
    sc_local =
      {sc_local with
       forall_typ = env.sc_local.forall_typ;
       all_exceptions = sc_local.all_exceptions;
      };
    sc_parent = SubStructure (name,env);
  }

let add_exception exc_name tlist env =
  if has_protocol_revision 4 then
    { env with exception_typ = SMap.add_unique exc_name tlist env.exception_typ }
  else
  if StringSet.mem exc_name env.all_exceptions
  then raise (EnvironmentError ("Exception " ^ exc_name ^ " registered twtce"))
  else
    {env with
     exception_typ = SMap.add_unique exc_name tlist env.exception_typ;
     all_exceptions = StringSet.add exc_name env.all_exceptions
    }

let add_exception exc_name tlist t =
  SMap.incr_timestamp ();
  try
    {t with sc_local = add_exception exc_name tlist t.sc_local}
  with EnvironmentError _ ->
    raise (
      EnvironmentError (
        Format.asprintf "Exception %s already registered in environment %s"
          exc_name
          (str_parent t.sc_parent)
      )
    )

let add_signature name s env =
  {env with signature_types = SMap.add_unique name s env.signature_types}

let add_signature name s env =
  SMap.incr_timestamp ();
  {env with sc_local = add_signature name s env.sc_local}

let add_abstract_type name params env =
  SMap.incr_timestamp ();
  let sc_local = env.sc_local in
  {env with
   sc_local = {
     sc_local with
     type_kind = SMap.add_unique name (TAbstract, params, default_trait) sc_local.type_kind
   }
  }

let fst_opt =
  function
    None -> None
  | Some e -> Some (fst e)

let print_int_as_before fmt i =
  if Compare.Int.(i = max_int)
  then Format.fprintf fmt "+inf"
  else Format.fprintf fmt "%i" i

let relative_path env1 path =
  let rec aux from_env to_env  =
    match from_env, to_env with
      [], _ -> to_env
    | hd1 :: tl1, hd2 :: tl2 -> (
        if String.equal hd1 hd2 then aux tl1 tl2
        else to_env
      )
    | _, [] -> []
  in
  aux (env_path_str env1) path

type alias_or_exist =
    EAlias of TYPE.t * multiple_path
  | ExistButNotAlias

let rec alias_or_exist ?before name args env =
  debug "[alias] Searching for %a@." Ident.print_strident name;
  find_env ?before name env
    (fun s env ->
       match _alias env s args with
         Some ((a, b),i) -> Some (EAlias (a, b), i)
       | None ->
         (
           match SMap.find_opt s env.sc_local.type_kind with
             None -> None
           | Some (_, i) -> Some (ExistButNotAlias, i)
         )
    )

and _alias env name args =
  debug "[alias] Searching for alias %s in environment %s@."
    name
    (str_parent env.sc_parent);
  match SMap.find_opt name env.sc_local.aliases with
    None -> None

  | Some ({aparams; atype; absolute_path},index) ->
      debug "[alias] Alias found : %a@." Love_type.pretty atype;
      let tvmap =
        try
          List.fold_left2
            (fun acc poly arg ->
               debug "[alias] %a = %a@." Love_type.pp_typvar poly Love_type.pretty arg;
               TVMap.add poly arg acc)
            TVMap.empty
            aparams
            args
        with Invalid_argument _ -> raise (EnvironmentError "Incorrect number of args")
      in
      let res_type = replace_map (fun x -> isComp x env) tvmap atype in
      debug "[alias] Alias type = %a@."
        Love_type.pretty res_type;
      Some ((res_type, absolute_path), index)

and add_typedef
    (name : string)
    (k : AST.type_kind)
    (td : TYPE.typedef)
    env =
  SMap.incr_timestamp ();
  debug "[add_typedef] Adding type %a as a %a type.@."
    (Love_type.pp_typdef ~name ~privacy:(Love_ast.kind_to_string k)) td
    Love_ast.pp_type_kind k
  ;
  let sc_local = env.sc_local in
  match td with
  | Alias {aparams; atype} ->
    let absolute_path atype =
      match atype with
        TUser(nname, _) ->
        debug "[add_typedef] Alias %s = %a@." name Ident.print_strident nname;
        let name_env =
          match get_env_of_id nname env
          with
            None -> raise (EnvironmentError "Trying to register an alias with an undefined type")
          | Some (e,_) -> e
        in
        debug "[add_typedef] Env of %a is %s@."
          Ident.print_strident nname
          (str_parent name_env.sc_parent);
        let path = AbsolutePath (env_path_str name_env) in
        debug "[add_typedef] Type %a is defined in %a@."
          Ident.print_strident nname
          pp_multiple_path path;
        path
      | _ -> Local
    in
    let absolute_path =
      match atype with
        TTuple l -> Multiple (List.map absolute_path l)
      | TArrow (t1, t2) -> Multiple [absolute_path t1; absolute_path t2]
      | TForall (_, t) -> absolute_path t
      | _ -> absolute_path atype
    in
    debug "[add_typedef] Adding alias %s = %a with absolute path %a@."
      name
      Love_type.pretty atype
      pp_multiple_path absolute_path;
    let alias = {aparams; atype; absolute_path} in
    let norm_atype =
      if Love_pervasives.has_protocol_revision 3 then
        normalize_type ~relative:true atype env
      else atype
    in
    debug "[add_typedef] Testing comparability of %a@."
      Love_type.pretty norm_atype;
    let tcomparable =
      Love_type_utils.isComparable
        (fun x -> isComp x env)
        norm_atype in
    {env with
     sc_local = {
       sc_local with
       aliases = (*TNMap*)SMap.add_unique name alias sc_local.aliases;
       type_kind =
         SMap.add_unique
           name
           (k,
            aparams,
            {tcomparable}
           )
           sc_local.type_kind
     }}
  | SumType {sparams; scons; srec} -> (
      let params = List.map (fun v -> TVar v) sparams in
      debug "[add_typedef] Adding Sum type %s@." name;
      let t = TUser (Ident.create_id name, params) in
      let isrec =
        match srec with
          Rec -> true
        | NonRec -> false
      in
      let env =
        if isrec
        then
          add_sum
            sparams
            t
            scons
            srec
            {env with sc_local = {
                 sc_local with
                 type_kind =
                   SMap.add_unique
                     name (k, sparams, {tcomparable = true}) sc_local.type_kind
               }
            }
        else env in
      let tcomparable =
        if Love_pervasives.has_protocol_revision 3 then
          List.for_all
            (fun (_, tl) ->
               List.for_all (
                 fun t : bool ->
                   match t with
                     TVar tv ->
                       if List.mem tv sparams
                       then true
                       else tv.tv_traits.tcomparable
                   | _ -> Love_type_utils.isComparable (fun x -> isComp x env) t
               )
                 tl
            )
            scons
        else
          List.for_all
            (fun (_, tl) ->
               List.for_all (Love_type_utils.isComparable (fun x -> isComp x env))
                 tl
            )
            scons
      in
      debug "[add_typedef] It is%s comparable@." (if tcomparable then "" else " not");
      if isrec
      then
        let sc_local = env.sc_local in
        { env with sc_local = {
              sc_local with
              type_kind =
                SMap.add
                  name (k, sparams, {tcomparable}) sc_local.type_kind
            }
        }
      else
          add_sum
            sparams
            t
            scons
            srec
            {env with sc_local = {
                 sc_local with
                 type_kind =
                   SMap.add_unique
                     name (k, sparams, {tcomparable}) sc_local.type_kind
               }
            }
    )

  | RecordType {rparams; rfields; rrec} ->
    let params = List.map (fun v -> TVar v) rparams in
    let t = TUser (Ident.create_id name, params) in
    let isrec =
      match rrec with
        Rec -> true
      | NonRec -> false
    in
    let env =
      if isrec
      then
        add_record
          rparams
          t
          rfields
          rrec
          {env with sc_local = {
               sc_local with
               type_kind =
                 SMap.add_unique name (k, rparams, {tcomparable = true}) sc_local.type_kind
             }
          }
      else env in
    let tcomparable =
      if Love_pervasives.has_protocol_revision 3
      then
        begin
          List.for_all (
            fun (_, t) ->
              match t with
                TVar tv ->
                  if List.mem tv rparams
                  then true
                  else tv.tv_traits.tcomparable
              | _ ->
                  Love_type_utils.isComparable (fun x -> isComp x env) t)
            rfields
        end
      else
        begin
          List.for_all (
            fun (_, t) -> Love_type_utils.isComparable (fun x -> isComp x env) t)
            rfields
        end
    in
    if isrec
    then
      let sc_local = env.sc_local in
      { env with sc_local = {
            sc_local with
            type_kind =
              SMap.add
                name (k, rparams, {tcomparable}) sc_local.type_kind
          }
      }
    else
      add_record
        rparams
        t
        rfields
        rrec
        {env with sc_local = {
             sc_local with
             type_kind =
               SMap.add_unique name (k, rparams, {tcomparable}) sc_local.type_kind
           }
        }

and contract_sig_to_env
    sc_parent
    (cs : TYPE.structure_sig)
    (parent_env : t)
  : t =

  let new_env = empty_env cs.sig_kind in
  (* 1. Add dependencies *)
  let new_env =
    match cs.sig_kind with
      Module -> new_env
    | Contract deps -> (
        let current_deps = parent_env.sc_local.dependencies in
        let current_modules = parent_env.sc_local.contract_types in
        List.fold_left
          (fun new_env (mod_name, kt1) ->
             match List.assoc_opt mod_name current_deps with
               Some kt1' when String.equal kt1 kt1' -> (
                 match SMap.find_opt mod_name current_modules with
                 | Some (s,_) -> {
                     new_env with
                     contract_types = SMap.add_unique mod_name s new_env.contract_types
                   }
                 | None ->
                   raise (
                     EnvironmentError ("Dependency " ^ mod_name  ^ " is not registered" ))
               )
             | _ ->
               raise (EnvironmentError ("Dependency " ^ mod_name  ^ " is incorrect" ))
          )
          new_env
          deps
      )
  in

  (* 2. Add content *)
  let add_content s content env : t =
    debug "[contract_sig_to_env] Adding %s to environment" s;
    match content with
    | SType ts -> (
        match ts with
          SPublic tdef -> add_typedef s TPublic tdef env
        | SPrivate tdef -> add_typedef s TPrivate tdef env
        | SAbstract tl ->
          let sc_local = env.sc_local in
          {env with
           sc_local =
             {sc_local with
              type_kind =
                SMap.add_unique s (TAbstract, tl, default_trait) sc_local.type_kind}
          }
      )
    | SException l ->
      let sc_local = env.sc_local in {
        env with
        sc_local =
          {sc_local with
           exception_typ =
             SMap.add_unique s l sc_local.exception_typ}
      }

    | SInit t ->
      let sc_local = env.sc_local in
      if Love_type.is_module cs
      then raise (EnvironmentError "Module registered with an entry point.")
      else {
        env with
        sc_local =
          {sc_local with
           var_typ =
             SMap.add_unique
               s
               (Init Public, t)
               sc_local.var_typ
          }
      }

    | SEntry t ->
      let sc_local = env.sc_local in
      if Love_type.is_module cs
      then raise (EnvironmentError "Module registered with an entry point.")
      else {
        env with
        sc_local =
          {sc_local with
           var_typ =
             SMap.add_unique
               s
               (Entry, Love_type_list.get_type "entrypoint" [t])
               sc_local.var_typ
          }
      }
    | SView t ->
      let sc_local = env.sc_local in {
        env with
        sc_local =
          {sc_local with
           var_typ =
             SMap.add_unique
               s
               (View External, Love_type_list.get_type "view" [t])
               sc_local.var_typ
          }
      }
    | SValue t ->
      let sc_local = env.sc_local in {
        env with
        sc_local =
          {sc_local with
           var_typ = SMap.add_unique s (Value Public, t) sc_local.var_typ
          }
      }
    | SStructure str ->
      debug "[contract_sig_to_env] Structure in signature@.";
      let sc_local = env.sc_local in
      let ctype =
        match str with
          Named n -> (
            match find_signature n env with
              None -> raise (EnvironmentError "Unregistered signature")
            | Some {result; _} -> result
          )
        | Anonymous a -> contract_sig_to_env (SubStructure (s, env)) a env
      in {
        env with
        sc_local =
          {sc_local with
           contract_types =
             SMap.add_unique
               s
               ctype
               sc_local.contract_types
          }
      }
    | SSignature s_sig  ->
      debug "[contract_sig_to_env] Signature definition in signature@.";
      let sc_local = env.sc_local in
      let ctype = contract_sig_to_env (SubStructure (s, env)) s_sig env
      in {
        env with
        sc_local =
          {sc_local with
           signature_types =
             SMap.add_unique
               s
               ctype
               sc_local.signature_types
          }
      }
  in
  let empty_env = {
    sc_parent;
    sc_local = new_env;
  }
  in
  List.fold_left
    (fun env (s,c) -> add_content s c env)
    empty_env
    cs.sig_content

and get_env_of_id ?before id env =
  match find_env ?before id env (fun _ _ -> Some ((),max_int))
  with
    None -> None
  | Some {last_env; index; _} -> Some (last_env, index)

and find_signature x (env : t) : t env_request =
  debug "[find_contract] Searching signature %a@." print_strident x;
  (* match Ident.split x with
    "UnitContract", None ->
    Some {
      result = {
        sc_local = empty_env (Contract []);
        sc_parent = Root}
        |> add_var
          ~kind:Entry
          "default"
          (Love_type_list.(get_type "entrypoint" [get_type "unit" []]));
      path = [];
      last_env = env;
      index = 0;
      in_core_env = false
    }
  | _ ->*)
    let search =
      (fun name env -> SMap.find_opt name env.sc_local.signature_types) in
    find_env x env search

and find_env :
  type b.
  ?before : int ->
  string Ident.t -> t ->
  (string -> t -> (b * int) option)
  -> b env_request = fun
  ?before
  (id : string Ident.t)
  env
  search ->
  let test_before =
    match before with
      None -> fun _ -> true
    | Some i ->
      fun i' ->
        let res = Compare.Int.(i > i')
        in
        if res
        then debug "[find_env] Test before succeeded, %i > %i@." i i'
        else debug "[find_env] Test before failed : %i <= %i@." i i';
        res
  in
  let print_before fmt = function
      None -> ()
    | Some i -> print_int_as_before fmt i in
  let rec __find_env id_acc id env =
    debug "[find_env] Searching for %a in environment %s = %a@."
      Ident.print_strident id
      (str_parent env.sc_parent)
      print_before before
    ;
    match Ident.split id with
    | name, None ->
      debug "[find_env] Last id@."; Some (name, env, List.rev id_acc)
    | name, Some i -> (
        match SMap.find_opt name env.sc_local.var_typ with
          Some ((_, TContractInstance ct), _) -> (
            let next_env =
              match ct with
                Anonymous s -> contract_sig_to_env Root s env
              | Named n -> (
                  match find_signature n env with
                    None -> raise (EnvironmentError "Unknown signature")
                  | Some {result; _} -> result
                )
            in
            __find_env (Path.Next name :: id_acc) i next_env
        )
        | Some _ -> raise (EnvironmentError "Variable is not a first class module")
        | None -> (
            match SMap.find_opt name env.sc_local.contract_types with
            | Some (e, index) ->
              debug "[find_env] Searching in sub structure %s = %s (index = %i)@."
                name (str_parent e.sc_parent)
                index;
              if test_before index
              then  (
                __find_env (Path.Next name :: id_acc) i e
              )
              else None

            | None ->
              debug "[find_env] Not found.@."; (*
          if search_sig then
            match SMap.find_opt name env.sc_local.signature_types with
              None -> None
            | Some (e,index) ->
              if test_before index then __find_env (Next name :: id_acc) i e
              else None
          else *) None
          )
      )
  in
  let search_in_core id =
    match __find_env [] id (get_core_env ()) with
      None -> (
        debug "[find_env] Module not found in Core@.";
        None
      )
    | Some (name, env, path) -> (
        match search name env with
          Some (result, index) ->
          debug "[find_env] Found in Core@.";
          Some {
            result; path;
            last_env = env;
            index;
            in_core_env = true
          }
        | None -> debug "[find_env] %s not found in Core@." name; None
      )
  in
  let search_in_parent () =
    if env.sc_local.is_module
    then (
      match env.sc_parent with
        Root -> (
        debug "[find_env] Current environment has no parent, checking in Core@.";
        search_in_core id
      )
      | SubStructure (_, p) ->
        debug "[find_env] Current environment has a parent@.";
        match find_env id p search with
          None -> None
        | Some r -> Some {r with path = Path.Prev :: r.path}
    )
    else (
      debug "[find_env] Current environment is a contract : must be self contained.\
             Searching in Core.@.";
      search_in_core id
    )
  in
  match __find_env [] id env with
    None -> (
      debug "[find_env] %a cannot be resolved environment, searching in parent@."
        Ident.print_strident id;
      search_in_parent ()
    )
  | Some (name, env, path) -> (
      match search name env with
        Some (result, index) -> Some {
          result; path;
          last_env = env;
          index;
          in_core_env = false
        }
      | None ->
        debug
          "[find_env] %s is not in the environment, searching in parent@."
          name
        ;
        search_in_parent ()
    )

and find_type_kind x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.type_kind in
  find_env x env search

and isComp x env =
  debug "[Love_tenv.isComp] Checking comparability of %a" Ident.print_strident x;
  match find_type_kind x env with
    None ->
    raise (
      EnvironmentError (
        Format.asprintf "Unknown type %a for checking comparability"
          Ident.print_strident x))
  | Some {result = (_,_,traits); _} -> traits.tcomparable

and normalize_type : relative:bool -> TYPE.t -> t -> TYPE.t =
  fun ~relative typ env ->

  (* From a type name and the environment containing it, returns the
     correct identifyer depending on the relative flag *)
  let normalize_name
      (*(index : int)*)
    (type_name : string Ident.t)
    (absolute_path : string list)
    : string Ident.t = (*
    let correct_type_name =
      let type_name = Ident.get_final type_name in
      Ident.create_id (
        match String.split_on_char '/' type_name with
          [_] -> type_name ^ "/" ^ (string_of_int index)
        | _ -> type_name
      )
    in *)
    if relative
    then (
      let type_name_path =
        debug "[normalize_name] Relative path from %s to path %a@."
          (str_parent env.sc_parent)
          (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s"))  absolute_path
        ;
        let res = relative_path env absolute_path in
        debug "[normalize_name] Relative path from %s to path %a = %a@."
          (str_parent env.sc_parent)
          (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s"))  absolute_path
          (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s"))  res;
        res
      in
      Ident.put_in_namespaces (List.rev type_name_path) type_name
    ) else (
      let type_name_path =
        debug "[normalize_name] Absolute path %a@."
          (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s"))  absolute_path ;
        absolute_path
      in
      Ident.put_in_namespaces (List.rev type_name_path) type_name
    )
  in

  let rec normalize_by_name before name normalized_params sub_env =
    debug "[normalize] User type %a (before %a)@."
      Ident.print_strident name
      print_int_as_before before;
    let rev_fake_params, tvar_map =
      List.fold_left (
        fun (acc_fake, acc_tvars) t ->
          let tvar = Love_type.fresh_typevar ~tv_traits:Love_type.comparable () in
          (TVar tvar :: acc_fake, TVMap.add tvar t acc_tvars)
      )
        ([], TVMap.empty)
        normalized_params
    in
    let fake_params = List.rev rev_fake_params in
    debug "[normalize] Searching aliases@.";
    match alias_or_exist ~before name fake_params sub_env with
      None ->
        debug "[normalize] Error : type %a is not found@."
          Ident.print_strident name;
        raise (
          EnvironmentError (
            Format.asprintf
              "Cannot find environment in which type %a is defined"
              Ident.print_strident name
          )
        )
    | Some {result = ExistButNotAlias; last_env; _} ->
        debug "[normalize] %a is not an alias, but exists in env@."
          Ident.print_strident name
        ;
        (* Searching where it is defined *)
        let path_to_definition_env = env_path_str last_env in
        debug "[normalize] Path to definition = %a@." (Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s")) path_to_definition_env;
        let name =
          normalize_name
            (* index *)
            (Ident.(create_id (get_final name)))
            path_to_definition_env
        in
        debug "[normalize] Final type name = %a@." Ident.print_strident name;
        TUser (name, normalized_params)
    | Some {result = EAlias ((TUser (sub_name, params) as t), _); last_env; index; _} ->
        debug "[normalize] Alias found, it is (%a)@." Love_type.pretty t;
        (* We must normalize name and TUser separately.
           Non polymorphic parameters at the definition of the alias are defined in last_env,
           while others are defined in sub_env.
        *)
        let new_params =
          if Love_pervasives.has_protocol_revision 5 then
            let atype =
              match find_env name env (fun name e -> SMap.find_opt name e.sc_local.aliases) with
                None -> raise (Exceptions.InvariantBroken "If an alias exists, it should be on the map")
              | Some {result = {atype; _}; _} ->
                  atype
            in
            match atype with
            | TUser (_sub_name, poly_params) ->
                let rec loop acc pars poly_pars =
                  match pars, poly_pars with
                  | [],[] -> List.rev acc
                  | par :: tlpar, TVar _ :: tlpolypar ->
                      (* Polymorphic parameter -> its definition is in sub_env *)
                      let param = normalize before par sub_env in
                      loop (param :: acc) tlpar tlpolypar
                  | par :: tlpar, _ :: tlpolypar ->
                      (* Non polymorphic parameter -> def in last_env *)
                      let param = normalize before par last_env in
                      loop (param :: acc) tlpar tlpolypar
                  | [], _ | _, [] ->
                      raise (Exceptions.InvariantBroken "Types should be equal")
                in
                loop [] params poly_params
            | _ -> raise (Exceptions.InvariantBroken "Alias should be a TUser")
          else List.map (fun t -> normalize before t sub_env) params
        in
        let t = normalize_by_name index sub_name new_params last_env in
        Love_type_utils.replace_map (fun x -> isComp x env) tvar_map t
    | Some {result = EAlias (t, path); last_env; index; _} ->
        debug "[normalize] Path to environment = %a@."
          pp_multiple_path path;
        let t = normalize index t last_env in
        Love_type_utils.replace_map (fun x -> isComp x env) tvar_map t

  and normalize : int -> TYPE.t -> t -> TYPE.t =
    fun before typ sub_env ->
      debug "[normalize] Type %a (before %a)@."
        Love_type.pretty typ
        print_int_as_before before;
      match typ with
        TUser (name, params) ->
        let params =
          List.map (
            fun t -> normalize before t sub_env
          )
            params in
        normalize_by_name before name params sub_env
      | TVar _ -> typ
      | TArrow (pt,rt) ->
        TArrow (normalize before pt sub_env, normalize before rt sub_env)

      | TTuple l ->
        TTuple (List.map (fun typ -> normalize before typ sub_env) l)
      | TForall (v, t) -> TForall (v, normalize before t sub_env)
      | TContractInstance _
      | TPackedStructure _ -> typ (* Warning : no normalization on contracts ?*)

  in
  debug "[normalize] Normalizing type %a@." Love_type.pretty typ;
  let res = normalize max_int typ env in
  debug "[normalize] Normalized Type %a = %a@."
    Love_type.pretty typ
    Love_type.pretty res;
  res

let contract_sig_to_env (name : string option) (cs : TYPE.structure_sig) env : t =
  debug "[contract_sig_to_env] Contract signature %a@."
    Love_type.pp_contract_sig cs
  ;
  let parent = match name with None -> Root | Some n -> SubStructure (n, env) in
  let res = contract_sig_to_env parent cs env in
  debug "[contract_sig_to_env] Done@.";
  res

let find_contract x env =
  debug "[find_contract] Searching contract %a@." print_strident x;
  let search = fun id env -> SMap.find_opt id env.sc_local.contract_types in
  find_env x env search

let find_contract x env =
  if String.equal (Ident.get_final x) CONSTANTS.current
  then (
    debug "[find_contract] Current contract@.";
    Some {
      result = env; path = [];
      last_env = env;
      index = max_int;
      in_core_env = false
    }
  )
  else find_contract x env

let constr_with_targs tconstr args env =
  let poly_args =
    match tconstr.cparent with
      TUser (_,l) -> l
    | _ -> assert false
  in
  let () =
    if ((Compare.Int.(<>)) (List.length args) (List.length poly_args))
    then raise (EnvironmentError "Bad arguments for constructor") in
  let map =
    List.fold_left2
      (fun acc ty -> function
           TVar tv -> TVMap.add tv ty acc
         | _ -> acc
      )
      TVMap.empty
      args
      poly_args
  in
  {tconstr with cargs = List.map (Love_type_utils.replace_map (fun x -> isComp x env) map) tconstr.cargs}

let field_with_targs tf args env =
  let poly_args =
    match tf.fparent with
      TUser (_,l) -> l
    | _ -> assert false
  in
  let () =
    if ((Compare.Int.(<>)) (List.length args) (List.length poly_args))
    then raise (EnvironmentError "Bad arguments for field") in
  let map =
    List.fold_left2
      (fun acc ty -> function
           TVar tv -> TVMap.add tv ty acc
         | _ -> acc
      )
      TVMap.empty
      args
      poly_args
  in
  {tf with ftyp = Love_type_utils.replace_map (fun x -> isComp x env) map tf.ftyp}

let find_def_path x env =
  match find_type_kind x env with
    None -> raise (EnvironmentError "Type should be defined")
  | Some {last_env; _} -> env_path_str last_env

let rec put_type_in_correct_namespace env relative_path t : TYPE.t =
  let res =
    match t with
      TUser (name, p) -> (
        debug
          "[put_type_in_correct_namespace] Searching for type %a (relative path = %a)@."
          print_strident name
          Path.pp_path relative_path;
        match find_type_kind name env with
          Some {path; in_core_env; _} ->
          let path = if in_core_env then path else relative_path @ path in
          debug "[put_type_in_correct_namespace] Type found, path is [%a]@."
            Path.pp_path path;
          let to_str_path p =
            List.fold_left
              (fun acc p ->
                 match p with
                   Path.Prev -> acc
                 | Next s -> s :: acc
              )
              []
              (Path.simplify_path p)
          in
          TUser (
            put_in_namespaces (to_str_path path) (Ident.(create_id @@ get_final name)),
            List.map (put_type_in_correct_namespace env relative_path) p
          )
        | None -> (
            debug "[put_type_in_correct_namespace] Type not found";
            raise (
              EnvironmentError (
                Format.asprintf
                  "Type %a not found while searching its position"
                  Ident.print_strident name
              )
            )
          )
      )
    | TPackedStructure _
    | TContractInstance _
    | TVar _ -> t

    | TTuple l -> TTuple ((List.map (put_type_in_correct_namespace env relative_path)) l)

    | TArrow (t1, t2) ->
      TArrow (put_type_in_correct_namespace env relative_path t1,
              put_type_in_correct_namespace env relative_path t2)
    | TForall (v, t) -> TForall (v, put_type_in_correct_namespace env relative_path t)
  in
  debug
    "[put_type_in_correct_namespace] Result = %a@."
    Love_type.pretty res;
  res

let correct_typ_constr tcstr path last_env =
  {tcstr with
   cargs = List.map (put_type_in_correct_namespace last_env path) tcstr.cargs;
   cparent = put_type_in_correct_namespace last_env path tcstr.cparent
  }

let correct_typ_field tfld path last_env =
  {tfld with
   ftyp = put_type_in_correct_namespace last_env path tfld.ftyp;
   fparent = put_type_in_correct_namespace last_env path tfld.fparent}

let find_sum x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.sum_typ in
  match find_env x env search with
    None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some {answer with
          result = List.map (fun tc -> correct_typ_constr tc path last_env) result}

let find_record x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.record_typ in
  match find_env x env search with
    None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some {answer with result = List.map (fun tf -> correct_typ_field tf path last_env) result}

let find_constr x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.constr_typ in
  match find_env x env search with
    None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some { answer with result = correct_typ_constr result path last_env }

let find_field x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.field_typ in
  match find_env (Ident.create_id x) env search with
  None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some {
      answer with
      result = correct_typ_field result path last_env
    }

let find_field_in path x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.field_typ in
  match find_env (Ident.concat path (Ident.create_id x)) env search with
    None -> None
  | Some ({result; path; last_env; _} as answer) ->
    Some {
      answer with
      result = correct_typ_field result path last_env
    }

let find_exception x env =
  let search = fun name env -> SMap.find_opt name env.sc_local.exception_typ in
  find_env x env search

let is_free x env = TVSet.mem x env.sc_local.forall_typ

let get_free env = env.sc_local.forall_typ

let add_subcontract_env sc (new_env : t) (env : t) =
  SMap.incr_timestamp ();
  debug "[add_subcontract_env] Adding %s to environment@." sc;
  let sc_local = env.sc_local in
  debug "[add_subcontract_env] Removing abstract and internal typedef definitions@.";
  let new_sc_local = new_env.sc_local in
  let new_sc_local =
    let remove_type tn sc_loc =
      match SMap.find_opt tn sc_loc.record_typ with
        Some (l,_) ->
        List.fold_left
          (fun acc {fname;_} ->
             debug "[add_subcontract_env] \
                    Removing field %s@." fname;
             {acc with field_typ = SMap.remove fname acc.field_typ})
          {sc_loc with record_typ = SMap.remove tn sc_loc.record_typ}
          l
      | None -> (
        match SMap.find_opt tn sc_loc.sum_typ with
            Some (l,_) ->
            List.fold_left
              (fun acc {cname;_} ->
             debug "[add_subcontract_env] \
                    Removing constructor %s@." cname;
                 {acc with constr_typ = SMap.remove cname acc.constr_typ})
              {sc_loc with sum_typ = SMap.remove tn sc_loc.sum_typ}
              l
          | None -> (
             debug "[add_subcontract_env] \
                    Removing alias %s@." tn;
              {sc_loc with aliases = SMap.remove tn sc_loc.aliases}
            )
      )
    in
    SMap.fold
      (fun typename ((tkind, _, _),_) sc_local ->
         match tkind with
         | TPublic | TPrivate ->
           debug "[add_subcontract_env] %s can remain@." typename; sc_local
         | TAbstract -> debug "[add_subcontract_env] %s is abstract@." typename;
           remove_type typename sc_local
         | TInternal -> debug "[add_subcontract_env] %s is internal@." typename;
           let sc_local = remove_type typename sc_local in
           {sc_local with type_kind = SMap.remove typename sc_local.type_kind}
      )
      new_sc_local.type_kind
      new_sc_local in
  (* Now removing values with internal type *)
  let new_sc_local =
    let new_values =
      SMap.filter
        (fun _ ((vkind,_),_) ->
           match vkind with
             Internal | Value Private | Init Private -> false
           | Value _ | View _ | Entry | Primitive _ | Init Public -> true
        ) new_sc_local.var_typ
    in {new_sc_local with var_typ = new_values}
  in
  let res =
    {env with
     sc_local =
       {sc_local
        with
         contract_types =
           SMap.add
             sc
             {
               sc_parent = SubStructure (sc, env);
               sc_local = new_sc_local;

             }
             sc_local.contract_types;
         all_exceptions = new_sc_local.all_exceptions
       };
    }
  in
  debug
    "[add_subcontract_env] Adding structure %s with timestamp %i"
    sc
    (SMap.get_timestamp ()); res

let get_sumtypedef clist =
  let scons,sparams =
    List.fold_left
      (fun (acc_cons, acc_vars) tc ->
         let vars =
           List.fold_left
             (fun acc t ->
                Love_type.fvars t |> TVSet.union acc)
             acc_vars
             tc.cargs
         in
         (tc.cname, tc.cargs) :: acc_cons, vars
      )
      ([], TVSet.empty)
      clist
  in
  let srec =
    match clist with
      {crec; _}  :: _ -> crec
    | _ -> assert false
  in
  SumType
    {sparams = TVSet.elements sparams;
     scons;
     srec}

let get_rectypedef flist =
  let rfields,rparams =
    List.fold_left
      (fun (acc_cons, acc_vars) tf ->
         let vars = Love_type.fvars tf.ftyp |> TVSet.union acc_vars
         in
         (tf.fname, tf.ftyp) :: acc_cons, vars
      )
      ([], TVSet.empty)
      flist
  in
  let rrec =
    match flist with
      {frec; _}  :: _ -> frec
    | _ -> assert false
  in
  RecordType
    {rparams = TVSet.elements rparams;
     rfields;
     rrec}

let _find_tdef name env : (TYPE.typedef * int) option =
  debug "[find_tdef] Searching definition of %s@." name;
  match SMap.find_opt name env.sc_local.record_typ with
    Some (r,i) -> Some (get_rectypedef r, i)
  | None ->
    debug "[find_tdef] Not a record@.";
    match SMap.find_opt name env.sc_local.sum_typ with
      Some (s,i) -> Some (get_sumtypedef s, i)
    | None ->
    debug "[find_tdef] Not a sum@.";
    match SMap.find_opt name env.sc_local.aliases with
      Some ({aparams; atype; _},i) ->
      debug "[find_tdef] Alias of %a@." Love_type.pretty atype;
      Some (Alias {aparams; atype}, i)
    | None -> debug "[find_tdef] Not an alias either@."; None

let find_tdef typename env =
  find_env typename env _find_tdef

let rec env_to_contract_sig (env : t) =
  let open Collections in
  let map_content =
    SMap.fold
      (fun name ((k, t), id) (acc : (string * sig_content) IntMap.t) ->
         match k, t with
           Entry, TUser (LName "entrypoint", [t]) ->
           IntMap.add id (name,(SEntry t)) acc
         | View _, TUser (LName "view", [t]) ->
           IntMap.add id (name,(SView t)) acc
         | Init Public, (TArrow (t,_)) -> IntMap.add id (name,(SInit t)) acc
         | Init Private, _ -> acc
         | Value Public, t
         | Primitive _, t -> IntMap.add id (name,(SValue t)) acc
         | (Value _ | Internal), _ -> acc
         | (Entry | View _ | Init _), _ ->
           raise (EnvironmentError "Entry/View/Init registered with invalid type")
      )
      env.sc_local.var_typ
      IntMap.empty
  in
  debug "[env_to_contract_sig] Adding Anonymous substructures";
  let map_content =
    SMap.fold
      (fun name (elt, id) acc ->
         debug "[env_to_contract_sig] Adding %s" name;
         IntMap.add id (name, (SStructure (Anonymous (env_to_contract_sig elt)))) acc
         )
      env.sc_local.contract_types
      map_content in
  debug "[env_to_contract_sig] Adding Signatures";
  let map_content =
    SMap.fold
      (fun name (e,id) acc ->
         debug "[env_to_contract_sig] Adding %s" name;
         IntMap.add id (name, SSignature (env_to_contract_sig e)) acc)
      env.sc_local.signature_types
      map_content
  in
  let map_content =
    if Love_pervasives.has_protocol_revision 3 then begin
      SMap.fold
        (fun name ((tkind, tparam, _), id) acc ->
           debug "[env_to_contract_sig] Adding type %s" name;
           match tkind with
             TAbstract -> IntMap.add id (name, SType (SAbstract tparam)) acc
           | TInternal -> acc
           | TPublic ->
               let typedef =
                 match find_tdef (Ident.create_id name) env with
                   None -> raise (EnvironmentError ("Public type " ^ name ^ " has no definition"))
                 | Some {result; _} -> result
               in
               IntMap.add id (name, SType (SPublic typedef)) acc
           | TPrivate ->
               let typedef =
                 match find_tdef (Ident.create_id name) env with
                   None -> raise (EnvironmentError ("Private type " ^ name ^ " has no definition"))
                 | Some {result; _} -> result
               in
               IntMap.add id (name, SType (SPrivate typedef)) acc
        )
        env.sc_local.type_kind
        map_content
    end else begin
      debug "[env_to_contract_sig] Adding Sum types";
      let map_content =
        SMap.fold
          (fun name (t, id) acc ->
             debug "[env_to_contract_sig] Adding sum type %s" name;
             match SMap.find_opt name env.sc_local.type_kind with
               None -> raise (EnvironmentError (
                 "SumType " ^ name ^ " has no registered kind."))
             | Some ((TAbstract,l,_), _) -> IntMap.add id (name,SType (SAbstract l)) acc
             | Some ((TPublic, _,_), _) ->
                 let td = get_sumtypedef t in IntMap.add id (name, SType (SPublic td)) acc
             | Some ((TPrivate, _,_), _) ->
                 let td = get_sumtypedef t in IntMap.add id (name, SType (SPrivate td)) acc
             | Some ((TInternal, _,_), _) -> acc
          )
          env.sc_local.sum_typ
          map_content
      in
      let map_content =
        SMap.fold
          (fun name (t, id) acc ->
             debug "[env_to_contract_sig] Adding record type %s" name;
             match SMap.find_opt name env.sc_local.type_kind with
               None -> raise (EnvironmentError (
                 "RecType " ^ name ^ " has no registered kind."))
             | Some ((TAbstract,l,_), _) -> IntMap.add id (name,SType (SAbstract l)) acc
             | Some ((TPublic, _,_), _) ->
                 let td = get_rectypedef t in IntMap.add id (name, SType (SPublic td)) acc
             | Some ((TPrivate, _,_), _) ->
                 let td = get_rectypedef t in IntMap.add id (name, SType (SPrivate td)) acc
             | Some ((TInternal, _,_), _) -> acc
          )
          env.sc_local.record_typ
          map_content
      in
      let map_content =
        SMap.fold
          (fun name ({aparams; atype; _}, id) acc ->
             debug "[env_to_contract_sig] Adding alias %s" name;
             match SMap.find_opt name env.sc_local.type_kind with
               None -> raise (EnvironmentError (
                 "Alias " ^ name ^ " has no registered kind."))
             | Some ((TAbstract,l,_), _) -> IntMap.add id (name,SType (SAbstract l)) acc
             | Some ((TPublic, _,_), _) ->
                 let td = Alias {aparams; atype} in IntMap.add id (name, SType (SPublic td)) acc
             | Some ((TPrivate, _,_), _) ->
                 let td = Alias {aparams; atype} in IntMap.add id (name, SType (SPrivate td)) acc
             | Some ((TInternal, _,_), _) -> acc
          )
          env.sc_local.aliases
          map_content
      in map_content
    end
  in
  let map_content =
    SMap.fold
      (fun name (l, id) acc -> IntMap.add id (name, SException l) acc)
      env.sc_local.exception_typ
      map_content
  in
  let sig_content =
    List.map
      (fun (_,e) -> e)
      (IntMap.bindings map_content)
  in
  {
    sig_content;
    sig_kind =
      if env.sc_local.is_module then Module else Contract (env.sc_local.dependencies);
  }

let copy_env name to_copy_env env =
  contract_sig_to_env (Some name) (env_to_contract_sig to_copy_env) env

(*
  let sc_local = current_env.sc_local in
  let fresh = {
    sc_local = {
      is_module = sc_local.is_module;
      dependencies = sc_local.dependencies;
      var_typ = SMap.empty;
      sum_typ = SMap.empty;
      constr_typ = SMap.empty;
      record_typ = SMap.empty;
      field_typ = SMap.empty;
      forall_typ = TVSet.empty;
      signature_types = SMap.empty;
      contract_types = SMap.empty;
      exception_typ = SMap.empty;
      type_kind = SMap.empty;
      all_exceptions = StringSet.singleton "Failure";
      aliases = SMap.empty;
    };
    sc_parent
  }
  in
  let var_list = SMap.elements_sorted_by_timestamp sc_local.var_typ in
  let sum_typ_list = SMap.elements_sorted_by_timestamp sc_local.sum_typ in
  let constr_typ_list = SMap.elements_sorted_by_timestamp sc_local.constr_typ in
  let record_typ_list = SMap.elements_sorted_by_timestamp sc_local.record_typ in
  let field_typ_list = SMap.elements_sorted_by_timestamp sc_local.field_typ in
  let forall_typ_list = SMap.elements_sorted_by_timestamp sc_local.forall_typ in
  let signature_typ_list = SMap.elements_sorted_by_timestamp sc_local.signature_typ in
  let contract_typ_list = SMap.elements_sorted_by_timestamp sc_local.contract_typ in
  let exception_typ_list = SMap.elements_sorted_by_timestamp sc_local.exception_typ in
  let type_kind_list = SMap.elements_sorted_by_timestamp sc_local.type_kind in
  let sum_typ_list = SMap.elements_sorted_by_timestamp sc_local.sum_typ in
  let sum_typ_list = SMap.elements_sorted_by_timestamp sc_local.sum_typ in
  let fresh_with_sigs =
    SMap.fold
      (fun name (sign, _) acc ->
         debug "[copy_env] Copying signature %s@." name;
         let copy = copy_env (SubStructure (name, acc)) sign in
         add_signature name copy acc
      )
      sc_local.signature_types
      fresh
  in
  let fresh_with_sigs_and_contracts =
    SMap.fold
      (fun name (str, _) acc ->
         debug "[copy_env] Copying structure %s@." name;
         let copy = copy_env (SubStructure (name, acc)) str in
         add_subcontract_env name copy acc
      )
      sc_local.contract_types
      fresh_with_sigs
  in fresh_with_sigs_and_contracts *)

let add_signed_subcontract str sig_name env =
  match find_signature sig_name env with
    None ->
    raise (
      EnvironmentError
        "Trying to register a signed subcontract with a non existing signature name."
    )
  | Some {result; _} ->
    let sig_copy = copy_env str result env in
    add_subcontract_env str sig_copy env


let add_typedef_in_subcontract id k (td : typedef) env =
  let rec add_submodules id env : t =
    match Ident.split id with
      mname, Some path ->
      let sub_env =
        match SMap.find_opt mname env.sc_local.contract_types with
          None -> new_subcontract_env mname Module env
        | Some (e, _) -> e
      in
      let res_env = add_submodules path sub_env in
      let sc_local = env.sc_local in
      let new_sc_local =
        {sc_local with
         contract_types = SMap.add_unique mname res_env env.sc_local.contract_types}
      in {env with sc_local = new_sc_local}
    | tname, None -> add_typedef tname k td env
  in
  add_submodules id env

let alias ?before name args env =
  debug "[alias] Searching for %a@." Ident.print_strident name;
  find_env ?before name env (fun s env -> _alias env s args)

let field_applied_types name params env =
  let rec_def =
    match find_record name env with
    | None ->
        env_error
          "Record type %a is unknown"
          print_strident name
    | Some t -> t.result
  in
  let poly_args =
    match rec_def with
      [] -> raise (Exceptions.InvariantBroken "Empty record type")
    | {fparent = TUser (_, p); _} :: _ -> p
    | _ -> raise (Exceptions.InvariantBroken "Invalid record type")
  in
  let poly_args_to_arg_map = try
    List.fold_left2
      (fun acc parg arg ->
         match parg with
           TVar tv -> TypeVarMap.add tv arg acc
         | _ ->  raise (Exceptions.InvariantBroken "Record type def should be polymorphic")
      )
      TypeVarMap.empty
      poly_args
      params
    with Failure _ -> raise (
        Exceptions.InvariantBroken "Record type should define at least 1 field")
  in
  let replace = Love_type_utils.replace_map (fun x -> isComp x env) poly_args_to_arg_map in
  List.map
    (fun f -> {f with ftyp = replace f.ftyp})
    rec_def

let is_abstract name env =
  match find_type_kind name env with
    Some {result = (TAbstract, _, _); _} -> true
  | _ -> false

let get_storage_type env =
  match find_tdef (Ident.create_id "storage") env
  with
    None -> None
  | Some {result; _} -> Some (Love_type.type_of_typedef "storage" result)

(** Given a list of field * type and a user type, returns the user type on which the correct
    type arguments are given
    Ex : [a, int; b, float] {a : 'a; b : 'b} = {a : int, b : float}
    Fails if the list of fields is different than the one registered in the environment
 *)
let record_type_new path (l : (field_name * TYPE.t) list) (env : t) : TYPE.t =
  let sorted_l = List.sort (fun (field, _) (field', _) -> String.compare field field') l
  in
  let find_tdef e tn =
    match find_tdef tn e with
      None -> raise (EnvironmentError "Unknown typedef")
    | Some {result; _} -> result in
  let search_aliases env t1 t2 =
    let nt1 = normalize_type ~relative:false t1 env in
    let nt2 = normalize_type ~relative:false t2 env in
    try
      search_aliases
        (fun x -> isComp x env)
        (find_tdef env)
        nt1
        nt2
    with
      Exceptions.BadArgument _ ->
        let error_msg =
          Format.asprintf
            "Type %a and type %a are incompatible"
            Love_type.pretty nt1 Love_type.pretty nt2 in
        raise (EnvironmentError error_msg)
  in
  let (field,typ) =
    match l with
      hd :: _ -> hd
    | [] -> raise (EnvironmentError "Record should have at least 1 element")
  in
  let env =
    match path with
      None -> env
    | Some i ->
        begin
          match
            get_env_of_id
              Ident.(concat i (create_id "__tmp"))
              env
          with
            None -> raise (EnvironmentError "No environment registered for record path")
          | Some (e,_ )-> e
        end
  in
  let typename,field_typ, parent =
    match find_field field env with
      None -> raise (
        EnvironmentError (
          Format.asprintf "Unknown field %s" field))
    | Some {
        result = {fparent = TUser (name, _) as parent; ftyp; _};
        _ } ->
      name, ftyp, parent
    | Some _ -> raise (
        EnvironmentError (
          Format.asprintf "Field %s associated to a non User type" field))
  in
  debug "[record_type] 1.Searching aliases between %a and %a"
    Love_type.pretty field_typ
    Love_type.pretty typ;
  let reg_fields =
    match find_record typename env with
      None -> raise @@ EnvironmentError (Format.asprintf "Cannot find record type %a" Ident.print_strident typename)
    | Some {result; _} -> result in
  let sorted_reg_fields =
    List.sort
      (fun {fname = n1; _} {fname = n2; _} -> String.compare n1 n2)
      reg_fields
  in
  let all_aliases =
    let rec compare_lists acc l reg_l =
        match l, reg_l with
      | [], [] -> acc
      | (f, _) :: _, [] ->
        raise (
          EnvironmentError (
            Format.asprintf "Incorrect number of fields in record : %s is unknown" f))
      | [], {fname; _} :: _ ->
        raise (
          EnvironmentError (
            Format.asprintf "Incorrect number of fields in record : %s is not defined" fname))
      | (f, typ) :: tl, {fname; ftyp; _} :: reg_tl -> (
         if String.equal f fname then
           let () = debug "[record_type] 2.Searching aliases between %a and %a"
                        Love_type.pretty ftyp
                        Love_type.pretty typ
           in
           let acc =
             let current_aliases =
               search_aliases
                 env
                 ftyp
                 typ in
             TypeVarMap.merge
                (fun _tv b1 b2 ->
                   match b1, b2 with
                       None, None -> None
                     | None, Some b
                     | Some b, None -> Some b
                     | Some b1, Some b2 ->
                         if Love_type.bool_equal b1 b2 then Some b1
                         else raise (EnvironmentError "Types are incompatible")
                )
                current_aliases
                acc
           in
           compare_lists acc tl reg_tl
         else
           raise (EnvironmentError (
               Format.asprintf "Incorrect field in record : %s does not belong to type %a"
                 fname
                 Love_type.pretty ftyp
             )
             )
       )
    in
    compare_lists TypeVarMap.empty sorted_l sorted_reg_fields
  in
  let t = Love_type_utils.replace_map (fun x -> isComp x env) all_aliases parent in
  match path, t with
    None,_ -> t
  | Some p, TUser (name, par) -> TUser (Ident.concat p name, par)
  | _,_ -> assert false

let record_type_old path (l : (field_name * TYPE.t) list) (env : t) : TYPE.t =
  let find_tdef e tn =
    match find_tdef tn e with
      None -> raise (EnvironmentError "Unknown typedef")
    | Some {result; _} -> result in
  let search_aliases env t1 t2 =
    search_aliases
      (fun x -> isComp x env)
      (find_tdef env)
      (normalize_type ~relative:false t1 env)
      (normalize_type ~relative:false t2 env) in
  let (field,typ), tl =
    match l with
      hd :: tl -> hd, tl
    | [] -> assert false
  in
  let env =
    match path with
      None -> env
    | Some i -> (
        match
          get_env_of_id
            Ident.(concat i (create_id "__tmp"))
            env
        with
          None -> raise (EnvironmentError "No environment registered for record path")
        | Some (e,_ )-> e
      )
  in
  let typename,field_typ, parent =
    match find_field field env with
      None -> raise (
        EnvironmentError (
          Format.asprintf "Unknown field %s" field))
    | Some {
        result = {fparent = TUser (name, _) as parent; ftyp; _};
        _ } ->
      name, ftyp, parent
    | Some _ -> raise (
        EnvironmentError (
          Format.asprintf "Field %s associated to a non User type" field))
  in
  debug "[record_type] 1.Searching aliases between %a and %a"
    Love_type.pretty field_typ
    Love_type.pretty typ;
  let hd_aliases =
    search_aliases
      env
      field_typ
      typ
  in
  let all_aliases =
    List.fold_left
      (fun acc (field,typ) ->
         let fty =
           match find_field field env with
             None -> raise (
               EnvironmentError (
                 Format.asprintf "Unknown field %s" field))
           | Some {result = {fparent = TUser (n,_); ftyp; _}; _}
             when Love_type.equal_tname n typename -> ftyp
           | Some _ ->
             raise (EnvironmentError (
                 Format.asprintf "Field %s associated to a non User type" field))
         in
         debug "[record_type] 2.Searching aliases between %a and %a"
           Love_type.pretty fty
           Love_type.pretty typ;
         let current_aliases =
           search_aliases
             env
             fty
             typ in
         TypeVarMap.merge
           (fun _tv b1 b2 ->
              match b1, b2 with
                None, None -> None
              | None, Some b
              | Some b, None -> Some b
              | Some b1, Some b2 ->
                if Love_type.bool_equal b1 b2 then Some b1
                else raise (EnvironmentError "Types are incompatible")
           )
           current_aliases
           acc
      )
      hd_aliases
      tl
  in
  let t = Love_type_utils.replace_map (fun x -> isComp x env) all_aliases parent in
  match path, t with
    None,_ -> t
  | Some p, TUser (name, par) -> TUser (Ident.concat p name, par)
  | _,_ -> assert false

let record_type path (l : (field_name * TYPE.t) list) (env : t) : TYPE.t =
  if Love_pervasives.has_protocol_revision 3 then
    record_type_new path l env
  else record_type_old path l env

let constr_type (name, args) env : TYPE.t =
  let find_tdef e tn =
    match find_tdef tn e with
      None -> raise (EnvironmentError "Unknown typedef")
    | Some {result; _} -> result in
  let poly_args, parent =
    match find_constr name env with
      None -> raise (EnvironmentError (Format.asprintf "Constructor %a is unknown" Ident.print_strident name))
    | Some {result = t; _} -> t.cargs, t.cparent
  in
  let args, poly_args =
    let asize, psize = List.length args, List.length poly_args in
    match asize, psize with
      1, 1 -> args, poly_args
    | _, 1 -> [TTuple args], poly_args
    | a, b when Compare.Int.(=) a b -> args, poly_args
    | _,_ ->
      raise (EnvironmentError "Constructor has an incorrect number of args")
  in
  let aliases =
    List.fold_left2
      (fun acc poly arg ->
         let current_aliases =
         debug "[constr_type] Searching aliases between %a and %a"
           Love_type.pretty poly
           Love_type.pretty arg;
         search_aliases
           (fun x -> isComp x env)
           (find_tdef env)
           poly
           arg in
         TypeVarMap.merge
           (fun _tv b1 b2 ->
              match b1, b2 with
                None, None -> None
              | None, Some b
              | Some b, None -> Some b
              | Some b1, Some b2 ->
                if Love_type.bool_equal b1 b2 then Some b1
                else raise (EnvironmentError "Types are incompatible")
           )
           current_aliases
           acc
      )
      TypeVarMap.empty
      poly_args
      args
  in
  let t = replace_map (fun x -> isComp x env) aliases parent in
  match t with
    TUser (tname, p) -> TUser (Ident.change_last name tname, p) | _ -> assert false

let find_var :
  AST.var_ident ->
  t ->
  (val_kind * TYPE.t) env_request =
  fun (x : string Ident.t) (env : t) ->
  let search =
    fun name env -> SMap.find_opt name env.sc_local.var_typ
  in
  match find_env x env search with
    None -> None

  | Some {result = (c, t); path; last_env; index; in_core_env} ->
    debug "[find_var] Found variable %a : %a%s@."
      Ident.print_strident x
      Love_type.pretty t
      (if in_core_env then " in Core" else "")
    ;
    let new_type =
      match c with
        Primitive _ -> t
      (* In this case, the type is returned by the content of Primitive.
         The type t is dummy. *)
      | View Local ->
          if Love_pervasives.has_protocol_revision 3 then begin
            match t with
              TUser (LName "view", [fty]) ->
                debug "[find_var] View type is %a" Love_type.pretty fty;
                Love_type.viewType
                  ~type_storage:(TUser (Ident.create_id "storage", []))
                  (put_type_in_correct_namespace last_env path fty)
            | _ ->
                raise (
                  Exceptions.InvariantBroken
                    (Format.asprintf
                       "Local view should have type view, but it has type %a"
                       Love_type.pretty t))
          end else put_type_in_correct_namespace last_env path t
      | _ -> put_type_in_correct_namespace last_env path t
    in
    debug "[find_var] Type of variable %a : %a@."
      Ident.print_strident x
      Love_type.pretty new_type;
    Some {result = (c, new_type); path; last_env; index; in_core_env}

let rec is_internal t env =
  let test t = is_internal t env in
  match t with
  | TVar _
  | TContractInstance _
  | TPackedStructure _ -> false

  | TArrow (t,t') -> test t || test t'

  | TTuple l ->
    List.exists test l

  | TUser (name,_) -> (
      match find_type_kind name env with
        None -> raise (EnvironmentError "Type undefined")
      | Some {result = TInternal, _, _; _} -> true
      | _ -> false
    )
  | TForall (_, t) -> test t

let add_var_path id t env = let idl = Ident.get_list id in
  let rec aux env = function
    | [] -> env
    | [id] -> add_var id t env
    | id :: idl ->
       let sub_env = match SMap.find_opt id env.sc_local.contract_types with
         | Some (e, _) -> e
         | None -> new_subcontract_env "__unknown_structure" Module env
       in
       let sub_env = aux sub_env idl in
       add_subcontract_env id sub_env env
  in
  aux env idl

let get_subenv_struct str env =
  match SMap.find_opt str env.sc_local.contract_types with
    None -> None
  | Some (e, _) -> Some e

let get_subenv_sig str env =
  match SMap.find_opt str env.sc_local.signature_types with
    None -> None
  | Some (e, _) -> Some e

(** Core environment *)

let init_core_env () =

  core_env := (empty (Contract []) ());

  (* Initializing types *)
  core_env := (
  Love_type_list.fold
    (fun name ->
       function
         {ct_typedef = Love_type_list.Core (params, traits); _} ->
         fun env ->
           let sc_local = env.sc_local in
           {env with
            sc_local = {
              sc_local with
              type_kind =
                SMap.add_unique
                  ~timestamp:0
                  name
                  (TAbstract, params, traits)
                  sc_local.type_kind
            }
           }
       | {ct_typedef = TypeDef td; _} -> add_typedef name TPublic td
    )
    !core_env
  );
  (* Initializing primitives *)
  let add_prim env (name, p : string * Love_primitive.generic_primitive) =
    match p.prim_ext_arg with
      ADNone -> add_var name ~kind:(Primitive p) (p.prim_type ANone) env
    | ADContractType ->
      add_var
        name
        ~kind:(Primitive p)
        (TUser (Ident.create_id "__special_type", []))
        env
  in
  let add_prim_list env = List.fold_left add_prim env in
  let add_module env (mod_name, prim_list) =
    let subenv = new_subcontract_env mod_name Module env in
    let subenv = add_prim_list subenv prim_list in
    add_subcontract_env mod_name subenv env
  in
  let core, mods = Love_primitive.get_primitives_as_modules () in
  core_env := (
    let e = !core_env in
    let e = add_prim_list e core in
    let e =
      List.fold_left
      add_module
      e
      mods
    in
    add_signature
      "UnitContract"
      ({
        sc_local = empty_env (Contract []);
        sc_parent = Root
      }
        |> add_var
          ~kind:Entry
          "default"
          (Love_type_list.(get_type "entrypoint" [get_type "unit" []]))
      ) e
  )
