

;
;        /!\ /!\ Do not modify this file /!\ /!\
;
; but the original template in `tezos-protocol-compiler`
;


(rule
 (targets environment.ml)
 (action
  (write-file %{targets}
              "module Name = struct let name = \"005-PsBabyM1\" end
include Tezos_protocol_environment.MakeV1(Name)()
module CamlinternalFormatBasics = struct include CamlinternalFormatBasics end
")))

(rule
 (targets registerer.ml)
 (deps config.mli config.ml dune_protocol_parameters.ml dune_misc.mli dune_misc.ml misc.mli misc.ml storage_description.mli storage_description.ml state_hash.ml nonce_hash.ml script_expr_hash.ml contract_hash.ml blinded_public_key_hash.mli blinded_public_key_hash.ml qty_repr.ml tez_repr.mli tez_repr.ml period_repr.mli period_repr.ml time_repr.mli time_repr.ml constants_repr.ml fitness_repr.ml raw_level_repr.mli raw_level_repr.ml voting_period_repr.mli voting_period_repr.ml cycle_repr.mli cycle_repr.ml level_repr.mli level_repr.ml seed_repr.mli seed_repr.ml gas_limit_repr.mli gas_limit_repr.ml script_int_repr.mli script_int_repr.ml script_timestamp_repr.mli script_timestamp_repr.ml michelson_v1_primitives.mli michelson_v1_primitives.ml script_repr.mli script_repr.ml contract_repr.mli contract_repr.ml roll_repr.mli roll_repr.ml vote_repr.mli vote_repr.ml block_header_repr.mli block_header_repr.ml dune_script_sig.ml lang1_script_repr.ml dune_script_registration.mli dune_script_registration.ml lang1_repr.ml love_pervasives.mli love_pervasives.ml love_type.mli love_type.ml love_type_list.mli love_type_list.ml love_primitive.mli love_primitive.ml love_ast.ml love_runtime_ast.ml love_value.ml love_printer.ml love_encoding_sig.ml love_json_encoding.mli love_json_encoding.ml love_binary_buffer.mli love_binary_buffer.ml love_binary_processor.ml love_binary_encoding.mli love_binary_encoding.ml love_encoding.mli love_encoding.ml love_size.ml love_script_repr.mli love_script_repr.ml love_repr.ml dune_script_repr.mli dune_script_repr.ml commitment_repr.mli commitment_repr.ml dune_parameters_repr.mli dune_parameters_repr.ml dune_operation_repr.mli dune_operation_repr.ml dune_lang_repr.ml legacy_script_support_repr.mli legacy_script_support_repr.ml operation_repr.mli operation_repr.ml manager_repr.mli manager_repr.ml parameters_repr.mli parameters_repr.ml raw_context.mli raw_context.ml storage_sigs.ml storage_functors.mli storage_functors.ml storage.mli storage.ml dune_storage.mli dune_storage.ml constants_storage.ml level_storage.mli level_storage.ml nonce_storage.mli nonce_storage.ml seed_storage.mli seed_storage.ml roll_storage.mli roll_storage.ml delegate_storage.mli delegate_storage.ml contract_storage.mli contract_storage.ml bootstrap_storage.mli bootstrap_storage.ml fitness_storage.ml vote_storage.mli vote_storage.ml commitment_storage.mli commitment_storage.ml dune_actions.mli dune_actions.ml init_storage.ml fees_storage.mli fees_storage.ml alpha_context.mli alpha_context.ml script_typed_ir.ml script_tc_errors.ml michelson_v1_gas.mli michelson_v1_gas.ml script_ir_annot.mli script_ir_annot.ml script_ir_translator.mli script_ir_translator.ml script_tc_errors_registration.ml script_interpreter.mli script_interpreter.ml baking.mli baking.ml amendment.mli amendment.ml dune_apply_results.mli dune_apply_results.ml apply_results.mli apply_results.ml dune_script_interpreter_sig.ml lang1_interpreter.ml dune_script_interpreter_registration.mli dune_script_interpreter_registration.ml lang1_script.ml love_gas.ml love_michelson.mli love_michelson.ml love_tenv.mli love_tenv.ml love_context.ml love_free_vars.ml love_env.ml love_translator.ml love_typechecker.mli love_typechecker.ml love_errors.ml love_prim_interp.mli love_prim_interp.ml love_prim_list.mli love_prim_list.ml love_interpreter.mli love_interpreter.ml love_interpreter_interface.ml love_script.ml dune_script_interpreter.mli dune_script_interpreter.ml dune_apply.mli dune_apply.ml apply.ml services_registration.ml constants_services.mli constants_services.ml contract_services.mli contract_services.ml delegate_services.mli delegate_services.ml helpers_services.mli helpers_services.ml voting_services.mli voting_services.ml dune_services.mli dune_services.ml alpha_services.mli alpha_services.ml main.mli main.ml
       (:src_dir TEZOS_PROTOCOL))
 (action
  (with-stdout-to %{targets}
                  (chdir %{workspace_root} (run %{bin:tezos-embedded-protocol-packer} "%{src_dir}" "005_PsBabyM1")))))

(rule
 (targets functor.ml)
 (deps config.mli config.ml dune_protocol_parameters.ml dune_misc.mli dune_misc.ml misc.mli misc.ml storage_description.mli storage_description.ml state_hash.ml nonce_hash.ml script_expr_hash.ml contract_hash.ml blinded_public_key_hash.mli blinded_public_key_hash.ml qty_repr.ml tez_repr.mli tez_repr.ml period_repr.mli period_repr.ml time_repr.mli time_repr.ml constants_repr.ml fitness_repr.ml raw_level_repr.mli raw_level_repr.ml voting_period_repr.mli voting_period_repr.ml cycle_repr.mli cycle_repr.ml level_repr.mli level_repr.ml seed_repr.mli seed_repr.ml gas_limit_repr.mli gas_limit_repr.ml script_int_repr.mli script_int_repr.ml script_timestamp_repr.mli script_timestamp_repr.ml michelson_v1_primitives.mli michelson_v1_primitives.ml script_repr.mli script_repr.ml contract_repr.mli contract_repr.ml roll_repr.mli roll_repr.ml vote_repr.mli vote_repr.ml block_header_repr.mli block_header_repr.ml dune_script_sig.ml lang1_script_repr.ml dune_script_registration.mli dune_script_registration.ml lang1_repr.ml love_pervasives.mli love_pervasives.ml love_type.mli love_type.ml love_type_list.mli love_type_list.ml love_primitive.mli love_primitive.ml love_ast.ml love_runtime_ast.ml love_value.ml love_printer.ml love_encoding_sig.ml love_json_encoding.mli love_json_encoding.ml love_binary_buffer.mli love_binary_buffer.ml love_binary_processor.ml love_binary_encoding.mli love_binary_encoding.ml love_encoding.mli love_encoding.ml love_size.ml love_script_repr.mli love_script_repr.ml love_repr.ml dune_script_repr.mli dune_script_repr.ml commitment_repr.mli commitment_repr.ml dune_parameters_repr.mli dune_parameters_repr.ml dune_operation_repr.mli dune_operation_repr.ml dune_lang_repr.ml legacy_script_support_repr.mli legacy_script_support_repr.ml operation_repr.mli operation_repr.ml manager_repr.mli manager_repr.ml parameters_repr.mli parameters_repr.ml raw_context.mli raw_context.ml storage_sigs.ml storage_functors.mli storage_functors.ml storage.mli storage.ml dune_storage.mli dune_storage.ml constants_storage.ml level_storage.mli level_storage.ml nonce_storage.mli nonce_storage.ml seed_storage.mli seed_storage.ml roll_storage.mli roll_storage.ml delegate_storage.mli delegate_storage.ml contract_storage.mli contract_storage.ml bootstrap_storage.mli bootstrap_storage.ml fitness_storage.ml vote_storage.mli vote_storage.ml commitment_storage.mli commitment_storage.ml dune_actions.mli dune_actions.ml init_storage.ml fees_storage.mli fees_storage.ml alpha_context.mli alpha_context.ml script_typed_ir.ml script_tc_errors.ml michelson_v1_gas.mli michelson_v1_gas.ml script_ir_annot.mli script_ir_annot.ml script_ir_translator.mli script_ir_translator.ml script_tc_errors_registration.ml script_interpreter.mli script_interpreter.ml baking.mli baking.ml amendment.mli amendment.ml dune_apply_results.mli dune_apply_results.ml apply_results.mli apply_results.ml dune_script_interpreter_sig.ml lang1_interpreter.ml dune_script_interpreter_registration.mli dune_script_interpreter_registration.ml lang1_script.ml love_gas.ml love_michelson.mli love_michelson.ml love_tenv.mli love_tenv.ml love_context.ml love_free_vars.ml love_env.ml love_translator.ml love_typechecker.mli love_typechecker.ml love_errors.ml love_prim_interp.mli love_prim_interp.ml love_prim_list.mli love_prim_list.ml love_interpreter.mli love_interpreter.ml love_interpreter_interface.ml love_script.ml dune_script_interpreter.mli dune_script_interpreter.ml dune_apply.mli dune_apply.ml apply.ml services_registration.ml constants_services.mli constants_services.ml contract_services.mli contract_services.ml delegate_services.mli delegate_services.ml helpers_services.mli helpers_services.ml voting_services.mli voting_services.ml dune_services.mli dune_services.ml alpha_services.mli alpha_services.ml main.mli main.ml
       (:src_dir TEZOS_PROTOCOL))
 (action (with-stdout-to %{targets}
                         (chdir %{workspace_root}
                                (run %{bin:tezos-protocol-compiler.tezos-protocol-packer} %{src_dir})))))

(rule
 (targets protocol.ml)
 (deps config.mli config.ml dune_protocol_parameters.ml dune_misc.mli dune_misc.ml misc.mli misc.ml storage_description.mli storage_description.ml state_hash.ml nonce_hash.ml script_expr_hash.ml contract_hash.ml blinded_public_key_hash.mli blinded_public_key_hash.ml qty_repr.ml tez_repr.mli tez_repr.ml period_repr.mli period_repr.ml time_repr.mli time_repr.ml constants_repr.ml fitness_repr.ml raw_level_repr.mli raw_level_repr.ml voting_period_repr.mli voting_period_repr.ml cycle_repr.mli cycle_repr.ml level_repr.mli level_repr.ml seed_repr.mli seed_repr.ml gas_limit_repr.mli gas_limit_repr.ml script_int_repr.mli script_int_repr.ml script_timestamp_repr.mli script_timestamp_repr.ml michelson_v1_primitives.mli michelson_v1_primitives.ml script_repr.mli script_repr.ml contract_repr.mli contract_repr.ml roll_repr.mli roll_repr.ml vote_repr.mli vote_repr.ml block_header_repr.mli block_header_repr.ml dune_script_sig.ml lang1_script_repr.ml dune_script_registration.mli dune_script_registration.ml lang1_repr.ml love_pervasives.mli love_pervasives.ml love_type.mli love_type.ml love_type_list.mli love_type_list.ml love_primitive.mli love_primitive.ml love_ast.ml love_runtime_ast.ml love_value.ml love_printer.ml love_encoding_sig.ml love_json_encoding.mli love_json_encoding.ml love_binary_buffer.mli love_binary_buffer.ml love_binary_processor.ml love_binary_encoding.mli love_binary_encoding.ml love_encoding.mli love_encoding.ml love_size.ml love_script_repr.mli love_script_repr.ml love_repr.ml dune_script_repr.mli dune_script_repr.ml commitment_repr.mli commitment_repr.ml dune_parameters_repr.mli dune_parameters_repr.ml dune_operation_repr.mli dune_operation_repr.ml dune_lang_repr.ml legacy_script_support_repr.mli legacy_script_support_repr.ml operation_repr.mli operation_repr.ml manager_repr.mli manager_repr.ml parameters_repr.mli parameters_repr.ml raw_context.mli raw_context.ml storage_sigs.ml storage_functors.mli storage_functors.ml storage.mli storage.ml dune_storage.mli dune_storage.ml constants_storage.ml level_storage.mli level_storage.ml nonce_storage.mli nonce_storage.ml seed_storage.mli seed_storage.ml roll_storage.mli roll_storage.ml delegate_storage.mli delegate_storage.ml contract_storage.mli contract_storage.ml bootstrap_storage.mli bootstrap_storage.ml fitness_storage.ml vote_storage.mli vote_storage.ml commitment_storage.mli commitment_storage.ml dune_actions.mli dune_actions.ml init_storage.ml fees_storage.mli fees_storage.ml alpha_context.mli alpha_context.ml script_typed_ir.ml script_tc_errors.ml michelson_v1_gas.mli michelson_v1_gas.ml script_ir_annot.mli script_ir_annot.ml script_ir_translator.mli script_ir_translator.ml script_tc_errors_registration.ml script_interpreter.mli script_interpreter.ml baking.mli baking.ml amendment.mli amendment.ml dune_apply_results.mli dune_apply_results.ml apply_results.mli apply_results.ml dune_script_interpreter_sig.ml lang1_interpreter.ml dune_script_interpreter_registration.mli dune_script_interpreter_registration.ml lang1_script.ml love_gas.ml love_michelson.mli love_michelson.ml love_tenv.mli love_tenv.ml love_context.ml love_free_vars.ml love_env.ml love_translator.ml love_typechecker.mli love_typechecker.ml love_errors.ml love_prim_interp.mli love_prim_interp.ml love_prim_list.mli love_prim_list.ml love_interpreter.mli love_interpreter.ml love_interpreter_interface.ml love_script.ml dune_script_interpreter.mli dune_script_interpreter.ml dune_apply.mli dune_apply.ml apply.ml services_registration.ml constants_services.mli constants_services.ml contract_services.mli contract_services.ml delegate_services.mli delegate_services.ml helpers_services.mli helpers_services.ml voting_services.mli voting_services.ml dune_services.mli dune_services.ml alpha_services.mli alpha_services.ml main.mli main.ml)
 (action
  (write-file %{targets}
    "module Environment = Tezos_protocol_environment_005_PsBabyM1.Environment
let hash = Tezos_crypto.Protocol_hash.of_b58check_exn \"PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS\"
let name = Environment.Name.name
include Tezos_raw_protocol_005_PsBabyM1
include Tezos_raw_protocol_005_PsBabyM1.Main
")))

(library
 (name tezos_protocol_environment_005_PsBabyM1)
 (public_name tezos-protocol-005-PsBabyM1.environment)
 (library_flags (:standard -linkall))
 (libraries tezos-protocol-environment)
 (modules Environment))

(library
 (name tezos_raw_protocol_005_PsBabyM1)
 (public_name tezos-protocol-005-PsBabyM1.raw)
 (libraries tezos_protocol_environment_005_PsBabyM1)
 (library_flags (:standard -linkall))
 (flags (:standard -nopervasives
                   -w +a-4-6-7-9-29-32-40..42-44-45-48
                   -warn-error -a+8
                   -open Tezos_protocol_environment_005_PsBabyM1__Environment
                   -open Pervasives
                   -open Error_monad))
 (modules Source Config Dune_protocol_parameters Dune_misc Misc Storage_description State_hash Nonce_hash Script_expr_hash Contract_hash Blinded_public_key_hash Qty_repr Tez_repr Period_repr Time_repr Constants_repr Fitness_repr Raw_level_repr Voting_period_repr Cycle_repr Level_repr Seed_repr Gas_limit_repr Script_int_repr Script_timestamp_repr Michelson_v1_primitives Script_repr Contract_repr Roll_repr Vote_repr Block_header_repr Dune_script_sig Lang1_script_repr Dune_script_registration Lang1_repr Love_pervasives Love_type Love_type_list Love_primitive Love_ast Love_runtime_ast Love_value Love_printer Love_encoding_sig Love_json_encoding Love_binary_buffer Love_binary_processor Love_binary_encoding Love_encoding Love_size Love_script_repr Love_repr Dune_script_repr Commitment_repr Dune_parameters_repr Dune_operation_repr Dune_lang_repr Legacy_script_support_repr Operation_repr Manager_repr Parameters_repr Raw_context Storage_sigs Storage_functors Storage Dune_storage Constants_storage Level_storage Nonce_storage Seed_storage Roll_storage Delegate_storage Contract_storage Bootstrap_storage Fitness_storage Vote_storage Commitment_storage Dune_actions Init_storage Fees_storage Alpha_context Script_typed_ir Script_tc_errors Michelson_v1_gas Script_ir_annot Script_ir_translator Script_tc_errors_registration Script_interpreter Baking Amendment Dune_apply_results Apply_results Dune_script_interpreter_sig Lang1_interpreter Dune_script_interpreter_registration Lang1_script Love_gas Love_michelson Love_tenv Love_context Love_free_vars Love_env Love_translator Love_typechecker Love_errors Love_prim_interp Love_prim_list Love_interpreter Love_interpreter_interface Love_script Dune_script_interpreter Dune_apply Apply Services_registration Constants_services Contract_services Delegate_services Helpers_services Voting_services Dune_services Alpha_services Main))

(install
 (section lib)
 (package tezos-protocol-005-PsBabyM1)
 (files (TEZOS_PROTOCOL as raw/TEZOS_PROTOCOL)))

(library
 (name tezos_protocol_005_PsBabyM1)
 (public_name tezos-protocol-005-PsBabyM1)
 (libraries
      tezos-protocol-environment
      tezos-protocol-environment-sigs
      tezos_raw_protocol_005_PsBabyM1)
 (flags -w "+a-4-6-7-9-29-40..42-44-45-48"
        -warn-error "-a+8"
        -nopervasives)
 (modules Protocol))

(library
 (name tezos_protocol_005_PsBabyM1_functor)
 (public_name tezos-protocol-005-PsBabyM1.functor)
 (libraries
      tezos-protocol-environment
      tezos-protocol-environment-sigs
      tezos_raw_protocol_005_PsBabyM1)
 (flags -w "+a-4-6-7-9-29-40..42-44-45-48"
        -warn-error "-a+8"
        -nopervasives)
 (modules Functor))

(library
 (name tezos_embedded_protocol_005_PsBabyM1)
 (public_name tezos-embedded-protocol-005-PsBabyM1)
 (library_flags (:standard -linkall))
 (libraries tezos-protocol-005-PsBabyM1
            tezos-protocol-updater
            tezos-protocol-environment)
 (flags (:standard -w +a-4-6-7-9-29-32-40..42-44-45-48
                   -warn-error -a+8))
 (modules Registerer))

(alias
 (name runtest_sandbox)
 (deps .tezos_protocol_005_PsBabyM1.objs/native/tezos_protocol_005_PsBabyM1.cmx))
