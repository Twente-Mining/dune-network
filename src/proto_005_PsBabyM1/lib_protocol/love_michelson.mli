(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_ast_types

val to_michelson_type :
  Alpha_context.context -> TYPE.t ->
  (int, Michelson_v1_primitives.prim) Micheline.node tzresult

val to_michelson_const :
  Alpha_context.context -> Love_script_repr.const -> Script_repr.expr tzresult

val from_michelson_node_const :
  Alpha_context.context ->
  ('a, Michelson_v1_primitives.prim) Micheline.node ->
  (Alpha_context.context * Love_script_repr.const) tzresult

val michelson_contract_as_sig :
  Alpha_context.context -> Script_repr.expr ->
  (Alpha_context.context * TYPE.structure_sig) tzresult

val from_michelson_const :
  Alpha_context.context ->
  Michelson_v1_primitives.prim Micheline.canonical ->
  (Alpha_context.context * Love_script_repr.const) tzresult
