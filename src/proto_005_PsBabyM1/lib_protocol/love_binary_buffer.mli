(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(** Types of binary buffer *)
type writer
type reader

(** 1. Writer *)

(** Initializes the buffer as a writer *)
val initialize_writer : unit -> writer

(** Returns the bytes representation of the buffer *)
val finalize : writer -> MBytes.t

(** `write buff v size` writes v as a 'size' bit value. Value must be positive. *)
val write : writer -> int64 -> int -> unit


(** Writes a boolean on 1 bit *)
val write_bool : writer -> bool -> unit

(** Write unsigned ints *)
val write_uint8 : writer -> int -> unit
val write_uint16 : writer -> int -> unit
val write_uint32 : writer -> int -> unit
val write_uint63 : writer -> int64 -> unit

(** Writes on the buffer an unsigned int prefixed by its size. *)
val write_uint : writer -> int -> unit

(** Writes an unbounded integer *)
val write_z : writer -> Z.t -> unit

(** Writes bytes *)
val write_bytes : writer -> MBytes.t -> unit

(** Writes a string *)
val write_string : writer -> string -> unit

(** Write a string representation *)
val write_str_repr : writer -> string -> unit

(** 2. Reader *)

(** Initializes the buffer as a reader *)
val initialize_reader : MBytes.t -> reader

(** read buff n reads n bits on the buffer as an unsigned value *)
val read : reader -> int -> int64

(** Reads 1 bit, returns true if 1, 0 otherwise *)
val read_bool : reader -> bool

(** Reads unsigned integers *)
val read_uint8 : reader -> int
val read_uint16 : reader -> int
val read_uint32 : reader -> int
val read_uint63 : reader -> int64

(** Reads an unsigned int prefixed by its size. *)
val read_uint : reader -> int

(** Reads an unbounded integer *)
val read_z : reader -> Z.t

(** Reads bytes *)
val read_bytes : reader -> MBytes.t

(** Reads a string *)
val read_string : reader -> string

(** Reads a string representation *)
val read_str_repr : reader -> string


val print_stats : Format.formatter -> writer -> unit

(** Statistics *)

val stats_add_i_custom : writer -> string -> int -> unit
