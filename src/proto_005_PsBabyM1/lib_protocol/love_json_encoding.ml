(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions
open Collections
open Utils
open Data_encoding
open Love_ast_types

(* equivalent encoding to Tez_repr.encoding *)
let int64_encoding =
  (check_size 10 (conv Z.of_int64 (Json.wrap_error Z.to_int64) n))

let strbnd_to_map b =
  Utils.bindings_to_map StringMap.add StringMap.empty b

let encode_string s =
  `String s

let decode_string = function
  | `String s -> s
  | _ -> raise (DecodingError "Bad string")

let encode_ident id =
  `A (List.map encode_string (Ident.get_list id))

let decode_ident =
  let open Ident in
  function
  | `A idl ->
     let idl = List.map decode_string idl in
     begin match List.rev idl with
     | id :: l -> put_in_namespaces l (create_id id)
     | _ -> raise (DecodingError "Bad ident (empty list)")
     end
  | _ -> raise (DecodingError "Bad ident")

let encode_trait =
  let open TYPE in
  function
  | { tcomparable } -> `Bool tcomparable

let decode_trait =
  let open TYPE in
  function
  | `Bool tcomparable -> { tcomparable }
  | _ -> raise (DecodingError "Bad trait")

let encode_visibility =
  function
  | AST.Private -> `String "private"
  | Public -> `String "public"

let decode_visibility =
  function
  | `String "private" -> AST.Private
  | `String "public" -> Public
  | _ -> raise (DecodingError "Bad visibility")

let encode_rec = function
  | TYPE.Rec -> `String "rec"
  | NonRec -> `String "nonrec"

let decode_rec = function
  | `String "rec" -> TYPE.Rec
  | `String "nonrec" -> NonRec
  | _ -> raise (DecodingError "Unknown recurisve flag")

let encode_pair_str_x encode (s, e) = s, encode e

let decode_pair_str_x decode (s, e) = s, decode e

let encode_pair1 encode (d1, d2) =
  `A [ encode d1; encode d2 ]

let decode_pair1 decode = function
  | `A [ d1; d2 ] -> (decode d1, decode d2)
  | _ -> raise (DecodingError "Bad pair")

let encode_pair2 encode1 encode2 (d1, d2) =
  `A [ encode1 d1; encode2 d2 ]

let decode_pair2 decode1 decode2 = function
  | `A [ d1; d2 ] -> (decode1 d1, decode2 d2)
  | _ -> raise (DecodingError "Bad pair")

let encode_tuple3 encode1 encode2 encode3 (d1, d2, d3) =
  `A [encode1 d1; encode2 d2; encode3 d3]

let decode_tuple3 decode1 decode2 decode3 = function
  | `A [d1; d2; d3] -> (decode1 d1, decode2 d2, decode3 d3)
  | _ -> raise (DecodingError "Bad tuple3")

let encode_option encode = function
  | Some e -> `O [ "some", encode e ]
  | None -> `String "none"

let decode_option decode = function
  | `O [ "some", e ] -> Some (decode e)
  | `String "none" -> None
  | _ -> raise (DecodingError "Bad option")

let encode_list encode = function
  | [] -> `Null
  | l -> `A (List.map encode l)

let decode_list decode = function
  | `Null -> []
  | `A l -> (List.map decode l)
  | _ -> raise (DecodingError "Bad list")

let encode_str_x_list encode l =
  `O (List.map (encode_pair_str_x encode) l)

let decode_str_x_list decode e =
  match e with
  | `O l -> (List.map (decode_pair_str_x decode) l)
  | `A _ -> decode_list (decode_pair2 decode_string decode) e
  | _ -> raise (DecodingError "Bad (string * 'a) list ")

module TypeEncoder = struct

  open TYPE

  let arrow_to_list t =
    let rec aux l = function
    | TArrow (t1, t2) -> aux (t1 :: l) t2
    | t -> t :: l
    in
    List.rev (aux [] t)

  let rec encode_type : TYPE.t -> Data_encoding.json = function
    | TUser (tn, tl) ->
       `O [ "user", `A [ encode_tname tn; encode_list encode_type tl ] ]
    | TTuple tl -> `O [ "tuple", encode_list encode_type tl ]
    | TArrow _ as t -> `O [ "arrow", encode_list encode_type (arrow_to_list t) ]
    | TVar tv -> `O [ "var", encode_tvar tv ]
    | TForall (tv, t) -> `O [ "forall", `A [ encode_tvar tv; encode_type t ] ]
    | TPackedStructure st -> `O [ "structure", encode_structure_type st ]
    | TContractInstance ct -> `O [ "instance", encode_structure_type ct ]

  and encode_structure_type = function
    | Named ctn -> encode_ident ctn (* Array *)
    | Anonymous cs -> encode_signature cs (* Object *)

  and encode_typedef = function
    | Alias { aparams; atype } ->
       `O [ "targs", encode_list encode_tvar aparams;
            "type", encode_type atype ]
    | SumType { sparams; scons; srec } ->
       `O [ "targs", encode_list encode_tvar sparams;
            "constr", encode_str_x_list (encode_list encode_type) scons;
            "recursive", encode_rec srec
          ]
    | RecordType { rparams; rfields; rrec } ->
       `O [ "targs", encode_list encode_tvar rparams;
            "fields", encode_str_x_list encode_type rfields;
            "recursive", encode_rec rrec ]

  and encode_sig_type = function
    | SPublic td -> `O ["public", encode_typedef td]
    | SPrivate td -> `O ["private", encode_typedef td]
    | SAbstract tl -> `O ["abstract", encode_list encode_tvar tl]

  and encode_sig_content = function
    | SType s -> `O ["type", encode_sig_type s]
    | SException tl -> `O ["exception", encode_list encode_type tl]
    | SInit t -> `O ["init", encode_type t]
    | SEntry t -> `O ["entry", encode_type t]
    | SView t -> `O ["view", encode_type t]
    | SValue t -> `O ["value", encode_type t]
    | SStructure st -> `O ["structure", encode_structure_type st]
    | SSignature s -> `O ["signature", encode_signature s]

  and encode_kind : TYPE.struct_kind -> Data_encoding.json = function
    | Module -> `String "module"
    | Contract l -> `O [ "contract", encode_str_x_list encode_string l]

  and encode_signature { sig_kind; sig_content; } : Data_encoding.json =
    `O [ "kind", encode_kind sig_kind;
         "content", encode_str_x_list encode_sig_content sig_content ]

  and encode_tvar { tv_name; tv_traits } =
    `O [ tv_name, encode_trait tv_traits ]

  and encode_tname = encode_ident

  and encode_exception (exn, tl) =
    `O [ "exn", `String exn; "args", encode_list encode_type tl ]

end

module TypeDecoder = struct

  open TYPE

  let list_to_arrow l =
    let rec aux t2 = function
      | t1 :: l -> aux (TArrow (t1, t2)) l
      | [] -> t2
    in
    match List.rev l with
    | t :: l ->  aux t l
    | [] -> raise (DecodingError "Empty type list")

  let rec decode_type : Data_encoding.json -> TYPE.t = function
    | `O [ "user", `A [ tn; tl ] ] ->
       TUser (decode_tname tn, decode_list decode_type tl)
    | `O [ "tuple", tl ] -> TTuple (decode_list decode_type tl)
    | `O [ "arrow", tl ] -> list_to_arrow (decode_list decode_type tl)
    | `O [ "var", tv ] -> TVar (decode_tvar tv)
    | `O [ "forall", `A [ tv; t ] ] -> TForall (decode_tvar tv, decode_type t)
    | `O [ "structure", st ] -> TPackedStructure (decode_structure_type st)
    | `O [ "instance", ct ] -> TContractInstance (decode_structure_type ct)
    | _ -> raise (DecodingError "Bad type")

  and decode_structure_type = function
    | (`A _) as sn -> Named (decode_ident sn) (* Array *)
    | (`O _) as s -> Anonymous (decode_signature s) (* Object *)
    | _ -> raise (DecodingError "Bad structure type")

  and decode_sig_content = function
    | `O ["type", s] -> SType (decode_sigtype s)
    | `O ["exception", tl] -> SException (decode_list decode_type tl)
    | `O ["init", t] -> SInit (decode_type t)
    | `O ["entry", t] -> SEntry (decode_type t)
    | `O ["view", t] -> SView (decode_type t)
    | `O ["value", t] -> SValue (decode_type t)
    | `O ["structure", st] -> SStructure (decode_structure_type st)
    | `O ["signature", s] -> SSignature (decode_signature s)
    | _ -> raise (DecodingError "Bad signature content")

  and decode_kind = function
    | `String "module" -> Module
    | `O [ "contract", l ] -> Contract (decode_str_x_list decode_string l)
    | _ -> raise (DecodingError "Bad structure kind")

  and decode_signature = function
    | `O [ "kind", kind; "content", content ] -> {
        sig_kind = decode_kind kind;
        sig_content = decode_str_x_list decode_sig_content content }
    | _ -> raise (DecodingError "Bad signature")

  and decode_tvar = function
    | `O [ tv_name, tv_traits ] ->
       { tv_name = tv_name; tv_traits = decode_trait tv_traits }
    | _ -> raise (DecodingError "Bad type variable")

  and decode_tname = decode_ident

  and decode_sigtype = function
    | `O ["public", td] -> SPublic (decode_typedef td)
    | `O ["private", td] -> SPrivate (decode_typedef td)
    | `O ["abstract", tl] -> SAbstract (decode_list decode_tvar tl)
    | _ -> raise (DecodingError "Bad sigtype definition")

  and decode_typedef = function
    | `O [ "targs", aparams; "type", atype ] ->
       Alias { aparams = decode_list decode_tvar aparams;
               atype = decode_type atype }
    | `O [ "targs", sparams; "constr", scons; "recursive", srec ] ->
       SumType { sparams = decode_list decode_tvar sparams;
                 scons = decode_str_x_list (decode_list decode_type) scons;
                 srec = decode_rec srec
               }
    | `O [ "targs", rparams; "fields", rfields; "recursive", rrec ] ->
       RecordType { rparams = decode_list decode_tvar rparams;
                    rfields = decode_str_x_list decode_type rfields;
                    rrec = decode_rec rrec }
    | _ -> raise (DecodingError "Bad type definition")

  and decode_exception = function
    | `O [ "exn", `String exn; "args", tl ] -> exn, decode_list decode_type tl
    | _ -> raise (DecodingError "Bad exception definition")

end

module Type = struct
  let encoding =
    conv TypeEncoder.encode_type TypeDecoder.decode_type json
end

module PrimEncoder = struct

  open TYPE

  let encode_extended_arg :
    TYPE.extended_arg -> Data_encoding.json = function
    | ANone -> `Null
    | AContractType ct ->
        `O [ "cnt_type", TypeEncoder.encode_structure_type ct ]

  let encode_prim :
    Love_primitive.t * TYPE.extended_arg -> Data_encoding.json =
    fun (p, xs) ->
    encode_pair2 encode_string encode_extended_arg (p.prim_name, xs)
end

module PrimDecoder = struct

  let decode_extended_arg :
    Data_encoding.json -> TYPE.extended_arg = function
    | `Null -> ANone
    | `O [ "cnt_type", ct ] ->
        AContractType (TypeDecoder.decode_structure_type ct)
    | _ ->
      raise (DecodingError "Unknown extended arg")

  let decode_prim :
    Data_encoding.json -> Love_primitive.t * TYPE.extended_arg =
    function
    | `A [ `String s; xa ] ->
        let xa = decode_extended_arg xa in
        begin match Love_primitive.from_string s with
          | Some p -> p, xa
          | None -> raise (DecodingError "Unknown primitive")
        end
    | _ ->
      raise (DecodingError "Unknown primitive")

end

let encode_annoted encode_content encode_annot a =
  `O [ "content", encode_content a.content;
       "annot", encode_annot a.annot ]

let decode_annoted decode_content decode_annot a =
  match a with
  | `O [ "content", content; "annot", annot ] ->
    { content = decode_content content;
      annot = decode_annot annot }
  | _ -> raise (DecodingError "Badly formed annotation")

let encode_type_kind = function
  | AST.TPublic -> `String "public"
  | TPrivate -> `String "private"
  | TInternal -> `String "internal"
  | TAbstract -> `String "abstract"

let decode_type_kind = function
  | `String "public" -> AST.TPublic
  | `String "private" -> TPrivate
  | `String "internal" -> TInternal
  | `String "abstract" -> TAbstract
  | _ -> raise (DecodingError "Badly formed type kind")

let location_encoding =
  conv
    (fun AST.{pos_lnum; pos_bol; pos_cnum} ->
       (pos_lnum, pos_bol, pos_cnum))
    (fun (pos_lnum, pos_bol, pos_cnum) ->
       {pos_lnum; pos_bol; pos_cnum})
    (obj3
       (req "lnum" int31)
       (req "bol" int31)
       (req "cnum" int31))

let encode_loc = Json.construct location_encoding
let decode_loc = Json.destruct location_encoding

module Ast = struct

  open AST

  let encode_annot = Json.construct (option location_encoding)
  let decode_annot = Json.destruct (option location_encoding)

  let encode_annoted enc a =
    `O [ "content", enc a.content; "annot", encode_annot a.annot]

  let decode_annoted dec =
    function
    | `O [ "content", c; "annot", a] ->
      { content = dec c; annot = decode_annot a }
    | _ -> raise (DecodingError "Badly formed annotation")

  let encode_exn_name = function
    | AST.Fail t -> `O [ "fail", TypeEncoder.encode_type t ]
    | Exception e -> `O [ "exception", encode_ident e ]

  let decode_exn_name = function
    | `O [ "fail", t ] -> AST.Fail (TypeDecoder.decode_type t)
    | `O [ "exception", e ] -> Exception (decode_ident e)
    | _ -> raise (DecodingError "Bad exception name")

  let encode_open_closed = function
    | Open -> `String "open"
    | Closed -> `String "closed"

  let decode_open_closed = function
    | `String "open" -> Open
    | `String "closed" -> Closed
    | _ -> raise (DecodingError "Bad record pattern open status")

  let encode_record_list encode_pattern {fields; open_record} =
    `O [
      "fields",
      encode_list (encode_pair2 encode_string (encode_option encode_pattern)) fields;
      "status", encode_open_closed open_record
    ]

  let decode_record_list decode_pattern = function
    | `O ["fields", f; "status", o] ->
        {fields =
           decode_list (decode_pair2 decode_string (decode_option decode_pattern)) f;
         open_record = decode_open_closed o
        }
    | _ -> raise (DecodingError " Bad record pattern")

  let rec encode_raw_const : raw_const -> Data_encoding.json =
    function
    | CUnit -> `Null
    | CBool b -> `Bool b
    | CString s -> `String s
    | CBytes b -> `O [ "bytes", Json.construct bytes b ]
    | CInt i -> `O [ "int", Json.construct z i ]
    | CNat i -> `O [ "nat", Json.construct z i ]
    | CDun d -> `O [ "mudun", Json.construct int64_encoding d ]
    | CKey k -> `O [ "key", Json.construct Signature.Public_key.encoding k ]
    | CKeyHash kh ->
       `O [ "pkh", Json.construct Signature.Public_key_hash.encoding kh ]
    | CSignature s -> `O [ "sig", Json.construct Signature.encoding s ]
    | CTimestamp t ->
       `O [ "time", Json.construct z (Script_timestamp_repr.to_zint t) ]
    | CAddress s ->
        match Contract_repr.of_b58check s with
        | Ok s ->
            `O [ "addr", Json.construct Contract_repr.encoding s ]
        | _ -> assert false

  and encode_const c = encode_annoted encode_raw_const c

  and encode_raw_pattern = function
    | PAny -> `Null
    | PVar v -> `String v
    | PAlias (p, v) -> `O [ "alias", `String v; "of", encode_pattern p ]
    | PConst c -> `O [ "const", encode_const c ]
    | PList pl -> `O [ "list", encode_list encode_pattern pl ]
    | PTuple pl -> `O [ "tuple", encode_list encode_pattern pl ]
    | PConstr (cstr, pl) ->
       `O [ "constr", `A [ encode_string cstr; encode_list encode_pattern pl ] ]
    | PContract (n, st) ->
        `O [ "contract", encode_pair2 encode_string
               TypeEncoder.encode_structure_type (n, st) ]
    | POr pl -> `O ["or", encode_list encode_pattern pl]
    | PRecord l -> `O ["record", encode_record_list encode_pattern l]

  and encode_pattern p = encode_annoted encode_raw_pattern p

  and encode_lambda { arg_pattern; body; arg_typ } =
    `O [ "lambda", `A [ encode_pattern arg_pattern;
                        TypeEncoder.encode_type arg_typ ];
         "body", encode_exp body ]

  and encode_struct_ref = function
    | Named sn -> encode_ident sn (* Array *)
    | Anonymous s -> encode_structure s (* Object *)

  and encode_raw_exp (e : raw_exp) : Data_encoding.json =
    match e with
    | Const c -> `O [ "const", encode_const c ]
    | Var v -> `O [ "var", encode_ident v ]
    | VarWithArg (v, xa) ->
       `O [ "var_arg", encode_pair2 encode_ident
                         PrimEncoder.encode_extended_arg (v, xa) ]
    | Let { bnd_pattern; bnd_val; body } ->
       `O [ "let", encode_pattern bnd_pattern;
            "equals", encode_exp bnd_val;
            "in", encode_exp body ]
    | LetRec { bnd_var; bnd_val; body; fun_typ } ->
       `O [ "letrec", `String bnd_var;
            "of_type", TypeEncoder.encode_type fun_typ;
            "equals", encode_exp bnd_val;
            "in", encode_exp body ]
    | Lambda l ->
       encode_lambda l (* Object *)
    | Apply { fct; args } ->
       `O [ "apply", `A [ encode_exp fct; encode_list encode_exp args ] ]
    | TLambda { targ; exp } ->
       `O [ "tlambda", `A [ TypeEncoder.encode_tvar targ; encode_exp exp ] ]
    | TApply { exp; typ } ->
       `O [ "tapply", `A [ encode_exp exp; TypeEncoder.encode_type typ ] ]
    | Seq el ->
       `O [ "seq", encode_list encode_exp el ]
    | If { cond; ifthen; ifelse } ->
       `O [ "if", encode_exp cond;
            "then", encode_exp ifthen;
            "else", encode_exp ifelse ]
    | Match { arg; cases } ->
       `O [ "match", encode_exp arg;
            "with", encode_list (encode_pair2 encode_pattern encode_exp) cases ]
    | Constructor { constr; ctyps; args } ->
      `O [ "construct", `A [ encode_ident constr;
                             encode_list TypeEncoder.encode_type ctyps;
                             encode_list encode_exp args ] ]
    | Nil ->
      `String "nil"
    | List (el, e) ->
       `O [ "cons", `A [ encode_list encode_exp el; encode_exp e ] ]
    | Tuple el ->
       `O [ "tuple", encode_list encode_exp el ]
    | Project { tuple; indexes } ->
       `O [ "project", encode_exp tuple;
            "on", encode_list (Json.construct int31) indexes ]
    | Update { tuple; updates } ->
       `O [ "update", encode_exp tuple;
            "with", encode_list (encode_pair2 (Json.construct int31)
                                   encode_exp) updates ]
    | Record { path; contents = fel } ->
       `O [ "record", encode_str_x_list encode_exp fel;
            "path", encode_option encode_ident path ]
    | GetField { record; field } ->
       `O [ "get", encode_string field;
            "of", encode_exp record ]
    | SetFields { record; updates } ->
       `O [ "set", encode_str_x_list encode_exp updates;
            "of", encode_exp record ]
    | PackStruct sr ->
       `O [ "packstruct", encode_struct_ref sr ]
    | Raise { exn; args; loc } ->
      `O [ "raise",
           `A [ encode_exn_name exn;
                encode_list encode_exp args;
                encode_option encode_loc loc ] ]
    | TryWith { arg; cases } ->
       `O [ "try", encode_exp arg;
            "with", encode_list (fun ((exn, pl), e) ->
                        `A [ encode_exn_name exn;
                             encode_list encode_pattern pl;
                             encode_exp e ]
              ) cases ]

  and encode_exp e = encode_annoted encode_raw_exp e

  and encode_content n = function
    | DefType (k,td) ->
       `O [ "typekind", encode_type_kind k;
            "typedef", `String n;
            "type", TypeEncoder.encode_typedef td ]
    | DefException tl ->
       `O [ "exception", `String n;
            "args", encode_list TypeEncoder.encode_type tl ]
    | Init { init_code; init_typ; init_persist } ->
       `O [ "init", `String n;
            "code", encode_exp init_code;
            "type", TypeEncoder.encode_type init_typ;
            "persist", `Bool init_persist ]
    | Entry { entry_code; entry_fee_code; entry_typ } ->
       `O [ "entry", `String n;
            "code", encode_exp entry_code;
            "fee_code", encode_option encode_exp entry_fee_code;
            "type", TypeEncoder.encode_type entry_typ ]
    | View { view_code; view_typ; view_recursive } ->
       `O [ "view", `String n;
            "code", encode_exp view_code;
            "type", TypeEncoder.encode_type view_typ;
            "recursive", encode_rec view_recursive;]
    | Value { value_code; value_typ; value_visibility; value_recursive } ->
       `O [ "value", `String n;
            "code", encode_exp value_code;
            "type", TypeEncoder.encode_type value_typ;
            "visibility", encode_visibility value_visibility;
            "recursive", encode_rec value_recursive ]
    | Structure s ->
       `O [ "structure", `String n;
            "contents", encode_structure s ]
    | Signature s ->
       `O [ "signature", `String n;
            "contents", TypeEncoder.encode_signature s ]

  and encode_structure { kind; structure_content } =
  `O [ "kind", TypeEncoder.encode_kind kind;
       "contents", encode_list (
         fun (n, c) ->
           encode_content n c
       ) structure_content ]

  and encode_top_structure { version; code } =
    `O [ "version", encode_pair1 (Json.construct int31) version;
         "code", encode_structure code ]

  let rec decode_raw_const : Data_encoding.json -> raw_const =
    function
    | `Null -> CUnit
    | `Bool b -> CBool b
    | `String s -> CString s
    | `O [ "bytes", b ] -> CBytes (Json.destruct bytes b)
    | `O [ "int", i ] -> CInt (Json.destruct z i)
    | `O [ "nat", i ] -> CNat (Json.destruct z i)
    | `O [ "dun", d ]
    | `O [ "mudun", d ] -> CDun (Json.destruct int64_encoding d)
    | `O [ "key", k ] -> CKey (Json.destruct Signature.Public_key.encoding k)
    | `O [ "keyhash", kh ]
    | `O [ "pkh", kh ] ->
       CKeyHash (Json.destruct Signature.Public_key_hash.encoding kh)
    | `O [ "signature", s ]
    | `O [ "sig", s ] -> CSignature (Json.destruct Signature.encoding s)
    | `O [ "timestamp", t ]
    | `O [ "time", t ] ->
       CTimestamp (Script_timestamp_repr.of_zint (Json.destruct z t))
    | `O [ "address", s ]
    | `O [ "addr", s ] ->
        let s = Json.destruct Contract_repr.encoding s in
        CAddress (Contract_repr.to_b58check s)
    | _ -> raise (DecodingError "Bad constant")

  and decode_const j = decode_annoted decode_raw_const j

  and decode_raw_pattern = function
    | `Null -> PAny
    | `String v -> PVar (v)
    | `O [ "alias", `String v; "of", p ] -> PAlias (decode_pattern p, v)
    | `O [ "const", c ] -> PConst (decode_const c)
    | `O [ "list", pl ] -> PList (decode_list decode_pattern pl)
    | `O [ "tuple", pl ] -> PTuple (decode_list decode_pattern pl)
    | `O [ "constr", `A [ cstr; pl ] ] ->
       PConstr (decode_string cstr, decode_list decode_pattern pl)
    | `O [ "contract", nst ] ->
        let n, st = decode_pair2 decode_string
            TypeDecoder.decode_structure_type nst in
        PContract (n, st)
    | `O [ "or", pl ] -> POr (decode_list decode_pattern pl)
    | `O [ "record", l ] -> PRecord (decode_record_list decode_pattern l)
    | _ -> raise (DecodingError "Bad pattern")

  and decode_pattern j = decode_annoted decode_raw_pattern j

  and decode_lambda = function
    | `O [ "lambda", `A [ arg_pattern; arg_typ ]; "body", body ] ->
       { arg_pattern = decode_pattern arg_pattern;
         body = decode_exp body;
         arg_typ = TypeDecoder.decode_type arg_typ }
    | _ -> raise (DecodingError "Bad lambda")

  and decode_struct_ref = function
    | (`A _) as sn -> Named (decode_ident sn) (* Array *)
    | (`O _) as s -> Anonymous (decode_structure s) (* Object *)
    | _ -> raise (DecodingError "Bad structure reference")

  and decode_raw_exp : Data_encoding.json -> raw_exp =
    function
    | `O [ "const", c ] -> Const (decode_const c)
    | `O [ "var", v ] -> Var (decode_ident v)
    | `O [ "var_arg", v_xa ] ->
        let v, xa = decode_pair2 decode_ident
                      PrimDecoder.decode_extended_arg v_xa in
        VarWithArg (v, xa)
    | `O [ "let", bnd_pattern; "equals", bnd_val; "in", body ] ->
        Let { bnd_pattern = decode_pattern bnd_pattern;
              bnd_val = decode_exp bnd_val; body = decode_exp body }
    | `O [ "letrec", `String bnd_var; "of_type", fun_typ;
           "equals", bnd_val; "in", body ] ->
      LetRec { bnd_var = bnd_var;
               bnd_val = decode_exp bnd_val; body = decode_exp body;
                fun_typ = TypeDecoder.decode_type fun_typ }
    | `O (("lambda", _) :: _) as l -> Lambda (decode_lambda l)
    | `O [ "apply", `A [ fct; args ] ] ->
       Apply { fct = decode_exp fct; args = decode_list decode_exp args }
    | `O [ "tlambda", `A [ tvar; exp ] ] ->
       TLambda { targ = TypeDecoder.decode_tvar tvar; exp = decode_exp exp }
    | `O [ "tapply", `A [ exp; typ ] ] ->
       TApply { exp = decode_exp exp; typ = TypeDecoder.decode_type typ }
    | `O [ "seq", el ] -> Seq (decode_list decode_exp el)
    | `O [ "if", cond; "then", ifthen; "else", ifelse ] ->
       If { cond = decode_exp cond;
            ifthen = decode_exp ifthen; ifelse = decode_exp ifelse }
    | `O [ "match", arg; "with", cases ] ->
       Match { arg = decode_exp arg;
               cases = decode_list
                         (decode_pair2 decode_pattern decode_exp) cases }
    | `O [ "construct", `A [ constr; ctyps; args ] ] ->
      Constructor { constr = decode_ident constr;
                    ctyps = decode_list TypeDecoder.decode_type ctyps;
                    args = decode_list decode_exp args }
    | `String "nil" -> Nil
    | `O [ "cons", `A [ el; e ] ] ->
       List (decode_list decode_exp el, decode_exp e)
    | `O [ "tuple", el ] -> Tuple (decode_list decode_exp el)
    | `O [ "project", tuple; "on", indexes ] ->
       Project { tuple = decode_exp tuple;
                 indexes = decode_list (Json.destruct int31) indexes }
    | `O [ "update", tuple; "with", updates ] ->
       Update { tuple = decode_exp tuple;
                updates = decode_list (decode_pair2 (Json.destruct int31)
                                         decode_exp) updates }
    | `O [ "record", fel; "path", path ] ->
       Record { path = decode_option decode_ident path;
                contents = decode_str_x_list decode_exp fel }
    | `O [ "get", field; "of", record ] ->
       GetField { record = decode_exp record; field = decode_string field }
    | `O [ "set", updates; "of", record ] ->
       SetFields {
           record = decode_exp record;
           updates = decode_str_x_list decode_exp updates }
    | `O [ "packstruct", sr ] -> PackStruct (decode_struct_ref sr)
    | `O [ "raise", `A [ exn; args; loc ] ] ->
      Raise { exn = decode_exn_name exn;
              args = decode_list decode_exp args;
              loc = decode_option decode_loc loc }
    | `O [ "try", arg; "with", cases ] ->
       TryWith { arg = decode_exp arg;
                 cases = decode_list (function
                             | `A [ exn; pl; e ] ->
                                (decode_exn_name exn,
                                 decode_list decode_pattern pl), decode_exp e
                             | _ -> raise (DecodingError "Bad trywith")) cases }
    | _ -> raise (DecodingError "Bad expression")

  and decode_exp j = decode_annoted decode_raw_exp j

  and decode_content = function
    | `O [ "typekind", k; "typedef", `String n; "type", td ] ->
        n, DefType (decode_type_kind k, TypeDecoder.decode_typedef td)
    | `O [ "exception", `String n; "args", tl ] ->
        n, DefException (decode_list TypeDecoder.decode_type tl)
    | `O [ "init", `String n; "code", init_code;
           "type", init_typ; "persist", `Bool init_persist ] ->
        n, Init { init_code = decode_exp init_code;
                  init_typ = TypeDecoder.decode_type init_typ;
                  init_persist }
    | `O [ "entry", `String n; "code", code;
           "fee_code", fee_code; "type", typ ] ->
        n, Entry { entry_code = decode_exp code;
                   entry_fee_code = decode_option decode_exp fee_code;
                   entry_typ = TypeDecoder.decode_type typ }
    | `O [ "view", `String n; "code", code; "type", typ; "recursive", recursive ] ->
        n, View { view_code = decode_exp code;
                  view_typ = TypeDecoder.decode_type typ;
                  view_recursive = decode_rec recursive
                }
    | `O [ "value", `String n; "code", code; "type", typ;
           "visibility", visibility; "recursive", recursive ] ->
        n, Value { value_code = decode_exp code;
                   value_typ = TypeDecoder.decode_type typ;
                   value_visibility = decode_visibility visibility;
                   value_recursive = decode_rec recursive }
    | `O [ "structure", `String n; "contents", s ] ->
        n, Structure (decode_structure s)
    | `O [ "signature", `String n; "contents", s ] ->
        n, Signature (TypeDecoder.decode_signature s)
    | _ -> raise (DecodingError "Bad content")

  and decode_structure = function
    | `O [ "kind", kind; "contents", contents; ] -> {
        kind = TypeDecoder.decode_kind kind;
        structure_content = decode_list decode_content contents;
      }
    | _ -> raise (DecodingError "Bad structure")

  and decode_top_structure = function
    | `O [ "version", version; "code", code ] ->
      { version = decode_pair1 (Json.destruct int31) version;
        code = decode_structure code }
    | `O [ "parse", `String s ] ->
        Love_lexer.parse_top_contract s
    | _ -> raise (DecodingError "Bad top structure")

  let const_encoding =
    conv encode_const decode_const json
  let structure_encoding =
    conv encode_structure decode_structure json
  let top_contract_encoding =
    conv encode_top_structure decode_top_structure json
  let exp_encoding =
    conv encode_exp decode_exp json
end




module RuntimeAst = struct

  open Love_runtime_ast

  let encode_exn_name = function
    | RFail t -> `O [ "fail", TypeEncoder.encode_type t ]
    | RException e -> `O [ "exception", encode_ident e ]

  let decode_exn_name = function
    | `O [ "fail", t ] -> RFail (TypeDecoder.decode_type t)
    | `O [ "exception", e ] -> RException (decode_ident e)
    | _ -> raise (DecodingError "Bad exception name")

  let rec encode_const : const -> Data_encoding.json = function
    | RCUnit -> `Null
    | RCBool b -> `Bool b
    | RCString s -> `String s
    | RCBytes b -> `O [ "bytes", Json.construct bytes b ]
    | RCInt i -> `O [ "int", Json.construct z i ]
    | RCNat i -> `O [ "nat", Json.construct z i ]
    | RCDun d -> `O [ "mudun", Json.construct Tez_repr.encoding d ]
    | RCKey k -> `O [ "key", Json.construct Signature.Public_key.encoding k ]
    | RCKeyHash kh ->
       `O [ "pkh", Json.construct Signature.Public_key_hash.encoding kh ]
    | RCSignature s -> `O [ "sig", Json.construct Signature.encoding s ]
    | RCTimestamp t ->
       `O [ "time", Json.construct z (Script_timestamp_repr.to_zint t) ]
    | RCAddress s -> `O [ "addr", Json.construct Contract_repr.encoding s ]

  and encode_pattern = function
    | RPAny -> `Null
    | RPVar v -> `String v
    | RPAlias (p, v) -> `O [ "alias", `String v; "of", encode_pattern p ]
    | RPConst c -> `O [ "const", encode_const c ]
    | RPList pl -> `O [ "list", encode_list encode_pattern pl ]
    | RPTuple pl -> `O [ "tuple", encode_list encode_pattern pl ]
    | RPConstr (cstr, pl) ->
       `O [ "constr", `A [ encode_string cstr; encode_list encode_pattern pl ] ]
    | RPContract (n, st) ->
       `O [ "contract", encode_pair2 encode_string
              TypeEncoder.encode_structure_type (n, st) ]
    | RPOr pl -> `O ["or", encode_list encode_pattern pl]
    | RPRecord l -> `O ["record", Ast.encode_record_list encode_pattern l]

  and encode_binding = function
    | RPattern (p, t) ->
       `O [ "pattern",
            encode_pair2 encode_pattern TypeEncoder.encode_type (p, t) ]
    | RTypeVar tv ->
       `O [ "typevar", TypeEncoder.encode_tvar tv ]

  and encode_param = function
    | RExp e ->`O [ "exp", encode_exp e ]
    | RType t -> `O [ "type", TypeEncoder.encode_type t ]

  and encode_struct_ref = function
    | RNamed sn -> encode_ident sn (* Array *)
    | RAnonymous s -> encode_structure s (* Object *)

  and encode_exp : exp -> Data_encoding.json = function
    | RConst c -> `O [ "const", encode_const c ]
    | RVar v -> `O [ "var", encode_ident v ]
    | RVarWithArg (v, xa) ->
       `O [ "var_arg", encode_pair2 encode_ident
                         PrimEncoder.encode_extended_arg (v, xa) ]
    | RLet { bnd_pattern; bnd_val; body } ->
       `O [ "let", encode_pattern bnd_pattern;
            "equals", encode_exp bnd_val;
            "in", encode_exp body ]
    | RLetRec { bnd_var; bnd_val; val_typ; body } ->
       `O [ "letrec", `String bnd_var;
            "of_type", TypeEncoder.encode_type val_typ;
            "equals", encode_exp bnd_val;
            "in", encode_exp body ]
    | RLambda { args; body } ->
       `O [ "lambda", encode_list encode_binding args;
            "body", encode_exp body ]
    | RApply { exp; args } ->
       `O [ "apply", `A [ encode_exp exp; encode_list encode_param args ] ]
    | RSeq el ->
       `O [ "seq", encode_list encode_exp el ]
    | RIf { cond; ifthen; ifelse } ->
       `O [ "if", encode_exp cond;
            "then", encode_exp ifthen;
            "else", encode_exp ifelse ]
    | RMatch { arg; cases } ->
       `O [ "match", encode_exp arg;
            "with", encode_list (encode_pair2 encode_pattern encode_exp) cases ]
    | RConstructor { type_name; constr; ctyps; args } ->
       `O [ "construct", `O [ constr, encode_list encode_exp args ] ;
            "type_appl", encode_list TypeEncoder.encode_type ctyps;
            "type_name", encode_ident type_name ]
    | RNil ->
       `String "nil"
    | RList (el, e) ->
       `O [ "cons", `A [ encode_list encode_exp el; encode_exp e ] ]
    | RTuple el ->
       `O [ "tuple", encode_list encode_exp el ]
    | RProject { tuple; indexes } ->
       `O [ "project", encode_exp tuple;
            "on", encode_list (Json.construct int31) indexes ]
    | RUpdate { tuple; updates } ->
       `O [ "update", encode_exp tuple;
            "with", encode_list (encode_pair2 (Json.construct int31)
                                   encode_exp) updates ]
    | RRecord { type_name; contents = fel } ->
       `O [ "record", encode_str_x_list encode_exp fel ;
            "type_name", encode_ident type_name ]
    | RGetField { record; field } ->
       `O [ "get", encode_string field;
            "of", encode_exp record ]
    | RSetFields { record; updates } ->
       `O [ "set", encode_str_x_list encode_exp updates;
            "of", encode_exp record ]
    | RPackStruct sr ->
       `O [ "packstruct", encode_struct_ref sr ]
    | RRaise { exn; args; loc } ->
      `O [ "raise",
           `A [ encode_exn_name exn;
                encode_list encode_exp args;
                encode_option encode_loc loc ] ]
    | RTryWith { arg; cases } ->
       `O [ "try", encode_exp arg;
            "with", encode_list (fun ((exn, pl), e) ->
                        `A [ encode_exn_name exn;
                             encode_list encode_pattern pl;
                             encode_exp e ]
              ) cases ]

  and encode_content n = function
    | RDefType (k,td) ->
       `O [ "typekind", encode_type_kind k;
            "typedef", `String n;
            "type", TypeEncoder.encode_typedef td ]
    | RDefException tl ->
       `O [ "exception", `String n;
            "args", encode_list TypeEncoder.encode_type tl ]
    | RInit { init_code; init_typ; init_persist } ->
       `O [ "init", `String n;
            "code", encode_exp init_code;
            "type", TypeEncoder.encode_type init_typ;
            "persist", `Bool init_persist ]
    | REntry { entry_code; entry_fee_code; entry_typ } ->
       `O [ "entry", `String n;
            "code", encode_exp entry_code;
            "fee_code", encode_option encode_exp entry_fee_code;
            "type", TypeEncoder.encode_type entry_typ ]
    | RView { view_code; view_typ; view_recursive } ->
       `O [ "view", `String n;
            "code", encode_exp view_code;
            "type", TypeEncoder.encode_type view_typ;
            "recursive", encode_rec view_recursive;]
    | RValue { value_code; value_typ; value_visibility; value_recursive } ->
       `O [ "value", `String n;
            "code", encode_exp value_code;
            "type", TypeEncoder.encode_type value_typ;
            "visibility", encode_visibility value_visibility;
            "recursive", encode_rec value_recursive ]
    | RStructure s ->
       `O [ "structure", `String n;
            "contents", encode_structure s ]
    | RSignature s ->
       `O [ "signature", `String n;
            "contents", TypeEncoder.encode_signature s ]

  and encode_structure { structure_content; kind } =
    `O [ "kind", TypeEncoder.encode_kind kind;
         "contents", encode_list (fun (n, c) ->
             encode_content n c) structure_content; ]

  and encode_top_structure { version; code } =
    `O [ "version", encode_pair1 (Json.construct int31) version;
         "code", encode_structure code ]

  let rec decode_const : Data_encoding.json -> const = function
    | `Null -> RCUnit
    | `Bool b -> RCBool b
    | `String s -> RCString s
    | `O [ "bytes", b ] -> RCBytes (Json.destruct bytes b)
    | `O [ "int", i ] -> RCInt (Json.destruct z i)
    | `O [ "nat", i ] -> RCNat (Json.destruct z i)
    | `O [ "dun", d ]
    | `O [ "mudun", d ] -> RCDun (Json.destruct Tez_repr.encoding d)
    | `O [ "key", k ] -> RCKey (Json.destruct Signature.Public_key.encoding k)
    | `O [ "keyhash", kh ]
    | `O [ "pkh", kh ] ->
       RCKeyHash (Json.destruct Signature.Public_key_hash.encoding kh)
    | `O [ "signature", s ]
    | `O [ "sig", s ] -> RCSignature (Json.destruct Signature.encoding s)
    | `O [ "timestamp", t ]
    | `O [ "time", t ] ->
       RCTimestamp (Script_timestamp_repr.of_zint (Json.destruct z t))
    | `O [ "address", s ]
    | `O [ "addr", s ] -> RCAddress (Json.destruct Contract_repr.encoding s)
    | _ -> raise (DecodingError "Bad constant")

  and decode_pattern = function
    | `Null -> RPAny
    | `String v -> RPVar v
    | `O [ "alias", `String v; "of", p ] -> RPAlias (decode_pattern p, v)
    | `O [ "const", c ] -> RPConst (decode_const c)
    | `O [ "list", pl ] -> RPList (decode_list decode_pattern pl)
    | `O [ "tuple", pl ] -> RPTuple (decode_list decode_pattern pl)
    | `O [ "constr", `A [ cstr; pl ] ] ->
       RPConstr (decode_string cstr, decode_list decode_pattern pl)
    | `O [ "contract", nst ] ->
        let n, st = decode_pair2 decode_string
            TypeDecoder.decode_structure_type nst in
        RPContract (n, st)
    | `O [ "or", pl ] -> RPOr (decode_list decode_pattern pl)
    | `O [ "record", pl ] -> RPRecord (Ast.decode_record_list decode_pattern pl)
    | _ -> raise (DecodingError "Bad pattern")

  and decode_binding = function
    | `O [ "pattern", pt ] ->
        let p, t = decode_pair2 decode_pattern TypeDecoder.decode_type pt in
       RPattern (p, t)
    | `O [ "typevar", tv ] ->
        RTypeVar (TypeDecoder.decode_tvar tv)
    | _ -> raise (DecodingError "Bad binding")

  and decode_param = function
    | `O [ "exp", e ] -> RExp (decode_exp e)
    | `O [ "type", t ] -> RType (TypeDecoder.decode_type t)
    | _ -> raise (DecodingError "Bad param")

  and decode_lambda = function
    | `O [ "lambda", args; "body", body ] ->
       { args = decode_list decode_binding args; body = decode_exp body }
    | _ -> raise (DecodingError "Bad lambda")

  and decode_struct_ref = function
    | (`A _) as sn -> RNamed (decode_ident sn) (* Array *)
    | (`O _) as s -> RAnonymous (decode_structure s) (* Object *)
    | _ -> raise (DecodingError "Bad structure reference")

  and decode_exp : Data_encoding.json -> exp = function
    | `O [ "const", c ] -> RConst (decode_const c)
    | `O [ "var", v ] -> RVar (decode_ident v)
    | `O [ "var_arg", v_xa ] ->
        let v, xa = decode_pair2 decode_ident
                      PrimDecoder.decode_extended_arg v_xa in
        RVarWithArg (v, xa)
    | `O [ "let", bnd_pattern; "equals", bnd_val; "in", body ] ->
        RLet { bnd_pattern = decode_pattern bnd_pattern;
               bnd_val = decode_exp bnd_val; body = decode_exp body }
    | `O [ "letrec", `String bnd_var; "of_type", fun_typ;
           "equals", bnd_val; "in", body ] ->
        RLetRec { bnd_var = bnd_var; bnd_val = decode_exp bnd_val;
                  val_typ = TypeDecoder.decode_type fun_typ;
                  body = decode_exp body }
    | `O (("lambda", _) :: _) as l -> RLambda (decode_lambda l)
    | `O [ "apply", `A [ exp; args ] ] ->
        RApply { exp = decode_exp exp; args = decode_list decode_param args }
    | `O [ "seq", el ] -> RSeq (decode_list decode_exp el)
    | `O [ "if", cond; "then", ifthen; "else", ifelse ] ->
        RIf { cond = decode_exp cond;
              ifthen = decode_exp ifthen; ifelse = decode_exp ifelse }
    | `O [ "match", arg; "with", cases ] ->
        RMatch { arg = decode_exp arg;
                 cases = decode_list
                          (decode_pair2 decode_pattern decode_exp) cases }
    | `O [ "construct", `O [ constr, args ];
           "type_appl", ctyps;
           "type_name", type_name ] ->
        RConstructor { type_name = decode_ident type_name; constr;
                       ctyps = decode_list TypeDecoder.decode_type ctyps;
                       args = decode_list decode_exp args }
    | `String "nil" -> RNil
    | `O [ "cons", `A [ el; e ] ] ->
        RList (decode_list decode_exp el, decode_exp e)
    | `O [ "tuple", el ] -> RTuple (decode_list decode_exp el)
    | `O [ "project", tuple; "on", indexes ] ->
        RProject { tuple = decode_exp tuple;
                   indexes = decode_list (Json.destruct int31) indexes }
    | `O [ "update", tuple; "with", updates ] ->
        RUpdate { tuple = decode_exp tuple;
                  updates = decode_list (decode_pair2 (Json.destruct int31)
                                          decode_exp) updates }
    | `O [ "record", fel; "type_name", type_name ] ->
        RRecord { type_name = decode_ident type_name;
                  contents = decode_str_x_list decode_exp fel }
    | `O [ "get", field; "of", record ] ->
        RGetField { record = decode_exp record; field = decode_string field }
    | `O [ "set", updates; "of", record ] ->
        RSetFields {
            record = decode_exp record;
            updates = decode_str_x_list decode_exp updates }
    | `O [ "packstruct", sr ] -> RPackStruct (decode_struct_ref sr)
    | `O [ "raise", `A [ exn; args; loc ] ] ->
        RRaise { exn = decode_exn_name exn;
                 args = decode_list decode_exp args;
                 loc = decode_option decode_loc loc }
    | `O [ "try", arg; "with", cases ] ->
        RTryWith { arg = decode_exp arg;
                   cases = decode_list (function
                             | `A [ exn; pl; e ] ->
                                (decode_exn_name exn,
                                 decode_list decode_pattern pl), decode_exp e
                             | _ -> raise (DecodingError "Bad trywith")) cases }
    | _ -> raise (DecodingError "Bad expression")

  and decode_content = function
    | `O [ "typekind", k; "typedef", `String n; "type", td ] ->
        n, RDefType (decode_type_kind k, TypeDecoder.decode_typedef td)
    | `O [ "exception", `String n; "args", tl ] ->
        n, RDefException (decode_list TypeDecoder.decode_type tl)
    | `O [ "init", `String n; "code", init_code;
           "type", init_typ; "persist", `Bool init_persist ] ->
        n, RInit { init_code = decode_exp init_code;
                   init_typ = TypeDecoder.decode_type init_typ;
                   init_persist }
    | `O [ "entry", `String n; "code", code;
           "fee_code", fee_code; "type", typ ] ->
        n, REntry { entry_code = decode_exp code;
                    entry_fee_code = decode_option decode_exp fee_code;
                    entry_typ = TypeDecoder.decode_type typ }
    | `O [ "view", `String n; "code", code; "type", typ; "recursive", recursive ] ->
        n, RView { view_code = decode_exp code;
                   view_typ = TypeDecoder.decode_type typ;
                   view_recursive = decode_rec recursive}
    | `O [ "value", `String n; "code", code; "type", typ;
           "visibility", visibility; "recursive", recursive ] ->
        n, RValue { value_code = decode_exp code;
                    value_typ = TypeDecoder.decode_type typ;
                    value_visibility = decode_visibility visibility;
                    value_recursive = decode_rec recursive }
    | `O [ "structure", `String n; "contents", s ] ->
        n, RStructure (decode_structure s)
    | `O [ "signature", `String n; "contents", s ] ->
        n, RSignature (TypeDecoder.decode_signature s)
    | _ -> raise (DecodingError "Bad content")

  and decode_structure = function
    | `O [ "kind", kind; "contents", contents; ] -> {
        structure_content = decode_list decode_content contents;
        kind = TypeDecoder.decode_kind kind
      }
    | _ -> raise (DecodingError "Bad structure")

  and decode_top_structure = function
    | `O [ "version", version; "code", code ] ->
      { version = decode_pair1 (Json.destruct int31) version;
        code = decode_structure code }
    | _ -> raise (DecodingError "Bad top structure")

  let const_encoding =
    conv encode_const decode_const json
  let structure_encoding =
    conv encode_structure decode_structure json
  let top_contract_encoding =
    conv encode_top_structure decode_top_structure json
  let exp_encoding =
    conv encode_exp decode_exp json
end




module ValueEncoder = struct

  open Love_value
  open Value
  open Op

  let rec encode_value (v : Love_value.Value.t) : Data_encoding.json =
    match Value.unrec_closure v with
    | VUnit -> `Null
    | VBool b -> `Bool b
    | VString s -> `String s
    | VBytes b -> `O [ "bytes", Json.construct bytes b ]
    | VInt i -> `O [ "int", Json.construct z i ]
    | VNat i -> `O [ "nat", Json.construct z i ]
    | VTuple vl -> `O [ "tuple", encode_list encode_value vl ]
    | VConstr (cn, vl) ->
       `O [ "constr", encode_pair2 (Json.construct string)
                        (encode_list encode_value) (cn, vl) ]
    | VRecord fcl ->
       `O [ "record", encode_str_x_list encode_value fcl ]
    | VList vl -> `O [ "list", encode_list encode_value vl ]
    | VSet vs -> `O [ "set", encode_list encode_value (ValueSet.elements vs) ]
    | VMap vm ->
       `O [ "map", encode_list (encode_pair1 encode_value)
                     (ValueMap.bindings vm) ]
    | VBigMap { id; diff; key_type; value_type } ->
       `O [ "bigmap", encode_option (Json.construct z) id;
            "key_type", TypeEncoder.encode_type key_type;
            "val_type", TypeEncoder.encode_type value_type;
            "diff", encode_list (encode_pair2 encode_value
                                   (encode_option encode_value))
              (ValueMap.bindings diff) ]
    | VDun d -> `O [ "mudun", Json.construct Tez_repr.encoding d ]
    | VKey k -> `O [ "key", Json.construct Signature.Public_key.encoding k ]
    | VKeyHash kh ->
       `O [ "pkh", Json.construct Signature.Public_key_hash.encoding kh ]
    | VSignature s -> `O [ "sig", Json.construct Signature.encoding s ]
    | VTimestamp t ->
       `O [ "time", Json.construct z (Script_timestamp_repr.to_zint t) ]
    | VAddress s -> `O [ "addr", Json.construct Contract_repr.encoding s ]
    | VOperation op -> `O [ "op", encode_op op ]
    | VPackedStructure (p, c) ->
       `O [ "packed", encode_pair2
              encode_ident encode_live_contract (p, c) ]
    | VContractInstance (s, a) ->
       `O [ "inst", Json.construct Contract_repr.encoding a;
            "sig", TypeEncoder.encode_signature s ]
    | VEntryPoint (a, e) ->
       `O [ "entry", `String e;
            "addr", Json.construct Contract_repr.encoding a ]
    | VView (a, e) ->
       `O [ "view", `String e;
            "addr", Json.construct Contract_repr.encoding a ]
    | VClosure { call_env; lambda = { args; body } } ->
       let values = encode_list
           (encode_pair2 encode_ident
              (encode_local_or_global encode_value
                 (encode_pointer_or_inlined encode_value)))
           call_env.values in
       let structs = encode_list
           (encode_pair2 encode_ident
              (encode_pointer_or_inlined encode_live_structure))
           call_env.structs in
       let sigs = encode_list
           (encode_pair2 encode_ident
              (encode_pointer_or_inlined TypeEncoder.encode_signature))
           call_env.sigs in
       let exns = encode_list
           (encode_pair1 encode_ident) call_env.exns in
       let types = encode_list
           (encode_pair1 encode_ident) call_env.types in
       let tvars = encode_str_x_list TypeEncoder.encode_type call_env.tvars in
       let args = encode_list RuntimeAst.encode_binding args in
       let body = RuntimeAst.encode_exp body in
       `O [ "closure", values; "structs", structs; "sigs", sigs; "exns", exns;
            "types", types; "tvars", tvars; "args", args; "body", body ]
    | VPrimitive (p, xa, vtl) ->
      `O [ "prim", `A [ PrimEncoder.encode_prim (p, xa);
                        encode_list encode_val_or_type vtl ] ]

  and encode_val_or_type (v : Love_value.Value.val_or_type) :
        Data_encoding.json =
    match v with
    | V v -> encode_value v
    | T t -> `O [ "type", TypeEncoder.encode_type t ]

  and encode_local_or_global :
    type t1 t2. (t1 -> json) -> (t2 -> json) ->
      (t1, t2) Value.local_or_global -> json =
    fun encode_local encode_global -> function
    | Local l -> `O [ "local", encode_local l ]
    | Global g -> `O [ "global", encode_global g ]

  and encode_pointer_or_inlined :
    type t. (t -> json) -> t Value.ptr_or_inlined -> json =
    fun encode_content -> function
    | Inlined (p, c) ->
        `O [ "inlined", encode_pair2 encode_ident encode_content (p, c) ]
    | Pointer p -> `O [ "pointer", encode_ident p ]

  and encode_manager_operation = function
    | Origination { delegate; script; credit; preorigination } ->
       `O [ "origination",
            `O [ "delegate", encode_option
                   (Json.construct Signature.Public_key_hash.encoding) delegate;
                 "script", encode_pair2 encode_value
                   encode_live_contract script;
                 "credit", Json.construct Tez_repr.encoding credit;
                 "preorigination", encode_option
                   (Json.construct Contract_repr.encoding) preorigination ] ]
    | Transaction { amount; parameters; entrypoint; destination } ->
       `O [ "transaction",
            `O [ "amount", Json.construct Tez_repr.encoding amount;
                 "parameters", encode_option encode_value parameters;
                 "entrypoint", encode_string entrypoint;
                 "destination", Json.construct
                                  Contract_repr.encoding destination ] ]
    | Delegation d ->
       `O [ "delegation", encode_option
                        (Json.construct Signature.Public_key_hash.encoding) d ]
    | Dune_manage_account
        { target; maxrolls; admin; white_list; delegation } ->
       `O [ "dune_manage_account",
            `O [ "target", encode_option ((Json.construct
                              Signature.Public_key_hash.encoding)) target;
                 "maxrolls",
                     encode_option (encode_option (
                     Json.construct int31)) maxrolls;
                 "admin",
                     encode_option (encode_option (
                     Json.construct Contract_repr.encoding)) admin;
                 "whitelist",
                     encode_option (encode_list (
                     Json.construct Contract_repr.encoding)) white_list;
                 "delegation",
                 encode_option (Json.construct bool) delegation ] ]

  and encode_internal_op { source; operation; nonce } =
    `O [ "source", Json.construct Contract_repr.encoding source;
         "operation", encode_manager_operation operation;
         "nonce", Json.construct int31 nonce ]

  and encode_big_map_diff_item = function
    | Update { big_map; diff_key; diff_key_hash; diff_value } ->
        `O [ "update", Json.construct z big_map;
             "diff_key", encode_value diff_key;
             "diff_key_hash", Json.construct
               Script_expr_hash.encoding diff_key_hash;
             "diff_value", encode_option encode_value diff_value ]
    | Clear id ->
        `O [ "clear", Json.construct z id ]
    | Copy (ids, idd) ->
        `O [ "copy", `A [ Json.construct z ids; Json.construct z idd ] ]
    | Alloc { big_map; key_type; value_type }  ->
        `O [ "alloc", Json.construct z big_map;
             "key_type", TypeEncoder.encode_type key_type;
             "value_type", TypeEncoder.encode_type value_type ]

  and encode_op (operation, big_map_diff_opt) =
    `A [ encode_internal_op operation;
         encode_option (encode_list encode_big_map_diff_item) big_map_diff_opt ]

  and encode_content n =
    let open LiveStructure in
    function
    | VType (k, td) ->
       `O [ "typedef", `String n;
            "kind", encode_type_kind k;
            "type", TypeEncoder.encode_typedef td ]
    | VException tl ->
       `O [ "exception", `String n;
            "args", encode_list TypeEncoder.encode_type tl ]
    | VInit { vinit_code; vinit_typ; vinit_persist } ->
       `O [ "init", `String n;
            "code", encode_value vinit_code;
            "type", TypeEncoder.encode_type vinit_typ;
            "persist", `Bool vinit_persist ]
    | VEntry { ventry_code; ventry_fee_code; ventry_typ } ->
       `O [ "entry", `String n;
            "code", encode_value ventry_code;
            "fee_code", encode_option encode_value ventry_fee_code;
            "type", TypeEncoder.encode_type ventry_typ ]
    | VView { vview_code; vview_typ; vview_recursive } ->
       `O [ "view", `String n;
            "code", encode_value vview_code;
            "type", TypeEncoder.encode_type vview_typ;
            "recursive", encode_rec vview_recursive;]
    | VValue { vvalue_code; vvalue_typ; vvalue_visibility; vvalue_recursive } ->
       `O [ "value", `String n;
            "code", encode_value vvalue_code;
            "type", TypeEncoder.encode_type vvalue_typ;
            "visibility", encode_visibility vvalue_visibility;
            "recursive", encode_rec vvalue_recursive ]
    | VStructure s ->
       `O [ "structure", `String n;
            "contents", encode_live_structure s ]
    | VSignature s ->
       `O [ "signature", `String n;
            "contents", TypeEncoder.encode_signature s ]

  and encode_live_structure { kind; content } =
    `O [ "kind", TypeEncoder.encode_kind kind;
         "contents", encode_list (fun (n, c) -> encode_content n c) content ]

  and encode_live_contract LiveContract.{ version; root_struct } =
    `O [ "version", encode_pair1 (Json.construct int31) version;
         "root_struct", encode_live_structure root_struct ]

  and encode_fee_code FeeCode.{ version; root_struct; fee_codes } =
    `O [ "version", encode_pair1 (Json.construct int31) version;
         "top_struct", encode_live_structure root_struct;
         "fee_codes", encode_list
           (encode_pair2 (Json.construct string)
              (encode_pair2 encode_value TypeEncoder.encode_type)) fee_codes ]

end

module ValueDecoder = struct

  open Love_value
  open Value
  open Op

  let rec decode_value (j : Data_encoding.json) : Love_value.Value.t =
    Value.rec_closure @@
    match j with
    | `O [ "parse", `String s ] ->
        Love_lexer.parse_value s |> value_of_value
    | `Null -> VUnit
    | `Bool b -> VBool b
    | `String s -> VString s
    | `O [ "bytes", b ] -> VBytes (Json.destruct bytes b)
    | `O [ "int", i ] -> VInt (Json.destruct z i)
    | `O [ "nat", i ] -> VNat (Json.destruct z i)
    | `O [ "tuple", vl ] -> VTuple (decode_list decode_value vl)
    | `O [ "constr", `A [ cn; vl ] ] ->
       VConstr (Json.destruct string cn, decode_list decode_value vl)
    | `O [ "record", fcl ] ->
       VRecord (decode_str_x_list decode_value fcl)
    | `O [ "list", vl ] -> VList (decode_list decode_value vl)
    | `O [ "set", vl ] -> VSet (ValueSet.of_list (decode_list decode_value vl))
    | `O [ "map", bl ] ->
       VMap (Utils.bindings_to_map ValueMap.add ValueMap.empty
               (decode_list (decode_pair1 decode_value) bl))
    | `O [ "bigmap", id; "key_type", key_type;
           "val_type", value_type; "diff", diff ] ->
       VBigMap { id = decode_option (Json.destruct z) id;
                 diff = Utils.bindings_to_map ValueMap.add ValueMap.empty
                     (decode_list (decode_pair2 decode_value
                                     (decode_option decode_value)) diff);
                 key_type = TypeDecoder.decode_type key_type;
                 value_type = TypeDecoder.decode_type value_type }
    | `O [ "dun", d ]
    | `O [ "mudun", d ] -> VDun (Json.destruct Tez_repr.encoding d)
    | `O [ "key", k ] -> VKey (Json.destruct Signature.Public_key.encoding k)
    | `O [ "keyhash", kh ]
    | `O [ "pkh", kh ] ->
       VKeyHash (Json.destruct Signature.Public_key_hash.encoding kh)
    | `O [ "signature", s ]
    | `O [ "sig", s ] -> VSignature (Json.destruct Signature.encoding s)
    | `O [ "timestamp", t ]
    | `O [ "time", t ] ->
       VTimestamp (Script_timestamp_repr.of_zint (Json.destruct z t))
    | `O [ "address", s ]
    | `O [ "addr", s ] -> VAddress (Json.destruct Contract_repr.encoding s)
    | `O [ "operation", op ]
    | `O [ "op", op ] -> VOperation (decode_op op)
    | `O [ "packedstruct", pc ]
    | `O [ "packed", pc ] ->
       let p, c = decode_pair2 decode_ident decode_live_contract pc in
       VPackedStructure (p, c)
    | `O [ "instance", a; "signature", s ]
    | `O [ "inst", a; "sig", s ] ->
       VContractInstance (TypeDecoder.decode_signature s,
                          Json.destruct Contract_repr.encoding a)
    | `O [ "entry", `String e; "addr", a ] ->
       VEntryPoint (Json.destruct Contract_repr.encoding a, e)
    | `O [ "view", `String e; "addr", a ] ->
       VView (Json.destruct Contract_repr.encoding a, e)
    | `O [ "closure", values; "structs", structs; "sigs", sigs; "exns", exns;
           "types", types; "tvars", tvars; "args", args; "body", body ] ->
       let values = decode_list
           (decode_pair2 decode_ident
              (decode_local_or_global decode_value
                 (decode_pointer_or_inlined decode_value))) values in
       let structs = decode_list
           (decode_pair2 decode_ident
              (decode_pointer_or_inlined decode_live_structure)) structs in
       let sigs = decode_list
           (decode_pair2 decode_ident
              (decode_pointer_or_inlined TypeDecoder.decode_signature)) sigs in
       let exns = decode_list
           (decode_pair1 decode_ident) exns in
       let types = decode_list
           (decode_pair1 decode_ident) types in
       let tvars = decode_str_x_list TypeDecoder.decode_type tvars in
       let args = decode_list RuntimeAst.decode_binding args in
       let body = RuntimeAst.decode_exp body in
       VClosure { call_env = { values; structs; sigs; exns; types; tvars };
                  lambda = { args; body }}
    | `O [ "primitive", `A [ p; vl ] ]
    | `O [ "prim", `A [ p; vl ] ] ->
       let p, xl = PrimDecoder.decode_prim p in
       VPrimitive (p, xl, decode_list decode_val_or_type vl)
    | `O ((s,_) :: _) -> raise (DecodingError ("Bad value " ^ s))
    | _ -> raise (DecodingError "Bad value")

  and decode_val_or_type (j : Data_encoding.json) :
        Love_value.Value.val_or_type =
    match j with
    | `O [ "type", t ] -> T (TypeDecoder.decode_type t)
    | _ -> V (decode_value j)

  and decode_local_or_global :
    type t1 t2. (json -> t1) -> (json -> t2) -> json ->
      (t1, t2) Value.local_or_global =
    fun decode_local decode_global -> function
    | `O [ "local", l ] -> Local (decode_local l)
    | `O [ "global", g ] -> Global (decode_global g)
    | _ -> raise (DecodingError "Bad local or global")

  and decode_pointer_or_inlined :
    type t. (json -> t) -> json -> t Value.ptr_or_inlined =
    fun decode_content -> function
    | `O [ "inlined", pc ] ->
        let p, c = decode_pair2 decode_ident decode_content pc in
        Inlined (p, c)
    | `O [ "pointer", p ] -> Pointer (decode_ident p)
    | _ -> raise (DecodingError "Bad pointer or inlined")

  and decode_manager_operation = function
    | `O [ "origination",
           `O [ "delegate", delegate; "script", script;
                "credit", credit; "preorigination", preorigination ] ] ->
       Origination { delegate =
                       decode_option (Json.destruct
                         Signature.Public_key_hash.encoding) delegate;
                     script = decode_pair2 decode_value
                                decode_live_contract script;
                     credit = Json.destruct Tez_repr.encoding credit;
                     preorigination = decode_option
                                        (Json.destruct Contract_repr.encoding)
                                        preorigination }
    | `O [ "transaction",
           `O [ "amount", amount; "parameters", parameters;
                "entrypoint", entrypoint; "destination", destination ] ] ->
       Transaction { amount = Json.destruct Tez_repr.encoding amount;
                     parameters = decode_option decode_value parameters;
                     entrypoint = decode_string entrypoint;
                     destination = Json.destruct
                                     Contract_repr.encoding destination }
    | `O [ "delegation", d ] ->
       Delegation (decode_option
                     (Json.destruct Signature.Public_key_hash.encoding) d)

    | `O [ "dune_manage_account",
           `O [ "target", target; "maxrolls", maxrolls; "admin", admin;
                 "whitelist", white_list; "delegation", delegation ] ] ->
      Dune_manage_account {
        target = decode_option (Json.destruct
                                  Signature.Public_key_hash.encoding) target;
        maxrolls = decode_option (decode_option (
                     Json.destruct int31)) maxrolls;
        admin = decode_option (decode_option (
                     Json.destruct Contract_repr.encoding)) admin;
        white_list = decode_option (decode_list (
                     Json.destruct Contract_repr.encoding)) white_list;
        delegation = decode_option (Json.destruct bool) delegation }
    | _ -> raise (DecodingError "Bad manager operation")

  and decode_internal_op = function
    | `O [ "source", source ; "operation", operation ; "nonce", nonce ] ->
       { source = Json.destruct Contract_repr.encoding source;
         operation = decode_manager_operation operation;
         nonce = Json.destruct int31 nonce }
    | _ -> raise (DecodingError "Bad internal operation")

  and decode_big_map_diff_item = function
    | `O [ "update", big_map; "diff_key", diff_key;
           "diff_key_hash", diff_key_hash; "diff_value", diff_value ] ->
        Update { big_map = Json.destruct z big_map;
                 diff_key = decode_value diff_key;
                 diff_key_hash = Json.destruct
                     Script_expr_hash.encoding diff_key_hash;
                 diff_value = decode_option decode_value diff_value }
    | `O [ "clear", id ] ->
        Clear (Json.destruct z id)
    | `O [ "copy", `A [ ids; idd ] ] ->
        Copy (Json.destruct z ids, Json.destruct z idd)
    | `O [ "alloc", big_map; "key_type", key_type;
           "value_type", value_type ] ->
        Alloc { big_map = Json.destruct z big_map;
                key_type = TypeDecoder.decode_type key_type;
                value_type = TypeDecoder.decode_type value_type }
    | _ -> raise (DecodingError "Bad big map diff item")

  and decode_op = function
    | `A [ operation; big_map_diff_opt ] ->
      (decode_internal_op operation,
       decode_option (decode_list decode_big_map_diff_item) big_map_diff_opt)
    | _ -> raise (DecodingError "Bad operation")

  and decode_content =
    let open LiveStructure in
    function
    | `O [ "typedef", `String n; "kind", k; "type", td ] ->
        n, VType (decode_type_kind k, TypeDecoder.decode_typedef td)
    | `O [ "exception", `String n; "args", tl ] ->
        n, VException (decode_list TypeDecoder.decode_type tl)
    | `O [ "init", `String n; "code", code;
           "type", typ; "persist", `Bool persist ] ->
        n, VInit { vinit_code = decode_value code;
                   vinit_typ = TypeDecoder.decode_type typ;
                   vinit_persist = persist }
    | `O [ "entry", `String n; "code", code;
           "fee_code", fee_code; "type", typ ] ->
        n, VEntry { ventry_code = decode_value code;
                    ventry_fee_code = decode_option decode_value fee_code;
                    ventry_typ = TypeDecoder.decode_type typ }
    | `O [ "view", `String n; "code", code; "type", typ; "recursive", recursive ] ->
        n, VView { vview_code = decode_value code;
                   vview_typ = TypeDecoder.decode_type typ;
                   vview_recursive = decode_rec recursive
                 }
    | `O [ "value", `String n; "code", code; "type", typ;
           "visibility", visibility; "recursive", recursive ] ->
        n, VValue { vvalue_code = decode_value code;
                    vvalue_typ = TypeDecoder.decode_type typ;
                    vvalue_visibility = decode_visibility visibility;
                    vvalue_recursive = decode_rec recursive }
    | `O [ "structure", `String n; "contents", s ] ->
        n, VStructure (decode_live_structure s)
    | `O [ "signature", `String n; "contents", s ] ->
        n, VSignature (TypeDecoder.decode_signature s)
    | _ -> raise (DecodingError "Bad content")

  and decode_live_structure = function
    | `O [ "kind", kind; "contents", contents; ] -> {
        kind = TypeDecoder.decode_kind kind;
        content = decode_list decode_content contents; }
    | _ -> raise (DecodingError "Bad live structure")

  and decode_live_contract = function
    | `O [ "version", version; "root_struct", root_struct ] ->
      LiveContract.{ version = decode_pair1 (Json.destruct int31) version;
                     root_struct = decode_live_structure root_struct }
    | _ -> raise (DecodingError "Bad live contract")

  and decode_fee_code = function
    | `O [ "version", version; "root_struct", root_struct;
           "fee_codes", fee_codes ] ->
      FeeCode.{ version = decode_pair1 (Json.destruct int31) version;
                root_struct = decode_live_structure root_struct;
                fee_codes = decode_list
                    (decode_pair2 (Json.destruct string)
                       (decode_pair2 decode_value TypeDecoder.decode_type)
                    ) fee_codes }
    | _ -> raise (DecodingError "Bad fee code")

end

module Value = struct
  let encoding =
    conv ValueEncoder.encode_value
      ValueDecoder.decode_value json
  let live_structure_encoding =
    conv ValueEncoder.encode_live_structure
      ValueDecoder.decode_live_structure json
  let live_contract_encoding =
    conv ValueEncoder.encode_live_contract
      ValueDecoder.decode_live_contract json
  let fee_code_encoding =
    conv ValueEncoder.encode_fee_code
      ValueDecoder.decode_fee_code json
end
