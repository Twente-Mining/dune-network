(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module Operation_shell = Operation
open Alpha_context
open Apply_results

let path = RPC_path.(open_root / "dune")

module S = struct
  open Data_encoding

  let build_operation =
    RPC_service.post_service
      ~description: "Forge an operation with minimal input"
      ~query: RPC_query.empty
      ~input: (obj3
                 (req "operations" Operation.contents_list_encoding)
                 (req "chain_id" Chain_id.encoding)
                 (dft "keys" (list Signature.Public_key.encoding) [])
              )
      ~output:
        (obj2
           (req "result" Apply_results.operation_data_and_metadata_encoding)
           (req "binary" bytes))
      RPC_path.(path / "build_operation")
end

let build_operation ctxt block =
  RPC_context.make_call0 S.build_operation ctxt block ()

open Services_registration

type source_info = {
  mutable op_counter : counter ;
}

let minimal_fee_factor n = n * Config.config.minimal_fee_factor
let minimal_fees = match
    minimal_fee_factor 100 |> Int64.of_int |> Tez.of_mutez
  with None -> assert false | Some t -> t
let minimal_nanotez_per_gas_unit = minimal_fee_factor 100 |> Z.of_int
let minimal_nanotez_per_byte = minimal_fee_factor 1000 |> Z.of_int

(* BEGIN from lib_client/injection.ml *)

let estimated_gas_single
    (type kind)
    (Manager_operation_result { operation_result ;
                                internal_operation_results ; _ }
     : kind Kind.manager contents_result) =
  let consumed_gas (type kind) (result : kind manager_operation_result) =
    match result with
    | Applied (Transaction_result { consumed_gas ; _ }) -> Ok consumed_gas
    | Applied (Origination_result { consumed_gas ; _ }) -> Ok consumed_gas
    | Applied (Reveal_result { consumed_gas }) -> Ok consumed_gas
    | Applied (Delegation_result { consumed_gas }) -> Ok consumed_gas
    | Applied (Dune_manager_operation_result { consumed_gas ; _ }) -> Ok consumed_gas
    | Skipped _ -> assert false
    | Backtracked (_, None) -> Ok Z.zero (* there must be another error for this to happen *)
    | Backtracked (_, Some errs) -> Error errs
    | Failed (_, errs) -> Error errs in
  List.fold_left
    (fun acc (Internal_operation_result (_, r)) ->
       acc >>? fun acc ->
       consumed_gas r >>? fun gas ->
       Ok (Z.add acc gas))
    (consumed_gas operation_result) internal_operation_results

let estimated_storage_single
    (type kind)
    origination_size
    (Manager_operation_result { operation_result ;
                                internal_operation_results ;
                                _ }
     : kind Kind.manager contents_result) =
  let storage_size_diff (type kind) ~internal (result : kind manager_operation_result) =
    match result with
    | Applied (Transaction_result { paid_storage_size_diff ; allocated_destination_contract ; collect_call_revelation ;_ }) ->
        let paid_storage_size_diff =
          if collect_call_revelation && not internal then
            Z.add paid_storage_size_diff origination_size
          else paid_storage_size_diff in
        if allocated_destination_contract then
          Ok (Z.add paid_storage_size_diff origination_size)
        else
          Ok paid_storage_size_diff
    | Applied (Origination_result { paid_storage_size_diff ; _ }) ->
        Ok (Z.add paid_storage_size_diff origination_size)
    | Applied (Reveal_result _)-> Ok Z.zero
    | Applied (Delegation_result _) -> Ok Z.zero
    | Applied (Dune_manager_operation_result { paid_storage_size_diff ; _ }) ->
        Ok paid_storage_size_diff
    | Skipped _ -> assert false
    | Backtracked (_, None) -> Ok Z.zero (* there must be another error for this to happen *)
    | Backtracked (_, Some errs) -> Error errs
    | Failed (_, errs) -> Error errs in
  List.fold_left
    (fun acc (Internal_operation_result (_, r)) ->
       acc >>? fun acc ->
       storage_size_diff ~internal:true r >>? fun storage ->
       Ok (Z.add acc storage))
    (storage_size_diff ~internal:false operation_result) internal_operation_results

let rec patch_fee :
  type kind. bool -> kind contents -> kind contents = fun first -> function
  | Manager_operation c as op ->
      let gas_limit = c.gas_limit in
      let size =
        if first then
          Data_encoding.Binary.fixed_length_exn
            Operation_shell.shell_header_encoding +
          Data_encoding.Binary.length
            Operation.contents_encoding
            (Contents op) +
          Signature.size
        else
          Data_encoding.Binary.length
            Operation.contents_encoding
            (Contents op)
      in
      let minimal_fees_in_nanotez =
        Z.mul (Z.of_int64 (Tez.to_mutez minimal_fees)) (Z.of_int 1000) in
      let minimal_fees_for_gas_in_nanotez =
        Z.mul minimal_nanotez_per_gas_unit gas_limit in
      let minimal_fees_for_size_in_nanotez =
        Z.mul minimal_nanotez_per_byte (Z.of_int size) in
      let fees_in_nanotez =
        Z.add minimal_fees_in_nanotez @@
        Z.add minimal_fees_for_gas_in_nanotez minimal_fees_for_size_in_nanotez in
      begin match Tez.of_mutez (Z.to_int64 (Z.div (Z.add (Z.of_int 999) fees_in_nanotez) (Z.of_int 1000))) with
        | None -> assert false
        | Some fee ->
            if Tez.( fee <= c.fee ) then
              op
            else
              patch_fee first (Manager_operation { c with fee })
      end
  | c -> c

(********** END from lib_client/injection.ml *)

let do_build_operation
    { block_hash ; block_header = _ ; context = ctxt } ()
    ( initial_contents , chain_id, keys ) =

  let keys =
    let r = ref Signature.Public_key_hash.Map.empty in
    List.iter (fun key ->
        let pkh = Signature.Public_key.hash key in
        r := Signature.Public_key_hash.Map.add pkh key !r
      ) keys;
    !r
  in
  let revelations = ref [] in
  let constants = Constants.parametric ctxt in
  let source_infos = ref Signature.Public_key_hash.Map.empty in
  let get_source_info source =
    Signature.Public_key_hash.Map.find source !source_infos
  in
  let register_source ctxt source counter =
    match get_source_info source with
    | _info -> return ()
    | exception Not_found ->
        Contract.get_counter ctxt source >>=? fun op_counter ->
        Contract.is_manager_key_revealed ctxt source >>=? fun is_revealed ->
        begin
          if is_revealed then return ()
          else
            match Signature.Public_key_hash.Map.find source keys with
            | key ->
                let operation = Reveal key in
                revelations :=
                  ( Contents ( Manager_operation {
                        source ;
                        fee = Tez.zero ;
                        counter ;
                        operation ;
                        gas_limit = Z.zero ;
                        storage_limit = Z.zero ;
                      } )) :: !revelations ;
                return ()
            | exception Not_found ->
                Dune_misc.failwith "Key for %s not revealed"
                  (Signature.Public_key_hash.to_b58check source)
        end >>=? fun () ->
        let info = {
          op_counter ;
        } in
        source_infos := Signature.Public_key_hash.Map.add source info
            !source_infos ;
        return ()
  in
  let operations = Operation.to_list initial_contents in
  let rec iter list =
    match list with
    | [] -> return ()
    | op :: tail ->
        begin
          match op with
          | Contents ( Manager_operation {
              source ;
              fee = _ ;
              counter ;
              operation = _ ;
              gas_limit = _ ;
              storage_limit = _ ;
            }) ->
              register_source ctxt source counter
          | _ -> return ()
        end >>=? fun () ->
        iter tail
  in
  iter operations >>=? fun () ->
  let operations = !revelations @ operations in
  List.fold_left (fun acc op ->
      acc >>=? fun acc ->
      begin
        match op with
        | Contents ( Manager_operation {
            source ;
            fee ;
            counter ;
            operation ;
            gas_limit = _ ;
            storage_limit = _ ;
          }) ->
            let info = get_source_info source in
            info.op_counter <- Z.add counter Z.one ;
            let op = Contents ( Manager_operation {
                source ;
                fee ;
                counter = Z.add counter info.op_counter ;
                operation ;
                gas_limit = constants.hard_gas_limit_per_operation ;
                storage_limit = constants.hard_storage_limit_per_operation ;
              } )
            in
            return op
        | _ -> return op
      end >>=? fun op ->
      return ( op :: acc )
    ) (return []) operations >>=? fun contents ->
  let contents = List.rev contents in
  let Contents_list contents_list = Operation.of_list contents in
  let protocol_data = Operation_data {
      contents = contents_list ; signature = None } in
  let shell = { Operation_shell.branch = block_hash } in
  let op = { shell ; protocol_data }
  in
  Helpers_services.Scripts.do_run_operation ctxt () ( op, chain_id ) >>=?
  fun ( _packed_protocol_data , packed_operation_metadata ) ->
  match packed_operation_metadata with
  | No_operation_metadata -> Dune_misc.failwith "internal error HELSER893"
  | Operation_metadata { contents = results } ->
      let results = Apply_results.to_list (Contents_result_list results)
      in
      let first = ref true in
      let maybe_first () =
        if !first then begin
          first := false;
          true
        end else
          false
      in
      let rec iter2 ops res =
        match ops, res with
        | [], [] -> return []
        | [], _
        | _, [] ->
            Dune_misc.failwith "internal error: HELSER902"
        | op :: ops, r :: res ->
            begin
              match op, r with
              | Contents ( Endorsement _ ),
                Apply_results.Contents_result (
                  Apply_results.Endorsement_result _ ) ->
                  return op
              | Contents ( Seed_nonce_revelation _ ),
                Apply_results.Contents_result (
                  Apply_results.Seed_nonce_revelation_result _ ) ->
                  return op
              | Contents ( Double_endorsement_evidence _ ),
                Apply_results.Contents_result (
                  Apply_results.Double_endorsement_evidence_result _ ) ->
                  return op
              | Contents ( Double_baking_evidence _ ),
                Apply_results.Contents_result (
                  Apply_results.Double_baking_evidence_result _ ) ->
                  return op
              | Contents ( Activate_account _ ),
                Apply_results.Contents_result (
                  Apply_results.Activate_account_result _ ) ->
                  return op
              | Contents ( Proposals _ ),
                Apply_results.Contents_result (
                  Apply_results.Proposals_result ) ->
                  return op
              | Contents ( Ballot _ ),
                Apply_results.Contents_result (
                  Apply_results.Ballot_result ) ->
                  return op
              | Contents ( Manager_operation {
                  source ; counter ; operation ; fee
                } ),
                Apply_results.Contents_result
                  ( Apply_results.Manager_operation_result _ as res ) ->
                  begin
                    estimated_gas_single res
                    >>? fun consumed_gas ->
                    estimated_storage_single
                      (Z.of_int constants.origination_size)
                      res
                    >>? fun storage_size ->
                    let gas_limit =
                      Z.min (Z.add consumed_gas (Z.of_int 100))
                        constants.hard_gas_limit_per_operation in
                    let storage_limit =
                      Z.min (Z.add storage_size (Z.of_int 20))
                        constants.hard_storage_limit_per_operation in
                    let op = Manager_operation
                        {
                          source ;
                          counter ;
                          operation ;

                          fee ;
                          gas_limit ;
                          storage_limit ;
                        }
                    in
                    let op = patch_fee (maybe_first()) op in
                    Ok ( Contents op )
                  end |> Lwt.return
              | _ ->
                  Dune_misc.failwith "internal error HELSER946"
            end >>=? fun op ->
            iter2 ops res >>=? fun ops ->
            return ( op :: ops )
      in
      iter2 contents results >>=? fun contents ->
      let contents_list = Operation.of_list contents in
      let binary = Data_encoding.Binary.to_bytes_exn
          Operation.unsigned_encoding (shell, contents_list) in
      let packed_protocol_data =
        let Contents_list contents = contents_list in
        Operation_data {
          contents ;
          signature = None ;
        }
      in
      return ( ( packed_protocol_data, packed_operation_metadata ), binary)

let register () =
  register0_fullctxt S.build_operation do_build_operation
