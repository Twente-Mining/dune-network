(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_ast_types

val pack_data :
  Love_context.t -> Love_value.Value.t ->
  (Love_context.t * MBytes.t) tzresult Lwt.t

val unpack_data :
  Love_context.t ->
  TYPE.t -> MBytes.t ->
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t

val hash_data :
  Love_context.t ->
  Love_value.Value.t ->
  (Love_context.t * Script_expr_hash.t) tzresult Lwt.t

val empty_big_map :
  TYPE.t -> TYPE.t -> Love_value.Value.bigmap

val big_map_mem :
  Love_context.t ->
  Love_value.Value.bigmap ->
  Love_value.ValueMap.key -> (Love_context.t * bool) tzresult Lwt.t

val big_map_get :
  Love_context.t ->
  Love_value.Value.bigmap ->
  Love_value.ValueMap.key ->
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t

val big_map_add :
  Love_context.t ->
  Love_value.Value.bigmap ->
  Love_value.ValueMap.key ->
  Love_value.Value.t ->
  (Love_context.t * Love_value.Value.bigmap) tzresult Lwt.t

val big_map_del :
  Love_context.t ->
  Love_value.Value.bigmap ->
  Love_value.ValueMap.key ->
  (Love_context.t * Love_value.Value.bigmap) tzresult Lwt.t

val collect_big_maps :
  Love_context.t ->
  Love_value.Value.t ->
  (Love_pervasives.Collections.ZSet.t * Love_context.t) tzresult Lwt.t

val extract_big_map_diff :
  Love_context.t ->
  temporary:bool ->
  to_duplicate:Love_pervasives.Collections.ZSet.t ->
  to_update:Love_pervasives.Collections.ZSet.t ->
  Love_value.Value.t ->
  (Love_value.Value.t *
   Love_value.Op.big_map_diff_item list option *
   Love_context.t) tzresult Lwt.t

val contract_at :
  Love_context.t ->
  Love_env.t ->
  Love_context.ContractMap.key ->
  TYPE.structure_type ->
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t

val register_primitive :
  Love_primitive.generic_primitive ->
  Alpha_context.Gas.cost ->
  (Love_context.t ->
   Love_env.t ->
   Love_primitive.generic_primitive ->
   TYPE.extended_arg ->
   Love_value.Value.val_or_type list ->
   (Love_context.t * Love_value.ValueSet.elt) tzresult Lwt.t) ->
  unit

val eval :
  Love_context.t ->
  Love_env.t ->
  (Love_primitive.t * TYPE.extended_arg) ->
  Love_value.Value.val_or_type list ->
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t
