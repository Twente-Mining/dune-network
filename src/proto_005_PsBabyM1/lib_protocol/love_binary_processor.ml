(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions
open Collections
open Love_ast_types
open Love_ast_types.AST


module Serializer = struct

  module LBB = Love_binary_buffer

  (** Combining serializers **)

  let ser_x_option ser_x state = function
    | None -> LBB.write_bool state false
    | Some x -> LBB.write_bool state true; ser_x state x

  let ser_list_size state l =
    let l = List.length l in
    if Compare.Int.(l = 0) then (* size = 0 *)
      LBB.write state 0L 2 (* 00 *)
    else if Compare.Int.(l < 9) then begin (* size = 1 - 8 *)
      LBB.write state 1L 1; (* 1 *)
      LBB.write state (Int64.of_int (l - 1)) 4;
    end
    else begin (* size >= 9 *)
      LBB.write state 1L 2; (* 01 *)
      LBB.write_uint state (l - 9)
    end

  let ser_x_list ser_x state xl =
    (* LBB.write_uint state (List.length xl); *)
    ser_list_size state xl;
    List.iter (fun x -> ser_x state x) xl

  let ser_x_y_list ser_x ser_y state xyl =
    (* LBB.write_uint state (List.length xyl); *)
    ser_list_size state xyl;
    List.iter (fun (x, y) -> ser_x state x; ser_y state y) xyl

  let ser_x_x_list ser_x state xxl =
    (* LBB.write_uint state (List.length xxl); *)
    ser_list_size state xxl;
    List.iter (fun (x1, x2) -> ser_x state x1; ser_x state x2) xxl

  let ser_x_x_pair ser_x state (x1,x2) =
    ser_x state x1; ser_x state x2

  let ser_x_y_pair ser_x ser_y state (x, y) =
    ser_x state x; ser_y state y


  (** Utility serializers **)

  let ser_ident state ident =
    let l = Ident.get_list ident in
    ser_x_list LBB.write_str_repr state l

  let ser_ident_x_list ser_x state ixl =
    ser_x_y_list ser_ident ser_x state ixl

  let ser_str_repr_x_list ser_x state sxl =
    ser_x_y_list LBB.write_str_repr ser_x state sxl

  let ser_str_repr_id_x_list ser_x state sxl =
    ser_x_y_list ser_ident ser_x state sxl

  let ser_int_x_list ser_x state ixl =
    ser_x_y_list (fun s i -> LBB.write_uint s i) ser_x state ixl


  let ser_timestamp state ts =
    LBB.write_z state (Script_timestamp_repr.to_zint ts)

  let ser_int64 state di64 =
    Log.debug "[ser_dun] Serializing dun value %Ld@." di64;
    LBB.write_z state (Z.of_int64 di64)

  let ser_dun state d =
    let di64 = Tez_repr.to_mutez d in
    ser_int64 state di64

  let ser_key state key =
    let open Signature in
    match Data_encoding.Binary.to_bytes Public_key.encoding key with
    | None -> failwith "Bad key"
    | Some b -> LBB.write_bytes state b

  let ser_key_hash state key_hash =
    let open Signature in
    match Data_encoding.Binary.to_bytes Public_key_hash.encoding key_hash with
    | None -> failwith "Bad key hash"
    | Some b -> LBB.write_bytes state b

  let ser_signature state sign =
    match Data_encoding.Binary.to_bytes Signature.encoding sign with
    | None -> failwith "Bad signature"
    | Some b -> LBB.write_bytes state b

  let ser_address state a =
    match Data_encoding.Binary.to_bytes Contract_repr.encoding a with
    | None -> failwith "Bad address"
    | Some b -> LBB.write_bytes state b

  let ser_script_expr_hash state sh =
    match Data_encoding.Binary.to_bytes Script_expr_hash.encoding sh with
    | None -> failwith "Bad script hash"
    | Some b -> LBB.write_bytes state b


  (** Main serializer *)

  let ser serial e =
    let state = LBB.initialize_writer () in
    serial state e;
    (* Format.printf "%a" LBB.print_stats state; *)
    LBB.finalize state


  (** Qualifier serializers **)

  let ser_visibility state =
    function
    | Private -> LBB.write_bool state false
    | Public -> LBB.write_bool state true


  let ser_rec state = function
    | TYPE.NonRec -> LBB.write_bool state false
    | Rec -> LBB.write_bool state true


  (** Type serializers **)

  module Type = struct

    open TYPE

    let ser_type_name = ser_ident

    let ser_trait state { tcomparable } = LBB.write_bool state tcomparable

    let ser_type_var state TYPE.{ tv_name; tv_traits } =
      LBB.write_str_repr state tv_name;
      ser_trait state tv_traits

    let rec ser_type state =
      let ser_type_list = ser_x_list ser_type in
      let size = 4 (* => 16 types *) in
      let write state i =
        LBB.stats_add_i_custom state "Type" size;
        LBB.write state (Int64.of_int i) size in
      function
      | TUser (tn, tl) ->
        write state 0;
        ser_type_name state tn; ser_type_list state tl
      | TTuple tl ->
        write state 1;
        ser_type_list state tl
      | TArrow (t1, t2) ->
        write state 2;
        ser_type state t1; ser_type state t2
      | TVar tv ->
        write state 3;
        ser_type_var state tv
      | TForall (tv, t) ->
        write state 4;
        ser_type_var state tv; ser_type state t
      | TPackedStructure st ->
        write state 5;
        ser_structure_type state st
      | TContractInstance ct ->
        write state 6;
        ser_structure_type state ct

    and ser_structure_type state = function
      | Anonymous cs -> LBB.write_bool state false; ser_signature state cs
      | Named cn -> LBB.write_bool state true; ser_ident state cn

    and ser_typedef state = function
      | Alias { aparams; atype } ->
        LBB.write state Int64.zero 2;
        ser_x_list ser_type_var state aparams;
        ser_type state atype
      | SumType { sparams; scons; srec } ->
        LBB.write state Int64.one 2;
        ser_x_list ser_type_var state sparams;
        ser_str_repr_x_list (ser_x_list ser_type) state scons;
        ser_rec state srec
      | RecordType { rparams; rfields; rrec } ->
        LBB.write state (Int64.of_int 2) 2;
        ser_x_list ser_type_var state rparams;
        ser_str_repr_x_list ser_type state rfields;
        ser_rec state rrec

    and ser_sigtype state = function
      | SPublic td ->
        LBB.write state Int64.zero 2;
        ser_typedef state td
      | SPrivate td ->
        LBB.write state Int64.one 2;
        ser_typedef state td
      | SAbstract tl ->
        LBB.write state (Int64.of_int 2) 2;
        ser_x_list ser_type_var state tl

    and ser_sig_content state =
      let write i = LBB.write state (Int64.of_int i) 4 in
      function
      | SType s -> write 0; ser_sigtype state s
      | SException tl -> write 1; ser_x_list ser_type state  tl
      | SInit t -> write 2; ser_type state t
      | SEntry t -> write 3; ser_type state t
      | SView t1 -> write 4; ser_type state t1;
      | SValue t -> write 5; ser_type state t
      | SStructure st -> write 6; ser_structure_type state st
      | SSignature s -> write 7; ser_signature state s

    and ser_kind state = function
      | Module -> LBB.write_bool state false
      | Contract l -> (
          LBB.write_bool state true;
          ser_x_y_list LBB.write_str_repr LBB.write_string state l
        )

    and ser_signature state { sig_kind; sig_content; } =
      ser_kind state sig_kind;
      ser_str_repr_x_list ser_sig_content state sig_content

    let serialize = ser ser_type
  end

  (** Primitive serializer **)

  let ser_extended_arg state p =
    let open TYPE in
    match p with
    | ANone -> LBB.write state Int64.zero 2
    | AContractType ct ->
        LBB.write state Int64.one 2; Type.ser_structure_type state ct

  let ser_primitive state (p, xa) =
    LBB.stats_add_i_custom state "Primitives" 8;
    LBB.write_uint8 state (Love_primitive.id_of_prim p);
    ser_extended_arg state xa

  let ser_type_kind state = function
    | AST.TPublic -> LBB.write state Int64.zero 2;
    | TPrivate -> LBB.write state Int64.one 2;
    | TInternal -> LBB.write state (Int64.of_int 2) 2;
    | TAbstract -> LBB.write state (Int64.of_int 3) 2

  let ser_loc state {pos_lnum;pos_bol;pos_cnum} =
    LBB.write_uint32 state pos_lnum;
    LBB.write_uint32 state pos_bol;
    LBB.write_uint32 state pos_cnum

  let ser_open_closed state = function
    | Open -> LBB.write_bool state true
    | Closed -> LBB.write_bool state false

  let ser_record_list ser_pattern state {fields; open_record} =
    ser_x_y_list LBB.write_str_repr (ser_x_option ser_pattern) state fields;
    ser_open_closed state open_record

  module SerAst
      (AnnotSerializer : sig val ser_annot : LBB.writer ->
         location option -> unit end) =
  struct

    (** Const serializers **)

    let ser_annoted ser state annoted =
      ser state annoted.Utils.content;
      AnnotSerializer.ser_annot state annoted.annot

    let ser_exn_name state = function
      | AST.Fail t -> LBB.write_bool state true; Type.ser_type state t
      | Exception e     -> LBB.write_bool state false; ser_ident state e

    let rec ser_raw_const state =
      let write state i =
        LBB.stats_add_i_custom state "Const" 5;
        LBB.write state (Int64.of_int i) 5 in
      function
      (* CUser -> 0 (reserved) *)
      | CUnit -> write state 1
      | CBool b -> write state 2; LBB.write_bool state b
      | CString s -> write state 3; LBB.write_string state s
      | CBytes b -> write state 4; LBB.write_bytes state b
      | CInt i -> write state 5; LBB.write_z state i
      | CNat i -> write state 6; LBB.write_z state i
      | CDun d -> write state 7; ser_int64 state d
      | CKey k -> write state 8; ser_key state k
      | CKeyHash kh -> write state 9; ser_key_hash state kh
      | CSignature s -> write state 10; ser_signature state s
      | CTimestamp t -> write state 11; ser_timestamp state t
      | CAddress a ->
          match Contract_repr.of_b58check a with
          | Ok a -> write state 12; ser_address state a
          | _ -> assert false

    and ser_const s = ser_annoted ser_raw_const s

    (** Pattern serializer **)

    and ser_raw_pattern state =
      let ser_pattern_list = ser_x_list ser_pattern in
      let write state i =
        LBB.stats_add_i_custom state "Pattern" 4;
        LBB.write state (Int64.of_int i) 4 in
      function
      (* 0 (reserved) *)
      | PAny -> write state 1
      | PVar v -> write state 2; LBB.write_str_repr state v
      | PAlias (p, v) ->
        write state 3; ser_pattern state p; LBB.write_str_repr state v
      | PConst c -> write state 4; ser_const state c
      | PList pl -> write state 5; ser_pattern_list state pl
      | PTuple pl -> write state 6; ser_pattern_list state pl
      | PConstr (cstr, pl) ->
        write state 7; LBB.write_str_repr state cstr; ser_pattern_list state pl
      | PContract (n, st) ->
        write state 8; LBB.write_str_repr state n;
        Type.ser_structure_type state st
      | POr pl -> write state 9; ser_pattern_list state pl
      | PRecord l -> write state 10; ser_record_list ser_pattern state l

    and ser_pattern s = ser_annoted ser_raw_pattern s

    and ser_exn_pattern state (exn, pl) =
      ser_exn_name state exn; ser_x_list ser_pattern state pl


    (** Exp serializer **)

    and ser_raw_exp state =
      let ser_exp_list = ser_x_list ser_exp in
      let write state i =
        LBB.stats_add_i_custom state "Exp" 6;
        LBB.write state (Int64.of_int i) 6 in
      function
      (* 0 (reserved) *)
      | Const c ->
        write state 1; ser_const state c
      | Var v ->
        write state 2; ser_ident state v
      | VarWithArg (v, xa) ->
        write state 3; ser_ident state v; ser_extended_arg state xa
      | Let { bnd_pattern; bnd_val; body } ->
        write state 4; ser_pattern state bnd_pattern;
        ser_exp state bnd_val; ser_exp state body;
      | LetRec { bnd_var; bnd_val; body; fun_typ } ->
        write state 5; LBB.write_str_repr state bnd_var;
        ser_exp state bnd_val; ser_exp state body; Type.ser_type state fun_typ
      | Lambda l ->
        write state 6; ser_lambda state l
      | Apply { fct; args } ->
        write state 7; ser_exp state fct; ser_exp_list state args
      | TLambda { targ; exp } ->
        write state 8; Type.ser_type_var state targ; ser_exp state exp
      | TApply { exp; typ } ->
        write state 9; ser_exp state exp; Type.ser_type state typ
      | Seq el ->
        write state 10; ser_exp_list state el
      | If { cond; ifthen; ifelse } ->
        write state 11; ser_exp state cond;
        ser_exp state ifthen; ser_exp state ifelse
      | Match { arg; cases } ->
        write state 12; ser_exp state arg;
        ser_x_y_list ser_pattern ser_exp state cases
      | Constructor { constr; ctyps; args } ->
        write state 13;
        ser_ident state constr;
        ser_x_list Type.ser_type state ctyps;
        ser_exp_list state args
      | Nil ->
        write state 14;
      | List (el, e) ->
        write state 15; ser_exp_list state el; ser_exp state e
      | Tuple el ->
        write state 16; ser_exp_list state el
      | Project { tuple; indexes } ->
        write state 17; ser_exp state tuple;
        ser_x_list LBB.write_uint state indexes
      | Update { tuple; updates } ->
        write state 18; ser_exp state tuple;
        ser_int_x_list ser_exp state updates
      | Record { path; contents = fel } ->
        write state 19;
        ser_x_option ser_ident state path;
        ser_str_repr_x_list ser_exp state fel
      | GetField { record; field } ->
        write state 20; ser_exp state record;
        LBB.write_str_repr state field
      | SetFields { record; updates } ->
        write state 21; ser_exp state record;
        ser_str_repr_x_list ser_exp state updates
      | PackStruct sr ->
        write state 22;
        ser_struct_ref state sr
      | Raise { exn; args; loc } ->
        write state 23; ser_exn_name state exn;
        ser_exp_list state args;
        ser_x_option ser_loc state loc
      | TryWith { arg; cases } ->
        write state 24; ser_exp state arg;
        ser_x_y_list ser_exn_pattern ser_exp state cases

    and ser_exp s = ser_annoted ser_raw_exp s

    and ser_lambda state { arg_pattern; body; arg_typ } =
      ser_pattern state arg_pattern;
      ser_exp state body;
      Type.ser_type state arg_typ


    (** Structure serializers **)

    and ser_init state { init_code; init_typ; init_persist } =
      ser_exp state init_code; Type.ser_type state init_typ;
      LBB.write_bool state init_persist;

    and ser_entry state { entry_code; entry_fee_code; entry_typ } =
      ser_exp state entry_code; ser_x_option ser_exp state entry_fee_code;
      Type.ser_type state entry_typ

    and ser_view state { view_code; view_typ; view_recursive } =
      ser_exp state view_code;
      Type.ser_type state view_typ;
      ser_rec state view_recursive

    and ser_value state
        { value_code; value_typ; value_visibility; value_recursive } =
      ser_exp state value_code; Type.ser_type state value_typ;
      ser_visibility state value_visibility;
      ser_rec state value_recursive

    and ser_content state =
      let write state i =
        LBB.stats_add_i_custom state "Content" 4;
        LBB.write state (Int64.of_int i) 4 in
      function
      | DefType (k, td) ->
          write state 0; ser_type_kind state k; Type.ser_typedef state td
      | DefException tl -> write state 1; ser_x_list Type.ser_type state tl
      | Init i -> write state 2; ser_init state i
      | Entry e -> write state 3; ser_entry state e
      | View v -> write state 4; ser_view state v
      | Value v -> write state 5; ser_value state v
      | Structure s -> write state 6; ser_structure state s
      | Signature s -> write state 7; Type.ser_signature state s

    and ser_structure state { kind; structure_content } =
      Type.ser_kind state kind;
      ser_str_repr_x_list ser_content state structure_content;

    and ser_struct_ref state = function
      | Anonymous s -> LBB.write_bool state false; ser_structure state s
      | Named sn -> LBB.write_bool state true; ser_ident state sn

    and ser_top_contract state { version; code } =
      ser_x_x_pair (LBB.write_uint) state version;
      ser_structure state code

    let serialize_const c = ser ser_const c
    let serialize_exp e = ser ser_exp e
    let serialize_structure s = ser ser_structure s
    let serialize_top_contract tc = ser ser_top_contract tc
  end

  module Located = SerAst(struct let ser_annot = ser_x_option ser_loc end)
  (*  module Raw = SerAst(struct let ser_annot _ _ = () end) *)




  module Runtime = struct

    module SerType = Type

    open Love_runtime_ast

    module Type = SerType

    let ser_exn_name state = function
      | RFail t -> LBB.write_bool state true; Type.ser_type state t
      | RException e -> LBB.write_bool state false; ser_ident state e

    (** Const serializers **)

    let rec ser_const state =
      let write state i =
        LBB.stats_add_i_custom state "Const" 5;
        LBB.write state (Int64.of_int i) 5 in
      function
      (* RCUser -> 0 (reserved) *)
      | RCUnit -> write state 1
      | RCBool b -> write state 2; LBB.write_bool state b
      | RCString s -> write state 3; LBB.write_string state s
      | RCBytes b -> write state 4; LBB.write_bytes state b
      | RCInt i -> write state 5; LBB.write_z state i
      | RCNat i -> write state 6; LBB.write_z state i
      | RCDun d -> write state 7; ser_dun state d
      | RCKey k -> write state 8; ser_key state k
      | RCKeyHash kh -> write state 9; ser_key_hash state kh
      | RCSignature s -> write state 10; ser_signature state s
      | RCTimestamp t -> write state 11; ser_timestamp state t
      | RCAddress a -> write state 12; ser_address state a

    (** Pattern serializer **)

    and ser_pattern state =
      let ser_pattern_list = ser_x_list ser_pattern in
      let write state i =
        LBB.stats_add_i_custom state "Pattern" 4;
        LBB.write state (Int64.of_int i) 4 in
      function
      (* 0 (reserved) *)
      | RPAny -> write state 1
      | RPVar v -> write state 2; LBB.write_str_repr state v
      | RPAlias (p, v) ->
        write state 3; ser_pattern state p; LBB.write_str_repr state v
      | RPConst c -> write state 4; ser_const state c
      | RPList pl -> write state 5; ser_pattern_list state pl
      | RPTuple pl -> write state 6; ser_pattern_list state pl
      | RPConstr (cstr, pl) ->
        write state 7; LBB.write_str_repr state cstr; ser_pattern_list state pl
      | RPContract (n, st) ->
        write state 8; LBB.write_str_repr state n;
        Type.ser_structure_type state st
      | RPOr pl -> write state 9; ser_pattern_list state pl
      | RPRecord l -> write state 10; ser_record_list ser_pattern state l

    and ser_exn_pattern state (exn, pl) =
      ser_exn_name state exn; ser_x_list ser_pattern state pl

    (** Exp serializer **)

    and ser_exp state =
      let ser_exp_list = ser_x_list ser_exp in
      let write state i =
        LBB.stats_add_i_custom state "Exp" 6;
        LBB.write state (Int64.of_int i) 6 in
      function
      (* 0 (reserved) *)
      | RConst c ->
        write state 1; ser_const state c
      | RVar v ->
        write state 2; ser_ident state v
      | RVarWithArg (v, xa) ->
        write state 3; ser_ident state v; ser_extended_arg state xa
      | RLet { bnd_pattern; bnd_val; body } ->
        write state 4; ser_pattern state bnd_pattern;
        ser_exp state bnd_val; ser_exp state body;
      | RLetRec { bnd_var; bnd_val; val_typ; body } ->
        write state 5; LBB.write_str_repr state bnd_var;
        ser_exp state bnd_val; Type.ser_type state val_typ; ser_exp state body
      | RLambda l ->
        write state 6; ser_lambda state l
      | RApply { exp; args } ->
        write state 7; ser_exp state exp; ser_x_list ser_param state args
      | RSeq el ->
        write state 8; ser_exp_list state el
      | RIf { cond; ifthen; ifelse } ->
        write state 9; ser_exp state cond;
        ser_exp state ifthen; ser_exp state ifelse
      | RMatch { arg; cases } ->
        write state 10; ser_exp state arg;
        ser_x_y_list ser_pattern ser_exp state cases
      | RConstructor { type_name; constr; ctyps; args } ->
        write state 11;
        ser_ident state type_name;
        LBB.write_str_repr state constr;
        ser_x_list Type.ser_type state ctyps;
        ser_exp_list state args
      | RNil ->
        write state 12;
      | RList (el, e) ->
        write state 13; ser_exp_list state el; ser_exp state e
      | RTuple el ->
        write state 14; ser_exp_list state el
      | RProject { tuple; indexes } ->
        write state 15; ser_exp state tuple;
        ser_x_list LBB.write_uint state indexes
      | RUpdate { tuple; updates } ->
        write state 16; ser_exp state tuple;
        ser_int_x_list ser_exp state updates
      | RRecord { type_name; contents = fel } ->
        write state 17;
        ser_ident state type_name;
        ser_str_repr_x_list ser_exp state fel
      | RGetField { record; field } ->
        write state 18; ser_exp state record;
        LBB.write_str_repr state field
      | RSetFields { record; updates } ->
        write state 19; ser_exp state record;
        ser_str_repr_x_list ser_exp state updates
      | RPackStruct sr ->
        write state 20;
        ser_struct_ref state sr
      | RRaise { exn; args; loc } ->
        write state 21; ser_exn_name state exn;
        ser_exp_list state args;
        ser_x_option ser_loc state loc
      | RTryWith { arg; cases } ->
        write state 22; ser_exp state arg;
        ser_x_y_list ser_exn_pattern ser_exp state cases

    and ser_binding state b =
      let write state i =
        LBB.stats_add_i_custom state "Binding" 1;
        LBB.write state (Int64.of_int i) 1 in
      match b with
      | RPattern (p, t) ->
        write state 0; ser_x_y_pair ser_pattern Type.ser_type state (p, t)
      | RTypeVar tv ->
        write state 1; Type.ser_type_var state tv

    and ser_param state p =
      let write state i =
        LBB.stats_add_i_custom state "Param" 1;
        LBB.write state (Int64.of_int i) 1 in
      match p with
      | RExp e -> write state 0; ser_exp state e
      | RType t -> write state 1; Type.ser_type state t

    and ser_lambda state { args; body } =
      ser_x_list ser_binding state args;
      ser_exp state body


    (** Structure serializers **)

    and ser_init state { init_code; init_typ; init_persist } =
      ser_exp state init_code; Type.ser_type state init_typ;
      LBB.write_bool state init_persist;

    and ser_entry state { entry_code; entry_fee_code; entry_typ } =
      ser_exp state entry_code; ser_x_option ser_exp state entry_fee_code;
      Type.ser_type state entry_typ

    and ser_view state { view_code; view_typ; view_recursive } =
      ser_exp state view_code; Type.ser_type state view_typ; ser_rec state view_recursive

    and ser_value state
        { value_code; value_typ; value_visibility; value_recursive } =
      ser_exp state value_code; Type.ser_type state value_typ;
      ser_visibility state value_visibility;
      ser_rec state value_recursive

    and ser_content state =
      let write state i =
        LBB.stats_add_i_custom state "Content" 4;
        LBB.write state (Int64.of_int i) 4 in
      function
      | RDefType (k, td) ->
          write state 0; ser_type_kind state k; Type.ser_typedef state td
      | RDefException tl -> write state 1; ser_x_list Type.ser_type state tl
      | RInit i -> write state 2; ser_init state i
      | REntry e -> write state 3; ser_entry state e
      | RView v -> write state 4; ser_view state v
      | RValue v -> write state 5; ser_value state v
      | RStructure s -> write state 6; ser_structure state s
      | RSignature s -> write state 7; Type.ser_signature state s

    and ser_structure state { kind; structure_content; } =
      Type.ser_kind state kind;
      ser_str_repr_x_list ser_content state structure_content;

    and ser_struct_ref state = function
      | RAnonymous s -> LBB.write_bool state false; ser_structure state s
      | RNamed sn -> LBB.write_bool state true; ser_ident state sn

    and ser_top_contract state { version; code } =
      ser_x_x_pair (LBB.write_uint) state version;
      ser_structure state code

    let serialize_const c = ser ser_const c
    let serialize_exp e = ser ser_exp e
    let serialize_structure s = ser ser_structure s
    let serialize_top_contract tc = ser ser_top_contract tc
  end



  module Value = struct
    open Love_value
    open Value

    let rec ser_value state value =
      let write state i = LBB.write state (Int64.of_int i) 6 in
      match Value.unrec_closure value with

      (* 0 (reserved) *)

      (* Base *)
      | VUnit -> write state 1
      | VBool b -> write state 2; LBB.write_bool state b
      | VString s -> write state 3; LBB.write_string state s
      | VBytes b ->  write state 4; LBB.write_bytes state b
      | VInt z ->  write state 5; LBB.write_z state z
      | VNat z ->  write state 6; LBB.write_z state z

      (* Composite *)
      | VTuple vl -> write state 7; ser_x_list ser_value state vl
      | VConstr (c, tl) ->
        write state 8; LBB.write_str_repr state c; ser_x_list ser_value state tl
      | VRecord fl ->
        write state 9; ser_x_y_list LBB.write_str_repr ser_value state fl

      (* Collections *)
      | VList l ->  write state 10; ser_x_list ser_value state l
      | VSet s ->
        write state 11; ser_x_list ser_value state (ValueSet.elements s)
      | VMap m ->
        write state 12;
        ser_x_list (ser_x_x_pair ser_value) state (ValueMap.bindings m)
      | VBigMap { id; diff; key_type; value_type } ->
        write state 13;
        ser_x_option LBB.write_z state id;
        Type.ser_type state key_type;
        Type.ser_type state value_type;
        ser_x_list (ser_x_y_pair ser_value
                      (ser_x_option ser_value)) state (ValueMap.bindings diff)

      (* Domain-specific *)
      | VDun d -> write state 14; ser_dun state d
      | VKey k -> write state 15; ser_key state k
      | VKeyHash kh -> write state 16; ser_key_hash state kh
      | VSignature s -> write state 17; ser_signature state s
      | VTimestamp ts -> write state 18; ser_timestamp state ts
      | VAddress a -> write state 19; ser_address state a
      | VOperation op -> write state 20; ser_op state op

      (* Structures and entry points *)
      | VPackedStructure (p, c) ->
        write state 21; ser_ident state p; ser_live_contract state c
      | VContractInstance (s, c) ->
        write state 22; Type.ser_signature state s; ser_address state c
      | VEntryPoint (c, str) ->
        write state 23; ser_address state c; LBB.write_str_repr state str
      | VView (c, str) ->
        write state 24; ser_address state c; LBB.write_str_repr state str

      (* Closures *)
      | VClosure { call_env; lambda } ->
        write state 25;
        ser_str_repr_id_x_list
          (ser_local_or_global ser_value
             (ser_pointer_or_inlined ser_value)) state call_env.values;
        ser_str_repr_id_x_list
          (ser_pointer_or_inlined ser_live_structure) state call_env.structs;
        ser_str_repr_id_x_list
          (ser_pointer_or_inlined Type.ser_signature) state call_env.sigs;
        ser_x_list (ser_x_x_pair ser_ident) state call_env.exns;
        ser_x_list (ser_x_x_pair ser_ident) state call_env.types;
        ser_str_repr_x_list Type.ser_type state call_env.tvars;
        Runtime.ser_lambda state lambda

      | VPrimitive (p, xa, vtl) ->
        write state 26;
        ser_primitive state (p, xa);
        ser_x_list ser_val_or_type state vtl

    and ser_val_or_type state vt =
      match vt with
      | V v -> ser_value state v
      | T t -> LBB.write state (Int64.of_int 27) 6; Type.ser_type state t

    and ser_local_or_global :
      type t1 t2. (LBB.writer -> t1 -> unit) -> (LBB.writer -> t2 -> unit) ->
        LBB.writer -> (t1, t2) Value.local_or_global -> unit =
      let write state i = LBB.write state (Int64.of_int i) 1 in
      fun ser_local ser_global state -> function
        | Local l -> write state 0; ser_local state l
        | Global g -> write state 1; ser_global state g

    and ser_pointer_or_inlined :
      type t. (LBB.writer -> t -> unit) -> LBB.writer ->
                t Value.ptr_or_inlined -> unit =
      let write state i = LBB.write state (Int64.of_int i) 1 in
      fun ser_val state -> function
        | Inlined (p, c) -> write state 0; ser_ident state p; ser_val state c
        | Pointer p -> write state 1; ser_ident state p


    (** Operation serializers **)

    and ser_manager_operation state op =
      let write state i = LBB.write state (Int64.of_int i) 3 in
      match op with
      | Op.Origination { delegate; script; credit; preorigination } ->
        write state 0;
        ser_x_option ser_key_hash state delegate;
        ser_x_y_pair ser_value ser_live_contract state script;
        ser_dun state credit;
        ser_x_option ser_address state preorigination
      | Transaction { amount; parameters; entrypoint; destination } ->
        write state 1;
        ser_dun state amount;
        ser_x_option ser_value state parameters;
        LBB.write_str_repr state entrypoint;
        ser_address state destination;
      | Delegation delegate ->
        write state 2;
        ser_x_option ser_key_hash state delegate;
      | Dune_manage_account
          { target; maxrolls; admin; white_list; delegation } ->
        write state 3;
        ser_x_option ser_key_hash state target;
        ser_x_option (ser_x_option LBB.write_uint) state maxrolls;
        ser_x_option (ser_x_option ser_address) state admin;
        ser_x_option (ser_x_list ser_address) state white_list;
        ser_x_option LBB.write_bool state delegation

    and ser_internal_op state Op.{ source; operation; nonce } =
        ser_address state source;
        ser_manager_operation state operation;
        LBB.write_uint state nonce

    and ser_big_map_diff_item state item =
      let write state i = LBB.write state (Int64.of_int i) 3 in
      match item with
      | Op.Update { big_map; diff_key; diff_key_hash; diff_value } ->
        write state 0;
        LBB.write_z state big_map;
        ser_value state diff_key;
        ser_script_expr_hash state diff_key_hash;
        ser_x_option ser_value state diff_value
      | Clear id ->
        write state 1;
        LBB.write_z state id
      | Copy (ids, idd) ->
        write state 2;
        LBB.write_z state ids;
        LBB.write_z state idd
      | Alloc { big_map; key_type; value_type }  ->
        write state 3;
        LBB.write_z state big_map;
        Type.ser_type state key_type;
        Type.ser_type state value_type

    and ser_op state (operation, big_map_diff_opt) =
      ser_internal_op state operation;
      ser_x_option (ser_x_list ser_big_map_diff_item) state big_map_diff_opt


    (** Structure serializers **)

    and ser_init state
        ({ vinit_code; vinit_typ; vinit_persist } : LiveStructure.init) =
      ser_value state vinit_code; Type.ser_type state vinit_typ;
      LBB.write_bool state vinit_persist;

    and ser_entry state
        ({ ventry_code; ventry_fee_code; ventry_typ } : LiveStructure.entry) =
      ser_value state ventry_code;
      ser_x_option ser_value state ventry_fee_code;
      Type.ser_type state ventry_typ

    and ser_view state ({ vview_code; vview_typ; vview_recursive } : LiveStructure.view) =
      ser_value state vview_code;
      Type.ser_type state vview_typ;
      ser_rec state vview_recursive

    and ser_cvalue state
        LiveStructure.{ vvalue_code; vvalue_typ; vvalue_visibility;
                        vvalue_recursive } =
      ser_value state vvalue_code;
      Type.ser_type state vvalue_typ;
      ser_visibility state vvalue_visibility;
      ser_rec state vvalue_recursive

    and ser_content state =
      let open LiveStructure in
      let write state i = LBB.write state (Int64.of_int i) 4 in
      function
      | VType (k,td) ->
          write state 0; ser_type_kind state k; Type.ser_typedef state td
      | VException tl -> write state 1; ser_x_list Type.ser_type state tl
      | VInit i -> write state 2; ser_init state i
      | VEntry e -> write state 3; ser_entry state e
      | VView v -> write state 4; ser_view state v
      | VValue v -> write state 5; ser_cvalue state v
      | VStructure s -> write state 6; ser_live_structure state s
      | VSignature s -> write state 7; Type.ser_signature state s

    and ser_live_structure state LiveStructure.{ kind; content } =
      Type.ser_kind state kind;
      ser_str_repr_x_list ser_content state content;

    and ser_live_contract state LiveContract.{ version; root_struct } =
      ser_x_x_pair LBB.write_uint state version;
      ser_live_structure state root_struct

    and ser_fee_code state FeeCode.{ version; root_struct; fee_codes } =
      ser_x_x_pair LBB.write_uint state version;
      ser_live_structure state root_struct;
      ser_str_repr_x_list (ser_x_y_pair ser_value Type.ser_type) state fee_codes

    let serialize value = ser ser_value value
    let serialize_live_structure ls = ser ser_live_structure ls
    let serialize_live_contract lc = ser ser_live_contract lc
    let serialize_fee_code fc = ser ser_fee_code fc
  end

end

module Deserializer = struct

  module LBB = Love_binary_buffer

  (** Combining deserializers **)

  let des_x_option des_x state =
    if LBB.read_bool state
    then Some (des_x state)
    else None

  let des_list_size state =
    let i = Int64.to_int (LBB.read state 1) in
    if Compare.Int.(i = 0) then begin
      let i = Int64.to_int (LBB.read state 1) in
      if Compare.Int.(i = 0) then 0 (* 00 / size = 0 *)
      else (LBB.read_uint state) + 9 (* 01 / size >= 9 *)
    end else (Int64.to_int (LBB.read state 4)) + 1 (* 1 / size = 1 - 8 *)

  let des_x_list des_x state =
    (* let length = LBB.read_uint state in *)
    let length = des_list_size state in
    let xl = ref [] in
    for _i = 1 to length do
      xl := (des_x state) :: !xl
    done;
    List.rev !xl

  let des_x_y_list des_x des_y state =
    (* let length = LBB.read_uint state in *)
    let length = des_list_size state in
    let xyl = ref [] in
    for _i = 1 to length do
      let x = des_x state in
      let y = des_y state in
      xyl := (x, y) :: !xyl
    done;
    List.rev !xyl

  let des_x_x_list des_x state =
    (* let length = LBB.read_uint state in *)
    let length = des_list_size state in
    let xxl = ref [] in
    for _i = 1 to length do
      let x1 = des_x state in
      let x2 = des_x state in
      xxl := (x1, x2) :: !xxl
    done;
    List.rev !xxl

  let des_x_x_pair des_x state =
    let x1 = des_x state in
    let x2 = des_x state in
    (x1, x2)

  let des_x_y_pair des_x des_y state =
    let x = des_x state in
    let y = des_y state in
    (x, y)


  (** Utility deserializers **)

  let des_ident state =
    Ident.put_list (des_x_list LBB.read_str_repr state)

  let des_ident_x_list des_x state =
    des_x_y_list des_ident des_x state

  let des_str_repr_x_list des_x state =
    des_x_y_list LBB.read_str_repr des_x state

  let des_str_repr_id_x_list des_x state =
    des_x_y_list des_ident des_x state

  let des_int_x_list des_x state =
    des_x_y_list (fun i -> LBB.read_uint i) des_x state


  let des_timestamp state =
    Script_timestamp_repr.of_zint (LBB.read_z state)

  let des_int64 state =
    LBB.read_z state |> Z.to_int64

  let des_dun state =
    Log.debug "[des_dun] Deserializing dun value@.";
    let di64 = des_int64 state in
    Log.debug "[des_dun] Deserializing dun value %Ld@." di64;
    match Tez_repr.of_mutez di64 with
    | None -> raise (DeserializeError ((-1), "Bad dun"))
    | Some d -> d

  let des_key state =
    let open Signature in
    let b = LBB.read_bytes state in
    match Data_encoding.Binary.of_bytes Public_key.encoding b with
    | None -> failwith "Bad key"
    | Some k -> k

  let des_key_hash state =
    let open Signature in
    let b = LBB.read_bytes state in
    match Data_encoding.Binary.of_bytes Public_key_hash.encoding b with
    | None -> failwith "Bad key hash"
    | Some kh -> kh

  let des_signature state =
    let b = LBB.read_bytes state in
    match Data_encoding.Binary.of_bytes Signature.encoding b with
    | None -> failwith "Bad signature"
    | Some s -> s

  let des_address state =
    let b = LBB.read_bytes state in
    match Data_encoding.Binary.of_bytes Contract_repr.encoding b with
    | None -> failwith "Bad address"
    | Some a -> a

  let des_script_expr_hash state =
    let b = LBB.read_bytes state in
    match Data_encoding.Binary.of_bytes Script_expr_hash.encoding b with
    | None -> failwith "Bad script hash"
    | Some sh -> sh


  (** Main deserializer *)

  let des deser b =
    let state = LBB.initialize_reader b in
    deser state


  (** Qualifier deserializers **)

  let des_visibility state =
    if LBB.read_bool state then Public else Private


  let des_rec state = if LBB.read_bool state then TYPE.Rec else NonRec


  (** Type deserializers **)

  module Type = struct

    open TYPE

    let des_type_name = des_ident

    let des_trait state = {tcomparable = LBB.read_bool state}

    let des_type_var state : type_var =
      let tv_name = LBB.read_str_repr state in
      let tv_traits = des_trait state in
      { tv_name; tv_traits }

    let rec des_type state =
      let des_type_list = des_x_list des_type in
      let size = 4 in
      match LBB.read state size |> Int64.to_int with
      | 0 ->
        let tname = des_type_name state in
        let tl = des_type_list state in
        TUser (tname, tl)
      | 1 -> TTuple (des_type_list state)
      | 2 ->
        let t1 = des_type state in
        let t2 = des_type state in
        TArrow (t1, t2)
      | 3 -> TVar (des_type_var state)
      | 4 ->
        let v = des_type_var state in
        let t = des_type state in
        TForall (v, t)
      | 5 -> TPackedStructure (des_structure_type state)
      | 6 -> TContractInstance (des_structure_type state)
      | i -> raise (DeserializeError (i, "Unknown type"))

    and des_structure_type state =
      if LBB.read_bool state
      then Named (des_ident state)
      else Anonymous (des_signature state)

    and des_sigtype state =
      match LBB.read state 2 |> Int64.to_int with
      | 0 -> SPublic (des_typedef state)
      | 1 -> SPrivate (des_typedef state)
      | 2 -> SAbstract (des_x_list des_type_var state)
      | i -> raise (DeserializeError (i, "Unknown sigtype"))

    and des_typedef state =
      match LBB.read state 2 |> Int64.to_int with
      | 0 ->
        let aparams = des_x_list des_type_var state in
        let atype = des_type state in
        Alias { aparams; atype }
      | 1 ->
        let sparams = des_x_list des_type_var state in
        let scons = des_str_repr_x_list (des_x_list des_type) state in
        let srec = des_rec state in
        SumType { sparams; scons; srec}
      | 2 ->
        let rparams = des_x_list des_type_var state in
        let rfields = des_str_repr_x_list des_type state in
        let rrec = des_rec state in
        RecordType { rparams; rfields; rrec }
      | i -> raise (DeserializeError (i, "Unknown type definition"))

    and des_sig_content state =
      match LBB.read state 4 |> Int64.to_int with
      | 0 -> SType (des_sigtype state)
      | 1 -> SException (des_x_list des_type state)
      | 2 -> SInit (des_type state)
      | 3 -> SEntry (des_type state)
      | 4 -> SView (des_type state)
      | 5 -> SValue (des_type state)
      | 6 -> SStructure (des_structure_type state)
      | 7 -> SSignature (des_signature state)
      | i -> raise (DeserializeError (i, "Unknown signature content"))

    and des_kind state =
      if LBB.read_bool state
      then Contract (des_x_y_list LBB.read_str_repr LBB.read_string state)
      else Module

    and des_signature state =
      let sig_kind = des_kind state in
      let sig_content = des_str_repr_x_list des_sig_content state in
      { sig_kind; sig_content}

    let deserialize = des des_type
  end

  (** Primitive deserializer **)

  let des_extended_arg state =
    let open TYPE in
    match LBB.read state 2 |> Int64.to_int with
    | 0 -> ANone
    | 1 -> AContractType (Type.des_structure_type state)
    | i ->  raise (DeserializeError (i, "Bad extended argument"))

  let des_primitive state =
    let i = LBB.read_uint8 state in
    try Love_primitive.prim_of_id i, des_extended_arg state
    with _ -> raise (DeserializeError (i, "Unknown primitive"))

  (** Const deserializers **)

  let des_type_kind state =
    match LBB.read state 2 |> Int64.to_int with
    | 0 -> AST.TPublic
    | 1 -> TPrivate
    | 2 -> TInternal
    | 3 -> TAbstract
    | i ->  raise (DeserializeError (i, "Bad type kind"))

  let des_loc state =
    let pos_lnum = LBB.read_uint32 state in
    let pos_bol =  LBB.read_uint32 state in
    let pos_cnum = LBB.read_uint32 state in
    {pos_lnum; pos_bol; pos_cnum}

  let des_open_closed state =
    if LBB.read_bool state then Open
    else Closed

  let des_record_list des_pattern state =
    let fields = des_x_y_list LBB.read_str_repr (des_x_option des_pattern) state in
    let open_record = des_open_closed state in
    AST.{fields; open_record}

  module DesAst
      (AnnotDeserializer: sig val des_annot : LBB.reader ->
         location option end) =
  struct

    let des_annoted des state =
      let content = des state in
      let annot = AnnotDeserializer.des_annot state in
      Utils.{content; annot}

    let des_exn_name state =
      if LBB.read_bool state
      then let t = Type.des_type state in AST.Fail t
      else let n = des_ident state in Exception n

    let rec des_raw_const state =
      match LBB.read state 5 |> Int64.to_int with
      (* 0 -> CUser (reserved) *)
      | 1 -> CUnit
      | 2 -> CBool (LBB.read_bool state)
      | 3 -> CString (LBB.read_string state)
      | 4 -> CBytes (LBB.read_bytes state)
      | 5 -> CInt (LBB.read_z state)
      | 6 as i ->
        let n = LBB.read_z state in
        if Compare.Z.(n < Z.zero)
        then raise (DeserializeError (i, "Negative CNat"))
        else CNat n
      | 7 -> CDun (des_int64 state)
      | 8 -> CKey (des_key state)
      | 9 -> CKeyHash (des_key_hash state)
      | 10 -> CSignature (des_signature state)
      | 11 -> CTimestamp (des_timestamp state)
      | 12 ->
          let s = des_address state in
          CAddress ( Contract_repr.to_b58check s )
      | i -> raise (DeserializeError (i, "Unknown constant"))

    and des_const s = des_annoted des_raw_const s

    (** Pattern deserializer **)

    and des_raw_pattern state =
      let des_pattern_list = des_x_list des_pattern in
      match LBB.read state 4 |> Int64.to_int with
      (* 0 (reserved) *)
      | 1 -> PAny
      | 2 -> PVar (LBB.read_str_repr state)
      | 3 ->
        let p = des_pattern state in
        let v = LBB.read_str_repr state in
        PAlias (p, v)
      | 4 -> PConst (des_const state)
      | 5 -> PList (des_pattern_list state)
      | 6 -> PTuple (des_pattern_list state)
      | 7 ->
        let cstr = LBB.read_str_repr state in
        let pl = des_pattern_list state in
        PConstr (cstr, pl)
      | 8 ->
        let n = LBB.read_str_repr state in
        let st = Type.des_structure_type state in
        PContract (n, st)
      | 9 -> POr (des_pattern_list state)
      | 10 -> PRecord (des_record_list des_pattern state)
      | i -> raise (DeserializeError (i, "Unknown pattern"))

    and des_pattern s = des_annoted des_raw_pattern s

    and des_exn_pattern state =
      let exn = des_exn_name state in
      let pl = des_x_list des_pattern state in
      exn, pl


    (** Exp deserializer **)

    and des_raw_exp state =
      let des_exp_list = des_x_list des_exp in
      match LBB.read state 6 |> Int64.to_int with
      (* 0 (reserved) *)
      | 1 -> Const (des_const state)
      | 2 -> Var (des_ident state)
      | 3 ->
          let v = des_ident state in
          let xa = des_extended_arg state in
          VarWithArg (v, xa)
      | 4 ->
        let bnd_pattern = des_pattern state in
        let bnd_val = des_exp state in
        let body = des_exp state in
        Let { bnd_pattern; bnd_val; body }
      | 5 ->
        let bnd_var = LBB.read_str_repr state in
        let bnd_val = des_exp state in
        let body = des_exp state in
        let fun_typ = Type.des_type state in
        LetRec { bnd_var; bnd_val; body; fun_typ }
      | 6 -> Lambda (des_lambda state)
      | 7 ->
        let fct = des_exp state in
        let args = des_exp_list state in
        Apply { fct; args }
      | 8 ->
        let targ = Type.des_type_var state in
        let exp = des_exp state in
        TLambda { targ; exp }
      | 9 ->
        let exp = des_exp state in
        let typ = Type.des_type state in
        TApply { exp; typ }
      | 10 -> Seq (des_exp_list state)
      | 11 ->
        let cond = des_exp state in
        let ifthen = des_exp state in
        let ifelse = des_exp state in
        If { cond; ifthen; ifelse }
      | 12 ->
        let arg = des_exp state in
        let cases = des_x_y_list des_pattern des_exp state in
        Match { arg; cases }
      | 13 ->
        let constr = des_ident state in
        let ctyps = des_x_list Type.des_type state in
        let args = des_exp_list state in
        Constructor { constr; ctyps; args }
      | 14 -> Nil
      | 15 ->
        let el = des_exp_list state in
        let e = des_exp state in
        List (el, e)
      | 16 -> Tuple (des_exp_list state)
      | 17 ->
        let tuple = des_exp state in
        let indexes = des_x_list LBB.read_uint state in
        Project { tuple; indexes }
      | 18 ->
        let tuple = des_exp state in
        let updates = des_int_x_list des_exp state in
        Update { tuple; updates }
      | 19 ->
        let path = des_x_option des_ident state in
        let fel = des_str_repr_x_list des_exp state in
        Record { path; contents = fel }
      | 20 ->
        let record = des_exp state in
        let field = LBB.read_str_repr state in
        GetField { record; field }
      | 21 ->
        let record = des_exp state in
        let updates = des_str_repr_x_list des_exp state in
        SetFields { record; updates }
      | 22 -> PackStruct (des_struct_ref state)
      | 23 ->
        let exn = des_exn_name state in
        let args = des_exp_list state in
        let loc = des_x_option des_loc state in
        Raise { exn; args; loc }
      | 24 ->
        let arg = des_exp state in
        let cases = des_x_y_list des_exn_pattern des_exp state in
        TryWith { arg; cases }
      | i -> raise (DeserializeError (i, "Unknown expression"))

    and des_exp state = des_annoted des_raw_exp state

    and des_lambda state =
      let arg_pattern = des_pattern state in
      let body = des_exp state in
      let arg_typ = Type.des_type state in
      { arg_pattern; body; arg_typ }

    (** Structure deserializers **)

    and des_init state =
      let init_code = des_exp state in
      let init_typ = Type.des_type state in
      let init_persist = LBB.read_bool state in
      { init_code; init_typ; init_persist }

    and des_entry state =
      let entry_code = des_exp state in
      let entry_fee_code = des_x_option des_exp state in
      let entry_typ = Type.des_type state in
      { entry_code; entry_fee_code; entry_typ }

    and des_view state =
      let view_code = des_exp state in
      let view_typ = Type.des_type state in
      let view_recursive = des_rec state in
      { view_code; view_typ; view_recursive }

    and des_value state =
      let value_code = des_exp state in
      let value_typ = Type.des_type state in
      let value_visibility = des_visibility state in
      let value_recursive = des_rec state in
      { value_code; value_typ; value_visibility; value_recursive }

    and des_content state =
      match LBB.read state 4 |> Int64.to_int with
      | 0 -> let k = des_type_kind state in DefType (k, Type.des_typedef state)
      | 1 -> DefException (des_x_list Type.des_type state)
      | 2 -> Init (des_init state)
      | 3 -> Entry (des_entry state)
      | 4 -> View (des_view state)
      | 5 -> Value (des_value state)
      | 6 -> Structure (des_structure state)
      | 7 -> Signature (Type.des_signature state)
      | i -> raise (DeserializeError (i, "Unknown content"))

    and des_structure state =
      let kind = Type.des_kind state in
      let structure_content = des_str_repr_x_list des_content state in
      { kind; structure_content }

    and des_struct_ref state =
      if LBB.read_bool state
      then Named (des_ident state)
      else Anonymous (des_structure state)

    and des_top_contract state =
      let version = des_x_x_pair (LBB.read_uint) state in
      let code = des_structure state in
      { version; code }

    (** Main deserializers **)

    let deserialize_const c = des des_const c
    let deserialize_exp e = des des_exp e
    let deserialize_structure s = des des_structure s
    let deserialize_top_contract tc = des des_top_contract tc
  end

  module Located = DesAst (struct let des_annot = des_x_option des_loc end)



  module Runtime = struct

    module DesType = Type

    open Love_runtime_ast

    module Type = DesType

    let des_exn_name state =
      if LBB.read_bool state
      then let t = Type.des_type state in RFail t
      else let n = des_ident state in RException n

    let rec des_const state =
      match LBB.read state 5 |> Int64.to_int with
      (* 0 -> RCUser (reserved) *)
      | 1 -> RCUnit
      | 2 -> RCBool (LBB.read_bool state)
      | 3 -> RCString (LBB.read_string state)
      | 4 -> RCBytes (LBB.read_bytes state)
      | 5 -> RCInt (LBB.read_z state)
      | 6 as i ->
        let n = LBB.read_z state in
        if Compare.Z.(n < Z.zero)
        then raise (DeserializeError (i, "Negative CNat"))
        else RCNat n
      | 7 -> RCDun (des_dun state)
      | 8 -> RCKey (des_key state)
      | 9 -> RCKeyHash (des_key_hash state)
      | 10 -> RCSignature (des_signature state)
      | 11 -> RCTimestamp (des_timestamp state)
      | 12 -> RCAddress (des_address state)
      | i -> raise (DeserializeError (i, "Unknown constant"))

    (** Pattern deserializer **)

    and des_pattern state =
      let des_pattern_list = des_x_list des_pattern in
      match LBB.read state 4 |> Int64.to_int with
      (* 0 (reserved) *)
      | 1 -> RPAny
      | 2 -> RPVar (LBB.read_str_repr state)
      | 3 ->
        let p = des_pattern state in
        let v = LBB.read_str_repr state in
        RPAlias (p, v)
      | 4 -> RPConst (des_const state)
      | 5 -> RPList (des_pattern_list state)
      | 6 -> RPTuple (des_pattern_list state)
      | 7 ->
        let cstr = LBB.read_str_repr state in
        let pl = des_pattern_list state in
        RPConstr (cstr, pl)
      | 8 ->
        let n = LBB.read_str_repr state in
        let st = Type.des_structure_type state in
        RPContract (n, st)
      | 9 -> RPOr (des_pattern_list state)
      | 10 -> RPRecord (des_record_list des_pattern state)
      | i -> raise (DeserializeError (i, "Unknown pattern"))

    and des_exn_pattern state =
      let exn = des_exn_name state in
      let pl = des_x_list des_pattern state in
      exn, pl


    (** Exp deserializer **)

    and des_exp state =
      let des_exp_list = des_x_list des_exp in
      match LBB.read state 6 |> Int64.to_int with
      (* 0 (reserved) *)
      | 1 -> RConst (des_const state)
      | 2 -> RVar (des_ident state)
      | 3 ->
          let v = des_ident state in
          let xa = des_extended_arg state in
          RVarWithArg (v, xa)
      | 4 ->
        let bnd_pattern = des_pattern state in
        let bnd_val = des_exp state in
        let body = des_exp state in
        RLet { bnd_pattern; bnd_val; body }
      | 5 ->
        let bnd_var = LBB.read_str_repr state in
        let bnd_val = des_exp state in
        let val_typ = Type.des_type state in
        let body = des_exp state in
        RLetRec { bnd_var; bnd_val; val_typ; body }
      | 6 -> RLambda (des_lambda state)
      | 7 ->
        let exp = des_exp state in
        let args = des_x_list des_param state in
        RApply { exp; args }
      | 8 -> RSeq (des_exp_list state)
      | 9 ->
        let cond = des_exp state in
        let ifthen = des_exp state in
        let ifelse = des_exp state in
        RIf { cond; ifthen; ifelse }
      | 10 ->
        let arg = des_exp state in
        let cases = des_x_y_list des_pattern des_exp state in
        RMatch { arg; cases }
      | 11 ->
        let type_name = des_ident state in
        let constr = LBB.read_str_repr state in
        let ctyps = des_x_list Type.des_type state in
        let args = des_exp_list state in
        RConstructor { type_name; constr; ctyps; args }
      | 12 -> RNil
      | 13 ->
        let el = des_exp_list state in
        let e = des_exp state in
        RList (el, e)
      | 14 -> RTuple (des_exp_list state)
      | 15 ->
        let tuple = des_exp state in
        let indexes = des_x_list LBB.read_uint state in
        RProject { tuple; indexes }
      | 16 ->
        let tuple = des_exp state in
        let updates = des_int_x_list des_exp state in
        RUpdate { tuple; updates }
      | 17 ->
        let type_name = des_ident state in
        let fel = des_str_repr_x_list des_exp state in
        RRecord { type_name; contents = fel }
      | 18 ->
        let record = des_exp state in
        let field = LBB.read_str_repr state in
        RGetField { record; field }
      | 19 ->
        let record = des_exp state in
        let updates = des_str_repr_x_list des_exp state in
        RSetFields { record; updates }
      | 20 -> RPackStruct (des_struct_ref state)
      | 21 ->
        let exn = des_exn_name state in
        let args = des_exp_list state in
        let loc = des_x_option des_loc state in
        RRaise { exn; args; loc }
      | 22 ->
        let arg = des_exp state in
        let cases = des_x_y_list des_exn_pattern des_exp state in
        RTryWith { arg; cases }
      | i -> raise (DeserializeError (i, "Unknown expression"))

    and des_binding state =
      match LBB.read state 1 |> Int64.to_int with
      | 0 ->
        let p, t = des_x_y_pair des_pattern Type.des_type state in
        RPattern (p, t)
      | 1 -> RTypeVar (Type.des_type_var state)
      | i -> raise (DeserializeError (i, "Unknown binding"))

    and des_param state =
      match LBB.read state 1 |> Int64.to_int with
      | 0 -> RExp (des_exp state)
      | 1 -> RType (Type.des_type state)
      | i -> raise (DeserializeError (i, "Unknown param"))

    and des_lambda state =
      let args = des_x_list des_binding state in
      let body = des_exp state in
      { args; body }


    (** Structure deserializers **)

    and des_init state =
      let init_code = des_exp state in
      let init_typ = Type.des_type state in
      let init_persist = LBB.read_bool state in
      { init_code; init_typ; init_persist }

    and des_entry state =
      let entry_code = des_exp state in
      let entry_fee_code = des_x_option des_exp state in
      let entry_typ = Type.des_type state in
      { entry_code; entry_fee_code; entry_typ }

    and des_view state =
      let view_code = des_exp state in
      let view_typ = Type.des_type state in
      let view_recursive = des_rec state in
      { view_code; view_typ; view_recursive }

    and des_value state =
      let value_code = des_exp state in
      let value_typ = Type.des_type state in
      let value_visibility = des_visibility state in
      let value_recursive = des_rec state in
      { value_code; value_typ; value_visibility; value_recursive }

    and des_content state =
      match LBB.read state 4 |> Int64.to_int with
      | 0 -> let k = des_type_kind state in RDefType (k, Type.des_typedef state)
      | 1 -> RDefException (des_x_list Type.des_type state)
      | 2 -> RInit (des_init state)
      | 3 -> REntry (des_entry state)
      | 4 -> RView (des_view state)
      | 5 -> RValue (des_value state)
      | 6 -> RStructure (des_structure state)
      | 7 -> RSignature (Type.des_signature state)
      | i -> raise (DeserializeError (i, "Unknown content"))

    and des_structure state =
      let kind = Type.des_kind state in
      let structure_content = des_str_repr_x_list des_content state in
      { kind; structure_content }

    and des_struct_ref state =
      if LBB.read_bool state
      then RNamed (des_ident state)
      else RAnonymous (des_structure state)

    and des_top_contract state =
      let version = des_x_x_pair (LBB.read_uint) state in
      let code = des_structure state in
      { version; code }

    (** Main deserializers **)

    let deserialize_const c = des des_const c
    let deserialize_exp e = des des_exp e
    let deserialize_structure s = des des_structure s
    let deserialize_top_contract tc = des des_top_contract tc
  end




  module Value = struct
    open Love_value
    open Value

    let rec des_value_internal state i =
      Value.rec_closure @@
      match i with
      (* 0 (reserved) *)
      | 1 -> VUnit
      | 2 -> VBool (LBB.read_bool state)
      | 3 -> VString (LBB.read_string state)
      | 4 -> VBytes (LBB.read_bytes state)
      | 5 -> VInt (LBB.read_z state)
      | 6 -> VNat (LBB.read_z state)
      | 7 -> VTuple (des_x_list des_value state)
      | 8 ->
        let c = LBB.read_str_repr state in
        let args = des_x_list des_value state in
        VConstr (c, args)
      | 9 -> VRecord (des_x_y_list LBB.read_str_repr des_value state)
      | 10 -> VList (des_x_list des_value state)
      | 11 -> VSet (ValueSet.of_list (des_x_list des_value state))
      | 12 ->
        VMap (
          List.fold_left (fun acc (k,b) -> ValueMap.add k b acc)
            ValueMap.empty (des_x_list (des_x_x_pair des_value) state))
      | 13 ->
        let id = des_x_option LBB.read_z state in
        let key_type = Type.des_type state in
        let value_type = Type.des_type state in
        let diff = List.fold_left (fun acc (k, b) ->
            ValueMap.add k b acc
          ) ValueMap.empty (des_x_list (des_x_y_pair des_value
                                          (des_x_option des_value)) state) in
        VBigMap { id; diff; key_type; value_type }
      | 14 -> VDun (des_dun state)
      | 15 -> VKey (des_key state)
      | 16 -> VKeyHash (des_key_hash state)
      | 17 -> VSignature (des_signature state)
      | 18 -> VTimestamp (des_timestamp state)
      | 19 -> VAddress (des_address state)
      | 20 -> VOperation (des_op state)
      | 21 ->
          let p = des_ident state in
          let c = des_live_contract state in
          VPackedStructure (p, c)
      | 22 ->
          let s = Type.des_signature state in
          let c = des_address state in
          VContractInstance (s, c)
      | 23 ->
          let c = des_address state in
          let str = LBB.read_str_repr state in
          VEntryPoint (c, str)
      | 24 ->
          let c = des_address state in
          let str = LBB.read_str_repr state in
          VView (c, str)
      | 25 ->
        let values =
          des_str_repr_id_x_list
            (des_local_or_global des_value
               (des_pointer_or_inlined des_value)) state in
        let structs =
          des_str_repr_id_x_list
            (des_pointer_or_inlined des_live_structure) state in
        let sigs =
          des_str_repr_id_x_list
            (des_pointer_or_inlined Type.des_signature) state in
        let exns = des_x_list (des_x_x_pair des_ident) state in
        let types = des_x_list (des_x_x_pair des_ident) state in
        let tvars = des_str_repr_x_list Type.des_type state in
        let lambda = Runtime.des_lambda state in
        VClosure { call_env = { values; structs; sigs; exns; types; tvars };
                   lambda }
      | 26 ->
        let p, xa = des_primitive state in
        let vtl = des_x_list des_val_or_type state in
        VPrimitive (p, xa, vtl)
      | _ -> raise (DeserializeError (i, "Unknown value"))

    and des_value state =
      let i = LBB.read state 6 |> Int64.to_int in
      des_value_internal state i

    and des_val_or_type state =
      let i = LBB.read state 6 |> Int64.to_int in
      if Compare.Int.(i = 27) then T (Type.des_type state)
      else V (des_value_internal state i)

    and des_local_or_global :
      type t1 t2. (LBB.reader -> t1) -> (LBB.reader -> t2) ->
        LBB.reader -> (t1, t2) Value.local_or_global =
      fun des_local des_global state ->
        match LBB.read state 1 |> Int64.to_int with
        | 0 -> Local (des_local state)
        | 1 -> Global (des_global state)
        | i -> raise (DeserializeError (i, "Unknown local or global"))

    and des_pointer_or_inlined :
      type t. (LBB.reader -> t) -> LBB.reader -> t Value.ptr_or_inlined =
      fun des_content state ->
        match LBB.read state 1 |> Int64.to_int with
        | 0 ->
            let p = des_ident state in
            let c = des_content state in
            Inlined (p, c)
        | 1 -> Pointer (des_ident state)
        | i -> raise (DeserializeError (i, "Unknown pointer or inlined"))


    (** Operation deserializers **)

    and des_manager_operation state =
      let open Op in
      match LBB.read state 3 |> Int64.to_int with
      | 0 ->
        let delegate = des_x_option des_key_hash state in
        let script = des_x_y_pair des_value des_live_contract state in
        let credit = des_dun state in
        let preorigination = des_x_option des_address state in
        Origination { delegate; script; credit; preorigination }
      | 1 ->
        let amount = des_dun state in
        let parameters = des_x_option des_value state in
        let entrypoint = LBB.read_str_repr state in
        let destination = des_address state in
        Transaction { amount; parameters; entrypoint; destination }
      | 2 -> Delegation (des_x_option des_key_hash state)
      | 3 ->
        let target = des_x_option des_key_hash state in
        let maxrolls = des_x_option (des_x_option LBB.read_uint) state in
        let admin = des_x_option (des_x_option des_address) state in
        let white_list = des_x_option (des_x_list des_address) state in
        let delegation = des_x_option LBB.read_bool state in
        Dune_manage_account { target; maxrolls; admin; white_list; delegation }
      | i -> raise (DeserializeError (i, "Unknown internal operation"))

    and des_internal_op state =
      let source = des_address state in
      let operation = des_manager_operation state in
      let nonce = LBB.read_uint state in
      Op.{ source; operation; nonce }

    and des_big_map_diff_item state =
      let i = LBB.read state 3 |> Int64.to_int in
      match i with
      | 0 ->
        let big_map = LBB.read_z state in
        let diff_key = des_value state in
        let diff_key_hash = des_script_expr_hash state in
        let diff_value = des_x_option des_value state in
        Op.Update { big_map; diff_key; diff_key_hash; diff_value }
      | 1 ->
        Clear (LBB.read_z state)
      | 2 ->
        let ids = LBB.read_z state in
        let idd = LBB.read_z state in
        Copy (ids, idd)
      | 3 ->
        let big_map = LBB.read_z state in
        let key_type = Type.des_type state in
        let value_type = Type.des_type state in
        Alloc { big_map; key_type; value_type }
      | i -> raise (DeserializeError (i, "Unknown big map diff item"))

    and des_op state =
      des_internal_op state,
      des_x_option (des_x_list des_big_map_diff_item) state


    (** Structure deserializers **)


    and des_init state =
      let vinit_code = des_value state in
      let vinit_typ = Type.des_type state in
      let vinit_persist = LBB.read_bool state in
      ({ vinit_code; vinit_typ; vinit_persist } : LiveStructure.init)

    and des_entry state =
      let ventry_code = des_value state in
      let ventry_fee_code = des_x_option des_value state in
      let ventry_typ = Type.des_type state in
      ({ ventry_code; ventry_fee_code; ventry_typ } : LiveStructure.entry)

    and des_view state =
      let vview_code = des_value state in
      let vview_typ = Type.des_type state in
      let vview_recursive = des_rec state in
      ({ vview_code; vview_typ; vview_recursive } : LiveStructure.view)

    and des_cvalue state =
      let vvalue_code = des_value state in
      let vvalue_typ = Type.des_type state in
      let vvalue_visibility = des_visibility state in
      let vvalue_recursive = des_rec state in
      LiveStructure.{ vvalue_code; vvalue_typ;
                      vvalue_visibility; vvalue_recursive }

    and des_content state =
      let open LiveStructure in
      match LBB.read state 4 |> Int64.to_int with
      | 0 -> let k = des_type_kind state in VType (k, Type.des_typedef state)
      | 1 -> VException (des_x_list Type.des_type state)
      | 2 -> VInit (des_init state)
      | 3 -> VEntry (des_entry state)
      | 4 -> VView (des_view state)
      | 5 -> VValue (des_cvalue state)
      | 6 -> VStructure (des_live_structure state)
      | 7 -> VSignature (Type.des_signature state)
      | i -> raise (DeserializeError (i, "Unknown content"))

    and des_live_structure state =
      let kind = Type.des_kind state in
      let content = des_str_repr_x_list des_content state in
      LiveStructure.{ kind; content }

    and bindings : type a. (int * a) list -> a IntMap.t =
      fun (l : (int * a) list) ->
      List.fold_left
        (fun acc (i, b) -> IntMap.add i b acc) IntMap.empty l

    and des_live_contract state =
      let version = des_x_x_pair LBB.read_uint state in
      let root_struct = des_live_structure state in
      LiveContract.{ version; root_struct }

    and des_fee_code state =
      let version = des_x_x_pair LBB.read_uint state in
      let root_struct = des_live_structure state in
      let fee_codes =
        des_str_repr_x_list (des_x_y_pair des_value Type.des_type) state in
      FeeCode.{ version; root_struct; fee_codes }

    let deserialize b = des des_value b
    let deserialize_live_structure b = des des_live_structure b
    let deserialize_live_contract b = des des_live_contract b
    let deserialize_fee_code b = des des_fee_code b
  end
end
