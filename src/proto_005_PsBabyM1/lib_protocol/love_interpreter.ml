(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_pervasives.Exceptions
open Love_runtime_ast
open Love_context
open Love_value
open Love_value.Value
open Love_ast_types

let qualify_exn ctxt env exn =
  if has_protocol_revision 4 then
    match exn with
    | RFail t -> return (ctxt, RFail t)
    | RException id ->
        Love_env.find_exn_name_opt ctxt env id
        >>=? fun (ctxt, id_opt) ->
        match id_opt with
        | Some id -> return (ctxt, RException id)
        | None -> raise (InvariantBroken
                           (Format.asprintf "Exception not found: %a"
                              Ident.print_strident id))
  else
    return (ctxt, exn)

let deconstruct_pattern
    (pattern : Love_runtime_ast.pattern) (value : Love_value.Value.t) :
  Love_value.Value.env option
  =
  let exception PatternDoesNotMatch in
  let add_uniq x v pxl =
    let id = Ident.create_id x in
    if not (List.mem_assoc id pxl) then (id, Value.Local v) :: pxl
    else raise (InvariantBroken "Variable bound several times in pattern")
  in
  let add_uniq_struct x (_st, a) pxl =
    let id = Ident.create_id x in
    let path = Ident.create_id (Contract_repr.to_b58check a) in
    if not (List.mem_assoc id pxl) then (id, Value.Pointer path) :: pxl
    else raise (InvariantBroken "Variable bound several times in pattern")
  in
  let rec aux pxl p v = match p, v with
    | RPAny, _ -> pxl
    | RPVar x, _ -> { pxl with values = add_uniq x v pxl.values }
    | RPAlias (p, x), _ ->
        let pxl = aux pxl p v in
        { pxl with values = add_uniq x v pxl.values }
    | RPConst c, v when Value.(equal (of_const c) v) -> pxl
    | RPList pl, VList vl -> aux_list pxl pl vl
    | RPTuple pl, VTuple vl ->
        begin
          try List.fold_left2 aux pxl pl vl
          with Invalid_argument _ ->
            raise (InvariantBroken "Wrong arity in pattern")
        end
    | RPConstr (pcstr, [RPAny]), VConstr (vcstr, _)
      when String.equal pcstr vcstr &&
           Love_pervasives.has_protocol_revision 3 ->
        pxl
    | RPConstr (pcstr, pl), VConstr (vcstr, [VTuple vl])
    | RPConstr (pcstr, pl), VConstr (vcstr, vl)
      when String.equal pcstr vcstr ->
        begin
          try List.fold_left2 aux pxl pl vl
          with Invalid_argument _ ->
            raise (InvariantBroken "Wrong arity in pattern")
        end
    | RPContract (n, _st), VContractInstance (st, a) ->
        { pxl with structs = add_uniq_struct n (st, a) pxl.structs }
    | RPOr pl, v -> begin
        let rec loop = function
          | [] -> raise PatternDoesNotMatch
          | hd :: tl ->
              try aux pxl hd v with
                PatternDoesNotMatch -> loop tl
        in loop pl
      end
    | RPRecord pl, VRecord l ->
        List.fold_left
          (fun pxl (name, p) ->
             match List.assoc_opt name l with
               None ->
                 raise (
                   InvariantBroken (
                     Format.asprintf
                       "Field %s not found in value %a"
                       name
                       Love_printer.Value.print v
                   )
                 )
             | Some v ->
                 match p with
                 | None -> { pxl with values = add_uniq name v pxl.values }
                 | Some p -> (aux pxl p v)
          )
          pxl
          pl.fields

    | (RPConst _ | RPList _
      | RPTuple _ | RPConstr _ | RPContract _ | RPRecord _), _ ->
        raise PatternDoesNotMatch (* or matching stuff of different types *)
  and aux_list pxl pl vl = match pl, vl with
    | [], [] -> pxl
    | p :: [], vl -> aux pxl p (VList vl)
    | p :: pl, v :: vl -> aux_list (aux pxl p v) pl vl
    | _ :: _, [] -> raise PatternDoesNotMatch
    | [], _ :: _ -> raise PatternDoesNotMatch (* should not occur *)
  in
  try
    (* only values and structs *)
    let pxl = aux { values = []; structs = []; sigs = []; exns = [];
                    types = []; tvars = [] } pattern value in
    Some { values = List.rev pxl.values; structs = List.rev pxl.structs;
           sigs = List.rev pxl.sigs; exns = List.rev pxl.exns;
           types = List.rev pxl.types; tvars = List.rev pxl.tvars }
  with PatternDoesNotMatch -> None

let rec eval
    (ctxt : Love_context.t) (env : Love_env.t) (exp : Love_runtime_ast.exp) :
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t
  =
    let module G = Love_gas.Cost_of in
    let env = Love_env.inc_depth env in
    consume_gas ctxt G.cycle >>=? fun ctxt ->
    match exp with

    | RConst c ->
        consume_gas ctxt G.const >>|? fun ctxt ->
        (ctxt, Value.of_const c)

    | RVar id ->
        consume_gas ctxt G.var >>=? fun ctxt ->
        Love_env.find_value_opt ctxt env id >>=? fun (ctxt, res) ->
        begin match res with
          | Some v ->
              preprocess_constant_prim ctxt env id v TYPE.ANone
          | None -> raise (InvariantBroken (
              Format.asprintf "Variable not found: %a" Ident.print_strident id))
        end

    | RVarWithArg (id, xa) ->
        consume_gas ctxt G.var >>=? fun ctxt ->
        Love_env.find_value_opt ctxt env id >>=? fun (ctxt, res) ->
        begin match res with
          | Some v -> preprocess_constant_prim ctxt env id v xa
          | None -> raise (InvariantBroken (
              Format.asprintf "Variable not found: %a" Ident.print_strident id))
        end

    | RLet { bnd_pattern; bnd_val; body } ->
        consume_gas ctxt G.letin >>=? fun ctxt ->
        eval ctxt env bnd_val >>=? fun (ctxt, bval) ->
        begin match deconstruct_pattern bnd_pattern bval with
          | Some bl -> eval ctxt (Love_env.add_bindings env bl) body
          | None -> raise (InvariantBroken "Incomplete pattern matching in let")
        end

    | RLetRec { bnd_var = v; bnd_val; body; _ } ->
        let id = Ident.create_id v in
        consume_gas ctxt G.letrec >>=? fun ctxt ->
        eval_rec_fun ctxt env id bnd_val >>=? fun (ctxt, v) ->
        eval ctxt (Love_env.add_local_value env id v) body

    | RLambda lambda -> (* G.tlambda ? *)
        consume_gas ctxt G.lambda >>=? fun ctxt ->
        let free_vars = Love_free_vars.exp exp in
        Love_env.get_closure ctxt env free_vars >>|? fun (ctxt, call_env) ->
        (ctxt, VClosure { call_env; lambda })

    | RApply { exp; args } -> (* G.tapply *)
        consume_gas ctxt G.apply >>=? fun ctxt ->
        eval ctxt env exp >>=? fun (ctxt, v) ->
        apply_value ctxt env v (Exp args) >>|? fun (ctxt, v) ->
        (ctxt, v)

    | RSeq el ->
        consume_gas ctxt G.seq >>=? fun ctxt ->
        fold_left_s (fun (ctxt, _v) e -> eval ctxt env e) (ctxt, VUnit) el

    | RIf { cond; ifthen; ifelse } ->
        consume_gas ctxt G.ite >>=? fun ctxt ->
        eval ctxt env cond >>=? fun (ctxt, res) ->
        begin match res with
          | VBool true -> eval ctxt env ifthen
          | VBool false -> eval ctxt env ifelse
          | _ -> raise (InvariantBroken "If condition must be a boolean")
        end

    | RMatch { arg; cases } ->
        let rec eval_match ctxt env c = function
          | [] -> raise (InvariantBroken "Incomplete pattern matching in match")
          | (p, e) :: cases ->
              match deconstruct_pattern p c with
              | Some bl -> eval ctxt (Love_env.add_bindings env bl) e
              | None -> eval_match ctxt env c cases
        in
        consume_gas ctxt G.matchwith >>=? fun ctxt ->
        eval ctxt env arg >>=? fun (ctxt, c) ->
        eval_match ctxt env c cases

    | RConstructor { type_name = _; constr; args; ctyps = _ } ->
        consume_gas ctxt (G.construct (List.length args)) >>=? fun ctxt ->
        fold_left_s (fun (ctxt, vl) e ->
            eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, v :: vl)
          ) (ctxt, []) args >>|? fun (ctxt, vl) ->
        (ctxt, VConstr (constr, List.rev vl))

    | RNil ->
        consume_gas ctxt (G.list 1) >>|? fun ctxt ->
        (ctxt, VList [])

    | RList (el, e) ->
        begin
          consume_gas ctxt (G.list (List.length el)) >>=? fun ctxt ->
          eval ctxt env e >>=? function
          | ctxt, VList vl ->
              fold_left_s (fun (ctxt, vl) e ->
                  eval ctxt env e >>|? fun (ctxt, v) ->
                  (ctxt, v :: vl)
                ) (ctxt, vl) (List.rev el) >>|? fun (ctxt, vl) ->
              (ctxt, VList vl)
          | _ -> raise (InvariantBroken
                          "Applying const to something other than a list")
        end

    | RTuple el ->
        consume_gas ctxt (G.tuple (List.length el)) >>=? fun ctxt ->
        fold_left_s (fun (ctxt, vl) e ->
            eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, v :: vl)
          ) (ctxt, []) el >>|? fun (ctxt, vl) ->
        (ctxt, VTuple (List.rev vl))

    | RProject { tuple; indexes } ->
        consume_gas ctxt G.project >>=? fun ctxt ->
        eval ctxt env tuple >>|? fun (ctxt, res) ->
        begin match res with
          | VTuple vl ->
              let vl = List.map (fun i ->
                  match List.nth_opt vl i with
                  | Some v -> v
                  | None -> raise (InvariantBroken "Project outside of tuple")
                ) indexes
              in
              begin match vl with
                | [v] -> ctxt, v
                | vl -> ctxt, VTuple vl
              end
          | _ -> raise (InvariantBroken "Project must be applied to tuple")
        end

    | RUpdate { tuple; updates } ->
        consume_gas ctxt (G.update (List.length updates)) >>=? fun ctxt ->
        eval ctxt env tuple >>=? fun (ctxt, res) ->
        begin match res with
          | VTuple vl ->
              fold_left_s (fun (ctxt, ul) (i, e) ->
                  eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, (i, v) :: ul)
                ) (ctxt, []) updates >>|? fun (ctxt, ul) ->
              let vl, _ =
                List.fold_left (fun (vl, idx) v ->
                    match List.assoc_opt idx ul with
                    | Some uv -> (uv :: vl, idx + 1)
                    | None -> (v :: vl, idx + 1)
                  ) ([], 0) vl
              in
              ctxt, VTuple (List.rev vl)
          | _ -> raise (InvariantBroken "Update must be applied to tuple")
        end

    | RRecord { type_name = _; contents = rec_def } ->
        consume_gas ctxt (G.record (List.length rec_def)) >>=? fun ctxt ->
        fold_left_s (fun (ctxt, l) (f, e) ->
            eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, (f, v) :: l)
          ) (ctxt, []) rec_def >>|? fun (ctxt, l) ->
        (ctxt, VRecord (List.rev l))

    | RGetField { record; field } ->
        consume_gas ctxt G.getfield >>=? fun ctxt ->
        eval ctxt env record >>|? fun (ctxt, res) ->
        begin match res with
          | VRecord fl ->
              begin match List.assoc_opt field fl with
                | Some v -> ctxt, v
                | None -> raise (InvariantBroken "GetField of inexistant field")
              end
          | _ -> raise (InvariantBroken "GetField must be applied to record")
        end

    | RSetFields { record; updates } ->
        consume_gas ctxt (G.setfields (List.length updates))>>=? fun ctxt ->
        eval ctxt env record >>=? fun (ctxt, res) ->
        begin match res with
          | VRecord fl ->
              fold_left_s (fun (ctxt, ul) (f, e) ->
                  eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, (f, v) :: ul)
                ) (ctxt, []) updates >>|? fun (ctxt, ul) ->
              let fl =
                List.fold_left (fun fl (f, v) ->
                    match List.assoc_opt f ul with
                    | Some uv -> (f, uv) :: fl
                    | None -> (f, v) :: fl
                  ) [] fl
              in
              ctxt, VRecord (List.rev fl)
          | _ -> raise (InvariantBroken "SetFields must be applied to record")
        end

    | RPackStruct sr ->
        consume_gas ctxt G.pack_contract >>=? fun ctxt ->
        begin match sr with
          | RAnonymous s ->
              if Love_runtime_ast.is_module s then
                raise (InvariantBroken
                         "Only contracts can be made first-class");
              eval_structure ctxt [CONSTANTS.anonymous]
                (Love_env.re_init env CONSTANTS.anonymous) s
              >>|? fun (ctxt, _env, s) ->
              ctxt, VPackedStructure
                (CONSTANTS.anonymous_id,
                 { version = Options.version; root_struct = s })
          | RNamed sn ->
              Love_env.find_struct_opt ctxt env sn >>|? fun (ctxt, res) ->
              begin match res with
                | Some (path, s) ->
                    if Love_value.LiveStructure.is_module s then
                      raise (InvariantBroken
                               "Only contracts can be made first-class");
                    ctxt, VPackedStructure
                      (path, { version = Options.version; root_struct = s })
                | None -> raise (InvariantBroken
                                   (Format.asprintf "Structure not found: %a"
                                      Ident.print_strident sn))
              end
        end

    | RRaise { exn; args; loc } ->
        consume_gas ctxt (G.raiseexn (List.length args)) >>=? fun ctxt ->
        qualify_exn ctxt env exn >>=? fun (ctxt, exn) ->
        fold_left_s (fun (ctxt, vl) e ->
            eval ctxt env e >>|? fun (ctxt, v) -> (ctxt, v :: vl)
          ) (ctxt, []) args >>|? fun (ctxt, vl) ->
        raise (UserException (ctxt, (exn, List.rev vl), loc))

    | RTryWith { arg; cases } ->
        consume_gas ctxt G.trywith >>=? fun ctxt ->
        Lwt.catch (fun () -> eval ctxt env arg)
          (function
            | (UserException (ctxt, (exn, vl), _)) as e ->
                let rec eval_match ctxt env exn vl = function
                  | [] -> raise e (* Uncaught : re-raise *)
                  | ((exn', pl), e) :: cases ->
                      qualify_exn ctxt env exn' >>=? fun (ctxt, exn') ->
                      if Love_runtime_ast.exn_equal exn exn'
                      then match deconstruct_pattern
                                   (RPConstr (exn_id exn', pl))
                                   (VConstr ((exn_id exn), vl)) with
                      | Some bl -> eval ctxt (Love_env.add_bindings env bl) e
                      | None -> eval_match ctxt env exn vl cases
                      else eval_match ctxt env exn vl cases
                in eval_match ctxt env exn vl cases
            | e -> raise e (* Not a user exception : re-raise *)
          )

and eval_rec_fun
    (ctxt : Love_context.t) (env : Love_env.t)
    (id : string Ident.t) (v : Love_runtime_ast.exp) :
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t
  =
  let env = Love_env.add_local_value env id VUnit in (* Dummy *)
  eval ctxt env v >>|? fun (ctxt, v) ->
  match v with
  | VClosure { call_env; lambda } ->
      let values = List.filter (fun (id', _) ->
          not (Ident.equal String.equal id id')) call_env.values in
      let rec v =
        VClosure { lambda;
                   call_env = { call_env with
                                values = (id, Local v) :: values } } in
      ctxt, v
  | _ ->
      raise (InvariantBroken "`let/val rec` must bind a function")

and preprocess_constant_prim
    (ctxt : Love_context.t) (env : Love_env.t)
    (id : string Ident.t) (value : Love_value.Value.t)
    (xa : TYPE.extended_arg) :
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t
  =
  match value, xa with
  | VPrimitive (prim, ANone, al), _ ->
      Love_prim_interp.eval ctxt env (prim, xa) al
  | VPrimitive (_, AContractType _, _), _ ->
      raise (InvariantBroken (
          Format.asprintf "Primitive already has an extended argument: %a"
            Ident.print_strident id))
  | _, AContractType _ ->
      raise (InvariantBroken (
          Format.asprintf "Only a primitive can be given an extended arg: %a"
            Ident.print_strident id))
  | v, ANone ->
      return (ctxt, v)

(* Avoid reconstructing closures on each application,
   try applying as many arguments as possible at once *)
and apply_exp
    (ctxt : Love_context.t) (env : Love_env.t)
    (exp : Love_runtime_ast.exp) (al : Love_context.val_or_exp)
    (call_env: Value.env) :
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t
  =
  match exp, al with
  | RLambda { args = (RTypeVar tv) :: args; body }, Exp ((RType t) :: al) ->
      let body = match args with [] -> body | _ -> RLambda { args; body } in
      let call_env = { call_env with
                       tvars = (tv.tv_name, t) :: call_env.tvars } in
      apply_exp ctxt env body (Exp al) call_env
  | RLambda { args = (RPattern (arg_pattern, _)) :: args; body },
    Exp ((RExp e) :: al) ->
      eval ctxt env e >>=? fun (ctxt, v) ->
      begin match deconstruct_pattern arg_pattern v with
        | Some bl ->
            let body = match args with
              | [] -> body
              | _ -> RLambda { args; body }
            in
            let call_env = { values = bl.values @ call_env.values;
                             structs = bl.structs @ call_env.structs;
                             sigs = bl.sigs @ call_env.sigs;
                             exns = bl.exns @ call_env.exns;
                             types = bl.types @ call_env.types;
                             tvars = bl.tvars @ call_env.tvars } in
            apply_exp ctxt env body (Exp al) call_env
        | None ->
            raise (InvariantBroken "Incomplete pattern matching in lambda")
      end
  | RLambda { args = (RPattern (arg_pattern, _)) :: args; body },
    Val (v :: al) ->
      begin match deconstruct_pattern arg_pattern v with
        | Some bl ->
            let body = match args with
                | [] -> body
                | _ -> RLambda { args; body }
            in
            let call_env = { values = bl.values @ call_env.values;
                             structs = bl.structs @ call_env.structs;
                             sigs = bl.sigs @ call_env.sigs;
                             exns = bl.exns @ call_env.exns;
                             types = bl.types @ call_env.types;
                             tvars = bl.tvars @ call_env.tvars } in
            apply_exp ctxt env body (Val al) call_env
        | None ->
            raise (InvariantBroken "Incomplete pattern matching in lambda")
      end
  | RLambda lambda, (Exp [] | Val []) ->
      return (ctxt, VClosure { call_env; lambda })
  | _, _ ->
      (* could use a clean env *)
      eval ctxt (Love_env.add_bindings env call_env) exp >>=? fun (ctxt, v) ->
      apply_value ctxt env v al

and apply_value
    (ctxt : Love_context.t) (env : Love_env.t)
    (value : Love_value.Value.t) (args : Love_context.val_or_exp) :
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t
  =
  match value, args with
  | VPrimitive (prim, xl, al), Exp (RExp e :: args) ->
      eval ctxt env e >>=? fun (ctxt, v) ->
      Love_prim_interp.eval ctxt env (prim, xl) (V v :: al)
      >>=? fun (ctxt, v) ->
      apply_value ctxt env v (Exp args)
  | VPrimitive (prim, xl, al), Exp (RType t :: args) ->
      Love_prim_interp.eval ctxt env (prim, xl) (T t :: al)
      >>=? fun (ctxt, v) ->
      apply_value ctxt env v (Exp args)
  | VPrimitive (prim, xl, al), Val (v :: args) ->
      Love_prim_interp.eval ctxt env (prim, xl) (V v :: al)
      >>=? fun (ctxt, v) ->
      apply_value ctxt env v (Val args)
  | VClosure { call_env; lambda }, _ ->
      apply_exp ctxt env (RLambda lambda) args call_env
  | VView (kt1, name), _->
      begin
        Love_context.resolve_path ctxt kt1 (Some (Ident.create_id name))
        >>=? function
        | ctxt, Some (CntLove (VView {vview_code; _})) ->
            apply_value ctxt env vview_code args
        | _ ->
            raise (InvariantBroken (
                Format.asprintf "View %a is not a callable view"
                  Love_printer.Value.print value))
      end
  | _, Exp (RType _ :: args) -> (* ignore type applications on primitives *)
      apply_value ctxt env value (Exp args) (* and values other than closures *)
  | _, (Exp [] | Val []) ->
      return (ctxt, value)
  | v, _ ->
      raise (InvariantBroken (Format.asprintf
                                "Can only apply functions/primitives: \
                                 %a is not a function nor a primitive"
                                Love_printer.Value.print v))

and eval_structure
    (ctxt : Love_context.t) (path : string list) (env : Love_env.t)
    Love_runtime_ast.{ structure_content; kind } :
  (Love_context.t * Love_env.t * Love_value.LiveStructure.t) tzresult Lwt.t
  =
  let open LiveStructure in
  let env = match kind with
    | Module -> env
    | Contract l -> Love_env.add_deps env l
  in
  fold_left_s (fun (ctxt, env, content) (n, c) ->
      let id = Ident.create_id n in
      match c with
      | RDefType (k, td) ->
          let path = Ident.put_in_namespaces path id in
          let env = Love_env.add_type_name env id path in
          let content = (n, VType (k, td)) :: content in
          return (ctxt, env, content)
      | RDefException tl ->
          let path = Ident.put_in_namespaces path id in
          let env = Love_env.add_exn_name env id path in
          let content = (n, VException tl) :: content in
          return (ctxt, env, content)
      | RInit { init_code; init_typ; init_persist } ->
          eval ctxt env init_code >>|? fun (ctxt, ic) ->
          let path = Ident.put_in_namespaces path id in
          let env = Love_env.add_inlined_value env id path ic in
          let init = { vinit_code = ic; vinit_typ = init_typ;
                       vinit_persist = init_persist } in
          let content = (n, VInit init) :: content in
          ctxt, env, content
      | REntry { entry_code; entry_fee_code; entry_typ } ->
          eval ctxt env entry_code >>=? fun (ctxt, ec) ->
          begin match ec, entry_fee_code with
            | VClosure { lambda = l; _ }, Some fc ->
                begin match Love_runtime_ast.lambdas_to_params (RLambda l) with
                  | pt1 :: pt2 :: pt3 :: _ ->
                      let lam =
                        Love_runtime_ast.params_to_lambdas [pt1; pt2; pt3] fc in
                      eval ctxt env lam >>|? fun (ctxt, fee_code) ->
                      (ctxt, Some fee_code)
                  | _ -> raise (InvariantBroken "Invalid entry point")
                end
            | _ -> return (ctxt, None)
          end >>|? fun (ctxt, fc) ->
          let path = Ident.put_in_namespaces path id in
          let env = Love_env.add_inlined_value env id path ec in
          let entry = { ventry_code = ec; ventry_fee_code = fc;
                        ventry_typ = entry_typ } in
          let content = (n, VEntry entry) :: content in
          ctxt, env, content
      | RView { view_code; view_typ; view_recursive } ->
          begin
            match view_recursive with
            | Rec -> eval_rec_fun ctxt env id view_code
            | NonRec -> eval ctxt env view_code end >>|? fun (ctxt, vc) ->
          let path = Ident.put_in_namespaces path id in
          let env = Love_env.add_inlined_value env id path vc in
          let view = {
            vview_code = vc;
            vview_typ = view_typ;
            vview_recursive = view_recursive
          } in
          let content = (n, VView view) :: content in
          ctxt, env, content
      | RValue { value_code; value_typ; value_visibility; value_recursive } ->
          begin match value_recursive with
            | Rec -> eval_rec_fun ctxt env id value_code
            | NonRec -> eval ctxt env value_code
          end >>|? fun (ctxt, v) ->
          let path = Ident.put_in_namespaces path id in
          let env = Love_env.add_inlined_value env id path v in
          let value = { vvalue_code = v; vvalue_typ = value_typ;
                        vvalue_visibility = value_visibility;
                        vvalue_recursive = value_recursive } in
          let content = (n, VValue value) :: content in
          ctxt, env, content
      | RStructure s ->
          eval_structure ctxt (n :: path) env s >>|? fun (ctxt, _env, s) ->
          let path = Ident.put_in_namespaces path id in
          let env = Love_env.add_inlined_struct env id path s in
          let content = (n, VStructure s) :: content in
          ctxt, env, content
      | RSignature s ->
          let path = Ident.put_in_namespaces path id in
          let env = Love_env.add_inlined_sig env id path s in
          let content = (n, VSignature s) :: content in
          return (ctxt, env, content)
    ) (ctxt, env, []) structure_content >>|? fun (ctxt, env, content) ->
  ctxt, env, { content = List.rev content; kind }

(** Typecheck some code *)
let typecheck_code
    (actxt : Alpha_context.t) (code : AST.top_contract) :
  Alpha_context.t tzresult Lwt.t
  =
  Lwt.catch (fun () ->
      let AST.{ version = _; code } = code in
      let type_env = Love_tenv.empty code.kind () in
      let _code, _type_env =
        Love_typechecker.typecheck_struct None type_env code in
      return actxt
    ) Love_errors.exn_handler

(** Typecheck a value *)
let typecheck_data
    (actxt : Alpha_context.t) (data : Love_value.Value.t) (typ : TYPE.t) :
  Alpha_context.t tzresult Lwt.t
  =
  Lwt.catch (fun () ->
      let type_env = Love_tenv.empty Module () in
      let ctxt = Love_context.init ~type_env actxt in
      let expected_ty = TYPE.{ lettypes = []; body = typ } in
      Love_typechecker.typecheck_value ctxt expected_ty data
      >>|? fun ctxt -> ctxt.actxt
    ) Love_errors.exn_handler

(** Typecheck the contract and turn it into a live_contract *)
let typecheck
    (actxt : Alpha_context.t) ~(source : Contract_repr.t)
    ~(payer : Contract_repr.t) ~(self : Contract_repr.t)
    ~(amount : Tez_repr.t) ~(code: AST.top_contract)
    ~(storage: Love_value.Value.t) :
  (Alpha_context.t *
   Love_value.LiveContract.t *
   Love_value.Value.t) tzresult Lwt.t
  =
  Lwt.catch (fun () ->
      let version = code.version in
      let type_env = Love_tenv.empty code.code.kind () in
      let ctxt =
        if has_protocol_revision 3 then
          Love_context.init ~self ~source ~payer ~amount ~type_env actxt
        else
          Love_context.init ~self ~type_env actxt
      in
      Love_typechecker.typecheck_top_contract_deps None ctxt code
      >>=? fun (ctxt, (code, type_env)) ->
      let ctxt = Love_context.set_type_env ctxt type_env in
      let code = Love_translator.translate_structure type_env code.code in
      let kt1 = Contract_repr.to_b58check self in
      eval_structure ctxt [kt1] (Love_env.init kt1) code
      >>=? fun (ctxt, env, root_struct) ->
      begin
        match List.assoc_opt CONSTANTS.init_storage root_struct.content with
        | Some (VInit { vinit_code = VClosure _ as fct;
                        vinit_typ = pt; vinit_persist }) ->
            let root_struct =
              if vinit_persist then root_struct
              else { root_struct with
                     content = List.filter (fun (n, _c) ->
                         not (String.equal n CONSTANTS.init_storage)
                       ) root_struct.content }
            in
            let expected_ty = TYPE.{ lettypes = []; body = pt } in
            Love_typechecker.typecheck_value ctxt expected_ty storage
            >>=? fun ctxt ->
            Love_translator.make_types_absolute_in_value ctxt env storage
            >>=? fun (ctxt, storage) ->
            let ctxt = Love_context.init
                ~self ctxt.actxt ~code:LiveContract.{ version; root_struct } in
            apply_value ctxt (Love_env.init "") fct (Val [storage])
            >>=? fun (ctxt, storage) ->
            Love_translator.inline_value ctxt storage
            >>|? fun (ctxt, storage) ->
            ctxt, root_struct, storage
        | Some _ ->
            raise (InvariantBroken "Bad storage initializer")
        | None ->
            let storage_type =
              TYPE.{ lettypes = [];
                          body = TUser (Ident.create_id "storage", []) } in
            Love_typechecker.typecheck_value ctxt storage_type storage
            >>=? fun ctxt ->
            Love_translator.make_types_absolute_in_value ctxt env storage
            >>|? fun (ctxt, storage) ->
            ctxt, root_struct, storage
      end
      >>|? fun (ctxt, root_struct, storage) ->
      ctxt.actxt, LiveContract.{ version; root_struct }, storage
    ) Love_errors.exn_handler

(** Typecheck the contract and turn it into a live_contract *)
let preprocess_already_typechecked
    (actxt : Alpha_context.t) ~(source : Contract_repr.t)
    ~(payer : Contract_repr.t) ~(self : Contract_repr.t)
    ~(amount : Tez_repr.t) ~(code: Love_value.LiveContract.t)
    ~(storage: Love_value.Value.t) :
  (Alpha_context.t * LiveContract.t * Value.t) tzresult Lwt.t
  =
  Lwt.catch (fun () ->
      let LiveContract.{ version; root_struct } = code in
      let type_env = Love_tenv.empty code.root_struct.kind () in
      let ctxt =
        if has_protocol_revision 3 then
          Love_context.init ~self ~source ~payer ~amount ~type_env actxt
        else
          Love_context.init ~self ~type_env actxt
      in
      let deps = match code.root_struct.kind with
        | TYPE.Module -> []
        | TYPE.Contract l -> l in
      Love_typechecker.extract_deps None ctxt deps
      >>=? fun ctxt ->
      let root_sig =
        Love_translator.sig_of_structure ~only_typedefs:true code.root_struct in
      let type_env =
        Love_tenv.contract_sig_to_env None root_sig ctxt.type_env in
      let ctxt = Love_context.set_type_env ctxt type_env in
      let env = Love_env.init "" in

      begin
        match List.assoc_opt CONSTANTS.init_storage root_struct.content with
        | Some (VInit { vinit_code = VClosure _ as fct;
                        vinit_typ = pt; vinit_persist }) ->
            let root_struct =
              if vinit_persist then root_struct
              else { root_struct with
                     content = List.filter (fun (n, _c) ->
                         not (String.equal n CONSTANTS.init_storage)
                       ) root_struct.content }
            in
            let expected_ty = TYPE.{ lettypes = []; body = pt } in
            Love_typechecker.typecheck_value ctxt expected_ty storage
            >>=? fun ctxt ->
            Love_translator.make_types_absolute_in_value ctxt env storage
            >>=? fun (ctxt, storage) ->
            apply_value ctxt env fct (Val [storage])
            >>=? fun (ctxt, storage) ->
            Love_translator.inline_value ctxt storage
            >>|? fun (ctxt, storage) ->
            ctxt, root_struct, storage
        | Some _ ->
            raise (InvariantBroken "Bad storage initializer")
        | None ->
            let storage_type =
              TYPE.{ lettypes = [];
                          body = TUser (Ident.create_id "storage", []) } in
            Love_typechecker.typecheck_value ctxt storage_type storage
            >>=? fun ctxt ->
            Love_translator.make_types_absolute_in_value ctxt env storage
            >>|? fun (ctxt, storage) ->
            ctxt, root_struct, storage
      end
      >>|? fun (ctxt, root_struct, storage) ->
      ctxt.actxt, LiveContract.{ version; root_struct }, storage
    ) Love_errors.exn_handler

let execute
    (actxt : Alpha_context.t) ~(source : Contract_repr.t)
    ~(payer : Contract_repr.t) ~(self : Contract_repr.t)
    ~(amount : Tez_repr.t) ~(entrypoint : string)
    ~(code: Love_value.LiveContract.t) ~(storage: Love_value.Value.t)
    ~(parameter: Love_value.Value.t) :
  (Alpha_context.t *
   (Value.t *
    Op.internal_operation list *
    Op.big_map_diff option *
    Value.t)) tzresult Lwt.t =
  Lwt.catch (fun () ->
      let type_env = Love_tenv.empty code.root_struct.kind () in
      let ctxt =
        Love_context.init ~self ~source ~payer ~amount
          ~code ~storage ~type_env actxt in
      match List.assoc_opt entrypoint code.root_struct.content with
      | Some (VEntry { ventry_code = VClosure _ as fct; ventry_typ; _ }) ->
          let deps = match code.root_struct.kind with
            | TYPE.Module -> []
            | TYPE.Contract l -> l in
          Love_typechecker.extract_deps None ctxt deps
          >>=? fun ctxt ->
          let root_sig = Love_translator.sig_of_structure
              ~only_typedefs:true code.root_struct in
          let type_env =
            Love_tenv.contract_sig_to_env None root_sig ctxt.type_env in
          let ctxt = Love_context.set_type_env ctxt type_env in
          let expected_ty = TYPE.{ lettypes = []; body = ventry_typ } in
          Love_typechecker.typecheck_value ctxt expected_ty parameter
          >>=? fun ctxt ->
          Love_prim_interp.collect_big_maps ctxt parameter
          >>=? fun (to_duplicate, ctxt) ->
          Love_prim_interp.collect_big_maps ctxt storage
          >>=? fun (to_update, ctxt) ->
          let args = storage :: (VDun amount) :: parameter :: [] in
          apply_value ctxt (Love_env.init "") fct (Val args) >>=?
          begin function
            | ctxt, VTuple [ VList ops; storage ] ->
                Love_translator.inline_value ctxt storage
                >>=? fun (ctxt, storage) ->
                Love_prim_interp.extract_big_map_diff ctxt
                  ~temporary:false ~to_duplicate ~to_update storage
                >>|? fun (storage, big_map_diff, ctxt) ->
                let ops, op_diffs = List.split (List.map (function
                    | VOperation (op, diff) -> op, diff
                    | _ -> failwith "Not an operation") ops)
                in
                let big_map_diff = match
                    List.flatten (List.map (Option.unopt ~default:[])
                                    (op_diffs @ [ big_map_diff ])) with
                | [] -> None
                | diff -> Some diff
                in
                ctxt.actxt, (VUnit, ops, big_map_diff, storage)
            | _ -> raise BadReturnType
          end
      | _ -> raise BadEntryPoint
    ) Love_errors.exn_handler

let execute_fee_script
    (actxt : Alpha_context.t) ~(source : Contract_repr.t)
    ~(payer : Contract_repr.t) ~(self : Contract_repr.t)
    ~(amount : Tez_repr.t) ~(entrypoint : string)
    ~(fee_code: Love_value.FeeCode.t) ~(storage: Love_value.Value.t)
     ~(parameter: Love_value.Value.t) :
  (Alpha_context.t * (Tez_repr.t * Z.t)) tzresult Lwt.t
  =
  Lwt.catch (fun () ->
      let FeeCode.{ version; root_struct; fee_codes } = fee_code in
      let code = LiveContract.{ version; root_struct } in
      let type_env = Love_tenv.empty root_struct.kind () in
      let ctxt = Love_context.init ~self ~source ~payer ~amount
          ~code ~storage ~type_env actxt in
      match List.assoc_opt entrypoint fee_codes with
      | Some (fee_code, ptype) ->
          begin
            let deps = match root_struct.kind with
              | TYPE.Module -> []
              | TYPE.Contract l -> l in
            Love_typechecker.extract_deps None ctxt deps
            >>=? fun ctxt ->
            let root_sig = Love_translator.sig_of_structure
                ~only_typedefs:true root_struct in
            let type_env =
              Love_tenv.contract_sig_to_env None root_sig ctxt.type_env in
            let ctxt = Love_context.set_type_env ctxt type_env in
            let expected_ty = TYPE.{ lettypes = []; body = ptype } in
            Love_typechecker.typecheck_value ctxt expected_ty parameter
            >>=? fun ctxt ->
            let args = storage :: (VDun amount) :: parameter :: [] in
            apply_value ctxt (Love_env.init "") fee_code (Val args)
            >>|? function
            | ctxt, VTuple [ max_fee; max_storage ] ->
                let max_fee, max_storage = match max_fee, max_storage with
                  | VDun mf, VNat ms -> mf, ms
                  | _ -> failwith "Not a (dun, nat) pair"
                in
                ctxt.actxt, (max_fee, max_storage)
            | _ -> raise BadReturnType
          end
      | _ -> raise BadEntryPoint
    ) Love_errors.exn_handler

let storage_big_map_diff
    (actxt : Alpha_context.t) (storage : Love_value.Value.t) :
  (Alpha_context.t * Value.t * Op.big_map_diff option) tzresult Lwt.t
  =
  Love_prim_interp.collect_big_maps (Love_context.init actxt) storage
  >>=? fun (to_duplicate, ctxt) ->
  Love_prim_interp.extract_big_map_diff ctxt ~temporary:false
    ~to_duplicate ~to_update:Collections.ZSet.empty storage
  >>|? fun (storage, big_map_diff, ctxt) ->
  ctxt.actxt, storage, big_map_diff

let rec find_value_in_code
    (id : string Ident.t) (s : Love_value.LiveStructure.t) :
  Love_value.LiveStructure.content option
  =
  match Ident.split id with
  | vname, None -> List.assoc_opt vname s.LiveStructure.content
  | mname, Some id -> (
      match List.assoc_opt mname s.content with
      | Some (LiveStructure.VStructure c) -> find_value_in_code id c
      | _ -> None
    )

(* This function, for now, is only used by RPC: it can be modified without
   revision check *)
let execute_value
    (actxt : Alpha_context.t) ~(source : Contract_repr.t)
    ~(payer : Contract_repr.t) ~(self : Contract_repr.t)
    ~(val_name : string) ~(code: Love_value.LiveContract.t)
    ~(storage: Love_value.Value.t) ~(parameter: Love_value.Value.t option) :
  (Alpha_context.t * Value.t * TYPE.t) tzresult Lwt.t
  =
  Lwt.catch (fun () ->
      let type_env = Love_tenv.empty code.root_struct.kind () in
      let ctxt =
        Love_context.init ~self ~source ~payer ~amount:Alpha_context.Tez.zero
          ~code ~storage ~type_env actxt in
      let val_name = Ident.string_to_ident val_name in
      let deps = match code.root_struct.kind with
        | TYPE.Module -> []
        | TYPE.Contract l -> l in
      match find_value_in_code val_name code.root_struct with
      | Some (VValue { vvalue_code = VClosure _ as fct; vvalue_typ; _ }) ->
          Love_typechecker.extract_deps None ctxt deps
          >>=? fun ctxt ->
          let root_sig = Love_translator.sig_of_structure
              ~only_typedefs:true code.root_struct in
          let type_env =
            Love_tenv.contract_sig_to_env None root_sig ctxt.type_env in
          let ctxt = Love_context.set_type_env ctxt type_env in
          let rec check_typ ctxt ty params =
            match ty, params with
              TYPE.TArrow (t1, t2), hd :: tl ->
                let expected_ty = TYPE.{ lettypes = []; body = t1 } in
                Love_typechecker.typecheck_value ctxt expected_ty hd >>=?
                fun ctxt -> check_typ ctxt t2 tl
            | TArrow _, [] -> raise (InvariantBroken "Missing arguments")
            | t, [] -> return (ctxt, t)
            | _ -> raise (InvariantBroken "Too many arguments")
          in
          let args =
            match parameter, vvalue_typ with
            | Some (VTuple l), TArrow (_, TArrow _) -> l
            | Some v, TArrow _ -> [v]
            | Some _, _ -> raise BadParameter
            | None, TArrow _ -> raise BadParameter
            | None, _ -> []
          in
          check_typ ctxt vvalue_typ args >>=? fun (ctxt, t) ->
          apply_value ctxt (Love_env.init "") fct (Val args) >>=?
          fun (ctxt, v) -> Error_monad.return (ctxt.actxt, v, t)
      | Some (VValue { vvalue_code = v; vvalue_typ; _ }) ->
          Error_monad.return (ctxt.actxt, v, vvalue_typ)
      | Some (VView {vview_code; vview_typ}) ->
          Love_typechecker.extract_deps None ctxt deps
          >>=? fun ctxt ->
          Log.debug "[love_interpreter] Executing %a@."
            Love_printer.Value.print vview_code;
          let root_sig = Love_translator.sig_of_structure
              ~only_typedefs:true code.root_struct in
          let type_env =
            Love_tenv.contract_sig_to_env None root_sig ctxt.type_env in
          let ctxt = Love_context.set_type_env ctxt type_env in
          let rec check_typ ctxt ty params =
            match ty, params with
              TYPE.TArrow (t1, t2), hd :: tl ->
                let expected_ty = TYPE.{ lettypes = []; body = t1 } in
                Love_typechecker.typecheck_value ctxt expected_ty hd >>=?
                fun ctxt -> check_typ ctxt t2 tl
            | TArrow _, [] -> raise (InvariantBroken "Missing arguments")
            | t, [] -> return (ctxt, t)
            | t, _ ->
                raise (InvariantBroken (
                    Format.asprintf "Too many arguments for type %a"
                      Love_type.pretty t))
          in
          let args =
            match parameter, vview_typ with
            | Some (VTuple l), TArrow (_, TArrow _) ->
                (* The view expects at least 2 arguments, and a tuple is
                   provided: splitting the tuple *)
                l
            | Some v, TArrow (_, _) ->
                (* The view expects only 1 argument *)
                [v]
            | Some _, _ ->
                (* The view expects no argument, yet one is given *)
                raise BadParameter
            | None, TArrow (_, _) ->
                (* The view expects one argument, and none are provided *)
                raise BadParameter
            | None, _ ->
                (* No argument is provided, and the view does not expect any *)
                []
          in
          check_typ ctxt vview_typ args >>=? fun (ctxt, t) ->
          apply_value ctxt (Love_env.init "")
            vview_code (Val (storage :: args))
          >>=? fun (ctxt, v) ->
          return (ctxt.actxt, v, t)
      | _ -> raise BadParameter
    ) Love_errors.exn_handler

let get_entrypoint
    (ctxt : Alpha_context.t) ~(code : Love_value.LiveContract.t)
    ~(entrypoint : string) :
  (Alpha_context.t * TYPE.t option) tzresult Lwt.t
  =
  Lwt.catch (fun () ->
      let e = List.find_opt (function
          | (name, LiveStructure.VEntry _) -> String.equal name entrypoint
          | _ -> false
        ) code.LiveContract.root_struct.LiveStructure.content
      in
      match e with
      | Some (_,VEntry e) -> return @@ (ctxt, Some e.LiveStructure.ventry_typ)
      | Some _ -> assert false
      | _ -> return (ctxt, None)
    ) Love_errors.exn_handler

let list_entrypoints
    (ctxt : Alpha_context.t) ~(code : Love_value.LiveContract.t) :
  (Alpha_context.t * (string * TYPE.t) list) tzresult Lwt.t
  =
  Lwt.catch (fun () ->
      let e = List.fold_left (fun acc -> function
          | (name, LiveStructure.VEntry e) -> (name, e.ventry_typ) :: acc
          | _ -> acc
        ) [] code.LiveContract.root_struct.LiveStructure.content
      in
      return (ctxt, (List.rev e))
    ) Love_errors.exn_handler

let () = Love_prim_list.register_apply_value apply_value
