(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_ast_types

exception EnvironmentError of string

type typ_constructor = private
  {
    cparent : TYPE.t;
    cname   : string;
    cargs   : TYPE.t list;
    crec    : TYPE.recursive
  }

type typ_field = private
  {
    fparent : TYPE.t;
    fname   : string;
    ftyp    : TYPE.t;
    frec    : TYPE.recursive
  }

type def_loc = External | Local

type val_kind =
  | Entry
  | View of def_loc
  | Value of AST.visibility
  | Init of AST.visibility
  | Internal

  | Primitive of Love_primitive.generic_primitive
  (** Special case only for the Core environment *)

type t

and contract_type = t

type 'res request_result = {
  result : 'res; (* The request result *)
  path : Path.t; (* The relative path of the environment in which the result has been found *)
  last_env : t; (* The environment in which the result has been found *)
  index : int; (* The eventual index of the result *)
  in_core_env : bool (* true : The result is in the core environment *)
}

type 'res env_request = 'res request_result option

(** Environment mode is by default to false. If set to true, the environment
    will display Internal and Abstract definition types. *)
val set_environment_mode : bool -> unit

(** The empty environment *)
val empty : TYPE.struct_kind -> unit -> t

val env_path_str : t -> string list

(** Applies the types to the constructor
    constr_with_args (('a, 'b) t_) [int; bool] = (int, bool) t.
    Fails if the list size if different than the number of arguments *)
val constr_with_targs : typ_constructor -> TYPE.t list -> t -> typ_constructor

(** Same than before, but for fields *)
val field_with_targs : typ_field -> TYPE.t list -> t-> typ_field

(** Registers a variable with the given type. *)
val add_var : ?kind:val_kind -> string -> TYPE.t -> t -> t

(** Updates the set of free variables. *)
val add_forall : TYPE.type_var -> t -> t

val add_typedef : string -> AST.type_kind -> TYPE.typedef -> t -> t

(** Adds a type definition as it would been defined by the id in argument.
    For example, add_typedef () M1.M2.t typedef' env adds the type t
    to the substructure M1.M2 in env. The name of the typedef must be the same
    than the name given in the type identifyer.
 *)
val add_typedef_in_subcontract :
  string Ident.t -> AST.type_kind -> TYPE.typedef -> t -> t

(** Adds exceptions to the environment *)
val add_exception : string -> TYPE.t list -> t -> t

(** Adds signatures (as environments) to the environment *)
val add_signature : string -> t -> t -> t

(** Adds an abstract type to the environment *)
val add_abstract_type : string -> TYPE.type_var list -> t -> t

val add_subcontract_env : string -> t -> t -> t

val new_subcontract_env : string -> TYPE.struct_kind -> t -> t

val add_signed_subcontract : string -> string Ident.t -> t -> t

(** Add a Path.t-prefixed variablt to the environent **)
val add_var_path : string Ident.t -> TYPE.t -> t -> t

(** Returns the type associated to the variable in argument *)
val find_var    : AST.var_ident -> t -> (val_kind * TYPE.t) env_request

(** Returns the list of constructors associated to the type name in argument *)
val find_sum    :string Ident.t ->  t -> typ_constructor list env_request

(** Returns the list of fields associated to the type name in argument *)
val find_record : string Ident.t -> t -> typ_field list env_request

(** Returns the details the constructor associated to the constructor name in argument *)
val find_constr : AST.cstr_name -> t -> typ_constructor env_request

(** Returns the details the field associated to the field name in argument *)
val find_field  : AST.field_name -> t -> (typ_field) env_request

(** Same as above, but search with the specified path *)
val find_field_in  : string Ident.t -> AST.field_name -> t -> (typ_field) env_request

(** Returns the details the field associated to the field name in argument *)
val find_contract : string Ident.t -> t -> (contract_type) env_request

(** Returns the signature associated to the id in argument *)
val find_signature  : string Ident.t -> t -> (t) env_request

(** Returns the arguments on the exception name in argument *)
val find_exception : string Ident.t -> t -> (TYPE.t list) env_request

(** Returns the arguments on the exception name in argument *)
val find_type_kind :
  string Ident.t -> t ->
  ((AST.type_kind * TYPE.type_var list * TYPE.traits)) env_request

(** Applies the function in argument to the environment that first matches
    the ident. Searches in parent environments if it doesn't found it in
    a sub environment or if the correspondign sub environment has a higher
    id than before (i.e. has been registered after).. *)
val find_env :
  ?before : int ->
  string Ident.t -> t -> (string -> t -> ('b * int) option) -> ('b) env_request

val isComp : string Ident.t -> t -> bool

(** Returns the storage type of the contract if it has been registered *)
val get_storage_type : t -> TYPE.t option

(** Tests if the typevar in argument is universally quantified. *)
val is_free : TYPE.type_var -> t -> bool

(** Returns the set of free variables *)
val get_free : t -> TYPE.TypeVarSet.t

val pp_env : Format.formatter -> t -> unit

val env_to_contract_sig : t -> TYPE.structure_sig

val contract_sig_to_env : string option -> TYPE.structure_sig -> t -> t

val find_tdef : TYPE.type_name -> t -> TYPE.typedef env_request

(** Given a list of field * type and a user type name, returns the user type on which the correct
    type arguments are given
    Ex : if type name = {a : 'a; b : 'b} in the environment, then
    record_type [a, int; b, float] name = TUser (name, [int;float]) = {a : int; b : float}
    Fails if the list of fields is different than the one registered in the environment
 *)
val record_type : (string Ident.t option) -> (AST.field_name * TYPE.t) list -> t -> TYPE.t

(** Same, but for a constructors *)
val constr_type : (AST.cstr_name * TYPE.t list) -> t -> TYPE.t

(** Same, but for aliases *)
val alias :
  ?before:int ->
  TYPE.type_name -> TYPE.t list -> t ->
  ((TYPE.t * TYPE.multiple_path)) env_request

(** Returns non polymorphic record fields using the type list in argument *)
val field_applied_types : string Ident.t -> TYPE.t list -> t -> typ_field list

(** Normalizes the type in argument. If relative is set to true, returns the complete type
    composed of types relatively normalized. If set to false, returns the type with absolute names.
*)
val normalize_type : relative:bool -> TYPE.t -> t -> TYPE.t

(** Tests if a given type is composed of an internal type. *)
val is_internal : TYPE.t -> t -> bool

val get_subenv_struct : string -> t -> t option
val get_subenv_sig : string -> t -> t option


val init_core_env : unit -> unit
