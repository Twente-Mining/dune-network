(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module M = struct

  open Alpha_context
  open Love_script_repr

  module Repr = Love_script_repr

  type execution_result = {
    ctxt : Alpha_context.t;
    storage : const;
    result : const option;
    big_map_diff : Contract.big_map_diff option;
    operations : packed_internal_operation list;
  }

  type fee_execution_result = {
    ctxt : Alpha_context.t;
    max_fee : Tez.t;
    max_storage : Z.t;
  }

  type val_execution_result = {
    ctxt : Alpha_context.t;
    result : const;
  }

  let initialize_rev r =
    if Love_pervasives.update_protocol_revision r
    then begin
      Love_type_list.init ();
      Love_prim_list.init ();
      Love_tenv.init_core_env ();
      Love_env.initialize ();
    end

  let initialize ctxt =
    initialize_rev (Dune_storage.protocol_revision ctxt)

  let normalize_script ctxt self code storage =
    initialize ctxt;
    match code with
    | Contract _ -> failwith "Contract can't be normalized"
    | LiveContract c ->
        let code, fee_code_opt = Love_translator.normalize_contract self c in
        let fee_code = Option.map ~f:(fun fc -> FeeCode fc) fee_code_opt in
        return ((LiveContract code, storage, fee_code), ctxt)
    | FeeCode _ -> failwith "FeeCode can't be normalized"

  let denormalize_script ctxt code storage fee_code =
    initialize ctxt;
    match code, fee_code with
    | LiveContract _, None -> return ((code, storage), ctxt)
    | LiveContract c, Some (FeeCode fc) ->
        let code = Love_translator.denormalize_contract c fc in
        return ((LiveContract code, storage), ctxt)
    | Contract _, _ -> failwith "Contract can't be denormalized"
    | LiveContract _, _ -> failwith "LiveContract can't be denormalized"
    | FeeCode _, _ -> failwith "FeeCode can' be denormalized"

  let typecheck_code ctxt code =
    initialize ctxt;
    match code with
    | Contract code -> Love_interpreter.typecheck_code ctxt code
    | LiveContract _
    | FeeCode _ -> failwith "Wrong kind of code for typecheck code"

  let typecheck_data ctxt data typ =
    initialize ctxt;
    match data, typ with
    | Value v, Type t -> Love_interpreter.typecheck_data ctxt v t
    | Type _, _
    | _, Value _ -> failwith "Wrong kind of data for typecheck data"

  let typecheck ctxt step_constants ~code ~storage =
    initialize ctxt;
    let Script_interpreter.{ source; payer; self; amount; internal; _ } =
      step_constants in
    begin match code, storage with
      | Contract code, Value storage ->
          Love_interpreter.typecheck ctxt
            ~source ~payer ~self ~amount ~code ~storage
      | LiveContract code, Value storage ->
          if internal then
            Love_interpreter.preprocess_already_typechecked ctxt
              ~source ~payer ~self ~amount ~code ~storage
          else
            failwith "Wrong kind of code/storage for typecheck"
      | FeeCode _, _
      | _, Type _ -> failwith "Wrong kind of code/storage for typecheck"
    end
    >>=? fun (ctxt, code, storage) ->
    Love_interpreter.storage_big_map_diff ctxt storage
    >>=? fun (ctxt, storage, big_map_diff) ->
    normalize_script ctxt self (LiveContract code) (Value storage)
    >>=? fun ((code, storage, fee_code), ctxt) ->
    let big_map_diff = Love_translator.translate_big_map_diff big_map_diff in
    return ((code, storage, fee_code), big_map_diff, ctxt)

  let execute ctxt _mode step_constants
      ~code ~storage ~entrypoint ~parameter ~apply:_ =
    initialize ctxt;
    let code, storage, parameter =
      match code, storage, parameter with
      | LiveContract c, Value s, Value p -> c, s, p
      | _ -> failwith "Wrong kind of code or storage for execute"
    in
    let Script_interpreter.{ source; payer; self; amount; _ } =
      step_constants in
    Love_interpreter.execute ctxt ~source ~payer ~self
      ~code ~storage ~entrypoint ~parameter ~amount
    >>=? fun (ctxt, (result, operations, big_map_diff, storage)) ->
    let operations = List.map Love_translator.translate_op operations in
    let big_map_diff = Love_translator.translate_big_map_diff big_map_diff in
    return { ctxt; storage = Value storage; result = Some (Value result);
             big_map_diff; operations }

  let execute_fee_script ctxt step_constants
      ~fee_code ~storage ~entrypoint ~parameter =
    initialize ctxt;
    let fee_code, storage, parameter =
      match fee_code, storage, parameter with
      | FeeCode fc, Value s, Value p -> fc, s, p
      | _ -> failwith "Wrong kind of code or storage for execute_fee_script"
    in
    let Script_interpreter.{ source; payer; self; amount; _ } =
      step_constants in
    Love_interpreter.execute_fee_script ctxt ~source ~payer ~self
      ~fee_code ~storage ~entrypoint ~parameter ~amount
    >>=? fun (ctxt, (max_fee, max_storage)) ->
    return { ctxt; max_fee; max_storage }

  let execute_value ctxt ~self ~code ~storage ~val_name ~parameter =
    initialize ctxt;
    let code, storage, parameter =
      match code, storage, parameter with
      | LiveContract c, Value s, Some (Value v) -> c, s, Some v
      | LiveContract c, Value s, None -> c, s, None
      | _ -> failwith "Wrong kind of code or storage for execute_value"
    in
    Love_interpreter.execute_value ctxt ~source:self ~payer:self ~self
      ~code ~storage ~val_name ~parameter
    >>=? fun (ctxt, result, _typ) ->
    return { ctxt; result = Value result }

  let get_entrypoint ctxt ~code ~entrypoint =
    match code with
    | LiveContract code ->
        begin
          Love_interpreter.get_entrypoint ctxt ~code ~entrypoint
          >>=? function
          | (ctxt, Some typ) -> return (ctxt, Type typ)
          | (_, None) -> failwith "Wrong kind of code for get_entrypoint"
        end
    | _ -> failwith "Wrong kind of code for get_entrypoint"

  let list_entrypoints ctxt ~code =
    match code with
    | LiveContract code ->
        Love_interpreter.list_entrypoints ctxt ~code
        >>=? fun (c, l) ->
        return (c, (List.map (fun (n, t) -> n, Type t) l))
    | _ -> failwith "Wrong kind of code for get_entrypoint"

  let pack_data ctxt ~typ ~data =
    begin
      let ctxt = Love_context.init ctxt in
      match typ, data with
      | Some (Type typ), Value data ->
          let external_deps = Love_type.external_dependencies typ in
          Love_pervasives.Collections.StringSet.fold (fun kt1 res ->
              res >>=? fun ctxt ->
              Love_typechecker.extract_deps None ctxt [kt1, kt1]
            ) external_deps (return ctxt)
          >>=? fun ctxt ->
          Love_typechecker.typecheck_value ctxt
            { lettypes = []; body = typ } data
          >>=? fun ctxt ->
          Love_prim_interp.pack_data ctxt data
      | None, Value data ->
          Love_prim_interp.pack_data ctxt data
      | Some (Value _), _ ->
          failwith "Provided type for pack_data is a value"
      | _, Type _ ->
          failwith "Provided value for pack_data is a type:\
                    only values can be packed"
    end
    >>=? fun (ctxt, v) ->
    return (ctxt.actxt, v)

  let unpack_data ctxt ~typ ~data =
    match typ with
    | Type typ ->
        let ctxt = Love_context.init ctxt in
        let external_deps = Love_type.external_dependencies typ in
        Love_pervasives.Collections.StringSet.fold (fun kt1 res ->
            res >>=? fun ctxt ->
            Love_typechecker.extract_deps None ctxt [kt1, kt1]
          ) external_deps (return ctxt)
        >>=? fun ctxt ->
        Love_prim_interp.unpack_data ctxt typ data
        >>=? fun (ctxt, v) ->
        return (ctxt.actxt, Value v)
    | Value _ -> failwith "Provided type for unpack_data is a value"

  let hash_data ctxt k =
    match k with
    | Value k ->
        begin
          match Data_encoding.Binary.to_bytes
                  Love_encoding.Value.encoding k with
          | Some res -> return (ctxt, Script_expr_hash.hash_bytes [res])
          | None -> failwith "Cannot hash value"
        end
    | Type _ -> failwith "Wrong kind of value for hash_data"

  let to_michelson_const ctxt m :
    (Alpha_context.t * Script_repr.expr) tzresult Lwt.t =
    Lwt.return @@ Love_michelson.to_michelson_const ctxt m
    >>=? fun res -> return (ctxt, res)

  let from_michelson_const ctxt m :
    (Alpha_context.t * Repr.const) tzresult Lwt.t =
    Lwt.return @@ Love_michelson.from_michelson_const ctxt m

end

include M

let () =
  if false then
    let module TEST = (M : Dune_script_interpreter_sig.S) in
    ()
