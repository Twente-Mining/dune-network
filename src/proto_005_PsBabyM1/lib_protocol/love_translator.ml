(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_pervasives.Exceptions
open Love_pervasives.Collections
open Love_ast_types

let translate_opt trans env = function
    None -> None, env
  | Some elt ->
      let content, env = trans env elt in
      Some content, env

let translate_exn
    (_env : Love_tenv.t) (e : AST.exn_name) :
  Love_runtime_ast.exn_name
  =
  match e with
  | Fail t -> RFail t
  | Exception id -> RException id

let translate_const
    (_env : Love_tenv.t) (c : AST.const) :
  Love_runtime_ast.const
  =
  match c.content with
  | CUnit -> RCUnit
  | CBool b -> RCBool b
  | CString s -> RCString s
  | CBytes b -> RCBytes b
  | CInt i -> RCInt i
  | CNat n -> RCNat n
  | CDun d -> RCDun (Tez_repr.of_mutez_exn d)
  | CKey k -> RCKey k
  | CKeyHash kh -> RCKeyHash kh
  | CSignature s -> RCSignature s
  | CTimestamp t -> RCTimestamp t
  | CAddress a ->
      match Contract_repr.of_b58check a with
      | Ok a -> RCAddress a
      | _ ->
          raise (InvariantBroken
                  ("Invalid contract value \"" ^ a ^ "\""))

let rec translate_pattern
    (env : Love_tenv.t) (p : AST.pattern) :
  Love_runtime_ast.pattern * Love_tenv.t
  =
  match p.content with
  | PAny -> RPAny, env
  | PVar v -> RPVar v, env
  | PAlias (p, v) ->
      let p, env = translate_pattern env p in
      RPAlias (p, v), env
  | PConst c -> RPConst (translate_const env c), env
  | PList pl ->
      let pl, env = List.fold_left (fun (pl, env) p ->
          let p, env = translate_pattern env p in
          p :: pl, env
        ) ([], env) pl
      in
      RPList (List.rev pl), env
  | PTuple pl ->
      let pl, env = List.fold_left (fun (pl, env) p ->
          let p, env = translate_pattern env p in
          p :: pl, env
        ) ([], env) pl
      in
      RPTuple (List.rev pl), env
  | PConstr (c, pl) ->
      let pl, env = List.fold_left (fun (pl, env) p ->
          let p, env = translate_pattern env p in
          p :: pl, env
        ) ([], env) pl
      in
      RPConstr (c, (List.rev pl)), env
  | PContract (n, Anonymous s) ->
      let new_env = Love_tenv.contract_sig_to_env (Some n) s env in
      RPContract (n, Anonymous s),
      Love_tenv.add_subcontract_env n new_env env
  | PContract (n, Named id) ->
      RPContract (n, Named id),
      Love_tenv.add_signed_subcontract n id env
  | POr pl ->
      let pl, env = List.fold_left (fun (pl, env) p ->
          let p, env = translate_pattern env p in
          p :: pl, env
        ) ([], env) pl
      in
      RPOr (List.rev pl), env
  | PRecord l ->
      let l, env = translate_record_list env l in RPRecord l, env

and translate_record_list env rl =
  let rfields, env =
     List.fold_left
       (fun (fields, env) (s, p) ->
          let p, env = translate_opt translate_pattern env p in
          (s, p) :: fields, env)
       ([], env)
       rl.fields in
  {rl with fields = List.rev rfields}, env

let rec translate_raw_exp
    (env : Love_tenv.t) (e : AST.raw_exp) :
  Love_runtime_ast.exp
  =
  match e with
  | Const c -> RConst (translate_const env c)
  | Var v -> RVar v
  | VarWithArg (v, xa) -> RVarWithArg (v, xa)
  | Let { bnd_pattern = p; bnd_val = e1; body = e2 } ->
      let e1 = translate_exp env e1 in
      let p, env = translate_pattern env p in
      let e2 = translate_exp env e2 in
      RLet { bnd_pattern = p; bnd_val = e1; body = e2 }
  | LetRec { bnd_var; bnd_val = e1; body = e2; fun_typ } ->
      let e1 = translate_exp env e1 in
      let e2 = translate_exp env e2 in
      RLetRec { bnd_var; bnd_val = e1; val_typ = fun_typ; body = e2 }
  | Lambda _ as l -> translate_lambda env l (* could add free vars *)
  | TLambda _ as l -> translate_lambda env l
  | Apply _ as a -> translate_apply env a
  | TApply _ as a -> translate_apply env a
  | Seq el -> RSeq (List.map (translate_exp env) el)
  | If { cond = e1; ifthen = e2; ifelse = e3 } ->
      let e1 = translate_exp env e1 in
      let e2 = translate_exp env e2 in
      let e3 = translate_exp env e3 in
      RIf { cond = e1; ifthen = e2; ifelse = e3 }
  | Match { arg = e; cases = pel } ->
      let e = translate_exp env e in
      let pel = List.map (fun (p, e) ->
          let p, env = translate_pattern env p in
          p, translate_exp env e) pel in
      RMatch { arg = e; cases = pel }
  | Constructor { constr; ctyps; args = el } ->
      begin match Love_tenv.find_constr constr env with
        | Some { result = { cparent = TUser (tname, _); cname; _ }; _ } ->
            let el = List.map (translate_exp env) el in
            let type_name = Ident.change_last constr
                (Ident.create_id (Ident.get_final tname)) in
            let constr = cname in
            RConstructor { type_name; constr; ctyps; args = el }
        | _ -> raise (InvariantBroken "Constructor not found")
      end
  | Nil -> RNil
  | List (el, e) -> RList (List.map (translate_exp env) el, translate_exp env e)
  | Tuple el -> RTuple (List.map (translate_exp env) el)
  | Project { tuple = e; indexes } ->
      RProject { tuple = translate_exp env e; indexes }
  | Update { tuple = e; updates = iel } ->
      let e = translate_exp env e in
      let iel = List.map (fun (i, e) -> i, translate_exp env e) iel in
      RUpdate { tuple = e; updates = iel }
  | Record { path; contents = fel } ->
      let res = match path with
        | None -> Love_tenv.find_field (fst (List.hd fel)) env
        | Some path -> Love_tenv.find_field_in path (fst (List.hd fel)) env
      in
      begin match res with
        | Some { result = { fparent = TUser (type_name, _); _ }; _ } ->
            let fel = List.map (fun (f, e) -> f, translate_exp env e) fel in
            RRecord { type_name; contents = fel }
        | _ -> raise (InvariantBroken "Field not found")
      end
  | GetField { record = e; field } ->
      RGetField { record = translate_exp env e; field }
  | SetFields { record = e; updates = fel } ->
      let e = translate_exp env e in
      let fel = List.map (fun (f, e) -> f, translate_exp env e) fel in
      RSetFields { record = e; updates = fel }
  | PackStruct r -> RPackStruct (translate_reference env r)
  | Raise { exn = en; args = el; loc } ->
      let en = translate_exn env en in
      let el = List.map (translate_exp env) el in
      RRaise { exn = en; args = el; loc }
  | TryWith { arg = e; cases = pel } ->
      let e = translate_exp env e in
      let pel = List.map (fun ((en, pl), e) ->
          let pl, env = List.fold_left (fun (pl, env) p ->
              let p, env = translate_pattern env p in
              p :: pl, env
            ) ([], env) pl
          in
          (translate_exn env en, List.rev pl), translate_exp env e) pel in
      RTryWith { arg = e; cases = pel }

and translate_exp
    (env : Love_tenv.t) (e : AST.exp) :
  Love_runtime_ast.exp
  =
  translate_raw_exp env e.content

and translate_lambda
    (env : Love_tenv.t) (l : AST.raw_exp) :
  Love_runtime_ast.exp
  =
  let open Love_ast_types.AST in
  let open Love_runtime_ast in
  let rec aux env args = function
    | Lambda { arg_pattern = p; arg_typ = t; body } ->
        let p, env = translate_pattern env p in
        let args = RPattern (p, t) :: args in
        aux env args body.content
    | TLambda { targ = tv; exp } ->
        let args = RTypeVar tv :: args in
        aux env args exp.content
    | e -> RLambda { args = List.rev args; body = translate_raw_exp env e }
  in
  aux env [] l

and translate_apply
    (env : Love_tenv.t) (l : AST.raw_exp) :
  Love_runtime_ast.exp
  =
  let open Love_ast_types.AST in
  let open Love_runtime_ast in
  let rec aux args = function
    | Apply { fct = e; args = el } ->
        let args = List.fold_left (fun args e ->
            (RExp (translate_exp env e)) :: args) args (List.rev el) in
        aux args e.content
    | TApply { exp = e; typ } ->
        aux ((RType typ) :: args) e.content
    | e -> RApply { exp = translate_raw_exp env e; args }
  in
  aux [] l

and translate_init
    (env : Love_tenv.t) (i : AST.init) :
  Love_runtime_ast.init
  =
  Love_runtime_ast.{
    init_code = translate_exp env i.init_code;
    init_typ = i.init_typ;
    init_persist = i.init_persist;
  }

and translate_entry
    (env : Love_tenv.t) (e : AST.entry) :
  Love_runtime_ast.entry
  =
  Love_runtime_ast.{
    entry_code = translate_exp env e.entry_code;
    entry_fee_code = Option.map ~f:(translate_exp env) e.entry_fee_code;
    entry_typ = e.entry_typ;
  }

and translate_view
    (env : Love_tenv.t) (v : AST.view) :
  Love_runtime_ast.view
  =
  Love_runtime_ast.{
    view_code = translate_exp env v.view_code;
    view_typ = v.view_typ;
    view_recursive = v.view_recursive
  }

and translate_value
    (env : Love_tenv.t) (v : AST.value) :
  Love_runtime_ast.value
  =
  Love_runtime_ast.{
    value_code = translate_exp env v.value_code;
    value_typ = v.value_typ;
    value_visibility = v.value_visibility;
    value_recursive = v.value_recursive;
  }

and translate_content
    (env : Love_tenv.t) (n : string) (c : AST.content) :
  Love_runtime_ast.content
  =
  match c with
  | DefType (k, td) -> RDefType (k, td)
  | DefException tl -> RDefException tl
  | Init i -> RInit (translate_init env i)
  | Entry e -> REntry (translate_entry env e)
  | View v -> RView (translate_view env v)
  | Value v -> RValue (translate_value env v)
  | Structure s ->
      begin match Love_tenv.get_subenv_struct n env with
        | Some env -> RStructure (translate_structure env s)
        | None -> raise (InvariantBroken ("Structure " ^ n ^ " not found"))
      end
  | Signature s -> RSignature s

and translate_structure
    (env : Love_tenv.t) (s : AST.structure) :
  Love_runtime_ast.structure
  =
  Love_runtime_ast.{
    structure_content =
      List.map (fun (n, c) ->
          n, translate_content env n c) s.structure_content;
    kind = s.kind
  }

and translate_reference
    (_env : Love_tenv.t) (r : AST.reference) :
  Love_runtime_ast.reference
  =
  match r with
  | Anonymous s ->
      let deps_env = Love_tenv.empty s.kind () in
      let root_sig = Love_ast_utils.sig_of_structure s in
      let env = Love_tenv.contract_sig_to_env None root_sig deps_env in
      RAnonymous (translate_structure env s)
  | Named id -> RNamed id

let translate_to_runtime
    (env : Love_tenv.t) (c : AST.top_contract) :
  Love_runtime_ast.top_contract
  =
  Love_runtime_ast.{
    version = c.version;
    code = translate_structure env c.code
  }

let translate_structure
    (env : Love_tenv.t) (s : AST.structure) :
  Love_runtime_ast.structure
  =
  Love_tenv.set_environment_mode true;
  let s = try translate_structure env s
    with e -> Love_tenv.set_environment_mode false; raise e in
  Love_tenv.set_environment_mode false;
  s

let translate_to_runtime
    (env : Love_tenv.t) (c : AST.top_contract) :
  Love_runtime_ast.top_contract
  =
  Love_tenv.set_environment_mode true;
  let c = try translate_to_runtime env c
    with e -> Love_tenv.set_environment_mode false; raise e in
  Love_tenv.set_environment_mode false;
  c

let inline_value
    (ctxt : Love_context.t) (v : Love_value.Value.t) :
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t
  =
  let open Love_value in

  let map f ctxt l =
    fold_left_s (fun (ctxt, l) x ->
        f ctxt x >>|? fun (ctxt, x) -> (ctxt, x :: l)
      ) (ctxt, []) l
    >>|? fun (ctxt, l) -> (ctxt, List.rev l)
  in

  let rec map_value (ctxt : Love_context.t) (v : Value.t) :
    (Love_context.t * Value.t) tzresult Lwt.t =
    let open Love_value.Value in
    begin match Value.unrec_closure v with
    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VDun _ | VKey _ | VKeyHash _ | VSignature _ | VTimestamp _
    | VAddress _ | VContractInstance (_, _)
    | VPackedStructure _ as v ->
        return (ctxt, v) (* is already self-contained *)
    | VEntryPoint (_, _) | VView (_, _) | VOperation _ ->
        raise (InvariantBroken "Forbidden value in contract")
    | VTuple vl ->
        map map_value ctxt vl >>|? fun (ctxt, vl) ->
        ctxt, VTuple vl
    | VConstr (cstr, vl) ->
        map map_value ctxt vl >>|? fun (ctxt, vl) ->
        ctxt, VConstr (cstr, vl)
    | VRecord fvl ->
        let map_field ctxt (f, v) =
          map_value ctxt v >>|? fun (ctxt, v) ->
          (ctxt, (f, v))
        in
        map map_field ctxt fvl >>|? fun (ctxt, fvl) ->
        ctxt, VRecord fvl
    | VList vl ->
        map map_value ctxt vl >>|? fun (ctxt, vl) ->
        ctxt, VList vl
    | VSet vs ->
        map map_value ctxt (ValueSet.elements vs) >>|? fun (ctxt, vl) ->
        ctxt, VSet (ValueSet.of_list vl)
    | VMap vm ->
        let map_binding ctxt (k, v) =
          map_value ctxt k >>=? fun (ctxt, k) ->
          map_value ctxt v >>|? fun (ctxt, v) ->
          (ctxt, (k, v))
        in
        map map_binding ctxt (ValueMap.bindings vm) >>|? fun (ctxt, bl) ->
        ctxt, VMap (Utils.bindings_to_map ValueMap.add ValueMap.empty bl)
    | VBigMap ({ diff = vm; _ } as bm) ->
        let map_binding ctxt (k, v) =
          map_value ctxt k >>=? fun (ctxt, k) ->
          match v with
          | None -> return (ctxt, (k, v))
          | Some v ->
              map_value ctxt v >>|? fun (ctxt, v) ->
              (ctxt, (k, Some v))
        in
        map map_binding ctxt (ValueMap.bindings vm) >>|? fun (ctxt, bl) ->
        ctxt, VBigMap { bm with diff = Utils.bindings_to_map
                                    ValueMap.add ValueMap.empty bl }
    | VPrimitive (p, xl, vtl) ->
        map map_val_or_type ctxt vtl >>|? fun (ctxt, vtl) ->
        ctxt, VPrimitive (p, xl, vtl)
    | VClosure { call_env; lambda } ->
        let map_loc_glob ctxt (id, p) =
          match p with
          | Local v ->
              map_value ctxt v >>|? fun (ctxt, v) ->
              ctxt, (id, Local v)
          | Global (Inlined (p, v)) ->
              map_value ctxt v >>|? fun (ctxt, v) ->
              ctxt, (id, Global (Inlined (p, v)))
          | Global (Pointer p) ->
              (* should deep inline if it belongs to the current contract *)
              Love_context.get_value ctxt p >>=? fun (ctxt, v) ->
              map_value ctxt v >>|? fun (ctxt, v) ->
              ctxt, (id, Global (Inlined (p, v)))
        in
        let map_ptr_inl_str ctxt (id, p) =
          match p with
          | Inlined (p, s) ->
              map_struct ctxt s >>|? fun (ctxt, s) ->
              ctxt, (id, Inlined (p, s))
          | Pointer p ->
              (* should deep inline if it belongs to the current contract *)
              Love_context.get_struct ctxt p >>=? fun (ctxt, s) ->
              map_struct ctxt s >>|? fun (ctxt, s) ->
              ctxt, (id, Inlined (p, s))
        in
        let map_ptr_inl_sig ctxt (id, p) =
          match p with
          | Inlined _ -> return (ctxt, (id, p))
          | Pointer p ->
              Love_context.get_sig ctxt p >>|? fun (ctxt, s) ->
              ctxt, (id, Inlined (p, s))
        in
        map map_loc_glob ctxt call_env.values >>=? fun (ctxt, values) ->
        map map_ptr_inl_str ctxt call_env.structs >>=? fun (ctxt, structs) ->
        map map_ptr_inl_sig ctxt call_env.sigs >>|? fun (ctxt, sigs) ->
        let call_env = { call_env with values; structs; sigs } in
        ctxt, VClosure { call_env; lambda }
    end
    >>|? fun (ctxt, v) -> ctxt, Value.rec_closure v

  and map_val_or_type (ctxt : Love_context.t) (vt : Value.val_or_type) :
    (Love_context.t * Value.val_or_type) tzresult Lwt.t =
    match vt with
    | V v -> map_value ctxt v >>|? fun (ctxt, v) -> ctxt, Value.V v
    | T _ -> return (ctxt, vt)

  and map_struct (ctxt : Love_context.t) (s : LiveStructure.t) :
    (Love_context.t * LiveStructure.t) tzresult Lwt.t =
    let open Love_value.LiveStructure in
    let map_content ctxt (n, c) =
      match c with
      | VType _
      | VException _
      | VSignature _ ->
          return (ctxt, (n, c))
      | VInit i ->
          map_value ctxt i.vinit_code >>|? fun (ctxt, vc) ->
          ctxt, (n, VInit { i with vinit_code = vc })
      | VEntry e ->
          map_value ctxt e.ventry_code >>|? fun (ctxt, vc) ->
          ctxt, (n, VEntry { e with ventry_code = vc })
      | VView v ->
          map_value ctxt v.vview_code >>|? fun (ctxt, vc) ->
          ctxt, (n, VView { v with vview_code = vc })
      | VValue v ->
          map_value ctxt v.vvalue_code >>|? fun (ctxt, vc) ->
          ctxt, (n, VValue { v with vvalue_code = vc })
      | VStructure s ->
          map_struct ctxt s >>|? fun (ctxt, s) ->
          ctxt, (n, VStructure s)
    in
    map map_content ctxt s.content >>|? fun (ctxt, content) ->
    ctxt, { s with content }
  in

  map_value ctxt v

let make_type_absolute
    (ctxt : Love_context.t) (env : Love_env.t) (t : TYPE.t) :
  (Love_context.t * TYPE.t) tzresult Lwt.t
  =
  let open TYPE in

  let rec aux (ctxt, ids) t =
    match t with
    | TVar _
    | TPackedStructure (Anonymous _)
    | TContractInstance (Anonymous _) ->
        return (ctxt, ids)
    | TForall (_, t) ->
        aux (ctxt, ids) t
    | TArrow (t1, t2) ->
        aux (ctxt, ids) t1 >>=? fun acc -> aux acc t2
    | TTuple tl ->
        fold_left_s aux (ctxt, ids) tl
    | TUser (id, tl) ->
        Love_env.find_type_name_opt ctxt env id >>=?
        begin function
          | _ctxt, None -> raise (InvariantBroken (
              Format.asprintf "Type %a not found" Ident.print_strident id))
          | ctxt, Some path ->
              fold_left_s aux (ctxt, (StringIdentMap.add id path ids)) tl
        end
    | TPackedStructure (Named id)
    | TContractInstance (Named id) ->
        Love_env.find_raw_sig_opt ctxt env id >>|?
        begin function
          | ctxt, Some (Pointer path | Inlined (path, _)) ->
              ctxt, StringIdentMap.add id path ids
          | _ -> raise (InvariantBroken "Signature not found")
        end
  in

  aux (ctxt, StringIdentMap.empty) t >>|? fun (ctxt, ids) ->

  let rec aux t =
    match t with
    | TVar _
    | TPackedStructure (Anonymous _)
    | TContractInstance (Anonymous _) -> t
    | TForall (tv, t) -> TForall (tv, aux t)
    | TArrow (t1, t2) -> TArrow (aux t1, aux t2)
    | TTuple tl -> TTuple (List.map aux tl)
    | TUser (id, tl) ->
        begin match StringIdentMap.find_opt id ids with
          | None -> raise (InvariantBroken "Type not found")
          | Some id -> TUser (id, List.map aux tl)
        end
    | TPackedStructure (Named id) ->
        begin match StringIdentMap.find_opt id ids with
          | None -> raise (InvariantBroken "Signature not found")
          | Some id -> TPackedStructure (Named id)
        end
    | TContractInstance (Named id) ->
        begin match StringIdentMap.find_opt id ids with
          | None -> raise (InvariantBroken "Signature not found")
          | Some id -> TPackedStructure (Named id)
        end
  in

  ctxt, aux t

let make_types_absolute_in_value
    (ctxt : Love_context.t) (env : Love_env.t) (v : Love_value.Value.t) :
  (Love_context.t * Love_value.Value.t) tzresult Lwt.t
  =
  let open Love_value in

  let add_absolute_type ctxt t types =
    if List.exists (fun (rt, _) -> rt == t) types then
      return (ctxt, types)
    else
      make_type_absolute ctxt env t >>|? fun (ctxt, at) ->
      ctxt, (t, at) :: types
  in

  let rec aux (ctxt, types) v =
    match Value.unrec_closure v with
    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VDun _ | VKey _ | VKeyHash _ | VSignature _ | VTimestamp _
    | VAddress _ | VOperation _
    | VContractInstance _ | VPackedStructure _
    | VEntryPoint (_, _) | VView (_, _) ->
        return (ctxt, types)
    | VList vl | VTuple vl | VConstr (_, vl) ->
        fold_left_s aux (ctxt, types) vl
    | VPrimitive (_, _, vtl) ->
        let open Love_value.Value in
        fold_left_s (fun (ctxt, types) vt ->
            match vt with
            | V v -> aux (ctxt, types) v
            | T t -> add_absolute_type ctxt t types
          ) (ctxt, types) vtl
    | VRecord fvl ->
        fold_left_s (fun acc (_f, v) -> aux acc v) (ctxt, types) fvl
    | VSet vs ->
        fold_left_s aux (ctxt, types) (ValueSet.elements vs)
    | VMap vm ->
        fold_left_s (fun acc (k, v) ->
            aux acc k >>=? fun acc -> aux acc v
          ) (ctxt, types) (ValueMap.bindings vm)
    | VBigMap { diff = vm; key_type; value_type; _ } ->
        fold_left_s (fun acc (k, v) ->
            aux acc k >>=? fun acc ->
            match v with
            | Some v -> aux acc v
            | None -> return acc
          ) (ctxt, types) (ValueMap.bindings vm) >>=? fun (ctxt, types) ->
        add_absolute_type ctxt key_type types >>=? fun (ctxt, types) ->
        add_absolute_type ctxt value_type types
    | VClosure { call_env = { values; _ }; _ } ->
        let open Love_value.Value in
        fold_left_s (fun acc (_id, vc) ->
            match vc with
            | Local v' -> aux acc v'
            | Global (Inlined (_p, v)) -> aux acc v
            | Global (Pointer _p) -> return acc
          ) (ctxt, types) values
  in

  aux (ctxt, []) v >>|? fun (ctxt, types) ->

  let subst_absolute_type t types =
    match List.find_opt (fun (rt, _) -> rt == t) types with
    | Some (_, at) -> at
    | None -> t
  in

  let rec aux v =
    Value.rec_closure @@
    match Value.unrec_closure v with
    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VDun _ | VKey _ | VKeyHash _ | VSignature _ | VTimestamp _
    | VAddress _ | VOperation _
    | VContractInstance _ | VPackedStructure _
    | VEntryPoint (_, _) | VView (_, _) -> v
    | VList vl -> VList (List.map aux vl)
    | VTuple vl -> VTuple (List.map aux vl)
    | VConstr (cstr, vl) -> VConstr (cstr, List.map aux vl)
    | VPrimitive (p, xl, vtl) ->
        let open Love_value.Value in
        let vtl = List.map (fun vt ->
            match vt with
            | V v -> V (aux v)
            | T _ -> vt
          ) vtl in
        VPrimitive (p, xl, vtl)
    | VRecord fvl -> VRecord (List.map (fun (f, v) -> f, aux v) fvl)
    | VSet vs -> VSet (ValueSet.map aux vs)
    | VMap vm ->
        VMap (ValueMap.fold (fun k v vm ->
            ValueMap.add (aux k) (aux v) vm) ValueMap.empty vm)
    | VBigMap ({ diff = vm; key_type; value_type; _ } as bm) ->
        let vm = ValueMap.fold (fun k v vm ->
            let v = match v with
              | None -> None
              | Some v -> Some (aux v)
            in
            ValueMap.add (aux k) v vm
          ) ValueMap.empty vm
        in
        let key_type = subst_absolute_type key_type types in
        let value_type = subst_absolute_type value_type types in
        VBigMap { bm with diff = vm; key_type; value_type }
    | VClosure { call_env = { values; _ } as call_env; lambda } ->
        let open Love_value.Value in
        let values = List.map (fun (id, p) ->
            id, match p with
            | Local v -> Local (aux v)
            | Global (Inlined (p, v)) -> Global (Inlined (p, (aux v)))
            | Global (Pointer _p) -> p
          ) values in
        let call_env = { call_env with values } in
        VClosure { call_env; lambda }
  in

  ctxt, aux v

let rebase_contract (_ctxt : Love_context.t)
    ~(from_path : string Ident.t) ~(to_path : string Ident.t)
    ({ root_struct; _ } as c : Love_value.LiveContract.t) :
  Love_value.LiveContract.t
  =
  let open Love_value in

  let rebase_path path =
    let rec aux p f =
      match p, f with
      | Some p, Some f ->
          begin match Ident.split p, Ident.split f with
            | (n1, p), (n2, f) when String.equal n1 n2 -> aux p f
            | _ -> None
          end
      | Some p, None -> Some (Ident.concat to_path p)
      | None, None -> Some to_path
      | None, Some _ -> None
    in
    aux (Some path) (Some from_path)
  in

  let rec map_value v =
    Value.rec_closure @@
    match Value.unrec_closure v with
    | VUnit | VBool _ | VString _ | VBytes _ | VInt _ | VNat _
    | VDun _ | VKey _ | VKeyHash _ | VSignature _ | VTimestamp _
    | VAddress _ | VContractInstance (_, _)
    | VPackedStructure _ as v -> v (* is already self-contained *)
    | VEntryPoint (_, _) | VView (_, _) | VOperation _ | VBigMap _ ->
        raise (InvariantBroken "Forbidden value in contract")
    | VTuple vl -> VTuple (List.map map_value vl)
    | VConstr (cstr, vl) -> VConstr (cstr, List.map map_value vl)
    | VRecord fvl -> VRecord (List.map (fun (f, v) -> f, map_value v) fvl)
    | VList vl -> VList (List.map map_value vl)
    | VSet vs -> VSet (ValueSet.map map_value vs)
    | VMap vm ->
        VMap (ValueMap.fold (fun k v vm ->
            ValueMap.add (map_value k) (map_value v) vm) ValueMap.empty vm)
    | VPrimitive (p, xl, vtl) ->
        let open Love_value.Value in
        let vtl = List.map (fun vt ->
            match vt with
            | V v -> V (map_value v)
            | T _ -> vt
          ) vtl in
        VPrimitive (p, xl, vtl)
    | VClosure { call_env; lambda } ->
        let open Love_value.Value in
        let values = List.map (fun (id, p) ->
            id, match p with
            | Local v -> Local (map_value v)
            | Global (Inlined (p, v)) ->
                begin match rebase_path p with
                  | Some p -> Global (Inlined (p, v))
                  | None -> Global (Inlined (p, v))
                end
            | Global (Pointer p) ->
                begin match rebase_path p with
                  | Some p -> Global (Pointer p)
                  | None -> Global (Pointer p)
                end
          ) call_env.values in
        let structs = List.map (fun (id, p) ->
            id, match p with
            | Inlined (p, s) ->
                begin match rebase_path p with
                  | Some p -> Inlined (p, s)
                  | None -> Inlined (p, s)
                end
            | Pointer p ->
                begin match rebase_path p with
                  | Some p -> Pointer p
                  | None -> Pointer p
                end
          ) call_env.structs in
        let sigs = List.map (fun (id, p) ->
            id, match p with
            | Inlined (p, s) ->
                begin match rebase_path p with
                  | Some p -> Inlined (p, s)
                  | None -> Inlined (p, s)
                end
            | Pointer p ->
                begin match rebase_path p with
                  | Some p -> Pointer p
                  | None -> Pointer p
                end
          ) call_env.sigs in
        let exns = List.map (fun (ids, idd) ->
            match rebase_path idd with
            | Some idd -> ids, idd
            | None -> ids, idd
          ) call_env.exns in
        let types = List.map (fun (ids, idd) ->
            match rebase_path idd with
            | Some idd -> ids, idd
            | None -> ids, idd
          ) call_env.types in
        (* tvars don't need to be rebased, they are always relative *)
        let call_env = { call_env with values; structs; sigs; exns; types } in
        VClosure { call_env; lambda }

  and map_struct s =
    let content = List.map (fun (n, c) ->
        let open Love_value.LiveStructure in
        n, match c with
        | VType _ | VException _ | VSignature _ -> c
        | VInit i ->
            VInit { i with vinit_code = map_value i.vinit_code }
        | VEntry e ->
            VEntry { e with ventry_code = map_value e.ventry_code;
                            ventry_fee_code =
                              match e.ventry_fee_code with
                              | None -> None
                              | Some fc -> Some (map_value fc) }
        | VView v ->
            VView { v with vview_code = map_value v.vview_code }
        | VValue v ->
            VValue { v with vvalue_code = map_value v.vvalue_code }
        | VStructure s -> VStructure (map_struct s)
      ) s.content
    in
    { s with content }
  in

  { c with root_struct = map_struct root_struct }

let normalize_contract (self : Contract_repr.t)
    Love_value.LiveContract.{ version; root_struct } :
  Love_value.LiveContract.t * Love_value.FeeCode.t
  =
  let open Love_value in

  let resolve_id env id =
    let open Love_value.LiveStructure in
    let res, idl = match Ident.get_list id with
      | [] -> raise (InvariantBroken "Empty ID")
      | n :: idl ->
          match StringMap.find_opt n env with
          | None -> None, []
          | Some (c, full_id) ->
              Some (c, List.rev (Ident.get_list full_id)), idl
    in
    let res = List.fold_left (fun res n ->
        match res with
        | None -> res
        | Some (VStructure s, path) ->
            begin match List.assoc_opt n s.content with
              | Some c -> Some (c, n :: path)
              | None -> None
            end
        | _ -> raise (InvariantBroken "Trying to resolve an ID in something\
                                       that is not a structure")
      ) res idl
    in
    match res with
    | None -> None
    | Some (c, n :: idl) ->
        Some (c, Ident.put_in_namespaces idl (Ident.create_id n))
    | Some (_, []) -> raise (InvariantBroken "Found an ID without a full path")
  in

  let type_deps env ty =
    let open TYPE in
    let rec aux ids = function
      | TVar _ -> ids
      | TForall (_, t) -> aux ids t
      | TArrow (t1, t2) -> aux (aux ids t1) t2
      | TTuple tl -> List.fold_left aux ids tl
      | TPackedStructure (Anonymous _s)
      | TContractInstance (Anonymous _s) -> ids (* self-contained: no deps *)
      | TPackedStructure (Named id)
      | TContractInstance (Named id) ->
          begin match resolve_id env id with
            | Some (_, full_id) -> StringIdentSet.add full_id ids
            | None ->
                if has_protocol_revision 5 then
                  (* Not found = defined in an external dependency *)
                  ids
                else
                  raise (InvariantBroken "Identifier not found")
          end
      | TUser (id, tl) ->
          let ids = List.fold_left aux ids tl in
          if Love_type_list.is_core_type id then ids
          else
            begin match resolve_id env id with
              | Some (_, full_id) -> StringIdentSet.add full_id ids
              | None ->
                  if has_protocol_revision 5 then
                    (* Not found = defined in an external dependency *)
                    ids
                  else
                    raise (InvariantBroken "Identifier not found")
            end
    in
    aux StringIdentSet.empty ty
  in

  let typedef_deps env td =
    let open TYPE in
    match td with
    | Alias { aparams = _; atype } ->
        type_deps env atype
    | SumType { sparams = _; scons; srec = _ } ->
        List.fold_left (fun ids (_, tl) ->
            List.fold_left (fun ids t ->
                StringIdentSet.union ids (type_deps env t)
              ) ids tl
          ) StringIdentSet.empty scons
    | RecordType { rparams = _; rfields; rrec = _ } ->
        List.fold_left (fun ids (_, t) ->
            StringIdentSet.union ids (type_deps env t)
          ) StringIdentSet.empty rfields
  in

  let closure_deps value =
    let open Love_value.Value in
    let self = Contract_repr.to_b58check self in
    let process deps = function
      | Pointer (LDot (kt1, id)) when String.equal kt1 self ->
          StringIdentSet.add id deps
      | Pointer _
      | Inlined _ -> deps
    in
    let process_n deps (_, vc) = process deps vc in
    match value with
    | VClosure { call_env = { values; structs; sigs; _ }; lambda = _ } ->
        let deps = List.fold_left (fun deps (_, lg_vc) ->
            match lg_vc with
            | Global vc -> process deps vc
            | Local _ -> deps
          ) StringIdentSet.empty values
        in
        let deps = List.fold_left process_n deps structs in (* for orig *)
        let deps = List.fold_left process_n deps sigs in (* for at/self *)
        deps
    | _ -> StringIdentSet.empty
  in

  let rec struct_deps path env deps (s : LiveStructure.t) =
    let open Love_value.LiveStructure in
    List.fold_left (fun (env, deps) (n, c) ->
        let full_id = Ident.put_in_namespaces path (Ident.create_id n) in
        match c with
        | VType (_k, td) ->
            let new_env = StringMap.add n (c, full_id) env in
            let env =
              if Love_type.is_recursive_typedef td then new_env else env in
            let td_deps = typedef_deps env td in
            let env =
              if Love_type.is_recursive_typedef td then env else new_env in
            let deps = StringIdentMap.add full_id td_deps deps in
            (env, deps)
        | VStructure s ->
            let _, deps = struct_deps (n :: path) env deps s in
            let env = StringMap.add n (c, full_id) env in
            (env, deps)
        | VSignature _s ->
            (* We are only interrested by contract signatures,
               which are self-contained (so no need to explore) *)
            let env = StringMap.add n (c, full_id) env in
            (env, deps)
        | VValue { vvalue_code = vc; vvalue_typ = _;
                   vvalue_visibility = _; vvalue_recursive = r }
        | VView { vview_code = vc; vview_typ = _; vview_recursive = r } ->
            let new_env = StringMap.add n (c, full_id) env in
            let env = if Love_type.is_recursive r then new_env else env in
            let fun_deps = closure_deps vc in
            let env = if Love_type.is_recursive r then env else new_env in
            let deps = StringIdentMap.add full_id fun_deps deps in
            (env, deps)
        | VException _
        | VInit _
        | VEntry _ -> (env, deps)
      ) (env, deps) s.content
  in

  let add_transitive_closure deps ids closure =
    let rec aux closure ids =
      if StringIdentSet.is_empty ids then closure
      else
        let new_ids = StringIdentSet.diff ids closure in
        let closure = StringIdentSet.union closure new_ids in
        let deps_ids = StringIdentSet.fold (fun id deps_ids ->
            match StringIdentMap.find_opt id deps with
            | None -> deps_ids
            | Some ids -> StringIdentSet.union deps_ids ids
          ) new_ids StringIdentSet.empty
        in
        aux closure deps_ids
    in
    aux closure ids
  in

  let split_fee_codes env deps (s : LiveStructure.t) =
    let open Love_value.LiveStructure in
    let content, fee_codes, closure =
      List.fold_left (fun (content, fee_codes, closure) (n, c) ->
          match c with
          | VEntry ({ ventry_fee_code = Some fc;
                      ventry_typ = ty; ventry_code = _ } as entry ) ->
              let ty_deps = type_deps env ty in
              let closure = add_transitive_closure deps ty_deps closure in
              let fun_deps = closure_deps fc in
              let closure = add_transitive_closure deps fun_deps closure in
              let entry = { entry with ventry_fee_code = None } in
              ((n, VEntry entry) :: content,
               (n, (fc, ty)) :: fee_codes, closure)
          | _ ->
              ((n, c) :: content, fee_codes, closure)
        ) ([], [], StringIdentSet.empty) s.content
    in
    List.rev content, List.rev fee_codes, closure
  in

  (* Closure contains full IDs of types/signatures to keep from contract *)
  let rec struct_closure closure path (s : LiveStructure.t) =
    let open Love_value.LiveStructure in
    List.rev (List.fold_left (fun content (n, c) ->
        let full_id = Ident.put_in_namespaces path (Ident.create_id n) in
        match c with
        | VStructure s ->
            begin match struct_closure closure (n :: path) s with
              | [] -> content
              | sc -> (n, VStructure { s with content = sc }) :: content
            end
        | VType _ | VSignature _ | VValue _ | VView _ ->
            if StringIdentSet.mem full_id closure
            then (n, c) :: content
            else content
        | VException _
        | VInit _
        | VEntry _ -> content
      ) [] s.content)
  in

  let env, deps =
    struct_deps [] StringMap.empty StringIdentMap.empty root_struct in

  let content, fee_codes, closure = split_fee_codes env deps root_struct in

  let root_struct = { root_struct with content } in
  let contract = Love_value.LiveContract.{ version; root_struct } in

  let fee_struct_content = struct_closure closure [] root_struct in
  let fee_struct = { root_struct with content = fee_struct_content } in

  let fee_code = Love_value.FeeCode.{ version; root_struct = fee_struct;
                                      fee_codes = fee_codes } in

  contract, fee_code

let normalize_contract (self : Contract_repr.t)
    (contract : Love_value.LiveContract.t) :
  Love_value.LiveContract.t * Love_value.FeeCode.t option
  =
  let has_fee_code (s : Love_value.LiveStructure.t) =
    let open Love_value.LiveStructure in
    if has_protocol_revision 5 then
      List.exists (fun (_n, c) ->
          match c with
          | VEntry { ventry_fee_code = Some _; _ } -> true
          | _ -> false
        ) s.content
    else
      true
  in

  if has_fee_code contract.root_struct then
    let contract, fee_code = normalize_contract self contract in
    contract, Some fee_code
  else
    contract, None


let denormalize_contract
    Love_value.LiveContract.{ version; root_struct }
    Love_value.FeeCode.{ version = _; root_struct = _; fee_codes } :
  Love_value.LiveContract.t
  =
  let open Love_value in
  let open Love_value.LiveStructure in
  let content = List.map (function
      | (n, VEntry e) as c ->
          begin match List.assoc_opt n fee_codes with
            | None -> c
            | Some (fc, _) -> n, VEntry { e with ventry_fee_code = Some fc }
          end
      | c -> c
    ) root_struct.content in
  LiveContract.{ version; root_struct = { root_struct with content } }

let rec sig_of_structure
    ~(only_typedefs : bool) Love_value.LiveStructure.{ content; kind } :
  TYPE.structure_sig
  =
  let open TYPE in
  let open Love_value.LiveStructure in
  let sig_content =
    List.fold_left (fun acc (n, content) ->
        match content with
        | VType (TPublic, td) -> (n, SType (SPublic td)) :: acc
        | VType (TPrivate, td) -> (n, SType (SPrivate td)) :: acc
        | VType (TAbstract, td) ->
            let params = match td with
              | Alias { aparams = p; _ }
              | SumType { sparams = p; _ }
              | RecordType { rparams = p; _ } -> p
            in
            (n, SType (SAbstract params)) :: acc
        | VType (TInternal, _td) -> acc
        | VException tl when not only_typedefs ->
            (n, SException tl) :: acc
        | VException _tl -> acc
        | VInit { vinit_typ = tparam; _ } when not only_typedefs ->
            (n, SInit tparam) :: acc
        | VInit _ -> acc
        | VEntry { ventry_typ = tparam;_ } when not only_typedefs ->
            (n, SEntry tparam) :: acc
        | VEntry _ -> acc
        | VView { vview_typ = tfun;_ }  when not only_typedefs ->
            (n, SView tfun) :: acc
        | VView _ -> acc
        | VValue { vvalue_typ; vvalue_visibility = Public; _ }
          when not only_typedefs ->
            (n, SValue vvalue_typ) :: acc
        | VValue { vvalue_visibility = Public; _ }
        | VValue { vvalue_visibility = Private; _ } -> acc
        | VStructure s ->
            (n, SStructure (Anonymous (sig_of_structure
                                         ~only_typedefs s))) :: acc
        | VSignature s ->
            (n, SSignature s) :: acc
      )
      []
      content
  in
  TYPE.{ sig_content = List.rev sig_content; sig_kind = kind }

let translate_op
    Love_value.Op.{ source; operation; nonce } :
  Alpha_context.packed_internal_operation
  =
  let open Alpha_context in
  let open Alpha_context.Script in
  let open Love_repr in
  match operation with
  | Origination {
      delegate; script = storage, code; credit; preorigination } ->
      let script =
        let storage = lazy_expr (Dune_expr (Const (Value storage))) in
        let code = lazy_expr (Dune_code (Code (LiveContract code))) in
        Script { code; storage }
      in
      let operation =
        Origination { delegate; script; credit; preorigination } in
      Internal_operation { source; operation; nonce }
  | Transaction { amount; parameters; entrypoint; destination } ->
      let parameters = match parameters with
        | None -> None
        | Some p -> Some (lazy_expr (Dune_expr (Const (Value p))))
      in
      let operation = Transaction { amount; parameters; entrypoint;
                                    destination; collect_call = None } in
      Internal_operation { source; operation; nonce }
  | Delegation delegate ->
      let operation = Delegation delegate in
      Internal_operation { source; operation; nonce }
  | Dune_manage_account
      { target; maxrolls; admin; white_list; delegation } ->
      let open Dune_operation_repr in
      let options = { maxrolls; admin; white_list; delegation ;
                      recovery = None ; actions = [] } in
      let options = Options.encode options in
      let target = match target with
        | Some target_pkh -> Some (target_pkh, None)
        | None -> None
      in
      let operation =
        Dune_manager_operation (Dune_manage_account { target; options }) in
      Internal_operation { source; operation; nonce }

let translate_big_map_diff
    (diff : Love_value.Op.big_map_diff option) :
  Alpha_context.Contract.big_map_diff_item list option
  =
  let open Alpha_context in
  let open Alpha_context.Script in
  let open Love_repr in
  let open Love_value in
  match diff with
  | None -> None
  | Some diff ->
      let diff = List.map (function
          | Op.Update { big_map; diff_key; diff_key_hash; diff_value } ->
              let diff_key = Dune_expr (Const (Value diff_key)) in
              let diff_value = match diff_value with
                | None -> None
                | Some v -> Some (Dune_expr (Const (Value v)))
              in
              Contract.Update { big_map; diff_key; diff_key_hash; diff_value }
          | Clear id -> Contract.Clear id
          | Copy (ids, idd) -> Contract.Copy (ids, idd)
          | Alloc { big_map; key_type; value_type; } ->
              let key_type = Dune_expr (Const (Type key_type)) in
              let value_type = Dune_expr (Const (Type value_type)) in
              Contract.Alloc { big_map; key_type; value_type; }
        ) diff
      in
      Some diff
