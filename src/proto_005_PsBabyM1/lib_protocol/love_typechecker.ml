(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_ast
open Love_ast_utils
open Love_ast_types
open Love_ast_types.AST
open Love_ast_types.TYPE
open Love_type
open Log
open Ident
open Utils
open Collections (* for IntMap and StringMap *)
open Love_type_utils
open Love_printer
open Error_monad

type typ = TYPE.t

exception TypingError of string * string * Love_tenv.t
exception UnknownType of type_name
exception UnknownConstr of cstr_name
exception UnknownField of field_name
exception TypingErrorLwt of (Love_context.t * string *  Love_tenv.t)

type where =
    TopLevel
  | Typedef
  | Storage
  | Parameter
  | ViewReturn
  | BigMapKey
  | BigMapValue
  | FunArgument

let w_to_str = function
    TopLevel -> "toplevel"
  | Typedef -> "type definition"
  | Storage -> "storage"
  | Parameter -> "parameter"
  | ViewReturn -> "view return"
  | BigMapKey -> "big map key"
  | BigMapValue -> "big map value"
  | FunArgument -> "function argument"


module TNSet = Collections.StringIdentSet
module TVSet = TYPE.TypeVarSet
module TVMap = TYPE.TypeVarMap

let rec check_forbidden_type rec_type ~where env t =
  debug "[check_forbidden_type] Checking if type %a in %s is forbidden@."
    Love_type.pretty t (w_to_str where);
  match t with
  | TPackedStructure _ -> false
  | TUser (LName "entrypoint", [t]) -> (
      match where with
        TopLevel
      | Typedef
      | FunArgument -> check_forbidden_type rec_type ~where:Parameter env t
      | Storage
      | Parameter
      | ViewReturn
      | BigMapKey
      | BigMapValue -> false
    )
  | TUser (LName n, [t1; t2]) when
      String.equal n (if Love_pervasives.has_protocol_revision 3 then "view" else "entrypoint")
    -> (
      match where with
        TopLevel | Typedef
      | FunArgument ->
          check_forbidden_type rec_type ~where:Parameter env t1 &&
          check_forbidden_type rec_type ~where:ViewReturn env t2
      | Storage
      | Parameter
      | ViewReturn
      | BigMapKey
      | BigMapValue -> false
    )
  | TUser (LName "view", [t]) -> (
      match where with
        TopLevel | Typedef
      | FunArgument ->
          check_forbidden_type rec_type ~where:TopLevel env t
      | Storage
      | Parameter
      | ViewReturn
      | BigMapKey
      | BigMapValue -> false
    )
  | TForall (tv, t) -> (
      match where with
        TopLevel
      | ViewReturn
      | Typedef
      | FunArgument ->
          check_forbidden_type
            rec_type
            ~where
            (Love_tenv.add_forall tv env) t
      | Storage
      | Parameter
      | BigMapKey
      | BigMapValue -> false
    )

  | TContractInstance _ -> (
      match where with
        TopLevel
      | Parameter
      | Typedef
      | FunArgument-> true
      | Storage (* temporary restriction *)
      | ViewReturn
      | BigMapKey
      | BigMapValue -> false
    )
  | TVar tv -> Love_tenv.is_free tv env
  | TArrow _ -> (
      match where with
      | TopLevel | Storage | ViewReturn | Typedef
      | FunArgument -> true
      | Parameter -> false (* temporary restriction *)
      | BigMapKey | BigMapValue -> false
    )

  | TTuple l -> List.for_all (check_forbidden_type ~where rec_type env) l

  | TUser (LName "set", [t])
  | TUser (LName "list", [t]) -> (check_forbidden_type ~where rec_type env) t
  | TUser (LName "map", [t; t']) ->
      (check_forbidden_type ~where rec_type env t) &&
      (check_forbidden_type ~where rec_type env t')
  | TUser (LName "bigmap", [t; t']) -> (
      match where with
      | Storage | Typedef
      | FunArgument ->
          (check_forbidden_type ~where:BigMapKey rec_type env) t &&
          (check_forbidden_type ~where:BigMapValue rec_type env) t'
      | Parameter -> false (* temporary restriction *)
      | TopLevel | BigMapKey | BigMapValue | ViewReturn -> false
    )

  | TUser (n,l) as t -> (
      begin
        debug "[check_forbidden_type] Type %a@." Ident.print_strident n;
        if TNSet.mem n rec_type
        then true
        else begin
          match l,Love_tenv.find_type_kind n env with
            _, None -> false
          | [], Some _ -> true
          | _, Some {result = (_,params,_); _} ->
              if not @@ Compare.Int.equal (List.length l) (List.length params)
              then false
              else begin
                let param_map =
                  List.fold_left2
                    (fun acc param ty ->
                       debug
                         "[check_forbidden_type] Associating %a to %a"
                         Love_type.pp_typvar param
                         Love_type.pretty ty;
                       TypeVarMap.add param ty acc)
                    TypeVarMap.empty
                    params
                    l
                in
                match Love_tenv.find_sum n env with
                  Some {result; _} ->
                    List.for_all (
                      fun Love_tenv.{cargs; crec; _} ->
                        let rec_type = match crec with
                            Rec -> TNSet.add n rec_type
                          | NonRec -> rec_type
                        in
                        let cargs =
                          List.map
                            (Love_type_utils.replace_map (fun x -> Love_tenv.isComp x env) param_map)
                            cargs
                        in
                        List.for_all (
                          fun t ->
                            debug "[check_forbidden_type] Checking type %a@."
                              (Love_type.pretty) t;
                            check_forbidden_type ~where rec_type env t
                        ) cargs
                    ) result
                | None -> (
                    match Love_tenv.find_record n env with
                      Some {result; _} ->
                        List.for_all
                          (fun Love_tenv.{ftyp; frec; _} ->
                             let rec_type =
                               match frec with
                                 Rec -> TNSet.add n rec_type
                               | NonRec -> rec_type in
                             let ftyp =
                               Love_type_utils.replace_map
                                 (fun x -> Love_tenv.isComp x env)
                                 param_map
                                 ftyp in
                             check_forbidden_type ~where rec_type env ftyp) result
                    | None -> (
                        let t' =
                          Love_tenv.normalize_type ~relative:true t env |>
                          Love_type_utils.replace_map
                            (fun x -> Love_tenv.isComp x env)
                            param_map
                        in
                        if (Compare.Int.equal (Love_type.compare t t') 0)
                        then true
                        else check_forbidden_type ~where rec_type env t'
                      )
                  )
              end
        end
      end
    )

let check_forbidden_type = check_forbidden_type TNSet.empty

let spf = Format.asprintf

(** Tests is the type in argument is well formed, i.e. composed of
    well defined types. It also checks that its kind is compabible
    with the type of its children.
    Example : type t = t1 * t2 is safe if both t1 and t2 are safe and
    their kind are compatible with t (here, t1 and t2 are not internal).
*)
let is_safe_res
    ~where
    (env : Love_tenv.t)
    (k : AST.type_kind)
    (t : typ) : (unit, string) result =
  debug "[is_safe] Checking type %a in %s in environment %a@."
    (Love_type.pretty) t
    (w_to_str where)
    Love_tenv.pp_env env
  ;
  let rec test ~comparable env t =
    let privacy_and_comparable_test =
      match t with
      | TPackedStructure _
      | TContractInstance _ ->
        if comparable then Error "Structures & instances are not comparable"
        else Ok ()
      | TUser (LName "bigmap", [t1; t2]) -> (
          match test ~comparable env t1 with
            Ok () -> test ~comparable env t2
          | r -> r
        )
      | TUser (LName "bigmap", _) -> Error "Bigmaps expects 2 type arguments"

      | TUser (LName "map", [t1; t2]) -> (
          match test ~comparable:true env t1 with
            Ok () -> test ~comparable env t2
          | r -> r
        )
      | TUser (LName "map", _) -> Error "Maps expects 2 type arguments"

      | TUser (LName "entrypoint", [t]) ->
        if comparable
        then Error "Entry points are not comparable else "
        else test ~comparable env t
      | TUser (LName "entrypoint", _) -> Error "Entrypoints expects 1 type arguments"

      | TUser (LName "view", [t]) -> (
          if comparable
          then Error "Views are not comparable"
          else
            test ~comparable env t
        )
      | TUser (LName "view", _) -> Error "Views expects 1 type arguments"
      | TArrow (t1, t2) -> (
          if comparable
          then Error "Lambdas are not comparable"
          else
            match test ~comparable env  t1 with
              Ok () -> test ~comparable env t2
            | r -> r
        )

      | TTuple l ->
        List.fold_left
          (function (Ok () : ('a,'b) result) -> test ~comparable env | r -> fun _ -> r)
          (Ok ())
          l
      | TUser (LName "list", [t]) -> test ~comparable env t
      | TUser (LName "list", _) -> Error "Lists expects 1 type arguments"
      | TUser (LName "set", [t]) -> test ~comparable:true env t
      | TUser (LName "set", _) -> Error "Sets expects 1 type arguments"
      | TVar _ -> Ok ()
      (* In type definitions, comparability is not settable.
         In foralls, the test is done previously.
         Hence, we don't need to check TVars. *)
      | TForall (tv, t) ->
        if comparable && not tv.tv_traits.tcomparable
        then Error "Comparable forall while quantifying on a non comparable type"
        else test ~comparable (Love_tenv.add_forall tv env) t
      | TUser (n, p) -> (
          let params_and_privacy =
            try
              let tkind = Love_tenv.find_type_kind n env in
              let sub_k,params, traits =
                match tkind with
                  Some {result = (sub_k,p, traits); _} ->
                  debug "[is_safe] Type %a is registered has %i parameters and traits %a@."
                    Ident.print_strident n (List.length p)
                    Love_type.pp_trait traits;
                  sub_k, p, traits
                | None ->
                  debug "[is_safe] Type %a is not registered@." Ident.print_strident n;
                  raise (UnknownType n)
              in
              if comparable && not (traits.tcomparable)
              then Error "Type is not comparable"
              else (
                let rec aux tvars params : ('a, 'b) result =
                  match tvars,params with
                    [], [] -> Ok ()
                  | tv :: tltv, hdp :: tlp -> (
                      match
                        test ~comparable:(tv.tv_traits.tcomparable||comparable) env hdp
                      with
                        Ok () -> aux tltv tlp
                      | r -> r
                    )
                  | _,_ -> Error "Bad number of polymorphic parameters" in
                match aux params p with
                  Error _ as e -> e
                | Ok () -> (
                    debug "[is_safe] Correct number of poymorphic arguments@.";
                    match sub_k, k with
                      TPublic, _
                    | TPrivate, TPrivate
                    | TPrivate, TAbstract
                    | TPrivate, TInternal

                    | TAbstract, TAbstract
                    | TAbstract, TInternal

                    | TInternal, TInternal
                    | TInternal, TAbstract
                    | TAbstract, TPublic
                    | TAbstract, TPrivate -> (Ok () : (unit, _) result)

                    | TPrivate, TPublic
                    | TInternal, TPublic
                    | TInternal, TPrivate ->
                      Error (
                        Format.asprintf
                          "Type %a is %a while its parent is %a"
                          Ident.print_strident n
                          Love_ast.pp_type_kind sub_k
                          Love_ast.pp_type_kind k
                      )
                  )
              )
            with
              UnknownType i ->
              Error (
                Format.asprintf
                  "User type %a is unknown"
                  print_strident i
              )
          in params_and_privacy
        )

    in
    match privacy_and_comparable_test with
      Ok _ ->
      debug "[is_safe] Parameter's list is OK @.";
      let fv = Love_type.fvars t in
      debug "[is_safe] Type vars =";
      TypeVarSet.iter (fun v -> debug "%a@." Love_type.pp_typvar v) fv;
      let free = Love_tenv.get_free env in
      debug "@.[is_safe] Free vars =";
      TypeVarSet.iter (fun v -> debug "%a@." Love_type.pp_typvar v) free;
      debug "@.";
      if TVSet.subset fv free
      then (Ok () : (unit, _) result)
      else
        begin
          match TVSet.find_first_opt (fun e -> not @@ TVSet.mem e free) fv with
          | None -> assert false
          | Some tv ->
            Error (Format.asprintf
                     "Polymorphic variable %a not found" Love_type.pp_typvar tv)
        end
    | r -> r
  in
  if not(Love_pervasives.has_protocol_revision 3) || check_forbidden_type ~where env t
  then
    test ~comparable:false env t
  else Error (Format.asprintf "Type %a is forbidden" Love_type.pretty t)

let rec ok_list :
  type t. (Love_tenv.t -> type_kind -> t -> (unit,string) result) -> Love_tenv.t -> type_kind -> t list -> (unit,string) result =
  fun f env k l ->
  match l with
    hd :: tl ->
      begin
        match f env k hd with
          Ok () -> ok_list f env k tl
        | e -> e
      end
  | [] -> Ok ()

let is_safe_list ~where env k l : (unit,string) result =
  ok_list (is_safe_res ~where) env k l

let loc_to_str annot default =
  match annot with
    None -> default
  | Some l -> Format.asprintf "location %a" Love_ast.pp_location l

let error =
  let pre = fun m -> Error m in
  Format.kasprintf pre

(* Todo : better error message with is_safe_res *)
let is_safe_tdef k name tdef env : (unit,string) result =
  let name_is_forbidden =
    match name with
    | "instance" -> true
    | _ -> Love_type_list.exists name
  in
  if name_is_forbidden then error "Type name %s is forbidden" name
  else
    let where =
      if not @@ Love_pervasives.has_protocol_revision 3 then TopLevel
      else if String.equal "storage" name then Storage
      else Typedef
    in
    match tdef with
    | SumType {sparams; scons; srec} ->
        let env_for_safe_test =
          let env =
            match srec with
              NonRec -> env
            | Rec -> Love_tenv.add_typedef name TPublic tdef env in
          List.fold_left
            (fun env tv -> Love_tenv.add_forall tv env) env sparams in
        ok_list (fun env k (_,l) -> is_safe_list ~where env k l) env_for_safe_test k scons
    | RecordType {rparams; rfields; rrec} ->
      let env_for_safe_test =
        let env =
          match rrec with
            NonRec -> env
          | Rec -> Love_tenv.add_typedef name TPublic tdef env in
        List.fold_left
          (fun env tv -> Love_tenv.add_forall tv env) env rparams in
      ok_list (fun e k (_,t) -> is_safe_res ~where e k t) env_for_safe_test k rfields
    | Alias {aparams; atype} ->
      (* Private aliases are forbidden *)
      match k with
        TPrivate ->
        Log.debug "Error : Aliases with private types are forbidden";
        Error "Private aliases are forbidden"
      | _ ->
        let env_for_safe_test =
          List.fold_left
            (fun env tv -> Love_tenv.add_forall tv env) env aparams in
        is_safe_res
          ~where
          env_for_safe_test
          k
          atype

let type_error loc env =
  let pre = fun m -> raise (TypingError (m, loc, env)) in
  Format.kasprintf pre

let type_error_lwt ctxt =
  let pre = fun m -> raise (TypingErrorLwt (ctxt, m, ctxt.type_env)) in
  Format.kasprintf pre

let check_private loc name env =
  let check_path p =
    List.for_all
      (function Path.Prev -> true | Next _ -> false)
      p in
  match Love_tenv.find_type_kind name env with
    None -> type_error loc env "Type %a not found" Ident.print_strident name
  | Some {result = (TPublic,_, _); _} (* valid : this type is public *) -> ()
  | Some {result = ((TPrivate | TAbstract | TInternal), _, _); path; _}
    when check_path path
    (* valid : these types are used locally. *)
    -> ()

  | _ ->
    raise (
      TypingError (
        Format.asprintf "Sum type %a cannot be build" Ident.print_strident name,
        loc, env
      )
    )

let sig_of_contract loc env n =
  match Love_tenv.find_contract n env with
  | Some { result; _ } ->
      Love_tenv.env_to_contract_sig result
  | _ -> type_error loc env "Structure %a not found" Ident.print_strident n

let print_error t1 t2 st1 st2 =
  debug
    "[check] Error : Type %a is not equal to %a because %a and %a are incompatible@."
    Love_type.pretty t1
    Love_type.pretty t2
    Love_type.pretty st1
    Love_type.pretty st2;
  match t1, t2 with
    TArrow _, TArrow _ -> ()
  | TArrow _, _ -> debug "[check] Did you forget arguments ?@."
  | _, TArrow _ -> debug "[check] Did you provide too many arguments ?@."
  | TForall _, TForall _ -> ()
  | _t, TForall _
  | TForall _, _t -> debug "[check] Check the polymorphic type applications ([<type>])@."
  | _,_ -> ()

let subtyp loc env t1 t2 =
  let signatures =
    fun n ->
      match Love_tenv.find_signature n env with
        None ->
        type_error loc env "Signature not found"
      | Some {result; _} -> Love_tenv.env_to_contract_sig result
  in
  Love_type.subtyp
    signatures
    (Love_tenv.normalize_type ~relative:false t1 env)
    (Love_tenv.normalize_type ~relative:false t2 env)

let check ?(error="") loc env (t1 : typ) (t2 : typ) : unit =
  debug
    "[check] Checking %a < %a@."
    Love_type.pretty t1
    Love_type.pretty t2;
  match subtyp loc env t1 t2 with
    Ok -> ()
  | res ->
    debug "[check] Error : %s" error;
    type_error loc env "%s" (Love_type.str_subtyp_result res)

let equal_type env t1 t2 =
  Love_type.bool_equal
    (Love_tenv.normalize_type ~relative:false t1 env)
    (Love_tenv.normalize_type ~relative:false t2 env)

let typecheck_const _env (c : const) : TYPE.t =
  debug
    "[typecheck_const] Typing constant@.";
  match c.content with
    CUnit ->
    debug "[typecheck_const] Unit@.";
    Love_type_list.get_type "unit" []
  | CBool _b ->
    debug "[typecheck_const] Bool@.";
    Love_type_list.get_type "bool" []
  | CString _s ->
    debug "[typecheck_const] String@.";
    Love_type_list.get_type "string" []
  | CBytes _b ->
    debug "[typecheck_const] Bytes@.";
    Love_type_list.get_type "bytes" []
  | CInt _i ->
    debug "[typecheck_const] Int@.";
    Love_type_list.get_type "int" []
  | CNat _i ->
    debug "[typecheck_const] Nat@.";
    Love_type_list.get_type "nat" []
  | CDun _d ->
    debug "[typecheck_const] Dun@.";
    Love_type_list.get_type "dun" []
  | CKey _k ->
    debug "[typecheck_const] Key@.";
    Love_type_list.get_type "key" []
  | CKeyHash _kh ->
    debug "[typecheck_const] KeyHash@.";
    Love_type_list.get_type "keyhash" []
  | CSignature _s ->
    debug "[typecheck_const] Signature@.";
    Love_type_list.get_type "signature" []
  | CAddress _a ->
    debug "[typecheck_const] Address@.";
    Love_type_list.get_type "address" []
  | CTimestamp _t ->
    debug "[typecheck_const] Timestamp@.";
    Love_type_list.get_type "timestamp" []

type pattern_check_request = {
  env : Love_tenv.t;
  new_vars : StringSet.t
}

type pattern_check_res = (pattern_check_request, string) result

let add_var var t {env; new_vars} =
  debug "[typecheck_pattern/add_var] Adding variable %s of type %a@."
    var Love_type.pretty t;
  if StringSet.mem var new_vars then
    Error (Format.sprintf "Variable %s is bounded twice in pattern" var)
  else
    Ok ({env = Love_tenv.add_var var t env; new_vars = StringSet.add var new_vars})

let tpc_pany _ _ req : pattern_check_res = Ok req

let tpc_pconst tpc_cst loc t c req : pattern_check_res =
  let typ = tpc_cst req.env c in
  check
    ~error:"Constant patterns with different types"
    loc
    req.env
    t
    typ;
  Ok req

let tpc_pvar _ t v req : pattern_check_res = add_var v t req

let tpc_palias tpc_pat loc t v pat req : pattern_check_res =
  let res = tpc_pat loc t pat req in
  add_var v t res

let tpc_pnil _ t req : pattern_check_res =
  match t with
    TUser (LName "list", [_]) -> Ok req
  | t -> Error (Format.asprintf "Pattern [] does not have the type %a" Love_type.pretty t)

let tpc_plist tpc_pat loc t pl req : pattern_check_res =
  let rev_pl = List.rev pl in
  let last, rest =
    match rev_pl with
      [] -> assert false
    | hd :: tl -> hd, tl in
  let req_with_last = tpc_pat loc t last req in
  match t with
    TUser (LName "list", [t])->
      Ok (
        List.fold_left
          (fun req p -> tpc_pat loc t p req)
          req_with_last
          rest
      )
  | t -> Error (
      Format.asprintf
        "Pattern is a list, it does not have the type %a" Love_type.pretty t)

let tpc_ptuple tpc_pat loc t pl req : pattern_check_res =
  match t with
    TTuple tl -> (
      try Ok (
          List.fold_left2
            (fun req p etype -> tpc_pat loc etype p req)
            req
            pl
            tl
        )
      with Invalid_argument _ ->
        type_error loc req.env
          "Pattern tuple with bad arity :\
           %i elements in tuple, expected %i by type %a"
          (List.length pl)
          (List.length tl)
          (Love_type.pretty) t
    )
  | t -> Error (
      Format.asprintf
        "Pattern is a tuple, it does not have the type %a"
        Love_type.pretty t
    )

let tpc_pconstr tpc_pat loc t c plist res : pattern_check_res =
  match t with
    TUser (name, l) -> (
      let tlist =
        let c, parameters = Ident.change_last name @@ Ident.create_id c, l in
        let cargs, tparent =
          match Love_tenv.find_constr c res.env with
            None -> raise (UnknownConstr c)
          | Some {result = {cargs;cparent; _}; _} -> cargs, cparent in
        let poly_parameters =
          match tparent with
            TUser (_, l) -> l
          | _ -> assert false (* A registered sum parent must always be a user type *)
        in
        let update_map =
          try
            List.fold_left2
              (fun acc poly_par par ->
                 match poly_par with
                   TVar v -> TVMap.add v par acc
                 | _ -> assert false
              )
              TVMap.empty
              poly_parameters
              parameters
          with
            Invalid_argument _ ->
              type_error loc res.env "Type %a is incompatible with pattern %a that has type %a"
                Love_type.pretty t Ident.print_strident c Love_type.pretty tparent
        in
        List.map
          (Love_type_utils.replace_map (fun x -> Love_tenv.isComp x res.env) update_map)
          cargs
      in
      let tlist =
        match tlist, plist with
        | [TTuple _], [_] -> tlist
        | [TTuple tl], _ -> tl
        | _, _ -> tlist in
        try
          let res =
            List.fold_left2
              (fun res p etype -> tpc_pat loc etype p res)
              res
              plist
              tlist in Ok res
        with Invalid_argument _ ->
          Error (
            Format.asprintf
              "Constructor with bad number of arguments :\
               %i elements expected (%a), %i provided"
              (List.length tlist)
              (Format.pp_print_list ~pp_sep:(fun fmt _ -> Format.fprintf fmt ", ") Love_type.pretty) tlist
              (List.length plist)
          )
    )
  | t ->
      Error (
        Format.asprintf "Pattern %s is a constructor; it does not have the type %a"
          c
          Love_type.pretty t
      )

let tpc_pcontract loc t name st res : pattern_check_res =
  let norm_t = Love_tenv.normalize_type ~relative:true t res.env in
  let norm_st =
    let t = TContractInstance st in
    Love_tenv.normalize_type ~relative:true t res.env
  in
  check loc res.env norm_st norm_t;
  let add_to_res res : pattern_check_res =
    match add_var name t res with
      Ok res' -> Ok {res with new_vars = res'.new_vars}
    | e -> e
  in
  match norm_t with
    TContractInstance (Anonymous a) ->
      let new_env = Love_tenv.contract_sig_to_env (Some name) a res.env in
      add_to_res {res with env = Love_tenv.add_subcontract_env name new_env res.env}
  | TContractInstance (Named n) ->
      add_to_res {res with env = Love_tenv.add_signed_subcontract name n res.env}
  | t ->
      Error (
        Format.asprintf "Pattern %s is a contract, it does not have the type %a"
          name
          Love_type.pretty t)

let tpc_por tcp_pat loc ty l res : pattern_check_res =
  match l with
    [] -> raise (Exceptions.InvariantBroken "Empty Or pattern matching in Typechecker")
  | hd :: tl ->
      let expected_res = tcp_pat loc ty hd res in
      let rec loop : 'pattern list -> pattern_check_res = function
          [] -> Ok expected_res
        | hd' :: tl -> begin
            let res = tcp_pat loc ty hd' res in
            if StringSet.equal res.new_vars expected_res.new_vars then
              loop tl
            else (* Fail *) begin
              let diff =
                StringSet.diff
                  (StringSet.union res.new_vars expected_res.new_vars)
                  res.new_vars
              in
              Error (
                Format.asprintf
                  "Or pattern binds different variables. Differences: %a"
                  (Format.pp_print_list
                     ~pp_sep:(fun fmt _ -> Format.fprintf fmt ", ")
                     (fun fmt -> Format.fprintf fmt "%s")
                  ) (Collections.StringSet.elements diff))
            end
          end
      in loop tl

let tpc_precord tcp_pat loc ty l req : pattern_check_res =
  try
    match ty with
      TUser (name, params) -> begin
        let fields = Love_tenv.field_applied_types name params req.env in
        let field_map =
          List.fold_left
            (fun acc Love_tenv.{fname; ftyp; _} ->
               StringMap.add fname ftyp acc
            )
            StringMap.empty
            fields
        in
        let remaining_fields, res =
          List.fold_left
            (fun (map, res) ->
               match res with
               | Error s -> fun _ -> (map, Error s)
               | Ok req ->
                   fun (name, p) ->
                     debug "[tpc_rec] Field %s@." name;
                     match StringMap.find_opt name map, p with
                       None, _ ->
                         (* Error *)
                         if StringMap.mem name field_map then
                           (map, Error (Format.sprintf "Field %s bound twice" name))
                         else
                           (map, Error (Format.sprintf "Unknown field %s" name))
                     | Some ty, None ->
                         begin
                           match add_var name ty req with
                             Ok req ->
                               (StringMap.remove name map), Ok req
                           | e -> map, e
                         end
                     | Some ty, Some pat ->
                         map, Ok (tcp_pat loc ty pat req)
            )
            (field_map, Ok req)
            l.fields
        in
        match res, l.open_record with
        | Error e, _ -> Error e
        | Ok _, Open -> res
        | Ok _, Closed ->
            match StringMap.find_first_opt (fun _ -> true) remaining_fields with
            | None -> res
            | Some (random, _) ->
                Error (
                  Format.asprintf "Record pattern is closed and does not binds field %s" random
                )
      end
    | t -> Error (Format.asprintf "Record pattern cannot have type %a" Love_type.pretty t)
  with Love_tenv.EnvironmentError s -> Error s

let pt_error loc env p str =
  type_error loc env "Error while typechecking pattern %a: %s"
    Love_printer.Ast.print_pattern p
    str

let rec typecheck_pattern loc t pattern req : pattern_check_request =
  debug "[typecheck_pattern] Typechecking pattern %a with type %a@."
    Ast.print_pattern pattern (Love_type.pretty) t;
  let opt_res =
    match pattern.content with
      PAny                 -> tpc_pany loc t req
    | PConst c             -> tpc_pconst typecheck_const loc t c req
    | PVar v               -> tpc_pvar loc t v req
    | PAlias (pat, v)      -> tpc_palias typecheck_pattern loc t v pat req
    | PList []             -> tpc_pnil loc t req
    | PList pl             -> tpc_plist typecheck_pattern loc t pl req
    | PTuple pl            -> tpc_ptuple typecheck_pattern loc t pl req
    | PConstr (c, plist)   -> tpc_pconstr typecheck_pattern loc t c plist req
    | PContract (name, st) -> tpc_pcontract loc t name st req
    | POr l                -> tpc_por typecheck_pattern loc t l req
    | PRecord l            -> tpc_precord typecheck_pattern loc t l req
  in
  match opt_res with
    Ok res -> res
  | Error s -> pt_error loc req.env pattern s

let typecheck_pattern loc env t pattern =
  debug "[typecheck_pattern] Typechecking pattern %a : %a"
    Love_printer.Ast.print_pattern pattern Love_type.pretty t;
  let t = Love_tenv.normalize_type ~relative:true t env in
  let res = typecheck_pattern loc t pattern {env; new_vars = StringSet.empty} in
  let () = debug "[typecheck_pattern] Pattern %a typechecked. %s variables %a added."
      Love_printer.Ast.print_pattern pattern
      (if StringSet.is_empty res.new_vars then "No new" else "New")
      (Format.pp_print_list
         ~pp_sep:(fun fmt _ -> Format.fprintf fmt ", ")
         (fun fmt -> Format.fprintf fmt "%s")
      ) (Collections.StringSet.elements res.new_vars) in
  res.env

let tcp_evar loc v ext_arg env =
  match Love_tenv.find_var v env with
    None -> type_error loc env "Unknown variable %a" print_strident v
  | Some {result = (Love_tenv.Primitive g, _); _} -> g.prim_type ext_arg
  | Some {result = _,t; _} -> t

let rec typecheck_exp (env : Love_tenv.t) (exp : exp) : exp * typ =
  debug "[typecheck_exp loc] Typechecking exp %a@." Ast.print_exp exp;
  let loc_to_str a =
    loc_to_str
      a
      (Format.asprintf "expression %a" Love_printer.Ast.print_exp exp)
  in
  let loc = loc_to_str exp.annot in
  let (e : exp), t =
    match exp.content with
    | Const c ->
        debug "[typecheck_exp] Constant@.";
        let t = typecheck_const env c in
        exp, t

    | Var v -> (
        debug "[typecheck_exp] Variable %a@." print_strident v;
        let new_typ = tcp_evar loc v ANone env in
        debug "[typecheck_exp] %a:%a@." print_strident v Love_type.pretty new_typ;
        exp, new_typ
      )

    | VarWithArg (v, xa) -> (
        debug "[typecheck_exp] Variable %a with extended arg@."
          print_strident v;
        let new_typ = tcp_evar loc v xa env in
        debug "[typecheck_exp] %a:%a@."
          print_strident v
          Love_type.pretty new_typ;
        exp, new_typ
      )

    | Let { bnd_pattern; bnd_val; body} ->
      debug "[typecheck_exp] Let@.";
      let bnd_val, typ = typecheck_exp env bnd_val in
      let env' = typecheck_pattern loc env typ bnd_pattern in
      let body, typ = typecheck_exp env' body
      in mk_let bnd_pattern bnd_val body, typ

    | LetRec { bnd_var; bnd_val; body; fun_typ} -> (
        debug "[typecheck_exp] LetRec of type %a@." Love_type.pretty fun_typ;
        match is_safe_res ~where:TopLevel env TInternal fun_typ with
          Ok () -> (
          let env' = Love_tenv.add_var bnd_var fun_typ env in
          let bnd_val, t' = typecheck_exp env' bnd_val in
          check loc env fun_typ t';
          let body, typ = typecheck_exp env' body in
          mk_let_rec bnd_var bnd_val body fun_typ, typ
        )
        | Error s ->
          type_error loc env
            "Type %a is unsafe : %s"
            (Love_type.pretty) fun_typ s
      )
    | Lambda {arg_pattern; body; arg_typ} -> (
        debug "[typecheck_exp] Lambda : (%a : %a) -> ...@."
          Ast.print_pattern arg_pattern Love_type.pretty arg_typ;
        match
          is_safe_res
            ~where:(if Love_pervasives.has_protocol_revision 3 then FunArgument else TopLevel)
            env
            TInternal
            arg_typ with
          Ok () -> (
            let env' = typecheck_pattern loc env arg_typ arg_pattern in
            let body, typ = typecheck_exp env' body in
            mk_lambda arg_pattern body arg_typ,
            TArrow (arg_typ, typ)
          )
        | Error s -> (
            type_error loc env
              "Type %a of argument %a is unsafe : %s"
              (Love_type.pretty) arg_typ
              Love_printer.Ast.print_pattern arg_pattern
              s
          )
      )
    | Apply { fct; args } ->
      debug "[typecheck_exp] Application of function %a@."
        Ast.print_exp fct;
      let fct', typ = typecheck_exp env fct in
      debug "[typecheck_exp] Unaliased type : %a@." Love_type.pretty typ;
      let rec check_arrow acc ftype args =
        match args with
          [] ->
          debug "[typecheck_exp] No more argument, type is %a"
            Love_type.pretty ftype;
          mk_apply fct' (List.rev acc), ftype
        | hd :: tl -> (
            match ftype with
            | TArrow (t1, t2) ->
              debug "[typecheck_exp] fun %a -> %a@."
                Love_type.pretty t1
                Love_type.pretty t2;
              let e, t = typecheck_exp env hd in
              debug "[typecheck_exp] Argument %a has type %a.\
                     Checking against %a, the type of %a@."
                Ast.print_exp hd
                Love_type.pretty t
                Love_type.pretty t1
                Ast.print_exp fct;
              check loc env t1 t;
              debug "[typecheck_exp] Argument has the correct type, continuing with %a.@."
              Love_type.pretty t2;
              check_arrow (e :: acc) t2 tl
            | _ -> (
              debug "[typecheck_exp] %a is not callable.@."
                Love_type.pretty ftype;
              let is_forall = match ftype with TForall _ -> true | _ -> false in
              let msg : _ format4 =
                if is_forall
                then
                  "Call error : %a is applied to %a of type %a, which is not a function. \
                   Did you forget the type application ?"
                else
                  "Call error : %a is applied to %a of type %a, which is not a function."
              in
              type_error loc env msg
                Love_printer.Ast.print_exp hd
                Love_printer.Ast.print_exp fct
                Love_type.pretty ftype
            )
          )
      in
      begin match typ with
      | TUser (LName "entrypoint", [pt]) ->
         (* Allow calling of entry points as function applications *)
         let open Love_ast_utils in
         let loc = exp.annot in
         let e = mk_var ?loc (Ident.put_in_namespace "Contract"
                                (Ident.create_id "call")) in
         let e = mk_tapply ?loc e pt in
         let e = mk_apply ?loc e (fct :: args) in
         typecheck_exp env e
      | TUser (LName "view", [t]) ->
         (* Allow calling of views as function applications *)
         let open Love_ast_utils in
         let loc = exp.annot in
         let e = mk_var ?loc (Ident.put_in_namespace "Contract"
                                (Ident.create_id "view")) in
         let e = mk_tapply ?loc e t in
         let e = mk_apply ?loc e (fct :: args) in
         typecheck_exp env e
      | _ -> check_arrow [] typ args
      end
    | Seq el ->
      let rec check_seq acc =
        function
          [] -> type_error loc env "Empty sequences are forbidden"
        | [e] -> let e, t = typecheck_exp env e in
          Love_ast_utils.mk_seq (List.rev (e :: acc)), t
        | hd :: tl ->
          let e, shouldbeunit = typecheck_exp env hd in
          check loc env shouldbeunit
            (Love_type_list.get_type "unit" []);
          check_seq (e :: acc) tl
      in check_seq [] el

    | If { cond; ifthen; ifelse } -> (
        debug "[typecheck_exp] If ";
        let cond, shouldbebool = typecheck_exp env cond in
        check
          ~error:"Condition is not boolean"
          loc
          env
          shouldbebool
          (Love_type_list.get_type "bool" []);
        let ifthen, thentype = typecheck_exp env ifthen in
        let ifelse, elsetype = typecheck_exp env ifelse in
        try
          check loc env thentype elsetype;
          mk_if cond ifthen ifelse, thentype
        with TypingError _ ->
          check loc env elsetype thentype; mk_if cond ifthen ifelse, elsetype
      )

    | Match { arg; cases } ->
      let arg', typ = typecheck_exp env arg in
      debug "[typecheck_exp] Match on %a : %a@."
        Ast.print_exp arg (Love_type.pretty) typ;
      let treat_case env (pat,exp) =
        let env' = typecheck_pattern loc env typ pat in
        let exp, exptyp = typecheck_exp env' exp in
        (pat, exp), exptyp
      in
      let hd_case, other_cases =
        match cases with
          [] -> type_error loc env "Empty pattern matching are forbidden"
        | hd :: tl -> hd, tl in
      let hd_exp, hd_typ = treat_case env hd_case in
      let tref = ref hd_typ in
      (* We will update this ref in the next loop. We want to prove that
         every branch have the same type, so we will perform a subtype check
         on the first type against the second, the second against the thierd, etc. and
         the lasr againse the first. *)
      let tl_cases =
        List.map
          (fun case ->
             let case, typ = treat_case env case in
             check loc env !tref typ;
             tref := typ;
             case
          )
          other_cases in
      check loc env hd_typ !tref;
      (mk_match arg' (hd_exp :: tl_cases)), hd_typ

    | Constructor { constr; ctyps; args } -> (
        debug "[typecheck_exp] Constructor %a of %a@."
          Ident.print_strident constr
            (Format.pp_print_list ~pp_sep:(fun fmt _ -> Format.fprintf fmt ";;")
               Love_printer.Ast.print_exp) args;
        let args, arg_typs =
          List.map
            (fun exp ->
               typecheck_exp env exp
            )
            args
          |> List.split
        in
        (* AST transformation : if the constructor expects 1 argument
           and args has more or equal  than 2 arguments, then args is
           converted to a tuple *)
        debug "[typecheck_exp] Searching type of constructor %a@."
          Ident.print_strident constr;
        let ctyp = Love_tenv.find_constr constr env in
        match ctyp with
        | None ->
          type_error loc env "Constructor %a is unknown" Ident.print_strident constr
        | Some {result = ct; _} ->
          let targs =
            let c_init_params =
              match ct.cparent with
                TUser (n, l) ->
                debug "[typecheck_exp] Constructor %a belongs to type %a@."
                  Ident.print_strident constr Ident.print_strident n;
                l
              | _ -> raise (Exceptions.InvariantBroken "Registered parent must be a TUser")
            in
            let replace_map =
              try
                List.fold_left2
                  (fun acc old new_t ->
                     match old with
                       TVar tv ->
                       debug "[typecheck_exp] Replacing %a by %a@."
                         Love_type.pp_typvar tv
                         (Love_type.pretty) new_t
                       ;
                       TypeVarMap.add tv new_t acc
                     | _ -> raise (
                         Exceptions.InvariantBroken
                           "Registered parent must be a TUser with only tvars as parameters")
                  )
                  TypeVarMap.empty
                  c_init_params
                  ctyps
              with
                Invalid_argument _ ->
                type_error loc env
                  "Constructor %a expects %i type arguments, but %i are provided."
                  Ident.print_strident constr
                  (List.length c_init_params)
                  (List.length ctyps)
            in
            List.map
              (Love_type_utils.replace_map (fun x -> Love_tenv.isComp x env) replace_map)
              ct.cargs in
          let targs =
            match targs, arg_typs with
            | [TTuple _], [TTuple _] -> targs
            | [TTuple l], _ -> l
            | t,_ -> t in
          let () =
            try List.iter2 (check loc env) targs arg_typs
            with Invalid_argument _ ->
              type_error loc env
                "Constructor %a expects %i arguments, but only %i are provided."
                Ident.print_strident constr
                (List.length targs)
                (List.length args)
          in
          debug "[typecheck_exp] Check OK.@.";
          let args =
            if Compare.Int.(List.length ct.Love_tenv.cargs = 1 &&
                            List.length (
                              if Love_pervasives.has_protocol_revision 3 then
                                arg_typs
                              else ctyps) >= 2)
            then (
              debug "[typecheck_exp] Expecting a tuple.";
              [ mk_tuple args ]
            )
            else args in

          let constr_typ =
            match ct.cparent with
              TUser (name, _) ->
              (* Additional check : is this constructor public, private, abstract, internal ?*)
              check_private loc name env;
              TUser (name, ctyps)
            (* Check done *)
            | _ -> assert false
          in
          debug "[typecheck_exp] Args are now %a@."
            (Format.pp_print_list ~pp_sep:(fun fmt _ -> Format.fprintf fmt ";;")
               Love_printer.Ast.print_exp) args;
          mk_constr constr ctyps args, constr_typ
      )

    | List (el, e) -> (
        match el with
          [] -> (* This should never happen, (:: l) has no sense*)
          type_error loc env "List expressions must be prefixed by at least 1 argument"
        | _ ->
          let e, etyp =  typecheck_exp env e in
          let eltlist_type =
            match etyp with
              TUser (LName "list", [t]) -> t
            | TForall _ ->
              type_error loc env
                "List expression %a has type %a, which is polymorphic. \
                 Did you forget the type application?"
                Love_printer.Ast.print_exp e
                (Love_type.pretty) etyp
            | _ ->
              type_error loc env
                "List expression %a has type %a, which is not a list"
                Love_printer.Ast.print_exp e
                (Love_type.pretty) etyp
          in
          let el =
            List.map
              (fun elt ->
                 let elt', elt_typ = typecheck_exp env elt in
                 if equal_type env elt_typ eltlist_type
                 then elt'
                 else (
                   type_error loc env
                     "List element %a has type %a, \
                      while it was expected to have the type %a"
                     Ast.print_exp elt
                     Love_type.pretty elt_typ
                     Love_type.pretty eltlist_type;
                 )
              )
              el in
          {content = List (el, e); annot = None}, etyp
      )
    | Nil -> mk_enil (), Love_type.type_empty_list
    | Tuple []  ->
      type_error loc env "Empty tuples are not allowed"
    | Tuple [_] ->
      type_error loc env
        "Tuple %a has only 1 element: must at least have 2 elements"
        Love_printer.Ast.print_exp exp
    | Tuple el  ->
      let el, ety =
        List.map (fun e ->
            let e', t = typecheck_exp env e in
            debug "[typecheck_exp] Tuple element %a : %a@."
              Ast.print_exp e Love_type.pretty t;
            e',t
          )
          el |> List.split
      in
      mk_tuple el, TTuple ety

    | Project { tuple; indexes } -> (
        let tuple, typ = typecheck_exp env tuple in
        match typ with
        | TTuple l -> (
            let tys = List.map (fun i ->
               match List.nth_opt l i with
               | Some t -> t
               | None ->
                 type_error loc env
                   "Projection out of bounds: \
                    %ith element (starting at 0) of %a is undefined"
                   i
                   Love_printer.Ast.print_exp tuple
              ) indexes
            in
            match tys with
            | [t] -> mk_projection tuple indexes, t
            | tl -> mk_projection  tuple indexes, TTuple tl
          )
        | _ ->
          type_error loc env
            "Expression %a has type %a: cannot project on a non-tuple expression"
            Love_printer.Ast.print_exp tuple
            (Love_type.pretty) typ
      )

    | Update { tuple; updates } ->
      (* Invariants : Updates not empty, never update twice the same field
         and max lower than tuple size  *)
      let tuple, tup_type = typecheck_exp env tuple in
      let tup_types, tup_length =
        match tup_type with
          TTuple l -> l, List.length l
        | _ ->
          type_error loc env
            "Expression %a has type %a: cannot update a non-tuple expression"
            Love_printer.Ast.print_exp tuple
            (Love_type.pretty) tup_type
      in
      let rec check_equal acc l =
        match l with
          [] -> acc
        | (i, e) :: tl ->
          if Compare.Int.(i < tup_length)
          then check_equal (IntMap.add i (typecheck_exp env e) acc) tl
          else
            type_error loc env
              "Tuple %a has size %i: update on index %i is impossible"
              Love_printer.Ast.print_exp tuple
              tup_length
              i
      in
      let update_types = check_equal IntMap.empty updates in
      let () =
        List.iteri
          (fun i tup_type ->
             match IntMap.find_opt i update_types with
               None -> ()
             | Some (_, t) ->
               check ~error:"Type of update is inconsistent with tuple type."
                 loc env t tup_type
          )
          tup_types
      in
      let updates =
        List.map
          (fun (i, _) ->
             match IntMap.find_opt i update_types with
               None -> assert false
             | Some (e, _) -> (i, e)
          )
          updates in
      mk_update tuple updates, tup_type

    | Record { contents = []; _} -> type_error loc env "Empty record are forbidden"
    | Record { path; contents = l }  ->
      let l, ftyp =
        List.split @@
        List.rev @@
        List.fold_left
          (fun acc (name, exp) ->
             let e, t = typecheck_exp env exp in
             ((name, e), (name, t)) :: acc
          )
          []
          l
      in
      let typ =
        try Love_tenv.record_type path ftyp env with
        | Love_tenv.EnvironmentError s ->
            type_error loc env "Error while typechecking record %a: %s"
              Love_printer.Ast.print_exp exp
              s
      in
      mk_record path l, typ

    | GetField { record; field } -> (
        let record, typ = typecheck_exp env record in
        match typ with
          TUser (name, args) ->
          let rec_def =
            match Love_tenv.find_record name env with
            | None ->
              type_error loc env
                "Record type %a is unknown"
                print_strident name
            | Some t -> t.result in
          let ftyp_opt = List.find_opt (fun tf ->
              String.equal tf.Love_tenv.fname field
            ) rec_def in
          let ftyp = match ftyp_opt with
            | None -> raise (UnknownField field)
            | Some t -> t in
          let ftyp_args = Love_tenv.field_with_targs ftyp args env in
          mk_get_field record field, ftyp_args.ftyp
        | _ ->
          type_error loc env
            "Expression %a has type %a: expected a record type."
            Love_printer.Ast.print_exp record
            (Love_type.pretty) typ
      )
    | SetFields { updates = [];_ } ->
      type_error loc env "SetFields expression should have at least one update"

    | SetFields {record; updates} ->
      debug "[typecheck_exp] Update of record %a@."
        Ast.print_exp record;
      let record, typ_rec = typecheck_exp env record in
      debug "[typecheck_exp] Record type %a@."
        (Love_type.pretty) typ_rec;
      let rec_def, targs = match typ_rec with
        | TUser (name, args) ->
          begin match Love_tenv.find_record name env with
            | None ->
              type_error loc env
                "Record type %a is unknown"
                print_strident name
            | Some t -> t.result, args
          end
        | _ ->
          type_error loc env
            "Expression %a has type %a: cannot set fields on a non-record type."
            Love_printer.Ast.print_exp record
            (Love_type.pretty) typ_rec
      in
      let poly_args_to_arg_map =
        match (List.hd rec_def).Love_tenv.fparent with
          TUser (_, a) ->
          List.fold_left2
            (fun acc parg arg ->
               match parg with
                 TVar tv -> TypeVarMap.add tv arg acc
               | _ ->  raise (Exceptions.InvariantBroken "Record type def should be polymorphic")
            )
            TypeVarMap.empty
            a
            targs
        | _ -> raise (Exceptions.InvariantBroken "Record type should be a User Type")
        | exception Failure _ -> raise (
            Exceptions.InvariantBroken "Record type should define at least 1 field")
      in
      let replace = Love_type_utils.replace_map (fun x -> Love_tenv.isComp x env) poly_args_to_arg_map in
      let treat_field (field, updt) =
        debug "[typecheck_exp] Field %s takes %a@." field
          Ast.print_exp updt;
        let ftyp_opt =
          List.find_opt
            (fun tf ->
               String.equal tf.Love_tenv.fname field
            ) rec_def in
        let ftyp =
          match ftyp_opt with
            None ->
            type_error loc env "Field %s not registered in environment" field
          | Some t -> t
        in
        check ~error:(
          spf "Field %s not belonging to the correct record" field)
          loc
          env
          (replace ftyp.fparent)
          typ_rec;
        let updt, t_updt = typecheck_exp env updt in
        check ~error:(spf "Badly typed field %s" field)
          loc
          env
          t_updt
          (replace ftyp.ftyp);
        (field, updt)
      in
      let updates = List.map treat_field updates in
      mk_set_field record updates, typ_rec

    | TLambda {targ; exp} ->
      debug "[typecheck_exp] TLambda (%a,%a)@."
        Love_type.pp_typvar targ Ast.print_exp exp;
      let env = Love_tenv.add_forall targ env in
      let exp', t = typecheck_exp env exp in
      debug "[typecheck_exp] TLambda (%a,%a) : %a@."
        Love_type.pp_typvar targ Ast.print_exp exp (Love_type.pretty) t;
      mk_tlambda targ exp', TForall (targ, t)

    | TApply {exp = {content = TLambda {targ; exp = exp'}; _}; typ} -> (
        debug "[typecheck_exp] TLambda in TApply : changing %a into %a@."
          Love_type.pp_typvar targ (Love_type.pretty) typ
        ;
        match
          is_safe_res
            ~where:(if Love_pervasives.has_protocol_revision 3 then FunArgument else TopLevel)
            env
            TInternal
            typ
        with
        Ok () ->
          let env = Love_tenv.add_forall targ env in
          let exp', t = typecheck_exp env exp' in
          debug "[typecheck_exp] TApply : Before replacing %a by %a, type is %a@."
            Love_type.pp_typvar targ (Love_type.pretty) typ
            (Love_type.pretty) t;
          let t = Love_type.replace (fun x -> Love_tenv.isComp x env) targ typ t in
          debug "[typecheck_exp] TApply : Type is %a@." (Love_type.pretty) t;
          mk_tapply (mk_tlambda targ exp') typ, t
        | Error s ->
          type_error loc env
            "Unsafe type application of %a: %s"
            (Love_type.pretty) typ
            s
      )
    | TApply {exp; typ} -> (
        debug "[typecheck_exp] Polymorphic expression %a applied to type %a@."
          Ast.print_exp exp
          Love_type.pretty typ;
        match
          is_safe_res
            ~where:(if Love_pervasives.has_protocol_revision 3 then FunArgument else TopLevel)
            env
            TInternal
            typ
        with
        Ok () -> (
        let exp', t' = typecheck_exp env exp in
        debug "[typecheck_exp] Polymorphic expression %a has type %a@."
          Ast.print_exp exp
          Love_type.pretty t';
        match t' with
          TForall (tvar, t) ->
          debug "[typecheck_exp] Replacing %a by %a@."
            Love_type.pp_typvar tvar Love_type.pretty typ;
          let new_type = Love_type.replace (fun x -> Love_tenv.isComp x env) tvar typ t in
          debug "[typecheck_exp] Updated type : %a@."
            Love_type.pretty new_type;
          mk_tapply exp' typ, new_type
        (*| TUser (n,l) -> (
          debug "[typecheck_exp] Polymorphic TUser typ@.";
          let rec replace_first_arg tl =
            match tl with
              [] ->
              debug "[typecheck_exp] Cannot apply [%a] on TUser %a@."
                (Love_type.pretty) typ (Love_type.pretty) t';
              type_error loc env
                "Expression %a has type %a, which is not quantified: cannot apply type %a"
                Love_printer.Ast.print_exp exp
                (Love_type.pretty) t'
                (Love_type.pretty) typ
              raise (TypingError ("TApply on a non quantified TUser type.", loc, env))
            | ((TVar tv) as t) :: tl ->
              if Love_tenv.is_free tv env
              then (
                debug "[typecheck_exp] Type variable %s already used@."
                tv.tv_name;
                t :: replace_first_arg tl ) else typ :: tl
            | t :: tl -> t :: replace_first_arg tl
          in
          let t = TUser (n, replace_first_arg l)
          in
          debug "[typecheck_exp] Polymorphic TUser typ = %a@." (Love_type.pretty) t;
          mk_tapply exp' typ, t
          ) *)
        | _ ->
          type_error loc env
            "Expression %a has type %a: cannot apply %a on non-polymorphic expressions."
            Love_printer.Ast.print_exp exp
            (Love_type.pretty) t'
            (Love_type.pretty) typ
      )
        | Error s ->
          type_error loc env "Unsafe application of type %a: %s"
            (Love_type.pretty) typ
            s
      )
    | PackStruct sr -> (
        match sr with
          Named n -> (
            match Love_tenv.find_contract n env with
              Some {result; _} ->
              mk_packstruct (Named n),
              TPackedStructure (Anonymous (Love_tenv.env_to_contract_sig result))
            | None ->
              type_error loc env
                "Contract %a not found in environment" print_strident n
          )
        | Anonymous s ->
          let emptyenv =
            Love_tenv.new_subcontract_env
              CONSTANTS.anonymous
              s.kind env
          in
          let s, newenv = typecheck_struct exp.annot emptyenv s in
          let ctrtyp = Love_tenv.env_to_contract_sig newenv in
          mk_packstruct (Anonymous s), TPackedStructure (Anonymous ctrtyp)
      )
    | Raise { exn; args; loc } ->
      debug "[typecheck_exp] Raise@.";
      let loc' = loc_to_str loc in
      let args_typ =
        match exn with
          Fail t -> [Love_tenv.normalize_type ~relative:true t env]
        | Exception exn ->
          match Love_tenv.find_exception exn env with
            None ->
            type_error loc' env "Undefined exception %a" print_strident exn
            | Some {result; _} ->
                List.map (fun t -> Love_tenv.normalize_type ~relative:true t env) result
      in
      let args, args_typ =
        match args, args_typ with
          [{content = Tuple l;_} as arg], [(TTuple t) as tup] ->
            debug "[typecheck_exp] Tuple %a of type %a@."
              Love_printer.Ast.print_exp arg Love_type.pretty tup;
            l, t
        | [{content = Tuple l;_} as args], t ->
            debug "[typecheck_exp] Arguments %a of types %a@."
              Love_printer.Ast.print_exp args
              (Format.pp_print_list
                 ~pp_sep:(fun fmt _ -> Format.fprintf fmt ", ")
                 Love_type.pretty) t; l, t
        | l, [(TTuple t) as tup] ->
            debug "[typecheck_exp] Arguments %a of tuple type %a@."
              Love_printer.Ast.print_exp (Love_ast_utils.mk_tuple args)
              Love_type.pretty tup;
            l, t
        | _,_ ->
            debug "[typecheck_exp] Arguments %a of tuple type %a@."
              (Format.pp_print_list
                 ~pp_sep:(fun fmt _ -> Format.fprintf fmt ", ")
                 Love_printer.Ast.print_exp) args
              (Format.pp_print_list
                 ~pp_sep:(fun fmt _ -> Format.fprintf fmt ", ")
                 Love_type.pretty) args_typ;
            args, args_typ in
      let alen = List.length args in let tlen = List.length args_typ in
      let () =
        if ((Compare.Int.(<>)) alen tlen)
        then
          type_error loc' env
            "Exception %a expects %i arguments, but it is applied to %i arguments."
            print_exn_name exn
            tlen alen
      in
      let rec check_args acc args args_typ =
        match args, args_typ with
          [], [] -> List.rev acc
        | arg :: tlarg, targ :: tlargt ->
          let arg, t = typecheck_exp env arg in
          check loc' env t targ;
          check_args (arg :: acc) tlarg tlargt
        | _,_ -> assert false
      in
      let args = check_args [] args args_typ in
      let tvar = Love_type.fresh_typevar () in
      mk_raise ?loc exn args, TForall (tvar, TVar (tvar))

    | TryWith { arg; cases } ->
      debug "[typecheck_exp] TryWith@.";
      let arg, typ = typecheck_exp env arg in
      let typecheck_case ((ename, plist), exp) =
        let test_exp env pats =
          let exp, etyp = typecheck_exp env exp in
          if equal_type env typ etyp
          then (ename, pats), exp
          else
            type_error loc env
              "Exception cases must have the same type: %a and %a are incompatible"
              (Love_type.pretty) typ
              (Love_type.pretty) etyp
        in
        let rec check_pat acc_pats acc_env pats args_typ =
          match pats, args_typ with
            [], [] -> test_exp acc_env (List.rev acc_pats)
          | pat :: tlarg, targ :: tlargt ->
            let env = typecheck_pattern loc acc_env targ pat in
            check_pat (pat :: acc_pats) env tlarg tlargt
          | _,_ ->
            type_error loc env "Failure duing pattern typechecking in exception."
        in
        match ename with
          AST.Fail t -> (
            match
              is_safe_res
                ~where:(if Love_pervasives.has_protocol_revision 3 then FunArgument else TopLevel)
                env
                TInternal
                t
            with
              Ok () -> (
                debug "[typecheck_exp] Fail (%a)" (Love_type.pretty) t;
                let t = Love_tenv.normalize_type ~relative:true t env in
                match plist, t with
                  [],_ ->
                  type_error loc env
                    "Failure with no argument is invalid"
                    (Love_type.pretty) t
                | [{content = PTuple plist; _}], TTuple l
                | plist, TTuple l -> check_pat [] env plist l
                | [p], t -> check_pat [] env [p] [t]
                | _,t ->
                  type_error loc env
                    "Failure badly typed by %a" (Love_type.pretty) t
              )
            | Error s ->
              type_error loc env
                "Failure applied the unsafe type %a: %s"
                (Love_type.pretty) t
                s
          )
        | Exception ename ->
          debug "[typecheck_exp] Fail (%a)" Ident.print_strident ename;
          let args_typ =
            match Love_tenv.find_exception ename env with
              None ->
              type_error loc env
                "Undefined exception %a" print_strident ename
            | Some {result; _} ->
                List.map (fun t -> Love_tenv.normalize_type ~relative:true t env) result
          in
          let plist, args_typ =
            match plist, args_typ with
              [{content = PTuple l; _}], [TTuple t] -> l, t
            | [{content = PTuple l; _}], t -> l, t
            | l, [TTuple t] -> l, t
            | _,_ -> plist, args_typ in
          let () =
            let plen = List.length plist in let tlen = List.length args_typ in
            if ((Compare.Int.(<>)) plen tlen)
            then
              type_error loc env
                "Exception in try with %a expects %i arguments,\
                 but it is applied to %i arguments.@."
                print_strident ename
                tlen plen
          in
          check_pat [] env plist args_typ
      in
      let l = List.map typecheck_case cases in
      mk_try_with arg l, typ
  in
  debug "[typecheck_exp] Expression %a : %a@."
    Ast.print_exp exp Love_type.pretty t;
  let t = Love_tenv.normalize_type ~relative:true t env in
  debug "[typecheck_exp] Expression %a with normalized type : %a@."
    Ast.print_exp exp Love_type.pretty t;
  e, t

and typecheck_signature
    loc
    (parent_env : Love_tenv.t)
    (name : String.t)
    (sign : TYPE.structure_sig) =
  debug "[typecheck_signature] Adding signature %s = %a to environment@." name
    Love_type.pp_contract_sig sign
  ;
  let sig_env = Love_tenv.new_subcontract_env name sign.sig_kind parent_env
  in
  let typecheck_sig_content sig_env (name, content) =
    (* to check : non polymorphic storage type,
       safe type definitions and type uses
       no entry points on modules *)
    match content with
    | SType ts -> (
        let storage_test tdef =
          if String.equal name "storage" && not (Love_type.is_module sign)
          then
            match Love_type.typedef_parameters tdef with
              [] -> ()
            | _ -> type_error loc sig_env "Storage type does not expect type arguments"
        in
        match ts with
          SPublic tdef ->
            begin
              match is_safe_tdef TPublic name tdef sig_env
              with
                Error s -> type_error loc sig_env "%s" s
              | Ok () ->
                  storage_test tdef;
                  Love_tenv.add_typedef name TPublic tdef sig_env
            end

        | SPrivate tdef ->
            begin
              match is_safe_tdef TPrivate name tdef sig_env with
                Error s -> type_error loc sig_env "%s" s
              | Ok () ->
                  storage_test tdef;
                  Love_tenv.add_typedef name TPrivate tdef sig_env
            end

        | SAbstract tl -> (
            let ok () = Love_tenv.add_abstract_type name tl sig_env in
            if String.equal name "storage" && not (Love_type.is_module sign)
            then match tl with
                [] -> ok ()
              | _ -> type_error loc sig_env "Unsafe abstract type %s in signature" name
            else ok ()
          )
      )
    | SException l -> (
        match
          is_safe_list
            ~where:TopLevel
            sig_env
            TAbstract
            l
        with
          Ok () -> Love_tenv.add_exception name l sig_env
        | Error s -> type_error loc sig_env "Unsafe exception %s in signature: %s" name s
      )

    | SInit t ->
        if Love_type.is_module sign
        then
          type_error loc sig_env
            "Signature of module cannot contain entry points (%s)"
            name
        else (
          if Love_pervasives.has_protocol_revision 3 then
            begin
          let t = Love_tenv.normalize_type ~relative:true t sig_env in
          match is_safe_res ~where:TopLevel sig_env TPublic t with
            Ok () ->
              let lambda =
                Love_type.initType
                  ~type_storage:(TUser ((Ident.create_id "storage"), []))
                  t
              in
            Love_tenv.add_var ~kind:(Love_tenv.Init Public) name lambda sig_env
          | Error s ->
            type_error loc sig_env "Type %a for initializer %s is unsafe: %s"
              (Love_type.pretty) t
              name
              s
        end
          else
            begin
              match is_safe_res ~where:TopLevel sig_env TPublic t with
                Ok () ->
                  Love_tenv.add_var ~kind:(Love_tenv.Value Public) name t sig_env
              | Error s ->
                  type_error loc sig_env "Type %a for initializer %s is unsafe: %s"
                    (Love_type.pretty) t
                    name
                    s
            end
        )

    | SEntry t -> (
        if Love_type.is_module sign
        then
          type_error loc sig_env
            "Signature of module cannot contain entry points (%s)"
            name
        else (
          match is_safe_res ~where:TopLevel sig_env TAbstract t with
            Ok () ->
            Love_tenv.add_var
              ~kind:Entry
              name
              (Love_type_list.get_type "entrypoint" [t])
              sig_env
          | Error s ->
            type_error loc sig_env "Type %a for entry point %s is unsafe: %s"
              (Love_type.pretty) t
              name
              s
        )
      )
    | SView t -> (
        if Love_type.is_module sign
        then
          type_error loc sig_env "Signature of module cannot contain views (%s)" name
        else (
          match is_safe_res ~where:TopLevel sig_env TAbstract t with
            Ok () ->
            Love_tenv.add_var
              ~kind:(View External)
              name
              (Love_type_list.get_type "view" [t])
              sig_env
          | Error s ->
            type_error loc sig_env
              "Unsafe type %a for view %s: %s"
              (Love_type.pretty) t
              name
              s
        )
      )

    | SValue t -> (
        match is_safe_res ~where:TopLevel sig_env TAbstract t with
          Ok () -> Love_tenv.add_var ~kind:(Value Public) name t sig_env
        | Error s ->
          type_error loc sig_env
            "Unsafe type %a for value %s: %s"
            (Love_type.pretty) t
            name
            s
      )

    | SStructure str -> (
        match str with
          Named n -> (
            debug "[typecheck_signature] Adding structure = %a" Ident.print_strident n;
            match Love_tenv.find_signature n sig_env with
              None ->
              type_error loc sig_env
                "Signature %a for structure %s is undefined" Ident.print_strident n name
            | Some _ -> Love_tenv.add_signed_subcontract name n sig_env
          )
        | Anonymous s ->
          debug "[typecheck_signature] Adding anonymous structure";
          let str_env = typecheck_signature loc sig_env name s in
          Love_tenv.add_subcontract_env name str_env sig_env
      )
    | SSignature s_sig ->
      debug "[typecheck_signature] Adding anonymous structure";
      let sub_sig_env =
        typecheck_signature
          loc
          sig_env
          name
          s_sig
      in Love_tenv.add_signature name sub_sig_env sig_env

  in
  let env =
    List.fold_left
      typecheck_sig_content
      sig_env
      sign.sig_content
  in
  debug "[typecheck_signature] New environment: %a@." Love_tenv.pp_env env;
  env

and typecheck_struct
    (loc : location option)
    (env : Love_tenv.t)
    (ctr : structure) : structure * Love_tenv.t =
  debug "[typecheck_struct] Typechecking a %s@."
    (if Love_ast.is_module ctr then "module" else "contract");
  let loc_def def = loc_to_str loc (Format.asprintf "definition of %s" def) in
  let treat_content name_acc env (name, content) :
    Collections.StringSet.t * content * Love_tenv.t =
    let () =
      if Collections.StringSet.mem name name_acc
      then
        type_error (loc_def name) env "Top level %s is defined twice" name
    in
    let name_acc = Collections.StringSet.add name name_acc in
    match content with
    | DefType (k,t) ->
        begin
          match is_safe_tdef k name t env with
            Ok () ->
              begin
                let tdef, new_env = DefType (k,t), Love_tenv.add_typedef name k t env in
                if String.equal name "storage" && not (Love_ast.is_module ctr)
                then
                  match k, Love_type.typedef_parameters t with
                  | TInternal, _ ->
                      type_error (loc_def name) env "Storage type definition cannot be internal"
                  | _, _ :: _ ->
                      type_error (loc_def name) env "Storage definition cannot be polymorphic"
                  | _, [] -> name_acc, tdef, new_env
                else name_acc, tdef, new_env
              end
          | Error s ->
              type_error (loc_def name) env
                "Definition of type %s = %a is unsafe : %s"
                name (Love_type.pp_typdef ~privacy:(Love_ast.kind_to_string k) ~name) t s
        end

    | DefException tl -> (
        match is_safe_list ~where:TopLevel env TAbstract tl with
          Ok () ->
          name_acc, DefException tl, Love_tenv.add_exception name tl env
        | Error s ->
          type_error (loc_def name) env "Definition of exception %s is unsafe: %s"
            name s
      )

    | Init {init_code; init_typ; init_persist} -> (
        debug "[typecheck_struct] Initializer : %a@."
          Love_type.pretty init_typ;
        let () =
          match is_safe_res ~where:TopLevel env TPublic init_typ with
            Error s -> type_error (loc_def name) env "Definition of initializer is unsafe: %s" s
          | Ok () ->
              if (Love_pervasives.has_protocol_revision 3) ||
                 check_forbidden_type ~where:TopLevel env init_typ
              then ()
              else
                type_error (loc_def name) env
                  "Init %s has type %a, which is forbidden at top level" name
                  Love_type.pretty init_typ
        in
        let init_code, t = typecheck_exp env init_code in
        let env, init_fun_typ =
          let env, type_storage =
            match Love_tenv.get_storage_type env with
              None ->
              type_error
                (loc_def name)
                env
                "Storage must be defined before the definition of init"
            | Some t -> env, t in
          let lambda =
            Love_type.initType
              ~type_storage
              init_typ in
          env, Love_tenv.normalize_type ~relative:true lambda env
        in
        debug "[typecheck_struct] Init : Checking compatibility between %a and %a@."
          Love_type.pretty init_fun_typ Love_type.pretty t;
        check (loc_def name) env init_fun_typ t;
        name_acc,
        Init {init_code; init_typ; init_persist},
        Love_tenv.add_var ~kind:(Init Public) name init_fun_typ env
      )

    | Entry ({ entry_code; entry_fee_code; entry_typ } as e) -> (
        debug "[typecheck_struct] Entry = %a : %a@."
          Ast.print_entry e
          (Love_type.pretty) (entry_typ)
        ;
        let ety = Love_type_list.get_type "entrypoint" [entry_typ] in
        let () =
          if Love_pervasives.has_protocol_revision 3 ||
             check_forbidden_type
               ~where:TopLevel
               env
               ety
          then ()
          else
            type_error (loc_def name) env
              "Error on %s: elements of type %a are forbidden at top-level"
              name
              Love_type.pretty ety in
        match
          is_safe_res
            ~where:TopLevel
            env
            TPublic
            (if Love_pervasives.has_protocol_revision 3 then ety else entry_typ)
        with
          Ok () -> (
            let () =
            if not (Love_pervasives.has_protocol_revision 3)
            then
              let () =
                if Love_ast.is_module ctr ||
                   not (check_forbidden_type env ~where:Parameter entry_typ)
                then
                  type_error (loc_def name) env "Forbidden entry point in module (%s)" name
              in ()
            in
            debug "[typecheck_struct] Entry : starting the typechecking@.";
            let env =
              Love_tenv.add_var
                ~kind:Love_tenv.(Entry)
                name
                ety
                env in
            let env, typ_as_lambda =
              let env, type_storage =
                  match Love_tenv.get_storage_type env with
                    None ->
                    type_error
                      (loc_def name)
                      env
                      "Storage must be defined before the definition of entrypoint %s"
                      name
                    | Some t -> env, t in
              let lambda =
                Love_type.entryPointType
                  ~type_storage
                  entry_typ in
              env, Love_tenv.normalize_type ~relative:true lambda env
            in
            let entry_code, t = typecheck_exp env entry_code in
            debug "[typecheck_struct] Entry (code) : Checking compatibility between %a and %a@."
              Love_type.pretty typ_as_lambda Love_type.pretty t;
            check (loc_def name) env typ_as_lambda t;

            let entry_fee_code = match entry_fee_code with
              | None -> None
              | Some fee_code ->
                match Love_ast_utils.Raw.lambdas_to_params entry_code with
                (* As code has been typechecked, it should be a lambda with 3 arguments *)
                | (p1, t1) :: (p2, t2) :: (p3, t3) :: _ ->
                  let env = typecheck_pattern (loc_def name) env t1 p1 in
                  let env = typecheck_pattern (loc_def name) env t2 p2 in
                  let env = typecheck_pattern (loc_def name) env t3 p3 in
                  let fee_code, t = typecheck_exp env fee_code in
                  let expected_ty =
                    TTuple [
                      Love_type_list.get_type "dun" [];
                      Love_type_list.get_type "nat" [];
                    ] in
                  debug "[typecheck_struct] Entry (fee_code) :\
                         Checking compatibility between %a and %a@."
                    Love_type.pretty expected_ty
                    Love_type.pretty t;
                  check (loc_def name) env expected_ty t;
                  Some fee_code
                | _ -> assert false
            in

            name_acc, Entry ({ entry_code; entry_fee_code; entry_typ }), env
          )
        | Error s ->
          type_error (loc_def name) env "Type %a of entry point %s is unsafe: %s"
            (Love_type.pretty) entry_typ name s
      )

    | View ({ view_code; view_typ; view_recursive } as v) -> (
        let vty = Love_type_list.get_type "view" [view_typ] in
        debug "[typecheck_struct] Analysis of view. Type of view is %a" Love_type.pretty vty;
        let () =
          if Love_pervasives.has_protocol_revision 3 ||
             check_forbidden_type ~where:TopLevel env vty
          then ()
          else
            type_error (loc_def name) env
              "Error on %s: elements of type %a are forbidden at top-level"
              name
              Love_type.pretty vty
        in
        debug "[typecheck_struct] View = %a : %a@."
          Ast.print_view v
          (Love_type.pretty) view_typ;
        match
          is_safe_res
            ~where:TopLevel
            env
            TAbstract
            (if Love_pervasives.has_protocol_revision 3 then
               vty
             else raise (Exceptions.InvariantBroken "Forbidden view"))
        with
          Ok () -> (
            let () =
              if Love_ast.is_module ctr
              then
                type_error (loc_def name) env "Forbidden view in module (%s)" name
            in
            debug "[typecheck_struct] View : starting the typechecking@.";
            let env =
              match view_recursive with
              Rec ->
                Love_tenv.add_var
                  ~kind:(View Local)
                  name
                  vty
                  env
              | NonRec -> env in
            let env, typ_as_lambda =
              let env, type_storage =
                match Love_tenv.get_storage_type env with
                  None ->
                    type_error
                      (loc_def name)
                      env
                      "Storage must be defined before the definition of view %s"
                      name
                | Some t -> env, t in
              let lambda =
                Love_type.viewType ~type_storage view_typ in
              env, Love_tenv.normalize_type ~relative:true lambda env
            in
            let view_code, t = typecheck_exp env view_code in
            let env =
              match view_recursive with
              NonRec ->
                Love_tenv.add_var
                  ~kind:(View Local)
                  name
                  vty
                  env
              | Rec -> env in
            let expected_typ =
              if Love_pervasives.has_protocol_revision 4 then
                t
              else
                let raw_typ = TUser (Ident.create_id "storage", []) @=> view_typ in
                Love_tenv.normalize_type ~relative:true raw_typ env
            in
            debug "[typecheck_struct] View : Checking compatibility between %a and %a@."
              Love_type.pretty typ_as_lambda Love_type.pretty expected_typ;
            check (loc_def name) env typ_as_lambda expected_typ;
            let view_typ =
              if Love_pervasives.has_protocol_revision 4 then
                view_typ
              else
                t
            in
            name_acc, View ({ view_code; view_typ; view_recursive }), env
          )
        | Error s ->
          type_error (loc_def name) env
            "Unsafe parameter type %a for view %s: %s"
            (Love_type.pretty) view_typ
            name
            s
      )

    | Value ({ value_code; value_typ;
               value_visibility; value_recursive } as v) -> (
        debug "[typecheck_struct] Value = %a : %a@."
          Ast.print_value v
          (Love_type.pretty) value_typ
        ;
        let () =
          if Love_pervasives.has_protocol_revision 3 then () else
          if check_forbidden_type ~where:TopLevel env value_typ
          then (
            match value_visibility with
              Public ->
                if Love_tenv.is_internal value_typ env
                then
                  type_error (loc_def name) env "Value %s is public, but its type is internal" name
                else ()
            | Private -> ()
          )
          else
            type_error (loc_def name) env
              "Value %s has type %a, which is forbidden at top level" name
              (Love_type.pretty) value_typ
        in
        match is_safe_res ~where:TopLevel env TInternal value_typ with
          Ok () -> (
            let () =
              if Love_pervasives.has_protocol_revision 3 then
                match value_visibility with
                  Public ->
                    if Love_tenv.is_internal value_typ env
                    then
                      type_error (loc_def name) env
                        "Value %s is public, but its type is internal" name
                    else ()
                | Private -> ()
              else ()
            in
            let env' = match value_recursive with
              | Rec -> Love_tenv.add_var ~kind:(Value (value_visibility))
                         name value_typ env
              | NonRec -> env
            in
            let value_code, t = typecheck_exp env' value_code in
            debug "[typecheck_struct] Value : Checking compatibility between %a and %a@."
              Love_type.pretty value_typ Love_type.pretty t;
            check (loc_def name) env value_typ t;
            name_acc,
            Value { value_code; value_typ; value_visibility; value_recursive },
            Love_tenv.add_var ~kind:(Value (value_visibility)) name value_typ env
          )
        | Error s ->
          type_error (loc_def name) env "Type %a of value %s is unsafe: %s"
            (Love_type.pretty) value_typ
            name
            s
      )

    | Structure c ->
        debug "[typecheck_struct] Struct = %a@." Ast.print_structure c;
        let sub_env = Love_tenv.new_subcontract_env name c.kind env in
        let c, sub_env = typecheck_struct loc sub_env c in
        name_acc, Structure c, Love_tenv.add_subcontract_env name sub_env env

    | Signature s ->
        name_acc,
        Signature s,
      Love_tenv.add_signature name (typecheck_signature (loc_def name) env name s) env
  in
  let _, rev_content, new_env =
    List.fold_left
      (fun (string_set, acc_content, acc_env) ((name, _) as c) ->
         debug "[typecheck_struct] Typechecking %s@." name;
         try let sset, c, e = treat_content string_set acc_env c in
           (sset, (name, c) :: acc_content, e)
         with
           TypingError (s, loc, env) ->
             debug
               "[typecheck_struct] Typing error, failing with environment %a"
               Love_tenv.pp_env env;
             raise (TypingError (s, loc, env))
         | Love_tenv.EnvironmentError s ->
             debug
               "[typecheck_struct] Environment error with environment %a"
               Love_tenv.pp_env env;
             raise (TypingError (s, (loc_def name), env))
         | e ->
             debug
               "[typecheck_struct] Generic error, failing with environment %a"
               Love_tenv.pp_env acc_env;
             raise e
      )
      (Collections.StringSet.empty, [], env)
      ctr.structure_content in
  let () =
    if Love_ast.is_module ctr
    then ()
    else
      match Love_tenv.get_storage_type new_env with
        None -> Log.print "Warning : Contract defined with no storage.@."
      | Some t -> (
          match is_safe_res ~where:TopLevel new_env TAbstract t with
            Ok () -> ()
          | Error s -> type_error "storage definition" env "Unsafe storage type: %s" s
        )
  in {
    structure_content = List.rev rev_content;
    kind = ctr.kind
  }, new_env

let typecheck_top_contract
    loc
    (env : Love_tenv.t)
    ({ version; code } : top_contract) : top_contract * Love_tenv.t =
  let code, env = typecheck_struct loc env code in
  { version; code }, env

let extract_deps
    (loc : location option)
    (ctxt : Love_context.t)
    (deps : (string * string) list)
  : Love_context.t Error_monad.tzresult Lwt.t
  =
  let loc m kt = loc_to_str loc (Format.asprintf "dependency %s = %s" m kt) in
  List.fold_left
    (fun ctxt (mname, kt1) ->
       ctxt >>=?
       (fun ctxt ->
          debug "[extract_deps] Dep %s@." mname;
          let b58 = Contract_repr.of_b58check kt1 in
          match b58 with
          | Error _ ->
              type_error_lwt ctxt "Invalid contract hash %s" kt1
          | Ok kt1' ->
              let env = ctxt.type_env in
              debug "[extract_deps] Contract found@.";
              (Love_context.get_code ctxt kt1') >>=?
              (function
                  _, None ->
                    type_error_lwt ctxt "Contract %s not found" kt1
                | ctxt, Some (CodeLove { root_struct = code; version = _}) ->
                    debug "[extract_deps] Found@.";
                    let ls_sig =
                      Love_translator.sig_of_structure
                        ~only_typedefs:false
                        code
                    in
                    debug "[extract_deps] Signature generated@.";
                    let ls_env =
                      Love_tenv.contract_sig_to_env (Some mname) ls_sig ctxt.type_env in
                    let type_env = Love_tenv.add_subcontract_env mname ls_env env in
                    return (Love_context.set_type_env ctxt type_env)
                | ctxt, Some (CodeMich code) ->
                    begin
                      match Love_michelson.michelson_contract_as_sig ctxt.actxt code with
                        Ok (actxt, sg) ->
                          let ls_env = Love_tenv.contract_sig_to_env (Some mname) sg env in
                          let ctxt = Love_context.set_type_env ctxt ls_env in
                          return {ctxt with actxt}
                      | Error _ ->
                          type_error
                            (loc mname kt1) env "Invalid michelson contract %s" kt1
                    end
              )
       )
    )
    (return ctxt)
    deps

let typecheck_top_contract_deps
    loc
    ctxt
    (({code; _} as c) : top_contract)
  : (Love_context.t * (top_contract * Love_tenv.t)) Error_monad.tzresult Lwt.t =
  let open Error_monad in
  debug "[typecheck_top_contract_deps] Starting to add dependencies@.";
  let deps_name =
    match code.kind with
      Module -> []
    | Contract l -> l
  in
  debug "[typecheck_top_contract_deps] Dependencies\n%a@."
    (Format.pp_print_list (fun fmt (n,k) -> Format.fprintf fmt "%s = %s" n k)) deps_name
  ;
  let ctxt : Love_context.t Error_monad.tzresult Lwt.t =
    extract_deps loc ctxt deps_name in
  ctxt >>=?
  (fun ctxt -> return @@ (ctxt, typecheck_top_contract loc ctxt.type_env c))

(* Typechecking of the Runtime AST *)
(* All this duplication is necessary just because the contract parameter,
   which may contain a lambda, must be typechecked against the expected
   contract parameter type *)

let typecheck_rconst _env (c : Love_runtime_ast.const) : TYPE.t =
  match c with
  | RCUnit        -> Love_type_list.get_type "unit" []
  | RCBool _      -> Love_type_list.get_type "bool" []
  | RCString _    -> Love_type_list.get_type "string" []
  | RCBytes _     -> Love_type_list.get_type "bytes" []
  | RCInt _       -> Love_type_list.get_type "int" []
  | RCNat _       -> Love_type_list.get_type "nat" []
  | RCDun _       -> Love_type_list.get_type "dun" []
  | RCKey _       -> Love_type_list.get_type "key" []
  | RCKeyHash _   -> Love_type_list.get_type "keyhash" []
  | RCSignature _ -> Love_type_list.get_type "signature" []
  | RCAddress _   -> Love_type_list.get_type "address" []
  | RCTimestamp _ -> Love_type_list.get_type "timestamp" []

let pt_error loc env p str =
  type_error loc env "Error while typechecking runtime pattern %a: %s"
    Love_printer.Runtime.print_rpattern p
    str

let rec typecheck_rpattern loc t pattern req : pattern_check_request =
  let opt_res =
    match pattern with
    | Love_runtime_ast.RPAny -> tpc_pany loc t req
    | RPConst c -> tpc_pconst typecheck_rconst loc t c req
    | RPVar v -> tpc_pvar loc t v req
    | RPAlias (pat, v) -> tpc_palias typecheck_rpattern loc t v pat req
    | RPList [] -> tpc_pnil None t req
    | RPList pl -> tpc_plist typecheck_rpattern loc t pl req
    | RPTuple pl -> tpc_ptuple typecheck_rpattern loc t pl req
    | RPConstr (c, plist) -> tpc_pconstr typecheck_rpattern loc t c plist req
    | RPContract (name, ctr) -> tpc_pcontract loc t name ctr req
    | RPOr l -> tpc_por typecheck_rpattern loc t l req
    | RPRecord l            -> tpc_precord typecheck_rpattern loc t l req
  in
  match opt_res with
    Ok res -> res
  | Error s -> pt_error loc req.env pattern s

let typecheck_rpattern env t pat =
  (typecheck_rpattern
    (Format.asprintf "%a" Love_printer.Runtime.print_rpattern pat)
    t
    pat
    {env; new_vars = StringSet.empty}).env

let rec typecheck_rexp
    tvar_map
    (env : Love_tenv.t)
    (exp : Love_runtime_ast.exp) : typ =
  let loc = Format.asprintf "%a" Love_printer.Runtime.print_rexp exp in
  let t =
    match exp with
    | RConst c -> typecheck_rconst env c

    | RVar v -> tcp_evar loc v ANone env

    | RVarWithArg (v, xa) -> tcp_evar loc v xa env

    | RLet { bnd_pattern; bnd_val; body} ->
      let t = typecheck_rexp tvar_map env bnd_val in
      let env' = typecheck_rpattern env t bnd_pattern in
      typecheck_rexp tvar_map env' body

    | RLetRec { bnd_var; bnd_val; body; val_typ } ->
        begin
          match
            is_safe_res
              ~where:(if Love_pervasives.has_protocol_revision 3 then FunArgument else TopLevel)
              env TInternal val_typ with
            Ok () ->
              let env' = Love_tenv.add_var bnd_var val_typ env in
              let t' = typecheck_rexp tvar_map env' bnd_val in
              check loc env val_typ t';
              typecheck_rexp tvar_map env' body
          | Error s ->
              type_error loc env
                "Type %a is unsafe : %s"
                Love_type.pretty val_typ s
        end
    | RLambda { args = []; body } -> typecheck_rexp tvar_map env body

    | RLambda { args = (RPattern (arg_pattern, arg_typ)) :: args; body } ->
        begin
          let arg_typ =
            match arg_typ with
              TVar tv ->
                begin
                debug "[typecheck_rexp] RLambda with an unknown type";
                match TVMap.find_opt tv tvar_map with
                  None ->
                    debug "[typecheck_rexp] Type %a not found" Love_type.pp_typvar tv; arg_typ
                | Some t ->
                    debug "[typecheck_rexp] Type found: %a" Love_type.pretty t;
                    t
              end
            | _ -> arg_typ in
          match
            is_safe_res
              ~where:(if Love_pervasives.has_protocol_revision 3 then FunArgument else TopLevel)
              env TInternal arg_typ with
            Ok () ->
              let env' = typecheck_rpattern env arg_typ arg_pattern in
              TArrow (arg_typ, typecheck_rexp tvar_map env' (RLambda { args; body }))
          | Error s ->
              type_error loc env "Type of argument %a is not safe"
                (Love_type.pretty) arg_typ s
    end

    | RLambda { args = (RTypeVar tv) :: args; body } ->
        let env' = Love_tenv.add_forall tv env in
        TForall (tv, typecheck_rexp tvar_map env' (RLambda { args; body }))

    | RApply { exp; args } ->
      let typ = typecheck_rexp tvar_map env exp in
      let rec check_arrow ftype = function
        | [] -> ftype
        | (Love_runtime_ast.RExp e) :: al ->
          begin match ftype with
            | TArrow (t1, t2) ->
              check loc env t1 (typecheck_rexp tvar_map env e);
              check_arrow t2 al
            | _ ->
                type_error loc env "Arguments applied to a non-function value"
          end
        | (RType typ) :: al ->
            match is_safe_res ~where:TopLevel env TInternal typ with
              Ok () ->
                begin
                  match ftype with
                  | TForall (tvar, t) ->
                      check_arrow
                        (Love_type.replace (fun x -> Love_tenv.isComp x env) tvar typ t) al
                  | TUser (n, tl) ->
                      let rec replace_first_arg = function
                        | [] -> type_error loc env "TApply on a non quantified TUser type."
                        | TVar _tv :: tl -> typ :: tl
                        | t :: tl -> t :: replace_first_arg tl
                      in
                      check_arrow (TUser (n, replace_first_arg tl)) al
                  | _ -> type_error loc env "TApply on a non quantified type."
                end
            | Error s -> type_error loc env "TApply on an unsafe type : %s" s
      in
      check_arrow typ args

    | RSeq el ->
      let rec check_seq = function
        | [] -> raise (TypingError (
            "Empty sequences are forbidden", loc, env))
        | [e] -> typecheck_rexp tvar_map env e
        | e :: el ->
            check
              loc
              env
              (typecheck_rexp tvar_map env e)
            (Love_type_list.get_type "unit" []);
          check_seq el
      in
      check_seq el

    | RIf { cond; ifthen; ifelse } ->
      let ct = typecheck_rexp tvar_map env cond in
      check loc ~error:"Condition is not boolean" env ct
        (Love_type_list.get_type "bool" []);
      let tt = typecheck_rexp tvar_map env ifthen in
      let et = typecheck_rexp tvar_map env ifelse in
      (try check loc env tt et; tt
       with TypingError _ -> check loc env et tt; et)

    | RMatch { arg; cases } ->
      let t = typecheck_rexp tvar_map env arg in
      let treat_case env (pat, exp) =
        let env' = typecheck_rpattern env t pat in
        typecheck_rexp tvar_map env' exp
      in
      let hd_case, other_cases =
        match cases with
        | [] -> raise (TypingError ("Empty pattern matching", loc, env))
        | hd :: tl -> hd, tl in
      let hd_typ = treat_case env hd_case in
      let tref = ref hd_typ in
      List.iter (fun case ->
          let t = treat_case env case in
          check loc env !tref t;
          tref := t
        ) other_cases;
      check loc env hd_typ !tref;
      hd_typ

    | RConstructor { type_name; constr; ctyps; args } -> (
        let constr = Ident.change_last type_name (Ident.create_id constr) in
        let arg_typs =
          List.map
            (fun exp ->
               typecheck_rexp tvar_map env exp
            )
            args
        in
        (* AST transformation : if the constructor expects 1 argument
           and args has more or equal  than 2 arguments, then args is
           converted to a tuple *)
        debug "[typecheck_exp] Searching type of constructor %a@."
          Ident.print_strident constr;
        let ctyp = Love_tenv.find_constr constr env in
        match ctyp with
        | None ->
          type_error loc env "Constructor %a is unknown" Ident.print_strident constr
        | Some {result = ct; _} ->
          let targs =
            let c_init_params =
              match ct.cparent with
                TUser (_, l) -> l
              | _ -> raise (Exceptions.InvariantBroken "Registered parent must be a TUser")
            in
            let replace_map =
              try
                List.fold_left2
                  (fun acc old new_t ->
                     match old with
                       TVar tv ->
                       debug "[typecheck_exp] Replacing %a by %a@."
                         Love_type.pp_typvar tv
                         (Love_type.pretty) new_t
                       ;
                       TypeVarMap.add tv new_t acc
                     | _ -> raise (
                         Exceptions.InvariantBroken
                           "Registered parent must be a TUser with only tvars as parameters")
                  )
                  TypeVarMap.empty
                  c_init_params
                  ctyps
              with
                Invalid_argument _ ->
                type_error loc env
                  "Constructor %a expects %i type arguments, but %i are provided."
                  Ident.print_strident constr
                  (List.length c_init_params)
                  (List.length ctyps)
            in
            List.map
              (Love_type_utils.replace_map (fun x -> Love_tenv.isComp x env) replace_map)
              ct.cargs in
          let targs =
            match targs, arg_typs with
            | [TTuple _], [TTuple _] -> targs
            | [TTuple l], _ -> l
            | t,_ -> t in
          let () =
            try List.iter2 (check loc env) targs arg_typs
            with Invalid_argument _ ->
              type_error loc env
                "Constructor %a expects %i arguments, but only %i are provided."
                Ident.print_strident constr
                (List.length targs)
                (List.length args)
          in
          let args_typ =
            if Compare.Int.(List.length ct.Love_tenv.cargs = 1 &&
                            List.length ctyps >= 2)
            then (
              [ TTuple ctyps ]
            )
            else ctyps in

          let constr_typ = match ct.cparent with
              TUser (name, _) ->
              (* Additional check : is this constructor public, private, abstract, internal ?*)
              check_private loc name env;
              TUser (name, args_typ)
            (* Check done *)
            | _ -> assert false
          in
          constr_typ
      )

    | RNil -> Love_type.type_empty_list

    | RList (el, e) ->
      begin match el with
        | [] -> raise (TypingError ("Badly formed list", loc, env))
        | _ ->
            let lt = typecheck_rexp tvar_map env e in
            let et = match Love_type.isListOf lt with
              | None -> raise (TypingError (
                  "Right side of list is not a list.", loc, env))
              | Some t -> t
            in
            List.iter (fun elt ->
                let t = typecheck_rexp tvar_map env elt in
                if equal_type env t et then ()
                else raise (TypingError (
                    "Elements of list must all have the same type",
                    loc, env))
              ) el;
            lt
      end

  | RTuple ([] | [_]) -> raise (TypingError ("Ill-formed tuple", loc, env))

  | RTuple el  -> TTuple (List.map (typecheck_rexp tvar_map env) el)

  | RProject { tuple; indexes } ->
      begin match typecheck_rexp tvar_map env tuple with
        | TTuple l ->
            let tl = List.map (fun i ->
              match List.nth_opt l i with
              | Some t -> t
              | _ -> raise (TypingError (
                  "Projection out of bounds", loc, env))
            ) indexes
            in
            begin match tl with
              | [t] -> t
              | tl -> TTuple tl
            end
        | _ -> raise (TypingError (
            "Projection on a non tuple element", loc, env))
      end

  | RUpdate { tuple; updates } ->
      let tup_type = typecheck_rexp tvar_map env tuple in
      let tup_types, tup_length =
        match tup_type with
        | TTuple l -> l, List.length l
        | _ -> raise (TypingError ("Update on a non tuple element", loc, env))
      in
      let rec check_equal acc l =
        match l with
        | [] -> acc
        | (i, e) :: tl ->
          if Compare.Int.(i < tup_length)
          then check_equal (IntMap.add i (typecheck_rexp tvar_map env e) acc) tl
          else raise (TypingError ("No update on update field", loc, env))
      in
      let update_types = check_equal IntMap.empty updates in
      List.iteri (fun i tup_type ->
          match IntMap.find_opt i update_types with
          | None -> ()
          | Some t ->
              check loc
                ~error:"Type of update is inconsistent with tuple type."
                env t tup_type
        ) tup_types;
      tup_type

  | RRecord { contents = []; _ } ->
      raise (TypingError ("Empty record are forbidden", loc, env))

  | RRecord { contents = l; _ }  ->
      let ftyp = List.map (fun (name, exp) ->
        (name, typecheck_rexp tvar_map env exp)) l in
      Love_tenv.record_type None ftyp env

  | RGetField { record; field } ->
      begin match typecheck_rexp tvar_map env record with
        | TUser (name, args) ->
          let rec_def =
            match Love_tenv.find_record name env with
            | Some t -> t.result
            | None -> raise (TypingError (
                "Record type not found in env", loc, env)) in
          let ftyp_opt = List.find_opt (fun tf ->
              String.equal tf.Love_tenv.fname field) rec_def in
          let ftyp = match ftyp_opt with
            | Some t -> t
            | None -> raise (UnknownField field) in
          let ftyp_args = Love_tenv.field_with_targs ftyp args env in
          ftyp_args.ftyp
        | _ -> raise (TypingError (
            "Record has not the type record", loc, env))
      end

  | RSetFields { updates = [];_ } -> (* Bad case *)
      raise (Exceptions.InvariantBroken "Expression SetFields has no updates")

  | RSetFields { record; updates } ->
      let typ_rec = typecheck_rexp tvar_map env record in
      let rec_def = match typ_rec with
        | TUser (name, _args) ->
            begin match Love_tenv.find_record name env with
              | Some t -> t.result
              | None -> raise (TypingError (
                  "Record type not found in env", loc, env))
            end
        | _ -> raise (TypingError (
            "Record has not the type record", loc, env))
      in
      let treat_field (field, updt) =
        let ftyp_opt = List.find_opt (fun tf ->
            String.equal tf.Love_tenv.fname field) rec_def in
        let ftyp =
          match ftyp_opt with
          | Some t -> t
          | None -> raise (TypingError (
              spf "Field %s not registered in environment" field, loc, env))
        in
        check loc ~error:(
          spf "Field %s not belonging to the correct record" field)
          env ftyp.fparent typ_rec;
        let t_updt = typecheck_rexp tvar_map env updt in
        check ~error:(spf "Badly typed field %s" field) loc env t_updt ftyp.ftyp
      in
      List.iter treat_field updates;
      typ_rec

  | RPackStruct sr ->
      begin match sr with
        | RNamed n ->
          begin match Love_tenv.find_contract n env with
            | Some { result = local_env; _ } ->
                TPackedStructure (
                  Anonymous (Love_tenv.env_to_contract_sig local_env))
            | None ->
                raise (TypingError (
                  Format.asprintf "Contract %a not found in environment"
                    print_strident n, loc, env))
          end
        | RAnonymous s ->
            let emptyenv = Love_tenv.new_subcontract_env
                CONSTANTS.anonymous s.kind env in
            let newenv = typecheck_rstruct emptyenv s in
            TPackedStructure (Anonymous (
                Love_tenv.env_to_contract_sig newenv))
      end

  | RRaise { exn; args; loc=raise_loc } ->
      let args_typ = match exn with
        | RFail t -> [t]
        | RException exn ->
            match Love_tenv.find_exception exn env with
              | None ->
                  raise (TypingError (
                    Format.asprintf "Undefined exception %a"
                      print_strident exn, loc_to_str raise_loc loc, env))
              | Some { result; _ } -> result
      in
      let args, args_typ = match args, args_typ with
          [RTuple l], [TTuple t] -> l, t
        | [RTuple l], t -> l, t
        | l, [TTuple t] -> l, t
        | _, _ -> args, args_typ in
      let alen = List.length args in let tlen = List.length args_typ in
      let () =
        if Compare.Int.(alen <> tlen)
        then raise (TypingError (
            Format.asprintf "Bad number of arguments for exception %a"
              (Love_runtime_ast.print_exn_name ) exn, loc, env))
      in
      let rec check_args args args_typ =
        match args, args_typ with
        | [], [] -> ()
        | arg :: tlarg, targ :: tlargt ->
            check loc env (typecheck_rexp tvar_map env arg) targ;
            check_args tlarg tlargt
        | _,_ -> assert false
      in
      check_args args args_typ;
      let tvar = Love_type.fresh_typevar () in
      TForall (tvar, TVar (tvar))

  | RTryWith { arg; cases } ->
      let typ = typecheck_rexp tvar_map env arg in
      let typecheck_case ((ename, plist), exp) : unit =
        let test_exp env =
          if equal_type env typ (typecheck_rexp tvar_map env exp) then ()
          else raise (TypingError (
              "Exception cases must have the same type", loc, env))
        in
        let rec check_pat acc_env pats args_typ =
          match pats, args_typ with
          | [], [] -> test_exp acc_env
          | pat :: tlarg, targ :: tlargt ->
              check_pat (typecheck_rpattern acc_env targ pat) tlarg tlargt
          | _, _ -> raise (TypingError (
              "Failure during pattern typechecking in exception.", loc, env))
        in
        match ename with
        | Love_runtime_ast.RFail t ->
            begin match plist, t with
                [], _ -> raise (TypingError (
                  "Failure with no argument must have type Unit", loc, env))
              | [Love_runtime_ast.RPTuple plist], TTuple l
              | plist, TTuple l -> check_pat env plist l
              | [p], t -> check_pat env [p] [t]
              | _, _ -> raise (TypingError (
                  "Exception pattern badly typed", loc, env))
            end
        | RException ename ->
            let args_typ = match Love_tenv.find_exception ename env with
              | None -> raise (TypingError (
                  Format.asprintf "Undefined exception %a"
                    print_strident ename, loc, env))
              | Some { result; _ } -> result
            in
            let plist, args_typ = match plist, args_typ with
                [RPTuple l], [TTuple t] -> l, t
              | [RPTuple l], t -> l, t
              | l, [TTuple t] -> l, t
              | _,_ -> plist, args_typ in
            let () =
              let plen = List.length plist in
              let tlen = List.length args_typ in
              if Compare.Int.(plen <> tlen)
              then raise (TypingError (
                  Format.asprintf
                    "Bad number of arguments for exception %a in try with"
                    print_strident ename, loc, env))
            in
            check_pat env plist args_typ
      in
      List.iter typecheck_case cases;
      typ
  in Love_tenv.normalize_type ~relative:true t env

and typecheck_rstruct (env : Love_tenv.t)
    (ctr : Love_runtime_ast.structure) : Love_tenv.t =
  let treat_content name_acc env (name, content) :
    Collections.StringSet.t * Love_tenv.t =
    let () =
      if Collections.StringSet.mem name name_acc
      then raise (TypingError (name ^ " is defined twice", "second definition of " ^ name, env)) in
    let name_acc = Collections.StringSet.add name name_acc in
    match content with
    | Love_runtime_ast.RDefType (k, t) ->
        begin
          match is_safe_tdef k name t env with
            Ok () ->
              begin
                let new_env = Love_tenv.add_typedef name k t env in
                if String.equal name "storage" && not (Love_runtime_ast.is_module ctr)
                then match k, Love_type.typedef_parameters t with
                  | TInternal, _ -> raise (TypingError (
                      "Storage type definition cannot be internal", "storage definition", env))
                  | _, _ :: _ -> raise (TypingError (
                      "Storage definition cannot be polymorphic", "storage definition", env) )
                  | _, [] -> name_acc, new_env
                else name_acc, new_env
              end
          | Error s ->
              type_error ("definition of " ^ name) env "Definition of %a is unsafe: %s"
                (Love_type.pp_typdef ~privacy:(Love_ast.kind_to_string k) ~name) t
                s
      end
    | RDefException tl ->
        begin
          match is_safe_list ~where:TopLevel env TInternal tl with
            Ok () -> name_acc, Love_tenv.add_exception name tl env
          | Error s ->
              type_error
                ("definition of exception " ^ name)
                env
                "Definition of exception is unsafe: %s" s
        end
    | RInit {init_code; init_typ; init_persist} ->
        debug "[typecheck_struct] Initializer : %a@."
          Love_type.pretty init_typ;
        let () =
          match is_safe_res ~where:TopLevel env TPublic init_typ with
            Error s ->
              type_error
                "definition of initializer" env "Definition of initializer is unsafe: %s" s
          | Ok () ->
              if Love_pervasives.has_protocol_revision 3 ||
                 check_forbidden_type ~where:TopLevel env init_typ
              then ()
              else
                type_error "definition of initializer" env
                  "Init %s has type %a, which is forbidden at top level" name
                  Love_type.pretty init_typ
        in
        let t = typecheck_rexp TVMap.empty env init_code in
        let env, init_fun_typ =
          let env, type_storage =
            match Love_tenv.get_storage_type env with
              None ->
              let unit = Love_type_list.get_type "unit" [] in
              Love_tenv.add_typedef
                "storage"
                TPublic
                (Alias {aparams = []; atype = unit})
                env,
              unit
            | Some t -> env, t in
          let lambda =
            Love_type.initType
              ~type_storage
              init_typ in
          env, Love_tenv.normalize_type ~relative:true lambda env
        in
        debug "[typecheck_struct] Init : Checking compatibility between %a and %a@."
          Love_type.pretty init_fun_typ Love_type.pretty t;
        check "definition of initializer" env init_fun_typ t;
        let visibility = if init_persist then Public else Private in
        name_acc,
        Love_tenv.add_var ~kind:(Value visibility) name init_fun_typ env

    | REntry { entry_code; entry_fee_code; entry_typ } ->
        begin
          let ety = Love_type_list.get_type "entrypoint" [entry_typ] in
          match
            is_safe_res
              ~where:TopLevel
              env
              TPublic
              (if Love_pervasives.has_protocol_revision 3 then ety else entry_typ)
          with
            Ok () ->
              begin
                let () =
                  if (Love_runtime_ast.is_module ctr) ||
                     (not (Love_pervasives.has_protocol_revision 3) ||
                      not (check_forbidden_type ~where:Parameter env entry_typ))
                  then raise (TypingError (
                      spf "Forbidden entry point in module %s" name, "definition of "^ name, env))
                in
                let env =
                  Love_tenv.add_var
                    ~kind:Love_tenv.Entry
                    name
                    (Love_type_list.get_type "entrypoint" [entry_typ])
                    env
                in
                let env, typ_as_lambda =
                  let env, type_storage =
                    match Love_tenv.get_storage_type env with
                    | None ->
                        let unit = Love_type_list.get_type "unit" [] in
                        Love_tenv.add_typedef
                          "storage"
                          TPublic
                          (Alias {aparams = []; atype = unit})
                          env,
                        unit
                    | Some t -> env, t in
                  let lambda = Love_type.entryPointType ~type_storage entry_typ in
                  env, Love_tenv.normalize_type ~relative:true lambda env
                in
                let t = typecheck_rexp TVMap.empty env entry_code in
                check ("definition of entrypoint " ^name) env typ_as_lambda t;
                let () = match entry_fee_code with
                  | None -> ()
                  | Some fee_code ->
                      match Love_runtime_ast.lambdas_to_params entry_code with
                      | RPattern (p1, t1) :: RPattern (p2, t2) ::
                        RPattern (p3, t3) :: _ ->
                          let env = typecheck_rpattern env t1 p1 in
                          let env = typecheck_rpattern env t2 p2 in
                          let env = typecheck_rpattern env t3 p3 in
                          let t = typecheck_rexp TVMap.empty env fee_code in
                          let expected_ty =
                            TTuple [
                              Love_type_list.get_type "dun" [];
                              Love_type_list.get_type "nat" [];
                            ]
                          in
                          check ("definition of entrypoint " ^ name) env expected_ty t
                      | _ -> assert false
                in
                name_acc, env
              end
          | Error s ->
              type_error ("definition of entrypoint"^name) env "Entry point type is unsafe: %s" s
        end

    | RView { view_code; view_typ; view_recursive } ->
        begin
        match is_safe_list ~where:TopLevel env TAbstract [view_typ] with
        | Ok () ->
            begin
              let () =
                if Love_runtime_ast.is_module ctr
                then
                  type_error ("definition of view " ^name) env "Forbidden view in module %s" name
              in
              let env =
                match view_recursive with
                  Rec ->
                    Love_tenv.add_var
                      ~kind:Love_tenv.(View Local)
                      name
                      (Love_type_list.get_type "view" [view_typ]) env
                | NonRec -> env in
              let env, typ_as_lambda =
                let env, type_storage =
                  match Love_tenv.get_storage_type env with
                  | None ->
                      let unit =
                        Love_type_list.get_type "unit" []
                      in
                      Love_tenv.add_typedef
                        "storage"
                        TPublic
                        (Alias {aparams = []; atype = unit}) env,
                      unit
                  | Some t -> env, t in
                let lambda =
                  Love_type.viewType ~type_storage view_typ in
                env, Love_tenv.normalize_type ~relative:true lambda env
              in
              let t = typecheck_rexp TVMap.empty env view_code in
              let env =
                match view_recursive with
                  NonRec ->
                    Love_tenv.add_var
                      ~kind:Love_tenv.(View Local)
                      name
                      (Love_type_list.get_type "view" [view_typ]) env
                | Rec -> env in
              check ("definition of view "^name) env typ_as_lambda t;
              name_acc, env
            end
        | Error s -> type_error ("definition of view " ^name) env "View type is unsafe: %s" s
      end

    | RValue { value_code; value_typ; value_visibility; value_recursive = _ } ->
        begin
          let msg = "definition of value " ^ name in
          let () = match value_visibility with
            | Public ->
                if Love_tenv.is_internal value_typ env
                then raise (TypingError (
                    "Value " ^ name ^ " is public, but its type is internal",
                    msg, env))
            | Private -> ()
          in
          match is_safe_res ~where:TopLevel env TInternal value_typ with
            Ok () ->
              let t = typecheck_rexp TVMap.empty env value_code in
              check msg env value_typ t;
              name_acc,
              Love_tenv.add_var ~kind:(Value (value_visibility)) name value_typ env
          | Error s ->
              type_error msg env "Value type %a is unsafe: s"
                Love_type.pretty value_typ s
      end

    | RStructure c ->
        let sub_env = Love_tenv.new_subcontract_env name c.kind env in
        let sub_env = typecheck_rstruct sub_env c in
        name_acc, Love_tenv.add_subcontract_env name sub_env env

    | RSignature s ->
        name_acc,
        Love_tenv.add_signature
          name
          (typecheck_signature ("definition of signature " ^ name) env name s) env
  in
  let _, new_env =
    List.fold_left (
      fun (string_set, acc_env) c ->
        treat_content string_set acc_env c
    ) (Collections.StringSet.empty, env) ctr.structure_content in
  let () =
    if Love_runtime_ast.is_module ctr then ()
    else match Love_tenv.get_storage_type new_env with
    | None -> ()
    | Some t ->
        match is_safe_res ~where:TopLevel new_env TInternal t with
          Ok () -> ()
        | Error s -> type_error "definition of storage" env "Unsafe storage type: %s" s
  in
  new_env

let check_lwt unify ctxt t t' =
  match t' with
    TVar tv -> (
      match TypeVarMap.find_opt tv unify with
        None ->
          debug "[check_lwt] Type var %a is unknown, adding correspondance %a <=> %a@."
            Love_type.pp_typvar tv
            Love_type.pretty t
            Love_type.pretty t';
          return (TypeVarMap.add tv t unify, ctxt)
      | Some (TVar t) when Love_type.equal_type_var t tv ->
          debug "[check_lwt] Typevar %a is known, but it is unified with itself."
            Love_type.pp_typvar tv;
          raise (Exceptions.InvariantBroken
                   "Error during unification in typecheck value : typevar unified with itself")
      | Some t' ->
          debug "[check_lwt] Typevar %a has been unified to to %a."
            Love_type.pp_typvar tv Love_type.pretty t';
          check "" ctxt.Love_context.type_env t t';
          return (unify, ctxt)
    )
  | _ ->
      debug "[check_lwt] Checking %a <= %a" Love_type.pretty t Love_type.pretty t';
      check "" ctxt.type_env t t';
      return (unify, ctxt)

let rec typecheck_value_list
    (ctxt : Love_context.t)
    (unify : TYPE.t TypeVarMap.t)
    (typl : typ list)
    (vs : Love_value.Value.t list) : (TYPE.t TypeVarMap.t * Love_context.t)
    Error_monad.tzresult Lwt.t =
  let rec simplify_args ctxt unify (tlist : typ list) args :
    (TYPE.t TypeVarMap.t * Love_context.t) Error_monad.tzresult Lwt.t =
    match args, tlist with
      arg :: arg_tl, typ :: typ_tl -> (
        typecheck_value ctxt unify typ arg >>=?
        (fun (unify, ctx) -> simplify_args ctx unify typ_tl arg_tl)
      )
    | [],[] -> return (unify, ctxt)
    | hd :: _, [] ->
        type_error_lwt
          ctxt
          "Typecheck value fail : Argument %a has no associated type" Love_printer.Value.print hd
    | [], hd :: _ ->
        type_error_lwt
          ctxt
          "Typecheck value fail : Type %a has no associated argument" Love_type.pretty hd
  in
  simplify_args ctxt unify typl vs

and typecheck_value
    (ctxt : Love_context.t)
    (unify : TYPE.t TypeVarMap.t)
    (typ : TYPE.t)
    (v : Love_value.Value.t) : (TYPE.t TypeVarMap.t * Love_context.t) Error_monad.tzresult Lwt.t =
  let open Love_value in
  let open Love_context in
  debug
    "[typecheck_value] check_lwting value %a with type %a@."
    Love_printer.Value.print v
    Love_type.pretty typ;
  let typ = Love_tenv.normalize_type ~relative:true typ ctxt.type_env in
  debug
    "[typecheck_value] Normalized type of value %a: %a@."
    Love_printer.Value.print v
    Love_type.pretty typ;
  match v with
  | VUnit ->
    debug "[typecheck_value] Unit@.";
    check_lwt unify ctxt (Love_type_list.get_type "unit" []) typ
  | VBool _b ->
    debug "[typecheck_value] Bool@.";
    check_lwt unify ctxt (Love_type_list.get_type "bool" []) typ
  | VString _s ->
    debug "[typecheck_value] String@.";
    check_lwt unify ctxt (Love_type_list.get_type "string" []) typ
  | VBytes _b ->
    debug "[typecheck_value] Bytes@.";
    check_lwt unify ctxt (Love_type_list.get_type "bytes" []) typ
  | VInt _i ->
    debug "[typecheck_value] Int@.";
    check_lwt unify ctxt (Love_type_list.get_type "int" []) typ
  | VNat _i ->
    debug "[typecheck_value] Nat@.";
    check_lwt unify ctxt (Love_type_list.get_type "nat" []) typ
  | VTuple vl -> (
      debug "[typecheck_value] Tuple@.";
      match typ with
        TTuple l -> typecheck_value_list ctxt unify l vl
      | TVar tv -> (
          match TypeVarMap.find_opt tv unify with
            None -> (
              debug "[typecheck_value] Unknown type, trying to guess it";
              let fake_t_list = List.map (fun _ -> TVar (Love_type.fresh_typevar ())) vl in
              typecheck_value_list ctxt unify fake_t_list vl >>=?
              (fun (unify, ctxt) ->
                 let guessed_tuple =
                   Love_type_utils.replace_map
                     (fun x -> Love_tenv.isComp x ctxt.type_env)
                     unify
                     (TTuple fake_t_list) in
                 debug "[typecheck_value] Tuple %a type is %a"
                   Love_printer.Value.print v
                   Love_type.pretty guessed_tuple;
                 let unify = TypeVarMap.add tv guessed_tuple unify in
                 return (unify, ctxt)
              )
            )
          | Some  (TVar t) when Love_type.equal_type_var t tv ->
            raise (Exceptions.InvariantBroken
                     "Error during tuple unification in typecheck value : \
                      typevar unified with itself")
          | Some (TTuple l) -> typecheck_value_list ctxt unify l vl
          | Some t ->
              type_error_lwt ctxt
                "Value %a is typed (after unification) as %a, while it should be typed as a tuple"
                Love_printer.Value.print v Love_type.pretty t
        )
      | t -> type_error_lwt ctxt
               "Value %a is typed as %a, while it should be typed as a tuple"
               Love_printer.Value.print v Love_type.pretty t
    )
  | VConstr (cname, args) -> (
      debug "[typecheck_value] Constructor : %s@." cname;
      match typ with
        | TUser (tname, params) ->
          debug "[typecheck_value] Constructor type : %a@." Ident.print_strident tname;
            begin match Love_tenv.find_sum tname ctxt.type_env with
              | None -> raise (UnknownType tname)
              | Some {result; _} ->
                  let tc_opt = List.find_opt (fun tc ->
                      String.equal cname tc.Love_tenv.cname
                    ) result
                  in
                  begin match tc_opt with
                    | None -> raise (UnknownConstr (Ident.create_id cname))
                    | Some tc ->
                        if Love_pervasives.has_protocol_revision 4 then
                          typecheck_value_list ctxt unify tc.cargs args
                        else
                          typecheck_value_list ctxt unify params args
                  end
            end
        | t ->
            type_error_lwt ctxt
              "Value %a is typed as %a : user type expected"
              Love_printer.Value.print v Love_type.pretty t
    )

  | VRecord fields ->
      debug "[typecheck_value] Record@.";
      let rec check_lwt_fields ctxt unify fl rfl =
        match fl, rfl with
        | (fn, v) :: fl, Love_tenv.{ fname; ftyp; fparent; _ } :: rfl ->
            if not (String.equal fn fname) then
              type_error_lwt ctxt
                "Record field %s does not appear in the record definition of %a (%s expected)."
                fn
                Love_type.pretty fparent
                fname
            else
              typecheck_value ctxt unify ftyp v >>=? fun (unify, ctxt) ->
              check_lwt_fields ctxt unify fl rfl
        | [], [] ->
            return (unify, ctxt)
        | (hd, _) :: _, [] ->
            type_error_lwt ctxt
              "Record value %a defines field %s, which is not in the definition of type %a"
              Love_printer.Value.print v
              hd
              Love_type.pretty typ
        | [], Love_tenv.{ fname; fparent; _ } :: _ ->
            type_error_lwt ctxt
              "Record value %a does not defines field %s, which is in the definition of type %a"
              Love_printer.Value.print v
              fname
              Love_type.pretty fparent
      in
      begin match typ with
        | TUser (tname, params) ->
            begin match Love_tenv.find_record tname ctxt.type_env with
              | None -> raise (UnknownType tname)
              | Some {result; _} ->
                  let fields =
                    List.fast_sort (fun (f1, _) (f2, _) ->
                        String.compare f1 f2) fields in
                  let rfields =
                    List.fast_sort
                      (fun Love_tenv.{ fname = f1; _ } Love_tenv.{ fname = f2; _ } ->
                         String.compare f1 f2) result in
                  let new_unify =
                    let poly_params =
                      let tparent = (List.hd rfields).fparent in
                      match tparent with
                        TUser (_, poly_params) -> poly_params
                      | _ -> raise (Exceptions.InvariantBroken
                                      "Record type registered as a non TUser type")
                    in
                    let replace_map =
                    try
                      List.fold_left2
                        (fun acc poly_param param ->
                           match poly_param with
                             TVar tv -> TVMap.add tv param acc
                           | _ ->
                               raise (
                                 Exceptions.InvariantBroken
                                   "Type from type_env should only have polymorphic arguments")
                        )
                        TVMap.empty
                        poly_params
                        params
                    with Invalid_argument _ ->
                      raise (
                        Exceptions.InvariantBroken (
                          Format.asprintf
                            "%a registered with %i arguments, \
                             but it has been used with %i arguments"
                          Ident.print_strident tname
                          (List.length poly_params)
                          (List.length params)
                        ))
                    in
                    TVMap.merge
                      (fun _ b1 b2 ->
                         match b1, b2 with
                           None, None -> None
                         | Some t, None | None, Some t -> Some t
                         | Some tunify, Some _ -> Some tunify
                      )
                      unify
                      replace_map
                  in
                  check_lwt_fields ctxt new_unify fields rfields
            end
        | t -> begin (* Error *)
            match fields with
              [] -> raise (Exceptions.InvariantBroken "Empty record during value typechecking")
            | (f, _) :: _ ->
                match Love_tenv.find_field f ctxt.type_env with
                  None ->
                    type_error_lwt ctxt
                      "Field %s does not belong to a defined record. \
                       Also, a value of type %a was expected, which is not a record."
                      f Love_type.pretty t
                | Some {result = {fparent; _}; _} ->
                    type_error_lwt ctxt
                      "Value %a has type %a, but a value of type %a was expected."
                      Love_printer.Value.print v Love_type.pretty fparent Love_type.pretty t
            end
      end
  | VList vlist -> (
      debug "[typecheck_value] List@.";
      match vlist with
        [] -> begin
          match typ with
            TUser (LName "list", _) -> return (unify, ctxt)
          | TVar tv -> (
              match TypeVarMap.find_opt tv unify with
                None -> (
                  let t_list = TVar (Love_type.fresh_typevar ()) in
                  return (
                    TypeVarMap.add
                      tv
                      (Love_type_list.get_type "list" [t_list])
                      unify,
                    ctxt)
                )
              | Some  (TVar t) when Love_type.equal_type_var t tv ->
                  raise (Exceptions.InvariantBroken
                           "Error during empty list unification in typecheck value : \
                            typevar unified with itself")
              | Some (TUser (LName "list", _)) -> return (unify, ctxt)
              | Some t ->
                  type_error_lwt ctxt
                    "The empty list cannot have (unified) type %a" Love_type.pretty t
            )
          | t ->
              type_error_lwt ctxt
                "The empty list cannot have type %a" Love_type.pretty t
        end
      | l -> begin
          match typ with
            TUser (LName "list", [t]) ->
            List.fold_left
              (fun lwt elt ->
                 lwt >>=? (fun (unify, ctxt) -> typecheck_value ctxt unify t elt))
              (return (unify, ctxt))
              l
          | TVar tv -> (
              match TypeVarMap.find_opt tv unify with
                None -> (
                  let tv' = TVar (Love_type.fresh_typevar ()) in
                  let t_list = List.map (fun _ -> TVar tv) l in
                  typecheck_value_list ctxt unify t_list l >>=?
                  (fun (unify, ctxt) ->
                     let new_tlist =
                       Love_type_utils.replace_map
                         (fun x -> Love_tenv.isComp x ctxt.type_env)
                         unify
                         (Love_type_list.get_type "list" [tv']) in
                     return (TypeVarMap.add tv new_tlist unify, ctxt)
                  )
                )
              | Some  (TVar t) when Love_type.equal_type_var t tv ->
                raise (Exceptions.InvariantBroken
                         "Error during list unification in typecheck value : \
                          typevar unified with itself")
              | Some (TUser (LName "list", [t])) ->
                List.fold_left
                  (fun lwt elt ->
                     lwt >>=? (fun (unify, ctxt) -> typecheck_value ctxt unify t elt))
                  (return (unify, ctxt))
                  l
              | Some t ->
                  type_error_lwt ctxt
                    "List %a cannot have (unified) type %a"
                    Love_printer.Value.print v Love_type.pretty t
            )
          | t ->
              type_error_lwt ctxt
                "List %a cannot have type %a"
                Love_printer.Value.print v Love_type.pretty t
        end
    )
  | VSet vset -> (
      debug "[typecheck_value] Set@.";
      if ValueSet.is_empty vset
      then (
        match typ with
        | TUser (LName "set", _) -> return (unify, ctxt)
        | TVar tv -> (
            match TypeVarMap.find_opt tv unify with
              None -> (
                let t' =
                  Love_type_list.get_type
                    "set"
                    [TVar (Love_type.fresh_typevar ())]
                in
                return (TypeVarMap.add tv t' unify, ctxt)
              )
            | Some (TVar t) when Love_type.equal_type_var t tv ->
              raise (Exceptions.InvariantBroken
                       "Error during empty set unification in typecheck value : \
                        typevar unified with itself")
            | Some (TUser (LName "set", _)) -> return (unify, ctxt)
            | Some t ->
                type_error_lwt ctxt "Empty set cannot have (unified) type %a"
                  Love_type.pretty t
          )
        | t ->
            type_error_lwt ctxt "Empty set cannot have type %a"
              Love_type.pretty t
    ) else (
        match typ with
        | TUser (LName "set", [t]) ->
          ValueSet.fold
            (fun elt lwt -> lwt >>=? (fun (unify, ctxt) ->
                 typecheck_value ctxt unify t elt))
            vset
            (return (unify, ctxt))
        | TVar tv -> (
            match TypeVarMap.find_opt tv unify with
              None -> (
                let tmp_t = TVar (Love_type.fresh_typevar ()) in
                ValueSet.fold
                  (fun elt lwt -> lwt >>=? (fun (unify, ctxt) ->
                       typecheck_value ctxt unify tmp_t elt))
                  vset
                  (return (unify, ctxt)) >>=? (
                  fun (unify, ctxt) ->
                    let new_tset =
                      Love_type_list.get_type
                        "set"
                        [Love_type_utils.replace_map
                           (fun x -> Love_tenv.isComp x ctxt.type_env) unify tmp_t
                        ]
                    in return (TypeVarMap.add tv new_tset unify, ctxt)
                )
              )
            | Some (TVar t) when Love_type.equal_type_var t tv ->
              raise (Exceptions.InvariantBroken
                       "Error during set unification in typecheck value : \
                        typevar unified with itself")
            | Some (TUser (LName "set", [t])) -> (
                ValueSet.fold
                  (fun elt lwt -> lwt >>=? (fun (unify, ctxt) ->
                       typecheck_value ctxt unify t elt))
                  vset
                  (return (unify, ctxt))
              )
            | Some t ->
                type_error_lwt ctxt "Set %a cannot have (unified) type %a"
                  Love_printer.Value.print v
                  Love_type.pretty t
          )
        | t ->
            type_error_lwt ctxt "Set %a cannot have type %a"
              Love_printer.Value.print v
              Love_type.pretty t
      )
    )
  | VMap map -> (
      debug "[typecheck_value] Map@.";
      if ValueMap.is_empty map
      then
        match typ with
        | TUser (LName "map", _) -> return (unify, ctxt)
        | TVar tv -> (
            match TypeVarMap.find_opt tv unify with
              None -> (
                let t' =
                  Love_type_list.get_type
                    "map"
                    [
                      TVar (Love_type.fresh_typevar ());
                      TVar (Love_type.fresh_typevar ())
                    ]
                in
                return (TypeVarMap.add tv t' unify, ctxt)
              )
            | Some (TVar t) when Love_type.equal_type_var t tv ->
              raise (Exceptions.InvariantBroken
                       "Error during empty map unification in typecheck value : \
                        typevar unified with itself")
            | Some (TUser (LName "map", _)) -> return (unify, ctxt)
            | Some t ->
                type_error_lwt ctxt "Empty map cannot have (aliased) type %a" Love_type.pretty t
          )
        | t -> type_error_lwt ctxt "Empty map cannot have type %a" Love_type.pretty t
      else
        match typ with
        | TUser (LName "map", [tk; tb]) ->
          ValueMap.fold
            (fun key bnd lwt ->
               lwt >>=? (
                 fun (unify, ctxt) ->
                   typecheck_value ctxt unify tk key >>=? (
                     fun (unify, ctxt) -> typecheck_value ctxt unify tb bnd
                   )
               )
            )
            map
            (return (unify, ctxt))
        | TVar tv -> (
            match TypeVarMap.find_opt tv unify with
              None -> (
                let tk = TVar (Love_type.fresh_typevar ()) in
                let tb = TVar (Love_type.fresh_typevar ()) in
                ValueMap.fold
                  (fun key bnd lwt ->
                     lwt >>=? (
                       fun (unify, ctxt) ->
                         typecheck_value ctxt unify tk key >>=? (
                           fun (unify, ctxt) -> typecheck_value ctxt unify tb bnd
                         )
                     )
                  )
                  map
                  (return (unify, ctxt)) >>=?
                (fun (unify, ctxt) ->
                   let real_map_type =
                     Love_type_utils.replace_map
                       (fun x -> Love_tenv.isComp x ctxt.type_env)
                       unify
                       (Love_type_list.get_type "map" [tk; tb])
                   in
                   return (TypeVarMap.add tv real_map_type unify, ctxt)
                )
              )
            | Some (TVar t) when Love_type.equal_type_var t tv ->
              raise (Exceptions.InvariantBroken
                       "Error during map unification in typecheck value : \
                        typevar unified with itself")
            | Some (TUser (LName "map", [tk; tb])) -> (
                ValueMap.fold
                  (fun key bnd lwt ->
                     lwt >>=? (
                       fun (unify, ctxt) ->
                         typecheck_value ctxt unify tk key >>=? (
                           fun (unify, ctxt) -> typecheck_value ctxt unify tb bnd
                         )
                     )
                  )
                  map
                  (return (unify, ctxt))
              )
            | Some t ->
                type_error_lwt ctxt
                  "Map %a cannot have (aliased) type %a"
                  Love_printer.Value.print v Love_type.pretty t
          )
        | t -> type_error_lwt ctxt
                 "Map %a cannot have type %a"
                 Love_printer.Value.print v Love_type.pretty t
    )

  | VBigMap { diff; key_type; value_type; _ } ->  (
      debug "[typecheck_value] BigMap@.";
      check_lwt
        unify
        ctxt
        (Love_type_list.get_type "bigmap" [key_type; value_type]) typ >>=?
      fun (unify, ctxt) ->
      ValueMap.fold
        (fun key bnd lwt ->
           lwt >>=? (
             fun (unify, ctxt) ->
               typecheck_value ctxt unify key_type key >>=? (
                 fun (unify, ctxt) ->
                   match bnd with
                   | None -> return (unify, ctxt)
                   | Some bnd -> typecheck_value ctxt unify value_type bnd
               )
           )
        )
        diff
        (return (unify, ctxt))
    )

  | VDun _d ->
    debug "[typecheck_value] Dun@.";
    check_lwt unify ctxt (Love_type_list.get_type "dun" []) typ
  | VKey _k ->
    debug "[typecheck_value] Key@.";
    check_lwt unify ctxt (Love_type_list.get_type "key" []) typ
  | VKeyHash _kh ->
    debug "[typecheck_value] KeyHash@.";
    check_lwt unify ctxt (Love_type_list.get_type "keyhash" []) typ
  | VSignature _s ->
    debug "[typecheck_value] Signature@.";
    check_lwt unify ctxt (Love_type_list.get_type "signature" []) typ


  | VContractInstance (c, a) ->
    debug "[typecheck_value] ContractInstance@.";
    check_lwt
      unify
      ctxt
      (TContractInstance (Anonymous c))
      typ >>=?
    (fun (unify, ctxt) ->
       Love_context.get_code ctxt a >>=?
       (fun (ctxt, opt) ->
          begin
            match opt with
            | None -> type_error_lwt ctxt "Contract %a not found" Contract_repr.pp a
            | Some (Love_context.CodeLove { root_struct = code; version = _}) ->
                return (ctxt, Love_translator.sig_of_structure ~only_typedefs:false code)
            | Some (Love_context.CodeMich code) ->
                begin
                  match Love_michelson.michelson_contract_as_sig ctxt.actxt code with
                    Ok (actxt, sg) -> return ({ctxt with actxt}, sg)
                  | Error l ->
                      type_error_lwt ctxt
                        "Error while translating michelson to love: %a"
                        (Format.pp_print_list
                           ~pp_sep:(fun fmt _ -> Format.fprintf fmt "; ")
                           (fun fmt (e : error) -> Format.fprintf fmt "%a" Error_monad.pp e)
                        )
                        l
                end
          end
          >>=? fun (ctxt, sg) ->
          check_lwt
            unify
            ctxt
            (TContractInstance (Anonymous sg))
            typ
       )
    )

  | VPackedStructure (_p, c) ->
    debug "[typecheck_value] ContractStructure@.";
    check_lwt unify ctxt (TPackedStructure (Anonymous (
        Love_translator.sig_of_structure ~only_typedefs:false c.root_struct))) typ

  | VAddress _a ->
    debug "[typecheck_value] Address@.";
    check_lwt unify ctxt (Love_type_list.get_type "address" []) typ
  | VTimestamp _t ->
    debug "[typecheck_value] Timestamp@.";
    check_lwt unify ctxt (Love_type_list.get_type "timestamp" []) typ
  | VOperation _o ->
    check_lwt unify ctxt (Love_type_list.get_type "operation" []) typ
  | VEntryPoint (_crepr, name) -> (*forbidden in storage/param -> never typed*)
      type_error_lwt ctxt "Invalid entry point %s: entry points are forbidden as values." name

  | VView (_crepr, name) -> (* forbidden in storage/param -> never typed*)
      type_error_lwt ctxt "Invalid view %s: views are forbidden as values." name

  | VClosure { call_env = { values = []; structs = []; sigs = [];
                            exns = []; types = []; tvars}; lambda } ->
      begin
        debug "[typecheck_value] Closure of expected type %a@." Love_type.pretty typ;
        let closure_unify =
          List.fold_left
            (fun acc (tvname, ty) ->
               debug "[typecheck_value] Correspondance between %s and %a"
                 tvname Love_type.pretty ty;
               TVMap.add {tv_name = tvname; tv_traits = Love_type.default_trait} ty acc
            )
            TVMap.empty
            tvars in
        match typ with
        | TArrow _ ->
            let rtyp = typecheck_rexp closure_unify ctxt.type_env (RLambda lambda) in
            check_lwt unify ctxt rtyp typ
        | _ ->
            type_error_lwt ctxt
              "Closure %a should be a function type, but it has type %a"
              Love_printer.Value.print v Love_type.pretty typ
      end

  | VClosure { call_env = { values = _; structs = _; sigs = _;
                            exns = _; types = _; tvars = _ }; lambda = _ } ->
      debug "[typecheck_value] Non empty cloture@.";
      type_error_lwt ctxt
        "Closure %a is not pure"
        Love_printer.Value.print v

  | VPrimitive (p, xl, vtl) ->
      debug "[typecheck_value] Love_primitive %s@." (Love_primitive.name p);
      typecheck_primitive ctxt (p, xl) vtl >>=?
      (fun (ctxt, t) -> check_lwt unify ctxt t typ)

(** Returns the new constant primitive, its type and the new arguments and their
    respective types *)
and typecheck_primitive ctxt (p, al)
    (args : Love_value.Value.val_or_type list) =
  let args = List.rev args in
  debug "[typecheck_primitive] Love_primitive %s applied to (%a)@."
    (Love_primitive.name p)
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
       Love_printer.Value.print_val_or_type) args;
  let primitive_type = Love_primitive.type_of (p, al) in
  let advance_in_arrow arrow args ctxt =
    let rec __advance_in_arrow arrow args acc =
      match args with
        [] -> acc >>=? (fun (prim_unify, ctxt) ->
          let ty =
            Love_type_utils.replace_map
              (fun x -> Love_tenv.isComp x ctxt.Love_context.type_env)
              prim_unify
              arrow
          in
          debug "[typecheck_primitive] Final type: %a@." Love_type.pretty ty;
          return (ctxt, ty))
      | (Love_value.Value.V arg) :: tl -> (
          match arrow with
            TArrow (t1, t2) ->
              __advance_in_arrow
                t2
                tl
                (acc >>=? (fun (prim_unify, ctxt) ->
                     debug "[typecheck_primitive] Value argument %a@." Love_printer.Value.print arg;
                     typecheck_value ctxt prim_unify t1 arg))
          | t ->
              type_error_lwt
                ctxt
                "Value %a cannot be applied to value of type %a."
                Love_printer.Value.print arg
                Love_type.pretty t
        )
      | (Love_value.Value.T arg) :: tl ->
          match arrow with
            TForall (tvar, t) ->
              __advance_in_arrow
                t
                tl
                (acc >>=? fun (prim_unify, ctxt) ->
                 debug "[typecheck_primitive] Type argument %a@." Love_type.pretty arg;
                 check_lwt prim_unify ctxt arg (TVar tvar))
          | t ->
              type_error_lwt
                ctxt
                "Type %a cannot be applied to value of type %a."
                Love_type.pretty arg
                Love_type.pretty t
    in
    __advance_in_arrow arrow args (return (TVMap.empty, ctxt)) in
  advance_in_arrow primitive_type args ctxt

let typecheck_value
    (ctxt : Love_context.t)
    (typ_exp : TYPE.type_exp)
    (v : Love_value.Value.t) =
  debug "[typecheck_value] Starting with environment %a@.\
         Checking if value %a is compatible with type %a@."
    Love_tenv.pp_env ctxt.type_env
    Love_printer.Value.print v
    Love_type.pretty typ_exp.body
  ;
  let env =
    List.fold_left
      (fun acc (id, (tdef : TYPE.typedef)) ->
         debug "[typecheck_value] Adding %a = %a in environment@."
           Ident.print_strident id
           (Love_type.pp_typdef ~name:"" ~privacy:"") tdef;
         Love_tenv.add_typedef_in_subcontract id TPublic tdef acc)
      ctxt.type_env
      typ_exp.lettypes
  in
  debug "[typecheck_value] Typechecking value@.";
  try
    typecheck_value {ctxt with type_env = env} TypeVarMap.empty typ_exp.body v >>=? (
      fun (_, ctxt) -> return ctxt
    )
  with
    exc ->
    debug "[typecheck_value] Error : failing with environment %a@."
      Love_tenv.pp_env env;
    raise exc
