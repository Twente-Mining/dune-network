(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
open Script_interpreter
open Script

type error += Unsupported_language of string
type error += Invalid_language_argument

let () =
  let open Data_encoding in
  (* Reject *)
  register_error_kind
    `Permanent
    ~id:"dune_script_interpreter.unsupported_language"
    ~title: "The specified language is not supported"
    ~description: "The language is not supported on this network"
    (obj1 (req "lang" string))
    (function Unsupported_language s -> Some s | _ -> None)
    (fun s -> Unsupported_language s);
  (* Reject *)
  register_error_kind
    `Permanent
    ~id:"dune_script_interpreter.invalid_language_argument"
    ~title: "Invalid language argument for the requested operation"
    ~description: "The requested operation can't be performed on the\
                   specified language or language combination"
    (unit)
    (function Invalid_language_argument -> Some () | _ -> None)
    (fun () -> Invalid_language_argument)

let module_of_code ctxt code =
  let module Lang = (val (Script_dune.module_of_code code) : Script_dune.S) in
  if (String.equal Config.config.network "Mainnet") &&
     not (Dune_storage.has_protocol_revision ctxt 3) then
    fail (Unsupported_language Lang.lang_str)
  else
    return (Dune_script_interpreter_registration.get_module Lang.lang)

let module_of_const ctxt const =
  let module Lang = (val (Script_dune.module_of_const const) : Script_dune.S) in
  if (String.equal Config.config.network "Mainnet") &&
     not (Dune_storage.has_protocol_revision ctxt 3) then
    fail (Unsupported_language Lang.lang_str)
  else
    return (Dune_script_interpreter_registration.get_module Lang.lang)

let normalize_script ctxt self script =
  force_decode ctxt script.code >>=? fun (code, ctxt) ->
  force_decode ctxt script.storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      normalize_script ctxt { code; storage } >>=? fun (script, ctxt) ->
      return (Script_michelson.to_canonical_script script, ctxt)
  | Dune_code code, Dune_expr storage ->
      module_of_code ctxt code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      let open Lang in
      begin match code, storage with
        | Repr.Code code, Repr.Const storage ->
            Interpreter.normalize_script ctxt self code storage
            >>=? fun ((code, storage, fee_code), ctxt) ->
            let script = Script.{
                code = Script.lazy_expr (Dune_code (Repr.Code code));
                storage = Script.lazy_expr (Dune_expr (Repr.Const storage));
                fee_code = Option.map
                    ~f:(fun c -> Script.lazy_expr
                           (Dune_code (Repr.Code c))) fee_code } in
            return (script, ctxt)
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument

let denormalize_script ctxt { code; storage; fee_code } =
  match fee_code with
  | None -> return ({ code; storage }, ctxt)
  | Some fee_code ->
      force_decode ctxt code >>=? fun (code, ctxt) ->
      force_decode ctxt storage >>=? fun (storage, ctxt) ->
      force_decode ctxt fee_code >>=? fun (fee_code, ctxt) ->
      match code, storage, fee_code with
      | Michelson_expr code, Michelson_expr storage, Michelson_expr fee_code ->
          let open Script_ir_translator in
          denormalize_script ctxt { code; storage; fee_code = Some fee_code }
          >>=? fun (script, ctxt) ->
          return (Script_michelson.to_script script, ctxt)
      | Dune_code code, Dune_expr storage, Dune_code fee_code ->
          module_of_code ctxt code >>=? fun m ->
          let module Lang = (val m : Dune_script_interpreter_registration.S) in
          let open Lang in
          begin match code, storage, fee_code with
            | Repr.Code code, Repr.Const storage, Repr.Code fee_code ->
                Interpreter.denormalize_script ctxt code storage (Some fee_code)
                >>=? fun ((code, storage), ctxt) ->
                let script = Script.{
                    code = Script.lazy_expr (Dune_code (Repr.Code code));
                    storage = Script.lazy_expr
                        (Dune_expr (Repr.Const storage)) } in
                return (script, ctxt)
            | _ -> fail Invalid_language_argument
          end
      | _ -> fail Invalid_language_argument

let readable_storage ctxt ~code ~storage =
  force_decode ctxt code >>=? fun (code, ctxt) ->
  force_decode ctxt storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      parse_script ctxt ~legacy:false { code; storage }
      >>=? fun (Ex_full_script script, ctxt) ->
      unparse_script ctxt Readable script >>=? fun (script, _ctxt) ->
      return (Michelson_expr script.storage)
  | Dune_code _code, Dune_expr _storage ->
      return storage
  | _ -> fail Invalid_language_argument

let readable_script ctxt script =
  denormalize_script ctxt script >>=? fun (script, ctxt) ->
  force_decode ctxt script.code >>=? fun (code, ctxt) ->
  force_decode ctxt script.storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      parse_script ctxt ~legacy:false { code; storage }
      >>=? fun (Ex_full_script script, ctxt) ->
      unparse_script ctxt Readable script >>=? fun (script, _ctxt) ->
      return (Script_michelson.to_script script)
  | Dune_code _code, Dune_expr _storage ->
      return script
  | _ -> fail Invalid_language_argument

let typecheck_code ctxt = function
  | Michelson_expr expr ->
      Script_ir_translator.typecheck_code ctxt expr
  | Dune_code code ->
      module_of_code ctxt code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      begin match code with
        | Lang.Repr.Code code ->
            Lang.Interpreter.typecheck_code ctxt code >>=? fun ctxt ->
            return ([], ctxt)
        | _ -> fail Invalid_language_argument
      end
  | Dune_expr _ -> fail Invalid_language_argument

let typecheck_data ctxt = function
  | Michelson_expr data, Michelson_expr ty ->
      Script_ir_translator.typecheck_data ctxt (data, ty)
  | Dune_expr data, Dune_expr type_expr ->
      module_of_const ctxt data >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      begin match data, type_expr with
        | Lang.Repr.Const data, Lang.Repr.Const type_expr ->
            Lang.Interpreter.typecheck_data ctxt data type_expr
        | _ -> fail Invalid_language_argument
      end
  | Michelson_expr data, Dune_expr type_expr ->
      begin
        module_of_const ctxt type_expr >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        Lang.Interpreter.from_michelson_const ctxt data
        >>=? fun (ctxt, data) ->
        match type_expr with
        | Lang.Repr.Const ty ->
            Lang.Interpreter.typecheck_data ctxt data ty
        | _ -> fail Invalid_language_argument
      end
  | Dune_expr data, Michelson_expr ty ->
      begin
        module_of_const ctxt data >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        match data with
        | Lang.Repr.Const data ->
            Lang.Interpreter.to_michelson_const ctxt data
            >>=? fun (ctxt, data) ->
            Script_ir_translator.typecheck_data ctxt (data, ty)
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument

let typecheck ?(type_only=false) ctxt step_constants script =
  Script.force_decode ctxt script.storage
  >>=? fun (storage, ctxt) -> (* see [note] *)
  Lwt.return (Gas.consume ctxt (Script.deserialized_cost storage))
  >>=? fun ctxt ->
  Script.force_decode ctxt script.code >>=? fun (code, ctxt) -> (* see [note] *)
  Lwt.return (Gas.consume ctxt (Script.deserialized_cost code)) >>=? fun ctxt ->
  match code with
  | Michelson_expr code ->
      let l = match storage with
        | Michelson_expr stor -> return (ctxt, stor)
        | Dune_expr s ->
            begin
              module_of_const ctxt s >>=? fun m ->
              let module Lang =
                (val m : Dune_script_interpreter_registration.S) in
              match s with
              | Lang.Repr.Const data ->
                  Lang.Interpreter.to_michelson_const ctxt data
              | _ -> fail Invalid_language_argument
            end
        | _ -> fail Invalid_language_argument
      in
      l >>=? fun (ctxt, storage) ->
      Script_ir_translator.parse_script ctxt ~legacy:false { code; storage }
      >>=? fun (Ex_full_script parsed_script, ctxt) ->
      let to_update = Script_ir_translator.no_big_map_id in
      begin
        if type_only then
          Script_ir_translator.extract_big_map_diff ctxt Optimized
            parsed_script.storage_type parsed_script.storage
            ~to_duplicate:Script_ir_translator.no_big_map_id
            ~to_update ~temporary:false
        else
          Script_ir_translator.collect_big_maps ctxt
            parsed_script.storage_type parsed_script.storage
          >>=? fun (to_duplicate, ctxt) ->
          Script_ir_translator.extract_big_map_diff ctxt Optimized
            parsed_script.storage_type parsed_script.storage
            ~to_duplicate ~to_update ~temporary:false
      end
      >>=? fun (storage, big_map_diff, ctxt) ->
      Script_ir_translator.unparse_data ctxt Optimized
        parsed_script.storage_type storage >>=? fun (storage, ctxt) ->
      let storage = Micheline.strip_locations storage in
      Script_ir_translator.normalize_script ctxt { code; storage }
      >>=? fun (script, ctxt) ->
      return ((Script_michelson.to_canonical_script script, big_map_diff), ctxt)
  | Dune_code code ->
      module_of_code ctxt code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      let open Lang in
      let l = match storage with
        | Dune_expr s ->
            begin
              match s with
              | Repr.Const s -> return (ctxt, s)
              | _ -> fail Invalid_language_argument
            end
        | Michelson_expr s ->
            Lang.Interpreter.from_michelson_const ctxt s
        | _ -> fail Invalid_language_argument
      in
      l >>=? fun (ctxt, storage) ->
      begin match code with
        | Repr.Code code ->
            Interpreter.typecheck ctxt step_constants ~code ~storage
            >>=? fun ((code, storage, fee_code), big_map_diff, ctxt) ->
            return (({ code = Script.lazy_expr (Dune_code (Repr.Code code));
                       storage = Script.lazy_expr
                           (Dune_expr (Repr.Const storage));
                       fee_code =
                         Option.map ~f:(fun c -> Script.lazy_expr
                                           (Dune_code (Repr.Code c))) fee_code
                     }, big_map_diff), ctxt)
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument

let execute ctxt mode step_constants ~entrypoint ~code ~storage
    ~parameter ~apply_manager_operation_content =
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      begin
        let parameter = match parameter with
          | Michelson_expr parameter -> return (ctxt, parameter)
          | Dune_expr e ->
              begin
                module_of_const ctxt e >>=? fun m ->
                let module Lang =
                  (val m : Dune_script_interpreter_registration.S) in
                match e with
                | Lang.Repr.Const data ->
                    Lang.Interpreter.to_michelson_const ctxt data
                | _ -> fail Invalid_language_argument
              end
          | _ -> fail Invalid_language_argument
        in
        parameter >>=? fun (ctxt, parameter) ->
        Script_interpreter.execute ctxt mode step_constants
          ~code ~storage ~entrypoint ~parameter
      end
  | Dune_code code, Dune_expr storage ->
      begin
        module_of_code ctxt code >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        let open Lang in
        let parameter = match parameter with
          | Dune_expr e ->
              begin
                match e with
                | Repr.Const e -> return (ctxt, e)
                | _ -> fail Invalid_language_argument
              end
          | Michelson_expr e ->
              Lang.Interpreter.from_michelson_const ctxt e
          | _ -> fail Invalid_language_argument
        in
        parameter >>=? fun (ctxt, parameter) ->
        match code, storage with
        | Repr.Code code, Repr.Const storage ->
            Interpreter.execute ctxt mode step_constants
              ~code ~storage ~entrypoint ~parameter
              ~apply:(fun ctxt ->
                  apply_manager_operation_content ctxt mode
                    ~payer:step_constants.payer
                    ~source:step_constants.source
                    ~chain_id:step_constants.chain_id
                    ~internal:true
                )
            >>=? fun { ctxt; storage; big_map_diff; result; operations  } ->
            let result = match result with
              | None -> None
              | Some expr -> Some (Dune_expr (Repr.Const expr))
            in
            let storage = Dune_expr (Repr.Const storage) in
            return { ctxt; result; storage; big_map_diff; operations }
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument

let trace ctxt mode step_constants ~entrypoint ~code ~storage ~parameter =
  match code, storage, parameter with
  | Michelson_expr code, Michelson_expr storage, Michelson_expr parameter ->
      Script_interpreter.trace ctxt mode step_constants
        ~code ~storage ~entrypoint ~parameter
  | Dune_code code, Dune_expr storage, Dune_expr parameter ->
      module_of_code ctxt code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      let open Lang in
      begin
        match code, storage, parameter with
        | Repr.Code code, Repr.Const storage, Repr.Const parameter ->
            Interpreter.execute ctxt mode step_constants
              ~code ~storage ~entrypoint ~parameter
              ~apply:(fun _ctxt -> assert false)
            >>=? fun Interpreter.{ ctxt; storage; big_map_diff;
                                   result; operations } ->
            let result = match result with
              | None -> None
              | Some expr -> Some (Dune_expr (Repr.Const expr))
            in
            let storage = Dune_expr (Repr.Const storage) in
            return ({ ctxt; result; storage; big_map_diff; operations }, [])
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument

let execute_fee_script ctxt step_constants ~entrypoint
    ~fee_code ~storage ~parameter =
  match fee_code, storage with
  | Michelson_expr fee_code, Michelson_expr storage ->
      begin
        let param = match parameter with
          | Michelson_expr parameter -> return (ctxt, parameter)
          | Dune_expr parameter ->
              begin
                module_of_const ctxt parameter >>=? fun m ->
                let module Lang =
                  (val m : Dune_script_interpreter_registration.S) in
                match parameter with
                | Lang.Repr.Const data ->
                    Lang.Interpreter.to_michelson_const ctxt data
                | _ -> fail Invalid_language_argument
              end
          | _ -> fail Invalid_language_argument
        in
        param >>=? fun (ctxt, parameter) ->
        Script_interpreter.execute_fee_script ctxt step_constants
          ~fee_code ~storage ~entrypoint ~parameter
      end
  | Dune_code fee_code, Dune_expr storage -> begin
      module_of_code ctxt fee_code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      let open Lang in
      let parameter = match parameter with
        | Dune_expr parameter ->
            begin
              match parameter with
              | Repr.Const p -> return (ctxt, p)
              | _ -> fail Invalid_language_argument
            end
        | Michelson_expr parameter ->
            Lang.Interpreter.from_michelson_const ctxt parameter
        | Dune_code _ -> fail Invalid_language_argument
      in
      parameter >>=? fun (ctxt, parameter) ->
      match fee_code, storage with
      | Repr.Code fee_code, Repr.Const storage ->
          Interpreter.execute_fee_script ctxt step_constants
            ~fee_code ~storage ~entrypoint ~parameter
          >>=? fun { ctxt; max_fee; max_storage } ->
          return { ctxt; max_fee; max_storage }
      | _ -> fail Invalid_language_argument
    end
  | _ -> fail Invalid_language_argument
           (* TODO Love : translation to/from Michelson exprs  *)

let execute_value ctxt ~self ~val_name ~code ~storage ~parameter =
  match code, storage with
  | Dune_code code, Dune_expr storage ->
      module_of_code ctxt code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      let open Lang in
      begin
        match code, storage, parameter with
        | Repr.Code code, Repr.Const storage, None ->
            Interpreter.execute_value ctxt
              ~self ~code ~storage ~val_name ~parameter:None
            >>=? fun { ctxt; result } ->
            return { ctxt; result = Dune_expr (Repr.Const result) }
        | Repr.Code code, Repr.Const storage,
          Some (Dune_expr (Repr.Const parameter)) ->
            Interpreter.execute_value ctxt
              ~self ~code ~storage ~val_name ~parameter:(Some parameter)
            >>=? fun { ctxt; result } ->
            return { ctxt; result = Dune_expr (Repr.Const result) }
        | Repr.Code code, Repr.Const storage, Some (Michelson_expr parameter) ->
            Lang.Interpreter.from_michelson_const ctxt parameter
            >>=? fun (ctxt, parameter) ->
            Interpreter.execute_value ctxt
              ~self ~code ~storage ~val_name ~parameter:(Some parameter)
            >>=? fun { ctxt; result } ->
            return { ctxt; result = Dune_expr (Repr.Const result) }
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument

let find_entrypoint ctxt legacy ~code ~entrypoint =
  match code with
  | Michelson_expr expr ->
      begin
        Lwt.return
          begin
            Script_ir_translator.parse_toplevel ~legacy expr
            >>? fun (arg_type, _, _, root_name, _fee_code) ->
            Script_ir_translator.parse_ty ctxt ~legacy
              ~allow_big_map:true ~allow_operation:false
              ~allow_contract:true arg_type >>? fun (Ex_ty arg_type, _) ->
            Script_ir_translator.find_entrypoint ~root_name arg_type entrypoint
          end
        >>= function
        | Ok (_f , Ex_ty ty)->
            Script_ir_translator.unparse_ty ctxt ty >>=? fun (ty_node, _) ->
            return (Dune_lang_repr.Michelson_expr
                      (Micheline.strip_locations ty_node))
        | Error _ ->
            raise Not_found
      end
  | Dune_code code ->
      module_of_code ctxt code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      begin
        match code with
        | Lang.Repr.Code code ->
            Lang.Interpreter.get_entrypoint ctxt ~code ~entrypoint
            >>=? fun (_, ty) ->
            return (Dune_expr(Lang.Repr.Const ty))
        | _ -> fail Invalid_language_argument
      end
  | Dune_expr _ -> fail Invalid_language_argument

let list_entrypoints ctxt legacy ~code =
  match code with
  | Michelson_expr expr ->
      Lwt.return
        begin
          Script_ir_translator.parse_toplevel ~legacy expr
          >>? fun (arg_type, _, _, root_name, _fee_code) ->
          Script_ir_translator.parse_ty ctxt ~legacy
            ~allow_big_map:true ~allow_operation:false
            ~allow_contract:true arg_type
          >>? fun (Script_ir_translator.Ex_ty arg_type, _) ->
          Script_ir_translator.list_entrypoints ~root_name arg_type ctxt
        end
      >>=? fun (unreachable_entrypoint,map) ->
      return
        (unreachable_entrypoint,
         Script_ir_translator.Entrypoints_map.fold (fun entry (_, ty) acc ->
             (entry, Dune_lang_repr.Michelson_expr
                (Micheline.strip_locations ty)) :: acc
           ) map [])
  | Dune_code code ->
      module_of_code ctxt code >>=? fun m ->
      let module Lang = (val m : Dune_script_interpreter_registration.S) in
      begin
        match code with
        | Lang.Repr.Code code ->
            Lang.Interpreter.list_entrypoints ctxt ~code
            >>=? fun (_, l) ->
            return ([], List.map (fun (n, t) ->
                (n, (Dune_expr(Lang.Repr.Const t)))) l)
        | _ -> fail Invalid_language_argument
      end
  | Dune_expr _ -> fail Invalid_language_argument

let pack_data ctxt ~data ~typ_opt =
  match data, typ_opt with
  | Michelson_expr data, Some (Michelson_expr typ) ->
      Lwt.return (Script_ir_translator.parse_ty ctxt
                    ~legacy:true ~allow_big_map:false
                    ~allow_operation:false ~allow_contract:true
                    (Micheline.root typ))
      >>=? fun (Script_ir_translator.Ex_ty typ, ctxt) ->
      Script_ir_translator.parse_data ctxt
        ~legacy:true typ (Micheline.root data)
      >>=? fun (data, ctxt) ->
      Script_ir_translator.pack_data ctxt typ data
  | Dune_expr data, Some (Dune_expr typ) ->
      begin
        module_of_const ctxt data >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        match data, typ with
        | Lang.Repr.Const data, Lang.Repr.Const typ ->
            Lang.Interpreter.pack_data ctxt ~typ:(Some typ) ~data
            >>=? fun (ctxt, b) -> return (b, ctxt)
        | _ -> fail Invalid_language_argument
      end
  | Dune_expr data, None ->
      begin
        module_of_const ctxt data >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        match data with
        | Lang.Repr.Const data ->
            Lang.Interpreter.pack_data ctxt ~typ:None ~data
            >>=? fun (ctxt, b) -> return (b, ctxt)
        | _ -> fail Invalid_language_argument
      end
  | Michelson_expr data, Some (Dune_expr typ) ->
      (* Packing as a michelson value *)
      begin
        module_of_const ctxt typ >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        match typ with
        | Lang.Repr.Const typ ->
            Lang.Interpreter.to_michelson_const ctxt typ
            >>=? fun (ctxt, typ) ->
            Lwt.return (Script_ir_translator.parse_ty ctxt
                          ~legacy:true ~allow_big_map:false
                          ~allow_operation:false ~allow_contract:true
                          (Micheline.root typ))
            >>=? fun (Script_ir_translator.Ex_ty typ, ctxt) ->
            Script_ir_translator.parse_data ctxt
              ~legacy:true typ (Micheline.root data)
            >>=? fun (data, ctxt) ->
            Script_ir_translator.pack_data ctxt typ data
        | _ -> fail Invalid_language_argument
      end
  | Dune_expr data, Some (Michelson_expr typ) -> (* Packing as a Love value *)
      begin
        module_of_const ctxt data >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        Lang.Interpreter.from_michelson_const ctxt typ
        >>=? fun (ctxt, typ) ->
        match data with
        | Lang.Repr.Const data ->
            Lang.Interpreter.pack_data ctxt ~typ:(Some typ) ~data
            >>=? fun (ctxt, b) -> return (b, ctxt)
        | _ -> fail Invalid_language_argument
      end
  | Michelson_expr _, None -> fail Invalid_language_argument
  | Dune_code _, _
  | _, Some (Dune_code _) -> fail Invalid_language_argument


let unpack_data ctxt ~data ~typ =
  match typ with
  | Michelson_expr _ -> (* Michelson does not require a type to unpack *)
      begin
        begin
          if MBytes.get_char data 0 != '\005' then
            failwith
              "Not a piece of packed Michelson data (must start with `0x05`)"
          else return_unit
        end
        >>=? fun () ->
        (* Remove first byte *)
        let data = MBytes.sub data 1 ((MBytes.length data) - 1) in
        match Data_encoding.Binary.of_bytes
                Alpha_context.Script.expr_encoding data with
        | None -> return None
        | Some e -> return (Some (e, ctxt))
      end
  | Dune_expr typ ->
      begin
        module_of_const ctxt typ >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        match typ with
        | Lang.Repr.Const typ ->
            Lang.Interpreter.unpack_data ctxt typ data
            >>=? fun (ctxt, value) ->
            return (Some (Dune_expr (Lang.Repr.Const value), ctxt))
        | _ -> fail Invalid_language_argument
      end
  | Dune_code _ -> fail Invalid_language_argument

let big_map_get_opt ctxt ~id ~key =
  let ctxt = Gas.set_unlimited ctxt in
  Big_map.exists ctxt id >>=? fun (ctxt, types) ->
  match key, types with
  | _, None -> raise Not_found
  | Script.Michelson_expr key,
    Some (Script.Michelson_expr key_type, Script.Michelson_expr value_type) ->
      begin
        Lwt.return (Script_ir_translator.parse_ty ctxt
                      ~legacy:true ~allow_big_map:false
                      ~allow_operation:false ~allow_contract:true
                      (Micheline.root key_type))
        >>=? fun (Script_ir_translator.Ex_ty key_type, ctxt) ->
        Lwt.return (Script_ir_translator.parse_ty ctxt
                      ~legacy:true ~allow_big_map:false
                      ~allow_operation:false ~allow_contract:true
                      (Micheline.root value_type))
        >>=? fun (Script_ir_translator.Ex_ty value_type, ctxt) ->
        Script_ir_translator.parse_data ctxt
          ~legacy:true key_type (Micheline.root key) >>=? fun (key, ctxt) ->
        Script_ir_translator.hash_data ctxt key_type key >>=? fun (key, ctxt) ->
        Big_map.get_opt ctxt id key >>=? fun (_ctxt, value) ->
        match value with
        | Some (Michelson_expr value) ->
            Script_ir_translator.parse_data ctxt
              ~legacy:true value_type (Micheline.root value)
            >>=? fun (value, ctxt) ->
            Script_ir_translator.unparse_data ctxt Readable value_type value
            >>=? fun (value, _ctxt) ->
            return_some (Dune_lang_repr.Michelson_expr
                           (Micheline.strip_locations value))
        | None | Some _ -> raise Not_found
      end
  | Script.Dune_expr key, Some (Script.Dune_expr key_type, _) ->
      begin
        module_of_const ctxt key_type >>=? fun m ->
        let module Lang = (val m : Dune_script_interpreter_registration.S) in
        match key with
        | Lang.Repr.Const key ->
            Lang.Interpreter.hash_data ctxt key >>=? fun (ctxt, hash) ->
            Big_map.get_opt ctxt id hash >>=? fun (_ctxt, value) ->
            return value
        | _ -> fail Invalid_language_argument
      end
  | _ -> fail Invalid_language_argument
