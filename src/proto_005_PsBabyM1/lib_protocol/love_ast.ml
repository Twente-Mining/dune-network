(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Utils
open Signature
open Ident
open Love_ast_types
open Love_ast_types.AST

let pp_location fmt { pos_lnum; pos_bol; pos_cnum } =
  Format.fprintf fmt "line %i : %i to %i"
    pos_lnum pos_bol pos_cnum

let compare_location t1 t2 =
  let ln = Compare.Int.compare t1.pos_lnum t2.pos_lnum in
  if not @@ Compare.Int.equal ln 0
  then ln
  else
    let bo = Compare.Int.compare t1.pos_bol t2.pos_bol in
    if not @@ Compare.Int.equal bo 0
    then bo
    else Compare.Int.compare t1.pos_cnum t2.pos_cnum

let pp_visibility fmt = function
  | Private -> Format.fprintf fmt "private"
  | Public  -> Format.fprintf fmt "public"

let kind_to_string  = function
  | TAbstract -> "abstract"
  | TPrivate -> "private"
  | TPublic -> ""
  | TInternal -> "internal"

let pp_type_kind fmt k =
  Format.fprintf fmt "%s" @@ kind_to_string k

let type_kind_to_sigtype = function
  | TPublic -> (fun td -> Some (TYPE.SPublic td))
  | TPrivate -> (fun td -> Some (SPrivate td))
  | TAbstract -> (fun td -> (Some (SAbstract (Love_type.typedef_parameters td))))
  | TInternal -> (fun _ -> None)

let exn_id = function
  | Fail _ -> "Failure"
  | Exception e -> Ident.get_final e

let print_exn_name fmt =
  function
  | Fail t -> Format.fprintf fmt "Failure [:%a]" Love_type.pretty t
  | Exception e -> print_strident fmt e

let exn_equal e1 e2 =
  match e1, e2 with
    Fail t1, Fail t2 -> Love_type.bool_equal t1 t2
  | Exception e1, Exception e2 -> Ident.equal String.equal e1 e2
  | _,_ -> false

let is_module c =
  match c.kind with
  | Module -> true
  | Contract _ -> false

let compare_annot l1 l2 = compare_option compare_location l1 l2

let compare_annoted cmp a1 a2 =
  let ann = compare_annot a1.annot a2.annot in
  if Compare.Int.equal ann 0 then cmp a1.content a2.content
  else ann

  let compare_visibility v1 v2 =
  match v1, v2 with
    Private, Private | Public, Public -> 0
  | Private, Public -> 1
  | Public, Private -> -1

let tkid = function
    TPublic -> 0
  | TPrivate -> 1
  | TInternal -> 2
  | TAbstract -> 3

let compare_type_kind t1 t2 = Compare.Int.compare (tkid t1) (tkid t2)

let compare_exn_name e1 e2 =
  match e1, e2 with
    Fail t1, Fail t2 -> Love_type.compare t1 t2
  | Exception n1, Exception n2 -> Ident.compare String.compare n1 n2
  | Fail _, Exception _ -> 1
  | Exception _, Fail _ -> -1

let cid = function
  | CUnit -> 0
  | CBool _ -> 1
  | CString _ -> 2
  | CBytes _ -> 3
  | CInt _ -> 4
  | CNat _ -> 5
  | CDun _ -> 6
  | CKey _ -> 7
  | CKeyHash _ -> 8
  | CSignature _ -> 9
  | CTimestamp _ -> 10
  | CAddress _ -> 11

let compare_raw_const c1 c2 =
  match c1, c2 with
  | CUnit, CUnit -> 0
  | CBool b1, CBool b2 -> Compare.Bool.compare b1 b2
  | CBytes b1, CBytes b2 -> MBytes.compare b1 b2
  | CInt i1, CInt i2
  | CNat i1, CNat i2 -> Z.compare i1 i2
  | CDun d1, CDun d2 -> Int64.compare d1 d2
  | CKey k1, CKey k2 -> Public_key.compare k1 k2
  | CKeyHash k1, CKeyHash k2 -> Public_key_hash.compare k1 k2
  | CSignature s1, CSignature s2 -> Signature.compare s1 s2
  | CTimestamp t1, CTimestamp t2 -> Z.compare t1 t2
  | CAddress a1, CAddress a2 -> Compare.String.compare a1 a2
  | _ -> (* Different *) Compare.Int.compare (cid c1) (cid c2)

let compare_const = compare_annoted compare_raw_const

let pid = function
  | PAny -> 0
  | PVar _ -> 1
  | PAlias _ -> 2
  | PConst _ -> 3
  | PList _ -> 6
  | PTuple _ -> 7
  | PConstr _ -> 8
  | PContract _ -> 9
  | POr _ -> 10
  | PRecord _ -> 11

let compare_record_list
    compare_pattern
    {fields = f1; open_record = o1}
    {fields = f2; open_record = o2} =
  match o1, o2 with
    Open, Closed -> 1
  | Closed, Open -> -1
  | (Open | Closed), (Open | Closed) ->
     compare_list (compare_pair String.compare (compare_option compare_pattern)) f1 f2

let rec compare_raw_pattern p1 p2 =
  match p1, p2 with
    PAny, PAny -> 0
  | PVar v1, PVar v2 -> String.compare v1 v2
  | PAlias (p1, n1), PAlias (p2, n2) ->
    let a = String.compare n1 n2 in
    if Compare.Int.equal a 0 then compare_pattern p1 p2
    else a

  | PConst c1, PConst c2 -> compare_const c1 c2
  | PList pl1, PList pl2
  | PTuple pl1, PTuple pl2
  | POr pl1, POr pl2 -> Utils.compare_list compare_pattern pl1 pl2
  | PConstr (n1, pl1), PConstr (n2, pl2) ->
    let a = String.compare n1 n2 in
    if Compare.Int.equal a 0 then Utils.compare_list compare_pattern pl1 pl2
    else a
  | PContract (n1, st1), PContract (n2, st2) ->
    let a = String.compare n1 n2 in
    if Compare.Int.equal a 0 then Love_type.compare_struct_type st1 st2
    else a
  | PRecord l1, PRecord l2 -> compare_record_list compare_pattern l1 l2
  | (PAny | PVar _ | PAlias _ | PConst _ | PList _ | PTuple _
    | POr _ | PConstr _ | PContract _ | PRecord _),
    (PAny | PVar _ | PAlias _ | PConst _ | PList _ | PTuple _
    | POr _ | PConstr _ | PContract _ | PRecord _) -> Compare.Int.compare (pid p1) (pid p2)

and compare_pattern p1 p2 = compare_annoted compare_raw_pattern p1 p2

let eid = function
  | Const _ -> 0
  | Var _ -> 1
  | VarWithArg _ -> 2
  | Let _ -> 3
  | LetRec _ -> 4
  | Lambda _ -> 5
  | Apply _ -> 6
  | TLambda _ -> 7
  | TApply _ -> 8
  | Seq _ -> 9
  | If _ -> 10
  | Match _ -> 11
  | Raise _ -> 12
  | TryWith _ -> 13
  | Nil -> 14
  | List _ -> 15
  | Tuple _ -> 16
  | Project _ -> 17
  | Update _ -> 18
  | Constructor _ -> 19
  | Record _ -> 20
  | GetField _ -> 21
  | SetFields _ -> 22
  | PackStruct _ -> 23

let contentid = function
  | DefType _ -> 0
  | DefException _ -> 1
  | Init _ -> 2
  | Entry _ -> 3
  | View _ -> 4
  | Value _ -> 5
  | Structure _ -> 6
  | Signature _ -> 7

let rec compare_raw_exp e1 e2 =
  match e1, e2 with
    Const c1, Const c2 -> compare_const c1 c2
  | Var v1, Var v2 -> Ident.compare String.compare v1 v2
  | Let {bnd_pattern = p1; bnd_val = e1; body = b1 },
    Let {bnd_pattern = p2; bnd_val = e2; body = b2 } ->
    compare_pair
      compare_pattern
      (compare_pair compare_exp compare_exp)
      (p1, (e1, b1))
      (p2, (e2, b2))
  | LetRec {bnd_var = v1; bnd_val = e1; body = b1; fun_typ = t1},
    LetRec {bnd_var = v2; bnd_val = e2; body = b2; fun_typ = t2} ->
    (compare_pair String.compare @@
     compare_pair compare_exp @@
     compare_pair compare_exp Love_type.compare)
      (v1, (e1, (b1, t1)))
      (v2, (e2, (b2, t2)))
  | Lambda {arg_pattern = p1; body = b1; arg_typ = t1},
    Lambda {arg_pattern = p2; body = b2; arg_typ = t2} ->
    (compare_pair compare_pattern @@
     compare_pair compare_exp Love_type.compare)
      (p1, (b1, t1))
      (p2, (b2, t2))
  | Apply {fct = f1; args = a1}, Apply {fct = f2; args = a2} ->
    compare_pair compare_exp (compare_list compare_exp) (f1, a1) (f2, a2)
  | TLambda {targ = t1; exp = e1}, TLambda {targ = t2; exp = e2} ->
    compare_pair Love_type.compare_tvar compare_exp (t1, e1) (t2, e2)
  | TApply {exp = e1; typ = t1}, TApply {exp = e2; typ = t2} ->
    compare_pair compare_exp Love_type.compare (e1, t1) (e2, t2)
  | Seq el1, Seq el2 -> compare_list compare_exp el1 el2
  | If {cond = c1; ifthen = t1; ifelse = e1},
    If {cond = c2; ifthen = t2; ifelse = e2} ->
    (compare_pair compare_exp @@
     compare_pair compare_exp compare_exp)
      (c1, (t1, e1))
      (c2, (t2, e2))
  | Match {arg = a1; cases = l1}, Match {arg = a2; cases = l2} ->
    compare_pair compare_exp (compare_list @@ compare_pair compare_pattern compare_exp)
      (a1, l1) (a2, l2)
  | Raise {exn = e1; args = a1; loc = l1}, Raise {exn = e2; args = a2; loc = l2} ->
    (compare_pair compare_exn_name @@
     compare_pair (compare_list compare_exp) compare_annot)
      (e1, (a1, l1)) (e2, (a2, l2))
  | TryWith {arg = e1; cases = c1}, TryWith {arg = e2; cases = c2} ->
    compare_pair
      compare_exp
      (compare_list @@ compare_pair (compare_pair compare_exn_name (compare_list compare_pattern)) compare_exp)
      (e1, c1) (e2, c2)
  | Nil, Nil -> 0
  | List (el1, e1), List (el2, e2) ->
    compare_pair (compare_list compare_exp) compare_exp (el1, e1) (el2,e2)
  | Tuple el1, Tuple el2 ->
    compare_list compare_exp el1 el2
  | Project {tuple = t1; indexes = i1},
    Project {tuple = t2; indexes = i2} ->
    compare_pair compare_exp (compare_list Compare.Int.compare) (t1, i1) (t2, i2)
  | Update {tuple = t1; updates = u1}, Update {tuple = t2; updates = u2} ->
    compare_pair
      compare_exp
      (compare_list @@ compare_pair Compare.Int.compare compare_exp)
      (t1, u1)
      (t2, u2)
  | Constructor {constr = c1; ctyps = tl1; args = a1},
    Constructor {constr = c2; ctyps = tl2; args = a2} ->
    compare_pair
      (Ident.compare String.compare)
      (compare_pair
         (compare_list Love_type.compare)
         (compare_list compare_exp)
      )
      (c1, (tl1, a1))
      (c2, (tl2, a2))
  | Record {path = p1; contents = c1}, Record {path = p2; contents = c2} ->
    compare_pair
      (compare_option @@ Ident.compare String.compare)
      (compare_list @@ compare_pair String.compare compare_exp)
      (p1, c1)
      (p2, c2)
  | GetField {record = r1; field = f1}, GetField {record = r2; field = f2} ->
    compare_pair
      compare_exp
      String.compare
      (r1, f1)
      (r2, f2)
  | SetFields {record = r1; updates = u1},
    SetFields {record = r2; updates = u2} ->
    compare_pair
      compare_exp
      (compare_list @@ compare_pair String.compare compare_exp)
      (r1, u1)
      (r2, u2)
  | PackStruct r1, PackStruct r2 -> compare_reference r1 r2
  | _ -> Compare.Int.compare (eid e1) (eid e2)

and compare_exp e1 e2 = compare_annoted compare_raw_exp e1 e2

and compare_entry e1 e2 =
  compare_pair
    compare_exp
    (compare_pair (compare_option compare_exp) Love_type.compare)
    (e1.entry_code, (e1.entry_fee_code, e1.entry_typ))
    (e2.entry_code, (e2.entry_fee_code, e2.entry_typ))

and compare_view v1 v2 =
  compare_tuple3
    compare_exp
    Love_type.compare
    Love_type.compare_rec
    (v1.view_code, v1.view_typ, v1.view_recursive)
    (v2.view_code, v2.view_typ, v2.view_recursive)

and compare_value v1 v2 =
  compare_tuple4
    compare_exp
    Love_type.compare
    compare_visibility
    Love_type.compare_rec
    (v1.value_code, v1.value_typ, v1.value_visibility, v1.value_recursive)
    (v2.value_code, v2.value_typ, v2.value_visibility, v2.value_recursive)

and compare_content c1 c2 =
  match c1, c2 with
    DefType (tk1, td1), DefType (tk2, td2) ->
    compare_pair compare_type_kind Love_type.compare_typedef (tk1, td1) (tk2, td2)
  | DefException l1, DefException l2 ->
    compare_list Love_type.compare l1 l2
  | Entry e1, Entry e2 -> compare_entry e1 e2
  | View v1, View v2 -> compare_view v1 v2
  | Value v1, Value v2 -> compare_value v1 v2
  | Structure s1, Structure s2 -> compare_structure s1 s2
  | Signature s1, Signature s2 -> Love_type.compare_sig_kind s1 s2
  | _ -> Compare.Int.compare (contentid c1) (contentid c2)

and compare_structure s1 s2 =
  compare_pair
    Love_type.compare_struct_kind
    (compare_list @@ compare_pair String.compare compare_content)
    (s1.kind, s1.structure_content)
    (s2.kind, s2.structure_content)

and compare_reference r1 r2 =
  match r1, r2 with
    Anonymous a1, Anonymous a2 -> compare_structure a1 a2
  | Named n1, Named n2 -> Ident.compare String.compare n1 n2
  | Anonymous _, Named _ -> 1
  | Named _, Anonymous _ -> -1

let compare_top_contract t1 t2 =
  compare_pair
    (compare_pair Compare.Int.compare Compare.Int.compare)
    compare_structure
    (t1.version, t1.code)
    (t2.version, t2.code)
