(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)


open Love_ast

open Love_pervasives
open Ident
open Signature
open Utils
open Love_ast_types
open Love_ast_types.AST

let print_prim fmt (p, al) =
  let open Love_primitive in
  match p, al with
  | { prim_name = "="; _ }, _
  | { prim_name = "<>"; _ }, _
  | { prim_name = "<"; _ }, _
  | { prim_name = "<="; _ }, _
  | { prim_name = ">"; _ }, _
  | { prim_name = ">="; _ }, _ ->
    Format.fprintf fmt
      "(fun ('a[Comparable]) \
            (x : ('a[Comparable])) \
            (y : ('a[Comparable])) -> \
       x %s [:('a[Comparable])] y)" (Love_primitive.name p)
  | _ ->
    if Love_primitive.is_infix p
    then (
      match p.prim_type al with
        TArrow (t1, (TArrow (t2, _))) ->
        Format.fprintf fmt "(fun (x : %a) (y : %a) -> x %s y)"
          Love_type.pretty t1
          Love_type.pretty t2
          (Love_primitive.name p)
      | _ -> raise (
          Exceptions.InvariantBroken
            (Format.asprintf
               "Type of infix primitive %s should be a two argument function."
               (Love_primitive.name p)
            )
        )
    ) else Format.fprintf fmt "%s" (Love_primitive.name p)

let print_extended_arg fmt = function
  | TYPE.ANone -> ()
  | AContractType st -> Love_type.pp_ccontract_sig fmt st

module Ast = struct

let print_annot fmt { pos_lnum; pos_bol; pos_cnum } =
  Format.fprintf fmt "line %i : %i to %i" pos_lnum pos_bol pos_cnum

let opt_print print (fmt : Format.formatter) = function
  | None -> ()
  | Some elt -> print fmt elt

let print_annoted print fmt v =
  print fmt v.content

let print_raw_const fmt (t : raw_const) =
  let open Format in
  match t with
  | CUnit -> fprintf fmt "()"
  | CBool b -> fprintf fmt "%b" b
  | CString s -> fprintf fmt "\"%s\"" s
  | CBytes b -> fprintf fmt "0x%s" (match MBytes.to_hex b with `Hex s -> s)
  | CInt i -> fprintf fmt "%s" (Z.to_string i)
  | CNat i -> fprintf fmt "%sp" (Z.to_string i)
  | CDun d -> fprintf fmt "%sdn" (Int64.to_string d)
  | CKey k -> fprintf fmt "%a" Public_key.pp k
  | CKeyHash kh -> fprintf fmt "%a#" Public_key_hash.pp kh
  | CSignature s -> fprintf fmt "%a" Signature.pp s
  | CTimestamp ts -> fprintf fmt "%s" (Script_timestamp_repr.to_string ts)
  | CAddress a -> fprintf fmt "%s" a

let print_const = print_annoted print_raw_const


let print_record_list print_pattern fmt {fields; open_record} =
  Format.fprintf fmt
    "%a%s"
    (Utils.print_list_s
       "; "
       (fun fmt (s, p) -> Format.fprintf fmt "%s%a" s (opt_print print_pattern) p)) fields
    (match open_record with Open -> "; _" | Closed -> "")

let rec print_raw_pattern fmt (p : raw_pattern) =
  let open Format in
  match p with
  | PAny -> fprintf fmt "_"
  | PVar v -> fprintf fmt "%s" v
  | PAlias (p, v) -> fprintf fmt "(%a) as %s" print_pattern p v
  | PConst c -> fprintf fmt "%a" (print_const) c
  | PList pl -> print_list_s " :: " print_pattern fmt pl
  | PTuple pl -> fprintf fmt "(%a)" (print_list_s ", " print_pattern) pl
  | PConstr (c, pl) ->
      fprintf fmt "%s (%a)" c (print_list_s ", " print_pattern) pl
  | PContract (n, st) ->
      fprintf fmt "contract %s : %a" n Love_type.pp_ccontract_sig st
  | POr pl -> print_list_s " | " print_pattern fmt pl
  | PRecord l -> fprintf fmt "{%a}" (print_record_list print_pattern) l

and print_pattern a = print_annoted print_raw_pattern a

let rec print_raw_exp fmt (exp : raw_exp) =
  let open Format in
  match exp with
  | Const c ->
      wrap_debug_left false fmt "const" print_const c
  | Var v ->
      fprintf fmt "%a" print_strident v
  | VarWithArg (v, xa) ->
      fprintf fmt "%a<:%a>" print_strident v print_extended_arg xa
  | Let { bnd_pattern; bnd_val; body } ->
      fprintf fmt "let %a =@,@[<v 2>  %a@] in@,%a"
        print_pattern bnd_pattern print_exp bnd_val print_exp body
  | LetRec { bnd_var; bnd_val; body; _ } ->
      fprintf fmt "let rec %s =@,@[<v 2>  %a@] in@,%a"
        bnd_var print_exp bnd_val print_exp body
  | Lambda { AST.arg_pattern; body; arg_typ } ->
      fprintf fmt "fun (%a : %a) -> (@,@[<v 2>  %a@]@,)"
        print_pattern arg_pattern
        Love_type.pretty arg_typ print_exp body
  | Apply { fct; args } -> fprintf fmt "(%a) %a" print_exp fct print_args args

  | TLambda { targ; exp } ->
      fprintf fmt "%a ->@,@[<v 2>  %a@]" Love_type.pp_typvar targ print_exp exp
  | TApply { exp; typ } ->
      fprintf fmt "%a [:%a]" print_exp exp Love_type.pretty typ
  | Seq el ->
      print_seq fmt el
  | If { cond; ifthen; ifelse } ->
      fprintf fmt "if (%a) then (@,@[<v 2>  %a@]@,) else (@,@[<v 2>  %a@]@,)"
        print_exp cond print_exp ifthen print_exp ifelse
  | Match { arg = { content = Var v; _ }; cases } ->
      fprintf fmt "match %a with@,@[<v 2>  %a@]"
        print_strident v print_cases cases
  | Match { arg = { content = Const c; _ }; cases } ->
      fprintf fmt "match %a with@,@[<v 2>  %a@]"
        print_const c print_cases cases
  | Match { arg; cases } ->
      fprintf fmt "match @,@[<v 2>  %a@]@, with @,@[<v 2>  %a@]"
        print_exp arg print_cases cases
  | Constructor { constr; ctyps; args } ->
    fprintf fmt "%a %a (%a)"
      print_strident constr
      (Format.pp_print_list (
          fun fmt -> Format.fprintf fmt "[:%a]" Love_type.pretty
        )) ctyps
      print_tuple args
  | Nil ->
      fprintf fmt "[]"
  | List (el, e) ->
      fprintf fmt "%a :: (%a)" print_list el print_exp e
  | Tuple el ->
      fprintf fmt "(%a)" print_tuple el
  | Project { tuple; indexes } ->
      fprintf fmt "(%a).%a" print_exp tuple print_project indexes
  | Update { tuple; updates } ->
      fprintf fmt "%a <- (%a)" print_exp tuple print_updates updates
  | Record { path; contents = rec_def } ->
      let prefix = match path with
        | None -> ""
        | Some path -> Format.asprintf "%a." print_strident path in
      fprintf fmt "%s{ %a }" prefix print_record rec_def
  | GetField { record = {content = Var _ | Const _; _} as record; field } ->
      fprintf fmt "%a.%s" print_exp record field
  | GetField { record; field } ->
      fprintf fmt "%a.%s" print_exp record field
  | SetFields { record; updates } ->
      fprintf fmt "{ %a with %a }" print_exp record print_record updates
  | PackStruct s ->
      begin match s with
      | AST.Anonymous s -> fprintf fmt "(contract %a)" print_structure s
      | AST.Named s -> fprintf fmt "(contract %a)" print_strident s
      end
  | Raise { exn; args; loc = _ } -> (
      match exn with
        Fail t -> fprintf fmt "(failwith [:%a] (%a))" Love_type.pretty t print_tuple args
      | Exception e ->
        fprintf fmt "raise %a (%a)" Ident.print_strident e print_tuple args
    )
  | TryWith { arg; cases } ->
      fprintf fmt "try (%a) with (%a)" print_exp arg print_exn_cases cases

and print_exp a = print_annoted print_raw_exp a

and print_args fmt =
  Format.fprintf fmt "@,@[<v 2>  %a@]@,"
    (Format.pp_print_list
       ~pp_sep:(fun fmt _ -> Format.fprintf fmt "@,")
       (fun fmt -> Format.fprintf fmt "(%a)" print_exp))

and print_seq fmt = print_list_s (";@,") (print_exp) fmt

and print_list fmt = print_list_s " :: " (print_exp) fmt

and print_tuple fmt = print_list_s (", ") (print_exp) fmt

and print_assign fmt (field, exp) =
  Format.fprintf fmt "%s = %a" field print_exp exp

and print_record fmt r =
  print_list_s "; " print_assign fmt r

and print_case fmt (pat, exp) =
  Format.fprintf fmt "%a ->@,@[<v 2>  %a@]" print_pattern pat print_exp exp

and print_cases fmt cases =
  Format.fprintf fmt "| ";
  print_list_s ("@,| ") print_case fmt cases

and print_exn_case fmt ((exn, pl), exp) =
  Format.fprintf fmt "%a(%a) ->@,@[<v 2>  %a@]"
    print_exn_name exn
    (print_list_s ", " print_pattern) pl
    print_exp exp

and print_exn_cases fmt cases =
  Format.fprintf fmt "| ";
  print_list_s "@,| " print_exn_case fmt cases

and print_int fmt i =
  Format.fprintf fmt "%d" i

and print_project fmt l =
  if Compare.Int.(List.length l > 1)
  then Format.fprintf fmt "(%a)" (print_list_s "," print_int) l
  else print_list_s "," print_int fmt l

and print_updates fmt updates =
  let updates = List.sort (fun (i,_) (j,_) -> Compare.Int.compare i j) updates in
  let rec to_str cpt acc = function
      [] -> List.rev acc
    | ((cpt',exp) :: tl) as l ->
      let acc, l =
        if Compare.Int.equal cpt cpt'
        then Format.asprintf "%a" print_exp exp :: acc, tl
        else "_" :: acc, l
      in
      to_str (cpt + 1) acc l
  in
  Format.pp_print_list
    ~pp_sep:(fun fmt _ -> Format.fprintf fmt ", ")
    (fun fmt s -> Format.fprintf fmt "%s" s)
    fmt
    (to_str 0 [] updates)

and print_init fmt (i : init)  =
  match i.init_code.content with
  | Lambda({ AST.arg_pattern = ap; body = b; _ }) ->
    Format.fprintf fmt "(%a : %a) = @,%a@,"
      print_pattern ap Love_type.pretty (i.init_typ) print_exp b
  | _ -> raise (Exceptions.GenericError "Wrong shape of init")

and print_fee_code fmt (fc : exp option) =
  match fc with
  | None -> ()
  | Some e -> Format.fprintf fmt "[%%fee@,@[<v 2>  %a@]@,]@," print_exp e

and print_entry fmt (e : entry)  =
  match e.entry_code.content with
  | Lambda {
      AST.arg_pattern = argp1;
      body = {
        content =
          Lambda { AST.arg_pattern = argp2;
                   body = {
                     content =
                       Lambda({ AST.arg_pattern = ap; body = b; _ }); _
                   }; _ }; _ }; _ } ->
    Format.fprintf fmt "%a %a (%a : %a) = @,%a%a@,"
      print_pattern argp1 print_pattern argp2 print_pattern ap
      Love_type.pretty (e.entry_typ) print_fee_code e.entry_fee_code print_exp b
  | _ -> raise (Exceptions.GenericError "Wrong shape of entry")

and print_view fmt (v : view)  =
  match v.view_code.content with
  | Lambda ({
      AST.arg_pattern = argp;
      body = { content =
                 Lambda({body; _ }); _ }; _ }
    ) ->
    Format.fprintf fmt "%a: %a = @,%a@,"
      print_pattern argp
      Love_type.pretty v.view_typ
      print_exp body
  | e -> print_raw_exp fmt e

and print_value fmt f =
  Format.fprintf fmt "{@,code = %a;@,typ = %a;@,visibility = %a}"
    print_exp f.value_code Love_type.pretty f.value_typ
    pp_visibility f.value_visibility

and print_value_alt fmt f =
  Format.fprintf fmt "%a" print_exp f.value_code

and print_deps =
  Format.pp_print_list
    ~pp_sep:(fun fmt () -> Format.fprintf fmt "@,")
    (fun fmt (n, kt) -> Format.fprintf fmt "use %s = %s" n kt)

and print_structure fmt t =
  let open Format in
  fprintf fmt "@,@[<v>%a@]@,"
    (print_list_s "@,"
       (fun fmt ->
          function
          | id, DefType (k,t) ->
            Love_type.pp_typdef fmt ~name:id
              ~privacy:(Love_ast.kind_to_string k) t;
            fprintf fmt "@,"
          | _id, Init i ->
            fprintf fmt "val%%init %s %a@," "storage" print_init i
          | id, Entry e ->
            fprintf fmt "val%%entry %s %a@," id print_entry e
          | id, View v ->
            fprintf fmt "val%%view %s %a@," id print_view v
          | id, Value v ->
            fprintf fmt "val %s : %a = @,@[<v 2>  %a@]@," id
              Love_type.pretty v.value_typ print_value_alt v
          | id, Structure s ->
            let prefix, deps =
              match s.kind with
              | Module -> "module", []
              | Contract l -> "contract", l in
            fprintf fmt "%s %s = struct@,@[<v 2>  %a@,%a@]@,end@,"
              prefix id print_deps deps print_structure s
          | id, Signature s ->
            let prefix, deps =
              match s.sig_kind with
              | Module -> "module", []
              | Contract l -> "contract", l in
            fprintf fmt "%s type %s = @,@[<v 2>  %a@,%a@]@,"
              prefix id print_deps deps
              Love_type.pp_contract_sig s
          | id, DefException tl ->
            fprintf fmt "exception %s of %a@," id
              (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt " * ")
                 Love_type.pretty) tl
       )
    )
    t.structure_content

end



module Runtime = struct

let print_rconst fmt (t : Love_runtime_ast.const) =
  let open Format in
  match t with
  | RCUnit -> fprintf fmt "()"
  | RCBool b -> fprintf fmt "%b" b
  | RCString s -> fprintf fmt "\"%s\"" s
  | RCBytes b -> fprintf fmt "0x%s" (match MBytes.to_hex b with `Hex s -> s)
  | RCInt i -> fprintf fmt "%s" (Z.to_string i)
  | RCNat i -> fprintf fmt "%sp" (Z.to_string i)
  | RCDun d -> fprintf fmt "%sdn" (Tez_repr.to_string d)
  | RCKey k -> fprintf fmt "%a" Public_key.pp k
  | RCKeyHash kh -> fprintf fmt "%a#" Public_key_hash.pp kh
  | RCSignature s -> fprintf fmt "%a" Signature.pp s
  | RCTimestamp ts -> fprintf fmt "%s" (Script_timestamp_repr.to_string ts)
  | RCAddress a -> fprintf fmt "%a" Contract_repr.pp a

let rec print_rpattern fmt (p : Love_runtime_ast.pattern) =
  let open Format in
  match p with
  | Love_runtime_ast.RPAny -> fprintf fmt "_"
  | Love_runtime_ast.RPVar v -> fprintf fmt "%s" v
  | Love_runtime_ast.RPAlias (p, v) ->
      fprintf fmt "(%a) as %s" print_rpattern p v
  | Love_runtime_ast.RPConst c -> fprintf fmt "%a" print_rconst c
  | Love_runtime_ast.RPList pl -> print_list_s " :: " print_rpattern fmt pl
  | Love_runtime_ast.RPTuple pl ->
    fprintf fmt "(%a)" (print_list_s ", " print_rpattern) pl
  | Love_runtime_ast.RPConstr (c, pl) ->
      fprintf fmt "%s (%a)" c (print_list_s ", " print_rpattern) pl
  | Love_runtime_ast.RPContract (n, st) ->
      fprintf fmt "%s : %a" n Love_type.pp_ccontract_sig st
  | Love_runtime_ast.RPOr pl -> print_list_s " | " print_rpattern fmt pl
  | Love_runtime_ast.RPRecord l -> fprintf fmt "{%a}" (Ast.print_record_list print_rpattern) l

let rec print_rexp fmt (exp : Love_runtime_ast.exp) =
  let open Format in
  match exp with
  | RConst c ->
      wrap_debug_left false fmt "const" print_rconst c
  | RVar v ->
      fprintf fmt "%a" print_strident v
  | RVarWithArg (v, xa) ->
      fprintf fmt "%a<:%a>" print_strident v print_extended_arg xa
  | RLet { bnd_pattern; bnd_val; body } ->
      fprintf fmt "let %a =@,@[<v 2>  %a@] in @,%a"
        print_rpattern bnd_pattern print_rexp bnd_val print_rexp body
  | RLetRec { bnd_var; bnd_val; body; _ } ->
      fprintf fmt "let rec %s =@,@[<v 2>  %a@] in @,%a"
        bnd_var print_rexp bnd_val print_rexp body
  | RLambda l ->
      print_rlambda fmt l
  | RApply { exp; args } ->
    fprintf fmt "(%a) %a" print_rexp exp print_rargs args
  | RSeq el ->
      print_rseq fmt el
  | RIf { cond; ifthen; ifelse } ->
      fprintf fmt "if (%a) then@,@[<v 2>  %a@]@,else@,@[<v 2>  %a@]@,"
        print_rexp cond print_rexp ifthen print_rexp ifelse
  | RMatch { arg = RVar v; cases } ->
      fprintf fmt "match %a with@,@[<v 2>  %a@]"
        print_strident v print_rcases cases
  | RMatch { arg = RConst c; cases } ->
      fprintf fmt "match %a with@,@[<v 2>  %a@]"
        print_rconst c print_rcases cases
  | RMatch { arg; cases } ->
      fprintf fmt "match @,@[<v 2>  %a@]@,with @,@[<v 2>  %a@]"
        print_rexp arg print_rcases cases
  | RConstructor { type_name; ctyps; constr; args = []} ->
      let constr = Ident.change_last type_name (Ident.create_id constr) in
      fprintf fmt "%a %a" print_strident constr
      (Format.pp_print_list (
          fun fmt -> Format.fprintf fmt "[:%a]" Love_type.pretty)) ctyps
  | RConstructor { type_name; ctyps; constr; args = [a]} ->
      let constr = Ident.change_last type_name (Ident.create_id constr) in
      fprintf fmt "%a %a %a" print_strident constr
      (Format.pp_print_list (
         fun fmt -> Format.fprintf fmt "[:%a]" Love_type.pretty)) ctyps
      print_rexp a
  | RConstructor { type_name; ctyps; constr; args } ->
      let constr = Ident.change_last type_name (Ident.create_id constr) in
      fprintf fmt "%a %a (%a)" print_strident constr
      (Format.pp_print_list (
          fun fmt -> Format.fprintf fmt "[:%a]" Love_type.pretty)) ctyps
      print_rtuple args
  | RNil ->
      fprintf fmt "[]"
  | RList (el, e) ->
      fprintf fmt "(%a) :: (%a)" print_rlist el print_rexp e
  | RTuple el ->
      fprintf fmt "(%a)" print_rtuple el
  | RProject { tuple; indexes } ->
      fprintf fmt "(%a).%a" print_rexp tuple Ast.print_project indexes
  | RUpdate { tuple; updates } ->
      fprintf fmt "((%a) <- (%a))" print_rexp tuple print_rupdates updates
  | RRecord { type_name; contents = rec_def } ->
      let prefix = match List.rev (Ident.get_list type_name) with
        | [] | [_] -> ""
        | l -> Format.asprintf "%a." print_strident
                 (Ident.put_list (List.rev l)) in
      fprintf fmt "%s{ %a }" prefix print_rrecord rec_def
  | RGetField { record; field } ->
      fprintf fmt "(%a).%s" print_rexp record field
  | RSetFields { record; updates } ->
      fprintf fmt "{ (%a) with %a }" print_rexp record print_rrecord updates
  | RPackStruct s ->
      begin match s with
        | RAnonymous s ->
          fprintf fmt "(contract (%a))" print_rstructure s
        | RNamed s ->
          fprintf fmt "(contract %a)" print_strident s
      end
  | RRaise { exn; args; loc = _ } ->
      fprintf fmt "%a (%a)"
        Love_runtime_ast.print_exn_name exn print_rtuple args
  | RTryWith { arg; cases } ->
      fprintf fmt "try (@,@[<v 2>  %a@]@,) with (%a)"
        print_rexp arg print_rexn_cases cases

and print_rlambda fmt (lambda : Love_runtime_ast.lambda) =
  let open Format in
  fprintf fmt "fun ";
  List.iter (function
      | Love_runtime_ast.RPattern (p, t) ->
          fprintf fmt "(%a : %a) "
            print_rpattern p Love_type.pretty t
      | Love_runtime_ast.RTypeVar tv ->
          fprintf fmt "%a " Love_type.pp_typvar tv
    ) lambda.Love_runtime_ast.args;
  fprintf fmt "->@,@[<v 2>  %a@]" print_rexp lambda.Love_runtime_ast.body

and print_rargs fmt =
  print_list_s " " (fun fmt arg -> match arg with
    | Love_runtime_ast.RExp e -> Format.fprintf fmt "(%a)" print_rexp e
    | Love_runtime_ast.RType t -> Format.fprintf fmt "[:%a]" Love_type.pretty t
  ) fmt

and print_rseq fmt = print_list_s (";@,") print_rexp fmt

and print_rlist fmt = print_list_s " :: " print_rexp fmt

and print_rtuple fmt = print_list_s (", ") print_rexp fmt

and print_rassign fmt (field, exp) =
  Format.fprintf fmt "%s = %a" field print_rexp exp

and print_rrecord fmt r =
  print_list_s "; " print_rassign fmt r

and print_rcase fmt (pat, exp) =
  Format.fprintf fmt "%a ->@,@[<v 2>  %a@]"
    print_rpattern pat print_rexp exp

and print_rcases fmt cases =
  Format.fprintf fmt "| ";
  print_list_s ("@,| ") print_rcase fmt cases

and print_rexn_case fmt ((exn, pl), exp) =
  Format.fprintf fmt "%a(%a) ->@,@[<v 2>  %a@]"
    Love_runtime_ast.print_exn_name exn
    (print_list_s ", " print_rpattern) pl print_rexp exp

and print_rexn_cases fmt cases =
  print_list_s "@,| " print_rexn_case fmt cases

and print_rupdate fmt (idx, exp) =
  Format.fprintf fmt "%d: %a" idx print_rexp exp

and print_rupdates fmt updates =
  print_list_s ", " print_rupdate fmt updates

and print_rinit fmt (i : Love_runtime_ast.init)  =
  match i.Love_runtime_ast.init_code with
  | Love_runtime_ast.RLambda {
      Love_runtime_ast.args = [
        Love_runtime_ast.RPattern (ap, _) ]; body = b; _ } ->
    Format.fprintf fmt "(%a : %a) = @,%a@,"
      print_rpattern ap Love_type.pretty (i.init_typ) print_rexp b
  | _ -> raise (Exceptions.GenericError "Wrong shape of init")

and print_rfee_code fmt (fc : Love_runtime_ast.exp option) =
  match fc with
  | None -> ()
  | Some e -> Format.fprintf fmt "[%%fee@,@[<v 2>  %a@]@,]@," print_rexp e

and print_rentry fmt (e : Love_runtime_ast.entry)  =
  match e.Love_runtime_ast.entry_code with
  | Love_runtime_ast.RLambda {
      Love_runtime_ast.args = [
        Love_runtime_ast.RPattern (p1,_);
        Love_runtime_ast.RPattern (p2,_);
        Love_runtime_ast.RPattern (p3,_) ]; body = b
    } ->
    Format.fprintf fmt "%a %a (%a : %a) = @,%a%a@,"
      print_rpattern p1 print_rpattern p2 print_rpattern p3
      Love_type.pretty (e.Love_runtime_ast.entry_typ)
      print_rfee_code e.Love_runtime_ast.entry_fee_code print_rexp b
  | _ -> raise (Exceptions.GenericError "Wrong shape of entry")

and print_rview fmt (v : Love_runtime_ast.view)  =
  match v.Love_runtime_ast.view_code with
  | Love_runtime_ast.RLambda {
      Love_runtime_ast.args = [
        Love_runtime_ast.RPattern (p1,_);
        Love_runtime_ast.RPattern _ ]; body = b } ->
    Format.fprintf fmt "%a : %a = @,%a@,"
      print_rpattern p1
      Love_type.pretty v.Love_runtime_ast.view_typ print_rexp b
  | _ -> raise (Exceptions.GenericError "Wrong shape of view")

and print_rvalue fmt f =
  Format.fprintf fmt "{@,code = %a;@,typ = %a;@,visibility = %a}"
    print_rexp
    f.Love_runtime_ast.value_code Love_type.pretty f.Love_runtime_ast.value_typ
    pp_visibility f.Love_runtime_ast.value_visibility

and print_rvalue_alt fmt f =
  Format.fprintf fmt "%a" print_rexp f.Love_runtime_ast.value_code

and print_rstructure fmt t =
  let open Format in
  fprintf fmt "@,@[<v>%a@]@,"
    (print_list_s "@,"
       (fun fmt ->
          function
          | id, Love_runtime_ast.RDefType (k, t) ->
            Love_type.pp_typdef fmt ~name:id ~privacy:(Love_ast.kind_to_string k) t
          | _id, Love_runtime_ast.RInit i ->
              fprintf fmt "val%%init %s %a@," "storage" print_rinit i
          | id, Love_runtime_ast.REntry e ->
              fprintf fmt "val%%entry %s %a@," id print_rentry e
          | id, Love_runtime_ast.RView v ->
              fprintf fmt "val%%view %s %a@," id print_rview v
          | id, Love_runtime_ast.RValue v ->
              fprintf fmt "val %s : %a = %a@," id
                Love_type.pretty v.value_typ print_rvalue_alt v
          | id, Love_runtime_ast.RStructure s ->
              let prefix, deps =
                match s.kind with
                | Module -> "module", []
                | Contract l -> "contract", l in
              fprintf fmt "%s %s = struct@,@[<v 2>  %a@,%a@]@,end@,"
                prefix id Ast.print_deps deps print_rstructure s
          | id, Love_runtime_ast.RSignature s ->
              let prefix, deps =
                match s.sig_kind with
                | Module -> "module", []
                | Contract l -> "contract", l in
              fprintf fmt "%s type %s = sig@,@[<v 2>  %a@,%a@]@,end@,"
                prefix id Ast.print_deps deps
                Love_type.pp_contract_sig s
          | id, Love_runtime_ast.RDefException tl ->
              fprintf fmt "exception %s of %a@," id
                (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt " * ")
                   Love_type.pretty) tl
       )
    )
    t.structure_content

end



module Value = struct

  open Love_value

  let rec print  fmt v =
    let open Value in
    let open Format in
    match v with
    | VUnit -> fprintf fmt "()"
    | VBool b -> fprintf fmt "%b" b
    | VString s -> fprintf fmt "\"%s\"" s
    | VBytes b -> fprintf fmt "0x%s" (match MBytes.to_hex b with `Hex s -> s)
    | VInt i -> fprintf fmt "%s" (Z.to_string i)
    | VNat i -> fprintf fmt "%sp" (Z.to_string i)
    | VTuple cl -> fprintf fmt "(%a)" print_tuple cl
    | VConstr (cstr, []) -> fprintf fmt "%s" cstr
    | VConstr (cstr, cl) ->
        fprintf fmt "%s (%a)" cstr print_tuple cl
    | VRecord fcl -> fprintf fmt "{ %a }" print_fields fcl
    | VList cl -> fprintf fmt "[ %a ]" print_list cl
    | VSet cs ->
        fprintf fmt "{ %a }" print_list (ValueSet.elements cs)
    | VMap cm ->
        fprintf fmt "{ %a }" print_map (ValueMap.bindings cm)
    | VBigMap { id = None; diff = _; _ } ->
        fprintf fmt "<bigmap>"
    | VBigMap { id = Some id; diff = _; _ } ->
        fprintf fmt "<bigmap:%s>" (Z.to_string id)
    | VDun d -> fprintf fmt "%sdn" (Tez_repr.to_string d)
    | VKey k -> fprintf fmt "%a" Public_key.pp k
    | VKeyHash kh -> fprintf fmt "%a#" Public_key_hash.pp kh
    | VSignature s -> fprintf fmt "%a" Signature.pp s
    | VTimestamp ts -> fprintf fmt "%s" (Script_timestamp_repr.to_string ts)
    | VAddress a -> fprintf fmt "%a" Contract_repr.pp a
    | VOperation _op ->
        fprintf fmt "<op>" (* show more *)
    | VPackedStructure (_path, c) ->
        fprintf fmt "<packed:%a>" print_livestructure c.root_struct
    | VContractInstance (_c, a) ->
        fprintf fmt "<contract:%a>" Contract_repr.pp a
    | VEntryPoint (a, e) ->
        fprintf fmt "<entry:%a/%s>" Contract_repr.pp a e
    | VView (a, v) ->
        fprintf fmt "<view:%a/%s>" Contract_repr.pp a v
    | VClosure { call_env; lambda = l } ->
        print_closure_env v fmt call_env;
        Runtime.print_rexp fmt (RLambda l)
    | VPrimitive (p, xl, _vl) -> print_prim fmt (p, xl)

  and print_val_or_type fmt vt =
    let open Value in
    let open Format in
    match vt with
    | V v -> print fmt v
    | T t -> fprintf fmt "[:%a]" Love_type.pretty t

  and print_field  fmt (f, c) =
    Format.fprintf fmt "%s = %a" f print c

  and print_binding  fmt (c1, c2) =
    Format.fprintf fmt "%a -> %a" print c1 print c2

  and print_fields  fmt =
    print_list_s "; " print_field fmt

  and print_tuple  fmt =
    print_list_s ", " print fmt

  and print_list  fmt =
    print_list_s "; " print fmt

  and print_map  fmt l =
    match l with
    | [] -> Format.fprintf fmt "-"
    | _ -> print_list_s "; " print_binding fmt l

  and print_env_value closure fmt (id, c) =
    match c with
    | Value.Local v when v == closure ->
        Format.fprintf fmt "%a -> @self" Ident.print_strident id
    | Value.Local v ->
        Format.fprintf fmt "%a -> %a"
          Ident.print_strident id print v
    | Value.Global (Value.Inlined (p, v)) ->
        Format.fprintf fmt "%a => %a:%a"
          Ident.print_strident id Ident.print_strident p print v
    | Value.Global (Value.Pointer p) ->
        Format.fprintf fmt "%a => %a"
          Ident.print_strident id Ident.print_strident p

  and print_env_ptr_or_inl :
    type t. (Format.formatter -> t -> unit) -> Format.formatter ->
    string Love_pervasives.Ident.t * t Value.ptr_or_inlined -> unit =
    fun pinl fmt (id, s) ->
    match s with
    | Value.Inlined (p, s) ->
        Format.fprintf fmt "%a => %a:%a"
          Ident.print_strident id Ident.print_strident p pinl s
    | Value.Pointer p ->
        Format.fprintf fmt "%a => %a"
          Ident.print_strident id Ident.print_strident p

  and print_closure_env closure fmt call_env =
    let { Value.values; structs; sigs; exns; types; tvars } = call_env in
    let print_opt_sep s l =
      match l with
      | [] -> ()
      | _  -> Format.fprintf fmt "%s" s in
    let print_bnds pl fmt l =
      print_list_s ", " pl fmt l; print_opt_sep ", " l in
    Format.fprintf fmt "[";
    print_bnds (print_env_value closure) fmt values;
    print_bnds (print_env_ptr_or_inl print_livestructure) fmt structs;
    print_bnds (print_env_ptr_or_inl
                  Love_type.pp_contract_sig) fmt sigs;
    print_bnds (fun fmt (ids, idd) ->
        Format.fprintf fmt "%a => %a"
          Ident.print_strident ids Ident.print_strident idd) fmt exns;
    print_bnds (fun fmt (ids, idd) ->
        Format.fprintf fmt "%a => %a"
          Ident.print_strident ids Ident.print_strident idd) fmt types;
    print_bnds (fun fmt (n, t) ->
        Format.fprintf fmt "%s => %a" n Love_type.pretty t) fmt tvars;
    Format.fprintf fmt "]";

  and print_init fmt (i : LiveStructure.init)  =
    match i.LiveStructure.vinit_code with
    | Value.VClosure { Value.call_env = _; lambda = {
        Love_runtime_ast.args = [
          Love_runtime_ast.RPattern (ap, _) ]; body = b; _ } } ->
      Format.fprintf fmt "(%a : %a) = @,%a@,"
        Runtime.print_rpattern ap Love_type.pretty (i.vinit_typ)
        Runtime.print_rexp b
    | _ -> raise (Exceptions.GenericError "Wrong shape of init")

  and print_fee_code fmt (fc : Value.t option) =
    match fc with
    | None -> ()
    | Some (Value.VClosure { Value.call_env = _; lambda = { body = b; _ } }) ->
        Format.fprintf fmt "[%%fee@,@[<v 2>  %a@]@,]@," Runtime.print_rexp b
    | _ -> raise (Exceptions.GenericError "Wrong shape of fee_code")

  and print_entry fmt (e : LiveStructure.entry) =
    match e.LiveStructure.ventry_code with
    | Value.VClosure { Value.call_env = _; lambda = {
        Love_runtime_ast.args = [
          Love_runtime_ast.RPattern (p1,_);
          Love_runtime_ast.RPattern (p2,_);
          Love_runtime_ast.RPattern (p3,_) ]; body = b
      } } ->
      Format.fprintf fmt "%a %a (%a : %a) = @,@[<v 2>  %a%a@]@,"
        Runtime.print_rpattern p1 Runtime.print_rpattern p2
        Runtime.print_rpattern p3
        Love_type.pretty (e.LiveStructure.ventry_typ)
        print_fee_code e.LiveStructure.ventry_fee_code
        Runtime.print_rexp b
    | _ -> raise (Exceptions.GenericError "Wrong shape of entry")

  and print_view fmt (v : LiveStructure.view)  =
    let open Runtime in
    match v.LiveStructure.vview_code with
    | Value.VClosure { Value.call_env = _; lambda = {
        Love_runtime_ast.args = [
          Love_runtime_ast.RPattern (p1,_);
          Love_runtime_ast.RPattern _ ]; body = b } } ->
      Format.fprintf fmt "%a : %a = @,@[<v 2>  %a@]@,"
        print_rpattern p1
        Love_type.pretty v.LiveStructure.vview_typ
        print_rexp b
    | _ -> raise (Exceptions.GenericError "Wrong shape of view")

  and print_value fmt (v : LiveStructure.value) =
    match v.LiveStructure.vvalue_code with
    | Value.VClosure { Value.call_env = _; Value.lambda } ->
        Format.fprintf fmt "%a = @,@[<v 2>  %a@]"
          Love_type.pretty v.LiveStructure.vvalue_typ
          Runtime.print_rexp (Love_runtime_ast.RLambda lambda)
    | _ ->
        Format.fprintf fmt "%a = @,@[<v 2>  %a@]"
          Love_type.pretty v.LiveStructure.vvalue_typ
          print v.LiveStructure.vvalue_code

  and print_livestructure =
    let open Format in fun  fmt (t : LiveStructure.t) ->
      fprintf fmt "@,@[<v>%a@]@,"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@,")
           (fun fmt ->
              function
              | id, LiveStructure.VType (k, t) ->
                Love_type.pp_typdef fmt ~name:id
                  ~privacy:(Love_ast.kind_to_string k) t
              | _id, LiveStructure.VInit i ->
                fprintf fmt "val%%init %s %a@," "storage" print_init i
              | id, LiveStructure.VEntry e ->
                fprintf fmt "val%%entry %s %a@," id print_entry e
              | id, LiveStructure.VView v ->
                fprintf fmt "val%%view %s %a@," id print_view v
              | id, LiveStructure.VValue v ->
                fprintf fmt "val %s : %a@," id print_value v
              | id, LiveStructure.VStructure s ->
                let prefix, deps =
                  match s.LiveStructure.kind with
                  | Module -> "module", []
                  | Contract l -> "contract", l in
                fprintf fmt "%s %s = struct@,%a@, %a@,end@,"
                  prefix id Ast.print_deps deps print_livestructure s
              | id, LiveStructure.VSignature s ->
                let prefix, deps =
                  match s.sig_kind with
                  | Module -> "module", []
                  | Contract l -> "contract", l in
                fprintf fmt "%s type %s = sig %a@,%a@,end"
                  prefix id Ast.print_deps deps
                  Love_type.pp_contract_sig s
              | id, LiveStructure.VException [] ->
                fprintf fmt "exception %s@," id
              | id, LiveStructure.VException tl ->
                fprintf fmt "exception %s of %a@," id
                  (Format.pp_print_list
                     ~pp_sep:(fun fmt () -> Format.fprintf fmt " * ")
                     Love_type.pretty) tl
           )
        )
        t.LiveStructure.content
end
