(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context

type error += Wrong_activation_level of int32 (* `Permanent *)
type error += Activation_level_should_be_last_of_cycle of int32 (* `Permanent *)

val finalize_block : t -> t tzresult Lwt.t

val apply_dune_manager_operation_content :
  apply_manager_operation_content:
    (Alpha_context.t ->
     Script_ir_translator.unparsing_mode ->
     payer:Contract.t ->
     source:Contract.t ->
     chain_id:Chain_id.t ->
     internal:bool ->
     'kind manager_operation ->
     (context *
      'kind Apply_results.successful_manager_operation_result *
      packed_internal_operation list) tzresult Lwt.t) ->
  Alpha_context.t ->
  since:Alpha_context.t ->
  Script_ir_translator.unparsing_mode ->
  payer:Contract.t ->
  source:Contract.t ->
  chain_id:Chain_id.t ->
  Dune_operation.dune_manager_operation ->
  (Alpha_context.t *
   Alpha_context.Kind.dune_manager_operation
     Apply_results.successful_manager_operation_result *
   packed_internal_operation list)
    tzresult Lwt.t
