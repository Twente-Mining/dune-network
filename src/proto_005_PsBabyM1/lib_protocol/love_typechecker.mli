(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Error_monad
open Love_ast_types

exception TypingError of string * string * Love_tenv.t
exception UnknownType of TYPE.type_name
exception UnknownConstr of AST.cstr_name
exception UnknownField of AST.field_name
exception TypingErrorLwt of (Love_context.t * string *  Love_tenv.t)

val extract_deps :
  AST.location option ->
  Love_context.t ->
  Love_tenv.t ->
  (string * string) list ->
  (Love_context.t * Love_tenv.t) tzresult Lwt.t

val typecheck_value :
  Love_context.t ->
  TYPE.type_exp ->
  Love_value.ValueSet.elt -> Love_context.t tzresult Lwt.t

val typecheck_struct :
  AST.location option ->
  Love_tenv.t ->
  AST.structure -> AST.structure * Love_tenv.t

val typecheck_top_contract_deps :
  AST.location option ->
  Love_context.t ->
  AST.top_contract ->
  (Love_context.t * (AST.top_contract * Love_tenv.t))
    tzresult Lwt.t

val extract_deps :
  AST.location option ->
  Love_context.t ->
  (string * string) list ->
  Love_context.t tzresult Lwt.t
