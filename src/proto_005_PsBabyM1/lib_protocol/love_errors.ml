(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives

type error += UnhandledUserException of string
type error += UnhandledException of string
type error += StackOverflow
type error += InvariantBroken of string
type error += GenericError of string
type error += BadArgument of string
type error += Uncomparable
type error += BadParameter
type error += BadReturnType
type error += BadStorage
type error += BadEntryPoint
type error += BadView
type error += DeserializeError of (int * string) (* Binary encoding *)
type error += DecodingError of string (* JSON encoding *)
type error += ParseError of string
type error += TypingError of string
type error += UnknownType of string
type error += UnknownConstr of string
type error += UnknownField of string
type error += EnvironmentError of string
type error += Overflow

let () =
  let open Data_encoding in
  (* Unhandled User Exception *)
  register_error_kind
    `Permanent
    ~id:"love.unhandled_user_exception"
    ~title: "An unhandled user exception occured"
    ~description: "An exception was raised by the interpreted\
                   contract and reached the top level"
    (obj1 (req "description" string))
    (function (UnhandledUserException s) -> Some s | _ -> None)
    (fun s -> UnhandledUserException s);
  (* Unhandled Exception *)
  register_error_kind
    `Permanent
    ~id:"love.unhandled_exception"
    ~title: "An unhandled exception occured"
    ~description: "An exception was raised by the interpreter or\
                   one of its components and reached the top level"
    (obj1 (req "description" string))
    (function (UnhandledException s) -> Some s | _ -> None)
    (fun s -> UnhandledException s);
  (* Stack Overflow *)
  register_error_kind
    `Permanent
    ~id:"love.stack_overflow"
    ~title: "A stack overflow occured"
    ~description: "The maximum stack length has been reached"
    (unit)
    (function StackOverflow -> Some () | _ -> None)
    (fun () -> StackOverflow);
  (* InvariantBroken *)
  register_error_kind
    `Permanent
    ~id:"love.invariant_broken"
    ~title: "A broken invariant was detected"
    ~description: "Something that should not occur have occured ;\
                   this is likely a bug in the interpreter"
    (obj1 (req "description" string))
    (function (InvariantBroken s) -> Some s | _ -> None)
    (fun s -> InvariantBroken s);
  (* GenericError *)
  register_error_kind
    `Permanent
    ~id:"love.generic_error"
    ~title: "A generic error occured"
    ~description: "An unspecific error occured"
    (obj1 (req "description" string))
    (function (GenericError s) -> Some s | _ -> None)
    (fun s -> GenericError s);
  (* BadArgument *)
  register_error_kind
    `Permanent
    ~id:"love.bad_argument"
    ~title: "Bad argument error"
    ~description: "A bad argument has been given to a function"
    (obj1 (req "description" string))
    (function (BadArgument s) -> Some s | _ -> None)
    (fun s -> BadArgument s);
  (* Uncomparable *)
  register_error_kind
    `Permanent
    ~id:"love.uncomparable"
    ~title: "An invalid comparison was attempted"
    ~description: "Tried to compare uncomparable values"
    (unit)
    (function Uncomparable -> Some () | _ -> None)
    (fun () -> Uncomparable);
  (* BadParameter *)
  register_error_kind
    `Permanent
    ~id:"love.badparameter"
    ~title: "The contract was called with the wrong kind of argument"
    ~description: "The contract was called with the wrong kind of argument"
    (unit)
    (function BadParameter -> Some () | _ -> None)
    (fun () -> BadParameter);
  (* BadReturnType *)
  register_error_kind
    `Permanent
    ~id:"love.badreturntype"
    ~title: "The contract return an invalid value"
    ~description: "The contract returned an invalid value"
    (unit)
    (function BadReturnType -> Some () | _ -> None)
    (fun () -> BadReturnType);
  (* BadStorage *)
  register_error_kind
    `Permanent
    ~id:"love.badstorage"
    ~title: "An invalid storage was provided"
    ~description: "The contract was deployed with a storage that does\
                   not match the type it expects"
    (unit)
    (function BadStorage -> Some () | _ -> None)
    (fun () -> BadStorage);
  (* BadEntryPoint *)
  register_error_kind
    `Permanent
    ~id:"love.badentrypoint"
    ~title: "The contract was called on an invalid entry point"
    ~description: "The contract was called on an invalid entry point"
    (unit)
    (function BadEntryPoint -> Some () | _ -> None)
    (fun () -> BadEntryPoint);
  (* BadView *)
  register_error_kind
    `Permanent
    ~id:"love.badview"
    ~title: "An invalid view was requested"
    ~description: "An invalid view was requested"
    (unit)
    (function BadView -> Some () | _ -> None)
    (fun () -> BadView);
  (* DeserializeError *)
  register_error_kind
    `Permanent
    ~id:"love.deserialization_error"
    ~title: "A deserialization error occured"
    ~description: "The deserializer failed to recognize the input data"
    (obj2 (req "code" int31 ) (req "description" string))
    (function (DeserializeError (i, s)) -> Some (i, s) | _ -> None)
    (fun (i,s) -> DeserializeError (i, s));
  (* DecodingError *)
  register_error_kind
    `Permanent
    ~id:"love.decoding_error"
    ~title: "A decoding error occured"
    ~description: "The decoder failed to recognize the input data"
    (obj1 (req "description" string))
    (function (DecodingError s) -> Some s | _ -> None)
    (fun s -> DecodingError s);
  (* ParseError *)
  register_error_kind
    `Permanent
    ~id:"love.parse_error"
    ~title: "A parse error occured"
    ~description: "The parser failed to recognize the input data"
    (obj1 (req "description" string))
    (function (ParseError s) -> Some s | _ -> None)
    (fun s -> ParseError s);
  (* Typing Error *)
  register_error_kind
    `Permanent
    ~id:"love.typing_error"
    ~title: "A typing error occurred"
    ~description: "An ill-typed contract or value was detected"
    (obj1 (req "description" string))
    (function TypingError s -> Some s | _ -> None)
    (fun s -> TypingError s);
  (* UnknownType Error *)
  register_error_kind
    `Permanent
    ~id:"love.unknown_type"
    ~title: "A typing error occurred"
    ~description: "An unknown type was encountered"
    (obj1 (req "description" string))
    (function UnknownType s -> Some s | _ -> None)
    (fun s -> UnknownType s);
  (* UnknownConstr Error *)
  register_error_kind
    `Permanent
    ~id:"love.unknown_constr"
    ~title: "A typing error occurred"
    ~description: "An unknown constructor was encountered"
    (obj1 (req "description" string))
    (function UnknownConstr s -> Some s | _ -> None)
    (fun s -> UnknownConstr s);
  (* UnknownField Error *)
  register_error_kind
    `Permanent
    ~id:"love.unknown_field"
    ~title: "A typing error occurred"
    ~description: "An unknown field was encountered"
    (obj1 (req "description" string))
    (function UnknownField s -> Some s | _ -> None)
    (fun s -> UnknownField s);
  (* Environment Error *)
  register_error_kind
    `Permanent
    ~id:"love.environment_error"
    ~title: "An environment error occurred"
    ~description: "Something that should be in the environment is not"
    (obj1 (req "description" string))
    (function EnvironmentError s -> Some s | _ -> None)
    (fun s -> EnvironmentError s);
  (* Overflow *)
  register_error_kind
    `Permanent
    ~id:"love.overflow"
    ~title: "An overflow occured"
    ~description: "A computation generated an out-of-bound value"
    (unit)
    (function Overflow -> Some () | _ -> None)
    (fun () -> Overflow)

let print_exn fmt (cstr, cl) =
  Format.fprintf fmt "%s (%a)" cstr
    Love_printer.Value.print_tuple cl

let exn_handler =
  let open Love_runtime_ast in
  let module TC = Love_typechecker in
  let module TE = Love_tenv in
  let module LE = Love_pervasives.Exceptions in
  let module LC = Love_context in
  function
  | LE.StackOverflow -> fail (StackOverflow)
  | LE.InvariantBroken s -> fail (InvariantBroken s)
  | LE.GenericError s -> fail (GenericError s)
  | LE.BadArgument s -> fail (BadArgument s)
  | LE.Uncomparable -> fail (Uncomparable)
  | LE.BadParameter -> fail (BadParameter)
  | LE.BadReturnType -> fail (BadReturnType)
  | LE.BadStorage -> fail (BadStorage)
  | LE.BadEntryPoint -> fail (BadEntryPoint)
  | LE.BadView -> fail (BadView)
  | LE.DeserializeError (i, s) -> fail (DeserializeError (i, s))
  | LE.DecodingError s -> fail (DecodingError s)
  | LE.ParseError s -> fail (ParseError s)
  | TC.TypingError (s, loc,_tenc) ->
      fail (TypingError (Format.asprintf "error at %s: %s" loc s))
  | TC.TypingErrorLwt (_, s, _tenc) -> fail (TypingError s)
  | TC.UnknownType type_name ->
      fail (UnknownType (Format.asprintf "%a" Ident.print_strident type_name))
  | TC.UnknownConstr cstr_name ->
      fail (UnknownConstr (Format.asprintf "%a" Ident.print_strident cstr_name))
  | TC.UnknownField field_name ->
      fail (UnknownField  (Format.asprintf "%s" field_name))
  | TE.EnvironmentError s -> fail (EnvironmentError s)
  | LC.UserException (_, (exn, cl), None) ->
      let s = Format.asprintf "%a" print_exn ((exn_id exn), cl) in
      fail (UnhandledUserException s)
  | LC.UserException (_, (exn, cl), Some l) ->
      let s = Format.asprintf "Exception raised at %a : %a"
          Love_ast.pp_location l print_exn ((exn_id exn), cl) in
      fail (UnhandledUserException s)
  | Z.Overflow -> fail (Overflow)
  | Failure s -> fail (UnhandledException ("Failure : " ^ s))
  | Assert_failure (f,_,_) -> fail (UnhandledException ("Assert failure " ^ f))
  | Match_failure (f,_,_) -> fail (UnhandledException ("Match failure " ^ f))
  | Out_of_memory -> fail (UnhandledException "Out of memory")
  | Stack_overflow -> fail (UnhandledException "Stack overflow")
  | _ as exn -> fail (UnhandledException (Dune_debug.string_of_exn exn))
