(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_value

type code =
  | Contract of Love_ast_types.AST.top_contract
  | LiveContract of LiveContract.t
  | FeeCode of FeeCode.t

type const =
  | Value of Value.t
  | Type of Love_ast_types.TYPE.t

let lang = Dune_script_sig.{
    name = "love" ;
    version = Options.version;
  }

let lang_str =
  Format.asprintf "love-%d.%d" (fst lang.version) (snd lang.version)

let recognizes l =
  String.equal l lang_str

let unit = Value Value.VUnit

let is_unit = function
  | Value Value.VUnit -> true
  | _ -> false

let const_encoding =
  let open Data_encoding in
  union ~tag_size:`Uint8 [
    case (Tag 0) ~title:"Value"
      Love_encoding.Value.encoding
      (function Value v -> Some v | _ -> None)
      (fun v -> Value v);
    case (Tag 1) ~title:"Type"
      Love_encoding.Type.encoding
      (function Type t -> Some t | _ -> None)
      (fun t -> Type t);
  ]

let code_encoding =
  let open Data_encoding in
  union ~tag_size:`Uint8 [
    case (Tag 0) ~title:"Contract (parser output)"
      Love_encoding.Ast.top_contract_encoding
      (function Contract s -> Some s | _ -> None)
      (fun s -> Contract s);
    case (Tag 1) ~title:"Live contract (deployed)"
      Love_encoding.Value.live_contract_encoding
      (function LiveContract c -> Some c | _ -> None)
      (fun c -> LiveContract c);
    case (Tag 2) ~title:"Fee code"
      Love_encoding.Value.fee_code_encoding
      (function FeeCode fc -> Some fc | _ -> None)
      (fun fc -> FeeCode fc);
  ]

let location_encoding = Data_encoding.unit

let cost_of_size (blocks, words) =
  let open Gas_limit_repr in
  ((Compare.Int.max 0 (blocks - 1)) *@ alloc_cost 0) +@
  alloc_cost words +@ step_cost blocks

let deserialized_const_cost = function
  | Value v -> cost_of_size (Love_size.Value.value_size (0, 0) v)
  | Type t -> cost_of_size (Love_size.Type.type_size (0, 0) t)

let deserialized_code_cost = function
  | Contract c -> cost_of_size (Love_size.Ast.top_contract_size (0, 0) c)
  | LiveContract c -> cost_of_size
                        (Love_size.Value.live_contract_size (0, 0) c)
  | FeeCode c -> cost_of_size (Love_size.Value.fee_code_size (0, 0) c)

let traversal_const_cost = function
  | Value v -> Gas_limit_repr.step_cost
                 (fst (Love_size.Value.value_size (0, 0) v))
  | Type t -> Gas_limit_repr.step_cost
                (fst (Love_size.Type.type_size (0, 0) t))

let traversal_code_cost = function
  | Contract c -> Gas_limit_repr.step_cost
                    (fst (Love_size.Ast.top_contract_size (0, 0) c))
  | LiveContract c -> Gas_limit_repr.step_cost
                        (fst (Love_size.Value.live_contract_size (0, 0) c))
  | FeeCode c -> Gas_limit_repr.step_cost
                   (fst (Love_size.Value.fee_code_size (0, 0) c))

let pp_const ppf = function
  | Value v -> Love_printer.Value.print ppf v
  | Type t -> Love_type.pretty ppf t
