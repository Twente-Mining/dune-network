open Love_ast_types

type default_type_def =
    Core of TYPE.type_var list * TYPE.traits
  | TypeDef of TYPE.typedef

type core_type = {
  ct_typedef : default_type_def;
  ct_ty : TYPE.t list -> TYPE.t
}

val register : string -> core_type -> unit
val fold : (string -> core_type -> 'a -> 'a) -> 'a -> 'a
val get_type : string -> TYPE.t list -> TYPE.t
val exists : string -> bool

val is_core_type : string Love_pervasives.Ident.t -> bool

val init : unit -> unit
