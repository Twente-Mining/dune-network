(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module TYPES : sig

  type ('tez_repr, 'period_repr) _parameters = {
    (* fixed *)
    proof_of_work_nonce_size : int option ;
    nonce_length : int option ;
    max_revelations_per_block : int option ;
    max_operation_data_length : int option;
    max_proposals_per_delegate : int option;

    (* parametric *)
    preserved_cycles: int option;
    blocks_per_cycle: int32 option;
    blocks_per_commitment: int32 option;
    blocks_per_roll_snapshot: int32 option;
    blocks_per_voting_period: int32 option;
    time_between_blocks: 'period_repr list option;
    endorsers_per_block: int option;
    hard_gas_limit_per_operation: Z.t option;
    hard_gas_limit_per_block: Z.t option;
    proof_of_work_threshold: int64 option;
    tokens_per_roll: 'tez_repr option;
    michelson_maximum_type_size: int option;
    seed_nonce_revelation_tip: Tez_repr.t option;
    origination_size: int option;
    block_security_deposit: Tez_repr.t option;
    endorsement_security_deposit: Tez_repr.t option;
    block_reward: Tez_repr.t option;
    endorsement_reward: Tez_repr.t option;
    cost_per_byte: Tez_repr.t option;
    hard_storage_limit_per_operation: Z.t option;
    hard_gas_limit_to_pay_fees: Z.t option;
    max_operation_ttl : int option;
    allow_collect_call : bool option;
    initial_endorsers : int option;
    delay_per_missing_endorsement : Period_repr.t option;

    (* Semantic protocol revisions *)
    protocol_revision: int option;
    protocol_actions : string list;
  }

  type ('commitment_repr, 'tez_repr, 'contract_repr) _accounts  = {
    commitments : 'commitment_repr list ;
    allow_peer_ids : string list ;
    revoke_peer_ids : string list ;
    actions : ( string * 'tez_repr * 'contract_repr list ) list ;
    fixes : (string list * MBytes.t option) list ;
  }

end

(* Signature re-exported by Alpha_context, to avoid multiple
   redefinitions of types and values *)

module type S = sig

  type tez_repr
  type period_repr
  type commitment_repr
  type contract_repr
  type parametric

  type parameters =
    (tez_repr, period_repr) TYPES._parameters
  type accounts =
    (commitment_repr, tez_repr, contract_repr) TYPES._accounts

  module Accounts : Dune_misc.BINARY_JSON with type t := accounts
  val create_for_action : string -> tez_repr -> contract_repr list -> accounts
  val create :
    ?commitments: commitment_repr list ->
    ?actions: (string * tez_repr * contract_repr list) list ->
    ?allow_peer_ids: string list ->
    ?revoke_peer_ids: string list ->
    ?fixes:(string list * MBytes.t option) list ->
    unit -> accounts

  val parameters_encoding : parameters Data_encoding.t
  val patch_constants : parameters -> parametric -> parametric
  val destruct_parameters_exn : Data_encoding.json -> parameters
end

include S with type tez_repr := Tez_repr.t
           and type period_repr := Period_repr.t
           and type commitment_repr := Commitment_repr.t
           and type contract_repr := Contract_repr.t
           and type parametric := Constants_repr.parametric
