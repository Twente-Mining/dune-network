(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type lang = {
  name : string ;
  version : (int * int) ;
}

module type S = sig
  type const
  type code
  (* type location *)
  val lang : lang
  val recognizes : string -> bool
  val unit : const
  val is_unit : const -> bool
  val const_encoding : const Data_encoding.t
  val code_encoding : code Data_encoding.t
  (* val location_encoding : unit Data_encoding.t *)
  val deserialized_const_cost : const -> Gas_limit_repr.cost
  val deserialized_code_cost : code -> Gas_limit_repr.cost
  val traversal_const_cost : const -> Gas_limit_repr.cost
  val traversal_code_cost : code -> Gas_limit_repr.cost
  val pp_const : Format.formatter -> const -> unit
end

module MapLang = Map.Make (struct
    type t = lang
    let compare
        { name = n1 ; version = v1, x1 }
        { name = n2 ; version = v2, x2 }  =
      match String.compare n1 n2 with
      | 0 ->
          begin match Compare.Int.compare v1 v2 with
            | 0 -> Compare.Int.compare x1 x2
            | c -> c
          end
      | c -> c
  end)
