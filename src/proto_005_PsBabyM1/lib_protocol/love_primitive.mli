(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_ast_types

type primitive_kind =
  | PrimFunction
  | PrimInfix
  | PrimPrefix

type extended_arg_def =
  | ADNone
  | ADContractType

type generic_primitive = {
  prim_name : string ;
  prim_kind : primitive_kind ;
  prim_ext_arg : extended_arg_def ;
  prim_type : TYPE.extended_arg -> TYPE.t ;
  prim_arity : int ;
  prim_id : int ;
}

type t = generic_primitive

val compare_extended_arg : TYPE.extended_arg -> TYPE.extended_arg -> int

val is_infix : t -> bool
val is_prefix : t -> bool
val type_of : t * TYPE.extended_arg -> TYPE.t
val name : t -> string
val id_of_prim : t -> int
val arity : t -> int
val compare : t -> t -> int

val max_possible_prim_id : int

val reset : unit -> unit

val of_string : string -> t
val infix_of_string : string -> t
val prefix_of_string : string -> t
val from_string : string -> generic_primitive option
val prim_of_id : int -> t

val add_primitive : generic_primitive -> unit

val get_primitives_as_modules :
  unit ->
  (string * generic_primitive) list *
  (Love_pervasives.Collections.StringMap.key *
   (string * generic_primitive) list) list
