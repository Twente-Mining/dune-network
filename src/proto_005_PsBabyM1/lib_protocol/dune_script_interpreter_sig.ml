(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context

module type S = sig

  module Repr : Dune_script_sig.S

  type execution_result = {
    ctxt : context ;
    storage : Repr.const ;
    result : Repr.const option ;
    big_map_diff : Contract.big_map_diff option ;
    operations : packed_internal_operation list ;
  }

  type fee_execution_result = {
    ctxt : context ;
    max_fee : Tez.t ;
    max_storage : Z.t ;
  }

  type val_execution_result = {
    ctxt : context;
    result : Repr.const;
  }

  val normalize_script :
    Alpha_context.t -> Contract_repr.t -> Repr.code -> Repr.const ->
      ((Repr.code * Repr.const * Repr.code option) *
       Alpha_context.t) tzresult Lwt.t

  val denormalize_script :
    Alpha_context.t -> Repr.code -> Repr.const -> Repr.code option ->
      ((Repr.code * Repr.const) * Alpha_context.t) tzresult Lwt.t

  val typecheck_code :
    Alpha_context.t -> Repr.code ->
      (Alpha_context.t) tzresult Lwt.t

  val typecheck_data :
    Alpha_context.t -> Repr.const -> Repr.const ->
      (Alpha_context.t) tzresult Lwt.t

  val typecheck :
    Alpha_context.t ->
    Script_interpreter.step_constants ->
    code:Repr.code ->
    storage:Repr.const ->
    ((Repr.code (* code *) *
        Repr.const (* storage *) *
          Repr.code option (* fee code *)) *
       Alpha_context.Contract.big_map_diff option *
         Alpha_context.t) tzresult Lwt.t

  val execute:
    Alpha_context.t ->
    Script_ir_translator.unparsing_mode ->
    Script_interpreter.step_constants ->
    code: Repr.code ->
    storage: Repr.const ->
    entrypoint: string ->
    parameter: Repr.const ->
    apply:(
      Alpha_context.t -> 'kind manager_operation ->
      (context *
       'kind Apply_results.successful_manager_operation_result *
       packed_internal_operation list) tzresult Lwt.t
    ) ->
    execution_result tzresult Lwt.t

  val execute_fee_script:
    Alpha_context.t ->
    Script_interpreter.step_constants ->
    fee_code: Repr.code ->
    storage: Repr.const ->
    entrypoint: string ->
    parameter: Repr.const ->
    fee_execution_result tzresult Lwt.t

  val execute_value:
    Alpha_context.t ->
    self:Contract_repr.t ->
    code: Repr.code ->
    storage: Repr.const ->
    val_name: string ->
    parameter: Repr.const option ->
    val_execution_result tzresult Lwt.t

  val get_entrypoint:
    Alpha_context.t ->
    code: Repr.code ->
    entrypoint: string ->
    (Alpha_context.t * Repr.const) tzresult Lwt.t

  val list_entrypoints:
    Alpha_context.t ->
    code: Repr.code ->
    (Alpha_context.t * (string * Repr.const) list) tzresult Lwt.t

  val pack_data:
    Alpha_context.t ->
    typ:Repr.const option ->
    data:Repr.const ->
    (Alpha_context.t * MBytes.t) tzresult Lwt.t

  val unpack_data:
    Alpha_context.t ->
    typ:Repr.const ->
    data:MBytes.t ->
    (Alpha_context.t * Repr.const) tzresult Lwt.t

  val hash_data:
    Alpha_context.t ->
    Repr.const ->
    (Alpha_context.t * Script_expr_hash.t) tzresult Lwt.t

  val to_michelson_const:
    Alpha_context.t ->
    Repr.const ->
    (Alpha_context.t * Script_repr.expr) tzresult Lwt.t

  val from_michelson_const:
    Alpha_context.t ->
    Script_repr.expr ->
    (Alpha_context.t * Repr.const) tzresult Lwt.t

  (* TODO: Code translators *)

(*
  val to_michelson_code:
    Alpha_context.t ->
    Repr.code ->
    (Alpha_context.t * Script_repr.expr option) tzresult Lwt.t

  val from_michelson_code:
    Alpha_context.t ->
    Script_repr.expr ->
    (Alpha_context.t * Repr.code option) tzresult Lwt.t *)
end
