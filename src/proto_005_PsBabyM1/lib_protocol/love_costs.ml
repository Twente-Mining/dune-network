(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives


let int_cost (n, w) i =
  (n + 1, w + 1 + (Z.numbits i + 63) / 64)

let string_cost (n, w) s =
  (n + 1, w + 1 + (String.length s + 7) / 8)

let bytes_cost (n, w) s =
  (n + 2, w + 1 + (MBytes.length s + 7) / 8 + 12)

let ident_cost (n, w) id =
  List.fold_left (fun (n, w) s ->
      string_cost (n + 1, w + 1) s
    ) (n, w) (Ident.get_list id)

let opt_cost (n, w) content_cost = function
  | None -> (n + 1, w + 1)
  | Some c -> content_cost (n + 1, w + 1) c



(* Types *)

module Type = struct

open Love_type

let type_var_cost (n, w) Love_type.{ tv_name; tv_traits = _ } =
  string_cost (n + 2, w + 2) tv_name

let kind_cost (n, w) k =
  match k with
  | Module -> (n + 1, w + 1)
  | Contract dl ->
      List.fold_left (fun (n, w) (s1, s2) ->
          string_cost (string_cost (n, w) s1) s2
        ) (n + 1, w + 1) dl

let rec signature_cost (n, w) Love_type.{ sig_kind; sig_content } =
  let sig_type_cost (n, w) = function
    | SAbstract tvl -> List.fold_left type_var_cost (n + 1, w + 1) tvl
    | SPrivate td -> typedef_cost (n + 1, w + 1) td
    | SPublic td -> typedef_cost (n + 1, w + 1) td
  in
  let sig_content_cost (n, w) scm =
    List.fold_left (fun (n, w) (cn, sc) ->
        let n, w = (n + 1, w + 1 + (String.length cn)) in
        match sc with
        | SType st -> sig_type_cost (n + 1, w + 1) st
        | SException tl -> List.fold_left type_cost (n + 1, w + 1) tl
        | SValue t -> type_cost (n + 1, w + 1) t
        | SEntry t -> type_cost (n + 1, w + 1) t
        | SView (t1, t2) -> type_cost (type_cost (n + 1, w + 1) t1) t2
        | SStructure st -> struct_type_cost (n + 1, w + 1) st
        | SSignature ss -> signature_cost (n + 1, w + 1) ss
      ) (n, w) scm
  in
  sig_content_cost (kind_cost (n, w) sig_kind) sig_content

and type_cost (n, w) (t : Love_type.t) =
  match t with
  | TUnit | TBool | TString | TBytes | TInt | TNat
  | TDun | TKey | TKeyHash | TSignature | TTimestamp
  | TAddress | TOperation ->
      (n + 1, w + 1)
  | TOption t | TList t | TSet t | TEntryPoint t ->
      type_cost (n + 1, w + 1) t
  | TMap (t1, t2) | TBigMap (t1, t2) | TArrow (t1, t2)| TView (t1, t2) ->
      type_cost (type_cost (n + 1, w + 1) t1) t2
  | TTuple tl ->
      List.fold_left type_cost (n + 1, w + 1) tl
  | TUser (id, tl) ->
      List.fold_left type_cost (ident_cost (n + 1, w + 1) id) tl
  | TPackedStructure st
  | TContractInstance st ->
      struct_type_cost (n + 1, w + 1) st
  | TVar tv ->
      type_var_cost (n + 1, w + 1) tv
  | TForall (tv, t) ->
      type_cost (type_var_cost (n + 1, w + 1) tv) t


and typedef_cost (n, w) (td : Love_type.typedef) =
  match td with
  | Alias { aparams; atype } ->
      List.fold_left type_var_cost (type_cost (n + 1, w + 1) atype) aparams
  | SumType { sparams; scons; srec = _ } ->
      let n, w = List.fold_left (fun (n, w) (s, tl) ->
          List.fold_left (type_cost) (string_cost (n, w) s) tl
        ) (n + 2, w + 2) scons in
      List.fold_left type_var_cost (n, w) sparams
  | RecordType { rparams; rfields; rrec = _ } ->
      let n, w = List.fold_left (fun (n, w) (s, t) ->
          type_cost (string_cost (n, w) s) t
        ) (n + 2, w + 2) rfields in
    List.fold_left type_var_cost (n, w) rparams

and struct_type_cost (n, w) st =
  match st with
  | Named id -> ident_cost (n + 1, w + 1) id
  | Anonymous s -> signature_cost (n + 1, w + 1) s

let contract_type_cost (n, w) ct =
  match ct with
  | StructType st -> struct_type_cost (n + 1, w + 1) st
  | ContractInstance stn -> ident_cost (n + 1, w + 1) stn

end



(* Primitives *)

module Primitive = struct

open Type
open Love_primitive

let prim_cost (n, w) (p : Love_primitive.t) =
  match p with
  | PBUnpack t -> type_cost (n + 1, w + 1) t
  | PBMEmpty (t1, t2) -> type_cost (type_cost (n + 1, w + 1) t1) t2
  | PCSelf ct | PCAt ct -> contract_type_cost (n + 1, w + 1) ct
  | _ -> (n + 1, w + 1)

end



(* Raw Ast *)

module Ast = struct

open Type
open Love_ast

let location_cost (n, w) _l =
  (n + 2, w + 2)

let exn_cost (n, w) (s : Love_ast.exn_name) =
  match s with
  | Fail t -> type_cost (n + 1, w + 1) t
  | Exception id -> ident_cost (n + 1, w + 1) id

let const_cost (n, w) (c : Love_ast.const) =
  let n, w = opt_cost (n, w) location_cost c.annot in
  match c.content with
  | CUnit | CBool _ -> (n + 1, w + 1)
  | CString s -> string_cost (n, w) s
  | CBytes b -> bytes_cost (n, w) b
  | CInt i | CNat i ->  int_cost (n, w) i
  | CDun _ -> (n + 1, w + 1)
  | CKey _ -> (n + 1, w + 1 + 7)
  | CKeyHash _ -> (n + 1, w + 1 + 4)
  | CSignature _ -> (n + 1, w + 1 + 12)
  | CTimestamp _ -> (n + 1, w + 2)
  | CAddress _ -> (n + 1, w + 1 + 4)
  | CPrimitive p -> Primitive.prim_cost (n + 1, w + 1) p

let rec pattern_cost (n, w) (p : Love_ast.pattern) =
  let n, w = opt_cost (n, w) location_cost p.annot in
  match p.content with
  | PAny | PNone -> (n + 1, w + 1)
  | PVar v -> string_cost (n + 1, w + 1) v
  | PAlias (p, v) -> pattern_cost (string_cost (n + 1, w + 1) v) p
  | PConst c -> const_cost (n + 1, w + 1) c
  | PSome p -> pattern_cost (n + 1, w + 1) p
  | PList pl | PTuple pl -> List.fold_left pattern_cost (n + 1, w + 1) pl
  | PConstr (c, pl) ->
    List.fold_left pattern_cost (string_cost (n + 1, w + 1) c) pl
  | PContract (cn, st) ->
    struct_type_cost (string_cost (n + 1, w + 1) cn) st

let rec exp_cost (n, w) (e : Love_ast.exp) =
  let n, w = opt_cost (n, w) location_cost e.annot in
  match e.content with
  | Const c ->
      const_cost (n + 1, w + 1) c
  | Var id ->
      ident_cost (n + 1, w + 1) id
  | Let { bnd_pattern = p; bnd_val = e1; body = e2 } ->
      exp_cost (exp_cost (pattern_cost (n + 1, w + 1) p) e1) e2
  | LetRec { bnd_var = v; bnd_val = e1; body = e2; fun_typ = t } ->
      type_cost (exp_cost (exp_cost (string_cost (n + 1, w + 1) v) e1) e2) t
  | Lambda l ->
      lambda_cost (n + 1, w + 1) l
  | TLambda { targ = tv; exp = e } ->
      exp_cost (type_var_cost (n + 1, w + 1) tv) e
  | Apply { fct = e; args = el } ->
      List.fold_left exp_cost (exp_cost (n + 1, w + 1) e) el
  | TApply { exp = e; typ = t } ->
      type_cost (exp_cost (n + 1, w + 1) e) t
  | Seq el ->
      List.fold_left exp_cost (n + 1, w + 1) el
  | If { cond = e1; ifthen = e2; ifelse = e3 } ->
      exp_cost (exp_cost (exp_cost (n + 1, w + 1) e1) e2) e3
  | Match { arg = e; cases = pel } ->
      List.fold_left (fun (n, w) (p, e) ->
          exp_cost (pattern_cost (n, w) p) e
        ) (exp_cost (n + 1, w + 1) e) pel
  | Constructor { constr = c; args = el; _ } ->
      List.fold_left exp_cost (ident_cost (n + 1, w + 1) c) el
  | ENone ->
      (n + 1, w + 1)
  | ESome e ->
      exp_cost (n + 1, w + 1) e
  | Nil ->
      (n + 1, w + 1)
  | List (el, e) ->
      List.fold_left exp_cost (exp_cost (n + 1, w + 1) e) el
  | Tuple el ->
      List.fold_left exp_cost (n + 1, w + 1) el
  | Project { tuple = e; indexes = il } ->
      let l = List.length il in
      exp_cost (n + 1, w + 1 + l) e
  | Update { tuple = e; updates = iel } ->
      List.fold_left (fun (n, w) (_, e) ->
          exp_cost (n + 1, w + 1) e
        ) (exp_cost (n + 1, w + 1) e) iel
  | Record { path = id_opt; contents = fel } ->
      let n, w = opt_cost (n, w) ident_cost id_opt in
      List.fold_left (fun (n, w) (f, e) ->
          exp_cost (string_cost (n, w) f) e
        ) (n + 1, w + 1) fel
  | GetField { record = e; field = f } ->
      exp_cost (string_cost (n, w) f) e
  | SetFields { record = e; updates = fel } ->
      List.fold_left (fun (n, w) (f, e) ->
          exp_cost (string_cost (n, w) f) e
        ) (exp_cost (n + 1, w + 1) e) fel
  | PackStruct r ->
      reference_cost (n + 1, w + 1) r
  | Raise { exn = en; args = el; loc = loc_opt } ->
      let n, w = opt_cost (n, w) location_cost loc_opt in
      List.fold_left exp_cost (exn_cost (n + 1, w + 1) en) el
  | TryWith { arg = e; cases = pel } ->
      List.fold_left (fun (n, w) ((en, pl), e) ->
          List.fold_left pattern_cost (exp_cost (exn_cost (n, w) en) e) pl
        ) (exp_cost (n + 1, w + 1) e) pel

and lambda_cost (n, w) { arg_pattern = p; body = e; arg_typ = t } =
  type_cost (exp_cost (pattern_cost (n, w) p) e) t

and reference_cost (n, w) sr =
  match sr with
  | Named id -> ident_cost (n + 1, w + 1) id
  | Anonymous s -> structure_cost (n + 1, w + 1) s

and entry_cost (n, w) Love_ast.{
    entry_code = e; entry_fee_code = e_opt; entry_typ = t } =
  let n, w = opt_cost (n, w) exp_cost e_opt in
  type_cost (exp_cost (n + 1, w + 1) e) t

and view_cost (n, w) Love_ast.{ view_code = e; view_typ = t } =
  type_cost (type_cost (exp_cost (n + 1, w + 1) e) (fst t)) (snd t)

and value_cost (n, w) Love_ast.{
    value_code = e; value_typ = t; value_visibility = _ } =
  type_cost (exp_cost (n + 2, w + 2) e) t

and content_cost (n, w) (c : Love_ast.content) =
  let open Love_ast in
  match c with
  | DefType (_k, td) -> typedef_cost (n + 2, w + 2) td
  | DefException tl -> List.fold_left type_cost (n + 1, w + 1) tl
  | Entry e -> entry_cost (n + 1, w + 1) e
  | View v -> view_cost (n + 1, w + 1) v
  | Value v -> value_cost (n + 1, w + 1) v
  | Structure s -> structure_cost (n + 1, w + 1) s
  | Signature s -> signature_cost (n + 1, w + 1) s

and structure_cost (n, w) Love_ast.{ structure_content = sc; kind = sk } =
  List.fold_left (fun (n, w) (cn, c) ->
      content_cost (string_cost (n, w) cn) c
    ) (kind_cost (n + 1, w + 1) sk) sc

let top_contract_cost (n, w) Love_ast.{ version = _; code } =
  structure_cost (n + 2, w + 2) code

end



(* Runtime Ast *)

module Runtime = struct

open Type
open Love_runtime_ast

let location_cost (n, w) _l =
  (n + 2, w + 2)

let exn_cost (n, w) (s : Love_runtime_ast.exn_name) =
  match s with
  | RFail t -> type_cost (n + 1, w + 1) t
  | RException id -> ident_cost (n + 1, w + 1) id

let const_cost (n, w) (c : Love_runtime_ast.const) =
  match c with
  | RCUnit | RCBool _ -> (n + 1, w + 1)
  | RCString s -> string_cost (n, w) s
  | RCBytes b -> bytes_cost (n, w) b
  | RCInt i | RCNat i ->  int_cost (n, w) i
  | RCDun _ -> (n + 1, w + 1)
  | RCKey _ -> (n + 1, w + 1 + 7)
  | RCKeyHash _ -> (n + 1, w + 1 + 4)
  | RCSignature _ -> (n + 1, w + 1 + 12)
  | RCTimestamp _ -> (n + 1, w + 2)
  | RCAddress _ -> (n + 1, w + 1 + 4)
  | RCPrimitive p -> Primitive.prim_cost (n + 1, w + 1) p

let rec pattern_cost (n, w) (p : Love_runtime_ast.pattern) =
  match p with
  | RPAny | RPNone -> (n + 1, w + 1)
  | RPVar v -> string_cost (n + 1, w + 1) v
  | RPAlias (p, v) -> pattern_cost (string_cost (n + 1, w + 1) v) p
  | RPConst c -> const_cost (n + 1, w + 1) c
  | RPSome p -> pattern_cost (n + 1, w + 1) p
  | RPList pl | RPTuple pl -> List.fold_left pattern_cost (n + 1, w + 1) pl
  | RPConstr (c, pl) ->
      List.fold_left pattern_cost (string_cost (n + 1, w + 1) c) pl
  | RPContract (cn, st) ->
      struct_type_cost (string_cost (n + 1, w + 1) cn) st

let rec exp_cost (n, w) (e : Love_runtime_ast.exp) =
  match e with
  | RConst c ->
      const_cost (n + 1, w + 1) c
  | RVar id ->
      ident_cost (n + 1, w + 1) id
  | RLet { bnd_pattern = p; bnd_val = e1; body = e2 } ->
      exp_cost (exp_cost (pattern_cost (n + 1, w + 1) p) e1) e2
  | RLetRec { bnd_var = v; bnd_val = e1; body = e2; val_typ = t } ->
      type_cost (exp_cost (exp_cost (string_cost (n + 1, w + 1) v) e1) e2) t
  | RLambda l ->
      lambda_cost (n + 1, w + 1) l
  | RApply { exp = e; args = pl } ->
      List.fold_left param_cost (exp_cost (n, w) e) pl
  | RSeq el ->
      List.fold_left exp_cost (n + 1, w + 1) el
  | RIf { cond = e1; ifthen = e2; ifelse = e3 } ->
      exp_cost (exp_cost (exp_cost (n + 1, w + 1) e1) e2) e3
  | RMatch { arg = e; cases = pel } ->
      List.fold_left (fun (n, w) (p, e) ->
          exp_cost (pattern_cost (n, w) p) e
        ) (exp_cost (n + 1, w + 1) e) pel
  | RConstructor { type_name = id; constr = c; args = el; ctyps = _ } ->
      List.fold_left exp_cost (string_cost (ident_cost (n + 1, w + 1) id) c) el
  | RNone ->
      (n + 1, w + 1)
  | RSome e ->
      exp_cost (n + 1, w + 1) e
  | RNil ->
      (n + 1, w + 1)
  | RList (el, e) ->
      List.fold_left exp_cost (exp_cost (n + 1, w + 1) e) el
  | RTuple el ->
      List.fold_left exp_cost (n + 1, w + 1) el
  | RProject { tuple = e; indexes = il } ->
      let l = List.length il in
      exp_cost (n + 1, w + 1 + l) e
  | RUpdate { tuple = e; updates = iel } ->
      List.fold_left (fun (n, w) (_, e) ->
          exp_cost (n + 1, w + 1) e
        ) (exp_cost (n + 1, w + 1) e) iel
  | RRecord { type_name = id; contents = fel } ->
      List.fold_left (fun (n, w) (f, e) ->
          exp_cost (string_cost (n, w) f) e
        ) (ident_cost (n + 1, w + 1) id) fel
  | RGetField { record = e; field = f } ->
      exp_cost (string_cost (n, w) f) e
  | RSetFields { record = e; updates = fel } ->
      List.fold_left (fun (n, w) (f, e) ->
          exp_cost (string_cost (n, w) f) e
        ) (exp_cost (n + 1, w + 1) e) fel
  | RPackStruct r ->
      reference_cost (n + 1, w + 1) r
  | RRaise { exn = en; args = el; loc = loc_opt } ->
      let n, w = opt_cost (n, w) location_cost loc_opt in
      List.fold_left exp_cost (exn_cost (n + 1, w + 1) en) el
  | RTryWith { arg = e; cases = pel } ->
      List.fold_left (fun (n, w) ((en, pl), e) ->
          List.fold_left pattern_cost (exp_cost (exn_cost (n, w) en) e) pl
        ) (exp_cost (n + 1, w + 1) e) pel

and binding_cost (n, w) b =
  let open Love_runtime_ast in
  match b with
  | RPattern (p, t) -> type_cost (pattern_cost (n + 1, w + 1) p) t
  | RTypeVar tv -> type_var_cost (n + 1, w + 1) tv

and param_cost (n, w) p =
  match p with
  | RExp e -> exp_cost (n + 1, w + 1) e
  | RType t -> type_cost (n + 1, w + 1) t

and lambda_cost (n, w) { args = bl; body = e } =
  List.fold_left binding_cost (exp_cost (n, w) e) bl

and reference_cost (n, w) sr =
  match sr with
  | RNamed id -> ident_cost (n + 1, w + 1) id
  | RAnonymous s -> structure_cost (n + 1, w + 1) s

and entry_cost (n, w)
    { entry_code = e; entry_fee_code = e_opt; entry_typ = t } =
  let n, w = opt_cost (n, w) exp_cost e_opt in
  type_cost (exp_cost (n + 1, w + 1) e) t

and view_cost (n, w) { view_code = e; view_typ = t } =
  type_cost (type_cost (exp_cost (n + 1, w + 1) e) (fst t)) (snd t)

and value_cost (n, w) { value_code = e; value_typ = t; value_visibility = _ } =
  type_cost (exp_cost (n + 2, w + 2) e) t

and content_cost (n, w) (c : Love_runtime_ast.content) =
  match c with
  | RDefType (_k, td) -> typedef_cost (n + 2, w + 2) td
  | RDefException tl -> List.fold_left type_cost (n + 1, w + 1) tl
  | REntry e -> entry_cost (n + 1, w + 1) e
  | RView v -> view_cost (n + 1, w + 1) v
  | RValue v -> value_cost (n + 1, w + 1) v
  | RStructure s -> structure_cost (n + 1, w + 1) s
  | RSignature s -> signature_cost (n + 1, w + 1) s

and structure_cost (n, w) { structure_content = sc; kind = sk } =
  List.fold_left (fun (n, w) (cn, c) ->
      content_cost (string_cost (n, w) cn) c
    ) (kind_cost (n + 1, w + 1) sk) sc

let top_contract_cost (n, w) { version = _; code } =
  structure_cost (n + 2, w + 2) code

end



(* Values *)

module Value = struct

open Love_value
open Value
open Type

let rec value_cost (n, w) (v : Value.t) =
  match v with
  | VUnit | VNone | VBool _ -> (n + 1, w + 1)
  | VString s -> string_cost (n, w) s
  | VBytes b -> bytes_cost (n, w) b
  | VInt i | VNat i ->  int_cost (n, w) i
  | VDun _ -> (n + 1, w + 1)
  | VKey _ -> (n + 1, w + 1 + 7)
  | VKeyHash _ -> (n + 1, w + 1 + 4)
  | VSignature _ -> (n + 1, w + 1 + 12)
  | VTimestamp _ -> (n + 1, w + 2)
  | VAddress _ -> (n + 1, w + 1 + 4)
  | VOperation (op, bmd_opt) ->
      opt_cost (op_cost (n + 1, w + 1) op) bmd_cost bmd_opt
  | VContractInstance _ ->
      (n + 1, w + 1 + 4)
  | VPackedStructure (id, c) ->
      live_contract_cost (ident_cost (n, w) id) c
  | VEntryPoint (_, s) | VView (_, s) ->
      string_cost (n + 1, w + 4) s
  | VSome v ->
      value_cost (n + 1, w + 1) v
  | VList vl | VTuple vl ->
      List.fold_left value_cost (n + 1, w + 1) vl
  | VConstr (cstr, vl) ->
      List.fold_left value_cost (string_cost (n + 1, w + 1) cstr) vl
  | VPrimitive (p, vl) ->
      List.fold_left value_cost (Primitive.prim_cost (n + 1, w + 1) p) vl
  | VRecord fvl ->
      List.fold_left (fun (n, w) (f, v) ->
          value_cost (string_cost (n, w) f) v) (n + 1, w + 1) fvl
  | VSet vs ->
      List.fold_left value_cost (n + 1, w + 1) (ValueSet.elements vs)
  | VMap vm ->
      List.fold_left (fun (n, w) (k, v) ->
          value_cost (value_cost (n, w) k) v
      ) (n + 1, w + 1) (ValueMap.bindings vm)
  | VBigMap { id = i_opt; diff; key_type; value_type } ->
      let n, w = opt_cost (n, w) int_cost i_opt in
      let n, w = List.fold_left (fun (n, w) (k, v_opt) ->
          opt_cost (value_cost (n, w) k) value_cost v_opt
        ) (n, w) (ValueMap.bindings diff)
      in
      type_cost (type_cost (n, w) key_type) value_type
  | VClosure { call_env = { values; structs; sigs; exns; types; tvars };
               lambda } ->
      let n, w = List.fold_left (fun (n, w) (id, vc) ->
        let n, w = ident_cost (n, w) id in
        match vc with
        | Local v' -> (* Warning : recursive value *)
            if v == v' then (n + 1, w + 1)
            else value_cost (n + 1, w + 1) v'
        | Global (Inlined (p, v)) ->
            value_cost (ident_cost (n + 1, w + 1) p) v
        | Global (Pointer p) ->
            ident_cost (n + 1, w + 1) p
      ) (n, w) values
      in
      let n, w = List.fold_left (fun (n, w) (id, pi) ->
          let n, w = ident_cost (n, w) id in
          match pi with
          | Inlined (p, s) ->
              live_structure_cost (ident_cost (n + 1, w + 1) p) s
          | Pointer p ->
              ident_cost (n + 1, w + 1) p
        ) (n, w) structs
      in
      let n, w = List.fold_left (fun (n, w) (id, pi) ->
          let n, w = ident_cost (n, w) id in
          match pi with
          | Inlined (p, s) ->
              signature_cost (ident_cost (n + 1, w + 1) p) s
          | Pointer p ->
              ident_cost (n + 1, w + 1) p
        ) (n, w) sigs
      in
      let n, w = List.fold_left (fun (n, w) (ids, idd) ->
          ident_cost (ident_cost (n, w) ids) idd) (n, w) exns in
      let n, w = List.fold_left (fun (n, w) (ids, idd) ->
          ident_cost (ident_cost (n, w) ids) idd) (n, w) types in
      let n, w = List.fold_left (fun (n, w) (s, t) ->
          type_cost (string_cost (n, w) s) t) (n, w) tvars in
      Runtime.lambda_cost (n, w) lambda

and op_cost (n, w) Op.{ source = _; operation; nonce = _ } =
  let n, w = (n + 1, w + 5) in
  match operation with
  | Origination { delegate = pkh_opt; script = (v, c);
                  credit = _; preorigination = a_opt } ->
       let n, w = opt_cost (n, w) (fun (n, w) _ -> (n + 1, w + 3)) pkh_opt in
       let n, w = value_cost (n, w) v in
       let n, w = live_contract_cost (n, w) c in
       let n, w = opt_cost (n, w) (fun (n, w) _ -> (n + 1, w + 3)) a_opt in
       (n + 2, w + 2)
  | Transaction { amount = _; parameters = v_opt;
                  entrypoint = s; destination = _ } ->
       let n, w = opt_cost (n, w) value_cost v_opt in
       let n, w = string_cost (n, w) s in
       (n + 2, w + 2)
  | Delegation pkh_opt ->
      opt_cost (n + 1, w + 1) (fun (n, w) _ -> (n + 1, w + 3)) pkh_opt
  | Dune_manage_account { target = _; maxrolls = _; admin = _;
                          white_list; delegation = _ } ->
      let n, w = (n + 1, w + 1 + 4) in (* target *)
      let n, w = (n + 2, w + 2 + 1) in (* maxrolls *)
      let n, w = (n + 2, w + 2 + 4) in (* admin *)
      let n, w = (n + 2, w + 2) in (* delegation *)
      let n, w = opt_cost (n, w) (fun (n, w) l ->
          let l = List.length l in
          (n + 1 + l, w + 1 + l * 4)
        ) white_list
      in
      (n + 2, w + 2)

and bmd_cost (n, w) (bmd : Op.big_map_diff) =
  let open Op in
  List.fold_left (fun (n, w) di ->
      match di with
      | Update { big_map = i; diff_key = v;
                 diff_key_hash = _; diff_value = v_opt } ->
          opt_cost (value_cost (int_cost (n + 1, w + 4) i) v) value_cost v_opt
      | Clear i -> int_cost (n + 1, w + 1) i
      | Copy (i1, i2) -> int_cost (int_cost (n + 1, w + 1) i1) i2
      | Alloc { big_map = i; key_type = t1; value_type = t2 } ->
          type_cost (type_cost (int_cost (n + 1, w + 1) i) t1) t2
    ) (n, w) bmd

and entry_cost (n, w)
    LiveStructure.{ ventry_code = v; ventry_fee_code = v_opt; ventry_typ = t } =
  let n, w = opt_cost (n, w) value_cost v_opt in
  type_cost (value_cost (n + 1, w + 1) v) t

and view_cost (n, w) LiveStructure.{ vview_code = v; vview_typ = t } =
  type_cost (type_cost (value_cost (n + 1, w + 1) v) (fst t)) (snd t)

and vvalue_cost (n, w)
    LiveStructure.{ vvalue_code = v; vvalue_typ = t; vvalue_visibility = _ } =
  type_cost (value_cost (n + 2, w + 2) v) t

and content_cost (n, w) (c : LiveStructure.content) =
  match c with
  | VType (_k, td) -> typedef_cost (n + 2, w + 2) td
  | VException tl -> List.fold_left type_cost (n + 1, w + 1) tl
  | VEntry e -> entry_cost (n + 1, w + 1) e
  | VView v -> view_cost (n + 1, w + 1) v
  | VValue v -> vvalue_cost (n + 1, w + 1) v
  | VStructure s -> live_structure_cost (n + 1, w + 1) s
  | VSignature s -> signature_cost (n + 1, w + 1) s

and live_structure_cost (n, w) LiveStructure.{ kind = sk; content = sc } =
  List.fold_left (fun (n, w) (cn, c) ->
      content_cost (string_cost (n, w) cn) c
    ) (kind_cost (n + 1, w + 1) sk) sc

and live_contract_cost (n, w) LiveContract.{ version = _; root_struct = s } =
  live_structure_cost (n + 2, w + 2) s

and fee_code_cost (n, w)
    FeeCode.{ version = _; root_struct = s; fee_codes = fcl } =
  List.fold_left (fun (n, w) (cn, (v, t)) ->
      type_cost (value_cost (string_cost (n, w) cn) v) t
    ) (live_structure_cost (n + 2, w + 2) s) fcl

end
