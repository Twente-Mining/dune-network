(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module PROTOCOL : sig
  val is_action : string -> bool

  val perform_action :
    Raw_context.t -> string -> Raw_context.t tzresult Lwt.t
end

module ACCOUNTS : sig
  val perform_action :
    Raw_context.t -> Delegate_storage.balance_updates ->
    string -> Tez_repr.t -> Contract_repr.t list ->
    ( Raw_context.t * Delegate_storage.balance_updates ) tzresult Lwt.t
end

module ACCOUNT : sig

  val perform_action :
    Raw_context.t -> Delegate_storage.balance_updates ->
    string -> Contract_repr.t -> Love_value.Value.t ->
    ( Raw_context.t * Delegate_storage.balance_updates ) tzresult Lwt.t

end
