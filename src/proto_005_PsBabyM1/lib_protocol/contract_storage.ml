(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Dune_lang_repr.For_all (* Script_repr = Dune_lang_repr *)

type error +=
  | Balance_too_low of Contract_repr.contract * Tez_repr.t * Tez_repr.t (* `Temporary *)
  | Counter_in_the_past of Contract_repr.contract * Z.t * Z.t (* `Branch *)
  | Counter_in_the_future of Contract_repr.contract * Z.t * Z.t (* `Temporary *)
  | Unspendable_contract of Contract_repr.contract (* `Permanent *)
  | Non_existing_contract of Contract_repr.contract (* `Temporary *)
  | Empty_implicit_contract of Signature.Public_key_hash.t (* `Temporary *)
  | Empty_transaction of Contract_repr.t (* `Temporary *)
  | Inconsistent_hash of Signature.Public_key.t * Signature.Public_key_hash.t * Signature.Public_key_hash.t (* `Permanent *)
  | Inconsistent_public_key of Signature.Public_key.t * Signature.Public_key.t (* `Permanent *)
  | Previously_revealed_key of Contract_repr.t (* `Permanent *)
  | Unrevealed_manager_key of Contract_repr.t (* `Permanent *)
  | Unknown_hash_code of Script_expr_hash.t (* `Permanent *)

let () =
  register_error_kind
    `Permanent
    ~id:"contract.unspendable_contract"
    ~title:"Unspendable contract"
    ~description:"An operation tried to spend tokens from an unspendable contract"
    ~pp:(fun ppf c ->
        Format.fprintf ppf "The tokens of contract %a can only be spent by its script"
          Contract_repr.pp c)
    Data_encoding.(obj1 (req "contract" Contract_repr.encoding))
    (function Unspendable_contract c   -> Some c | _ -> None)
    (fun c -> Unspendable_contract c) ;
  register_error_kind
    `Temporary
    ~id:"contract.balance_too_low"
    ~title:"Balance too low"
    ~description:"An operation tried to spend more tokens than the contract has"
    ~pp:(fun ppf (c, b, a) ->
        Format.fprintf ppf "Balance of contract %a too low (%a) to spend %a"
          Contract_repr.pp c Tez_repr.pp b Tez_repr.pp a)
    Data_encoding.(obj3
                     (req "contract" Contract_repr.encoding)
                     (req "balance" Tez_repr.encoding)
                     (req "amount" Tez_repr.encoding))
    (function Balance_too_low (c, b, a)   -> Some (c, b, a) | _ -> None)
    (fun (c, b, a) -> Balance_too_low (c, b, a)) ;
  register_error_kind
    `Temporary
    ~id:"contract.counter_in_the_future"
    ~title:"Invalid counter (not yet reached) in a manager operation"
    ~description:"An operation assumed a contract counter in the future"
    ~pp:(fun ppf (contract, exp, found) ->
        Format.fprintf ppf
          "Counter %s not yet reached for contract %a (expected %s)"
          (Z.to_string found)
          Contract_repr.pp contract
          (Z.to_string exp))
    Data_encoding.
      (obj3
         (req "contract" Contract_repr.encoding)
         (req "expected" z)
         (req "found" z))
    (function Counter_in_the_future (c, x, y) -> Some (c, x, y) | _ -> None)
    (fun (c, x, y) -> Counter_in_the_future (c, x, y)) ;
  register_error_kind
    `Branch
    ~id:"contract.counter_in_the_past"
    ~title:"Invalid counter (already used) in a manager operation"
    ~description:"An operation assumed a contract counter in the past"
    ~pp:(fun ppf (contract, exp, found) ->
        Format.fprintf ppf
          "Counter %s already used for contract %a (expected %s)"
          (Z.to_string found)
          Contract_repr.pp contract
          (Z.to_string exp))
    Data_encoding.
      (obj3
         (req "contract" Contract_repr.encoding)
         (req "expected" z)
         (req "found" z))
    (function Counter_in_the_past (c, x, y) -> Some (c, x, y) | _ -> None)
    (fun (c, x, y) -> Counter_in_the_past (c, x, y)) ;
  register_error_kind
    `Temporary
    ~id:"contract.non_existing_contract"
    ~title:"Non existing contract"
    ~description:"A contract handle is not present in the context \
                  (either it never was or it has been destroyed)"
    ~pp:(fun ppf contract ->
        Format.fprintf ppf "Contract %a does not exist"
          Contract_repr.pp contract)
    Data_encoding.(obj1 (req "contract" Contract_repr.encoding))
    (function Non_existing_contract c -> Some c | _ -> None)
    (fun c -> Non_existing_contract c) ;
  register_error_kind
    `Permanent
    ~id:"contract.manager.inconsistent_hash"
    ~title:"Inconsistent public key hash"
    ~description:"A revealed manager public key is inconsistent with the announced hash"
    ~pp:(fun ppf (k, eh, ph) ->
        Format.fprintf ppf "The hash of the manager public key %s is not %a as announced but %a"
          (Signature.Public_key.to_b58check k)
          Signature.Public_key_hash.pp ph
          Signature.Public_key_hash.pp eh)
    Data_encoding.(obj3
                     (req "public_key" Signature.Public_key.encoding)
                     (req "expected_hash" Signature.Public_key_hash.encoding)
                     (req "provided_hash" Signature.Public_key_hash.encoding))
    (function Inconsistent_hash (k, eh, ph) -> Some (k, eh, ph) | _ -> None)
    (fun (k, eh, ph) -> Inconsistent_hash (k, eh, ph)) ;
  register_error_kind
    `Permanent
    ~id:"contract.manager.inconsistent_public_key"
    ~title:"Inconsistent public key"
    ~description:"A provided manager public key is different with the public key stored in the contract"
    ~pp:(fun ppf (eh, ph) ->
        Format.fprintf ppf "Expected manager public key %s but %s was provided"
          (Signature.Public_key.to_b58check ph)
          (Signature.Public_key.to_b58check eh))
    Data_encoding.(obj2
                     (req "public_key" Signature.Public_key.encoding)
                     (req "expected_public_key" Signature.Public_key.encoding))
    (function Inconsistent_public_key (eh, ph) -> Some (eh, ph) | _ -> None)
    (fun (eh, ph) -> Inconsistent_public_key (eh, ph)) ;
  register_error_kind
    `Branch
    ~id:"contract.unrevealed_key"
    ~title:"Manager operation precedes key revelation"
    ~description:
      "One tried to apply a manager operation \
       without revealing the manager public key"
    ~pp:(fun ppf s ->
        Format.fprintf ppf "Unrevealed manager key for contract %a."
          Contract_repr.pp s)
    Data_encoding.(obj1 (req "contract" Contract_repr.encoding))
    (function Unrevealed_manager_key s -> Some s | _ -> None)
    (fun s -> Unrevealed_manager_key s) ;
  register_error_kind
    `Branch
    ~id:"contract.previously_revealed_key"
    ~title:"Manager operation already revealed"
    ~description:
      "One tried to revealed twice a manager public key"
    ~pp:(fun ppf s ->
        Format.fprintf ppf "Previously revealed manager key for contract %a."
          Contract_repr.pp s)
    Data_encoding.(obj1 (req "contract" Contract_repr.encoding))
    (function Previously_revealed_key s -> Some s | _ -> None)
    (fun s -> Previously_revealed_key s) ;
  register_error_kind
    `Branch
    ~id:"implicit.empty_implicit_contract"
    ~title:"Empty implicit contract"
    ~description:"No manager operations are allowed on an empty implicit contract."
    ~pp:(fun ppf implicit ->
        Format.fprintf ppf
          "Empty implicit contract (%a)"
          Signature.Public_key_hash.pp implicit)
    Data_encoding.(obj1 (req "implicit" Signature.Public_key_hash.encoding))
    (function Empty_implicit_contract c -> Some c | _ -> None)
    (fun c -> Empty_implicit_contract c) ;
  register_error_kind
    `Branch
    ~id:"contract.empty_transaction"
    ~title:"Empty transaction"
    ~description:(Format.asprintf
                    "Forbidden to credit 0%s to a contract without code."
                    Config.config.cursym)
    ~pp:(fun ppf contract ->
        Format.fprintf ppf
          "Transaction of 0%s towards a contract without code are forbidden (%a)."
          Tezos_base.Config.config.cursym
          Contract_repr.pp contract)
    Data_encoding.(obj1 (req "contract" Contract_repr.encoding))
    (function Empty_transaction c -> Some c | _ -> None)
    (fun c -> Empty_transaction c)

let () =
  register_error_kind
    `Branch
    ~id:"scripts.unknown_hash"
    ~title:"Unknown code hash"
    ~description:
      "The supplied hash does not match any know code in the storage"
    ~pp:(fun ppf s ->
        Format.fprintf ppf "Unknown code hash %a."
          Script_expr_hash.pp s)
    Data_encoding.(obj1 (req "code_hash" Script_expr_hash.encoding))
    (function Unknown_hash_code h -> Some h | _ -> None)
    (fun h -> Unknown_hash_code h)

let hash_code code =
   Lwt.return (Script_repr.force_decode code) >>=? fun (code, _gas_cost) ->
   let bytes =
     Data_encoding.Binary.to_bytes_exn Script_repr.expr_encoding code in
   return (Script_expr_hash.hash_bytes [ bytes ])

let hash_script_code { Script_repr.code ; storage = _ } = hash_code code

let store_hash_script_code c script =
  hash_script_code script >>=? fun hash ->
  Storage.Scripts.mem c hash >>= begin function
    | true -> return c
    | false -> Storage.Scripts.init c hash script.code
  end >>=? fun c ->
  return ( c, hash )

let failwith = Dune_misc.failwith

type big_map_diff_item =
  | Update of {
      big_map : Z.t;
      diff_key : Script_repr.expr;
      diff_key_hash : Script_expr_hash.t;
      diff_value : Script_repr.expr option;
    }
  | Clear of Z.t
  | Copy of Z.t * Z.t
  | Alloc of {
      big_map : Z.t;
      key_type : Script_repr.expr;
      value_type : Script_repr.expr;
    }

type big_map_diff = big_map_diff_item list

let big_map_diff_item_encoding =
  let open Data_encoding in
  union
    [ case (Tag 0) ~title:"update"
        (obj5
           (req "action" (constant "update"))
           (req "big_map" z)
           (req "key_hash" Script_expr_hash.encoding)
           (req "key" Script_repr.expr_encoding)
           (opt "value" Script_repr.expr_encoding))
        (function
          | Update { big_map ; diff_key_hash ; diff_key ; diff_value } ->
              Some ((), big_map, diff_key_hash, diff_key, diff_value)
          | _ -> None )
        (fun ((), big_map, diff_key_hash, diff_key, diff_value) ->
           Update { big_map ; diff_key_hash ; diff_key ; diff_value }) ;
      case (Tag 1) ~title:"remove"
        (obj2
           (req "action" (constant "remove"))
           (req "big_map" z))
        (function
          | Clear big_map ->
              Some ((), big_map)
          | _ -> None )
        (fun ((), big_map) ->
           Clear big_map) ;
      case (Tag 2) ~title:"copy"
        (obj3
           (req "action" (constant "copy"))
           (req "source_big_map" z)
           (req "destination_big_map" z))
        (function
          | Copy (src, dst) ->
              Some ((), src, dst)
          | _ -> None )
        (fun ((), src, dst) ->
           Copy (src, dst)) ;
      case (Tag 3) ~title:"alloc"
        (obj4
           (req "action" (constant "alloc"))
           (req "big_map" z)
           (req "key_type" Script_repr.expr_encoding)
           (req "value_type" Script_repr.expr_encoding))
        (function
          | Alloc { big_map ; key_type ; value_type } ->
              Some ((), big_map, key_type, value_type)
          | _ -> None )
        (fun ((), big_map, key_type, value_type) ->
           Alloc { big_map ; key_type ; value_type }) ]

let big_map_diff_encoding =
  let open Data_encoding in
  def "contract.big_map_diff" @@
  list big_map_diff_item_encoding

let big_map_key_cost = 65
let big_map_cost = 33

let update_script_big_map c = function
  | None -> return (c, Z.zero)
  | Some diff ->
      fold_left_s (fun (c, total) -> function
          | Clear id ->
              Storage.Big_map.Total_bytes.get c id >>=? fun size ->
              Storage.Big_map.remove_rec c id >>= fun c ->
              if Compare.Z.(id < Z.zero) then
                return (c, total)
              else
                return (c, Z.sub (Z.sub total size) (Z.of_int big_map_cost))
          | Copy (from, to_) ->
              Storage.Big_map.copy c ~from ~to_ >>=? fun c ->
              if Compare.Z.(to_ < Z.zero) then
                return (c, total)
              else
                Storage.Big_map.Total_bytes.get c from >>=? fun size ->
                return (c, Z.add (Z.add total size) (Z.of_int big_map_cost))
          | Alloc  { big_map ; key_type ; value_type } ->
              Storage.Big_map.Total_bytes.init c big_map Z.zero >>=? fun c ->
              (* Annotations are erased to allow sharing on
                 [Copy]. The types from the contract code are used,
                 these ones are only used to make sure they are
                 compatible during transmissions between contracts,
                 and only need to be compatible, annotations
                 nonwhistanding. *)
              let key_type = Script_repr.strip_annotations key_type in
              let value_type = Script_repr.strip_annotations value_type in
              Storage.Big_map.Key_type.init c big_map key_type >>=? fun c ->
              Storage.Big_map.Value_type.init c big_map value_type >>=? fun c ->
              if Compare.Z.(big_map < Z.zero) then
                return (c, total)
              else
                return (c, Z.add total (Z.of_int big_map_cost))
          | Update { big_map ; diff_key_hash ; diff_value = None } ->
              Storage.Big_map.Contents.remove (c, big_map) diff_key_hash
              >>=? fun (c, freed, existed) ->
              let freed = if existed then freed + big_map_key_cost else freed in
              Storage.Big_map.Total_bytes.get c big_map >>=? fun size ->
              Storage.Big_map.Total_bytes.set c big_map (Z.sub size (Z.of_int freed)) >>=? fun c ->
              if Compare.Z.(big_map < Z.zero) then
                return (c, total)
              else
                return (c, Z.sub total (Z.of_int freed))
          | Update { big_map ; diff_key_hash ; diff_value = Some v } ->
              Storage.Big_map.Contents.init_set (c, big_map) diff_key_hash v
              >>=? fun (c, size_diff, existed) ->
              let size_diff = if existed then size_diff else size_diff + big_map_key_cost in
              Storage.Big_map.Total_bytes.get c big_map >>=? fun size ->
              Storage.Big_map.Total_bytes.set c big_map (Z.add size (Z.of_int size_diff)) >>=? fun c ->
              if Compare.Z.(big_map < Z.zero) then
                return (c, total)
              else
                return (c, Z.add total (Z.of_int size_diff)))
        (c, Z.zero) diff

let create_base c
    ?(prepaid_bootstrap_storage=false) (* Free space for bootstrap contracts *)
    contract
    ~balance ~manager ~delegate ?script () =
  begin match Contract_repr.is_implicit contract with
    | None -> return c
    | Some _ ->
        Storage.Contract.Global_counter.get c >>=? fun counter ->
        Storage.Contract.Counter.init c contract counter
  end >>=? fun c ->
  Storage.Contract.Balance.init c contract balance >>=? fun c ->
  begin match manager with
    | Some manager ->
        Storage.Contract.Manager.init c contract (Manager_repr.Hash manager)
    | None -> return c
  end >>=? fun c ->
  begin
    match delegate with
    | None -> return c
    | Some delegate ->
        Delegate_storage.init c contract delegate
  end >>=? fun c ->
  match script with
  | Some (script, { Script_repr.code ; storage ; fee_code }, big_map_diff) ->
      Storage.Contract.Code.init c contract code >>=? fun (c, code_size) ->
      Storage.Contract.Storage.init c contract storage >>=? fun (c, storage_size) ->
      begin match fee_code with
        | None -> return (c, 0)
        | Some fee_code -> Storage.Contract.FeeCode.init c contract fee_code
      end >>=? fun (c, fee_code_size) ->
      store_hash_script_code c script >>=? fun (c, _code_hash) ->
      update_script_big_map c big_map_diff >>=? fun (c, big_map_size) ->
      let total_size = Z.add (Z.of_int fee_code_size) @@
        Z.add (Z.add (Z.of_int code_size) (Z.of_int storage_size)) big_map_size in
      assert Compare.Z.(total_size >= Z.zero) ;
      let prepaid_bootstrap_storage =
        if prepaid_bootstrap_storage then
          total_size
        else
          Z.zero
      in
      Storage.Contract.Paid_storage_space.init c contract prepaid_bootstrap_storage >>=? fun c ->
      Storage.Contract.Used_storage_space.init c contract total_size
  | None ->
      return c

let originate c ?prepaid_bootstrap_storage contract
    ~balance ~script ~delegate =
  create_base c ?prepaid_bootstrap_storage contract ~balance
    ~manager:None ~delegate ~script ()

let create_implicit c manager ~balance =
  create_base c (Contract_repr.implicit_contract manager)
    ~balance ~manager:(Some manager) ?script:None ~delegate:None ()

let delete c contract =
  match Contract_repr.is_implicit contract with
  | None ->
      (* For non implicit contract Big_map should be cleared *)
      failwith "Non implicit contracts cannot be removed"
  | Some _ ->
      Delegate_storage.remove c contract >>=? fun c ->
      Storage.Contract.Balance.delete c contract >>=? fun c ->
      Storage.Contract.Manager.delete c contract >>=? fun c ->
      Storage.Contract.Counter.delete c contract >>=? fun c ->
      Storage.Contract.Code.remove c contract >>=? fun (c, _, _) ->
      Storage.Contract.Storage.remove c contract >>=? fun (c, _, _) ->
      Storage.Contract.Paid_storage_space.remove c contract >>= fun c ->
      Storage.Contract.Used_storage_space.remove c contract >>= fun c ->
      (* dune *)
      Storage.Contract.Legacy2019.del c contract >>= fun c ->
      Storage.Contract.Delegation_closed.del c contract >>= fun c ->
      Storage.Contract.Admin.remove c contract >>= fun c ->
      Storage.Contract.Recovery.remove c contract >>= fun c ->
      Storage.Contract.WhiteList.clear (c, contract) >>= fun c ->
      Storage.Contract.Delegate_nrolls.remove c contract >>= fun c ->
      Storage.Contract.Delegate_maxrolls.remove c contract >>= fun c ->
      return c

let allocated c contract =
  Storage.Contract.Balance.get_option c contract >>=? function
  | None -> return_false
  | Some _ -> return_true

let exists c contract =
  match Contract_repr.is_implicit contract with
  | Some _ -> return_true
  | None -> allocated c contract

let must_exist c contract =
  exists c contract >>=? function
  | true -> return_unit
  | false -> fail (Non_existing_contract contract)

let must_be_allocated c contract =
  allocated c contract >>=? function
  | true -> return_unit
  | false ->
      match Contract_repr.is_implicit contract with
      | Some pkh -> fail (Empty_implicit_contract pkh)
      | None -> fail (Non_existing_contract contract)

let list c = Storage.Contract.list c

let fresh_contract_from_current_nonce c =
  Lwt.return (Raw_context.increment_origination_nonce c) >>=? fun (c, nonce) ->
  return (c, Contract_repr.originated_contract nonce)

let originated_from_current_nonce ~since: ctxt_since ~until: ctxt_until =
  Lwt.return (Raw_context.origination_nonce ctxt_since) >>=? fun since ->
  Lwt.return (Raw_context.origination_nonce ctxt_until) >>=? fun until ->
  filter_map_s
    (fun contract -> exists ctxt_until contract >>=? function
       | true -> return_some contract
       | false -> return_none)
    (Contract_repr.originated_contracts ~since ~until)

let get_counter c manager =
  let contract = Contract_repr.implicit_contract manager in
  Storage.Contract.Counter.get_option c contract >>=? function
  | None -> begin
      match Contract_repr.is_implicit contract with
      | Some _ -> Storage.Contract.Global_counter.get c
      | None -> return Z.zero (* instead of failwith "get_counter" *)
    end
  | Some v -> return v

let check_counter_increment c manager counter =
  get_counter c manager >>=? fun contract_counter ->
  let expected = Z.succ contract_counter in
  if Compare.Z.(expected = counter)
  then return_unit
  else
    let contract = Contract_repr.implicit_contract manager in
    if Compare.Z.(expected > counter) then
      fail (Counter_in_the_past (contract, expected, counter))
    else
      fail (Counter_in_the_future (contract, expected, counter))

let increment_counter c manager =
  let contract = Contract_repr.implicit_contract manager in
  Storage.Contract.Global_counter.get c >>=? fun global_counter ->
  Storage.Contract.Global_counter.set c (Z.succ global_counter) >>=? fun c ->
  Storage.Contract.Counter.get c contract >>=? fun contract_counter ->
  Storage.Contract.Counter.set c contract (Z.succ contract_counter)

let get_script_code c contract =
  Storage.Contract.Code.get_option c contract

let get_storage ctxt contract =
  Storage.Contract.Storage.get_option ctxt contract

let get_fee_code ctxt contract =
  Storage.Contract.FeeCode.get_option ctxt contract

let get_script ctxt contract =
  get_script_code ctxt contract >>=? fun (ctxt, code) ->
  get_storage ctxt contract >>=? fun (ctxt, storage) ->
  get_fee_code ctxt contract >>=? fun (ctxt, fee_code) ->
  match code, storage  with
  | None, Some _ | Some _, None -> failwith "missing code or storage"
  | None, None -> return (ctxt, None)
  | Some code, Some storage ->
      return (ctxt, Some { Script_repr.code ; storage ; fee_code })

let must_be_allocated_or_implicit c contract =
  match Contract_repr.is_implicit contract with
  | Some _ -> return_unit
  | None -> allocated c contract >>=? function
    | true -> return_unit
    | false -> fail (Non_existing_contract contract)

let get_hash_code ctxt code_hash =
  Storage.Scripts.get_option ctxt code_hash >>=? function
  | None -> fail (Unknown_hash_code code_hash)
  | Some code -> return code

let known_hash_code ctxt code_hash =
  Storage.Scripts.mem ctxt code_hash

let is_allowed_baker c contract =
  match Contract_repr.is_implicit contract with
  | None -> return_false
  | Some _ ->
      Storage.Contract.Allowed_baker.mem c contract >>= return

let change_allowed_baker c contract is_allowed_baker =
  match Contract_repr.is_implicit contract with
  | None ->
      if Dune_storage.has_protocol_revision c 3 then
        Dune_misc.failwith "change_allowed_baker: not an implicit contract"
      else
        return c
  | Some _ ->
      begin
        if is_allowed_baker then
          Storage.Contract.Allowed_baker.set c contract true
        else
          Storage.Contract.Allowed_baker.del c contract
      end >>= return

let is_superadmin c contract =
  Storage.Contract.Superadmin.mem c contract >>= return

let change_superadmin c contract superadmin =
  if superadmin then
    Storage.Contract.Superadmin.set c contract true
  else
    Storage.Contract.Superadmin.del c contract

let get_kyc c contract =
  match Contract_repr.is_implicit contract with
  | None -> return Dune_misc.KYC.all
  | Some _ ->
      Storage.Contract.KYC.get_option c contract >>=? function
      | None -> return Dune_misc.KYC.not_authorized
      | Some n -> return n

let change_kyc c contract kyc =
  match Tezos_base.Config.config.kyc_level with
  | Kyc_open -> Dune_misc.failwith "change_kyc: network has no KYC"
  | Kyc_source | Kyc_both ->
      match Contract_repr.is_implicit contract with
      | None -> Dune_misc.failwith "change_kyc: not an implicit contract"
      | Some _ ->
          begin
            if Compare.Int.( kyc = Dune_misc.KYC.not_authorized ) then
              Storage.Contract.KYC.remove c contract
            else
              Storage.Contract.KYC.init_set c contract kyc
          end
          >>= return

module MakeContractProperty(
    S:
      Storage_sigs.Indexed_data_storage
    with type key = Contract_repr.t
     and type value = Contract_repr.t
     and type t := Raw_context.t) = struct

  let get c contract = S.get_option c contract
  let set c contract admin =
    S.get_option c contract >>=? fun prev_admin ->
    match admin, prev_admin with
    | None, None -> return c
    | None, Some _ ->
        S.delete c contract
    | Some admin, Some prev_admin when Contract_repr.( admin = prev_admin ) ->
        return c
    | Some admin, _ ->
        S.init_set c contract admin >>= fun ctxt ->
        return ctxt
end
module ADMIN = MakeContractProperty(Storage.Contract.Admin)
let get_admin = ADMIN.get
let set_admin = ADMIN.set
module RECOVERY = MakeContractProperty(Storage.Contract.Recovery)
let get_recovery = RECOVERY.get
let set_recovery = RECOVERY.set

let get_whitelist c contract = Storage.Contract.WhiteList.elements (c, contract)
let set_whitelist c contract whitelist =
  begin
    if Compare.Int.(List.length whitelist > 4) then
      failwith "white-list cannot exceed 4 contracts"
    else
      return ()
  end >>=? fun () ->
  Storage.Contract.WhiteList.clear (c, contract) >>= fun c ->
  let rec iter c targets =
    match targets with
      [] -> return c
    | target :: targets ->
        Storage.Contract.WhiteList.add (c, contract) target >>= fun c ->
        iter c targets
  in
  iter c whitelist

let check_whitelisted c source maybe_destination =
  let contract = Contract_repr.implicit_contract source in
  begin
    Delegate_storage.is_frozen_account c contract >>=?
    function
    | Some 0l ->
        Dune_misc.failwith
          "Your account is frozen. You must delegate it to unfreeze it."
    | Some cycle ->
        Dune_misc.failwith
          "Your account is frozen until cycle %ld." cycle
    | None -> return ()
  end >>=? fun () ->
  get_whitelist c contract >>= function
  | [] -> return ()
  | white_list ->
      match maybe_destination with
      | None -> (* originations are impossible with a white-list *)
          failwith "Originations are forbidden for accounts with white-lists"
      | Some destination ->
          let rec iter destination white_list =
            match white_list with
            | [] -> failwith "Transfer forbidden to account outside white-list"
            | ele :: white_list ->
                if Contract_repr.equal ele destination then
                  return ()
                else
                  iter destination white_list
          in
          iter destination white_list

let set_delegation c contract delegation =
  Storage.Contract.Delegation_closed.set c contract (not delegation)
  >>= fun ctxt -> return ctxt

let get_delegation c contract =
  Storage.Contract.Delegation_closed.mem c contract >>= fun bool ->
  return (not bool)

let get_manager_004 c contract =
  Storage.Contract.Manager.get_option c contract >>=? function
  | None -> begin
      match Contract_repr.is_implicit contract with
      | Some manager -> return manager
      | None -> failwith "get_manager"
    end
  | Some (Manager_repr.Hash v) -> return v
  | Some (Manager_repr.Public_key v) -> return (Signature.Public_key.hash v)

let get_manager_key c manager =
  let contract = Contract_repr.implicit_contract manager in
  Storage.Contract.Manager.get_option c contract >>=? function
  | None -> failwith "get_manager_key"
  | Some (Manager_repr.Hash _) -> fail (Unrevealed_manager_key contract)
  | Some (Manager_repr.Public_key v) -> return v

let is_manager_key_revealed c manager =
  let contract = Contract_repr.implicit_contract manager in
  Storage.Contract.Manager.get_option c contract >>=? function
  | None -> return_false
  | Some (Manager_repr.Hash _) -> return_false
  | Some (Manager_repr.Public_key _) -> return_true

let reveal_manager_key c manager public_key =
  let contract = Contract_repr.implicit_contract manager in
  Storage.Contract.Manager.get_option c contract >>=? begin
    function
    | Some Public_key _ -> fail (Previously_revealed_key contract)
    | Some Hash v -> return (c,v)
    | None ->
        match Contract_repr.is_implicit contract with
        | None -> failwith "reveal_manager_key"
        | Some v ->
            create_implicit c v ~balance:Tez_repr.zero >>=? fun c ->
            return (c,v)
  end >>=? fun (c,v) ->
  let actual_hash = Signature.Public_key.hash public_key in
  if (Signature.Public_key_hash.equal actual_hash v) then
    let v = (Manager_repr.Public_key public_key) in
    Storage.Contract.Manager.set c contract v >>=? fun c ->
    return c
  else fail (Inconsistent_hash (public_key,v,actual_hash))

(* Dune: Check that the account was KYC before revealing its key *)
let reveal_manager_key c manager public_key =
  let contract = Contract_repr.implicit_contract manager in
  match Tezos_base.Config.config.kyc_level with
  | Kyc_open -> reveal_manager_key c manager public_key
  | Kyc_source | Kyc_both ->
      get_kyc c contract >>=? fun kyc ->
      if Compare.Int.( kyc land Dune_misc.KYC.can_reveal <> 0 ) then
        reveal_manager_key c manager public_key
      else
        Dune_misc.failwith "Reveal: cannot reveal key before KYC"

let get_balance c contract =
  Storage.Contract.Balance.get_option c contract >>=? function
  | None -> begin
      match Contract_repr.is_implicit contract with
      | Some _ -> return Tez_repr.zero
      | None -> failwith "get_balance"
    end
  | Some v -> return v

let update_script_storage c contract storage big_map_diff =
  let storage = Script_repr.lazy_expr storage in
  update_script_big_map c big_map_diff >>=? fun (c, big_map_size_diff) ->
  Storage.Contract.Storage.set c contract storage >>=? fun (c, size_diff) ->
  Storage.Contract.Used_storage_space.get c contract >>=? fun previous_size ->
  let new_size = Z.add previous_size (Z.add big_map_size_diff (Z.of_int size_diff)) in
  Storage.Contract.Used_storage_space.set c contract new_size

let spend c contract amount =
  if Tez_repr.(amount = zero) then
    return c
  else
  Storage.Contract.Balance.get c contract >>=? fun balance ->
  match Tez_repr.(balance -? amount) with
  | Error _ ->
      fail (Balance_too_low (contract, balance, amount))
  | Ok new_balance ->
      Storage.Contract.Balance.set c contract new_balance >>=? fun c ->
      Roll_storage.Contract.remove_amount c contract amount >>=? fun c ->
      if Tez_repr.(new_balance > Tez_repr.zero) then
        return c
      else match Contract_repr.is_implicit contract with
        | None -> return c (* Never delete originated contracts *)
        | Some pkh ->
            Delegate_storage.get c contract >>=? function
            | Some pkh'
              when (* Dune: fix deletion of delegated contract *)
                (* Don't delete "delegate" contract *)
                Signature.Public_key_hash.equal pkh pkh' ->
                return c
            | Some _
            | None ->
                (* Delete empty implicit contract *)
                delete c contract

let credit c contract amount =
  begin
    if Tez_repr.(amount <> Tez_repr.zero) then
      return c
    else
      Storage.Contract.Code.mem c contract >>=? fun (c, target_has_code) ->
      fail_unless target_has_code (Empty_transaction contract) >>=? fun () ->
      return c
  end >>=? fun c ->
  Storage.Contract.Balance.get_option c contract >>=? function
  | None -> begin
      match Contract_repr.is_implicit contract with
      | None -> fail (Non_existing_contract contract)
      | Some manager ->
          match Tezos_base.Config.config.kyc_level with
          | Kyc_open | Kyc_source ->
              create_implicit c manager ~balance:amount
          | Kyc_both ->
              get_kyc c contract >>=? fun kyc ->
              if Compare.Int.(
                  kyc land Dune_misc.KYC.can_receive <> 0 ) then
                create_implicit c manager ~balance:amount
              else
                Dune_misc.failwith "Cannot credit account before KYC"
    end
  | Some balance ->
      Lwt.return Tez_repr.(amount +? balance) >>=? fun balance ->
      Storage.Contract.Balance.set c contract balance >>=? fun c ->
      Roll_storage.Contract.add_amount c contract amount

let init c =
  Storage.Contract.Global_counter.init c Z.zero

let used_storage_space c contract =
  Storage.Contract.Used_storage_space.get_option c contract >>=? function
  | None -> return Z.zero
  | Some fees -> return fees

let paid_storage_space c contract =
  Storage.Contract.Paid_storage_space.get_option c contract >>=? function
  | None -> return Z.zero
  | Some paid_space -> return paid_space

let set_paid_storage_space_and_return_fees_to_pay c contract new_storage_space =
  Storage.Contract.Paid_storage_space.get c contract >>=? fun already_paid_space ->
  if Compare.Z.(already_paid_space >= new_storage_space) then
    return (Z.zero, c)
  else
    let to_pay = Z.sub new_storage_space already_paid_space in
    Storage.Contract.Paid_storage_space.set c contract new_storage_space >>=? fun c ->
    return (to_pay, c)

let unfreeze_frozen_accounts ctxt last_cycle =
  (* Unfreeze accounts that were frozen for 60 cycles because they
     didn't delegate before Babylon+. *)
  Storage.Accounts_with_frozen_balance.fold (ctxt, last_cycle)
    ~init:(Ok ctxt)
    ~f:(fun key_hash acc ->
        Lwt.return acc >>=? fun ctxt ->
        Delegate_storage.unfreeze_account ctxt
          (Contract_repr.implicit_contract key_hash ))
  >>=? fun ctxt ->
  Storage.Accounts_with_frozen_balance.clear (ctxt, last_cycle)
  >>= fun ctxt -> return ctxt

(* Every 10 cycles, burn 1/10 the balance of undelegated accounts >
   100 DUN *)
let remove_account_under = Tez_repr.hundred
let deflate_undelegated_accounts ctxt last_cycle balance_updates =
  Storage.Cycle.Next_deflate.get_option ctxt >>=? function
  | None -> return (ctxt, balance_updates)
  | Some c ->
      if Cycle_repr.( last_cycle >= c ) then
        let share_of_balance_to_burn =
          Int64.of_int Config.config.share_of_balance_to_burn in
        Storage.Contract.fold ctxt ~init:( Ok (ctxt, balance_updates))
          ~f:(fun contract acc ->
              Lwt.return acc >>=? fun acc ->
              let (ctxt, balance_updates) = acc in
              match Contract_repr.is_implicit contract with
              | None -> return acc
              | Some _key_hash ->
                  Storage.Contract.Legacy2019.mem ctxt contract
                  >>= fun is_before_2019 ->
                  if is_before_2019 then
                    Delegate_storage.get ctxt contract >>=? function
                    | Some _ ->
                        Storage.Contract.Legacy2019.del ctxt contract >>=
                        fun ctxt ->
                        return (ctxt, balance_updates)
                    | None ->
                        begin
                          Storage.Contract.Balance.get_option ctxt contract
                          >>=? function
                          | Some balance -> return balance
                          | None -> return Tez_repr.zero
                        end >>=? fun balance ->
                        if Tez_repr.( balance > remove_account_under ) then
                          begin
                            match Tez_repr.( balance /?
                                             share_of_balance_to_burn ) with
                            | Error _ -> assert false
                            | Ok deflation -> Lwt.return deflation
                          end >>= fun deflation ->
                          match Tez_repr.( balance -? deflation ) with
                          | Error _ -> assert false
                          | Ok new_balance ->
                              Storage.Contract.Balance.set ctxt contract
                                new_balance
                              >>=? fun ctxt ->
                              let balance_updates =
                                (Delegate_storage.Contract contract,
                                 Delegate_storage.Debited deflation) ::
                                balance_updates
                              in
                              return (ctxt, balance_updates)
                        else
                          delete ctxt contract >>=? fun ctxt ->
                          let balance_updates =
                            (Delegate_storage.Contract contract,
                             Delegate_storage.Debited balance) ::
                            balance_updates
                          in
                          return (ctxt, balance_updates)
                  else
                    return acc
            ) >>=? fun (ctxt, balance_updates) ->
        Storage.Cycle.Next_deflate.set ctxt
          Cycle_repr.( add last_cycle
                         Config.config.number_of_cycles_between_burn)
        >>=? fun ctxt ->
        return (ctxt, balance_updates)
      else
        return (ctxt, balance_updates)

let cycle_end ctxt last_cycle balance_updates =
  unfreeze_frozen_accounts ctxt last_cycle >>=? fun ctxt ->
  deflate_undelegated_accounts ctxt last_cycle balance_updates
