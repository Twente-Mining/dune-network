(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module StringMap = Map.Make(String)

module PROTOCOL = struct

  let actions = ref StringMap.empty

  let perform_action ctxt action =
    match StringMap.find_opt action !actions with
    | None ->
        Dune_misc.failwith "Undefined protocol action %S" action
    | Some action -> action ctxt

  let register_action name f =
    if StringMap.mem name !actions then
      Dune_debug.printf ~n:0
        "Warning: protocol action %S registered twice" name;
    actions := StringMap.add name f !actions

  let is_action action =
    StringMap.mem action !actions

  (* Protocol actions *)

  let () =
    register_action "freeze_undelegated_accounts"
      (fun ctxt ->
         Storage.Contract.fold ctxt ~init:(Ok ctxt)
           ~f:(fun contract ctxt ->
               Lwt.return ctxt >>=? fun ctxt ->
               match Contract_repr.is_implicit contract with
               | None -> return ctxt
               | Some _key_hash ->
                   Delegate_storage.get ctxt contract >>=? function
                   | Some _ -> return ctxt
                   | None ->
                       Delegate_storage.freeze_account ctxt contract
             )
      );
    register_action "burn_all_commitments"
      (fun ctxt ->
         Storage.Commitments.clear ctxt >>= return
      );
    register_action "activate_burn_legacy"
      (fun ctxt ->
         let cycle = (Raw_context.current_level ctxt).cycle in
         Storage.Cycle.Next_deflate.init_set ctxt Cycle_repr.( add cycle 1 )
         >>= fun ctxt -> return ctxt
      );
    register_action "deactivate_burn_legacy"
      (fun ctxt ->
         Storage.Cycle.Next_deflate.remove ctxt
         >>= fun ctxt -> return ctxt
      );

    ()

end

let change_delegations ctxt contract delegation =
  begin
    match Contract_repr.is_implicit contract with
    | None ->
        Dune_misc.failwith "change_delegations only available for bakers"
    | Some delegate ->
        return delegate
  end >>=? fun delegate ->
  begin
    match delegation with
    | None -> return ctxt
    | Some baker ->
        Delegate_storage.registered ctxt baker >>=? fun is_baker ->
        if is_baker then
          return ctxt
        else
          Dune_misc.failwith
            "move_delegations: target must be an active baker"
  end >>=? fun ctxt ->
  Delegate_storage.delegated_contracts ctxt delegate >>= fun contracts ->
  let n = ref 0 in
  fold_left_s (fun ctxt source ->
      incr n;
      if not ( Dune_storage.has_protocol_revision ctxt 3 ) ||
         Contract_repr.( source <> contract ) then
        Delegate_storage.set ctxt source delegation
      else
        return ctxt
    ) ctxt contracts

module ACCOUNTS = struct

  let actions = ref StringMap.empty

  let perform_action ctxt balance_updates action amount contracts =
    match StringMap.find_opt action !actions with
    | None ->
        Dune_misc.failwith "Undefined accounts action %S" action
    | Some action ->
        fold_left_s
          (fun (ctxt, balances) contract ->
             action ctxt balances amount contract)
          (ctxt, balance_updates) contracts

  let register_action name f =
    if StringMap.mem name !actions then
      Dune_debug.printf ~n:0 "Warning: accounts action %S registered twice" name;
    actions := StringMap.add name f !actions

  let register_simple_action name f =
    register_action name (fun ctxt balances _amount contract ->
        f ctxt contract >>=? fun ctxt -> return (ctxt, balances)
      )

  (* Accounts actions, using Dictator's Manage_accounts *)

  let () =
    register_simple_action "unfreeze"
      (fun ctxt contract ->
         Delegate_storage.unfreeze_account ctxt contract
      );

    register_simple_action "undelegate" (fun ctxt contract ->
        change_delegations ctxt contract None ) ;

    register_simple_action "add_allowed_baker"
      (fun ctxt contract ->
         Contract_storage.change_allowed_baker ctxt contract true
      );

    register_simple_action "remove_allowed_baker"
      (fun ctxt contract ->
         Contract_storage.change_allowed_baker ctxt contract false
      );

    register_simple_action "add_superadmin"
      (fun ctxt contract ->
         Contract_storage.change_superadmin ctxt contract true >>= return
      );

    register_simple_action "remove_superadmin"
      (fun ctxt contract ->
         Contract_storage.change_superadmin ctxt contract false >>= return
      );

    register_action "kyc_accounts"
      (fun ctxt balances amount contract ->
         if Dune_storage.has_protocol_revision ctxt 3 then
           Contract_storage.change_kyc ctxt contract
             ( Tez_repr.to_mutez amount |> Int64.to_int ) >>=?
           fun ctxt -> return (ctxt, balances)
         else
           Dune_misc.failwith "set_kyc: not available in revision < 3"
      );

    ()

end


module ACCOUNT = struct

  let actions = ref StringMap.empty

  let perform_action ctxt balance_updates action contract arg =
    match StringMap.find_opt action !actions with
    | None ->
        Dune_misc.failwith "Undefined account action %S" action
    | Some action ->
        action ctxt balance_updates contract arg

  let register_action name f =
    if StringMap.mem name !actions then
      Dune_debug.printf ~n:0 "Warning: account action %S registered twice" name;
    actions := StringMap.add name f !actions

  open Love_value.Value

  let register_unit_action name f =
    register_action name (fun ctxt balances contract arg ->
        match arg with
        | VUnit -> f ctxt contract >>=? fun ctxt -> return (ctxt, balances)
        | _ -> Dune_misc.failwith "unexpected arg for %s" name)


  let () =
    register_unit_action "clear_delegations"
      (fun ctxt contract ->
         change_delegations ctxt contract None );

    register_action "move_delegations"
      (fun ctxt balances contract arg ->
         match arg with
         | VAddress delegate ->
             begin
               match Contract_repr.is_implicit delegate with
               | None ->
                   Dune_misc.failwith
                     "move_delegations: target must be a baker pkh"
               | Some delegate -> return delegate
             end >>=? fun delegate ->
             change_delegations ctxt contract (Some delegate) >>=? fun ctxt ->
             return ( ctxt, balances )
         | _ ->
             Dune_misc.failwith
               "move_delegations: arg must be destination baker"
      ) ;

    register_action "recover_account"
      (fun ctxt balances source arg ->
         match arg with
         | VAddress destination ->
            begin
              Contract_storage.get_recovery ctxt destination >>=? function
              | Some recovery when Contract_repr.( recovery = source ) ->
                  begin
                    (* undelegate to be able to clear the balance *)
                    begin
                      match Contract_repr.is_implicit destination with
                      | None ->
                          Dune_misc.failwith
                            "recover_account: recovered account cannot be a KT1"
                      | Some pkh -> return pkh
                    end >>=? fun destination_pkh ->
                    begin
                      Delegate_storage.registered ctxt destination_pkh
                      >>=? fun is_baker ->
                      if is_baker then
                        return ctxt
                      else
                        Delegate_storage.set ctxt destination None
                    end >>=? fun ctxt ->
                    (* transfer the balance *)
                    Contract_storage.get_balance ctxt destination
                    >>=? fun balance ->
                    Contract_storage.spend ctxt destination balance >>=?
                    fun ctxt ->
                    Contract_storage.credit ctxt destination balance >>=?
                    fun ctxt ->
                    return (ctxt, balances)
                  end
              | Some _ ->
                  Dune_misc.failwith
                    "recover_account: recovered account has another recovery account"
              | None ->
                  Dune_misc.failwith
                    "recover_account: recovered account has no recovery account"
            end
         | _ ->
             Dune_misc.failwith
               "recover_account: arg must be recovered account"
      ) ;


    ()

end
