(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Ident
open Utils

open Love_ast_types

type location = AST.location

type var_ident = string Ident.t
type cstr_name = string
type field_name = string

type visibility = AST.visibility

type type_kind = AST.type_kind

type exn_name =
  | RFail of TYPE.t
  | RException of string Ident.t

type const =
  | RCUnit
  | RCBool of bool
  | RCString of string
  | RCBytes of MBytes.t
  | RCInt of Z.t
  | RCNat of Z.t
  | RCDun of Tez_repr.t
  | RCKey of Signature.Public_key.t
  | RCKeyHash of Signature.Public_key_hash.t
  | RCSignature of Signature.t
  | RCTimestamp of Script_timestamp_repr.t
  | RCAddress of Contract_repr.t

type pattern =
  | RPAny
  | RPVar of string
  | RPAlias of pattern * string
  | RPConst of const
  | RPList of pattern list
  | RPTuple of pattern list
  | RPConstr of string * pattern list
  | RPContract of string * TYPE.structure_type
  | RPOr of pattern list
  | RPRecord of pattern AST.pattern_record_list

type exp =
  (* Base language *)
  | RConst of const
  | RVar of var_ident
  | RVarWithArg of var_ident * TYPE.extended_arg
  | RLet of { bnd_pattern: pattern; bnd_val: exp; body: exp }
  | RLetRec of { bnd_var: string; bnd_val: exp;
                 val_typ: TYPE.t; body: exp }
  | RLambda of lambda
  | RApply of { exp: exp; args: param list }

  (* Common extensions *)
  | RSeq of exp list
  | RIf of { cond: exp; ifthen: exp; ifelse: exp }
  | RMatch of { arg: exp; cases: (pattern * exp) list }

  (* Exceptions *)
  | RRaise of { exn: exn_name; args: exp list; loc: location option}
  | RTryWith of { arg: exp; cases: ((exn_name * pattern list) * exp) list }

  (* Collections *)
  | RNil
  | RList of exp list * exp

  (* Tuples *)
  | RTuple of exp list
  | RProject of { tuple: exp; indexes: int list } (* non-empty, >= 0 *)
  | RUpdate of { tuple: exp; updates: (int * exp) list } (* non-empty, >= 0 *)

  (* Sums *)
  | RConstructor of { type_name: TYPE.type_name;
                      ctyps : TYPE.t list;
                      constr: cstr_name; args: exp list }

  (* Records *)
  | RRecord of { type_name: TYPE.type_name;
                 contents: (field_name * exp) list }
  | RGetField of { record: exp; field: field_name }
  | RSetFields of { record: exp; updates: (field_name * exp) list }

  (* First-class structures *)
  | RPackStruct of reference

and binding =
  | RPattern of pattern * TYPE.t
  | RTypeVar of TYPE.type_var

and param =
  | RExp of exp
  | RType of TYPE.t

and lambda = {
  args: binding list; (* non-empty *)
  body: exp; (* different from Lambda *)
}

and init = {
  init_code: exp;
  init_typ: TYPE.t; (* Parameter type *)
  init_persist: bool;
}

and entry = {
  entry_code: exp;
  entry_fee_code: exp option; (* An expression of type (fun, nat) *)
  entry_typ: TYPE.t; (* Parameter type *)
}

and view = {
  view_code: exp;
  view_typ: TYPE.t; (* Type fo the view content *)
  view_recursive: TYPE.recursive;
}

and value = {
  value_code: exp;
  value_typ: TYPE.t;
  value_visibility: visibility;
  value_recursive: TYPE.recursive;
}

and content =
  | RDefType of type_kind * TYPE.typedef
  | RDefException of TYPE.t list
  | RInit of init
  | REntry of entry
  | RView of view
  | RValue of value
  | RStructure of structure
  | RSignature of TYPE.structure_sig

and structure = {
  kind : TYPE.struct_kind;
  structure_content : (string * content) list;
} (* unique names at top level *)

and reference =
  | RAnonymous of structure
  | RNamed of string Ident.t

type top_contract = {
  version: (int * int); (* (major, minor) *)
  code: structure;
}

let pp_visibility = Love_ast.pp_visibility

let kind_to_string = Love_ast.kind_to_string

let pp_type_kind = Love_ast.pp_type_kind

let type_kind_to_sigtype = Love_ast.type_kind_to_sigtype

let exn_id = function
  | RFail _ -> "Failure"
  | RException e -> Ident.get_final e

let print_exn_name fmt =
  function
  | RFail t -> Format.fprintf fmt "Failure [<%a>]" Love_type.pretty t
  | RException e -> print_strident fmt e

let exn_equal e1 e2 =
  match e1, e2 with
  | RFail t1, RFail t2 -> Love_type.bool_equal t1 t2
  | RException e1, RException e2 -> Ident.equal String.equal e1 e2
  | _, _ -> false

let is_module c =
  match c.kind with
  | Module -> true
  | Contract _ -> false



let compare_exn_name en1 en2 =
  match en1, en2 with
  | RFail t1, RFail t2 -> Love_type.compare t1 t2
  | RException id1, RException id2 -> Ident.compare String.compare id1 id2
  | RFail _, _ -> -1      | _, RFail _ -> 1
  (* | RException _, _ -> -1 | _, RException _ -> 1 *)

let compare_const c1 c2 =
  match c1, c2 with
  | RCUnit, RCUnit -> 0
  | RCBool b1, RCBool b2 -> Compare.Bool.compare b1 b2
  | RCString s1, RCString s2 -> String.compare s1 s2
  | RCBytes b1, RCBytes b2 -> MBytes.compare b1 b2
  | RCInt i1, RCInt i2
  | RCNat i1, RCNat i2 -> Z.compare i1 i2
  | RCDun d1, RCDun d2 -> Tez_repr.compare d1 d2
  | RCKey k1, RCKey k2 -> Signature.Public_key.compare k1 k2
  | RCKeyHash kh1, RCKeyHash kh2 -> Signature.Public_key_hash.compare kh1 kh2
  | RCSignature s1, RCSignature s2 -> Signature.compare s1 s2
  | RCTimestamp ts1, RCTimestamp ts2 -> Script_timestamp_repr.compare ts1 ts2
  | RCAddress a1, RCAddress a2 -> Contract_repr.compare a1 a2
  | RCUnit, _ -> -1        | _, RCUnit -> 1
  | RCBool _, _ -> -1      | _, RCBool _ -> 1
  | RCString _, _ -> -1    | _, RCString _ -> 1
  | RCBytes _, _ -> -1     | _, RCBytes _ -> 1
  | RCInt _, _ -> -1       | _, RCInt _ -> 1
  | RCNat _, _ -> -1       | _, RCNat _ -> 1
  | RCDun _, _ -> -1       | _, RCDun _ -> 1
  | RCKey _, _ -> -1       | _, RCKey _ -> 1
  | RCKeyHash _, _ -> -1   | _, RCKeyHash _ -> 1
  | RCSignature _, _ -> -1 | _, RCSignature _ -> 1
  | RCTimestamp _, _ -> -1 | _, RCTimestamp _ -> 1
(*  | RCAddress _, _ -> -1   | _, RCAddress _ -> 1 *)

let rec compare_pattern p1 p2 =
  match p1, p2 with
  | RPAny, RPAny -> 0
  | RPVar v1, RPVar v2 -> String.compare v1 v2
  | RPAlias (p1, v1), RPAlias (p2, v2) ->
     compare_pair compare_pattern String.compare (p1, v1) (p2, v2)
  | RPConst c1, RPConst c2 -> compare_const c1 c2
  | RPList pl1, RPList pl2
  | RPTuple pl1, RPTuple pl2
  | RPOr pl1, RPOr pl2 -> compare_list compare_pattern pl1 pl2
  | RPConstr (c1, pl1), RPConstr (c2, pl2) ->
     compare_pair String.compare
       (compare_list compare_pattern) (c1, pl1) (c2, pl2)
  | RPContract (c1, st1), RPContract (c2, st2) ->
     compare_pair String.compare
       Love_type.compare_struct_type (c1, st1) (c2, st2)
  | RPRecord l1, RPRecord l2 -> Love_ast.compare_record_list compare_pattern l1 l2
  | RPAny, _ -> -1        | _, RPAny -> 1
  | RPVar _, _ -> -1      | _, RPVar _ -> 1
  | RPAlias _, _ -> -1    | _, RPAlias _ -> 1
  | RPConst _, _ -> -1    | _, RPConst _ -> 1
  | RPList _, _ -> -1     | _, RPList _ -> 1
  | RPTuple _, _ -> -1    | _, RPTuple _ -> 1
  | RPConstr _, _ -> -1   | _, RPConstr _ -> 1
  | RPContract _, _ -> -1 | _, RPContract _ -> 1
  | RPOr _, _ -> -1 | _, RPOr _ -> 1
(* RPRecord _, _ -> -1 | _, RPRecord _ -> 1 *)

let rec compare_exp e1 e2 =
  let compare_strident id1 id2 = Ident.compare String.compare id1 id2 in
  match e1, e2 with
  | RConst c1, RConst c2 -> compare_const c1 c2
  | RVar v1, RVar v2 -> compare_strident v1 v2
  | RVarWithArg (v1, xa1), RVarWithArg (v2, xa2) ->
     compare_pair (compare_strident)
       (Love_primitive.compare_extended_arg) (v1, xa1) (v2, xa2)
  | RLet { bnd_pattern = p1; bnd_val = e11; body = e12 },
    RLet { bnd_pattern = p2; bnd_val = e21; body = e22 } ->
     compare_tuple3 compare_pattern compare_exp
       compare_exp (p1, e11, e12) (p2, e21, e22)
  | RLetRec { bnd_var = v1; bnd_val = e11; val_typ = t1; body = e12 },
    RLetRec { bnd_var = v2; bnd_val = e21; val_typ = t2; body = e22 } ->
     compare_tuple4 String.compare compare_exp Love_type.compare
       compare_exp (v1, e11, t1, e12) (v2, e21, t2, e22)
  | RLambda l1, RLambda l2 -> compare_lambda l1 l2
  | RApply { exp = e1; args = pl1 }, RApply { exp = e2; args = pl2 } ->
     compare_pair compare_exp (compare_list compare_param) (e1, pl1) (e2, pl2)
  | RSeq el1, RSeq el2 -> compare_list compare_exp el1 el2
  | RIf { cond = e11; ifthen = e12; ifelse = e13 },
    RIf { cond = e21; ifthen = e22; ifelse = e23 } ->
     compare_tuple3 compare_exp compare_exp
       compare_exp (e11, e12, e13) (e21, e22, e23)
  | RMatch { arg = e1; cases = pel1 },
    RMatch { arg = e2; cases = pel2 } ->
     compare_pair compare_exp
       (compare_list (compare_pair compare_pattern compare_exp))
       (e1, pel1) (e2, pel2)
  | RRaise { exn = en1; args = el1; loc = lo1 },
    RRaise { exn = en2; args = el2; loc = lo2 } ->
     compare_tuple3 compare_exn_name (compare_list compare_exp)
       (compare_option Love_ast.compare_location)
       (en1, el1, lo1) (en2, el2, lo2)
  | RTryWith { arg = e1; cases = exel1 },
    RTryWith { arg = e2; cases = exel2 } ->
     compare_pair compare_exp (
         compare_list (compare_pair (
                           compare_pair compare_exn_name (
                               compare_list compare_pattern)
                         ) compare_exp)
       ) (e1, exel1) (e2, exel2)
  | RNil, RNil -> 0
  | RList (el1, e1), RList (el2, e2) ->
     compare_pair (compare_list compare_exp) compare_exp (el1, e1) (el2, e2)
  | RTuple el1, RTuple el2 -> compare_list compare_exp el1 el2
  | RProject { tuple = e1; indexes = il1 },
    RProject { tuple = e2; indexes = il2 } ->
     compare_pair compare_exp
       (compare_list Compare.Int.compare) (e1, il1) (e2, il2)
  | RUpdate { tuple = e1; updates = iel1 },
    RUpdate { tuple = e2; updates = iel2 } ->
     compare_pair compare_exp (
         compare_list (compare_pair Compare.Int.compare compare_exp)
       ) (e1, iel1) (e2, iel2)
  | RConstructor { type_name = tn1; ctyps = tl1; constr = cn1; args = el1 },
    RConstructor { type_name = tn2; ctyps = tl2; constr = cn2; args = el2 } ->
     compare_tuple4 compare_strident (compare_list Love_type.compare)
       String.compare (compare_list compare_exp)
       (tn1, tl1, cn1, el1) (tn2, tl2, cn2, el2)
  | RRecord { type_name = tn1; contents = fel1 },
    RRecord { type_name = tn2; contents = fel2 } ->
     compare_pair compare_strident (
         compare_list (compare_pair String.compare compare_exp)
       ) (tn1, fel1) (tn2, fel2)
  | RGetField { record = e1; field = fn1 },
    RGetField { record = e2; field = fn2 } ->
     compare_pair compare_exp String.compare (e1, fn1) (e2, fn2)
  | RSetFields { record = e1; updates = fel1 },
    RSetFields { record = e2; updates = fel2 } ->
     compare_pair compare_exp (
         compare_list (compare_pair String.compare compare_exp)
       ) (e1, fel1) (e2, fel2)
  | RPackStruct r1, RPackStruct r2 -> compare_reference r1 r2
  | RConst _, _ -> -1       | _, RConst _ -> 1
  | RVar _, _ -> -1         | _, RVar _ -> 1
  | RVarWithArg _, _ -> -1  | _, RVarWithArg _ -> 1
  | RLet _, _ -> -1         | _, RLet _ -> 1
  | RLetRec _, _ -> -1      | _, RLetRec _ -> 1
  | RLambda _, _ -> -1      | _, RLambda _ -> 1
  | RApply _, _ -> -1       | _, RApply _ -> 1
  | RSeq _, _ -> -1         | _, RSeq _ -> 1
  | RIf _, _ -> -1          | _, RIf _ -> 1
  | RMatch _, _ -> -1       | _, RMatch _ -> 1
  | RRaise _, _ -> -1       | _, RRaise _ -> 1
  | RTryWith _, _ -> -1     | _, RTryWith _ -> 1
  | RNil, _ -> -1           | _, RNil -> 1
  | RList _, _ -> -1        | _, RList _ -> 1
  | RTuple _, _ -> -1       | _, RTuple _ -> 1
  | RProject _, _ -> -1     | _, RProject _ -> 1
  | RUpdate _, _ -> -1      | _, RUpdate _ -> 1
  | RConstructor _, _ -> -1 | _, RConstructor _ -> 1
  | RRecord _, _ -> -1      | _, RRecord _ -> 1
  | RGetField _, _ -> -1    | _, RGetField _ -> 1
  | RSetFields _, _ -> -1   | _, RSetFields _ -> 1
  (* | RPackStruct _, _ -> -1  | _, RPackStruct _ -> 1 *)

and compare_param p1 p2 =
  match p1, p2 with
  | RExp e1, RExp e2 -> compare_exp e1 e2
  | RType t1, RType t2 -> Love_type.compare t1 t2
  | RExp _, _ -> -1 | _, RExp _ -> 1
  (* | RType _, _ -> -1 | _, RType _ -> 1 *)

and compare_binding b1 b2 =
  match b1, b2 with
  | RPattern (p1, t1), RPattern (p2, t2) ->
     compare_pair compare_pattern Love_type.compare (p1, t1) (p2, t2)
  | RTypeVar tv1, RTypeVar tv2 ->
     Love_type.compare_tvar tv1 tv2
  | RPattern _, _ -> -1 | _, RPattern _ -> 1
  (* | RTypeVar _, _ -> -1 | _, RTypeVar _ -> 1 *)

and compare_lambda l1 l2 =
  compare_pair (compare_list compare_binding)
    compare_exp (l1.args, l1.body) (l2.args, l2.body)

and compare_content c1 c2 =
  match c1, c2 with
  | RDefType (tk1, td1), RDefType (tk2, td2) ->
     compare_pair Love_ast.compare_type_kind
       Love_type.compare_typedef (tk1, td1) (tk2, td2)
  | RDefException tl1, RDefException tl2 ->
     compare_list Love_type.compare tl1 tl2
  | RInit { init_code = e1; init_typ = t1; init_persist = b1 },
    RInit { init_code = e2; init_typ = t2; init_persist = b2 } ->
     compare_tuple3 compare_exp Love_type.compare
       Compare.Bool.compare (e1, t1, b1) (e2, t2, b2)
  | REntry { entry_code = e1; entry_fee_code = eo1; entry_typ = t1 },
    REntry { entry_code = e2; entry_fee_code = eo2; entry_typ = t2 } ->
     compare_tuple3 compare_exp (compare_option compare_exp)
       Love_type.compare (e1, eo1, t1) (e2, eo2, t2)
  | RView { view_code = e1; view_typ = t1 },
    RView { view_code = e2; view_typ = t2 } ->
     compare_pair compare_exp Love_type.compare (e1, t1) (e2, t2)
  | RValue { value_code = e1; value_typ = t1;
             value_visibility = vs1; value_recursive = vr1 },
    RValue { value_code = e2; value_typ = t2;
             value_visibility = vs2; value_recursive = vr2 } ->
     compare_tuple4 compare_exp Love_type.compare
       Love_ast.compare_visibility Love_type.compare_rec
       (e1, t1, vs1, vr1) (e2, t2, vs2, vr2)
  | RStructure s1, RStructure s2 -> compare_structure s1 s2
  | RSignature s1, RSignature s2 -> Love_type.compare_sig_kind s1 s2
  | RDefType _, _ -> -1      | _, RDefType _ -> 1
  | RDefException _, _ -> -1 | _, RDefException _ -> 1
  | RInit _, _ -> -1         | _, RInit _ -> 1
  | REntry _, _ -> -1        | _, REntry _ -> 1
  | RView _, _ -> -1         | _, RView _ -> 1
  | RValue _, _ -> -1        | _, RValue _ -> 1
  | RStructure _, _ -> -1    | _, RStructure _ -> 1
  (* | RSignature _, _ -> -1    | _, RSignature _ -> 1 *)

and compare_structure s1 s2 =
  compare_pair Love_type.compare_struct_kind
    (compare_list (compare_pair String.compare compare_content))
    (s1.kind, s1.structure_content) (s2.kind, s2.structure_content)

and compare_reference r1 r2 =
  match r1, r2 with
  | RAnonymous s1, RAnonymous s2 -> compare_structure s1 s2
  | RNamed id1, RNamed id2 -> Ident.compare String.compare id1 id2
  | RAnonymous _, _ -> -1 | _, RAnonymous _ -> 1
  (* | RNamed _, _ -> -1     | _, RNamed _ -> 1 *)

let lambdas_to_params = function
  | RLambda { args; _ } -> args
  | _ -> []

let params_to_lambdas pl b =
  RLambda { args = pl; body = b }
