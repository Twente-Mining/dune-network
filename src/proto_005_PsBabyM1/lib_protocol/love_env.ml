(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Love_pervasives.Exceptions
open Love_pervasives.Collections
open Love_value
open Love_ast_types

type search_result =
  | CValue of Value.val_contents
  | CStructure of LiveStructure.t Value.ptr_or_inlined
  | CSignature of TYPE.structure_sig Value.ptr_or_inlined
  | CExnName of string Ident.t
  | CTypeName of string Ident.t

type t = {
  values: (int * Value.val_contents) StringIdentMap.t;
  structs: (int * LiveStructure.t Value.ptr_or_inlined) StringIdentMap.t;
  sigs: (int * TYPE.structure_sig Value.ptr_or_inlined) StringIdentMap.t;
  exns: (int * string Ident.t) StringIdentMap.t;
  types: (int * string Ident.t) StringIdentMap.t;
  tvars: TYPE.t StringMap.t; (* added on type application *)
  depth: int; (* call depth *)
  root: string; (* kt1 or pseudo-name of the contract being evaluated,
                   to bypass the KT1-based search when evaluating
                   the root contract or an anonymous contract *)
  deps: string list; (* contracts marked as dependencies *)
  mutable counter: int; (* to timestamp things in environment *)
}

type 'a find_ret = (Love_context.t * 'a option) Error_monad.tzresult Lwt.t

(* GLOBAL_STATE *)
let init_values = ref StringIdentMap.empty
let init_types = ref StringIdentMap.empty
let init_counter = ref 0

let init root = {
  values = !init_values;
  structs = StringIdentMap.empty;
  sigs = StringIdentMap.empty;
  exns = StringIdentMap.empty;
  types = !init_types;
  tvars = StringMap.empty;
  depth = 0;
  root;
  deps = [];
  counter = !init_counter;
}

let re_init env root =
  { (init root) with depth = env.depth }

let fresh env =
  env.counter <- env.counter + 1;
  env.counter

let inc_depth env =
  let depth = env.depth + 1 in
  begin if Compare.Int.(depth > !Options.max_stack)
    then raise StackOverflow end;
  { env with depth }

let is_in_root env path =
  let maybe_root, id_opt = Ident.split path in
  if String.equal maybe_root env.root then true, id_opt
  else false, None

let is_in_deps env path =
  let maybe_dep, id_opt = Ident.split path in
  match List.find_opt (String.equal maybe_dep) env.deps with
  | Some _ -> true, id_opt
  | None -> false, None

let add_deps env deps_list =
  List.fold_left (fun env (n, kt1) ->
      let id = Ident.create_id n in
      let path = Ident.create_id kt1 in
      { env with
        deps = kt1 :: env.deps;
        structs = StringIdentMap.add id
            (fresh env, Value.Pointer path) env.structs }
    ) env deps_list


let add_value env id v =
  { env with values = StringIdentMap.add id (fresh env, v) env.values }

let add_local_value env id v =
  { env with values = StringIdentMap.add id
                 (fresh env, Value.Local v) env.values }

let add_inlined_value_r3 env id path v =
  let values =
    match is_in_root env path with
    | false, _ -> env.values
    | true, _ ->
        StringIdentMap.add path
          (fresh env, Value.Global (Value.Inlined (path, v))) env.values
  in
  { env with values = StringIdentMap.add id
                 (fresh env, Value.Global (Value.Inlined (path, v))) values }

let add_inlined_value_r2 env id path v =
  { env with values = StringIdentMap.add id
                 (fresh env, Value.Global
                    (Value.Inlined (path, v))) env.values }

let add_inlined_value env id path v =
  (* Fixes issue #17 *)
  if has_protocol_revision 3 then
    add_inlined_value_r3 env id path v
  else
    add_inlined_value_r2 env id path v

let add_inlined_struct env id path s =
  { env with structs = StringIdentMap.add id
                 (fresh env, Value.Inlined (path, s)) env.structs }

let add_inlined_sig env id path s =
  { env with sigs = StringIdentMap.add id
                 (fresh env, Value.Inlined (path, s)) env.sigs }

let add_exn_name env id en =
  { env with exns = StringIdentMap.add id (fresh env, en) env.exns }

let add_type_name env id tn =
  { env with types = StringIdentMap.add id (fresh env, tn) env.types }

let add_bindings env (e : Value.env) =
  let values = List.fold_left (fun values (id, v) ->
      StringIdentMap.add id (fresh env, v) values
    ) env.values (List.rev e.values) in
  let structs = List.fold_left (fun structs (id, s) ->
      StringIdentMap.add id (fresh env, s) structs
    ) env.structs (List.rev e.structs) in
  let sigs = List.fold_left (fun sigs (id, s) ->
      StringIdentMap.add id (fresh env, s) sigs
    ) env.sigs (List.rev e.sigs) in
  let exns = List.fold_left (fun exns (id, en) ->
      StringIdentMap.add id (fresh env, en) exns
    ) env.exns (List.rev e.exns) in
  let types = List.fold_left (fun types (id, tn) ->
      StringIdentMap.add id (fresh env, tn) types
    ) env.types (List.rev e.types) in
  let tvars = List.fold_left (fun tvars (n, t) ->
      StringMap.add n t tvars
    ) env.tvars (List.rev e.tvars) in
  { env with values; structs; sigs; exns; types; tvars }

let contents_to_cntlove = function
  | None -> None
  | Some cnt -> Some (Love_context.CntLove cnt)

let contents_to_result path =
  let is_instance_field path =
    match Ident.get_list path with
    | [maybe_kt1; n] ->
        begin match Utils.address_of_string_opt maybe_kt1 with
          | Some a -> Some (a, n)
          | None -> None
        end
    | _ -> None
  in
  let open Value in
  let open LiveStructure in
  function
  | Some (Love_context.CntLove cnt) ->
      begin
        match cnt with
        | VValue { vvalue_code = v; _ } ->
            Some (CValue (Global (Inlined (path, v))))
        | VEntry _ ->
            begin match is_instance_field path with
              | None -> raise (InvariantBroken
                                 "Can't use an entry outside of an instance")
              | Some (a, n) -> Some (CValue (
                  Global (Inlined (path, VEntryPoint (a, n)))))
            end
        | VView _ ->
            begin match is_instance_field path with
              | None -> raise (InvariantBroken
                                 "Can't use a view outside of an instance")
              | Some (a, n) -> Some (CValue (
                  Global (Inlined (path, VView (a, n)))))
            end
        | VStructure s ->
            Some (CStructure (Inlined (path, s)))
        | VSignature s ->
            Some (CSignature (Inlined (path, s)))
        | VException _tl -> Some (CExnName path)
        | VType (_k, _td) -> Some (CTypeName path)
        | VInit _ -> None
      end
  | Some (CntMichelson _) ->
      begin
        match is_instance_field path with
        | None -> raise (
            InvariantBroken
              "Michelson entry point referenced without a KT1 prefix")
        | Some (a, n) ->
            Some (CValue (Global (Inlined (path, VEntryPoint (a, n)))))

      end
  | None -> None

(* Resolve a relative path
   Try to resolve both by finding the whole identifier in the
   environment and by finding the root and resolving from it.
   Use the timestamp to determine which result to return (the most recent).
   When resolving from root:
   - If inlined, directly resolve in the structure
   - If it's a pointer, then ask the context to resolve
     (the first element of a pointer is a KT1, because it is a structure
      introduced by dependencies or by a Contract.at)
   Note that the result might still be a pointer (in particular,
   if the element was added to the environment from a closure). *)
let resolve_relative ctxt env source wrap id =
  let age_result_opt = match StringIdentMap.find_opt id source with
    | None -> None
    | Some (i, r) -> Some (i, r)
  in
  let age_root_id_opt = match Ident.split id with
    | _, None -> None (* this case is already handled above *)
    | n, Some id ->
        match StringIdentMap.find_opt (Ident.create_id n) env.structs with
        | None -> None
        | Some (i, s) -> Some (i, s, id)
  in
  match age_result_opt, age_root_id_opt with
  | None, None -> raise (InvariantBroken (
      Format.asprintf "Identifier %a not found" Ident.print_strident id))
  | Some (_, r), None -> return (ctxt, Some (wrap r))
  | Some (i1, r), Some (i2, _, _) when Compare.Int.(i1 > i2) ->
      return (ctxt, Some (wrap r))
  | Some _, Some (_, s, id) (* when i2 >= i1 *)
  | None, Some (_, s, id) ->
      match s with
      | Inlined (path, s) ->
          LiveStructure.resolve_id_in_struct s id |> fun res ->
          return (ctxt, contents_to_result (Ident.concat path id)
                    (contents_to_cntlove res))
      | Pointer path -> (* with id length >= 1 *)
          let path = Ident.concat path id in
          Love_context.resolve_path_with_kt1 ctxt path >>|? fun (ctxt, res) ->
          ctxt, contents_to_result path res


(* Resolve an absolute path
   If the path starts from the current root, then try to resolve from the
   current environment, using a procedure similar to resolve_relative.
   Otherwise, ask the context to resolve (it should start with a KT1) *)
let resolve_absolute ctxt env source wrap path =
  match is_in_root env path with
  | true, None ->
      raise (InvariantBroken ("Attempting to resolve an empty path in root"))
  | true, Some id ->
      (* Fixes issue #17 *)
      begin match StringIdentMap.find_opt path source with
        | Some (_i, res) -> return (ctxt, Some (wrap res))
        | None ->
            begin match Ident.split id with
              | n, None ->
                  begin match StringIdentMap.find_opt
                                (Ident.create_id n) source with
                  | Some (_i, res) -> return (ctxt, Some (wrap res))
                  | None ->
                      raise (InvariantBroken ("Identifier not found : " ^ n))
                  end
              | n, Some id ->
                  match StringIdentMap.find_opt
                          (Ident.create_id n) env.structs with
                  | None ->
                      raise (InvariantBroken ("Identifier not found : " ^ n))
                  | Some (_i, s) ->
                      match s with
                      | Inlined (path, s) ->
                          LiveStructure.resolve_id_in_struct s id |> fun res ->
                          return (ctxt, contents_to_result
                                    (Ident.concat path id)
                                    (contents_to_cntlove res))
                      | Pointer path -> (* with id length >= 1 *)
                          let path = Ident.concat path id in
                          Love_context.resolve_path_with_kt1 ctxt path
                          >>|? fun (ctxt, res) ->
                          ctxt, contents_to_result path res
            end
      end
  | false, _ ->
      Love_context.resolve_path_with_kt1 ctxt path >>|? fun (ctxt, res) ->
      ctxt, contents_to_result path res


let find_raw_value_opt ctxt env id : Value.val_contents find_ret =
  resolve_relative ctxt env env.values (fun x -> CValue x) id
  >>|? fun (ctxt, res) -> match res with
  | Some (CValue v) -> ctxt, Some v
  | _ -> ctxt, None

let find_value_opt ctxt env id : Value.t find_ret =
  resolve_relative ctxt env env.values (fun x -> CValue x) id
  >>=? fun (ctxt, res) -> match res with
  | Some (CValue (Local v))
  | Some (CValue (Global (Inlined (_, v)))) -> return (ctxt, Some v)
  | Some (CValue (Global (Pointer path))) ->
      resolve_absolute ctxt env env.values (fun x -> CValue x) path
      >>|? fun (ctxt, res) ->
      begin match res with
        | Some (CValue (Local v))
        | Some (CValue (Global (Inlined (_, v)))) -> (ctxt, Some v)
        | Some (CValue (Global (Pointer _path))) ->
            raise (InvariantBroken "Still a pointer after resolve_absolute")
        | _ -> (ctxt, None)
      end
  | _ -> return (ctxt, None)

let find_raw_struct_opt ctxt env id :
  LiveStructure.t Value.ptr_or_inlined find_ret =
  resolve_relative ctxt env env.structs (fun x -> CStructure x) id
  >>|? fun (ctxt, res) -> match res with
  | Some (CStructure s) -> ctxt, Some s
  | _ -> ctxt, None

let find_struct_opt ctxt env id : (string Ident.t * LiveStructure.t) find_ret =
  resolve_relative ctxt env env.structs (fun x -> CStructure x) id
  >>=? fun (ctxt, res) -> match res with
  | Some (CStructure (Value.Inlined (path, s))) -> return (ctxt, Some (path, s))
  | Some (CStructure (Value.Pointer path)) ->
      (* May need resolve_absolute *)
      Love_context.resolve_path_with_kt1 ctxt path >>|? fun (ctxt, res) ->
      begin match res with
        | Some (Love_context.CntLove (VStructure s)) -> ctxt, Some (path, s)
        | Some _ | None -> ctxt, None
      end
  | _ -> return (ctxt, None)

let find_raw_sig_opt ctxt env id :
  TYPE.structure_sig Value.ptr_or_inlined find_ret =
  match Ident.get_list id with
  | ["UnitContract"] ->
      return (ctxt, Some (Value.Inlined (
          Ident.create_id "UnitContract", Love_type.unit_contract_sig)))
  | _ ->
      resolve_relative ctxt env env.sigs (fun x -> CSignature x) id
      >>|? fun (ctxt, res) -> match res with
      | Some (CSignature s) -> ctxt, Some s
      | _ -> ctxt, None

let find_sig_opt ctxt env id : TYPE.structure_sig find_ret =
  match Ident.get_list id with
  | ["UnitContract"] -> return (ctxt, Some Love_type.unit_contract_sig)
  | _ ->
      resolve_relative ctxt env env.sigs (fun x -> CSignature x) id
      >>=? fun (ctxt, res) -> match res with
      | Some (CSignature (Inlined (_path, s))) -> return (ctxt, Some s)
      | Some (CSignature (Pointer path)) ->

          resolve_absolute ctxt env env.sigs (fun x -> CSignature x) path
          >>|? fun (ctxt, res) ->
          begin match res with
            | Some (CSignature (Inlined (_path, s))) -> (ctxt, Some s)
            | Some (CSignature (Pointer _path)) ->
                raise (InvariantBroken "Still a pointer after resolve_absolute")
            | Some _ -> (ctxt, None)
            | _ -> (ctxt, None)
          end

      (* May need resolve_absolute *) (*
        Love_context.resolve_path_with_kt1 ctxt path >>|? fun (ctxt, res) ->
        begin match res with
          | Some (VSignature s) -> ctxt, Some s
          | Some _ | None -> ctxt, None
        end
*)
      | Some _ | None -> return (ctxt, None)

let find_exn_name_opt ctxt env id =
  resolve_relative ctxt env env.exns (fun x -> CExnName x) id
  >>|? fun (ctxt, res) -> match res with
  | Some (CExnName tn) -> ctxt, Some tn
  | _ -> ctxt, None

let find_type_name_opt ctxt env id =
  resolve_relative ctxt env env.types (fun x -> CTypeName x) id
  >>|? fun (ctxt, res) -> match res with
  | Some (CTypeName tn) -> ctxt, Some tn
  | _ -> ctxt, None

let find_tvar_opt env n =
  StringMap.find_opt n env.tvars


(* Extract a closure from the environment using the provided ids
   (they constitute the set of free variables of the lambdas
   whose closure is being computed).
   When dealing with an inlined element, if it is in the current
   contract (root), then turn it into a pointer (we want to avoid
   inlined values in the closure of the current contract, so as
   to save space). *)
let get_closure ctxt env (free_ids : Love_free_vars.vars) =
  let open Value in
  List.fold_left (fun res id ->
      res >>=? fun (ctxt, vl) ->
      find_raw_value_opt ctxt env id >>|? fun (ctxt, res) ->
      match res with
      | Some ((Local (VPrimitive (pn, ANone, []))) as v) ->
          begin match id with
            | LDot _ -> ctxt, vl (* LDot not allowed on local -> primitive *)
            | LName n when String.equal n pn.prim_name -> ctxt, vl (* prim *)
            | LName _-> ctxt, (id, v) :: vl (* not a primitive *)
          end
      | Some (Global (Inlined (path, _)))
        when fst (is_in_root env path) || fst (is_in_deps env path) ->
          ctxt, (id, (Global (Pointer path))) :: vl
      | Some v -> ctxt, (id, v) :: vl
      | None -> raise (InvariantBroken
                         (Format.asprintf "Variable %a not found"
                            Ident.print_strident id))
    ) (return (ctxt, [])) free_ids.var
  >>=? fun (ctxt, values) ->
  List.fold_left (fun res id ->
      res >>=? fun (ctxt, sl) ->
      find_raw_struct_opt ctxt env id >>|? fun (ctxt, res) ->
      match res with
      | Some (Inlined (path, _))
        when fst (is_in_root env path) || fst (is_in_deps env path) ->
          ctxt, (id, (Pointer path)) :: sl
      | Some s -> ctxt, (id, s) :: sl
      | None -> raise (InvariantBroken
                         (Format.asprintf "Structure %a not found"
                            Ident.print_strident id))
    ) (return (ctxt, [])) free_ids.str
  >>=? fun (ctxt, structs) ->
  List.fold_left (fun res id ->
      res >>=? fun (ctxt, sl) ->
      find_raw_sig_opt ctxt env id >>|? fun (ctxt, res) ->
      match res with
      | Some (Inlined (path, _))
        when fst (is_in_root env path) || fst (is_in_deps env path) ->
          ctxt, (id, (Pointer path)) :: sl
      | Some s -> ctxt, (id, s) :: sl
      | None -> raise (InvariantBroken
                         (Format.asprintf "Signature %a not found"
                            Ident.print_strident id))
    ) (return (ctxt, [])) free_ids.sgn
  >>=? fun (ctxt, sigs) ->
  List.fold_left (fun res id ->
      res >>=? fun (ctxt, enl) ->
      find_exn_name_opt ctxt env id >>|? fun (ctxt, res) ->
      match res with
      | Some en -> ctxt, (id, en) :: enl
      | None -> raise (InvariantBroken
                         (Format.asprintf "Exception name %a not found"
                            Ident.print_strident id))
    ) (return (ctxt, [])) free_ids.exn
  >>=? fun (ctxt, exns) ->
  List.fold_left (fun res id ->
      res >>=? fun (ctxt, tnl) ->
      find_type_name_opt ctxt env id >>|? fun (ctxt, res) ->
      match res with
      | Some (LName _) -> ctxt, tnl (* no path : is a builtin type *)
      | Some tn -> ctxt, (id, tn) :: tnl
      | None -> raise (InvariantBroken
                         (Format.asprintf "Type name %a not found"
                            Ident.print_strident id))
    ) (return (ctxt, [])) free_ids.tyn
  >>=? fun (ctxt, types) ->
  let tvars = List.fold_left (fun tl n ->
      match StringMap.find_opt n env.tvars with
      | Some t -> (n, t) :: tl
      | None -> raise (InvariantBroken
                         (Format.asprintf "Type variable %s not found" n))
    ) [] free_ids.tyv in
  return (ctxt, Love_value.Value.{ values; structs; sigs; exns; types; tvars })

let sig_of_sigref ctxt env =
  let open TYPE in
  function
  | Anonymous s -> return (ctxt, s)
  | Named id ->
      find_sig_opt ctxt env id >>|? fun (ctxt, res) ->
      match res with
      | Some s -> ctxt, s
      | None -> raise (InvariantBroken "Signature not found")

let initialize () =

  init_values := StringIdentMap.empty;
  init_types := StringIdentMap.empty;
  init_counter := 0;

  let fresh () =
    init_counter := !init_counter + 1;
    !init_counter
  in

  init_types := Love_type_list.fold (fun tn _ init_types ->
      let id = Ident.create_id tn in
      StringIdentMap.add id (fresh (), id) init_types
    ) !init_types;

  let root_module, modules = Love_primitive.get_primitives_as_modules () in

  List.iter (fun (pn, p) ->
      init_values := StringIdentMap.add (Ident.create_id pn)
          (fresh (), Value.(Local (VPrimitive (p, ANone, [])))) !init_values
    ) root_module;

  List.iter (fun (mn, m) ->
      List.iter (fun (pn, p) ->
          let id = Ident.put_in_namespace mn (Ident.create_id pn) in
          init_values := StringIdentMap.add id
              (fresh (), Value.(Local (VPrimitive (p, ANone, [])))) !init_values
        ) m
    ) modules;
