(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Client_proto_contracts
open Client_proto_args
open Client_proto_context_commands
open Client_proto_dune_args
open Client_keys
open Clic

let proto_param ~name ~desc t =
  Clic.param
    ~name
    ~desc
    (Clic.parameter (fun _ str -> Lwt.return (Protocol_hash.of_b58check str)))
    t

let commands = ref []

let command ~group ~desc args path f =
  let cmd = Clic.command ~group ~desc args path f in
  commands := cmd :: !commands

let destinations_encoding =
  let open Data_encoding in
  list
    (obj5
       (req "address" Contract.encoding)
       (req "amount" Tez.encoding)
       (opt "collect_call_fee_gas" z)
       (dft "entrypoint" string "default")
       (opt "arg" string))

let read_destinations_file filename =
  Lwt_utils_unix.Json.read_file filename
  >>=? fun json ->
  match Data_encoding.Json.destruct destinations_encoding json with
  | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
      Format.kasprintf
        (fun s -> failwith "%s" s)
        "Invalid destinations file: %a %a"
        (fun ppf -> Data_encoding.Json.print_error ppf)
        exn
        Data_encoding.Json.pp
        json
  | destinations ->
      return destinations

let batch_transfer_command =
  command
    ~group:Client_proto_context_commands.group
    ~desc:"Batch transfer tokens."
    (args14
       fee_arg
       dry_run_switch
       verbose_signing_switch
       gas_limit_arg
       storage_limit_arg
       counter_arg
       no_print_source_flag
       minimal_fees_arg
       minimal_nanotez_per_byte_arg
       minimal_nanotez_per_gas_unit_arg
       force_low_fee_arg
       fee_cap_arg
       burn_cap_arg
       batch_size_arg)
    ( prefixes ["dune"; "batch"; "transfer"]
    @@ tez_param ~name:"total" ~desc:"total amount taken from source"
    @@ prefix "from"
    @@ ContractAlias.destination_param
         ~name:"src"
         ~desc:"name of the source contract"
    @@ prefix "to"
    @@ string
         ~name:"file"
         ~desc:
           "JSON file with destinations and amounts\n\
            of the form\n\
            [ {\"address\": \"dn1...\", \"amount\": \"10000\", \"arg\": \
            \"Unit\"}, ... ]\n\
            The field \"amount\" is given in mutez, and \"arg\" is optional."
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           no_print_source,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap,
           batch_size )
         total
         (_, source)
         destination_filename
         cctxt ->
      let fee_parameter =
        {
          Injection.minimal_fees;
          minimal_nanotez_per_byte;
          minimal_nanotez_per_gas_unit;
          force_low_fee;
          fee_cap;
          burn_cap;
        }
      in
      match Contract.is_implicit source with
      | None ->
          failwith
            "only implicit accounts can be the source of a batch transfer"
      | Some source -> (
          read_destinations_file destination_filename
          >>=? fun destinations ->
          Client_keys.get_key cctxt source
          >>=? fun (_, src_pk, src_sk) ->
          Client_proto_dune_context.batch_transfer
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            ?confirmations:cctxt#confirmations
            ~dry_run
            ~verbose_signing
            ~fee_parameter
            ?batch_size
            ~source
            ?fee
            ~src_pk
            ~src_sk
            ~destinations
            ~total
            ?gas_limit
            ?storage_limit
            ?counter
            ()
          >>= report_michelson_errors
                ~no_print_source
                ~msg:"transfer simulation failed"
                cctxt
          >>= function
          | None -> return_unit | Some (_res, _contracts) -> return_unit ))

let protocol_group =
  {Clic.name = "Dune Protocol"; title = "Dune Protocol specific commands"}

let file_parameter =
  Clic.parameter (fun _ p ->
      if not (Sys.file_exists p) then failwith "File doesn't exist: '%s'" p
      else return p)

let activate_protocol_command =
  command
    ~group:protocol_group
    ~desc:"Activate a protocol"
    Client_proto_context_commands.(
      args12
        fee_arg
        dry_run_switch
        verbose_signing_switch
        gas_limit_arg
        storage_limit_arg
        counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    ( prefixes ["dune"; "activate"; "protocol"]
    @@ proto_param ~name:"version" ~desc:"Protocol version (b58check)"
    @@ prefixes ["with"; "key"]
    @@ ContractAlias.destination_param
         ~name:"activator"
         ~desc:"Activator's key"
    @@ prefixes ["and"; "parameters"]
    @@ param
         ~name:"parameters"
         ~desc:"Protocol parameters (as JSON file)"
         file_parameter
    @@ prefixes ["at"; "level"]
    @@ param
         ~name:"level"
         ~desc:"Level"
         (parameter (fun _ctx s ->
              try return (Int32.of_string s)
              with _ -> failwith "%s is not an int32 value" s))
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap )
         protocol
         (_, source)
         param_json_file
         level
         (cctxt : Protocol_client_context.full) ->
      match Contract.is_implicit source with
      | None ->
          failwith
            "only implicit accounts can be the source of an activate protocol \
             operation"
      | Some source ->
          Client_keys.get_key cctxt source
          >>=? fun (_, src_pk, src_sk) ->
          Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
          (* TODO Dune: check whether `Shell_services.Protocol.list
             cctxt` contains the protocol *)
          >>=? fun json ->
          ( match Dune_parameters.destruct_parameters_exn json with
          | exception exn ->
              Lwt.return (error_exn exn)
          | s ->
              return s )
          >>=? fun protocol_parameters ->
          let fee_parameter =
            {
              Injection.minimal_fees;
              minimal_nanotez_per_byte;
              minimal_nanotez_per_gas_unit;
              force_low_fee;
              fee_cap;
              burn_cap;
            }
          in
          Client_proto_dune_context.activate_protocol_operation
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            ?confirmations:cctxt#confirmations
            ~dry_run
            ~verbose_signing
            ~fee_parameter
            ~level
            ~protocol
            ~protocol_parameters
            ~source
            ?fee
            ~src_pk
            ~src_sk
            ?gas_limit
            ?storage_limit
            ?counter
            ()
          >>=? fun _res -> return_unit)

let change_parameters_command =
  command
    ~group:protocol_group
    ~desc:"Change protocol parameters"
    Client_proto_context_commands.(
      args12
        fee_arg
        dry_run_switch
        verbose_signing_switch
        gas_limit_arg
        storage_limit_arg
        counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    ( prefixes ["dune"; "change"; "parameters"]
    @@ param
         ~name:"parameters"
         ~desc:"Protocol parameters (as JSON file)"
         file_parameter
    @@ prefixes ["with"; "key"]
    @@ ContractAlias.destination_param
         ~name:"activator"
         ~desc:"Activator's key"
    @@ prefixes ["at"; "level"]
    @@ param
         ~name:"level"
         ~desc:"Level"
         (parameter (fun _ctx s ->
              try return (Int32.of_string s)
              with _ -> failwith "%s is not an int32 value" s))
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap )
         param_json_file
         (_, source)
         level
         (cctxt : Protocol_client_context.full) ->
      match Contract.is_implicit source with
      | None ->
          failwith
            "only implicit accounts can be the source of a change of protocol \
             parameters"
      | Some source ->
          Client_keys.get_key cctxt source
          >>=? fun (_, src_pk, src_sk) ->
          Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
          >>=? fun json ->
          ( match Dune_parameters.destruct_parameters_exn json with
          | exception exn ->
              Lwt.return (error_exn exn)
          | s ->
              return s )
          >>=? fun protocol_parameters ->
          let fee_parameter =
            {
              Injection.minimal_fees;
              minimal_nanotez_per_byte;
              minimal_nanotez_per_gas_unit;
              force_low_fee;
              fee_cap;
              burn_cap;
            }
          in
          Client_proto_dune_context.activate_protocol_operation
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            ?confirmations:cctxt#confirmations
            ~dry_run
            ~verbose_signing
            ~fee_parameter
            ~level
            ~protocol_parameters
            ~source
            ?fee
            ~src_pk
            ~src_sk
            ?gas_limit
            ?storage_limit
            ?counter
            ()
          >>=? fun _res -> return_unit)

let manage_accounts_command =
  command
    ~group:protocol_group
    ~desc:"Manage accounts"
    Client_proto_context_commands.(
      args12
        fee_arg
        dry_run_switch
        verbose_signing_switch
        gas_limit_arg
        storage_limit_arg
        counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    ( prefixes ["dune"; "manage"; "accounts"]
    @@ prefixes ["with"; "key"]
    @@ ContractAlias.destination_param
         ~name:"activator"
         ~desc:"Activator's key"
    @@ prefixes ["and"; "parameters"]
    @@ param
         ~name:"parameters"
         ~desc:"Accounts parameters (as JSON file)"
         file_parameter
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap )
         (_, source)
         param_json_file
         (cctxt : Protocol_client_context.full) ->
      Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
      >>=? fun json ->
      match Contract.is_implicit source with
      | None ->
          failwith
            "only implicit accounts can be the source of a manage accounts \
             command"
      | Some source ->
          Client_keys.get_key cctxt source
          >>=? fun (_, src_pk, src_sk) ->
          ( match Dune_parameters.Accounts.destruct_exn json with
          | exception exn ->
              Lwt.return (error_exn exn)
          | s ->
              return s )
          >>=? fun protocol_parameters ->
          let protocol_parameters =
            Dune_parameters.Accounts.encode protocol_parameters
          in
          let fee_parameter =
            {
              Injection.minimal_fees;
              minimal_nanotez_per_byte;
              minimal_nanotez_per_gas_unit;
              force_low_fee;
              fee_cap;
              burn_cap;
            }
          in
          Client_proto_dune_context.manage_accounts_operation
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            ?confirmations:cctxt#confirmations
            ~dry_run
            ~verbose_signing
            ~fee_parameter
            ~protocol_parameters
            ~source
            ?fee
            ~src_pk
            ~src_sk
            ?gas_limit
            ?storage_limit
            ?counter
            ()
          >>=? fun _res -> return_unit)

let () =
  command
    ~group
    ~desc:"Clear Delegations"
    Client_proto_context_commands.(
      args12
        fee_arg
        dry_run_switch
        verbose_signing_switch
        gas_limit_arg
        storage_limit_arg
        counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    ( prefixes ["dune"; "clear"; "delegations"; "of"]
    @@ Public_key_hash.source_param ~name:"mgr" ~desc:"the delegate key"
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap )
         source
         (cctxt : Protocol_client_context.full) ->
      Client_keys.get_key cctxt source
      >>=? fun (_, src_pk, src_sk) ->
      let fee_parameter =
        {
          Injection.minimal_fees;
          minimal_nanotez_per_byte;
          minimal_nanotez_per_gas_unit;
          force_low_fee;
          fee_cap;
          burn_cap;
        }
      in
      Client_proto_dune_context.clear_delegations_operation
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        ?confirmations:cctxt#confirmations
        ~dry_run
        ~verbose_signing
        ~fee_parameter
        ~source
        ?fee
        ~src_pk
        ~src_sk
        ?gas_limit
        ?storage_limit
        ?counter
        ()
      >>=? fun _res -> return_unit)

let manage_account_command_template ~desc ~cmd f =
  command
    ~group
    ~desc
    Client_proto_context_commands.(
      args12
        fee_arg
        dry_run_switch
        verbose_signing_switch
        gas_limit_arg
        storage_limit_arg
        counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    ( prefixes ["dune"]
    @@ cmd
    @@ prefixes ["of"]
    @@ ContractAlias.destination_param ~name:"target" ~desc:"Target's key"
    @@ prefixes ["with"; "admin"]
    @@ ContractAlias.destination_param ~name:"admin" ~desc:"Admin's key"
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap )
         arg
         (_, contract)
         (_, admin)
         (cctxt : Protocol_client_context.full) ->
      ( match Contract.is_implicit contract with
      | None ->
          failwith "only implicit accounts can be the managed"
      | Some target ->
          return target )
      >>=? fun target ->
      ( match Contract.is_implicit admin with
      | None ->
          failwith "only implicit accounts can be admin here"
      | Some admin ->
          return admin )
      >>=? fun source ->
      f cctxt contract arg
      >>=? fun options ->
      let options = Dune_operation.Options.encode options in
      Client_keys.get_key cctxt source
      >>=? fun (_, src_pk, src_sk) ->
      Client_keys.get_key cctxt target
      >>=? fun (_, _tgt_pk, tgt_sk) ->
      ( if Signature.Public_key_hash.equal source target then return_none
      else
        Client_keys.sign
          cctxt
          ~interactive:(cctxt :> Client_context.io_wallet)
          tgt_sk
          options
        >>=? fun sign -> return_some (target, sign) )
      >>=? fun target ->
      let fee_parameter =
        {
          Injection.minimal_fees;
          minimal_nanotez_per_byte;
          minimal_nanotez_per_gas_unit;
          force_low_fee;
          fee_cap;
          burn_cap;
        }
      in
      Client_proto_dune_context.manage_account_operation
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        ?confirmations:cctxt#confirmations
        ~dry_run
        ~verbose_signing
        ~fee_parameter
        ?target
        ~options
        ~source
        ?fee
        ~src_pk
        ~src_sk
        ?gas_limit
        ?storage_limit
        ?counter
        ()
      >>=? fun _res -> return_unit)

let manage_account_command =
  manage_account_command_template
    ~desc:"Manage account"
    ~cmd:(fun next ->
      prefixes ["change"; "account"; "parameters"]
      @@ param
           ~name:"parameters"
           ~desc:"Account parameters (as JSON file)"
           file_parameter
      @@ next)
    (fun _cctxt _target param_json_file ->
      Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
      >>=? fun json ->
      match Dune_operation.Options.destruct_exn json with
      | exception exn ->
          Lwt.return (error_exn exn)
      | s ->
          return s)

let empty_options =
  Dune_operation.
    {
      admin = None;
      maxrolls = None;
      white_list = None;
      delegation = None;
      recovery = None;
      actions = [];
    }

let set_contract_command text f =
  manage_account_command_template
    ~desc:("set " ^ text)
    ~cmd:(fun next ->
      prefixes ["set"; text]
      @@ ContractAlias.destination_param
           ~name:("new " ^ text)
           ~desc:("New " ^ text)
      @@ next)
    (fun _cctxt target (_, admin) ->
      let admin = if Contract.(admin = target) then None else Some admin in
      return (f admin))

let set_admin_command =
  set_contract_command "admin" (fun c ->
      Dune_operation.{empty_options with admin = Some c})

let set_recovery_command =
  set_contract_command "recovery" (fun c ->
      Dune_operation.{empty_options with recovery = Some c})

let bool_param ~name ~desc next =
  Clic.param
    ~name
    ~desc
    (Clic.parameter (fun (_ : < .. >) s -> return @@ bool_of_string s))
    next

let int_option_param ~name ~desc next =
  Clic.param
    ~name
    ~desc
    (Clic.parameter (fun (_ : < .. >) s ->
         return @@ if s = "none" then None else Some (int_of_string s)))
    next

let set_delegation_command =
  manage_account_command_template
    ~desc:"set delegation"
    ~cmd:(fun next ->
      prefixes ["set"; "delegation"]
      @@ bool_param ~name:"new-delegation" ~desc:"Whether delegation is ok"
      @@ next)
    (fun _cctxt _target delegation ->
      return Dune_operation.{empty_options with delegation = Some delegation})

let set_maxrolls_command =
  manage_account_command_template
    ~desc:"set maxrolls"
    ~cmd:(fun next ->
      prefixes ["set"; "maxrolls"]
      @@ int_option_param ~name:"new-maxrolls" ~desc:"maxrolls: int or 'none'"
      @@ next)
    (fun _cctxt _target maxrolls ->
      return Dune_operation.{empty_options with maxrolls = Some maxrolls})

let set_whitelist_command =
  manage_account_command_template
    ~desc:"set white_list"
    ~cmd:(fun next ->
      prefixes ["set"; "whitelist"]
      @@ Clic.string
           ~name:"new-whitelist"
           ~desc:"whitelist: comma separated list of contracts'"
      @@ next)
    (fun cctxt _target whitelist ->
      ( if whitelist = "none" then return []
      else
        String.split ',' whitelist
        |> map_s (fun s ->
               ContractAlias.get_contract cctxt s >>=? fun (_, c) -> return c)
      )
      >>=? fun white_list ->
      return Dune_operation.{empty_options with white_list = Some white_list})

let get_parameters_command =
  command
    ~group
    ~desc:"Get the parameters of a contract."
    no_options
    ( prefixes ["dune"; "get"; "account"; "parameters"; "for"]
    @@ ContractAlias.destination_param ~name:"src" ~desc:"source contract"
    @@ stop )
    (fun () (_, contract) (cctxt : Protocol_client_context.full) ->
      Alpha_services.Contract.parameters
        cctxt
        (cctxt#chain, cctxt#block)
        contract
      >>=? fun options ->
      cctxt#answer "%a" Dune_operation.Options.pp options
      >>= fun () -> return_unit)

let get_account_info_command =
  command
    ~group
    ~desc:"Get the account info of a contract."
    no_options
    ( prefixes ["dune"; "get"; "account"; "info"; "for"]
    @@ ContractAlias.destination_param ~name:"src" ~desc:"source contract"
    @@ stop )
    (fun () (_, contract) (cctxt : Protocol_client_context.full) ->
      Alpha_services.Contract.account_info
        cctxt
        (cctxt#chain, cctxt#block)
        contract
      >>=? fun info ->
      cctxt#answer "%a" Alpha_services.Contract.Account_info.pp info
      >>= fun () -> return_unit)

let check_fundraiser_account =
  command
    ~group:Client_proto_context_commands.group
    ~desc:"Activate a fundraiser account."
    no_options
    ( prefixes ["dune"; "check"; "fundraiser"; "account"]
    @@ Client_proto_dune_context.Ed25519_public_key_hash.param
    @@ prefixes ["with"]
    @@ param
         ~name:"code"
         (Clic.parameter (fun _ctx code ->
              protect (fun () ->
                  return (Blinded_public_key_hash.activation_code_of_hex code))))
         ~desc:"Activation code obtained from the Tezos foundation."
    @@ stop )
    (fun () pkh code (cctxt : Protocol_client_context.full) ->
      let blinded_pkh = Blinded_public_key_hash.of_ed25519_pkh code pkh in
      cctxt#message
        "@[Blinded key hash = %a@]@."
        Blinded_public_key_hash.pp
        blinded_pkh
      >>= fun () -> return_unit)

let sign_command =
  command
    ~group
    ~desc:"Sign an operation."
    (args1 out_arg)
    ( prefixes ["sign"; "operation"]
    @@ param
         ~name:"op"
         (Clic.parameter (fun cctxt s ->
              let ignore_new_lines s =
                String.split '\n' s |> String.concat ""
              in
              match String.split ~limit:1 ':' s with
              | ["file"; s] ->
                  cctxt#read_file s >>|? ignore_new_lines
              | ["hex"; s] ->
                  return s
              | _ -> (
                try
                  Hex.to_bytes (`Hex s) |> ignore ;
                  return s
                with Invalid_argument _ -> (
                  cctxt#read_file s
                  >>= function
                  | Ok res ->
                      return (ignore_new_lines res)
                  | Error _ ->
                      failwith
                        "%S is neither a file nor a valid hexadecimal value"
                        s ) )))
         ~desc:"Serialized operation in hexadecimal (in a file or directly)."
    @@ prefix "for"
    @@ ContractAlias.destination_param
         ~name:"signer"
         ~desc:"name of the signer"
    @@ stop )
    (fun out op (_, src) (cctxt : #Client_context.full) ->
      match Contract.is_implicit src with
      | None ->
          failwith "only implicit accounts can sign an operation"
      | Some src_pkh ->
          Client_keys.get_key cctxt src_pkh
          >>=? fun (_src_name, _src_pk, src_sk) ->
          let bytes = Hex.to_bytes (`Hex op) in
          let (_shell, Contents_list contents) =
            Data_encoding.Binary.of_bytes_exn Operation.unsigned_encoding bytes
          in
          Shell_services.Chain.chain_id cctxt ~chain:cctxt#chain ()
          >>=? fun chain_id ->
          let watermark =
            match contents with
            | Single (Endorsement _) ->
                Signature.(Endorsement chain_id)
            | _ ->
                Signature.Generic_operation
          in
          Injection.simulate
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            contents
          >>= report_michelson_errors
                ~exit_on_error:false
                ~msg:"simulation failed"
                cctxt
          >>= (function
                | None ->
                    Lwt.return_unit
                | Some (_oph, _op, result) ->
                    cctxt#message
                      "@[<v 2>Parsed operation:@,%a@]"
                      Operation_result.pp_operation_result
                      (contents, result.contents))
          >>= fun () ->
          ( match contents with
          | Single (Manager_operation {source; _}) ->
              if Signature.Public_key_hash.(source <> src_pkh) then
                cctxt#warning
                  "Signing an operation whose source is %a for %a will fail \
                   when injecting."
                  Signature.Public_key_hash.pp
                  src_pkh
                  Signature.Public_key_hash.pp
                  source
                >>= return
              else return_unit
          | _ ->
              return_unit )
          >>=? fun () ->
          Dune_injection.confirm
            cctxt
            ~prompt:"Would you like to sign this operation?"
            ~msg:"Signing was rejected by the user."
          >>=? fun () ->
          Client_keys.sign
            cctxt
            ~watermark
            ~interactive:(cctxt :> Client_context.io_wallet)
            src_sk
            bytes
          >>=? fun signature ->
          cctxt#message "Signature: %a" Signature.pp signature
          >>= fun () ->
          let signed_bytes =
            Bytes.concat
              (Bytes.of_string "")
              [bytes; Signature.to_bytes signature]
          in
          ( match out with
          | None ->
              cctxt#message
                "@[<v>Signed operation:@,%a@]"
                Hex.pp
                (Hex.of_bytes signed_bytes)
          | Some f ->
              let (`Hex hex) = Hex.of_bytes signed_bytes in
              let oc = open_out f in
              output_string oc hex ;
              close_out oc ;
              cctxt#message
                "@[<v>Serialized signed operation written to %s@]"
                f )
          >>= return)

let inject_command =
  command
    ~group
    ~desc:"Inject an operation."
    (args1 no_print_source_flag)
    ( prefixes ["inject"; "operation"]
    @@ param
         ~name:"op"
         (Clic.parameter (fun cctxt s ->
              let ignore_new_lines s =
                String.split '\n' s |> String.concat ""
              in
              match String.split ~limit:1 ':' s with
              | ["file"; s] ->
                  cctxt#read_file s >>|? ignore_new_lines
              | ["hex"; s] ->
                  return s
              | _ -> (
                try
                  Hex.to_bytes (`Hex s) |> ignore ;
                  return s
                with Invalid_argument _ -> (
                  cctxt#read_file s
                  >>= function
                  | Ok res ->
                      return (ignore_new_lines res)
                  | Error _ ->
                      failwith
                        "%S is neither a file nor a valid hexadecimal value"
                        s ) )))
         ~desc:"Serialized operation in hexadecimal (in a file or directly)."
    @@ stop )
    (fun no_print_source op (cctxt : #Client_context.full) ->
      let bytes = Hex.to_bytes (`Hex op) in
      let op = Data_encoding.Binary.of_bytes_exn Operation.encoding bytes in
      let (Operation_data protocol_data) = op.protocol_data in
      Injection.simulate
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        protocol_data.contents
      >>=? fun (_oph, _op, result) ->
      cctxt#message
        "@[<v 2>Parsed operation:@,%a@]"
        Operation_result.pp_operation_result
        (protocol_data.contents, result.contents)
      >>= fun () ->
      Dune_injection.confirm
        cctxt
        ~prompt:"Would you like to inject this operation?"
        ~msg:"Injection was rejected by the user."
      >>=? fun () ->
      Injection.force_inject_operation
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        ?confirmations:cctxt#confirmations
        ~preapply_result:result
        {shell = op.shell; protocol_data}
      >>= report_michelson_errors
            ~no_print_source
            ~msg:"injection failed"
            cctxt
      >>= fun _ -> return_unit)

let output_protocol_parameters_command =
  command
    ~group
    ~desc:"Output protocol parameters."
    (args1 Client_proto_dune_args.output_arg)
    (fixed ["dune"; "output"; "protocol"; "parameters"])
    (fun ppf (cctxt : Protocol_client_context.full) ->
      Alpha_services.Constants.all cctxt (cctxt#chain, cctxt#block)
      >>= function
      | Ok constants ->
          Format.fprintf
            ppf
            "%a@."
            Alpha_context.Constants.JSON.pp
            constants.parametric ;
          return_unit
      | Error _ ->
          Format.eprintf
            "Warning: could not retrieve constants, printing defaults@." ;
          Format.fprintf
            ppf
            "%a@."
            Alpha_context.Constants.JSON.pp
            Alpha_context.Constants.default ;
          return_unit)

let pp_dune_parameters ppf options =
  let json =
    Data_encoding.Json.construct Dune_parameters.parameters_encoding options
  in
  Data_encoding.Json.pp ppf json

let get_activate_protocol_command =
  command
    ~group
    ~desc:"Get next activate protocol"
    (args1 Client_proto_dune_args.output_arg)
    (fixed ["dune"; "get"; "activate"; "protocol"])
    (fun ppf (cctxt : Protocol_client_context.full) ->
      Alpha_services.Constants.Dune.get_activate_protocol
        cctxt
        (cctxt#chain, cctxt#block)
      >>= function
      | Ok None ->
          Format.fprintf ppf "No protocol activation scheduled@." ;
          return_unit
      | Ok (Some (level, protocol, params)) ->
          Format.fprintf
            ppf
            "Protocol activate at level %ld@.  Protocol: %a@.  Params: %a@."
            level
            (Option.pp Protocol_hash.pp)
            protocol
            (Option.pp pp_dune_parameters)
            params ;
          return_unit
      | Error _ ->
          Format.eprintf "Error: could not retrieve next protocol activation@." ;
          return_unit)

let of_mutez_exn n =
  match Tez.of_mutez n with None -> assert false | Some n -> n

let () =
  command
    ~group
    ~desc:"Create a private network configuration"
    no_options
    ( prefixes ["dune"; "create"; "private"; "network"]
    @@ Clic.string ~name:"network-name" ~desc:"Network Name"
    @@ prefixes ["with"; "supply"]
    @@ tez_param ~name:"supply" ~desc:"total supply"
    @@ prefixes ["in"]
    @@ Clic.string ~name:"directory" ~desc:"Directory"
    @@ stop )
    (fun () network supply directory (_cctxt : Protocol_client_context.full) ->
      let supply_mutez = Tez.to_mutez supply in
      if Compare.Int64.(supply_mutez < 1_000_000_000_000L) then
        failwith
          "Error: the minimal supply for private networks should be at least \
           1_000_000@."
      else
        let network = String.capitalize_ascii network in
        let network_lowercase = String.lowercase_ascii network in
        Format.eprintf
          "Private Network configuration %s created in %s:@."
          network
          directory ;
        let write_file name s =
          let oc = open_out (Filename.concat directory name) in
          output_string oc s ;
          close_out oc ;
          Format.eprintf "  File %S@." name
        in
        let superadmin_keyset = Signature.generate_key () in
        let baker_keyset = Signature.generate_key () in
        let supply_keyset = Signature.generate_key () in
        let keys =
          List.map
            (fun (name, (pkh, pk, sk)) ->
              let name = Printf.sprintf "%s-%s" network_lowercase name in
              let sk_uri =
                Client_keys.make_sk_uri @@ Uri.of_string @@ "unencrypted:"
                ^ Signature.Secret_key.to_b58check sk
              in
              let pk_uri =
                Client_keys.make_pk_uri @@ Uri.of_string @@ "unencrypted:"
                ^ Signature.Public_key.to_b58check pk
              in
              let pk = Some (pk_uri, Some pk) in
              let sk = Some sk_uri in
              Client_keys.{name; pkh; pk; sk})
            [ ("admin", superadmin_keyset);
              ("baker", baker_keyset);
              ("supply", supply_keyset) ]
        in
        let json =
          Data_encoding.(
            Json.construct (list Client_keys.key_entry_encoding) keys)
        in
        let s = Format.asprintf "%a@." Data_encoding.Json.pp json in
        write_file (Printf.sprintf "%s.keyfile" network) s ;
        let (genesis_time, genesis_block) =
          Client_dune_commands.generate_genesis_hash ()
        in
        let (_, genesis_key, _) = superadmin_keyset in
        let genesis_key = Signature.Public_key.to_b58check genesis_key in
        let prefix_dir = "dune-" ^ network_lowercase in
        let p2p_version =
          Printf.sprintf
            "DUNE_%s_%s"
            (String.uppercase_ascii network)
            genesis_time
        in
        let only_allowed_bakers = true in
        let expected_pow = 20. in
        let access_control = true in
        let (`Hex p2p_secret) = Hex.of_bytes (Rand.generate 10) in
        (* TODO: set rewards and deposits to 0, set minimal_fee_factor
            to 0 *)
        let config = Dune_config.Set_config_mainnet.config in
        let config =
          {
            config with
            network;
            genesis_time;
            genesis_block;
            genesis_key;
            prefix_dir;
            p2p_version;
            bootstrap_peers = [];
            only_allowed_bakers;
            expected_pow;
            p2p_secret = Some p2p_secret;
            access_control;
            minimal_fee_factor = 0;
          }
        in
        let s =
          Data_encoding.Json.to_string
            ~minify:false
            (Data_encoding.Json.construct
               Tezos_base.Config_network.encoding
               config)
        in
        write_file (Printf.sprintf "%s-config.json" network) s ;
        let s =
          Printf.sprintf
            {|
let config = Config_type.{
  network = "%s";
  forced_protocol_upgrades = [];
  genesis_key = "%s" ;
  genesis_time = "%s" ;
  genesis_block = "%s";
  genesis_protocol = "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P";
  p2p_version = "%s" ;
  max_operation_data_length = 65536 ;
  encoding_version = 2 ;
  prefix_dir = "%s" ;
  p2p_port = 9733 ;
  rpc_port = 8733 ;
  discovery_port = 10733 ;
  signer_tcp_port = 7733 ;
  signer_http_port = 6733 ;
  bootstrap_peers = [ ] ;
  cursym = "\xC4\x91" ;
  tzcompat = false ;
  protocol_revision = 0 ;
  only_allowed_bakers = %b ;
  expected_pow = %.0f. ;
  self_amending = false ;

  number_of_cycles_between_burn = 10 ;
  share_of_balance_to_burn = 10 ;
  frozen_account_cycles = 60 ;
  p2p_secret = Some "%s" ;
  access_control = %b ;
  minimal_fee_factor = 0 ;
}
let config_option = Some config
|}
            network
            genesis_key
            genesis_time
            genesis_block
            p2p_version
            prefix_dir
            only_allowed_bakers
            expected_pow
            p2p_secret
            access_control
        in
        write_file (Printf.sprintf "set_config_%s.ml" network_lowercase) s ;
        let of_seconds n =
          match Period.of_seconds n with Error _ -> assert false | Ok n -> n
        in
        let constants = Alpha_context.Constants.default in
        let constants =
          {
            constants with
            time_between_blocks = [of_seconds 10L; of_seconds 15L];
            delay_per_missing_endorsement = of_seconds 2L;
            (* no deposits *)
            block_security_deposit = Tez.zero;
            endorsement_security_deposit = Tez.zero;
            (* no rewards *)
            block_reward = Tez.zero;
            endorsement_reward = Tez.zero;
            seed_nonce_revelation_tip = Tez.zero;
            (* no burn *)
            origination_size = 0;
            cost_per_byte = Tez.zero;
          }
        in
        let superadmin_balance_mutez = 10_000_000_000L in
        let baker_balance_mutez = 90_000_000_000L in
        let supply_balance_mutez =
          Int64.sub
            supply_mutez
            (Int64.add superadmin_balance_mutez baker_balance_mutez)
        in
        let superadmin_account =
          let (public_key_hash, _, _) = superadmin_keyset in
          Alpha_context.Parameters.
            {
              public_key_hash;
              public_key = None;
              amount = of_mutez_exn superadmin_balance_mutez;
            }
        in
        let baker_account =
          let (public_key_hash, pk, _) = baker_keyset in
          Alpha_context.Parameters.
            {
              public_key_hash;
              public_key = Some pk;
              amount = of_mutez_exn baker_balance_mutez;
            }
        in
        let supply_account =
          let (public_key_hash, _, _) = supply_keyset in
          Alpha_context.Parameters.
            {
              public_key_hash;
              public_key = None;
              amount = of_mutez_exn supply_balance_mutez;
            }
        in
        let parameters =
          Alpha_context.Parameters.
            {
              constants;
              bootstrap_accounts =
                [superadmin_account; baker_account; supply_account];
              bootstrap_contracts = [];
              commitments = [];
              security_deposit_ramp_up_cycles = None;
              no_reward_cycles = None;
            }
        in
        let s =
          Format.asprintf "%a@." Alpha_context.Parameters.JSON.pp parameters
        in
        write_file (Printf.sprintf "%s-parameters.json" network) s ;
        return_unit)

let get_delegate_specific_balance_command name f =
  command
    ~group
    ~desc:("Get the " ^ name ^ " balance of a contract.")
    no_options
    ( prefixes ["dune"; "get"; name; "balance"; "for"]
    @@ ContractAlias.destination_param ~name:"src" ~desc:"source contract"
    @@ stop )
    (fun () (_, contract) (cctxt : Protocol_client_context.full) ->
      match Contract.is_implicit contract with
      | None ->
          failwith "Originated contracts have no such balance"
      | Some pkh ->
          f cctxt (cctxt#chain, cctxt#block) pkh
          >>=? fun amount ->
          cctxt#answer "%a %s" Tez.pp amount Client_proto_args.tez_sym
          >>= fun () -> return_unit)

let get_full_balance_command =
  get_delegate_specific_balance_command "full" Alpha_services.Delegate.balance

let get_frozen_balance_command =
  get_delegate_specific_balance_command
    "frozen"
    Alpha_services.Delegate.frozen_balance

let get_staking_balance_command =
  get_delegate_specific_balance_command
    "staking"
    Alpha_services.Delegate.staking_balance

let exec_value_command =
  command
    ~group
    ~desc:"Executes a pure smart contract function or gets a value."
    (args3 no_print_source_flag arg_arg gas_limit_arg)
    ( prefixes ["dune"; "execute"]
    @@ value_param
         ~name:"value"
         ~desc:"Name of the global constant/function to be called."
    @@ prefix "from"
    @@ ContractAlias.destination_param
         ~name:"src"
         ~desc:"name of the contract executed"
    @@ stop )
    (fun (no_print_source, arg, gas) value_name (_, source) cctxt ->
      ( match Contract_repr.is_originated source with
      | Some _ -> (
          ( match gas with
          | None ->
              Alpha_services.Constants.all cctxt (cctxt#chain, cctxt#block)
              >>=? fun {parametric = {hard_gas_limit_per_operation; _}; _} ->
              return hard_gas_limit_per_operation
          | Some gas ->
              return gas )
          >>=? fun gas ->
          match arg with
          | None ->
              Alpha_services.Contract.exec_fun
                cctxt
                (cctxt#chain, cctxt#block)
                source
                value_name
                (None, gas)
          | Some param ->
              Dune_lang_v1_parser.subst_in_source cctxt param
              >>= fun param ->
              let param =
                (fst @@ Dune_lang_v1_parser.parse_expression param).expanded
              in
              Alpha_services.Contract.exec_fun
                cctxt
                (cctxt#chain, cctxt#block)
                source
                value_name
                (Some param, gas) )
      | None ->
          Error_monad.failwith "Must be a smart contract" )
      >>= report_michelson_errors
            ~no_print_source
            ~msg:(injection_failed "execute")
            cctxt
      >>= function
      | Some (Dune_expr d) ->
          cctxt#message "@[<v 0>Result: %a@]" Dune_script_repr.pp_const d
          >>= fun _ -> return_unit
      | _ ->
          return_unit)

let entrypoint_type_command =
  command
    ~group
    ~desc:"Returns the value type expected by an entrypoint."
    (args1 no_print_source_flag)
    ( prefixes ["dune"; "get"; "entrypoint"]
    @@ value_param ~name:"value" ~desc:"Name of the top-level value"
    @@ prefix "from"
    @@ ContractAlias.destination_param
         ~name:"src"
         ~desc:"name of the contract executed"
    @@ stop )
    (fun no_print_source entry_name (_, source) cctxt ->
      ( match Contract_repr.is_originated source with
      | Some _ ->
          Alpha_services.Contract.entrypoint_type
            cctxt
            (cctxt#chain, cctxt#block)
            source
            entry_name
      | None ->
          Error_monad.failwith "Must be a smart contract" )
      >>= report_michelson_errors
            ~no_print_source
            ~msg:(injection_failed "entrypoint_type")
            cctxt
      >>= function
      | Some (Dune_expr d) ->
          cctxt#message "@[<v 0>Result: %a@]" Dune_script_repr.pp_const d
          >>= fun _ -> return_unit
      | Some _ ->
          return_unit (* todo : michelson *)
      | _ ->
          return_unit)

let list_entrypoints_command =
  command
    ~group
    ~desc:"Lists the entrypoints of a contract."
    (args1 no_print_source_flag)
    ( prefixes ["dune"; "list"; "entrypoints"]
    @@ prefix "from"
    @@ ContractAlias.destination_param
         ~name:"src"
         ~desc:"name of the contract executed"
    @@ stop )
    (fun no_print_source (_, source) cctxt ->
      ( match Contract_repr.is_originated source with
      | Some _ ->
          Alpha_services.Contract.list_entrypoints
            cctxt
            (cctxt#chain, cctxt#block)
            source
      | None ->
          Error_monad.failwith "Must be a smart contract" )
      >>= report_michelson_errors
            ~no_print_source
            ~msg:(injection_failed "list_entrypoints")
            cctxt
      >>= function
      | Some (_, l) ->
          List.fold_left
            (fun acc e ->
              ( acc
                >>=? fun _ ->
                match e with
                | (name, Script_all.Dune_expr d) ->
                    cctxt#message
                      "@[<v 0>%s: %a@]"
                      name
                      Dune_script_repr.pp_const
                      d
                    >>= fun _ -> return_unit
                | (_, Dune_code _) ->
                    return_unit
                | (_, Michelson_expr _) ->
                    return_unit
                : unit tzresult Lwt.t )
              (* todo : michelson *))
            return_unit
            l
      | _ ->
          return_unit)

let unpack =
  command
    ~group
    ~desc:
      "Parse a byte sequence (in hexadecimal notation) as a love expression."
    (args1 gas_limit_arg)
    ( prefixes ["dune"; "unpack"; "data"]
    @@ Clic.param
         ~name:"bytes"
         ~desc:"the packed data to parse"
         Client_proto_args.bytes_parameter
    @@ prefixes ["of"; "type"]
    @@ Clic.param
         ~name:"data"
         ~desc:"the type of the data to unpack"
         data_parameter
    @@ stop )
    (fun original_gas bytes typ cctxt ->
      Alpha_services.Helpers.Scripts.unpack_data
        cctxt
        (cctxt#chain, cctxt#block)
        (bytes, typ.expanded, original_gas)
      >>=? function
      | None ->
          cctxt#message "Unpack failed" >>= fun () -> return_unit
      | Some (res, _remaining_gas) ->
          cctxt#message "Unpack: %a" Dune_lang_v1_printer.print_expr res
          >>= fun () -> return_unit)

let big_map_get_opt =
  command
    ~group
    ~desc:
      "Returns the value stored in a bigmap given its id and the unhashed key."
    (args1 no_print_source_flag)
    ( prefixes ["bigmap"; "get"]
    @@ prefix "id"
    @@ param
         ~name:"bigmap-id"
         (Clic.parameter (fun _cctxt s ->
              try return @@ Z.of_string s
              with _ -> failwith "Id is expecte to be an integer"))
         ~desc:"Id of the big map"
    @@ prefix "key"
    @@ param
         ~name:"bigmap-key"
         ~desc:"Key of the big map"
         (Clic.parameter (fun _ s ->
              return @@ Dune_lang_v1_parser.parse_expression ~check:true s))
    @@ stop )
    (fun no_print_source id value cctxt ->
      let v = (fst value).Dune_lang_v1_parser.expanded in
      Alpha_services.Contract.big_map_get_opt
        cctxt
        (cctxt#chain, cctxt#block)
        id
        v
      >>= report_michelson_errors
            ~no_print_source
            ~msg:(injection_failed "bigmap-get-opt")
            cctxt
      >>= function
      | Some (Some (Dune_expr e)) ->
          cctxt#message "@[<v 0> %a@]" Dune_script_repr.pp_const e
          >>= fun _ -> return_unit
      | Some None ->
          cctxt#message "@[<v 0> Not found@]" >>= fun _ -> return_unit
      | _ ->
          return_unit)

let lookup_operation_command =
  command
    ~group
    ~desc:"Look an operation up in the previous blocks."
    (args2
       (arg
          ~long:"distance"
          ~placeholder:"num_blocks"
          ~doc:
            "Maximum distance from specified block at which to search for the \
             operation"
          non_negative_param)
       (arg
          ~long:"timespan"
          ~placeholder:"seconds"
          ~doc:
            "Timespan in seconds before specified block for which to search \
             for the operation"
          non_negative_param))
    ( prefixes ["lookup"; "operation"]
    @@ param
         ~name:"operation"
         ~desc:"Operation hash to look for"
         (parameter (fun _ x ->
              match Operation_hash.of_b58check_opt x with
              | None ->
                  Error_monad.failwith "Invalid operation hash: '%s'" x
              | Some hash ->
                  return hash))
    @@ stop )
    (fun (distance, timespan) operation_hash cctxt ->
      let module Block_services = Block_services.Make (Protocol) (Protocol) in
      Block_services.Operation_lookup.lookup_operation
        ?distance
        ?timespan:(Option.map ~f:Int64.of_int timespan)
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        operation_hash
      >>= function
      | Error _ ->
          cctxt#error
            "@[<v 2>Operation %a was not found.@]"
            Operation_hash.pp
            operation_hash
      | Ok ((block, _pass), op) -> (
          cctxt#message
            "@[<v 2>Operation found in block:@,%a@]"
            Block_hash.pp
            block
          >>= fun () ->
          let (Operation_data {contents = op_content; _}) = op.protocol_data in
          let res =
            match op.receipt with
            | No_operation_metadata ->
                None
            | Operation_metadata {contents = result} -> (
              match
                Apply_results.maybe_pack_contents_list op_content result
              with
              | None ->
                  None
              | Some opres ->
                  let (_op, res) = Apply_results.unpack_contents_list opres in
                  Some res )
          in
          match res with
          | None ->
              cctxt#message "@[<v 2>Cannot show operation.@]"
              >>= fun () -> return_unit
          | Some res ->
              cctxt#message
                "@[<v 2>Operation:@,%a@]"
                Operation_result.pp_operation_result
                (op_content, res)
              >>= fun () -> return_unit ))

let dummy_activate_protocol_command =
  command
    ~group:protocol_group
    ~desc:"activate a protocol from genesis (for compatibility)"
    no_options
    ( prefixes ["activate"; "protocol"]
    @@ proto_param ~name:"version" ~desc:"Protocol version (b58check)"
    @@ prefixes ["with"; "fitness"]
    @@ param
         ~name:"fitness"
         ~desc:"Hardcoded fitness of the first block (integer)"
         (Clic.parameter (fun _ _ -> return_unit))
    @@ prefixes ["and"; "key"]
    @@ param
         ~name:"activator"
         ~desc:"Activator's account alias"
         (Clic.parameter (fun _ _ -> return_unit))
    @@ prefixes ["and"; "parameters"]
    @@ param
         ~name:"parameters"
         ~desc:"Parameters's file"
         (Clic.parameter (fun _ _ -> return_unit))
    @@ stop )
    (fun () new_hash _fitness _sk _param_json_file cctxt ->
      match Source.hash with
      | None ->
          assert false
      | Some current ->
          if Protocol_hash.(current = new_hash) then
            cctxt#message "This protocol is already activated" >>= return
          else
            failwith
              "Cannot activate protocol %S, because protocol %S is already \
               activated"
              (Protocol_hash.to_b58check new_hash)
              (Protocol_hash.to_b58check current))

let is_contained regexp s =
  match Str.search_forward regexp s 0 with
  | exception Not_found ->
      false
  | _ ->
      true

let () =
  command
    ~group:Client_proto_contracts_commands.group
    ~desc:"Search all known contracts in the wallet."
    no_options
    ( prefixes ["search"; "contracts"]
    @@ param
         ~name:"string"
         ~desc:"string to search"
         (Clic.parameter (fun _ s -> return s))
    @@ stop )
    (fun () string (cctxt : Protocol_client_context.full) ->
      list_contracts cctxt
      >>=? fun contracts ->
      let regexp = Str.regexp_string_case_fold string in
      iter_s
        (fun (prefix, alias, contract) ->
          let pkh = Contract.to_b58check contract in
          if is_contained regexp alias || is_contained regexp pkh then
            cctxt#message "%s%s: %s" prefix alias pkh >>= return
          else return_unit)
        contracts)

let commands () = !commands

let () =
  ignore
    [ ();
      batch_transfer_command;
      activate_protocol_command;
      dummy_activate_protocol_command;
      change_parameters_command;
      manage_accounts_command;
      manage_account_command;
      get_parameters_command;
      get_account_info_command;
      check_fundraiser_account;
      sign_command;
      inject_command;
      output_protocol_parameters_command;
      get_full_balance_command;
      get_frozen_balance_command;
      get_staking_balance_command;
      get_activate_protocol_command;
      exec_value_command;
      entrypoint_type_command;
      list_entrypoints_command;
      big_map_get_opt;
      unpack ]
