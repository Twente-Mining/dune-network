(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type kyc_level = Kyc_open | Kyc_source | Kyc_both

type config = {
  network : string;
  forced_protocol_upgrades : (Int32.t * string) list;
  genesis_key : string;
  genesis_time : string;
  genesis_block : string;
  genesis_protocol : string;
  p2p_version : string;
  max_operation_data_length : int;
  encoding_version : int;
  prefix_dir : string;
  (* "dune" or "tezos" *)
  p2p_port : int;
  (* 9733 *)
  rpc_port : int;
  (* 8733 *)
  discovery_port : int;
  (* 10733 *)
  signer_tcp_port : int;
  (* 7733 *)
  signer_http_port : int;
  (* 6733 *)
  bootstrap_peers : string list;
  (* [ "boot.tzbeta.net" ] *)
  cursym : string;
  tzcompat : bool;
  protocol_revision : int;
  only_allowed_bakers : bool;
  expected_pow : float;
  self_amending : bool;
  number_of_cycles_between_burn : int;
  share_of_balance_to_burn : int;
  frozen_account_cycles : int;
  p2p_secret : string option;
  nodes_list : string option;
  access_control : bool;
  minimal_fee_factor : int;
  closed_network : bool;
  kyc_level : kyc_level;
}

val config : config
