(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(** Dune addition : Pretty print information. The environment variable
    DUNE_DEBUG can be used to set the verbosity level, with "n"
    disabling all printing. *)
val printf : ?n:int -> ('a, Format.formatter, unit) format -> 'a

val string_of_exn : exn -> string

val polymorphic_compare : 'a -> 'a -> int

module Array : sig
  type 'a t

  val length : 'a t -> int

  val make : int -> 'a -> 'a t

  val get : 'a t -> int -> 'a

  val set : 'a t -> int -> 'a -> unit
end

val is_peer_id : string -> bool
