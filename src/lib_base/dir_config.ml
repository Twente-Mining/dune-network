(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let ( // ) = Filename.concat

let getenv_with_default ~default ~description var =
  let var =
    Tezos_stdlib.Environment_variable.make
      var
      ~description:(description ^ "\nDefault: " ^ default)
  in
  match Tezos_stdlib.Environment_variable.get_opt var with
  | None ->
      default
  | Some v ->
      v

let home =
  getenv_with_default
    "HOME"
    ~default:"/root"
    ~description:"Home directory of current user"

let prefix_dir =
  getenv_with_default
    "DUNE_PREFIX"
    ~default:Config.config.prefix_dir
    ~description:"Prefix of directories used by the Dune client and node"

let default_dir command =
  if Filename.is_relative prefix_dir then
    home // Printf.sprintf ".%s-%s" prefix_dir command
  else Filename.concat prefix_dir command
