(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* Dune: we moved this module here because we don't want 'dun' to depend
   on it. However, it looks like tezos-crypto depends on dune-config,
   and tezos-base depends on tezos-crypto, so it might become necessary
   to move it earlier in the dependencies, for example if we decide to
   move tezos compatibility mode or guild navigator mode to the config
   (currently, they are set by env variables, still in dune-config *)

open Dune_config
include Config_type

let default =
  match Set_config.config_option with
  | Some config ->
      config
  | None ->
      Set_config_private.config

let encoding = Config_encoding.encoding default

let get_config () =
  let config_filename =
    Tezos_stdlib.Environment_variable.get Config_encoding.variable
  in
  Printf.eprintf "%s=%S\n%!" Config_encoding.variable.name config_filename ;
  match config_filename with
  | "mainnet" ->
      Set_config_mainnet.config
  | "testnet" ->
      Set_config_testnet.config
  | "private" ->
      Set_config_private.config
  | _ ->
      if not (Sys.file_exists config_filename) then (
        Printf.eprintf
          "Error: %s targets inexistent file %S.\n%!"
          Config_encoding.variable.name
          config_filename ;
        exit 2 ) ;
      let config =
        let s = Dune_std.FILE.read config_filename in
        try
          let json = Ezjsonm.from_string s in
          Data_encoding.Json.destruct encoding json
        with exn ->
          Printf.eprintf
            "Error: %s in %S\n%!"
            (Printexc.to_string exn)
            config_filename ;
          exit 2
      in
      config

let config = try Some (get_config ()) with Not_found -> None

let config =
  match config with
  | Some config ->
      config
  | None -> (
    match Set_config.config_option with
    | Some config ->
        config
    | None ->
        Printf.eprintf
          "Error: the env variable %s must be defined.\n%!"
          Config_encoding.variable.name ;
        exit 2 )

let config =
  if Env_config.tezos_compatible_mode then
    {
      config with
      tzcompat = true;
      cursym = "\xEA\x9C\xA9";
      self_amending = true;
    }
  else config

let config =
  match Env_config.genesis_protocol_revision with
  | None ->
      config
  | Some protocol_revision ->
      {config with protocol_revision}

let () =
  if not config.tzcompat then
    Printf.eprintf
      "Config: %s. Revision: %d\n%!"
      config.network
      Source_config.max_revision ;
  ExternalCallback.register "get-config" (fun () -> config)

let config = ExternalCallback.callback "get-config" ()
