(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Lwt_log_core

let term = try Sys.getenv "TERM" with Not_found -> "none"

let syscall cmd =
  let (inc, outc, errc) = Unix.open_process_full cmd (Unix.environment ()) in
  let buf = Buffer.create 16 in
  let buferr = Buffer.create 16 in
  ( try
      while true do
        Buffer.add_channel buf inc 1
      done
    with End_of_file -> () ) ;
  ( try
      while true do
        Buffer.add_channel buferr errc 1
      done
    with End_of_file -> () ) ;
  let status = Unix.close_process_full (inc, outc, errc) in
  let s = Buffer.contents buferr in
  let l = String.length s in
  let serr = if l > 0 then String.sub s 0 (String.length s - 1) else s in
  (Buffer.contents buf, serr, status)

let has_colors =
  try
    match syscall ("tput -T" ^ term ^ " colors") with
    | (s, _, WEXITED 0) ->
        int_of_string (String.trim s) >= 8
    | _ ->
        false
  with _ -> false

let color_fmt level fmt =
  if not has_colors then fmt
  else
    match level with
    | Error ->
        "\027[31m" ^^ fmt ^^ "\027[0m"
    | Warning ->
        "\027[33m" ^^ fmt ^^ "\027[0m"
    | Fatal ->
        "\027[41m" ^^ fmt ^^ "\027[0m"
    | _ ->
        fmt
