(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Client_keys
open Clic

let group =
  {Clic.name = "dune"; title = "Additionnal commands provided by DUNE"}

let long_switch = switch ~short:'L' ~long:"long" ~doc:"print long version" ()

let algo_param () =
  Clic.parameter
    ~autocomplete:(fun _ -> return ["ed25519"; "secp256k1"; "p256"])
    (fun _ name ->
      match name with
      | "ed25519" ->
          return Signature.Ed25519
      | "secp256k1" ->
          return Signature.Secp256k1
      | "p256" ->
          return Signature.P256
      | name ->
          failwith
            "Unknown signature algorithm (%s). Available: 'ed25519', \
             'secp256k1' or 'p256'"
            name)

let sig_algo_arg =
  Clic.default_arg
    ~doc:"use custom signature algorithm"
    ~long:"sig"
    ~short:'s'
    ~placeholder:"ed25519|secp256k1|p256"
    ~default:"ed25519"
    (algo_param ())

module Make (S : sig
  val hash_algos : (string * (string -> string tzresult)) list
end) =
struct
  let hash_algo_param () =
    Clic.parameter
      ~autocomplete:(fun _ -> return (List.map fst S.hash_algos))
      (fun _ name ->
        try return (name, List.assoc name S.hash_algos)
        with Not_found ->
          failwith
            "Unknown hashing algorithm (%s). Available: %s"
            name
            (String.concat ", " (List.map fst S.hash_algos)))

  let hash_algo_arg =
    Clic.default_arg
      ~doc:"use custom b58hash prefix"
      ~long:"hash"
      ~short:'h'
      ~placeholder:(String.concat "| " (List.map fst S.hash_algos))
      ~default:"public_key"
      (hash_algo_param ())
end

module HASH_STRING = Make (struct
  let hash_algos =
    List.map
      (fun (s, f) -> (s, fun s -> Ok (f [s])))
      [ ( "block",
          fun contents ->
            Block_hash.to_b58check (Block_hash.hash_string contents) );
        ( "operation",
          fun contents ->
            Operation_hash.to_b58check (Operation_hash.hash_string contents) );
        ( "public_key_hash",
          fun contents ->
            let module Hash =
              Blake2B.Make
                (Base58)
                (struct
                  let name = "hash"

                  let title = "Blake2B hash"

                  let b58check_prefix = Base58.Prefix.ed25519_public_key_hash

                  let size = Some 20
                end)
            in
            Hash.to_b58check (Hash.hash_string contents) );
        ( "chain_id",
          fun contents -> Chain_id.to_b58check (Chain_id.hash_string contents)
        ) ]
end)

module HASH_HEXA = Make (struct
  let check_size hexa size =
    let size = 2 * size in
    let len = String.length hexa in
    if len < size then (
      Printf.eprintf
        "Warning: provided size too small (%d < %d), padding left with 0s\n"
        len
        size ;
      String.make (size - len) '0' ^ hexa )
    else hexa

  let hash_algos =
    [ ( "protocol_hash",
        fun hexa ->
          let hexa = check_size hexa Protocol_hash.size in
          Protocol_hash.of_hex (`Hex hexa)
          >>? fun s -> Ok (Protocol_hash.to_b58check s) );
      ( "block_hash",
        fun hexa ->
          Block_hash.of_hex (`Hex hexa)
          >>? fun s -> Ok (Block_hash.to_b58check s) );
      ( "operation_hash",
        fun hexa ->
          Operation_hash.of_hex (`Hex hexa)
          >>? fun s -> Ok (Operation_hash.to_b58check s) );
      ( "public_key_hash",
        fun hexa ->
          let hexa = check_size hexa Signature.Public_key_hash.size in
          Signature.Public_key_hash.of_hex (`Hex hexa)
          >>? fun s -> Ok (Signature.Public_key_hash.to_b58check s) );
      ( "public_key",
        fun hexa ->
          Signature.Public_key.of_hex (`Hex hexa)
          >>? fun s -> Ok (Signature.Public_key.to_b58check s) );
      ( "secret_key",
        fun hexa ->
          Signature.Secret_key.of_hex (`Hex hexa)
          >>? fun s -> Ok (Signature.Secret_key.to_b58check s) );
      ( "ed25519_public_key_hash",
        fun hexa ->
          let hexa = check_size hexa Ed25519.Public_key_hash.size in
          Ed25519.Public_key_hash.of_hex (`Hex hexa)
          >>? fun s -> Ok (Ed25519.Public_key_hash.to_b58check s) );
      ( "ed25519_public_key",
        fun hexa ->
          Ed25519.Public_key.of_hex (`Hex hexa)
          >>? fun s -> Ok (Ed25519.Public_key.to_b58check s) );
      ( "ed25519_secret_key",
        fun hexa ->
          Ed25519.Secret_key.of_hex (`Hex hexa)
          >>? fun s -> Ok (Ed25519.Secret_key.to_b58check s) );
      ( "ed25519_signature",
        fun hexa ->
          Ed25519.of_hex (`Hex hexa) >>? fun s -> Ok (Ed25519.to_b58check s) );
      ( "secp256k1_public_key_hash",
        fun hexa ->
          Secp256k1.Public_key_hash.of_hex (`Hex hexa)
          >>? fun s -> Ok (Secp256k1.Public_key_hash.to_b58check s) );
      ( "secp256k1_public_key",
        fun hexa ->
          Secp256k1.Public_key.of_hex (`Hex hexa)
          >>? fun s -> Ok (Secp256k1.Public_key.to_b58check s) );
      ( "secp256k1_secret_key",
        fun hexa ->
          Secp256k1.Secret_key.of_hex (`Hex hexa)
          >>? fun s -> Ok (Secp256k1.Secret_key.to_b58check s) );
      ( "secp256k1_signature",
        fun hexa ->
          Secp256k1.of_hex (`Hex hexa)
          >>? fun s -> Ok (Secp256k1.to_b58check s) );
      ( "p256_public_key_hash",
        fun hexa ->
          P256.Public_key_hash.of_hex (`Hex hexa)
          >>? fun s -> Ok (P256.Public_key_hash.to_b58check s) );
      ( "p256_public_key",
        fun hexa ->
          P256.Public_key.of_hex (`Hex hexa)
          >>? fun s -> Ok (P256.Public_key.to_b58check s) );
      ( "p256_secret_key",
        fun hexa ->
          P256.Secret_key.of_hex (`Hex hexa)
          >>? fun s -> Ok (P256.Secret_key.to_b58check s) );
      ( "p256_signature",
        fun hexa ->
          P256.of_hex (`Hex hexa) >>? fun s -> Ok (P256.to_b58check s) );
      ( "chain_id",
        fun hexa ->
          Chain_id.of_hex (`Hex hexa) >>? fun s -> Ok (Chain_id.to_b58check s)
      );
      ( "context_hash",
        fun hexa ->
          Context_hash.of_hex (`Hex hexa)
          >>? fun s -> Ok (Context_hash.to_b58check s) ) ]
end)

let output_arg =
  default_arg
    ~doc:"write to a file"
    ~long:"output"
    ~short:'o'
    ~placeholder:"path"
    ~default:"-"
    (parameter (fun _ ->
       function
       | "-" ->
           return Format.std_formatter
       | file ->
           let ppf = Format.formatter_of_out_channel (open_out file) in
           ignore Clic.(setup_formatter ppf Plain Full) ;
           return ppf))

let optional_output_arg =
  default_arg
    ~doc:"write to a file"
    ~long:"output"
    ~short:'o'
    ~placeholder:"path"
    ~default:"-"
    (parameter (fun _ ->
       function
       | "-" ->
           return None
       | file ->
           let ppf = Format.formatter_of_out_channel (open_out file) in
           ignore Clic.(setup_formatter ppf Plain Full) ;
           return (Some ppf)))

let input_arg ~default =
  default_arg
    ~doc:"read from a file"
    ~long:"input"
    ~short:'i'
    ~placeholder:"path"
    ~default
    (parameter (fun _ s -> return s))

let date_arg =
  arg
    ~doc:"specific date"
    ~long:"date in rfc3339 format"
    ~short:'d'
    ~placeholder:"date"
    (parameter (fun _ s ->
         match Time.Protocol.of_notation s with
         | Some d ->
             return d
         | None ->
             failwith "Bad date format, should in rfc3339 format"))

let commands = ref []

let command ~group ~desc args path f =
  let cmd = Clic.command ~group ~desc args path f in
  commands := cmd :: !commands

let () =
  command
    ~group
    ~desc:"Generate a pair of keys and print them."
    (args1 sig_algo_arg)
    (prefixes ["dune"; "gen"; "print"; "keys"] @@ stop)
    (fun algo (cctxt : Client_context.full) ->
      let (pkh, pk, sk) = Signature.generate_key ~algo () in
      let skloc = Tezos_signer_backends.Unencrypted.make_sk sk in
      cctxt#message "Hash: %a" Signature.Public_key_hash.pp pkh
      >>= fun () ->
      cctxt#message "Public Key: %a" Signature.Public_key.pp pk
      >>= fun () ->
      Secret_key.to_source skloc
      >>=? fun skloc ->
      cctxt#message "Secret Key: %s" skloc >>= fun () -> return_unit)

let backup_wallet cctxt f =
  Client_keys.Public_key.load cctxt
  >>=? fun pks ->
  Client_keys.Secret_key.load cctxt
  >>=? fun sks ->
  Client_keys.Public_key_hash.load cctxt
  >>=? fun pkhs ->
  f ()
  >>= fun res ->
  match res with
  | Error _ ->
      Client_keys.Public_key.set cctxt pks
      >>=? fun () ->
      Client_keys.Secret_key.set cctxt sks
      >>=? fun () ->
      Client_keys.Public_key_hash.set cctxt pkhs >>=? fun () -> Lwt.return res
  | Ok _ ->
      Lwt.return res

let () =
  command
    ~group
    ~desc:"Rename an existing key."
    (args1 (Secret_key.force_switch ()))
    ( prefixes ["dune"; "rename"; "key"]
    @@ Clic.string ~name:"src" ~desc:"old key name"
    @@ Clic.string ~name:"dst" ~desc:"new key name"
    @@ stop )
    (fun force src dst (cctxt : Client_context.full) ->
      (* every operation will load/save the file after each modification, so
          we use backup_wallet to be able to restore the original files in
          case one error happens in the middle. *)
      backup_wallet cctxt
      @@ fun () ->
      Client_keys.Public_key.find cctxt src
      >>=? fun (pk_uri, public_key) ->
      Client_keys.Secret_key.find cctxt src
      >>=? fun sk_uri ->
      Client_keys.Public_key_hash.find cctxt src
      >>=? fun pkh ->
      Secret_key.del cctxt src
      >>=? fun () ->
      Public_key.del cctxt src
      >>=? fun () ->
      Public_key_hash.del cctxt src
      >>=? fun () ->
      register_key cctxt ~force (pkh, pk_uri, sk_uri) dst ?public_key
      >>=? fun () ->
      cctxt#message "Key %S renamed to %S" src dst >>= fun () -> return_unit)

let () =
  command
    ~group
    ~desc:"Hash a string."
    (args1 HASH_STRING.hash_algo_arg)
    ( prefixes ["dune"; "hash"; "string"]
    @@ Clic.string ~name:"data" ~desc:"string to hash"
    @@ stop )
    (fun (kind, f) string (cctxt : Client_context.full) ->
      match f string with
      | Ok hash ->
          cctxt#message "Hash(%s): %s" kind hash >>= fun () -> return_unit
      | _ ->
          assert false)

let () =
  command
    ~group
    ~desc:"Print all the possible key hashes."
    no_options
    ( prefixes ["dune"; "print"; "key"; "hashes"]
    @@ Clic.string ~name:"keyhash" ~desc:"keyhash to print"
    @@ stop )
    (fun () keyhash (cctxt : Client_context.full) ->
      ( match Signature.Public_key_hash.of_b58check keyhash with
      | Ok key ->
          Lwt_list.iter_s
            (fun s -> cctxt#message "Keyhash: %s" s)
            (Signature.Public_key_hash.to_b58check_all key)
      | Error _ ->
          cctxt#message "Wrong key hash %S" keyhash )
      >>= fun () -> return_unit)

let () =
  command
    ~group
    ~desc:"Base58check to hexa."
    no_options
    ( prefixes ["dune"; "decode"; "b58check"]
    @@ Clic.string ~name:"b58check" ~desc:"b58check to print"
    @@ stop )
    (fun () s (cctxt : Client_context.full) ->
      ( match
          match Base58.decode s with
          | Some (Ed25519.Public_key_hash.Data pkh) ->
              Some
                ("ed25519_public_key_hash", Ed25519.Public_key_hash.to_hex pkh)
          | Some (Ed25519.Public_key.Data pkh) ->
              Some ("ed25519_public_key", Ed25519.Public_key.to_hex pkh)
          | Some (Ed25519.Secret_key.Data pkh) ->
              Some ("ed25519_secret_key", Ed25519.Secret_key.to_hex pkh)
          | Some (Ed25519.Data pkh) ->
              Some ("ed25519_signature", Ed25519.to_hex pkh)
          | Some (Secp256k1.Public_key_hash.Data pkh) ->
              Some
                ( "secp256k1_public_key_hash",
                  Secp256k1.Public_key_hash.to_hex pkh )
          | Some (Secp256k1.Public_key.Data pkh) ->
              Some ("secp256k1_public_key", Secp256k1.Public_key.to_hex pkh)
          | Some (Secp256k1.Secret_key.Data pkh) ->
              Some ("secp256k1_private_key", Secp256k1.Secret_key.to_hex pkh)
          | Some (Secp256k1.Data pkh) ->
              Some ("secp256k1_signature", Secp256k1.to_hex pkh)
          | Some (P256.Public_key_hash.Data pkh) ->
              Some ("p256_public_key_hash", P256.Public_key_hash.to_hex pkh)
          | Some (P256.Public_key.Data pkh) ->
              Some ("p256_public_key", P256.Public_key.to_hex pkh)
          | Some (P256.Secret_key.Data pkh) ->
              Some ("p256_secret_key", P256.Secret_key.to_hex pkh)
          | Some (P256.Data pkh) ->
              Some ("p256_signature", P256.to_hex pkh)
          | Some (Signature.Data pkh) ->
              Some ("signature", Signature.to_hex pkh)
          | Some (Context_hash.Data pkh) ->
              Some ("context_hash", Context_hash.to_hex pkh)
          | Some (Chain_id.Data pkh) ->
              Some ("chain_id", Chain_id.to_hex pkh)
          | Some (Protocol_hash.Data pkh) ->
              Some ("protocol_hash", Protocol_hash.to_hex pkh)
          | _ ->
              None
        with
      | None ->
          cctxt#message "Hash %S not implemented or wrong" s
      | Some (kind, `Hex hex) ->
          cctxt#message "%s = %s %s" s kind hex )
      >>= fun () -> return_unit)

let () =
  command
    ~group
    ~desc:"Base58check of hexa."
    (args1 HASH_HEXA.hash_algo_arg)
    ( prefixes ["dune"; "encode"; "b58check"]
    @@ Clic.string ~name:"b58check" ~desc:"hexa to display"
    @@ stop )
    (fun (kind, encode) hexa (cctxt : Client_context.full) ->
      match encode hexa with
      | Ok hash ->
          cctxt#message "%s %s = %s" kind hexa hash >>= fun () -> return_unit
      | Error errs ->
          refail errs)

let () =
  command
    ~group
    ~desc:"Decrypt an encrypted secret key."
    no_options
    (prefixes ["dune"; "decrypt"; "secret"; "key"] @@ stop)
    (fun () (cctxt : Client_context.full) ->
      cctxt#prompt_password "Enter encrypted secret key: "
      >>=? fun mb_uri ->
      let uri = Uri.of_string (Bigstring.to_string mb_uri) in
      ( match Uri.scheme uri with
      | Some "encrypted" ->
          return_unit
      | _ ->
          failwith
            "This command can only be used with the \"encrypted\" scheme" )
      >>=? fun () ->
      let sk_uri = make_sk_uri uri in
      Tezos_signer_backends.Encrypted.decrypt cctxt sk_uri
      >>=? fun skey ->
      cctxt#message
        "Decrypted secret key unencrypted:%a"
        Signature.Secret_key.pp
        skey
      >>= fun () -> return_unit)

let () =
  command
    ~group
    ~desc:"Change the encryption password of an implicit account's secret key."
    no_options
    ( prefixes ["dune"; "change"; "secret"; "key"; "password"; "for"; "address"]
    @@ Public_key_hash.alias_param @@ stop )
    (fun () (name, _) (cctxt : #Client_context.full) ->
      alias_keys cctxt name
      >>=? fun key_info ->
      match key_info with
      | None ->
          cctxt#message "No keys found for address" >>= fun () -> return_unit
      | Some (pkh, pk_opt, sk_uri_opt) -> (
        match pk_opt with
        | None ->
            cctxt#message "No public key found for address"
            >>= fun () -> return_unit
        | Some pk -> (
            let pk_uri = Tezos_signer_backends.Unencrypted.make_pk pk in
            match sk_uri_opt with
            | None ->
                cctxt#message "No secret key found for address"
                >>= fun () -> return_unit
            | Some sk_uri ->
                ( match Uri.scheme (sk_uri :> Uri.t) with
                | Some "encrypted" ->
                    Tezos_signer_backends.Encrypted.decrypt cctxt sk_uri
                    >>=? fun skey -> return skey
                | Some "unencrypted" ->
                    Lwt.return
                      (Signature.Secret_key.of_b58check
                         (Uri.path (sk_uri :> Uri.t)))
                | _ ->
                    failwith
                      "Secret key doesn't have \"encrypted\" or \
                       \"unencrypted\" scheme" )
                >>=? fun skey ->
                Tezos_signer_backends.Encrypted.encrypt cctxt skey
                >>=? fun sk_uri ->
                register_key cctxt ~force:true (pkh, pk_uri, sk_uri) name
                >>=? fun () -> return_unit ) ))

let () =
  command
    ~group
    ~desc:"Find a base58 prefix."
    no_options
    ( prefixes ["dune"; "find"; "base58"; "prefix"]
    @@ Clic.string ~name:"target" ~desc:"target to generate"
    @@ prefixes ["for"; "size"]
    @@ Clic.string ~name:"hash_size" ~desc:"size of hash"
    @@ stop )
    (fun () target hash_len _cctxt ->
      let hash_len = int_of_string hash_len in
      if not (Base58.Alphabet.all_in_alphabet Base58.Alphabet.bitcoin target)
      then (
        Format.eprintf
          "Invalid target %s. Chars must be in %a@."
          target
          Base58.Alphabet.pp
          Base58.Alphabet.bitcoin ;
        return_unit )
      else
        let hash_bottom = String.make hash_len '\000' in
        let hash_top = String.make hash_len '\255' in
        let target_len = String.length target in
        let rec iter prefix n =
          if n = 256 then (
            if String.length prefix = 1 then
              Printf.eprintf "%d/256 tested\n%!" (1 + int_of_char prefix.[0]) ;
            false )
          else
            let new_prefix = prefix ^ String.make 1 (char_of_int n) in
            if
              String.sub
                (Base58.safe_encode (new_prefix ^ hash_bottom))
                0
                target_len
              = target
              && String.sub
                   (Base58.safe_encode (new_prefix ^ hash_top))
                   0
                   target_len
                 = target
            then (
              Printf.eprintf "Found: \"" ;
              for i = 0 to String.length new_prefix - 1 do
                Printf.eprintf "\\%03d" (int_of_char new_prefix.[i])
              done ;
              Printf.eprintf
                "\" (%d)\n"
                (String.length (Base58.safe_encode (new_prefix ^ hash_bottom))) ;
              true )
            else
              ( if String.length new_prefix < target_len then iter new_prefix 0
              else false )
              || iter prefix (n + 1)
        in
        ignore (iter "" 0) ;
        return_unit)

let generate_genesis_hash ?date () =
  let prefix = "BLockGenesisGenesisGenesisGenesisGenesis" in
  let date =
    match date with
    | None ->
        Time.Protocol.of_notation_exn @@ Lwt_main.run @@ Lwt_process.pread_line
        @@ Lwt_process.shell "TZ='UTC' date +%FT%TZ"
    | Some date ->
        date
  in
  let suffix =
    String.sub
      (Base58.raw_encode (Digest.string (Time.Protocol.to_notation date)))
      0
      11
  in
  let (genesis, date) =
    match Base58.raw_decode (prefix ^ suffix) with
    | None ->
        assert false
    | Some p ->
        let p = String.sub p 0 (String.length p - 4) in
        (Base58.safe_encode p, Time.Protocol.to_notation date)
  in
  let _ = Block_hash.of_b58check_exn genesis in
  (date, genesis)

let () =
  command
    ~group
    ~desc:"Generate a Genesis block hash."
    (args2 date_arg output_arg)
    (fixed ["dune"; "generate"; "genesis"; "hash"])
    (fun (date, ppf) _cctxt ->
      let (date, genesis) = generate_genesis_hash ?date () in
      Format.fprintf ppf "Date: %s@." date ;
      Format.fprintf ppf "Hash: %s@." genesis ;
      return_unit)

open Tezos_rpc_http
open Tezos_rpc_http_server
open Tezos_stdlib_unix
module StringMap = Map.Make (String)

let content_type file =
  let len = String.length file in
  let dot_pos =
    match String.rindex file '.' with exception Not_found -> -1 | pos -> pos
  in
  let ext = String.sub file (dot_pos + 1) (len - dot_pos - 1) in
  match
    match String.lowercase_ascii ext with
    | "js" ->
        Some "application/javascript"
    | "html" | "htm" ->
        Some "text/html"
    | "css" ->
        Some "text/css"
    | "png" ->
        Some "image/png"
    | _ ->
        None
  with
  | None ->
      []
  | Some content_type ->
      [("content-type", content_type)]

let normalize_path path =
  let rec iter path rev =
    match path with
    | [] ->
        List.rev rev
    | "." :: path ->
        iter path rev
    | ".." :: path -> (
      match rev with [] -> iter path rev | _ :: rev -> iter path rev )
    | hd :: tl ->
        iter tl (hd :: rev)
  in
  iter path []

let dynamic_webserver root _meth ~path ~headers:_ =
  let path = normalize_path path in
  let path = match path with [] -> ["index.html"] | _ -> path in
  let file = String.concat "/" path in
  Printf.eprintf "Web request: %s\n%!" file ;
  ( match root with
  | None ->
      Lwt.return_none
  | Some dir ->
      Lwt.catch
        (fun () ->
          let filename = Filename.concat dir file in
          Printf.eprintf "Filename: %s\n%!" filename ;
          if Sys.file_exists filename then
            Lwt_utils_unix.read_file filename
            >>= fun content -> Lwt.return_some (content_type file, content)
          else Lwt.return_none)
        (fun _ -> Lwt.return_none) )
  >>= function Some v -> Lwt.return_some v | None -> Lwt.return_none

let run_server (_cctxt : #Client_context.wallet) ~root ~port =
  let dir = RPC_directory.empty in
  let mode : Conduit_lwt_unix.server = `TCP (`Port port) in
  Lwt.catch
    (fun () ->
      Lwt.choose
        [ (Lwt_exit.termination_thread >>= fun _code -> failwith "terminated");
          (let host = "0.0.0.0" in
           RPC_server.launch
             ~host
             mode
             dir
             ~media_types:Media_type.all_media_types
             ~dynamic:(dynamic_webserver root)
           >>= fun _server -> fst (Lwt.wait ())) ])
    (function
      | Unix.Unix_error (Unix.EADDRINUSE, "bind", "") ->
          failwith "Port already in use."
      | exn ->
          Lwt.return (error_exn exn))

let () =
  command
    ~group
    ~desc:"Start a web server"
    (args2
       (default_arg
          ~doc:"listening TCP port or service name"
          ~short:'p'
          ~long:"port"
          ~placeholder:"port number"
          ~default:"9000"
          (parameter (fun _ s -> return s)))
       (arg
          ~doc:"root directory"
          ~short:'r'
          ~long:"root"
          ~placeholder:"root directory"
          (parameter (fun _ s -> return s))))
    (fixed ["dune"; "server"])
    (fun (port, root) cctxt ->
      let port = int_of_string port in
      Printf.eprintf "Starting web server on port %d\n%!" port ;
      run_server cctxt ~root ~port >>=? fun _cctxt -> return_unit)

let random_string =
  let bitcoin_alphabet =
    "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
  in
  let bitcoin_alphabet_len = String.length bitcoin_alphabet in
  fun len ->
    String.init len (fun _ ->
        bitcoin_alphabet.[Random.int bitcoin_alphabet_len])

let random_lowercase_string =
  let bitcoin_alphabet = "abcdefghijkmnopqrstuvwxyz" in
  let bitcoin_alphabet_len = String.length bitcoin_alphabet in
  fun len ->
    String.init len (fun _ ->
        bitcoin_alphabet.[Random.int bitcoin_alphabet_len])

let random_email () =
  Printf.sprintf
    "%s.%s@dune.example.com"
    (random_lowercase_string 8)
    (random_lowercase_string 8)

module Blinded_public_key_hash = struct
  module H =
    Blake2B.Make
      (Base58)
      (struct
        let name = "Blinded public key hash"

        let title = "A blinded public key hash"

        let b58check_prefix = "\001\002\049\223"

        let size = Some Ed25519.Public_key_hash.size
      end)

  include H

  let () = Base58.check_encoded_prefix b58check_encoding "btz1" 37

  let of_ed25519_pkh activation_code pkh =
    hash_bytes ~key:activation_code [Ed25519.Public_key_hash.to_bytes pkh]

  let activation_code_size = Ed25519.Public_key_hash.size

  let activation_code_encoding = Data_encoding.Fixed.bytes activation_code_size

  let _activation_code_of_hex h =
    if Compare.Int.(String.length h <> activation_code_size * 2) then
      invalid_arg "Blinded_public_key_hash.activation_code_of_hex" ;
    MBytes.of_hex (`Hex h)
end

let faucet_encoding =
  let open Data_encoding in
  obj6
    (req "pkh" Ed25519.Public_key_hash.encoding)
    (req "amount" string)
    (req "secret" Blinded_public_key_hash.activation_code_encoding)
    (req "mnemonic" (list string))
    (req "password" string)
    (req "email" string)

let commitments_encoding =
  Data_encoding.(
    obj1
      (req "commitments" (list (tup2 Blinded_public_key_hash.encoding string))))

let blind_arg ~default =
  default_arg
    ~doc:"blind argument"
    ~long:"blind"
    ~short:'b'
    ~placeholder:"blind"
    ~default
    (parameter (fun _ s -> return s))

let save_value file encoding value =
  let s =
    Data_encoding.Json.to_string
      ~newline:true
      ~minify:false
      (Data_encoding.Json.construct encoding value)
  in
  let oc = open_out file in
  output_string oc s ;
  close_out oc ;
  Printf.eprintf "File %S generated\n%!" file

let () =
  command
    ~group
    ~desc:"Generate a sandbox faucet."
    (args1 (blind_arg ~default:(random_string 10)))
    ( prefixes ["dune"; "gen"]
    @@ Clic.string ~name:"nfaucets" ~desc:"number of faucets"
    @@ prefixes ["faucets"]
    @@ stop )
    (fun blind nfaucets (_cctxt : Client_context.full) ->
      let nfaucets = int_of_string nfaucets in
      let blind = Bytes.of_string blind in
      let commitments = ref [] in
      for i = 1 to nfaucets do
        let entropy =
          Blinded_public_key_hash.hash_bytes
            ~key:blind
            [Bytes.of_string (string_of_int i)]
        in
        let mnemonic =
          Bip39.of_entropy
            (Bigstring.of_bytes (Blinded_public_key_hash.to_bytes entropy))
        in
        let password = random_string 10 in
        let email = random_email () in
        let passphrase =
          Bigstring.(concat "" [of_string email; of_string password])
        in
        let seed = Bip39.to_seed ~passphrase mnemonic in
        let sk = Bigstring.sub seed 0 32 in
        let sk =
          match Ed25519.sk_of_bytes_opt (Bigstring.to_bytes sk) with
          | None ->
              assert false
          | Some sk ->
              sk
        in
        let sk = Tezos_crypto.Signature.((Ed25519 sk : secret_key)) in
        let pk = Signature.Secret_key.to_public_key sk in
        let pkh = Signature.Public_key.hash pk in
        let pkh_b58 =
          match pkh with Ed25519 pkh -> pkh | _ -> assert false
        in
        let activation_code =
          Blinded_public_key_hash.hash_bytes
            ~key:blind
            [Signature.Public_key_hash.to_bytes pkh]
        in
        let amount = "100000000" in
        let mnemonic = Bip39.to_words mnemonic in
        let activation_code =
          Blinded_public_key_hash.to_bytes activation_code
        in
        save_value
          (Printf.sprintf "faucet_%d.json" i)
          faucet_encoding
          (pkh_b58, amount, activation_code, mnemonic, password, email) ;
        let blinded_pkh =
          Blinded_public_key_hash.of_ed25519_pkh activation_code pkh_b58
        in
        commitments := (blinded_pkh, amount) :: !commitments
      done ;
      save_value "commitments.json" commitments_encoding !commitments ;
      return ())

let commands = !commands
