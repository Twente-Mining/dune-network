(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

val commands : Client_context.full Clic.command list

val group : Clic.group

val input_arg : default:string -> (string, 'a) Clic.arg

val output_arg : (Format.formatter, Client_context.full) Clic.arg

val optional_output_arg :
  (Format.formatter option, Client_context.full) Clic.arg

val long_switch : (bool, Client_context.full) Clic.arg

module HASH_STRING : sig
  val hash_algo_arg :
    (string * (string -> string tzresult), Client_context.full) Clic.arg
end

val generate_genesis_hash : ?date:Time.Protocol.t -> unit -> string * string
