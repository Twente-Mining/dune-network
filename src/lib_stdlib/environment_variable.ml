(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type t = {
  name : string;
  description : string;
  allowed_values : string list option;
  visible : string list;
}

module Map = TzString.Map

let var_register = ref Map.empty

let make ?allowed_values ?(visible = []) name ~description =
  let v = {name; description; allowed_values; visible} in
  var_register := Map.add name v !var_register ;
  v

(* let get s = Map.find s !var_register
 *
 * let get_opt s = Map.find_opt s !var_register *)

let get v = Sys.getenv v.name

let get_opt v = Sys.getenv_opt v.name

let get_all () =
  Map.fold (fun _ v acc -> v :: acc) !var_register [] |> List.rev

(* let env_info v =
 *   let doc = match v.allowed_values with
 *     | None -> v.description
 *     | Some allowed ->
 *         v.description ^ "\n$(b,Accepted values:) " ^
 *         String.concat ", " allowed in
 *   Cmdliner.Term.env_info
 *     v.name
 *     ~doc
 *
 * let get_all_envs_info () =
 *   Map.fold (fun _ v acc -> env_info v :: acc) !var_register []
 *   |> List.rev *)
