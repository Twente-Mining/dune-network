let max_files = ref 10

let rotate_file file =
  let rec iter file filebase n =
    if Sys.file_exists file then (
      if n > !max_files then (
        Printf.eprintf "tzrotate: remove %s\n%!" file ;
        Sys.remove file )
      else
        let next_file = Printf.sprintf "%s.%d" filebase n in
        iter next_file filebase (n + 1) ;
        Printf.eprintf "tzrotate: rename %s to %s\n%!" file next_file ;
        Sys.rename file next_file )
  in
  iter file file 1

let () =
  let arg_list =
    [("--max", Arg.Int (( := ) max_files), "N Maximal log file number")]
  in
  let arg_usage = "tzrotate LOGFILES: rotate log files" in
  Arg.parse arg_list rotate_file arg_usage
