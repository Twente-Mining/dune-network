(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Config.TYPES

(*
let sandbox_file_name = "sandbox.json"
let sandbox_file_content = {|
{
    "genesis_pubkey":
      "edpkuSLWfVU1Vq7Jg9FucPyKmma6otcMHac9zG4oU1KMHSTBpJuGQ2"
}
|}
*)

(*
          (req "bootstrap_accounts" (list bootstrap_account_encoding))
          (dft "bootstrap_contracts" (list bootstrap_contract_encoding) [])
          (dft "commitments" (list Commitment_repr.encoding) [])
          (opt "security_deposit_ramp_up_cycles" int31)
          (opt "no_reward_cycles" int31))

constants:
          (opt "preserved_cycles" uint8)
          (opt "blocks_per_cycle" int32)
          (opt "blocks_per_commitment" int32)
          (opt "blocks_per_roll_snapshot" int32)
          (opt "blocks_per_voting_period" int32)
          (opt "time_between_blocks" (list Period_repr.encoding)
          (opt "endorsers_per_  block" uint16)
          (opt "hard_gas_limit_per_operation" z)
          (opt "hard_gas_limit_per_block" z)
             (opt "proof_of_work_threshold" int64)
             (opt "tokens_per_roll" Tez_repr.encoding)
             (opt "michelson_maximum_type_size" uint16)
             (opt "seed_nonce_revelation_tip" Tez_repr.encoding)
             (opt "origination_burn" Tez_repr.encoding)
             (opt "block_security_deposit" Tez_repr.encoding)
             (opt "endorsement_security_deposit" Tez_repr.encoding)
             (opt "block_reward" Tez_repr.encoding))
             (opt "endorsement_reward" Tez_repr.encoding)
             (opt "cost_per_byte" Tez_repr.encoding)
             (opt "hard_storage_limit_per_operation" z)

*)

let protocol_parameters_file_name = "protocol_parameters.json"

let protocol_parameters_file_content () =
  let b = Buffer.create 1000 in
  Buffer.add_string b {|
{ "bootstrap_accounts":
  [
|} ;
  let first = ref true in
  List.iter
    (fun id ->
      match id.account with
      | "" | "0" ->
          ()
      | _ ->
          if !first then first := false else Buffer.add_string b ",\n" ;
          Printf.bprintf b {|  [ "%s", "%s" ] |} id.public_key id.account)
    Config.c.identities ;
  Buffer.add_string
    b
    {|
  ],
  "time_between_blocks" : [ "3", "3" ],
  "blocks_per_roll_snapshot" : 512,
  "blocks_per_cycle" : 4096,
  "blocks_per_voting_period" : 64,
  "blocks_per_commitment" : 4,
  "preserved_cycles" : 5,
  "proof_of_work_threshold": "-1"
}
|} ;
  Buffer.contents b

(*
activator/dictator_pubkey should not be put in the config file anymore
  "activator_pubkey":
    "edpkuSLWfVU1Vq7Jg9FucPyKmma6otcMHac9zG4oU1KMHSTBpJuGQ2",

not in config file anymore
  "first_free_baking_slot" : 4
*)

let version_file_name = "version.json"

let version_file_content = {|
{
  "version": "0.0.3"
}
|}

(*
Hash: tz1id1nnbxUwK1dgJwUra5NEYixK37k6hRri
Public Key: edpkv2Gafe23nz4x4hJ1SaUM3H5hBAp5LDwsvGVjPsbMyZHNomxxQA
Secret Key: unencrypted:edsk4LhLg3212HzL7eCXfCWWvyWFDfwUS7doL4JUSmkgTe4qwZbVYm

let activator_secret = "edsk4LhLg3212HzL7eCXfCWWvyWFDfwUS7doL4JUSmkgTe4qwZbVYm"
*)

let shared_tezos_node_sh =
  {|#!/bin/sh

    NODE_DIR=`dirname $0`
    . $NODE_DIR/env.sh

COMMAND="$DUNE_SRCDIR/dune-node run --data-dir $DUNE_RUNDIR/$DUNE_NODE $*"

    echo "$COMMAND"
    exec $COMMAND 2>&1
  |}

let shared_tezos_client_sh =
  {|#!/bin/sh

    NODE_DIR=`dirname $0`
    . $NODE_DIR/env.sh

COMMAND="$DUNE_SRCDIR/dune-client -base-dir $DUNE_RUNDIR/$DUNE_NODE/client --addr $DUNE_ADDR --port $DUNE_RPC_PORT $*"

    echo "$COMMAND"
    exec $COMMAND 2>&1

  |}

let shared_tezos_baker_sh ~proto_name =
  Printf.sprintf
    {|#!/bin/sh

      NODE_DIR=`dirname $0`
      . $NODE_DIR/env.sh

COMMAND="$DUNE_SRCDIR/dune-baker-%s --base-dir $DUNE_RUNDIR/$DUNE_NODE/client --addr $DUNE_ADDR --port $DUNE_RPC_PORT run with local node $DUNE_RUNDIR/$DUNE_NODE $DUNE_ACCOUNT $*"

      echo "$COMMAND"
      exec $COMMAND 2>&1
    |}
    proto_name

let shared_tezos_endorser_sh ~proto_name =
  Printf.sprintf
    {|#!/bin/sh

      NODE_DIR=`dirname $0`
      . $NODE_DIR/env.sh

COMMAND="$DUNE_SRCDIR/dune-endorser-%s --base-dir $DUNE_RUNDIR/$DUNE_NODE/client --addr $DUNE_ADDR --port $DUNE_RPC_PORT run $DUNE_ACCOUNT $*"

      echo "$COMMAND"
      exec $COMMAND 2>&1
    |}
    proto_name

let shared_tezos_activate_sh =
  {|#!/bin/sh

    NODE_DIR=`dirname $0`
    . $NODE_DIR/env.sh

COMMAND="$DUNE_SRCDIR/dune-client -base-dir $DUNE_RUNDIR/$DUNE_NODE/client --addr $DUNE_ADDR --port $DUNE_RPC_PORT -block genesis activate protocol $PROTO_HASH with fitness 1 and key activator and parameters $DUNE_RUNDIR/protocol_parameters.json"

    echo "$COMMAND"
    exec $COMMAND 2>&1
  |}
