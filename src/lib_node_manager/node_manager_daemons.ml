(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Node_manager_utils
open Node_manager_state
open Node_manager_encodings
module Public_key_hash = Signature.Public_key_hash

let pp_kind ppf k = Format.pp_print_string ppf (string_of_kind k)

let get_opt ~id = DaemonSet.find_opt ~id global_state.daemons

let get ~id = DaemonSet.find ~id global_state.daemons

let overlap_delegates id desc protocols all_active_protocols daemons =
  List.find_opt
    (fun o ->
      id <> o.id
      && ( all_active_protocols
         || not Protocol_hash.Set.(is_empty @@ inter protocols o.protocols) )
      &&
      match (desc, o.desc) with
      | (Baker {delegates = d1; _}, Baker {delegates = d2; _})
      | (Endorser {delegates = d1; _}, Endorser {delegates = d2; _}) ->
          (o.state)#state = Lwt_process.Running
          && not Public_key_hash.Set.(is_empty @@ inter d1 d2)
      | (_, _) ->
          false)
    (DaemonSet.elements daemons)

let check_no_overlap ?(fail = false) id desc protocols all_active_protocols =
  match
    overlap_delegates
      id
      desc
      protocols
      all_active_protocols
      global_state.daemons
  with
  | Some o ->
      if fail then
        failwith
          "Conflicting %a with already running ID %d"
          pp_kind
          (kind_of_desc o.desc)
          o.id
      else
        lwt_warn
          "Register %a (%d): Conflicting %a with already running ID %d"
          pp_kind
          (kind_of_desc desc)
          id
          pp_kind
          (kind_of_desc o.desc)
          o.id
        >>= return
  | None ->
      return_unit

let find_daemon_by_id id =
  match id with None -> None | Some id -> get_opt ~id

let find_daemon_by_pid pid =
  match pid with
  | None ->
      None
  | Some pid ->
      List.find_opt
        (fun d -> (d.state)#pid = pid)
        (DaemonSet.elements global_state.daemons)

let register_daemon ?fail ?id ?pid ~protocols ~all_active_protocols desc =
  let protocols = Protocol_hash.Set.of_list protocols in
  let last_ping = Systime_os.now () in
  with_state_lock
  @@ fun () ->
  ( match (find_daemon_by_id id, find_daemon_by_pid pid) with
  | (None, None) ->
      (* External daemon *)
      fresh_id ()
      >>= fun id ->
      (match pid with None -> failwith "Unspecified pid" | Some i -> return i)
      >>=? fun pid ->
      let state = new daemon_state pid in
      return
        {
          id;
          desc;
          protocols;
          all_active_protocols;
          spawned = false;
          last_ping;
          state;
        }
  | (Some d, _) | (None, Some d) ->
      (* Spawned daemon *)
      let state =
        match pid with
        | Some pid when pid <> (d.state)#pid ->
            new daemon_state pid
        | _ ->
            d.state
      in
      return {d with desc; protocols; state} )
  >>=? fun daemon ->
  check_no_overlap
    ?fail
    daemon.id
    daemon.desc
    daemon.protocols
    daemon.all_active_protocols
  >>=? fun () ->
  global_state.daemons <- DaemonSet.replace daemon global_state.daemons ;
  write_state () >>=? fun () -> return (daemon.id, daemon.state)

let unregister_daemon ?(fail = false) ~id =
  with_state_lock
  @@ fun () ->
  match get_opt ~id with
  | None when fail ->
      failwith "Daemon not registered"
  | None ->
      return_unit
  | Some d ->
      global_state.daemons <- DaemonSet.remove d global_state.daemons ;
      write_state ()

let list_daemons ?kind ?(running = false) () =
  match (kind, running) with
  | (None, false) ->
      global_state.daemons
  | (_, _) ->
      DaemonSet.filter
        (fun d ->
          (kind = None || kind = Some (kind_of_desc d.desc))
          && ((not running) || (d.state)#state = Lwt_process.Running))
        global_state.daemons

let ping d =
  with_state_lock
  @@ fun () ->
  global_state.daemons <-
    DaemonSet.replace
      {d with last_ping = Systime_os.now ()}
      global_state.daemons ;
  (* Don't write state to file as this function is called
     frequently *)
  return_unit

let daemon_ping ~id =
  match get_opt ~id with None -> return_unit | Some d -> ping d

let daemon_ping_pid ~pid =
  match
    List.find_opt
      (fun d -> (d.state)#pid = pid)
      (DaemonSet.elements global_state.daemons)
  with
  | None ->
      return_unit
  | Some d ->
      ping d

let clean_daemons () =
  with_state_lock
  @@ fun () ->
  let daemons =
    DaemonSet.filter (fun d -> is_alive (d.state)#pid) global_state.daemons
  in
  global_state.daemons <- daemons ;
  write_state ()

let kill_daemon ?(signal = Sys.sigterm) id =
  with_state_lock
  @@ fun () ->
  let d = get ~id in
  try (d.state)#kill signal ; return_unit with
  | Unix.Unix_error (Unix.ESRCH, _, _) ->
      return_unit
  | e ->
      fail (Exn e)

let log_filename ~kind ~id =
  get_ctxt ()
  >>=? fun ctxt ->
  let log_file = Format.asprintf "%a.%d.log" pp_kind kind id in
  return (Filename.concat ctxt#base_dir log_file)

let arg_of_opt name pp = function None -> [] | Some x -> [name; pp x]

let cmd_of_desc = function
  | Baker
      { minimal_fees;
        minimal_nanotez_per_gas_unit;
        minimal_nanotez_per_byte;
        await_endorsements;
        max_priority;
        delegates } ->
      get_ctxt ()
      >>=? fun ctxt ->
      ( "dune-baker-all",
        ["run"; "with"; "local"; "node"; ctxt#base_dir]
        @ ( Public_key_hash.Set.elements delegates
          |> List.map Public_key_hash.to_b58check )
        @ arg_of_opt "--minimal-fees" (fun s -> s) minimal_fees
        @ arg_of_opt
            "--minimal-nanodun-per-gas-unit"
            Z.to_string
            minimal_nanotez_per_gas_unit
        @ arg_of_opt
            "--minimal-nanodun-per-byte"
            Z.to_string
            minimal_nanotez_per_byte
        @ ( if await_endorsements = Some true then []
          else ["--no-waiting-for-late-endorsements"] )
        @ arg_of_opt "--max-priority" string_of_int max_priority )
      |> return
  | Endorser {delay; delegates} ->
      ( "dune-endorser-all",
        ["run"]
        @ ( Public_key_hash.Set.elements delegates
          |> List.map Public_key_hash.to_b58check )
        @ arg_of_opt "--endorsement-delay" string_of_int delay )
      |> return
  | Accuser {preserved_levels} ->
      ( "dune-accuser-all",
        ["run"]
        @ arg_of_opt "--preserved-levels" string_of_int preserved_levels )
      |> return

let common_args ?(protocols = []) () =
  let global_args =
    match global_state.rpc_config with
    | None ->
        []
    | Some {host; port; tls} ->
        ["--addr"; host; "--port"; string_of_int port]
        @ if tls then ["--tls"] else []
  in
  let global_args =
    match global_state.client_dir with
    | None ->
        global_args
    | Some d ->
        "--base-dir" :: d :: global_args
  in
  let proto_args =
    List.map (fun p -> ["--protocol"; Protocol_hash.to_b58check p]) protocols
    |> List.flatten
  in
  (global_args, proto_args)

let start_daemon ?id ?(protocols = []) desc =
  (match id with None -> fresh_id () | Some id -> Lwt.return id)
  >>= fun id ->
  let kind = kind_of_desc desc in
  log_filename ~kind ~id
  >>=? fun log_file ->
  open_log_file (Systime_os.now ()) log_file
  >>=? fun fdlog ->
  let self_dir = Filename.dirname (full_path_self ()) in
  cmd_of_desc desc
  >>=? fun (bin, cmd_args) ->
  let (global_args, proto_args) = common_args ~protocols () in
  let args = global_args @ cmd_args @ proto_args in
  let bin = Filename.concat self_dir bin in
  (* binary must be in same location as node *)
  Lwt_unix.file_exists bin
  >>= (function
        | true ->
            return_unit
        | false ->
            failwith "Daemon binary %S does not exist on system" bin)
  >>=? fun () ->
  let protocols = Protocol_hash.Set.of_list protocols in
  let all_active_protocols = Protocol_hash.Set.is_empty protocols in
  let cmd = bin :: args in
  with_state_lock
  @@ fun () ->
  check_no_overlap ~fail:true id desc protocols all_active_protocols
  >>=? fun () ->
  log_notice
    "Starting %a daemon %d (%s)"
    pp_kind
    kind
    id
    (String.concat " " cmd) ;
  let cmd = Array.of_list cmd in
  let stdout = `FD_copy fdlog in
  (* order important *)
  let stderr = `FD_move fdlog in
  let proc = Lwt_process.open_process_none ~stdout ~stderr (bin, cmd) in
  let state = (proc :> daemon_state) in
  (* Create daemon structure *)
  let daemon =
    match get_opt ~id with
    | None ->
        {
          id;
          desc;
          protocols;
          all_active_protocols;
          spawned = true;
          last_ping = Time.System.epoch;
          state;
        }
    | Some daemon ->
        {daemon with all_active_protocols; spawned = true; state}
  in
  global_state.daemons <- DaemonSet.replace daemon global_state.daemons ;
  write_state () >>=? fun () -> return (id, state)

let restart_daemon d =
  match ((d.state)#state, d.spawned) with
  | (Lwt_process.Running, _) ->
      failwith "This daemon is already running"
  | (_, false) ->
      failwith "This daemon was not spawned, cannot restart it"
  | _ ->
      let protocols =
        if d.all_active_protocols then None
        else Some (Protocol_hash.Set.elements d.protocols)
      in
      start_daemon ~id:d.id ?protocols d.desc

let restart_daemon_no_fail d =
  restart_daemon d
  >>= function
  | Error _ ->
      lwt_warn
        "Could not restart %a with id %d"
        pp_kind
        (kind_of_desc d.desc)
        d.id
      >>= fun () -> Lwt.return_none
  | Ok res ->
      Lwt.return_some res

let restart_daemon_by_id ?(fail = true) ~id =
  if fail then restart_daemon (get ~id) >>=? return_some
  else restart_daemon_no_fail (get ~id) >>= return

let resume_all_stopped_daemons () =
  Lwt_list.filter_map_p
    restart_daemon_no_fail
    (DaemonSet.elements global_state.daemons)

let get_delegate x =
  match Public_key_hash.of_b58check_opt x with
  | Some pkh ->
      return pkh
  | None -> (
    match String.Map.find_opt x global_state.accounts with
    | Some pkh ->
        return pkh
    | None ->
        failwith "Unknown account alias %s" x )

let get_delegates delegates =
  if delegates = [] then failwith "Empty list of delegates"
  else map_p get_delegate delegates >>|? Public_key_hash.Set.of_list

let start_baker ?protocols ?minimal_fees ?minimal_nanotez_per_gas_unit
    ?minimal_nanotez_per_byte ?await_endorsements ?max_priority delegates =
  get_delegates delegates
  >>=? fun delegates ->
  start_daemon ?protocols
  @@ Baker
       {
         minimal_fees;
         minimal_nanotez_per_gas_unit;
         minimal_nanotez_per_byte;
         await_endorsements;
         max_priority;
         delegates;
       }

let start_endorser ?protocols ?delay delegates =
  get_delegates delegates
  >>=? fun delegates -> start_daemon ?protocols @@ Endorser {delay; delegates}

let start_accuser ?protocols ?preserved_levels () =
  start_daemon ?protocols @@ Accuser {preserved_levels}

let tail_log ?size id =
  let d = get ~id in
  log_filename ~kind:(kind_of_desc d.desc) ~id:d.id
  >>=? fun f -> tail_log_file ?size f
