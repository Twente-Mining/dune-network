(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Node_manager_state
open Node_manager_encodings

let init ~base_dir ~rpc_config =
  global_state.ctxt <-
    Some (new Node_manager_file.manager ~base_dir "manager_state") ;
  with_full_lock (fun ctxt ->
      global_state.rpc_config <- rpc_config ;
      ctxt#load ~default:(mk_empty_state ()) state_encoding
      >>=? fun st ->
      global_state.client_dir <- st.client_dir ;
      global_state.accounts <- st.accounts ;
      global_state.daemons <- st.daemons ;
      reinit_max_id () >>= fun () -> return_unit)
  >>=? fun () ->
  (* Resart all registered daemons *)
  Node_manager_daemons.resume_all_stopped_daemons () >>= fun _ -> return_unit

let write_state () = Node_manager_encodings.write_state ()
