(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Node_manager_utils
module Public_key_hash = Signature.Public_key_hash

include Internal_event.Legacy_logging.Make (struct
  let name = "node.manager"
end)

class daemon_state pid =
  object
    method pid = pid

    method state = get_status pid

    method kill (_signal : int) : unit =
      Format.ksprintf
        Pervasives.failwith
        "Cannot kill %d, a non spawned process"
        pid
  end

type daemon = {
  id : int;
  desc : daemon_desc;
  protocols : Protocol_hash.Set.t;
  all_active_protocols : bool;
  spawned : bool;
  last_ping : Time.System.t;
  state : daemon_state;
}

and daemon_desc =
  | Baker of {
      delegates : Public_key_hash.Set.t;
      minimal_fees : string option;
      minimal_nanotez_per_gas_unit : Z.t option;
      minimal_nanotez_per_byte : Z.t option;
      await_endorsements : bool option;
      max_priority : int option;
    }
  | Endorser of {delegates : Public_key_hash.Set.t; delay : int option}
  | Accuser of {preserved_levels : int option}

module DaemonSet : sig
  type t

  val empty : t

  val remove : daemon -> t -> t

  val replace : daemon -> t -> t

  val find : id:int -> t -> daemon

  val find_opt : id:int -> t -> daemon option

  val of_list : daemon list -> t

  val elements : t -> daemon list

  val max_elt_opt : t -> daemon option

  val exists : (daemon -> bool) -> t -> bool

  val filter : (daemon -> bool) -> t -> t
end = struct
  include Set.Make (struct
    type t = daemon

    let compare d1 d2 = Pervasives.compare d1.id d2.id
  end)

  let dummy_daemon id =
    {
      id;
      desc = Accuser {preserved_levels = None};
      protocols = Protocol_hash.Set.empty;
      all_active_protocols = false;
      spawned = false;
      last_ping = Time.System.epoch;
      state = new daemon_state (-1);
    }

  let replace d set = add d (remove d set)

  let find ~id set = find (dummy_daemon id) set

  let find_opt ~id set = try Some (find ~id set) with Not_found -> None
end

type t = {
  mutable ctxt : Node_manager_file.t option;
  mutable client_dir : string option;
  mutable rpc_config : rpc_config option;
  mutable accounts : Public_key_hash.t String.Map.t;
  mutable daemons : DaemonSet.t;
}

and rpc_config = {host : string; port : int; tls : bool}

type kind = Baker_kind | Endorser_kind | Accuser_kind

let max_id = ref (-1)

let max_id_mutex = Lwt_mutex.create ()

let state_mutex = Lwt_mutex.create ()

let with_state_lock f = Lwt_mutex.with_lock state_mutex f

let fresh_id () =
  Lwt_mutex.with_lock max_id_mutex
  @@ fun () -> incr max_id ; Lwt.return !max_id

let mk_empty_state () =
  {
    ctxt = None;
    rpc_config = None;
    client_dir = None;
    accounts = String.Map.empty;
    daemons = DaemonSet.empty;
  }

let global_state = mk_empty_state ()

let get_ctxt () =
  match global_state.ctxt with
  | None ->
      failwith "Uninitialized node manager state"
  | Some ctxt ->
      return ctxt

let with_file_lock f =
  get_ctxt () >>=? fun ctxt -> ctxt#with_lock (fun () -> f ctxt)

let with_full_lock f = with_state_lock @@ fun () -> with_file_lock f

let reinit_max_id () =
  Lwt_mutex.with_lock max_id_mutex
  @@ fun () ->
  ( match DaemonSet.max_elt_opt global_state.daemons with
  | Some d ->
      max_id := d.id
  | None ->
      () ) ;
  Lwt.return_unit

let write_state state_encoding =
  with_file_lock @@ fun ctxt -> ctxt#write global_state state_encoding
