(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open RPC_context
open Node_manager_directory

module Accounts = struct
  let get_accounts ctxt = make_call S.Accounts.accounts ctxt () () ()

  let get_account ctxt alias =
    make_call S.Accounts.account ctxt ((), alias) () ()

  let register_accounts ctxt accounts =
    make_call S.Accounts.register_accounts ctxt () () accounts

  let register_client_dir ctxt d =
    make_call S.Accounts.register_client_dir ctxt () () d
end

module Daemons = struct
  let list ?kind ?(running = false) (ctxt : #simple) =
    make_call
      S.Daemons.list
      ctxt
      ()
      (object
         method kind = kind

         method running = running
      end)
      ()

  let register ?(fail = false) ?pid ?id (ctxt : #simple) ~desc ~protocols
      ~all_active_protocols =
    make_call
      S.Daemons.register
      ctxt
      ()
      fail
      (id, desc, protocols, all_active_protocols, pid)

  let ping (ctxt : #simple) ~id = make_call S.Daemons.ping ctxt ((), id) () ()

  let ping_pid (ctxt : #simple) ~pid =
    make_call S.Daemons.ping_pid ctxt ((), pid) () ()

  let unregister (ctxt : #simple) ?(fail = false) ~pid =
    make_call S.Daemons.unregister ctxt ((), pid) fail ()

  let clean (ctxt : #simple) = make_call S.Daemons.clean ctxt () () ()

  let kill ?signal (ctxt : #simple) ~id =
    make_call S.Daemons.kill ctxt ((), id) signal ()

  let log ?size (ctxt : #simple) ~id =
    make_call S.Daemons.log ctxt ((), id) size ()

  let start_baker (ctxt : #simple) ?protocols ?minimal_fees
      ?minimal_nanotez_per_gas_unit ?minimal_nanotez_per_byte
      ?await_endorsements ?max_priority delegates =
    make_call
      S.Daemons.start_baker
      ctxt
      ()
      ()
      ( protocols,
        minimal_fees,
        minimal_nanotez_per_gas_unit,
        minimal_nanotez_per_byte,
        await_endorsements,
        max_priority,
        delegates )

  let start_endorser (ctxt : #simple) ?protocols ?delay delegates =
    make_call S.Daemons.start_endorser ctxt () () (protocols, delay, delegates)

  let start_accuser ?protocols ?preserved_levels (ctxt : #simple) =
    make_call S.Daemons.start_accuser ctxt () () (protocols, preserved_levels)
end
