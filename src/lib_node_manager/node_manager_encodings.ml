(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Data_encoding
open Node_manager_state
module Public_key_hash = Signature.Public_key_hash

let accounts_list_encoding =
  list (obj2 (req "alias" string) (req "pkh" Public_key_hash.encoding))

let kinds =
  [ ("baker", Baker_kind);
    ("endorser", Endorser_kind);
    ("accuser", Accuser_kind) ]

let string_of_kind = function
  | Baker_kind ->
      "baker"
  | Endorser_kind ->
      "endorser"
  | Accuser_kind ->
      "accuser"

let kind_of_string s =
  match List.assoc_opt s kinds with
  | Some k ->
      Ok k
  | None ->
      Error
        (Format.sprintf
           "Bad daemon kind %S. Possible values are %s"
           s
           (String.concat ", " (List.map fst kinds)))

let kind_of_desc = function
  | Baker _ ->
      Baker_kind
  | Endorser _ ->
      Endorser_kind
  | Accuser _ ->
      Accuser_kind

let kind_encoding = string_enum kinds

let accounts_encoding =
  conv
    String.Map.bindings
    (fun l ->
      List.fold_left
        (fun acc (alias, pkh) -> String.Map.add alias pkh acc)
        String.Map.empty
        l)
    accounts_list_encoding

let public_key_hash_set_encoding =
  conv
    Public_key_hash.Set.elements
    Public_key_hash.Set.of_list
    (list Public_key_hash.encoding)

let protocol_hash_set_encoding =
  conv
    Protocol_hash.Set.elements
    Protocol_hash.Set.of_list
    (list Protocol_hash.encoding)

let daemon_desc_encoding =
  union
    [ case
        (Tag 0)
        ~title:"daemon.baker"
        (obj7
           (req "kind" (constant "baker"))
           (req "delegates" public_key_hash_set_encoding)
           (opt "minimal_fees" string)
           (opt "minimal_nanotez_per_gas_unit" z)
           (opt "minimal_nanotez_per_byte" z)
           (opt "await_endorsements" bool)
           (opt "max_priority" int31))
        (function
          | Baker
              { delegates;
                minimal_fees;
                minimal_nanotez_per_gas_unit;
                minimal_nanotez_per_byte;
                await_endorsements;
                max_priority } ->
              Some
                ( (),
                  delegates,
                  minimal_fees,
                  minimal_nanotez_per_gas_unit,
                  minimal_nanotez_per_byte,
                  await_endorsements,
                  max_priority )
          | _ ->
              None)
        (fun ( (),
               delegates,
               minimal_fees,
               minimal_nanotez_per_gas_unit,
               minimal_nanotez_per_byte,
               await_endorsements,
               max_priority ) ->
          Baker
            {
              delegates;
              minimal_fees;
              minimal_nanotez_per_gas_unit;
              minimal_nanotez_per_byte;
              await_endorsements;
              max_priority;
            });
      case
        (Tag 1)
        ~title:"daemon.endorser"
        (obj3
           (req "kind" (constant "endorser"))
           (req "delegates" public_key_hash_set_encoding)
           (opt "delay" int31))
        (function
          | Endorser {delegates; delay} ->
              Some ((), delegates, delay)
          | _ ->
              None)
        (fun ((), delegates, delay) -> Endorser {delegates; delay});
      case
        (Tag 2)
        ~title:"daemon.accuser"
        (obj2 (req "kind" (constant "accuser")) (opt "preserved_levels" int31))
        (function
          | Accuser {preserved_levels} ->
              Some ((), preserved_levels)
          | _ ->
              None)
        (fun ((), preserved_levels) -> Accuser {preserved_levels}) ]

let process_state_encoding =
  union
    [ case
        (Tag 0)
        ~title:"process.running"
        (obj1 (req "state" (constant "running")))
        (function Lwt_process.Running -> Some () | _ -> None)
        (fun () -> Lwt_process.Running);
      case
        (Tag 1)
        ~title:"process.exited"
        (obj2 (req "state" (constant "exited")) (dft "code" int31 (-1)))
        (function
          | Lwt_process.Exited (Unix.WEXITED s) -> Some ((), s) | _ -> None)
        (fun ((), s) -> Lwt_process.Exited (Unix.WEXITED s));
      case
        (Tag 2)
        ~title:"process.stopped"
        (obj2 (req "state" (constant "stopped")) (req "code" int31))
        (function
          | Lwt_process.Exited (Unix.WSTOPPED s) -> Some ((), s) | _ -> None)
        (fun ((), s) -> Lwt_process.Exited (Unix.WSTOPPED s));
      case
        (Tag 3)
        ~title:"process.signaled"
        (obj2 (req "state" (constant "signaled")) (req "code" int31))
        (function
          | Lwt_process.Exited (Unix.WSIGNALED s) -> Some ((), s) | _ -> None)
        (fun ((), s) -> Lwt_process.Exited (Unix.WSIGNALED s)) ]

let daemon_state_encoding : daemon_state Data_encoding.encoding =
  conv
    (fun p -> (p#pid, p#state))
    (fun (pid, _state) -> new daemon_state pid)
    (merge_objs (obj1 (req "pid" int31)) process_state_encoding)

let daemon_id_state_encoding =
  merge_objs (obj1 (req "id" int31)) daemon_state_encoding

let daemon_encoding =
  conv
    (fun {id; desc; protocols; all_active_protocols; spawned; last_ping; state} ->
      ((id, desc), (protocols, all_active_protocols, spawned, last_ping, state)))
    (fun ( (id, desc),
           (protocols, all_active_protocols, spawned, last_ping, state) ) ->
      {id; desc; protocols; all_active_protocols; spawned; last_ping; state})
    (merge_objs
       (merge_objs (obj1 (req "id" int31)) daemon_desc_encoding)
       (obj5
          (req "protocols" protocol_hash_set_encoding)
          (req "all_active_protocols" bool)
          (req "spawned" bool)
          (req "last_ping" Time.System.encoding)
          (req "state" daemon_state_encoding)))

let daemon_set_encoding =
  conv DaemonSet.elements DaemonSet.of_list (list daemon_encoding)

let spawned_daemons_encoding =
  conv
    (fun s -> DaemonSet.elements s |> List.filter (fun d -> d.spawned))
    DaemonSet.of_list
    (list daemon_encoding)

let state_encoding =
  conv
    (fun {client_dir; accounts; daemons; _} -> (client_dir, accounts, daemons))
    (fun (client_dir, accounts, daemons) ->
      {(mk_empty_state ()) with client_dir; accounts; daemons})
    (obj3
       (opt "client_dir" string)
       (dft "accounts" accounts_encoding String.Map.empty)
       (dft "daemons" spawned_daemons_encoding DaemonSet.empty))

let write_state () = Node_manager_state.write_state state_encoding
