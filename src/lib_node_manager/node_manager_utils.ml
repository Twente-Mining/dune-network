(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let is_alive pid =
  try Unix.kill pid 0 ; (* alive *)
                        true with
  | Unix.Unix_error (Unix.ESRCH, _, _) ->
      (* dead *)
      false
  | Unix.Unix_error (Unix.EPERM, _, _) ->
      true

let get_status pid =
  if is_alive pid then Lwt_process.Running
  else Lwt_process.Exited (Unix.WEXITED (-1))

let get_full_path p = if Filename.is_relative p then Unix.getcwd () ^ p else p

let full_path_self () = get_full_path Sys.argv.(0)

let rec wait_dead pid amount =
  (* Format.eprintf "wait dead %d : %f@." pid amount; *)
  let step = 0.3 in
  if amount <= 0. then return_false
  else if is_alive pid then
    Lwt_unix.sleep step >>= fun () -> wait_dead pid (amount -. step)
  else return_true

let open_log_file date f =
  Lwt.catch
    (fun () ->
      let date = Time.System.to_notation date in
      Lwt_utils_unix.create_dir ~perm:0o700 (Filename.dirname f)
      >>= fun () ->
      Lwt_unix.file_exists f
      >>= fun exists ->
      ( if exists then Lwt_unix.rename f (Printf.sprintf "%s.saved-%s" f date)
      else Lwt.return () )
      >>= fun () ->
      Lwt_unix.(
        openfile f [O_CREAT; O_WRONLY; O_APPEND] 0o600 >|= unix_file_descr)
      >>= return)
    (fun e -> failwith "Could not open file %s (%s)" f (Printexc.to_string e))

let tail_log_file ?(size = 8192) f =
  Lwt.catch
    (fun () ->
      Lwt_unix.file_exists f
      >>= function
      | false ->
          failwith "File %s does not exist" f
      | true ->
          Lwt_unix.openfile f [O_RDONLY] 0o600
          >>= fun fd ->
          Lwt_unix.lseek fd 0 Unix.SEEK_END
          >>= fun file_size ->
          let size = min file_size size in
          Lwt_unix.lseek fd (-size) Unix.SEEK_END
          >>= fun _ ->
          let buffer = Bytes.create size in
          Lwt_unix.read fd buffer 0 size
          >>= fun r_size ->
          let buffer =
            if size = r_size then buffer else Bytes.sub buffer 0 r_size
          in
          Lwt_unix.close fd >>= fun () -> Bytes.to_string buffer |> return)
    (fun e ->
      failwith "Could not read log file %s (%s)" f (Printexc.to_string e))
