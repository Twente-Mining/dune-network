(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* Export initializer, services and directory to outside *)

type daemon_state = Node_manager_state.daemon_state

type daemon = Node_manager_state.daemon = {
  id : int;
  desc : daemon_desc;
  protocols : Protocol_hash.Set.t;
  all_active_protocols : bool;
  spawned : bool;
  last_ping : Time.System.t;
  state : daemon_state;
}

and daemon_desc = Node_manager_state.daemon_desc =
  | Baker of {
      delegates : Signature.Public_key_hash.Set.t;
      minimal_fees : string option;
      minimal_nanotez_per_gas_unit : Z.t option;
      minimal_nanotez_per_byte : Z.t option;
      await_endorsements : bool option;
      max_priority : int option;
    }
  | Endorser of {
      delegates : Signature.Public_key_hash.Set.t;
      delay : int option;
    }
  | Accuser of {preserved_levels : int option}

type rpc_config = Node_manager_state.rpc_config = {
  host : string;
  port : int;
  tls : bool;
}

include Node_manager_init
module Services = Node_manager_services

let build_rpc_directory () = Node_manager_directory.build_rpc_directory ()
