(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2020 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Node_manager_encodings
module Public_key_hash = Signature.Public_key_hash

module S = struct
  let root_path = RPC_path.(root / "admin")

  module Accounts = struct
    let path = RPC_path.(root_path / "accounts")

    let accounts =
      RPC_service.get_service
        ~description:"Returns registered accounts with their alias."
        ~query:RPC_query.empty
        ~output:accounts_encoding
        path

    let account =
      RPC_service.get_service
        ~description:"Returns a public key hash for a registered alias."
        ~query:RPC_query.empty
        ~output:Signature.Public_key_hash.encoding
        RPC_path.(root_path / "account" /: RPC_arg.(like string "alias"))

    let register_accounts =
      RPC_service.post_service
        ~description:"Register accounts with their aliases."
        ~query:RPC_query.empty
        ~input:accounts_list_encoding
        ~output:Data_encoding.unit
        RPC_path.(path / "register")

    let register_client_dir =
      RPC_service.post_service
        ~description:"Register local client directory for daemons."
        ~query:RPC_query.empty
        ~input:Data_encoding.string
        ~output:Data_encoding.unit
        RPC_path.(path / "register_client_dir")
  end

  module Daemons = struct
    let path = RPC_path.(root_path / "daemons")

    let kind_arg =
      RPC_arg.make
        ~name:"daemon_kind"
        ~descr:"A kind of daemon (baker, endorser, accuser)"
        ~destruct:kind_of_string
        ~construct:string_of_kind
        ()

    let list_query =
      let open RPC_query in
      query (fun kind running ->
          object
            method kind = kind

            method running = running
          end)
      |+ opt_field "kind" kind_arg (fun t -> t#kind)
      |+ flag "running" (fun t -> t#running)
      |> seal

    let list =
      RPC_service.get_service
        ~description:"Returns the running daemons on this node."
        ~query:list_query
        ~output:daemon_set_encoding
        path

    let fail_query =
      let open RPC_query in
      query (fun fail -> fail) |+ flag "fail" (fun t -> t) |> seal

    let register =
      RPC_service.post_service
        ~description:"Register a daemon on this node."
        ~query:fail_query
        ~input:
          Data_encoding.(
            obj5
              (opt "id" int31)
              (req "desc" daemon_desc_encoding)
              (req "protocols" (list Protocol_hash.encoding))
              (req "all_active_protocols" bool)
              (opt "pid" int31))
        ~output:daemon_id_state_encoding
        RPC_path.(path / "register")

    let clean =
      RPC_service.post_service
        ~description:"Clean dead daemons from running list."
        ~query:RPC_query.empty
        ~input:Data_encoding.unit
        ~output:Data_encoding.unit
        RPC_path.(path / "clean")

    let path_id = RPC_path.(root_path / "daemon" /: RPC_arg.(like int "id"))

    let ping =
      RPC_service.post_service
        ~description:"Update a running daemon on this node."
        ~query:RPC_query.empty
        ~input:Data_encoding.unit
        ~output:Data_encoding.unit
        RPC_path.(path_id / "ping")

    let get =
      RPC_service.get_service
        ~description:"Returns information about a daemon on this node."
        ~query:RPC_query.empty
        ~output:daemon_encoding
        path_id

    let ping_pid =
      RPC_service.post_service
        ~description:"Update a running daemon on this node."
        ~query:RPC_query.empty
        ~input:Data_encoding.unit
        ~output:Data_encoding.unit
        RPC_path.(path / "ping_pid" /: RPC_arg.(like int "pid"))

    let unregister =
      RPC_service.delete_service
        ~description:"Remove a daemon on this node."
        ~query:fail_query
        ~output:Data_encoding.unit
        RPC_path.(path_id / "remove")

    let start =
      RPC_service.post_service
        ~description:"Start a registered daemon on this node."
        ~query:RPC_query.empty
        ~input:Data_encoding.unit
        ~output:daemon_id_state_encoding
        RPC_path.(path_id / "start")

    let signal_rpc_arg =
      RPC_arg.make
        ~name:"Unix_signal"
        ~descr:"A unix signal"
        ~destruct:(fun s ->
          match String.lowercase_ascii s with
          | "sigabrt" | "abrt" ->
              Ok Sys.sigabrt
          | "sigalrm" | "alrm" ->
              Ok Sys.sigalrm
          | "sigfpe" | "fpe" ->
              Ok Sys.sigfpe
          | "sighup" | "hup" ->
              Ok Sys.sighup
          | "sigill" | "ill" ->
              Ok Sys.sigill
          | "sigint" | "int" ->
              Ok Sys.sigint
          | "sigkill" | "kill" ->
              Ok Sys.sigkill
          | "sigpipe" | "pipe" ->
              Ok Sys.sigpipe
          | "sigquit" | "quit" ->
              Ok Sys.sigquit
          | "sigsegv" | "segv" ->
              Ok Sys.sigsegv
          | "sigterm" | "term" ->
              Ok Sys.sigterm
          | "sigusr1" | "usr1" ->
              Ok Sys.sigusr1
          | "sigusr2" | "usr2" ->
              Ok Sys.sigusr2
          | "sigchld" | "chld" ->
              Ok Sys.sigchld
          | "sigcont" | "cont" ->
              Ok Sys.sigcont
          | "sigstop" | "stop" ->
              Ok Sys.sigstop
          | "sigtstp" | "tstp" ->
              Ok Sys.sigtstp
          | "sigttin" | "ttin" ->
              Ok Sys.sigttin
          | "sigttou" | "ttou" ->
              Ok Sys.sigttou
          | "sigvtalrm" | "vtalrm" ->
              Ok Sys.sigvtalrm
          | "sigprof" | "prof" ->
              Ok Sys.sigprof
          | "sigbus" | "bus" ->
              Ok Sys.sigbus
          | "sigpoll" | "poll" ->
              Ok Sys.sigpoll
          | "sigsys" | "sys" ->
              Ok Sys.sigsys
          | "sigtrap" | "trap" ->
              Ok Sys.sigtrap
          | "sigurg" | "urg" ->
              Ok Sys.sigurg
          | "sigxcpu" | "xcpu" ->
              Ok Sys.sigxcpu
          | "sigxfsz" | "xfsz" ->
              Ok Sys.sigxfsz
          | _ ->
              Error (Format.sprintf "Unknown signal: %S" s))
        ~construct:(fun s ->
          List.assoc
            s
            [ (Sys.sigabrt, "sigabrt");
              (Sys.sigalrm, "sigalrm");
              (Sys.sigfpe, "sigfpe");
              (Sys.sighup, "sighup");
              (Sys.sigill, "sigill");
              (Sys.sigint, "sigint");
              (Sys.sigkill, "sigkill");
              (Sys.sigpipe, "sigpipe");
              (Sys.sigquit, "sigquit");
              (Sys.sigsegv, "sigsegv");
              (Sys.sigterm, "sigterm");
              (Sys.sigusr1, "sigusr1");
              (Sys.sigusr2, "sigusr2");
              (Sys.sigchld, "sigchld");
              (Sys.sigcont, "sigcont");
              (Sys.sigstop, "sigstop");
              (Sys.sigtstp, "sigtstp");
              (Sys.sigttin, "sigttin");
              (Sys.sigttou, "sigttou");
              (Sys.sigvtalrm, "sigvtalrm");
              (Sys.sigprof, "sigprof");
              (Sys.sigbus, "sigbus");
              (Sys.sigpoll, "sigpoll");
              (Sys.sigsys, "sigsys");
              (Sys.sigtrap, "sigtrap");
              (Sys.sigurg, "sigurg");
              (Sys.sigxcpu, "sigxcpu");
              (Sys.sigxfsz, "sigxfsz") ])
        ()

    let signal_query =
      let open RPC_query in
      query (function signal -> signal)
      |+ opt_field "signal" signal_rpc_arg (fun t -> t)
      |> seal

    let size_query =
      let open RPC_query in
      query (function s -> s)
      |+ opt_field
           "size"
           RPC_arg.(like int "size" ~descr:"requested size in bytes")
           (fun t -> t)
      |> seal

    let kill =
      RPC_service.post_service
        ~description:"Kill a running daemon."
        ~query:signal_query
        ~input:Data_encoding.unit
        ~output:Data_encoding.unit
        RPC_path.(path_id / "kill")

    let start_baker =
      RPC_service.post_service
        ~description:"Start a baker on this node."
        ~query:RPC_query.empty
        ~input:
          Data_encoding.(
            obj7
              (opt "protocols" (list Protocol_hash.encoding))
              (opt "minimal_fees" string)
              (opt "minimal_nanotez_per_gas_unit" z)
              (opt "minimal_nanotez_per_byte" z)
              (opt "await_endorsements" bool)
              (opt "max_priority" int31)
              (req "delegates" (list string)))
        ~output:daemon_id_state_encoding
        RPC_path.(path / "start_baker")

    let log =
      RPC_service.get_service
        ~description:"Get the tail of the log for a daemon."
        ~query:size_query
        ~output:Data_encoding.string
        RPC_path.(path_id / "log")

    let start_endorser =
      RPC_service.post_service
        ~description:"Start a endorser on this node."
        ~query:RPC_query.empty
        ~input:
          Data_encoding.(
            obj3
              (opt "protocols" (list Protocol_hash.encoding))
              (opt "delay" int31)
              (req "delegates" (list string)))
        ~output:daemon_id_state_encoding
        RPC_path.(path / "start_endorser")

    let start_accuser =
      RPC_service.post_service
        ~description:"Start an accuser on this node."
        ~query:RPC_query.empty
        ~input:
          Data_encoding.(
            obj2
              (opt "protocols" (list Protocol_hash.encoding))
              (opt "preserved_levels" int31))
        ~output:daemon_id_state_encoding
        RPC_path.(path / "start_accuser")
  end
end

module Accounts = struct
  open Node_manager_accounts

  let accounts () () = get_accounts () |> return

  let account name () () = get_account name |> return

  let register_accounts () accounts = register_accounts accounts

  let register_client_dir () d = register_client_dir d
end

module Daemons = struct
  open Node_manager_daemons

  let list t () = list_daemons ?kind:t#kind ~running:t#running () |> return

  let register fail (id, desc, protocols, all_active_protocols, pid) =
    register_daemon ~fail ?id ~protocols ~all_active_protocols ?pid desc

  let ping id () () = daemon_ping ~id

  let ping_pid pid () () = daemon_ping_pid ~pid

  let unregister id fail () = unregister_daemon ~fail ~id

  let start id () () =
    restart_daemon_by_id ~fail:true ~id
    >>|? function None -> assert false | Some res -> res

  let log id size () = tail_log ?size id

  let clean () () = clean_daemons ()

  let kill id signal () = kill_daemon ?signal id

  let start_baker ()
      ( protocols,
        minimal_fees,
        minimal_nanotez_per_gas_unit,
        minimal_nanotez_per_byte,
        await_endorsements,
        max_priority,
        delegates ) =
    start_baker
      ?protocols
      ?minimal_fees
      ?minimal_nanotez_per_gas_unit
      ?minimal_nanotez_per_byte
      ?await_endorsements
      ?max_priority
      delegates

  let start_endorser () (protocols, delay, delegates) =
    start_endorser ?protocols ?delay delegates

  let start_accuser () (protocols, preserved_levels) =
    start_accuser ?protocols ?preserved_levels ()

  let get id () () = get ~id |> return
end

let build_rpc_directory () =
  let dir : unit RPC_directory.t ref = ref RPC_directory.empty in
  let register0 s f =
    dir := RPC_directory.register !dir s (fun () p q -> f p q)
  in
  let register1 s f =
    dir := RPC_directory.register !dir s (fun ((), a) p q -> f a p q)
  in
  register0 S.Accounts.accounts Accounts.accounts ;
  register0 S.Accounts.register_accounts Accounts.register_accounts ;
  register0 S.Accounts.register_client_dir Accounts.register_client_dir ;
  register1 S.Accounts.account Accounts.account ;
  register0 S.Daemons.list Daemons.list ;
  register0 S.Daemons.register Daemons.register ;
  register0 S.Daemons.clean Daemons.clean ;
  register1 S.Daemons.ping Daemons.ping ;
  register1 S.Daemons.ping_pid Daemons.ping_pid ;
  register1 S.Daemons.unregister Daemons.unregister ;
  register1 S.Daemons.kill Daemons.kill ;
  register1 S.Daemons.get Daemons.get ;
  register1 S.Daemons.start Daemons.start ;
  register1 S.Daemons.log Daemons.log ;
  register0 S.Daemons.start_baker Daemons.start_baker ;
  register0 S.Daemons.start_endorser Daemons.start_endorser ;
  register0 S.Daemons.start_accuser Daemons.start_accuser ;
  !dir
