(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019-2020 Origin-Labs                                       *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

val load_nodes_list :
  string ->
  string option ->
  (P2p_addr.t * int) option P2p_peer.Map.t option tzresult Lwt.t

val check_auth :
  Node_config_file.rpc ->
  Resto.meth ->
  Ipaddr.t option ->
  path:string list ->
  headers:(string * string) list ->
  bool

val dynamic_webserver :
  Node_config_file.rpc ->
  Resto.meth ->
  path:string list ->
  headers:'a ->
  ((string * string) list * string) option Lwt.t
