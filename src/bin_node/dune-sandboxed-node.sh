#! /usr/bin/env bash

dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
export DUNE_GENESIS_PROTOCOL_REVISION=max
$dir/tezos-sandboxed-node.sh $@
