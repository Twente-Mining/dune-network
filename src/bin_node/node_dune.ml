(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019-2020 Origin-Labs                                       *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module WWW = Dune_www.Dune_www_files
module StringMap = Map.Make (String)

let files =
  let files = ref StringMap.empty in
  List.iter
    (fun (file, content) -> files := StringMap.add file content !files)
    WWW.files ;
  !files

let nodes_list_encoding =
  Data_encoding.(
    obj1
      (req
         "nodes"
         (list
            (union
               [ case
                   ~title:"short"
                   (Tag 0)
                   P2p_peer.Id.encoding
                   (function (x, None) -> Some x | _ -> None)
                   (fun x -> (x, None));
                 case
                   ~title:"object"
                   (Tag 1)
                   (obj2
                      (req "peer_id" P2p_peer.Id.encoding)
                      (opt "addr" string))
                   (fun x -> Some x)
                   (fun x -> x) ]))))

let ( // ) = Filename.concat

let load_nodes_list default_dir filename =
  match filename with
  | None ->
      let closed_network = Tezos_base.Config.config.closed_network in
      if closed_network then
        failwith
          "closed_network is true in the network config, but no nodes_list \
           was specified."
      else return_none
  | Some filename -> (
      let filename =
        if Filename.is_relative filename then default_dir // filename
        else filename
      in
      Lwt_utils_unix.Json.read_file filename
      >>=? fun json ->
      match Data_encoding.Json.destruct nodes_list_encoding json with
      | exception exn ->
          fail (Exn exn)
      | list ->
          fold_left_s
            (fun map (id, addr) ->
              match addr with
              | None ->
                  return (P2p_peer.Map.add id None map)
              | Some addr -> (
                  Node_config_file.resolve_bootstrap_addrs [addr]
                  >>= function
                  | [] ->
                      assert false
                  | addr :: _ ->
                      return (P2p_peer.Map.add id (Some addr) map) ))
            P2p_peer.Map.empty
            list
          >>=? fun map -> return_some map )

(* TODO: for now, we only check the basic auth schema, but we could also
   restrict by IP and by path (making some paths public for example). *)
let check_auth rpc_config meth ip ~path ~headers =
  (*  if Dune_config.Env_config.tezos_compatible_mode then true else *)
  if String.Map.is_empty rpc_config.Node_config_file.users then
    rpc_config.Node_config_file.allow_public_access
    ||
    match ip with
    | None ->
        assert false
    | Some ip -> (
      match Ipaddr.to_string ip with
      | "127.0.0.1" | "::1" | "::ffff:127.0.0.1" ->
          true
      | ip ->
          Format.eprintf "RPC error: unauthorized IP %s@." ip ;
          false )
  else
    let users = rpc_config.Node_config_file.users in
    (* authentified users have full access *)
    RPC_auth.check_basic_auth ~users ~headers
    || (* unauthentified users have only access to read-only
       unexpensive calls *)
       rpc_config.allow_public_access
       &&
       match meth with
       | `GET -> (
         match path with
         | "network" :: ("version" | "versions") :: _
         | "monitor" :: _
         | "describe" :: _
         | "dune" :: "version" :: _
         | "fetch_protocol" :: _
         | "protocols" :: _ ->
             true
         | "chains" :: _ :: chain_query -> (
           match chain_query with
           | "chain_id" :: _
           | "checkpoint" :: _
           | "invalid_blocks" :: _
           | "mempool" :: _ ->
               true
           (* do not allow listing of multiple blocks *)
           | "blocks" :: _ :: block_query -> (
             match block_query with
             | []
             | "endorsing_power" :: _
             | "hash" :: _
             | "header" :: _
             | "helpers" :: _
             | "live_blocks" :: _
             | "metadata" :: _
             | "minimal_valid_time" :: _
             | "operation_hashes" :: _
             | "operations" :: _
             | "protocols" :: _
             | "required_endorsements" :: _ ->
                 true
             | "context" :: context_query -> (
               match context_query with
               | "activate_protocol" :: _
               | "big_maps" :: _
               | "constants" :: _
               (* do not allow listing of multiple
                                   contracts/delegates *)
               | "contracts" :: _ :: _
               | "delegates" :: _ :: _
               | "nonces" :: _
               | "seed" :: _ ->
                   true
               (* do not allow access to the raw context *)
               | _ ->
                   false )
             | _ ->
                 false )
           | _ ->
               false )
         | _ ->
             false )
       | `POST -> (
         match path with
         | "injection" :: _
         | "chains" :: _ :: "blocks" :: _ :: "helpers" :: _
         | "chains" :: _ :: "mempool" :: _ ->
             true
         | _ ->
             Format.eprintf "Unauthentified method POST@." ;
             false )
       | _ ->
           Format.eprintf
             "Unauthorized method %s@."
             ( match meth with
             | `POST ->
                 "POST"
             | `GET ->
                 "GET"
             | `DELETE ->
                 "DELETE"
             | `PATCH ->
                 "PATCH"
             | `PUT ->
                 "PUT" ) ;
           false

let return_dynamic path =
  try Some (StringMap.find (String.concat "/" path) files)
  with Not_found -> None

let content_type file =
  let len = String.length file in
  let dot_pos =
    match String.rindex file '.' with exception Not_found -> -1 | pos -> pos
  in
  let ext = String.sub file (dot_pos + 1) (len - dot_pos - 1) in
  match
    match String.lowercase_ascii ext with
    | "js" ->
        Some "application/javascript"
    | "html" | "htm" ->
        Some "text/html"
    | "css" ->
        Some "text/css"
    | "png" ->
        Some "image/png"
    | _ ->
        None
  with
  | None ->
      []
  | Some content_type ->
      [("content-type", content_type)]

let normalize_path path =
  let rec iter path rev =
    match path with
    | [] ->
        List.rev rev
    | "." :: path ->
        iter path rev
    | ".." :: path -> (
      match rev with [] -> iter path rev | _ :: rev -> iter path rev )
    | hd :: tl ->
        iter tl (hd :: rev)
  in
  iter path []

let dynamic_webserver rpc_config meth ~path ~headers:_ =
  let root = rpc_config.Node_config_file.root in
  let path = normalize_path path in
  let path = match path with [] -> ["index.html"] | _ -> path in
  let file = String.concat "/" path in
  ( match root with
  | None ->
      Lwt.return_none
  | Some dir ->
      Lwt.catch
        (fun () ->
          let filename = Filename.concat dir file in
          if Sys.file_exists filename then
            Lwt_utils_unix.read_file filename
            >>= fun content -> Lwt.return_some (content_type file, content)
          else Lwt.return_none)
        (fun _ -> Lwt.return_none) )
  >>= function
  | Some v ->
      Lwt.return_some v
  | None -> (
    match meth with
    | `GET -> (
      match return_dynamic path with
      | Some body ->
          Lwt.return_some (content_type file, body)
      | None ->
          Lwt.return_none )
    | _ ->
        Lwt.return_none )
