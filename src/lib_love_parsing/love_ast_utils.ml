(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_ast_types
open AST

(** Type definitions *)
let mk_sumtype sparams scons srec = TYPE.SumType {sparams; scons; srec}

let mk_rectype rparams rfields rrec = TYPE.RecordType {rparams; rfields; rrec}

let mk_alias aparams atype = TYPE.Alias {aparams; atype}

let mk_loc ?loc content = {content; annot = loc}

let e = mk_loc

(** Const *)

let mk_cunit ?loc () = e ?loc CUnit

let mk_cbool ?loc b = e ?loc (CBool b)

let mk_cstring ?loc s = e ?loc (CString s)

let mk_cbytes ?loc b = e ?loc (CBytes b)

let mk_cint ?loc i = e ?loc (CInt i)

let mk_cnat ?loc i = e ?loc (CNat i)

let mk_cdun ?loc d = e ?loc (CDun d)

let mk_ckey ?loc k = e ?loc (CKey k)

let mk_ckeyhash ?loc kh = e ?loc (CKeyHash kh)

let mk_csig ?loc s = e ?loc (CSignature s)

let mk_caddress ?loc a = e ?loc (CAddress a)

let mk_ctimestamp ?loc ts = e ?loc (CTimestamp ts)

(** Pattern *)

let mk_pany ?loc () = e ?loc PAny

let mk_pvar ?loc v = e ?loc (PVar v)

let mk_palias ?loc p v = e ?loc (PAlias (p, v))

let mk_pconst ?loc c = e ?loc (PConst c)

let mk_plist ?loc l = e ?loc (PList l)

let mk_ptuple ?loc l = e ?loc (PTuple l)

let mk_pconstr ?loc c l = e ?loc (PConstr (c, l))

let mk_pcontract ?loc n st = e ?loc (PContract (n, st))

let mk_por ?loc l = e ?loc (POr l)

let mk_precord ?loc l = e ?loc (PRecord l)

(** Expressions *)
let mk_const ?loc const = e ?loc (Const const)

let mk_var ?loc v = e ?loc (Var v)

let mk_var_with_arg ?loc v xa = e ?loc (VarWithArg (v, xa))

let mk_let ?loc bnd_pattern bnd_val body =
  e ?loc @@ Let {bnd_pattern; bnd_val; body}

let mk_let_rec ?loc bnd_var bnd_val body fun_typ =
  let bnd_var = bnd_var in
  let rec check_typ t =
    match t with
    | TYPE.TArrow (_, _) ->
        e ?loc @@ LetRec {bnd_var; bnd_val; body; fun_typ}
    | TYPE.TForall (_, t) ->
        check_typ t
    | _ ->
        raise
          (BadArgument
             "Lost_ast_utils.mk_let_rec: Recursive let has a non arrow type")
  in
  check_typ fun_typ

let mk_lambda ?loc arg_pattern body arg_typ =
  e ?loc @@ Lambda {arg_pattern; body; arg_typ}

let mk_apply ?loc fct args = e ?loc @@ Apply {fct; args}

let mk_apply_c ?loc cst args =
  e ?loc
  @@ Apply {fct = mk_const ?loc cst; args = List.map (mk_const ?loc) args}

let mk_tlambda ?loc targ exp = e ?loc @@ TLambda {targ; exp}

let mk_tapply ?loc exp typ = e ?loc @@ TApply {exp; typ}

let mk_seq ?loc elist = e ?loc @@ Seq elist

let mk_if ?loc cond ifthen ifelse = e ?loc @@ If {cond; ifthen; ifelse}

let mk_match ?loc arg cases = e ?loc @@ Match {arg; cases}

let mk_constr ?loc constr ctyps args =
  e ?loc @@ Constructor {constr; ctyps; args}

let mk_enil ?loc ?typ () =
  let en = e ?loc Nil in
  match typ with None -> en | Some t -> mk_tapply ?loc en t

let mk_list ?loc ?typ args = e ?loc @@ List (args, mk_enil ?typ ())

let mk_concat_list ?loc args l = e ?loc @@ List (args, l)

let mk_tuple ?loc l = e ?loc @@ Tuple l

let mk_projection ?loc tuple indexes = e ?loc @@ Project {tuple; indexes}

let mk_update ?loc tuple updates = e ?loc @@ Update {tuple; updates}

let mk_record ?loc p l = e ?loc @@ Record {path = p; contents = l}

let mk_get_field ?loc record field = e ?loc @@ GetField {record; field}

let mk_set_field ?loc record updates = e ?loc @@ SetFields {record; updates}

let mk_packstruct ?loc sr = e ?loc @@ PackStruct sr

let mk_raise ?loc exn args = e ?loc @@ Raise {exn; args; loc}

let mk_try_with ?loc arg cases = e ?loc @@ TryWith {arg; cases}

(** Contract utils *)

let mk_entry entry_code entry_fee_code param_typ =
  Entry {entry_code; entry_fee_code; entry_typ = param_typ}

let mk_view view_code view_typ view_recursive =
  View {view_code; view_typ; view_recursive}

let mk_value value_code value_typ value_visibility value_recursive =
  Value {value_code; value_typ; value_visibility; value_recursive}

let mk_structure c = Structure c

let mk_type k tdef = DefType (k, tdef)

(** Utils *)

let rec sig_of_structure {structure_content; kind} : TYPE.structure_sig =
  let sig_content =
    List.fold_left
      (fun acc (n, content) ->
        match content with
        | DefType (TInternal, _td) ->
            acc
        | DefType (TPublic, td) ->
            (n, TYPE.SType (SPublic td)) :: acc
        | DefType (TPrivate, td) ->
            (n, TYPE.SType (SPrivate td)) :: acc
        | DefType (TAbstract, td) ->
            let params =
              match td with
              | Alias {aparams = p; _}
              | SumType {sparams = p; _}
              | RecordType {rparams = p; _} ->
                  p
            in
            (n, SType (SAbstract params)) :: acc
        | DefException tl ->
            (n, SException tl) :: acc
        | Init {init_typ = tparam; _} ->
            (n, SInit tparam) :: acc
        | Entry {entry_typ = tparam; _} ->
            (n, SEntry tparam) :: acc
        | View {view_typ; _} ->
            (n, SView view_typ) :: acc
        | Value {value_typ; value_visibility = Public; _} ->
            (n, SValue value_typ) :: acc
        | Value {value_visibility = Private; _} ->
            acc
        | Structure s ->
            (n, SStructure (Anonymous (sig_of_structure s))) :: acc
        | Signature s ->
            (n, SSignature s) :: acc)
      []
      structure_content
  in
  TYPE.{sig_content = List.rev sig_content; sig_kind = kind}

module Raw = struct
  let lambdas_to_params e =
    let destruct_lambda (x : exp) =
      match x with
      | {content = Lambda {arg_pattern = p; arg_typ = t; body = b}; _} ->
          Some (p, t, b)
      | _ ->
          None
    in
    let rec aux pl l =
      match destruct_lambda l with
      | None ->
          List.rev pl
      | Some (p, t, b) ->
          aux ((p, t) :: pl) b
    in
    aux [] e

  let params_to_lambdas pl b =
    let mk_lambda arg_pattern arg_typ body : exp =
      {content = Lambda {arg_pattern; body; arg_typ}; annot = None}
    in
    List.fold_left (fun e (p, t) -> mk_lambda p t e) b (List.rev pl)
end
