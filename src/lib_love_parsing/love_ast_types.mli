(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

exception BadArgument of string

exception ParseError of string

type 'a ident = LDot of 'a * 'a ident | LName of 'a

module TYPE : sig
  type traits = {tcomparable : bool}

  type type_var = {tv_name : string; tv_traits : traits}

  type type_name = string ident

  type structure_type_name = string ident

  type multiple_path =
    | Local
    | AbsolutePath of string list
    | Multiple of multiple_path list

  type dependency = string * string

  type dependencies = dependency list

  type struct_kind = Module | Contract of dependencies

  type recursive = Rec | NonRec

  type typedef =
    | Alias of {aparams : type_var list; atype : t}
    | SumType of {
        sparams : type_var list;
        scons : (string * t list) list;
        srec : recursive;
      }
    | RecordType of {
        rparams : type_var list;
        rfields : (string * t) list;
        rrec : recursive;
      }

  and sig_type =
    | SAbstract of type_var list
        (** Abstract type, exists outside but has no definition *)
    | SPrivate of typedef
        (** Private type, exists but cannot be built outside *)
    | SPublic of typedef  (** Public type, no restriction *)

  and sig_content =
    | SType of sig_type
    | SException of t list
    | SInit of t (* initializer parameter *)
    | SEntry of t (* entry points parameter *)
    | SView of t (* function type *)
    | SValue of t
    | SStructure of structure_type
    | SSignature of structure_sig

  and structure_sig = {
    sig_kind : struct_kind;
    sig_content : (string * sig_content) list;
  }

  and structure_type =
    | Anonymous of structure_sig
    | Named of structure_type_name

  and t =
    (* Composite *)
    | TUser of type_name * t list (* ('a, 'b) t <=> TSum ("t", [TVar a, TVar b]) *)
    | TTuple of t list
    (* Functions, closures, primitive *)
    | TArrow of t * t
    (* Type variables *)
    | TVar of type_var
    (* Polymorphism *)
    | TForall of type_var * t (* comparable ? *)
    (* Contracts and entry points *)
    | TPackedStructure of structure_type (* type of packed structure *)
    | TContractInstance of structure_type

  (* type of contract instance *)

  type type_exp = {
    (* let lettypes in body *)
    lettypes : (string ident * typedef) list;
    body : t;
  }

  type extended_arg = ANone | AContractType of structure_type

  (* Arguments of type user_comp return true for user types that are comparable
   when their polymorphic arguments are. *)
  type user_comp = type_name -> bool

  (** Collections for type variables *)
  module TypeVarSet : S.SET with type elt = type_var

  module TypeVarMap : S.MAP with type key = type_var

  val default_trait : traits
end

module VALUE : sig
  type t =
    (* Base *)
    | VUnit
    | VBool of bool
    | VString of string
    | VBytes of MBytes.t
    | VInt of Z.t
    | VNat of Z.t
    (* Composite *)
    | VTuple of t list
    | VConstr of string * t list
    | VRecord of (string * t) list
    (* Collections *)
    | VList of t list
    | VSet of t list
    | VMap of (t * t) list
    | VBigMap of bigmap
    (* Domain-specific *)
    | VDun of int64
    | VKey of Signature.Public_key.t
    | VKeyHash of Signature.Public_key_hash.t
    | VSignature of Signature.t
    | VTimestamp of Z.t
    | VAddress of string

  (* b58check *)
  and bigmap = {diff : (t * t) list; key_type : TYPE.t; value_type : TYPE.t}
end

module AST : sig
  type ('a, 'annot) annoted = {content : 'a; annot : 'annot}

  type location = {pos_lnum : int; pos_bol : int; pos_cnum : int}

  type var_ident = string ident

  type cstr_name = string ident

  type field_name = string

  type visibility = Private | Public

  type type_kind = TPublic | TPrivate | TInternal | TAbstract

  type exn_name = Fail of TYPE.t | Exception of string ident

  (*
module type Annot = sig
  type t
  val encoding: t Data_encoding.t
  val serialize : Love_binary_buffer.writer -> t -> unit
  val deserialize : Love_binary_buffer.reader -> t
  val print: Format.formatter -> t -> unit
end
*)

  type 'a located = ('a, location option) annoted

  type raw_const =
    | CUnit
    | CBool of bool
    | CString of string
    | CBytes of MBytes.t
    | CInt of Z.t
    | CNat of Z.t
    | CDun of int64
    | CKey of Signature.Public_key.t
    | CKeyHash of Signature.Public_key_hash.t
    | CSignature of Signature.t
    | CTimestamp of Z.t
    | CAddress of string

  and const = raw_const located

  type open_closed = Open | Closed

  type 'pattern pattern_record_list = {
    fields : (string * 'pattern option) list;
    open_record : open_closed;
  }

  type raw_pattern =
    | PAny
    | PVar of string
    | PAlias of pattern * string
    | PConst of const
    | PList of pattern list
    | PTuple of pattern list
    | PConstr of string * pattern list
    | PContract of string * TYPE.structure_type
    | POr of pattern list
    | PRecord of pattern pattern_record_list

  and pattern = raw_pattern located

  type raw_exp =
    (* Base language *)
    | Const of const
    | Var of var_ident
    | VarWithArg of var_ident * TYPE.extended_arg
    | Let of {bnd_pattern : pattern; bnd_val : exp; body : exp}
    | LetRec of {bnd_var : string; bnd_val : exp; body : exp; fun_typ : TYPE.t}
    | Lambda of lambda
    | Apply of {fct : exp; args : exp list}
    | TLambda of {targ : TYPE.type_var; exp : exp}
    | TApply of {exp : exp; typ : TYPE.t}
    (* Common extensions *)
    | Seq of exp list
    | If of {cond : exp; ifthen : exp; ifelse : exp}
    | Match of {arg : exp; cases : (pattern * exp) list}
    (* Exceptions *)
    | Raise of {exn : exn_name; args : exp list; loc : location option}
    | TryWith of {arg : exp; cases : ((exn_name * pattern list) * exp) list}
    (* Collections *)
    | Nil
    | List of exp list * exp
    (* Tuples *)
    | Tuple of exp list
    | Project of {tuple : exp; indexes : int list} (* non-empty, >= 0 *)
    | Update of {tuple : exp; updates : (int * exp) list} (* non-empty, >= 0 *)
    (* Sums *)
    | Constructor of {constr : cstr_name; ctyps : TYPE.t list; args : exp list}
    (* Records *)
    | Record of {
        path : string ident option;
        contents : (field_name * exp) list;
      }
    | GetField of {record : exp; field : field_name}
    | SetFields of {record : exp; updates : (field_name * exp) list}
    (* First-class structures *)
    | PackStruct of reference

  and exp = (raw_exp, location option) annoted

  and lambda = {arg_pattern : pattern; body : exp; arg_typ : TYPE.t}

  and entry = {
    entry_code : exp;
    entry_fee_code : exp option;
    (* An expression of type (fun, nat) *)
    entry_typ : TYPE.t; (* Parameter type *)
  }

  and view = {
    view_code : exp;
    view_typ : TYPE.t;
    (* Type of the view content *)
    view_recursive : TYPE.recursive;
  }

  and value = {
    value_code : exp;
    value_typ : TYPE.t;
    value_visibility : visibility;
    value_recursive : TYPE.recursive;
  }

  and init = {init_code : exp; init_typ : TYPE.t; init_persist : bool}

  and content =
    | DefType of type_kind * TYPE.typedef
    | DefException of TYPE.t list
    | Init of init
    | Entry of entry
    | View of view
    | Value of value
    | Structure of structure
    | Signature of TYPE.structure_sig

  and structure = {
    kind : TYPE.struct_kind;
    structure_content : (string * content) list;
  }

  (* unique names at top level *)
  and reference = Anonymous of structure | Named of string ident

  type top_contract = {
    version : int * int;
    (* (major, minor) *)
    code : structure;
  }

  type top_value = VALUE of VALUE.t | TYPE of TYPE.t
end

module CONSTANTS : sig
  val current : string

  val anonymous : string

  val current_id : string ident

  val anonymous_id : string ident

  val init_storage : string
end

module OPTIONS : sig
  (* GLOBAL_STATE *)

  val version : int * int

  val keep_ghost : bool ref
end
