(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_ast_types
open Love_ast_types.AST

(** Type definition builders *)
val mk_sumtype :
  TYPE.type_var list ->
  (string * TYPE.t list) list ->
  TYPE.recursive ->
  TYPE.typedef

val mk_rectype :
  TYPE.type_var list ->
  (string * TYPE.t) list ->
  TYPE.recursive ->
  TYPE.typedef

val mk_alias : TYPE.type_var list -> TYPE.t -> TYPE.typedef

(** Constant builder *)
val mk_cunit : ?loc:location -> unit -> const

val mk_cbool : ?loc:location -> bool -> const

val mk_cstring : ?loc:location -> string -> const

val mk_cbytes : ?loc:location -> MBytes.t -> const

val mk_cint : ?loc:location -> Z.t -> const

val mk_cnat : ?loc:location -> Z.t -> const

val mk_cdun : ?loc:location -> int64 -> const

val mk_ckey : ?loc:location -> Signature.Public_key.t -> const

val mk_ckeyhash : ?loc:location -> Signature.Public_key_hash.t -> const

val mk_csig : ?loc:location -> Signature.t -> const

val mk_caddress : ?loc:location -> string -> const

val mk_ctimestamp : ?loc:location -> Z.t -> const

(*
val is_runtime_only_const : const -> bool
*)

(** Pattern builder *)
val mk_pany : ?loc:location -> unit -> pattern

val mk_pvar : ?loc:location -> string -> pattern

val mk_palias : ?loc:location -> pattern -> string -> pattern

val mk_pconst : ?loc:location -> const -> pattern

val mk_plist : ?loc:location -> pattern list -> pattern

val mk_ptuple : ?loc:location -> pattern list -> pattern

val mk_pconstr : ?loc:location -> string -> pattern list -> pattern

val mk_pcontract : ?loc:location -> string -> TYPE.structure_type -> pattern

val mk_por : ?loc:location -> pattern list -> pattern

val mk_precord : ?loc:location -> pattern pattern_record_list -> pattern

(** Expression builder *)
val mk_const : ?loc:location -> const -> exp

val mk_var : ?loc:location -> string ident -> exp

val mk_var_with_arg : ?loc:location -> string ident -> TYPE.extended_arg -> exp

val mk_let : ?loc:location -> pattern -> exp -> exp -> exp

val mk_let_rec : ?loc:location -> string -> exp -> exp -> TYPE.t -> exp

val mk_lambda : ?loc:location -> pattern -> exp -> TYPE.t -> exp

val mk_apply : ?loc:location -> exp -> exp list -> exp

val mk_apply_c : ?loc:location -> const -> const list -> exp

val mk_tlambda : ?loc:location -> TYPE.type_var -> exp -> exp

val mk_tapply : ?loc:location -> exp -> TYPE.t -> exp

val mk_seq : ?loc:location -> exp list -> exp

val mk_if : ?loc:location -> exp -> exp -> exp -> exp

val mk_match : ?loc:location -> exp -> (pattern * exp) list -> exp

val mk_constr : ?loc:location -> string ident -> TYPE.t list -> exp list -> exp

val mk_list : ?loc:location -> ?typ:TYPE.t -> exp list -> exp

val mk_concat_list : ?loc:location -> exp list -> exp -> exp

val mk_tuple : ?loc:location -> exp list -> exp

val mk_projection : ?loc:location -> exp -> int list -> exp

val mk_update : ?loc:location -> exp -> (int * exp) list -> exp

val mk_record :
  ?loc:location -> string ident option -> (string * exp) list -> exp

val mk_get_field : ?loc:location -> exp -> string -> exp

val mk_set_field : ?loc:location -> exp -> (string * exp) list -> exp

val mk_packstruct : ?loc:location -> reference -> exp

val mk_raise : ?loc:location -> exn_name -> exp list -> exp

val mk_try_with :
  ?loc:location -> exp -> ((exn_name * pattern list) * exp) list -> exp

val mk_enil : ?loc:location -> ?typ:TYPE.t -> unit -> exp

(*
val mk_emptyset    : ?loc:location -> ?typ:TYPE.t -> unit -> exp
val mk_emptymap    : ?loc:location -> ?typ_key:TYPE.t -> ?typ_bnd:TYPE.t -> unit -> exp
val mk_emptybigmap : ?loc:location -> ?typ_key:TYPE.t -> ?typ_bnd:TYPE.t -> unit -> exp
*)
(*
val mk_primitive_lambda :
  ?loc:location ->
  (TYPE.type_name -> TYPE.typedef) ->
  Love_primitive.t -> TYPE.t -> exp
*)

(** Contracts *)
val mk_type : AST.type_kind -> TYPE.typedef -> content

val mk_entry : exp -> exp option -> TYPE.t -> content

val mk_view : exp -> TYPE.t -> TYPE.recursive -> content

val mk_value : exp -> TYPE.t -> AST.visibility -> TYPE.recursive -> content

val mk_structure : structure -> content

val sig_of_structure : structure -> TYPE.structure_sig

module Raw : sig
  val lambdas_to_params : exp -> (pattern * TYPE.t) list

  val params_to_lambdas : (pattern * TYPE.t) list -> exp -> exp
end
