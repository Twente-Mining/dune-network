(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_ast_types.TYPE

(** Checks if a given type is comparable. *)
val isComparable : user_comp -> t -> bool

(** Utils replacing type variables by actual types. *)
val replace_struct_sig :
  user_comp -> t TypeVarMap.t -> structure_sig -> structure_sig

val replace_tdef : user_comp -> t TypeVarMap.t -> typedef -> typedef

val replace_sig_type : user_comp -> t TypeVarMap.t -> sig_type -> sig_type

val replace_map : user_comp -> t TypeVarMap.t -> t -> t

(** Defines an arrow type. The operator is right associative
    t1 @=> t2 @=> t3  =  TArrow (t1, TArrow (t2, t3))
*)
val ( @=> ) : t -> t -> t

(** Returns the arrow type corresponding to the list
    of types.

    Examples:
    * arrowFromList [t1; t2] = t1 @=> t2
    * arrowFromList [t] = t
    * arrowFromList [] = Exception.BadArgument "arrowFromList"
 *)
val arrowFromList : t list -> t
