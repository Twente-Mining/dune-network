(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let init_signal () =
  let handler name id =
    try
      Format.eprintf "Received the %s signal, triggering shutdown.@." name ;
      Lwt_exit.exit id
    with _ -> ()
  in
  ignore
    (Lwt_unix.on_signal Sys.sigint (handler "INT") : Lwt_unix.signal_handler_id) ;
  ignore
    ( Lwt_unix.on_signal Sys.sigterm (handler "TERM")
      : Lwt_unix.signal_handler_id )

let may_lock_pidfile = function
  | None ->
      return_unit
  | Some pidfile ->
      trace (failure "Failed to create the pidfile: %s" pidfile)
      @@ Lwt_lock_file.create ~unlink_on_exit:true pidfile

let rec retry (cctxt : #Client_context.full) ~delay ~tries f x =
  f x
  >>= function
  | Ok _ as r ->
      Lwt.return r
  | Error
      (RPC_client_errors.Request_failed {error = Connection_failed _; _} :: _)
    as err
    when tries > 0 -> (
      cctxt#message "Connection refused, retrying in %.2f seconds..." delay
      >>= fun () ->
      Lwt.pick
        [ (Lwt_unix.sleep delay >|= fun () -> `Continue);
          (Lwt_exit.termination_thread >|= fun _ -> `Killed) ]
      >>= function
      | `Killed ->
          Lwt.return err
      | `Continue ->
          retry cctxt ~delay:(delay *. 1.5) ~tries:(tries - 1) f x )
  | Error _ as err ->
      Lwt.return err

let await_bootstrapped_node (cctxt : #Client_context.full) =
  (* Waiting for the node to be synchronized *)
  cctxt#message "Waiting for the node to be synchronized with its peers..."
  >>= fun () ->
  retry cctxt ~tries:5 ~delay:1. Shell_services.Monitor.bootstrapped cctxt
  >>=? fun _ -> cctxt#message "Node synchronized." >>= fun () -> return_unit

let clean_nonces (wallet : #Client_context.wallet) location =
  wallet#write
    (Client_baking_files.filename location)
    []
    (* Dummy empty map *)
    Data_encoding.(list unit)

let monitor_fork_testchain (cctxt : #Client_context.full) ~cleanup_nonces =
  (* Waiting for the node to be synchronized *)
  cctxt#message "Waiting for the test chain to be forked..."
  >>= fun () ->
  Shell_services.Monitor.active_chains cctxt
  >>=? fun (stream, _) ->
  let rec loop () =
    Lwt_stream.next stream
    >>= fun l ->
    let testchain =
      List.find_opt
        (function Shell_services.Monitor.Active_test _ -> true | _ -> false)
        l
    in
    match testchain with
    | Some (Active_test {expiration_date; _}) ->
        let abort_daemon () =
          cctxt#message
            "Test chain's expiration date reached (%a)... Stopping the \
             daemon.@."
            Time.Protocol.pp_hum
            expiration_date
          >>= fun () ->
          if cleanup_nonces then
            (* Clean-up existing nonces *)
            cctxt#with_lock (fun () ->
                Client_baking_files.resolve_location cctxt ~chain:`Test `Nonce
                >>=? fun nonces_location -> clean_nonces cctxt nonces_location)
          else return_unit >>=? fun () -> exit 0
        in
        let canceler = Lwt_canceler.create () in
        Lwt_canceler.on_cancel canceler (fun () ->
            abort_daemon () >>= function _ -> Lwt.return_unit) ;
        let now = Time.System.(to_protocol (Systime_os.now ())) in
        let delay = Int64.to_int (Time.Protocol.diff expiration_date now) in
        if delay <= 0 then (* Testchain already expired... Retrying. *)
          loop ()
        else
          let timeout =
            Lwt_timeout.create delay (fun () ->
                Lwt_canceler.cancel canceler |> ignore)
          in
          Lwt_timeout.start timeout ; return_unit
    | None ->
        loop ()
    | Some _ ->
        loop ()
  in
  Lwt.pick
    [ (Lwt_exit.termination_thread >>= fun _ -> failwith "Interrupted...");
      loop () ]
  >>=? fun () -> cctxt#message "Test chain forked." >>= fun () -> return_unit

let lwt_stream_partition f l stream =
  let rec lwt_stream_partition l acc stream =
    match l with
    | [] ->
        (List.rev acc, stream)
    | x :: l ->
        let f = function Ok v -> f x v | Error _ -> false in
        let not_f v = not (f v) in
        let stream_true_x = Lwt_stream.filter f stream in
        let stream_false_x = Lwt_stream.filter not_f stream in
        lwt_stream_partition l ((x, stream_true_x) :: acc) stream_false_x
  in
  let (t_stream, stream) = lwt_stream_partition l [] stream in
  let f_stream =
    Lwt_stream.filter (function Ok _ -> true | Error _ -> false) stream
  in
  let err_stream =
    Lwt_stream.filter_map
      (function Ok _ -> None | Error err -> Some err)
      stream
  in
  (t_stream, f_stream, err_stream)
