(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Client_context

module type DAEMON_PARAM = sig
  type args

  val name : string

  val cleanup_testchain_nonces : bool

  val monitor_all_valid_blocks : bool

  val desc_of_args : args -> Dune_node_manager.daemon_desc
end

module type S = sig
  type args

  val register :
    Protocol_hash.t ->
    (full ->
    chain:Chain_services.chain ->
    args ->
    Client_baking_blocks.block_info tzresult Lwt_stream.t ->
    unit tzresult Lwt.t) ->
    unit

  val register_unknown_handler :
    (string ->
    full ->
    Client_baking_blocks.block_info tzresult Lwt_stream.t ->
    unit tzresult Lwt.t) ->
    unit

  val run :
    full ->
    chain:Chain_services.chain ->
    ?protocols:Protocol_hash.t list ->
    args ->
    unit tzresult Lwt.t
end

module Make (D : DAEMON_PARAM) : S with type args := D.args
