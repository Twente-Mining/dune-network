(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Client_context
open Delegate_utils

module type DAEMON_PARAM = sig
  type args

  val name : string

  val cleanup_testchain_nonces : bool

  val monitor_all_valid_blocks : bool

  val desc_of_args : args -> Dune_node_manager.daemon_desc
end

module type S = sig
  type args

  val register :
    Protocol_hash.t ->
    (full ->
    chain:Chain_services.chain ->
    args ->
    Client_baking_blocks.block_info tzresult Lwt_stream.t ->
    unit tzresult Lwt.t) ->
    unit

  val register_unknown_handler :
    (string ->
    full ->
    Client_baking_blocks.block_info tzresult Lwt_stream.t ->
    unit tzresult Lwt.t) ->
    unit

  val run :
    full ->
    chain:Chain_services.chain ->
    ?protocols:Protocol_hash.t list ->
    args ->
    unit tzresult Lwt.t
end

module Make (D : DAEMON_PARAM) = struct
  type args = D.args

  exception Protocol_not_found

  let protocols = Protocol_hash.Table.create 7

  let get_daemons () =
    Protocol_hash.Table.fold (fun k c acc -> (k, c) :: acc) protocols []

  let last_registered_protocol = ref None

  let register name daemon =
    last_registered_protocol := Some name ;
    Protocol_hash.Table.replace protocols name daemon ;
    (* Register protocol in client with no commands just so that it is known *)
    Client_commands.register name (fun _ -> [])

  let daemon_for_protocol proto =
    try Protocol_hash.Table.find protocols proto
    with Not_found -> raise Protocol_not_found

  let _last_registered_protocol () =
    match !last_registered_protocol with
    | None ->
        assert false
    | Some protocol ->
        protocol

  let unknown_handler = ref (fun _ _ _ -> return_unit)

  let register_unknown_handler f = unknown_handler := f

  let unknown_handler name cctxt stream = !unknown_handler name cctxt stream

  (* Exit when connection to node is lost *)
  let error_handler (cctxt : full) stream =
    Lwt_stream.get stream
    >>= function
    | None ->
        cctxt#warning "Connection to node lost, exiting."
    | Some _err ->
        cctxt#warning "Error in block stream, exiting."

  let run (cctxt : full) ~chain ?protocols (args : args) =
    let (daemons, all_active_protocols) =
      match protocols with
      | None ->
          (get_daemons (), true)
      | Some protos ->
          (List.map (fun p -> (p, daemon_for_protocol p)) protos, false)
    in
    let protocols = List.map fst daemons in
    let pid = Unix.getpid () in
    Dune_node_manager.Services.Daemons.register
      cctxt
      ~desc:(D.desc_of_args args)
      ~protocols
      ~all_active_protocols
      ~pid
    >>= (function
          | Error _ ->
              cctxt#warning "Could not register %s to node." D.name
          | Ok _ ->
              Lwt.return_unit)
    >>= fun () ->
    await_bootstrapped_node cctxt
    >>=? fun _ ->
    ( if chain = `Test then
      monitor_fork_testchain cctxt ~cleanup_nonces:D.cleanup_testchain_nonces
    else return_unit )
    >>=? fun () ->
    ( if D.monitor_all_valid_blocks then
      Client_baking_blocks.monitor_valid_blocks
        ~next_protocols:None
        cctxt
        ~chains:[chain]
        ()
    else Client_baking_blocks.monitor_heads ~next_protocols:None cctxt chain )
    >>=? fun block_stream ->
    cctxt#message "Unified %s started." D.name
    >>= fun () ->
    let (daemon_streams, unknown_block_stream, error_block_stream) =
      lwt_stream_partition
        (fun (daemon_proto, _) {Client_baking_blocks.next_protocol; _} ->
          Protocol_hash.equal next_protocol daemon_proto)
        daemons
        block_stream
    in
    let name = String.capitalize_ascii D.name in
    let daemon_threads =
      List.map
        (fun ((proto, daemon), stream) ->
          cctxt#message
            "%s started for protocol %a."
            name
            Protocol_hash.pp
            proto
          >>= fun () ->
          daemon cctxt ~chain args stream
          >>= function
          | Ok () ->
              cctxt#message
                "%s terminated for protocol %a."
                name
                Protocol_hash.pp
                proto
          | Error _err ->
              cctxt#message
                "%s terminated with error for protocol %a."
                name
                Protocol_hash.pp
                proto)
        daemon_streams
    in
    let unknown_thread =
      unknown_handler name cctxt unknown_block_stream
      >>= fun _ -> Lwt.return_unit
    in
    let errors_thread = error_handler cctxt error_block_stream in
    Lwt.pick
      [ (Lwt_exit.termination_thread >|= fun _ -> ());
        errors_thread;
        Lwt.join (unknown_thread :: daemon_threads) ]
    >>= fun () -> return_unit
end
