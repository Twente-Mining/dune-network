(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

type block_hash = BlockHash of string (* 22 bytes *)

type contract_dn1 = ContractHash of string (* 21 bytes *)

type arg = string (* any length, binary encoded *)

type internal_manager_operation = Transaction of transaction

and transaction = {
  tr_source : contract_dn1;
  tr_fee : int;
  tr_counter : int;
  tr_gas_limit : int;
  tr_storage_limit : int;
  tr_amount : int;
  tr_destination : contract_dn1;
  tr_parameters : arg option;
}

type manager_operation =
  | ManagerOperation of block_hash * internal_manager_operation list
