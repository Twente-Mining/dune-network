type t

val lowercase_ascii : char -> char

val code : char -> int

external unsafe_chr : int -> char = "%identity"
