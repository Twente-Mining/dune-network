(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* For now, test the dune app on a Ledger wallet Nano S *)

open Tezos_data_encoding
open Tezos_crypto
open Ledgerwallet_tezos
open Ledgerwallet_dune
open Dune_forge

let mika_blocks =
  [| (* 1 *)
     "\002<\139?ZS\255\149N]A\004L\155`\011j\147\201\177\030\255`\197/\243\168\201\175\181\244\183-:\007\252\141\000\000\000\011\215";
     "\002<\139?Z\243}\178\162\t\144/Y\132\029\155\226\133\219e\166\194W \
      \191\216\150=\015g\019\251O\223\018\131\202\000\000\000\011\216";
     "\002<\139?Z\134\156\\\020zQ%\175E\222\b\162\222?\225\242\b$\253i%\160J\237\159I\235\167O\023!\255\000\000\000\011\217";
     "\001<\139?Z\000\000\011\218\001\134\156\\\020zQ%\175E\222\b\162\222?\225\242\b$\253i%\160J\237\159I\235\167O\023!\255\000\000\000\000]r\133\213\004\170\137\158\225>\159\235\142\150\153t\025\002\023\172n-\1862\023\014\128\023\131\160_\231\165\242j\239d\000\000\000\017\000\000\000\001\000\000\000\000\b\000\000\000\000\000\001\130v\183\164\024(\252\1985\215(:\213\178\128\"\158t\155\208\167u\197\127S\239~Z\027\190\166\149\145\022\000\000\000\000\000\003)\190\148E\000";
     "\002<\139?ZfG7O?#\245{\187\007\014$\1451EWn\174\181\170\211\220\231P\182\237w4\020E\n\
      ^\000\000\000\011\218";
     (* failed *)
     (* 6 *)
     "\002<\139?Z\014\189y\236YB\220\184\235\253hhp\127\226\255\2268\175\226\131\147\136\187\145\165p=\157\151=\199\000\000\000\018\014";
     "\002<\139?Z\025M\204\252\023\143\000\246\020\011\023\172\001\1904\228\146|\"\135\248G\017\255\151\253<\158\162\015O\190\000\000\000\019a";
     "\001<\139?Z\000\000\019\212\001\020Ma\209\132\199\149\236\128\177\13922\177{h\186\173m\218p.b\161\222\187ys\202\211\142\191\000\000\000\000]s\221\t\004 \
      \223(\152\016s\215\146c\253\249\2064\231\002b3\147\1443\152\170\215$+bIx\206\201\191\207\000\000\000\017\000\000\000\001\000\000\000\000\b\000\000\000\000\000\002Kt\223G\186\185\020\226\155'~\231\153\221\021\245\140B\146\159\001[\243Hez\"\197\001\222\203\1960\221\000\000\000\000\000\003\145fD\237\000"
  |]

let hard = Int32.logor 0x8000_0000l

let pkhs =
  [| "dn1H1ZfSHkzX7QXUajzyd7k9VpjKAdsE55d9";
     "dn1LpKvv6URb5PSoW6MqbfRdTe3Z8rsHngNK" |]

let vendor_id = 0x2C97

let product_id = 0x0001

let root = [hard 44l; hard 1729l]

let main_chain_id = "\xe7\x5d\x4a\x33"

let mika_chain_id = "<\139?Z"

let curve = Ledgerwallet_dune.Ed25519

let branch = "01234567890123456789012345678901"

let () = assert (String.length branch = 32)

let replay = ref false

let delay = ref 0.1

let check = function
  | Ok v ->
      v
  | Error err ->
      Format.eprintf
        "Transport Error: %a\n@."
        Ledgerwallet.Transport.pp_error
        err ;
      exit 12

let set_main_chain_id cs pos = Forge_maker.set_string cs pos main_chain_id

open Forge_types

let contract_hash_of_pkh s =
  match Signature.Public_key_hash.of_b58check_opt s with
  | None ->
      assert false
  | Some pkh ->
      let bytes = Signature.Public_key_hash.to_bytes pkh in
      assert (Bytes.length bytes = 21) ;
      ContractHash (Bytes.to_string bytes)

let build_transaction src tr_parameters =
  let op =
    ManagerOperation
      ( BlockHash branch,
        [ Transaction
            {
              tr_source = contract_hash_of_pkh src;
              tr_fee = 1;
              tr_counter = 1;
              tr_gas_limit = 1;
              tr_storage_limit = 1;
              tr_amount = 1;
              tr_destination = contract_hash_of_pkh pkhs.(1);
              tr_parameters;
            } ] )
  in
  let cs =
    Forge_maker.with_cstruct (fun cs pos ->
        let pos = Forge_maker.set_uint8 cs pos 0x03 in
        (* water mark *)
        Forge_maker.forge_manager_operation cs pos op)
  in
  Printf.eprintf "build_transaction: %d bytes\n%!" (Cstruct.len cs) ;
  Printf.eprintf "hex: %s\n%!" (Forge_maker.to_hexa cs) ;
  cs

let build_short_operation () =
  (* Invalid: too short *)
  let cs = Cstruct.create 15 in
  Cstruct.set_uint8 cs 0 0x03 ;
  cs

let build_block_at_level ~level =
  (*
struct block_wire {
    uint8_t magic_byte;
    uint32_t chain_id;
    uint32_t level;
    uint8_t proto;
    // ... beyond this we don't care
} __attribute__((packed));
*)
  Forge_maker.with_cstruct (fun cs pos ->
      (* block must be at least 33 chars for parse_operations *)
      let pos = Forge_maker.set_uint8 cs pos 0x01 in
      (* block watermark *)
      let pos = set_main_chain_id cs pos in
      (* block watermark *)
      let pos = Forge_maker.set_uint32 cs pos level in
      (* level *)
      Forge_maker.set_uint8 cs pos 0)

(* proto *)

let pk_of_path h curve path =
  let cs =
    Ledgerwallet_dune.get_public_key ~prompt:false h curve path |> check
  in
  let tab = Cstruct.to_bytes cs in
  (* first byte = 2 *)
  TzEndian.set_int8 tab 0 0 ;
  (* hackish, but works. *)
  let pk =
    Data_encoding.Binary.of_bytes_exn Signature.Public_key.encoding tab
  in
  Signature.Public_key.to_b58check pk

let pkh_of_pk pk =
  match Signature.Public_key.of_b58check_opt pk with
  | None ->
      assert false
  | Some pk ->
      let pkh = Signature.Public_key.hash pk in
      Signature.Public_key_hash.to_b58check pkh

let string_of_path path =
  String.concat
    "'/"
    (List.map (fun v -> Int32.to_string (Int32.logand 0x7fff_ffffl v)) path)

let print_pk path pk =
  Printf.eprintf "Public Key %s: %s\n%!" (string_of_path path) pk

let print_pkh path pkh =
  Printf.eprintf "Public Key Hash %s: %s\n%!" (string_of_path path) pkh

let list_auth_keys h =
  Printf.eprintf "Getting auth keys:\n%!" ;
  match Ledgerwallet_dune.get_authorized_path_and_curve h with
  | Ok list ->
      List.iteri
        (fun i (path, _curve) ->
          Printf.eprintf "%d -> %s\n%!" i (string_of_path path))
        list
  | Error _ ->
      ()

let switch_to_wallet_mode h is_baking =
  match is_baking with
  | Ledgerwallet_dune.Version.Wallet ->
      ()
  | _ -> (
      Printf.eprintf "switch to wallet...\n%!" ;
      match Ledgerwallet_dune.set_baking_mode h Wallet |> check with
      | Baking _ ->
          assert false
      | Wallet ->
          () )

let switch_to_baking_mode h ~volatile_watermarks is_baking =
  match is_baking with
  | Ledgerwallet_dune.Version.Baking {volatile_watermarks = v; _}
    when volatile_watermarks = v ->
      ()
  | _ -> (
      Printf.eprintf "switch to baking...\n%!" ;
      match
        Ledgerwallet_dune.set_baking_mode
          h
          (Baking {always = false; volatile_watermarks})
        |> check
      with
      | Baking _ ->
          ()
      | Wallet ->
          assert false )

let max_delay = ref 0

let npaths = ref 1

let setup_ledger = ref false

let init_level = ref 1

let nlevels = ref (Array.length mika_blocks)

let wallet = ref false

let persistent = ref false

let test_wallet h is_baking paths =
  switch_to_wallet_mode h is_baking ;
  match paths with
  | [] ->
      assert false
  | (path, path_str, pkh) :: _ ->
      let op = build_transaction pkh None in
      Printf.eprintf "Signing transaction for %s\n%!" path_str ;
      let _ = Ledgerwallet_dune.sign h curve path op |> check in
      let op = build_transaction pkh (Some "small arg") in
      Printf.eprintf "Signing contract call for %s\n%!" path_str ;
      let _ = Ledgerwallet_dune.sign h curve path op |> check in
      let op =
        build_transaction
          pkh
          (Some
             "very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg, very large arg, very large arg, very large arg, \
              very large arg")
      in
      Printf.eprintf "Signing large contract call for %s\n%!" path_str ;
      let _ = Ledgerwallet_dune.sign h curve path op |> check in
      ()

let test_baking h is_baking paths =
  switch_to_baking_mode h is_baking ~volatile_watermarks:(not !persistent) ;
  let main_chain_id = if !replay then mika_chain_id else main_chain_id in
  if !setup_ledger then (
    Ledgerwallet_dune.deauthorize_baking h |> check ;
    list_auth_keys h ;
    match paths with
    | [] ->
        assert false
    | (path, path_str, _pkh) :: rem ->
        Printf.eprintf "Setup for %s\n%!" path_str ;
        let _pubkey =
          Ledgerwallet_dune.setup_baking
            h
            ~main_chain_id
            ~main_hwm:0l
            ~test_hwm:0l
            curve
            path
          |> check
        in
        list_auth_keys h ;
        List.iter
          (fun (path, path_str, _pkh) ->
            Printf.eprintf "Setup for %s\n%!" path_str ;
            let _pubkey =
              Ledgerwallet_dune.setup_baking
                ~add:true
                h
                ~main_chain_id
                ~main_hwm:0l
                ~test_hwm:0l
                curve
                path
              |> check
            in
            list_auth_keys h)
          rem ) ;
  list_auth_keys h ;
  Printf.eprintf "Getting auth hwm:\n%!" ;
  let list = Ledgerwallet_dune.get_all_high_watermarks h |> check in
  List.iteri
    (fun i (`Main_hwm hwm, _, `Chain_id s) ->
      Printf.eprintf "%d -> %ld %S\n%!" i hwm s)
    list ;
  let errors = ref 0 in
  let errors2 = ref 0 in
  for level = !init_level to !init_level + !nlevels - 1 do
    if !max_delay > 0 then (
      let delay = Random.int !max_delay in
      Printf.eprintf "Waiting %d seconds (%d/%d)\n%!" delay !errors !errors2 ;
      Unix.sleep delay ) ;
    let (b, what_str) =
      if !replay then
        let s =
          try mika_blocks.(level - !init_level) with _ -> assert false
        in
        ( Cstruct.of_string s,
          Printf.sprintf "mika len %d at level %d" (String.length s) level )
      else
        let level = Int32.of_int level in
        (build_block_at_level ~level, Printf.sprintf "block at level %ld" level)
    in
    List.iter
      (fun (path, path_str, _pkh) ->
        Printf.eprintf "Signing with %s: %s...%!" path_str what_str ;
        let _ = Ledgerwallet_dune.sign h curve path b |> check in
        Printf.eprintf "OK\n%!"
        (*
          match
            Ledgerwallet_dune.sign h curve path b
          with Ok _ -> ()
          | Error _ -> incr errors;
          match
            Ledgerwallet_dune.sign h curve path b
          with Ok _ -> ()
          | Error _ -> incr errors2
*))
      paths ;
    ignore (Unix.select [] [] [] !delay)
  done ;
  ()

let () =
  Arg.parse
    [ ( "--delay",
        Arg.Float (( := ) delay),
        "Set the delay between blocks in seconds" );
      ("--replay", Arg.Set replay, "Replay previous bugs");
      ( "--max-delay",
        Arg.Int (( := ) max_delay),
        "Max delay between signatures" );
      ("--npaths", Arg.Int (( := ) npaths), "Number of paths per ledger");
      ("--setup", Arg.Set setup_ledger, "Setup ledger");
      ("--level", Arg.Int (( := ) init_level), "Init Level");
      ("--nlevels", Arg.Int (( := ) nlevels), "Levels to sign");
      ("--wallet", Arg.Set wallet, "Test wallet");
      ("--persistent", Arg.Set persistent, "Use persistent memory") ]
    (fun _ -> failwith "no anon arg")
    "" ;
  Printf.eprintf "Checking HID\n%!" ;
  let ledgers = Hidapi.enumerate ~vendor_id ~product_id () in
  let nledgers = List.length ledgers in
  Printf.eprintf "Found %d ledgers\n%!" nledgers ;
  if nledgers > 0 then (
    let () = () in
    Printf.eprintf "Opening ledger\n%!" ;
    let h = Hidapi.open_id_exn ~vendor_id ~product_id in
    Printf.eprintf "Query ledger version\n%!" ;
    let version = Ledgerwallet_dune.get_version h |> check in
    let is_baking = version.Version.app_class in
    Format.eprintf
      "Ledger version: %a (baking %b)\n%!"
      Ledgerwallet_dune.Version.pp
      version
      (match is_baking with Wallet -> false | Baking _ -> true) ;
    Printf.eprintf "Query ledger commit\n%!" ;
    let commit = Ledgerwallet_dune.get_git_commit h |> check in
    Format.eprintf "Ledger commit: %s\n%!" commit ;
    Printf.eprintf "Query Public Key\n%!" ;
    let paths =
      List.init !npaths (fun i ->
          (root @ [hard 0l; hard (Int32.of_int i)], Printf.sprintf "0'/%d'" i))
    in
    let paths =
      List.map
        (fun (path, path_src) ->
          let pk = pk_of_path h curve path in
          print_pk path pk ;
          let pkh = pkh_of_pk pk in
          print_pkh path pkh ; (path, path_src, pkh))
        paths
    in
    if !wallet then test_wallet h is_baking paths
    else test_baking h is_baking paths ;
    Printf.eprintf "Closing ledger\n%!" ;
    Hidapi.close h )
