(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

include Internal_event.Legacy_logging.Make_semantic (struct
  let name = "client.proxy"
end)

let pp_peer ppf fd =
  match Lwt_unix.getpeername fd with
  | ADDR_UNIX _ ->
      Format.fprintf ppf "UNIX"
  | ADDR_INET (host, port) ->
      Format.fprintf ppf "%s:%d" (Unix.string_of_inet_addr host) port

let string_of_peer fd =
  match Lwt_unix.getpeername fd with
  | ADDR_UNIX _ ->
      "UNIX"
  | ADDR_INET (host, port) ->
      Format.sprintf "%s:%d" (Unix.string_of_inet_addr host) port

let nb_connected_remotes =
  Tag.def ~doc:"Number of connected remotes" "nb_remotes" Format.pp_print_int

let remote_info = Tag.def ~doc:"Remote information" "remote_info" pp_peer
