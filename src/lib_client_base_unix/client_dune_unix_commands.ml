(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Clic
open Client_dune_commands

let to_json_string ?(compact = false) encoding data =
  let ezjson =
    (module Json_repr.Ezjsonm : Json_repr.Repr
      with type value = Json_repr.ezjsonm )
  in
  Json_repr.pp
    ~compact
    ezjson
    Format.str_formatter
    (Json_encoding.construct encoding data) ;
  Format.flush_str_formatter ()

let commands = ref []

let command ~group ~desc args path f =
  let cmd = Clic.command ~group ~desc args path f in
  commands := cmd :: !commands

let () =
  command
    ~group
    ~desc:"Hash a file."
    (args1 HASH_STRING.hash_algo_arg)
    ( prefixes ["dune"; "hash"; "file"]
    @@ Clic.string ~name:"data" ~desc:"file to hash"
    @@ stop )
    (fun (kind, f) filename (cctxt : Client_context.full) ->
      Lwt_utils_unix.read_file filename
      >>= fun content ->
      match f content with
      | Ok hash ->
          cctxt#message "Hash (%s): %s" kind hash >>= fun () -> return_unit
      | _ ->
          assert false)

let () =
  command
    ~group
    ~desc:"The JSON corresponding to some hexa."
    (args2 (input_arg ~default:"protocol_parameters.bin") output_arg)
    (fixed ["dune"; "hexa"; "to"; "json"])
    (fun (file, ppf) _cctxt ->
      Tezos_stdlib_unix.Lwt_utils_unix.read_file file
      >>= fun bytes ->
      let bytes = String.trim bytes in
      (let bytes = Hex.to_bytes (`Hex bytes) in
       match Data_encoding.Binary.of_bytes Data_encoding.json bytes with
       | None ->
           Format.fprintf ppf "Error decoding@."
       | Some json ->
           Format.fprintf ppf "%a@." Data_encoding.Json.pp json) ;
      return_unit)

let () =
  command
    ~group
    ~desc:"Output the current network configuration (mainnet/testnet/appnet)."
    (args1 output_arg)
    (fixed ["dune"; "output"; "network"; "config"])
    (fun ppf _cctxt ->
      let s =
        Data_encoding.Json.to_string
          ~minify:false
          (Data_encoding.Json.construct
             Tezos_base.Config_network.encoding
             Tezos_base.Config_network.config)
      in
      Format.fprintf ppf "%s@." s ;
      return_unit)

let () =
  command
    ~group
    ~desc:"Export keys in key-file"
    (args1 output_arg)
    ( prefixes ["dune"; "export"; "keys"]
    @@ seq_of_param Client_keys.Public_key_hash.alias_param )
    (fun ppf keys cctxt ->
      let open Client_keys in
      map_s
        (fun (name, pkh) ->
          Client_keys.Public_key.find_opt cctxt name
          >>= (function Ok x -> Lwt.return x | _ -> Lwt.return None)
          >>= fun pk ->
          Client_keys.Secret_key.find_opt cctxt name
          >>= (function Ok x -> Lwt.return x | _ -> Lwt.return None)
          >>= fun sk -> return {name; pkh; pk; sk})
        keys
      >>=? fun keys ->
      let json =
        Data_encoding.(
          Json.construct (list Client_keys.key_entry_encoding) keys)
      in
      Format.fprintf ppf "%a@." Data_encoding.Json.pp json ;
      return_unit)

let () =
  command
    ~group
    ~desc:"Import keys from key-file"
    (args1 (Client_keys.Public_key.force_switch ()))
    ( prefixes ["dune"; "import"; "keys"]
    @@ Clic.string ~name:"key-file" ~desc:"key-file"
    @@ stop )
    (fun force file (cctxt : Client_context.full) ->
      let open Client_keys in
      Lwt_utils_unix.Json.read_file file
      >>=? fun json ->
      let keys =
        Data_encoding.Json.destruct
          Data_encoding.(list Client_keys.key_entry_encoding)
          json
      in
      iter_s
        (fun {name; pkh; pk; sk} ->
          cctxt#message "Adding key %S..." name
          >>= (fun () ->
                match (pk, sk) with
                | (None, None) ->
                    Public_key_hash.add ~force cctxt name pkh
                    >>=? fun () ->
                    cctxt#message
                      "...added as address %a"
                      Signature.Public_key_hash.pp
                      pkh
                    >>= fun () -> return_unit
                | (Some pk, None) ->
                    Public_key_hash.add ~force cctxt name pkh
                    >>=? fun () ->
                    Public_key.add ~force cctxt name pk
                    >>=? fun () ->
                    cctxt#message
                      "... added as public key %a"
                      Signature.Public_key_hash.pp
                      pkh
                    >>= fun () -> return_unit
                | (_, Some sk_uri) ->
                    Client_keys.neuterize sk_uri
                    >>=? fun pk_uri ->
                    Client_keys.public_key_hash pk_uri
                    >>=? fun (_pkh, public_key) ->
                    cctxt#message
                      "... added as secret key %a"
                      Signature.Public_key_hash.pp
                      pkh
                    >>= fun () ->
                    register_key
                      cctxt
                      ~force
                      (pkh, pk_uri, sk_uri)
                      ?public_key
                      name)
          >>= function
          | Ok () ->
              return_unit
          | Error [] ->
              assert false
          | Error (err :: _) ->
              Format.eprintf "Error with key %S: %a@." name pp err ;
              return_unit)
        keys
      >>=? fun () -> return_unit)

(* If a directory "logs" exists in ~/.dune-client/, it will trigger
   permanent logging of requests in the background. It can be used in
   a debug setup to understand what happened after a command failed.
*)

let full_logger ~base_dir =
  let logs_dirname = Filename.concat base_dir "logs" in
  if Sys.file_exists logs_dirname then (
    let pid = Unix.getpid () in
    let date = Systime_os.now () |> Ptime.to_rfc3339 in
    let oc =
      Printf.kprintf
        open_out
        "%s/%s-%s-%d.log"
        logs_dirname
        (Filename.basename Sys.argv.(0))
        date
        pid
    in
    Array.iteri
      (fun i arg -> Printf.fprintf oc "arg[%d] = %S\n" i arg)
      Sys.argv ;
    Some (Format.formatter_of_out_channel oc) )
  else None

let commands = !commands
