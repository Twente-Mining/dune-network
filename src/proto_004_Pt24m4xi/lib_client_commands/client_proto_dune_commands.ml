(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Client_proto_context
open Client_proto_contracts
open Client_proto_args
open Client_proto_context_commands

let proto_param ~name ~desc t =
  Clic.param
    ~name
    ~desc
    (Clic.parameter (fun _ str -> Lwt.return (Protocol_hash.of_b58check str)))
    t

let destinations_encoding =
  let open Data_encoding in
  list
    (obj4
       (req "address" Contract.encoding)
       (req "amount" Tez.encoding)
       (opt "collect_call_fee_gas" z)
       (opt "arg" string))

let read_destinations_file filename =
  Lwt_utils_unix.Json.read_file filename
  >>=? fun json ->
  match Data_encoding.Json.destruct destinations_encoding json with
  | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
      Format.kasprintf
        (fun s -> failwith "%s" s)
        "Invalid destinations file: %a %a"
        (fun ppf -> Data_encoding.Json.print_error ppf)
        exn
        Data_encoding.Json.pp
        json
  | destinations ->
      return destinations

open Clic

let batch_transfer_command =
  command
    ~group:Client_proto_context_commands.group
    ~desc:"Batch transfer tokens."
    (args14
       fee_arg
       dry_run_switch
       verbose_signing_switch
       gas_limit_arg
       storage_limit_arg
       counter_arg
       no_print_source_flag
       minimal_fees_arg
       minimal_nanotez_per_byte_arg
       minimal_nanotez_per_gas_unit_arg
       force_low_fee_arg
       fee_cap_arg
       burn_cap_arg
       batch_size_arg)
    ( prefixes ["dune"; "batch"; "transfer"]
    @@ tez_param ~name:"total" ~desc:"total amount taken from source"
    @@ prefix "from"
    @@ ContractAlias.destination_param
         ~name:"src"
         ~desc:"name of the source contract"
    @@ prefix "to"
    @@ string
         ~name:"file"
         ~desc:
           "JSON file with destinations and amounts\n\
            of the form\n\
            [ {\"address\": \"dn1...\", \"amount\": \"10000\", \"arg\": \
            \"Unit\"}, ... ]\n\
            The field \"amount\" is given in mutez, and \"arg\" is optional."
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           no_print_source,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap,
           batch_size )
         total
         (_, source)
         destination_filename
         cctxt ->
      source_to_keys cctxt ~chain:cctxt#chain ~block:cctxt#block source
      >>=? fun (src_pk, src_sk) ->
      let fee_parameter =
        {
          Injection.minimal_fees;
          minimal_nanotez_per_byte;
          minimal_nanotez_per_gas_unit;
          force_low_fee;
          fee_cap;
          burn_cap;
        }
      in
      read_destinations_file destination_filename
      >>=? fun destinations ->
      Client_proto_dune_context.batch_transfer
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        ?confirmations:cctxt#confirmations
        ~dry_run
        ~verbose_signing
        ~fee_parameter
        ?batch_size
        ~source
        ?fee
        ~src_pk
        ~src_sk
        ~destinations
        ~total
        ?gas_limit
        ?storage_limit
        ?counter
        ()
      >>= report_michelson_errors
            ~no_print_source
            ~msg:"transfer simulation failed"
            cctxt
      >>= function
      | None -> return_unit | Some (_res, _contracts) -> return_unit)

let protocol_group =
  {Clic.name = "Dune Protocol"; title = "Dune Protocol specific commands"}

let file_parameter =
  Clic.parameter (fun _ p ->
      if not (Sys.file_exists p) then failwith "File doesn't exist: '%s'" p
      else return p)

let activate_protocol_command =
  command
    ~group:protocol_group
    ~desc:"Activate a protocol"
    Client_proto_context_commands.(
      args12
        fee_arg
        dry_run_switch
        verbose_signing_switch
        gas_limit_arg
        storage_limit_arg
        counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    ( prefixes ["dune"; "activate"; "protocol"]
    @@ proto_param ~name:"version" ~desc:"Protocol version (b58check)"
    @@ prefixes ["with"; "key"]
    @@ ContractAlias.destination_param
         ~name:"activator"
         ~desc:"Activator's key"
    @@ prefixes ["and"; "parameters"]
    @@ param
         ~name:"parameters"
         ~desc:"Protocol parameters (as JSON file)"
         file_parameter
    @@ prefixes ["at"; "level"]
    @@ param
         ~name:"level"
         ~desc:"Level"
         (parameter (fun _ctx s ->
              try return (Int32.of_string s)
              with _ -> failwith "%s is not an int32 value" s))
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap )
         protocol
         (_, source)
         param_json_file
         level
         (cctxt : Alpha_client_context.full) ->
      source_to_keys cctxt ~chain:cctxt#chain ~block:cctxt#block source
      >>=? fun (src_pk, src_sk) ->
      Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
      (* TODO Dune: check whether `Shell_services.Protocol.list cctxt`
         contains the protocol *)
      >>=? fun json ->
      match
        Data_encoding.Json.destruct Dune_parameters.parameters_encoding json
      with
      | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
          fail
            (failure
               "Invalid parameters: %a %a"
               (fun ppf -> Data_encoding.Json.print_error ppf)
               exn
               Data_encoding.Json.pp
               json)
      | protocol_parameters ->
          let fee_parameter =
            {
              Injection.minimal_fees;
              minimal_nanotez_per_byte;
              minimal_nanotez_per_gas_unit;
              force_low_fee;
              fee_cap;
              burn_cap;
            }
          in
          Client_proto_dune_context.activate_protocol_operation
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            ?confirmations:cctxt#confirmations
            ~dry_run
            ~verbose_signing
            ~fee_parameter
            ~level
            ~protocol
            ~protocol_parameters
            ~source
            ?fee
            ~src_pk
            ~src_sk
            ?gas_limit
            ?storage_limit
            ?counter
            ()
          >>=? fun _res -> return_unit)

let change_parameters_command =
  command
    ~group:protocol_group
    ~desc:"Change protocol parameters"
    Client_proto_context_commands.(
      args12
        fee_arg
        dry_run_switch
        verbose_signing_switch
        gas_limit_arg
        storage_limit_arg
        counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    ( prefixes ["dune"; "change"; "parameters"]
    @@ param
         ~name:"parameters"
         ~desc:"Protocol parameters (as JSON file)"
         file_parameter
    @@ prefixes ["with"; "key"]
    @@ ContractAlias.destination_param
         ~name:"activator"
         ~desc:"Activator's key"
    @@ prefixes ["at"; "level"]
    @@ param
         ~name:"level"
         ~desc:"Level"
         (parameter (fun _ctx s ->
              try return (Int32.of_string s)
              with _ -> failwith "%s is not an int32 value" s))
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap )
         param_json_file
         (_, source)
         level
         (cctxt : Alpha_client_context.full) ->
      source_to_keys cctxt ~chain:cctxt#chain ~block:cctxt#block source
      >>=? fun (src_pk, src_sk) ->
      Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
      >>=? fun json ->
      match
        Data_encoding.Json.destruct Dune_parameters.parameters_encoding json
      with
      | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
          fail
            (failure
               "Invalid parameters: %a %a"
               (fun ppf -> Data_encoding.Json.print_error ppf)
               exn
               Data_encoding.Json.pp
               json)
      | protocol_parameters ->
          let fee_parameter =
            {
              Injection.minimal_fees;
              minimal_nanotez_per_byte;
              minimal_nanotez_per_gas_unit;
              force_low_fee;
              fee_cap;
              burn_cap;
            }
          in
          Client_proto_dune_context.activate_protocol_operation
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            ?confirmations:cctxt#confirmations
            ~dry_run
            ~verbose_signing
            ~fee_parameter
            ~level
            ~protocol_parameters
            ~source
            ?fee
            ~src_pk
            ~src_sk
            ?gas_limit
            ?storage_limit
            ?counter
            ()
          >>=? fun _res -> return_unit)

let manage_accounts_command =
  command
    ~group:protocol_group
    ~desc:"Manage accounts"
    Client_proto_context_commands.(
      args12
        fee_arg
        dry_run_switch
        verbose_signing_switch
        gas_limit_arg
        storage_limit_arg
        counter_arg
        minimal_fees_arg
        minimal_nanotez_per_byte_arg
        minimal_nanotez_per_gas_unit_arg
        force_low_fee_arg
        fee_cap_arg
        burn_cap_arg)
    ( prefixes ["dune"; "manage"; "accounts"]
    @@ prefixes ["with"; "key"]
    @@ ContractAlias.destination_param
         ~name:"activator"
         ~desc:"Activator's key"
    @@ prefixes ["and"; "parameters"]
    @@ param
         ~name:"parameters"
         ~desc:"Accounts parameters (as JSON file)"
         file_parameter
    @@ stop )
    (fun ( fee,
           dry_run,
           verbose_signing,
           gas_limit,
           storage_limit,
           counter,
           minimal_fees,
           minimal_nanotez_per_byte,
           minimal_nanotez_per_gas_unit,
           force_low_fee,
           fee_cap,
           burn_cap )
         (_, source)
         param_json_file
         (cctxt : Alpha_client_context.full) ->
      source_to_keys cctxt ~chain:cctxt#chain ~block:cctxt#block source
      >>=? fun (src_pk, src_sk) ->
      Tezos_stdlib_unix.Lwt_utils_unix.Json.read_file param_json_file
      >>=? fun json ->
      let protocol_parameters =
        Data_encoding.Binary.to_bytes_exn Data_encoding.json json
      in
      let fee_parameter =
        {
          Injection.minimal_fees;
          minimal_nanotez_per_byte;
          minimal_nanotez_per_gas_unit;
          force_low_fee;
          fee_cap;
          burn_cap;
        }
      in
      Client_proto_dune_context.manage_accounts_operation
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        ?confirmations:cctxt#confirmations
        ~dry_run
        ~verbose_signing
        ~fee_parameter
        ~protocol_parameters
        ~source
        ?fee
        ~src_pk
        ~src_sk
        ?gas_limit
        ?storage_limit
        ?counter
        ()
      >>=? fun _res -> return_unit)

let check_fundraiser_account =
  command
    ~group:Client_proto_context_commands.group
    ~desc:"Activate a fundraiser account."
    no_options
    ( prefixes ["dune"; "check"; "fundraiser"; "account"]
    @@ Client_proto_dune_context.Ed25519_public_key_hash.param
    @@ prefixes ["with"]
    @@ param
         ~name:"code"
         (Clic.parameter (fun _ctx code ->
              protect (fun () ->
                  return (Blinded_public_key_hash.activation_code_of_hex code))))
         ~desc:"Activation code obtained from the Tezos foundation."
    @@ stop )
    (fun () pkh code (cctxt : Alpha_client_context.full) ->
      let blinded_pkh = Blinded_public_key_hash.of_ed25519_pkh code pkh in
      cctxt#message
        "@[Blinded key hash = %a@]@."
        Blinded_public_key_hash.pp
        blinded_pkh
      >>= fun () -> return_unit)

(*
  | Single (Activate_account { id = pkh ; activation_code }) -> begin
    let blinded_pkh =
      Blinded_public_key_hash.of_ed25519_pkh activation_code pkh in
    Commitment.get_opt ctxt blinded_pkh >>=? function
    | None -> fail (Invalid_activation { pkh })
    | Some amount ->
        Commitment.delete ctxt blinded_pkh >>=? fun ctxt ->
        let contract = Contract.implicit_contract (Signature.Ed25519 pkh) in
        Contract.(credit ctxt contract amount) >>=? fun ctxt ->
        return (ctxt, Single_result (Activate_account_result
                                       [ Contract contract, Credited amount ]))
*)

let sign_command =
  command
    ~desc:"Sign an operation."
    (args1 out_arg)
    ( prefixes ["sign"; "operation"]
    @@ param
         ~name:"op"
         (Clic.parameter (fun cctxt s ->
              let ignore_new_lines s =
                String.split '\n' s |> String.concat ""
              in
              match String.split ~limit:1 ':' s with
              | ["file"; s] ->
                  cctxt#read_file s >>|? ignore_new_lines
              | ["hex"; s] ->
                  return s
              | _ -> (
                try
                  Hex.to_bytes (`Hex s) |> ignore ;
                  return s
                with Invalid_argument _ -> (
                  cctxt#read_file s
                  >>= function
                  | Ok res ->
                      return (ignore_new_lines res)
                  | Error _ ->
                      failwith
                        "%S is neither a file nor a valid hexadecimal value"
                        s ) )))
         ~desc:"Serialized operation in hexadecimal (in a file or directly)."
    @@ prefix "for"
    @@ ContractAlias.destination_param
         ~name:"signer"
         ~desc:"name of the signer"
    @@ stop )
    (fun out op (_, src) (cctxt : #Client_context.full) ->
      Client_proto_context.get_manager
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        src
      >>=? fun (_src_name, src_pkh, _src_pk, sk) ->
      let bytes = Hex.to_bytes (`Hex op) in
      let (_shell, Contents_list contents) =
        Data_encoding.Binary.of_bytes_exn Operation.unsigned_encoding bytes
      in
      Shell_services.Chain.chain_id cctxt ~chain:cctxt#chain ()
      >>=? fun chain_id ->
      let watermark =
        match contents with
        | Single (Endorsement _) ->
            Signature.(Endorsement chain_id)
        | _ ->
            Signature.Generic_operation
      in
      Injection.simulate cctxt ~chain:cctxt#chain ~block:cctxt#block contents
      >>= report_michelson_errors
            ~exit_on_error:false
            ~msg:"simulation failed"
            cctxt
      >>= (function
            | None ->
                Lwt.return_unit
            | Some (_oph, _op, result) ->
                cctxt#message
                  "@[<v 2>Parsed operation:@,%a@]"
                  Operation_result.pp_operation_result
                  (contents, result.contents))
      >>= fun () ->
      ( match contents with
      | Single (Manager_operation {source; _}) ->
          Client_proto_contracts.get_manager
            cctxt
            ~chain:cctxt#chain
            ~block:cctxt#block
            source
          >>=? fun manager ->
          if Signature.Public_key_hash.(manager <> src_pkh) then
            cctxt#warning
              "Signing an operation whose source manager is %a for %a will \
               fail when injecting."
              Signature.Public_key_hash.pp
              manager
              Signature.Public_key_hash.pp
              src_pkh
            >>= return
          else return_unit
      | _ ->
          return_unit )
      >>=? fun () ->
      Dune_injection.confirm
        cctxt
        ~prompt:"Would you like to sign this operation?"
        ~msg:"Signing was rejected by the user."
      >>=? fun () ->
      Client_keys.sign
        cctxt
        ~watermark
        ~interactive:(cctxt :> Client_context.io_wallet)
        sk
        bytes
      >>=? fun signature ->
      cctxt#message "Signature: %a" Signature.pp signature
      >>= fun () ->
      let signed_bytes =
        Bytes.concat (Bytes.of_string "") [bytes; Signature.to_bytes signature]
      in
      ( match out with
      | None ->
          cctxt#message
            "@[<v>Signed operation:@,%a@]"
            Hex.pp
            (Hex.of_bytes signed_bytes)
      | Some f ->
          let (`Hex hex) = Hex.of_bytes signed_bytes in
          let oc = open_out f in
          output_string oc hex ;
          close_out oc ;
          cctxt#message "@[<v>Serialized signed operation written to %s@]" f )
      >>= return)

let inject_command =
  command
    ~desc:"Inject an operation."
    (args1 no_print_source_flag)
    ( prefixes ["inject"; "operation"]
    @@ param
         ~name:"op"
         (Clic.parameter (fun cctxt s ->
              let ignore_new_lines s =
                String.split '\n' s |> String.concat ""
              in
              match String.split ~limit:1 ':' s with
              | ["file"; s] ->
                  cctxt#read_file s >>|? ignore_new_lines
              | ["hex"; s] ->
                  return s
              | _ -> (
                try
                  Hex.to_bytes (`Hex s) |> ignore ;
                  return s
                with Invalid_argument _ -> (
                  cctxt#read_file s
                  >>= function
                  | Ok res ->
                      return (ignore_new_lines res)
                  | Error _ ->
                      failwith
                        "%S is neither a file nor a valid hexadecimal value"
                        s ) )))
         ~desc:"Serialized operation in hexadecimal (in a file or directly)."
    @@ stop )
    (fun no_print_source op (cctxt : #Client_context.full) ->
      let bytes = Hex.to_bytes (`Hex op) in
      let op = Data_encoding.Binary.of_bytes_exn Operation.encoding bytes in
      let (Operation_data protocol_data) = op.protocol_data in
      Injection.simulate
        cctxt
        ~chain:cctxt#chain
        ~block:cctxt#block
        protocol_data.contents
      >>=? fun (_oph, _op, result) ->
      cctxt#message
        "@[<v 2>Parsed operation:@,%a@]"
        Operation_result.pp_operation_result
        (protocol_data.contents, result.contents)
      >>= fun () ->
      Dune_injection.confirm
        cctxt
        ~prompt:"Would you like to inject this operation?"
        ~msg:"Injection was rejected by the user."
      >>=? fun () ->
      Injection.force_inject_operation
        cctxt
        ~chain:cctxt#chain
        ?confirmations:cctxt#confirmations
        ~preapply_result:result
        {shell = op.shell; protocol_data}
      >>= report_michelson_errors
            ~no_print_source
            ~msg:"injection failed"
            cctxt
      >>= fun _ -> return_unit)

let commands () =
  [ batch_transfer_command;
    activate_protocol_command;
    change_parameters_command;
    manage_accounts_command;
    check_fundraiser_account;
    sign_command;
    inject_command ]
