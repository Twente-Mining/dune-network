(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Tezos_micheline (* For Micheline_parser *)

open Dune_lang_v1_parser

let print_expr fmt = function
  | Script.Michelson_expr expr ->
      Michelson_v1_printer.print_expr fmt expr
  | _ ->
      assert false

(* TODO Dune prio 10 *)

let print_expr_unwrapped fmt = function
  | Script.Michelson_expr expr ->
      Michelson_v1_printer.print_expr_unwrapped fmt expr
  | _ ->
      assert false

(* TODO Dune prio 10 *)

let print_execution_trace = Michelson_v1_printer.print_execution_trace

let print_with_injected_types type_map fmt parsed =
  match parsed.original with
  | Michelson_parsed parsed ->
      let program = Michelson_v1_printer.inject_types type_map parsed in
      Micheline_printer.print_expr fmt program
  | _ ->
      assert false

(* TODO Dune prio 10 *)

let unparse_toplevel ?type_map expr =
  match expr with
  | Script.Michelson_expr expr ->
      let original = Michelson_v1_printer.unparse_toplevel ?type_map expr in
      {
        source = original.source;
        expanded = Script.Michelson_expr original.expanded;
        original = Michelson_parsed original;
      }
  | _ ->
      assert false

(* TODO Dune prio 10 *)
