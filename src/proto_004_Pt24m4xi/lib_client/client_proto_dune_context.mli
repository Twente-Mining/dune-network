(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Tezos_micheline
open Dune_lang_more (* Michelson_v1_* = Dune_lang_v1_* *)

open Dune_injection

type program =
  | Program of Michelson_v1_parser.parsed Micheline_parser.parsing_result
  | Program_hash of Script_expr_hash.t
  | Contract_hash of Contract.t

module Program_or_hash : sig
  val source_param :
    ?name:string ->
    ?desc:string ->
    ('a, (#Client_context.wallet as 'wallet)) Clic.params ->
    (program -> 'a, 'wallet) Clic.params
end

module Ed25519_public_key_hash : sig
  val param :
    ?name:string ->
    ?desc:string ->
    ('a, (#Client_context.wallet as 'wallet)) Clic.params ->
    (Ed25519.Public_key_hash.t -> 'a, 'wallet) Clic.params
end

val batch_transfer :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:public_key ->
  src_sk:Client_keys.sk_uri ->
  destinations:(Contract.t * Tez.t * Z.t option * string option) list ->
  total:Tez.t ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  ?batch_size:int ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  (Apply_results.packed_contents_result list * Contract.t list) tzresult Lwt.t

val activate_protocol_operation :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->
  level:int32 ->
  ?protocol:Protocol_hash.t ->
  ?protocol_parameters:Dune_parameters.parameters ->
  ?fee:Tez.tez ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  ( ( Operation_hash.t
    * Kind.dune_manager_operation Kind.manager contents
    * Kind.dune_manager_operation Kind.manager
      Protocol.Apply_results.contents_result )
  * Contract.t list )
  tzresult
  Lwt.t

val manage_accounts_operation :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->
  protocol_parameters:MBytes.t ->
  ?fee:Tez.tez ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  unit ->
  ( ( Operation_hash.t
    * Kind.dune_manager_operation Kind.manager contents
    * Kind.dune_manager_operation Kind.manager
      Protocol.Apply_results.contents_result )
  * Contract.t list )
  tzresult
  Lwt.t

val forge_set_delegate :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?fee:Tez.t ->
  Contract.t ->
  src_pk:public_key ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  public_key_hash option ->
  Kind.delegation Kind.manager forge_result tzresult Lwt.t

val forge_register_as_delegate :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?fee:Tez.t ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  public_key ->
  Kind.delegation Kind.manager forge_result tzresult Lwt.t

val forge_originate_account :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:public_key ->
  manager_pkh:public_key_hash ->
  ?delegatable:bool ->
  ?delegate:public_key_hash ->
  balance:Tez.tez ->
  ?fee:Tez.t ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  unit ->
  (MBytes.t * Contract.t option) tzresult Lwt.t

val forge_originate_contract :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  delegate:public_key_hash option ->
  ?delegatable:bool ->
  ?spendable:bool ->
  initial_storage:string ->
  manager:public_key_hash ->
  balance:Tez.t ->
  source:Contract.t ->
  src_pk:public_key ->
  ?code_hash:Script_expr_hash.t ->
  ?code:Script.expr ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  unit ->
  (MBytes.t * Contract.t option) tzresult Lwt.t

val forge_transfer :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:public_key ->
  destination:Contract.t ->
  ?arg:string ->
  amount:Tez.t ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:Injection.fee_parameter ->
  ?collect_call:collect_call ->
  ?forge_file:string ->
  unit ->
  Kind.transaction Kind.manager forge_result tzresult Lwt.t

val forge_reveal :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:public_key ->
  ?fee:Tez.t ->
  fee_parameter:Injection.fee_parameter ->
  ?forge_file:string ->
  unit ->
  Kind.reveal Kind.manager forge_result tzresult Lwt.t
