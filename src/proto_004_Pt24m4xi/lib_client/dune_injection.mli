(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context
open Apply_results
open Injection

type 'kind forge_result = MBytes.t * 'kind contents_result option

val confirm :
  #Client_context.io -> msg:string -> prompt:string -> unit tzresult Lwt.t

val inject_batch_manager_operations :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  ?confirmations:int ->
  ?dry_run:bool ->
  ?verbose_signing:bool ->
  source:Contract.t ->
  src_pk:Signature.public_key ->
  src_sk:Client_keys.sk_uri ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:fee_parameter ->
  packed_manager_operation list ->
  (Operation_hash.t * packed_contents_result list * counter) tzresult Lwt.t

val forge_operation :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  fee_parameter:fee_parameter ->
  ?compute_fee:bool ->
  ?forge_file:string ->
  'kind contents_list ->
  (MBytes.t * 'kind contents_result_list option) tzresult Lwt.t

val forge_manager_operation :
  #Alpha_client_context.full ->
  chain:Shell_services.chain ->
  block:Shell_services.block ->
  ?branch:int ->
  source:Contract.t ->
  src_pk:Signature.public_key ->
  ?fee:Tez.t ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  ?counter:Z.t ->
  fee_parameter:fee_parameter ->
  ?forge_file:string ->
  'kind manager_operation ->
  'kind Kind.manager forge_result tzresult Lwt.t
