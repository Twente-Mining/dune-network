(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context

type original = Michelson_parsed of Michelson_v1_parser.parsed | Dune_parsed

type parsed = {source : string; expanded : Script.expr; original : original}

let parse_toplevel ?check src =
  (* TODO Dune prio 10 *)
  let (v, errors) = Michelson_v1_parser.parse_toplevel ?check src in
  ( {
      source = v.source;
      expanded = Script.Michelson_expr v.expanded;
      original = Michelson_parsed v;
    },
    errors )

let parse_expression ?check src =
  (* TODO Dune prio 10 *)
  let (v, errors) = Michelson_v1_parser.parse_expression ?check src in
  ( {
      source = v.source;
      expanded = Script.Michelson_expr v.expanded;
      original = Michelson_parsed v;
    },
    errors )
