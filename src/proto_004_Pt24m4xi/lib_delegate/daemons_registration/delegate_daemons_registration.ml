(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Protocol
open Alpha_context

let map_raw_level block_stream =
  return
    (Lwt_stream.map_s
       (function
         | Error _ as err ->
             Lwt.return err
         | Ok
             { Dune_delegate_daemons.Client_baking_blocks.hash;
               chain_id;
               predecessor;
               fitness;
               timestamp;
               protocol;
               next_protocol;
               proto_level;
               level;
               context } -> (
           match Raw_level.of_int32 level with
           | Ok level ->
               return
                 {
                   Client_baking_blocks.hash;
                   chain_id;
                   predecessor;
                   fitness;
                   timestamp;
                   protocol;
                   next_protocol;
                   proto_level;
                   level;
                   context;
                 }
           | Error _ ->
               failwith "Cannot convert level into int32" ))
       block_stream)

open Dune_delegate_daemons
open Delegate_daemons

let minimal_fees_to_tez = function
  | None ->
      return_none
  | Some s -> (
    match Tez.of_string s with
    | Some t ->
        return_some t
    | None ->
        failwith "Bad minimal fees %s" s )

let () =
  Baker.Daemons.register
    Protocol.hash
    (fun cctxt
         ~chain
         { Baker.minimal_fees;
           minimal_nanotez_per_gas_unit;
           minimal_nanotez_per_byte;
           await_endorsements;
           max_priority;
           context_path;
           delegates;
           lazy_baker = _ }
         block_stream
         ->
      minimal_fees_to_tez minimal_fees
      >>=? fun minimal_fees ->
      map_raw_level block_stream
      >>=? fun block_stream ->
      Client_baking_forge.create
        (new Alpha_client_context.wrap_full cctxt)
        ?minimal_fees
        ?minimal_nanotez_per_gas_unit
        ?minimal_nanotez_per_byte
        ?await_endorsements
        ?max_priority
        ~chain
        ~context_path
        delegates
        block_stream)

let () =
  Endorser.Daemons.register
    Protocol.hash
    (fun cctxt ~chain:_ {Endorser.delay; delegates} block_stream ->
      map_raw_level block_stream
      >>=? fun block_stream ->
      Client_baking_endorsement.create
        (new Alpha_client_context.wrap_full cctxt)
        ~delay
        delegates
        block_stream)

let () =
  Accuser.Daemons.register
    Protocol.hash
    (fun cctxt ~chain:_ {Accuser.preserved_levels} block_stream ->
      map_raw_level block_stream
      >>=? fun block_stream ->
      Client_baking_denunciation.create
        (new Alpha_client_context.wrap_full cctxt)
        ~preserved_levels
        block_stream)

module EvL = Internal_event.Legacy_logging.Make_semantic (struct
  let name = Protocol.name ^ ".unknown_handler"
end)

open EvL

let () =
  let unknown_handler name cctxt block_stream =
    Client_baking_scheduling.main
      ~name
      ~cctxt:(new Alpha_client_context.wrap_full cctxt)
      ~stream:block_stream
      ~state_maker:(fun _ -> return_unit)
      ~pre_loop:(fun _ _ _ -> return_unit)
      ~compute_timeout:(fun _ -> Lwt_utils.never_ending ())
      ~timeout_k:(fun _ _ () -> return_unit)
      ~event_k:(fun _ () bi ->
        lwt_log_error
          Tag.DSL.(
            fun f ->
              f "Unknown next protocol %a for block %a"
              -% t event "unknown_protocol"
              -% a
                   Protocol_hash.Logging.tag
                   bi.Client_baking_blocks.next_protocol
              -% a Block_hash.Logging.tag bi.Client_baking_blocks.hash)
        >>= return)
  in
  Baker.Daemons.register_unknown_handler unknown_handler ;
  Endorser.Daemons.register_unknown_handler unknown_handler ;
  Accuser.Daemons.register_unknown_handler unknown_handler
