(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module MapLang = Dune_script_sig.MapLang

module type S = sig
  module Repr : Dune_script_repr.S
  module Interpreter : Dune_script_interpreter_sig.S
    with module Repr := Repr.Internal
end

let scripts = ref MapLang.empty

module Make0
    (Internal : Dune_script_sig.S)
    (R : Dune_script_repr.S with module Internal := Internal)
    (I : Dune_script_interpreter_sig.S with module Repr := Internal) : S =
struct
  module Repr = struct
    module Internal = Internal
    include R
  end
  module Interpreter = I
end

module Make
    (Internal : Dune_script_sig.S)
    (R : Dune_script_repr.S with module Internal := Internal)
    (I : Dune_script_interpreter_sig.S with module Repr := Internal) : S =
struct

  module M = Make0 (Internal) (R) (I)
  include M

  let () =
    scripts := MapLang.add R.lang (module M : S) !scripts

end

let get_module lang =
  match MapLang.find_opt lang !scripts with
  | None ->
      let { Dune_script_sig.name = n; version = v, x } = lang in
      failwith (Format.sprintf "Unknown language %s.%d.%d" n v x)
  | Some language -> language
