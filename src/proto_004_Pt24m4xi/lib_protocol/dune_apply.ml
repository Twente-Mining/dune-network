(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* The operation manage_protocol can be used to:
   * Change the protocol
   * Change some protocol parameters
   * Add new commitments (Tezos ICO)
   * Change the balance of some accounts (maybe we should disable this ?)
   All these operations will happen at the beginning of the specified level.
*)


open Alpha_context (* Dune_storage *)
open Apply_results
open Dune_operation

type error +=
    Wrong_activation_level of int32 (* `Permanent *)
type error +=
    Activation_level_should_be_last_of_cycle of int32 (* `Permanent *)

let () =
  register_error_kind
    `Permanent
    ~id:"operation.wrong_activation_level"
    ~title:"Wrong level in Activate Protocol"
    ~description:"The Activate Protocol operation comes too late."
    ~pp:(fun ppf level ->
        Format.fprintf ppf "Wrong level %ld for Activate Protocol." level
      )
    Data_encoding.(obj1 (req "level" int32))
    (function Wrong_activation_level level -> Some level | _ -> None)
    (fun level -> Wrong_activation_level level) ;
  register_error_kind
    `Permanent
    ~id:"operation.activation_level_should_be_last"
    ~title:"Level in Activate Protocol should be the last level of the cycle"
    ~description:"Some parameters that can only be changed \
                  at cycle end were changed"
    ~pp:(fun ppf level ->
        Format.fprintf ppf
          "Cannot change parameters at level %ld these parameters, \
           should be changed at the last level of a cycle" level
      )
    Data_encoding.(obj1 (req "level" int32))
    (function
      | Activation_level_should_be_last_of_cycle level -> Some level
      | _ -> None)
    (fun level -> Activation_level_should_be_last_of_cycle level) ;
  ()


let finalize_block ctxt =

  (* Check if we want to activate a new protocol *)
  begin
    let level = Alpha_context.Level.current ctxt in
    let level = Raw_level.to_int32 level.level in
    Dune_storage.get_activate_protocol_level ctxt >>=? function
    | None -> return ctxt
    | Some at_level ->
        if Int32.equal level at_level then

          (* First, activate protocol: just write the new protocol hash
             in the context, nothing more ! *)
          Dune_storage.get_activate_protocol_and_cleanup ctxt >>=? fun
            ( ctxt, protocol, protocol_parameters ) ->
          begin
            match protocol with
            | None -> Lwt.return ctxt
            | Some protocol ->
                match Source.hash with
                | None -> assert false
                | Some proto_hash ->
                    (* activate only if the current protocol executing this
                       code is not the target protocol. We also increase
                       the revision by 1 (it might be overloaded just
                       after if a revision is explicitely specified). *)
                    if Protocol_hash.( proto_hash <> protocol ) then
                      Alpha_context.activate ctxt protocol >>= fun ctxt ->
                      Dune_storage.set_protocol_revision ctxt
                        (Dune_storage.protocol_revision ctxt + 1)
                    else
                      Lwt.return ctxt
          end >>= fun ctxt ->

          (* Patch modified constants *)
          begin match protocol_parameters with
            | None -> Lwt.return ctxt
            | Some protocol_parameters ->
                Dune_storage.patch_constants ctxt
                  (Dune_parameters.patch_constants protocol_parameters)
          end >>= fun ctxt ->

          (* Set protocol revision *)
          begin match protocol_parameters with
            | None | Some { protocol_revision = None } -> return ctxt
            | Some { protocol_revision = Some revision } ->
                Dune_storage.set_protocol_revision ctxt revision >>= fun ctxt ->
                return ctxt
          end
        else
          return ctxt
  end >>=? fun ctxt ->
  return ctxt

(* check that the Foundation is triggering the change *)
let check_source_foundation ctxt source =
  Dune_storage.get_foundation_pubkey ctxt >>= fun pubkey ->
  match Contract.is_implicit source with
  | None -> fail (Operation_repr.Invalid_signature)
  | Some source ->
      if Signature.Public_key_hash.equal
          (Signature.Public_key.hash pubkey) source then
        return ()
      else
        fail (Operation_repr.Invalid_signature)

let apply_dune_manager_operation_content
    ~apply_manager_operation_content:_
    ctxt ~since _mode ~payer:_ ~source ~internal op =
  let _before_operation =
    (* This context is not used for backtracking. Only to compute
       gas consumption and originations for the operation result. *)
    ctxt in
  let _spend =
    (* Ignore the spendable flag for smart contracts. *)
    if internal then Contract.spend_from_script else Contract.spend in

  (* 1. source already checked against foundation key in pre-check *)
  match op with
  | Dune_manage_accounts bytes ->

      begin
        (* 2. check protocol parameters *)
        match Data_encoding.Binary.of_bytes Data_encoding.json bytes with
        | None -> fail (Raw_context.Failed_to_parse_parameter bytes)
        | Some json ->
            match Data_encoding.Json.destruct
                    Dune_parameters.accounts_encoding json with
            | exception (Data_encoding.Json.Cannot_destruct _ as exn) ->
                Format.kasprintf
                  failwith "Invalid accounts: %a %a"
                  (fun ppf -> Data_encoding.Json.print_error ppf) exn
                  Data_encoding.Json.pp json
            | param ->

                (* A commitment with an amount of 0 means it should be
                   removed. For every commitment added or removed, the
                   balance of the source (dictator) is either debited
                   or credited, so the supply is constant. *)
                let balance_updates = [] in
                fold_left_s (fun ( ctxt, balance_updates )
                              { Commitment.blinded_public_key_hash = bkh ;
                                amount } ->
                              Commitment.get_opt ctxt bkh >>=?
                              function
                              | Some prev_amount ->
                                  if Tez.(amount = zero) then
                                    Commitment.delete ctxt bkh >>=? fun ctxt ->
                                    Contract.credit ctxt
                                      source prev_amount >>=? fun ctxt ->
                                    let balance_updates =
                                      ( Delegate.Contract source,
                                        Delegate.Credited prev_amount ) ::
                                      balance_updates in
                                    return ( ctxt, balance_updates )
                                  else
                                    failwith "Commitment already included"
                              | None ->
                                  Commitment.init_set ctxt bkh amount
                                  >>=? fun ctxt ->
                                  Contract.spend_from_script ctxt
                                    source amount >>=? fun ctxt ->
                                  let balance_updates =
                                    ( Delegate.Contract source,
                                      Delegate.Debited amount ) ::
                                    balance_updates in
                                  return ( ctxt, balance_updates )
                            )
                  ( ctxt, balance_updates )
                  param.commitments >>=? fun ( ctxt, balance_updates ) ->

                begin
                  match param.change_foundation_pubkey with
                  | None -> return ctxt
                  | Some pubkey ->
                      Dune_storage.set_foundation_pubkey ctxt pubkey >>= return
                end >>=? fun ctxt ->

                Lwt_list.fold_left_s
                  (fun ctxt (key_hash, is_foundation_baker) ->
                     let c = Contract.implicit_contract key_hash in
                     Contract.change_foundation_baker ctxt c
                       is_foundation_baker
                  ) ctxt param.change_foundation_bakers >>= fun ctxt ->
                return ( ctxt, balance_updates )

      end >>=? fun ( ctxt, balance_updates ) ->

      (* 4. returns everything *)
      return (ctxt,
              Dune_manager_operation_result {
                consumed_gas = Gas.consumed ~since ~until:ctxt;
                originated_contracts = [] ;
                paid_storage_size_diff = Z.zero;
                balance_updates ;
                result = Dune_apply_results.Dune_manage_accounts_result;
              }, [])


  | Dune_activate_protocol { level ; protocol ; protocol_parameters } ->

      (* 1. check level *)
      let next_level =
        Raw_level.to_int32 (Level.current ctxt).level
        |> Int32.add 1l in
      fail_when Compare.Int32.(level <= next_level)
        (Wrong_activation_level level)
      >>=? fun () ->

      (* 2.1. check protocol revision *)
      begin match protocol_parameters with
        | None | Some { protocol_revision = None } -> return_unit
        | Some { protocol_revision = Some revision } ->
            let prev_rev = Dune_storage.protocol_revision ctxt in
            if Compare.Int.(revision <= prev_rev) then
              failwith
                "Cannot set protocol revision to a non-increasing value"
            else return_unit
      end >>=? fun () ->

      (* 2.2. check modifications that can happen only at cycle change *)
      let need_to_wait_for_end_of_cycle () =
        Raw_level.of_int32 level >|? Level.from_raw ctxt |> Lwt.return
        >>=? fun activate_level ->
        let last_of_level_cyle =
          (Level.last_level_in_cycle ctxt activate_level.cycle).level
          |> Raw_level.to_int32 in
        fail_unless
          Compare.Int32.(level = last_of_level_cyle)
          (Activation_level_should_be_last_of_cycle level)
      in
      begin match protocol_parameters with
        | Some { tokens_per_roll = Some n } when
            Tez.(n <> Constants.tokens_per_roll ctxt)
          -> failwith "Cannot change token_per_roll"
        | Some { preserved_cycles = Some n } when
            Compare.Int.(n <> Constants.preserved_cycles ctxt)
          -> failwith "Cannot change preserved_cycle"
        (* Remaining ones must be done at end of cycle only *)
        | Some { blocks_per_cycle = Some n } when
            Compare.Int32.(n <> Constants.blocks_per_cycle ctxt) ->
            need_to_wait_for_end_of_cycle ()
        | Some { blocks_per_commitment = Some n } when
            Compare.Int32.(n <> Constants.blocks_per_commitment ctxt) ->
            need_to_wait_for_end_of_cycle ()
        | Some { blocks_per_roll_snapshot = Some n } when
            Compare.Int32.(n <> Constants.blocks_per_roll_snapshot ctxt) ->
            need_to_wait_for_end_of_cycle ()
        | Some { blocks_per_voting_period = Some n } when
            Compare.Int32.(n <> Constants.blocks_per_voting_period ctxt) ->
            need_to_wait_for_end_of_cycle ()
        | _ -> return_unit
      end >>=? fun () ->

      (* 3. update the storage *)
      Dune_storage.set_activate_protocol ctxt
        ?protocol ?protocol_parameters
        level
      >>= fun ctxt ->

      (* 4. returns everything *)
      return (ctxt,
              Dune_manager_operation_result {
                consumed_gas = Gas.consumed ~since ~until:ctxt;
                originated_contracts = [] ;
                paid_storage_size_diff = Z.zero;
                balance_updates = [];
                result = Dune_apply_results.Dune_activate_protocol_result;
              }, [])
