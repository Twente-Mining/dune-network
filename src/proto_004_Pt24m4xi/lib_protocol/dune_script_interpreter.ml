(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Alpha_context
open Script_interpreter

open Script

let typecheck_code ctxt = function
  | Michelson_expr expr ->
      Script_ir_translator.typecheck_code ctxt expr
  | Dune_code _code ->
     assert false
  | _ -> assert false (* TODO Dune prio 4 *)


let typecheck_data ctxt = function
  | Michelson_expr data, Michelson_expr ty ->
      Script_ir_translator.typecheck_data ctxt (data, ty)
  | Dune_expr _data, Dune_expr _type_expr ->
     assert false
  | _ -> assert false (* TODO Dune prio 4 *)

let normalize_script ctxt script =
  force_decode ctxt script.code >>=? fun (code, ctxt) ->
  force_decode ctxt script.storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      normalize_script ctxt { code ; storage } >>=? fun (script, ctxt) ->
      return (Script_michelson.to_canonical_script script, ctxt)
  | _ -> assert false (* TODO Dune prio 1 *)

let denormalize_script ctxt { code ; storage ; fee_code } =
  match fee_code with
  | None -> return ({ code ; storage }, ctxt)
  | Some fee_code ->
      force_decode ctxt code >>=? fun (code, ctxt) ->
      force_decode ctxt storage >>=? fun (storage, ctxt) ->
      force_decode ctxt fee_code >>=? fun (fee_code, ctxt) ->
      match code, storage, fee_code with
      | Michelson_expr code, Michelson_expr storage, Michelson_expr fee_code ->
          let open Script_ir_translator in
          denormalize_script ctxt { code ; storage ; fee_code = Some fee_code }
          >>=? fun (script, ctxt) ->
          return (Script_michelson.to_script script, ctxt)
      | _ -> assert false (* TODO Dune prio 1 *)

let typecheck ctxt script =
  Script.force_decode ctxt script.storage >>=? fun (storage, ctxt) -> (* see [note] *)
  Lwt.return (Gas.consume ctxt (Script.deserialized_cost storage)) >>=? fun ctxt ->
  Script.force_decode ctxt script.code >>=? fun (code, ctxt) -> (* see [note] *)
  Lwt.return (Gas.consume ctxt (Script.deserialized_cost code)) >>=? fun ctxt ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      Script_ir_translator.parse_script ctxt { code ; storage } >>=?
      fun (ex_script, ctxt) ->
      Script_ir_translator.big_map_initialization ctxt Optimized ex_script
      >>=? fun (big_map_diff, ctxt) ->
      Script_ir_translator.normalize_script ctxt { code ; storage } >>=? fun (script, ctxt) ->
      return ( ( Script_michelson.to_canonical_script script, big_map_diff ), ctxt)
  | Dune_code code, Dune_expr storage ->
     let module Lang =
       (val (Script_dune.module_of_code code) : Script_dune.S) in
     let module Lang =
       (val (Dune_script_interpreter_registration.get_module Lang.lang) :
          Dune_script_interpreter_registration.S) in
     begin match code, storage with
     | Lang.Repr.Code code, Lang.Repr.Const storage ->
        Lang.Interpreter.typecheck ctxt code storage
        >>=? fun ((code, storage, fee_code), ctxt) ->
        return (({ code = Script_all.lazy_expr
                            (Dune_code (Lang.Repr.Code code));
                   storage = Script_all.lazy_expr
                               (Dune_expr (Lang.Repr.Const storage));
                   fee_code = Option.map ~f:(fun c -> Script_all.lazy_expr
                               (Dune_code (Lang.Repr.Code c))) fee_code },
                 None), ctxt)
     | _ -> failwith "booh"
     end
  | _ ->
      normalize_script ctxt script >>=? fun (script, ctxt) ->
      return ( (script, None ), ctxt) (* TODO Dune prio 15 *)

let readable_storage ctxt ~code ~storage =
  force_decode ctxt code >>=? fun (code, ctxt) ->
  force_decode ctxt storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      parse_script ctxt { code ; storage } >>=? fun (Ex_full_script script, ctxt) ->
      unparse_script ctxt Readable script >>=? fun (script, _ctxt) ->
      return (Michelson_expr script.storage)
  | _ -> return storage (* TODO Dune prio 1 *)

let readable_script ctxt script =
  denormalize_script ctxt script >>=? fun (script, ctxt) ->
  force_decode ctxt script.code >>=? fun (code, ctxt) ->
  force_decode ctxt script.storage >>=? fun (storage, ctxt) ->
  match code, storage with
  | Michelson_expr code, Michelson_expr storage ->
      let open Script_ir_translator in
      parse_script ctxt { code ; storage } >>=? fun (Ex_full_script script, ctxt) ->
      unparse_script ctxt Readable script >>=? fun (script, _ctxt) ->
      return (Script_michelson.to_script script)
  | _ -> return script (* TODO Dune prio 1 *)

let execute ctxt mode ~source ~payer
    ~self ~code ~storage ~parameter
    ~collect_call ~amount
    ~apply_manager_operation_content
  =
  match code, storage, parameter with
  | Michelson_expr code,
    Michelson_expr storage,
    Michelson_expr parameter ->
      Script_interpreter.execute
        ctxt mode ~source ~payer ~self
        ~code ~storage ~parameter ~collect_call ~amount

  | Dune_code code ,
    Dune_expr storage ,
    Dune_expr parameter ->

      let module Lang = (val (Script_dune.module_of_code code)
                          : Script_dune.S ) in
      let module Lang = (val (Dune_script_interpreter_registration.get_module
                                Lang.lang
                             )
                          : Dune_script_interpreter_registration.S) in
      begin
        match code, storage, parameter with
        | Lang.Repr.Code code ,
          Lang.Repr.Const storage ,
          Lang.Repr.Const parameter  ->
            Lang.Interpreter.execute
              ctxt mode ~source ~payer ~self ~code ~storage ~parameter
              ~collect_call ~amount
              ~apply:(fun ctxt ->
                  apply_manager_operation_content ctxt mode
                     ~payer ~source ~internal:true
              )
            >>=? fun { Lang.Interpreter.ctxt ; storage;
                       result=_ ; operations } ->
            let big_map_diff = None in (* TODO Dune *)
            let storage = Dune_expr (Lang.Repr.Const storage) in
            return { ctxt ; storage ; big_map_diff ; operations }
        | _ -> assert false (* TODO Dune : define error *)
      end
  | _ -> assert false (* TODO Dune : translation to/from Michelson exprs  *)

let execute_fee_script ctxt ~source ~payer
    ~self ~fee_code ~storage ~parameter ~amount
  =
  match fee_code, storage, parameter with
  | Michelson_expr fee_code,
    Michelson_expr storage,
    Michelson_expr parameter ->
      Script_interpreter.execute_fee_script
        ctxt ~source ~payer ~self
        ~fee_code ~storage ~parameter ~amount

  | Dune_code fee_code ,
    Dune_expr storage ,
    Dune_expr parameter ->

      let module Lang = (val (Script_dune.module_of_code fee_code)
                          : Script_dune.S ) in
      let module Lang = (val (Dune_script_interpreter_registration.get_module
                                Lang.lang
                             )
                          : Dune_script_interpreter_registration.S) in
      begin
        match fee_code, storage, parameter with
        | Lang.Repr.Code fee_code ,
          Lang.Repr.Const storage ,
          Lang.Repr.Const parameter  ->
            Lang.Interpreter.execute_fee_script
              ctxt ~source ~payer ~self ~fee_code ~storage ~parameter ~amount
            >>=? fun { Lang.Interpreter.ctxt ; max_fee ; max_storage } ->
            return { ctxt ; max_fee ; max_storage }
        | _ -> assert false (* TODO Dune : define error *)
      end
  | _ -> assert false (* TODO Dune : translation to/from Michelson exprs  *)

open Script_all

let trace ctxt mode ~source ~payer ~self ~code ~storage ~parameter ~collect_call ~amount =
  match code, storage, parameter with
  | Michelson_expr code, Michelson_expr storage, Michelson_expr parameter ->
      Script_interpreter.trace ctxt mode ~source ~payer ~self ~code ~storage
        ~parameter ~collect_call ~amount
  | _ -> assert false (* TODO Dune *)
