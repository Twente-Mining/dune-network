(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2019 Origin-Labs                                          *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

(* Fabrice:

   We changed the semantics of `tree`, `mem_tree` and `find_tree`:

   * In Irmin, a `tree` can be either an internal node or a blob. In
   Ironmin, we limited `tree` to only match an internal node, never a
   blob.

   * `mem` returns true if a blob is found. `mem_tree` is supposed to
   be more generic, it returns true whenever something is found, an
   internal node or a blob. In Ironmin, `mem_tree` will only return
   true if an internal node is found.

   * `find` returns a blob when it is found. `find_tree` returns
   anything that is found. In Ironmin, `find_tree` can only return an
   internal node, never a blob.


*)

type metadata = unit

type tree_hash =
  [`Contents of Context_hash.t * metadata | `Node of Context_hash.t]

module type S = sig
  val init : unit -> unit

  module Hash : sig
    type t = Context_hash.t
  end

  module Irmin : sig
    type config

    module Info : sig
      type t

      val v : date:int64 -> author:string -> string -> t

      val date : t -> int64

      val author : t -> string

      val message : t -> string
    end
  end

  module GitStore : sig
    (* In Ironmin, a tree is always a node. In Irmin, it can also be a
       content *)
    type tree

    val empty_tree : unit -> tree

    module Repo : sig
      type t

      val v : Irmin.config -> t Lwt.t
    end

    module Tree : sig
      val hash : Repo.t -> tree -> tree_hash Lwt.t

      val mem : tree -> string list -> bool Lwt.t

      val find : tree -> string list -> Bigstring.t option Lwt.t

      val remove : tree -> string list -> tree Lwt.t

      val add :
        tree -> string list -> ?metadata:metadata -> Bigstring.t -> tree Lwt.t

      (* [find_tree] is supposed to return anything. In Ironmin, we
         only return Some when it is a tree, while Irmin also returns
         Some when it is some content *)
      val mem_tree : tree -> string list -> bool Lwt.t

      val find_tree : tree -> string list -> tree option Lwt.t

      val add_tree : tree -> string list -> tree -> tree Lwt.t

      val list :
        tree -> string list -> (string * [`Contents | `Node]) list Lwt.t
    end

    module Commit : sig
      type t

      val tree : t -> tree Lwt.t

      val of_hash : Repo.t -> Hash.t -> t option Lwt.t

      val v : Repo.t -> info:Irmin.Info.t -> parents:t list -> tree -> t Lwt.t

      val hash : t -> Hash.t

      val info : t -> Irmin.Info.t
    end

    module Branch : sig
      val set : Repo.t -> string -> Commit.t -> unit Lwt.t

      val remove : Repo.t -> string -> unit Lwt.t

      val master : string
    end
  end

  module Irmin_lmdb : sig
    val config :
      ?config:Irmin.config ->
      ?mapsize:int64 ->
      ?readonly:bool ->
      string ->
      Irmin.config
  end

  val hash :
    date:int64 ->
    message:string ->
    parents:GitStore.Commit.t list ->
    tree:GitStore.tree ->
    Hash.t Lwt.t

  val gc : GitStore.Repo.t -> genesis:Hash.t -> current:Hash.t -> bool

  val revert : GitStore.Repo.t -> unit

  val storage_dir : GitStore.Repo.t -> string

  val clear_stats : unit -> unit

  val print_stats : GitStore.Repo.t -> unit

  (* functions for saving snapshots *)
  val fold_tree_path :
    ?progress:(int -> unit) ->
    GitStore.Repo.t ->
    GitStore.tree ->
    ([> `Data of Bigstring.t | `Node of (string * tree_hash) list] ->
    unit Lwt.t) ->
    unit Lwt.t

  val context_parents :
    GitStore.Repo.t -> GitStore.Commit.t -> Hash.t list Lwt.t

  (* functions for restoring snapshots *)
  module MEMCACHE : sig
    type memcache

    val new_memcache : unit -> memcache

    val set_context :
      memcache ->
      GitStore.Repo.t ->
      info:Irmin.Info.t ->
      parents:Hash.t list ->
      node:Hash.t ->
      Hash.t Lwt.t

    val add_mbytes : memcache -> GitStore.Repo.t -> Bigstring.t -> unit Lwt.t

    val add_hash :
      memcache ->
      GitStore.Repo.t ->
      GitStore.tree ->
      string list ->
      tree_hash ->
      GitStore.tree option Lwt.t

    val add_tree : memcache -> GitStore.Repo.t -> GitStore.tree -> unit Lwt.t
  end

  (* Further... *)
  val compute_context_hash :
    GitStore.Repo.t ->
    GitStore.tree ->
    info:Irmin.Info.t ->
    parents_hashes:Hash.t list ->
    Hash.t ->
    Hash.t * GitStore.Commit.t list * GitStore.tree
end
