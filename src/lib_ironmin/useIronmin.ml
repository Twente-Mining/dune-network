(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2019 Origin-Labs                                          *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

module Hash = struct
  type t = Context_hash.t

  let to_string h = Context_hash.to_string h

  let of_string h =
    match Context_hash.of_string_opt h with
    | None ->
        assert false
    | Some v ->
        v

  let hash_string l =
    let hash = Context_hash.hash_string l in
    (* if debug_iron then Printf.eprintf "%S = hash_iron %S\n%!"
       (to_string hash) (String.concat "" l); *)
    hash
end

module Storage = Ocplib_ironmin.Ironmin_lmdb

module X = Ocplib_ironmin.Ironmin.Make (struct
  module Hash = Hash
  module Storage = Storage
  module Key = Storage_keys

  type config = Storage.config

  let create_storage = Storage.create_storage
end)

module GitStore = struct
  include X

  let empty_tree () = Tree.empty ()
end

module Irmin = struct
  module Info = GitStore.Info

  type config = Ocplib_ironmin.Ironmin_lmdb.config
end

let hash ~date ~message ~parents ~tree =
  let hash =
    GitStore.Commit.to_hash
      ~info:(GitStore.Info.v ~date ~author:"Tezos" message)
      ~parents
      tree
  in
  Lwt.return hash

module Irmin_lmdb = struct
  let config ?config:_ ?mapsize ?readonly dir =
    Storage.create_config ?mapsize ?readonly dir
end

let init () = GitStore.init ()

include GitStore.TOPLEVEL
