(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2019 Origin labs                                          *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

(* keys (strings) are storing using a uint8 opcode:
   * 0..63 : strings with corresponding size (len)
   * 64..191 : strings replaced by an identifier
   * 172..254 : binary representation of an hexadecimal (172 + len)
   * 255 : short-int representation of an integer
*)

let use_hexa_compress =
  not (Ocplib_ironmin.Ironmin.if_var_exists "IRONMIN_NO_HEXA_COMPRESS")

let use_string_to_num = true

(* We return two strings, because Irmin does not store the content of
   a file as it is hashed, but without its length (nodes are stored as
   they are hashed, however). So there is a string for hash
   computation, and a string for storage *)

let hexa_point = 191

let int_point = 255

type t = Key of int | Filename of string | Hexa of string | Integer of int

let buf = Bytes.create 64

let hexa_of_filename s =
  let len = String.length s in
  if use_hexa_compress then
    try
      let n = int_of_string s in
      let s2 = string_of_int n in
      if s = s2 then Integer n else raise Not_found
    with _ ->
      if len land 1 = 0 then
        let rec iter s i len =
          if i = len then
            let s2 = Bytes.sub_string buf 0 (len / 2) in
            (* Printf.eprintf "hexa_of_filename: %S for %S\n%!" s2 s;*)
            Hexa s2
          else
            let a = s.[i] in
            let b = s.[i + 1] in
            match (a, b) with
            | ('0' .. '9', '0' .. '9') ->
                let c =
                  ((int_of_char a - int_of_char '0') * 16)
                  + (int_of_char b - int_of_char '0')
                in
                Bytes.set buf (i / 2) (char_of_int c) ;
                iter s (i + 2) len
            | ('0' .. '9', 'a' .. 'f') ->
                let c =
                  ((int_of_char a - int_of_char '0') * 16)
                  + (int_of_char b - int_of_char 'a' + 10)
                in
                Bytes.set buf (i / 2) (char_of_int c) ;
                iter s (i + 2) len
            | ('a' .. 'f', '0' .. '9') ->
                let c =
                  ((int_of_char a - int_of_char 'a' + 10) * 16)
                  + (int_of_char b - int_of_char '0')
                in
                Bytes.set buf (i / 2) (char_of_int c) ;
                iter s (i + 2) len
            | ('a' .. 'f', 'a' .. 'f') ->
                let c =
                  ((int_of_char a - int_of_char 'a' + 10) * 16)
                  + (int_of_char b - int_of_char 'a' + 10)
                in
                Bytes.set buf (i / 2) (char_of_int c) ;
                iter s (i + 2) len
            | _ ->
                Format.eprintf "Filename3 %S@." s ;
                Filename s
        in
        iter s 0 len
      else (
        Format.eprintf "Filename2 %S@." s ;
        Filename s )
  else (
    Format.eprintf "Filename0 %S@." s ;
    Filename s )

(* These encodings can be extended anytime, as they are not used in
   the computation of the context hash, only in its storage. *)
let filename_of_key = function
  | 64 ->
      "balance"
  | 65 ->
      "active_delegates_with_rolls"
  | 66 ->
      "change"
  | 67 ->
      "constants"
  | 68 ->
      "contracts"
  | 69 ->
      "counter"
  | 70 ->
      "current"
  | 71 ->
      "current_period_kind"
  | 72 ->
      "current_quorum"
  | 73 ->
      "cycle"
  | 74 ->
      "data"
  | 75 ->
      "delegate"
  | 76 ->
      "delegate_desactivation"
  | 77 ->
      "delegates"
  | 78 ->
      "delegates_with_frozen_balance"
  | 79 ->
      "deposits"
  | 80 ->
      "ed25519"
  | 81 ->
      "fees"
  | 82 ->
      "first_level"
  | 83 ->
      "frozen_balance"
  | 84 ->
      "global_counter"
  | 85 ->
      "index"
  | 86 ->
      "last_block_priority"
  | 87 ->
      "last_roll"
  | 88 ->
      "limbo"
  | 89 ->
      "manager"
  | 90 ->
      "next"
  | 91 ->
      "nonces"
  | 92 ->
      "owner"
  | 93 ->
      "protocol"
  | 94 ->
      "random_seed"
  | 95 ->
      "rewards"
  | 96 ->
      "roll_list"
  | 97 ->
      "rolls"
  | 98 ->
      "roll_snapshot"
  | 99 ->
      "snapshot"
  | 100 ->
      "spendable"
  | 101 ->
      "successor"
  | 102 ->
      "test_chain"
  | 103 ->
      "v1"
  | 104 ->
      "version"
  | 105 ->
      "votes"
  | 106 ->
      "delegated"
  | 107 ->
      "delegatable"
  | 108 ->
      "commitments"
  | 109 ->
      "ramp_up"
  | 110 ->
      "storage"
  | 111 ->
      "len"
  | 112 ->
      "big_map"
  | 113 ->
      "secp256k1"
  | 114 ->
      "originated"
  | 115 ->
      "paid_bytes"
  | 116 ->
      "used_bytes"
  | 117 ->
      "p256"
  | 118 ->
      "code"
  | 119 ->
      "inactive_delegate"
  | 120 ->
      "current_proposal"
  | 121 ->
      "listings"
  | 122 ->
      "ballots"
  | 123 ->
      "listings_size"
  | 124 ->
      "proposals"
  | 125 ->
      "proposals_count"
  | 126 ->
      "prev_epochs"
  | 127 ->
      "scripts"
  | 128 ->
      "level"
  | 129 ->
      "protocol_parameters"
  | 130 ->
      "activate_protocol"
  | 131 ->
      "fee_code"
  | 132 ->
      "protocol_revision"
  | 133 ->
      "key_type"
  | 134 ->
      "total_bytes"
  | 135 ->
      "value_type"
  | 136 ->
      "contents"
  | 137 ->
      "nrolls"
  | 138 ->
      "superadmin"
  | 139 ->
      "big_maps"
  | 140 ->
      "block_priority"
  | 141 ->
      "pending_protocol_init"
  | 142 ->
      "maxrolls"
  | 143 ->
      "white_list"
  | 144 ->
      "delegation_closed"
  | 145 ->
      "kyc"
  | 146 ->
      "participation_ema"
  | 147 ->
      "next_deflate"
  (* we can go to 191 *)
  | k ->
      Printf.kprintf (fun s -> raise (Failure s)) "unexpected key = %d" k

(* ironmin version *)
let key_of_filename s =
  if use_string_to_num then
    match s with
    | "balance" ->
        Key 64
    | "active_delegates_with_rolls" ->
        Key 65
    | "change" ->
        Key 66
    | "constants" ->
        Key 67
    | "contracts" ->
        Key 68
    | "counter" ->
        Key 69
    | "current" ->
        Key 70
    | "current_period_kind" ->
        Key 71
    | "current_quorum" ->
        Key 72
    | "cycle" ->
        Key 73
    | "data" ->
        Key 74
    | "delegate" ->
        Key 75
    | "delegate_desactivation" ->
        Key 76
    | "delegates" ->
        Key 77
    | "delegates_with_frozen_balance" ->
        Key 78
    | "deposits" ->
        Key 79
    | "ed25519" ->
        Key 80
    | "fees" ->
        Key 81
    | "first_level" ->
        Key 82
    | "frozen_balance" ->
        Key 83
    | "global_counter" ->
        Key 84
    | "index" ->
        Key 85
    | "last_block_priority" ->
        Key 86
    | "last_roll" ->
        Key 87
    | "limbo" ->
        Key 88
    | "manager" ->
        Key 89
    | "next" ->
        Key 90
    | "nonces" ->
        Key 91
    | "owner" ->
        Key 92
    | "protocol" ->
        Key 93
    | "random_seed" ->
        Key 94
    | "rewards" ->
        Key 95
    | "roll_list" ->
        Key 96
    | "rolls" ->
        Key 97
    | "roll_snapshot" ->
        Key 98
    | "snapshot" ->
        Key 99
    | "spendable" ->
        Key 100
    | "successor" ->
        Key 101
    | "test_chain" ->
        Key 102
    | "v1" ->
        Key 103
    | "version" ->
        Key 104
    | "votes" ->
        Key 105
    | "delegated" ->
        Key 106
    | "delegatable" ->
        Key 107
    | "commitments" ->
        Key 108
    | "ramp_up" ->
        Key 109
    | "storage" ->
        Key 110
    | "len" ->
        Key 111
    | "big_map" ->
        Key 112
    | "secp256k1" ->
        Key 113
    | "originated" ->
        Key 114
    | "paid_bytes" ->
        Key 115
    | "used_bytes" ->
        Key 116
    | "p256" ->
        Key 117
    | "code" ->
        Key 118
    | "inactive_delegate" ->
        Key 119
    | "current_proposal" ->
        Key 120
    | "listings" ->
        Key 121
    | "ballots" ->
        Key 122
    | "listings_size" ->
        Key 123
    | "proposals" ->
        Key 124
    | "proposals_count" ->
        Key 125
    | "prev_epochs" ->
        Key 126
    | "scripts" ->
        Key 127
    | "level" ->
        Key 128
    | "protocol_parameters" ->
        Key 129
    | "activate_protocol" ->
        Key 130
    | "fee_code" ->
        Key 131
    | "protocol_revision" ->
        Key 132
    | "key_type" ->
        Key 133
    | "total_bytes" ->
        Key 134
    | "value_type" ->
        Key 135
    | "contents" ->
        Key 136
    | "nrolls" ->
        Key 137
    | "superadmin" ->
        Key 138
    | "big_maps" ->
        Key 139
    | "block_priority" ->
        Key 140
    | "pending_protocol_init" ->
        Key 141
    | "maxrolls" ->
        Key 142
    | "white_list" ->
        Key 143
    | "delegation_closed" ->
        Key 144
    | "kyc" ->
        Key 145
    | "participation_ema" ->
        Key 146
    | "next_deflate" ->
        Key 147
    (* we can go to 191 *)
    | _ ->
        (*            Printf.eprintf "| %S -> 65\n%!" s; *)
        hexa_of_filename s
  else hexa_of_filename s

let () =
  for i = 0 to 255 do
    match filename_of_key i with
    | exception _ ->
        ()
    | n -> (
      match key_of_filename n with
      | Key x when x = i ->
          ()
      | _ ->
          Printf.eprintf "Error in storage_keys with key %d\n%!" i ;
          exit 2 )
  done

open Ocplib_ironmin.Uintvar

(*
  kind:
   0 Tree
   1 DirectContent "inited"
   2..33 DirectContent string[]
   255 Content
*)
let create key =
  let key = key_of_filename key in
  let key_len =
    match key with
    | Filename key ->
        1 + String.length key
    | Hexa key ->
        1 + String.length key
    | Key _ ->
        1
    | Integer n ->
        1 + len_uint64 (Int64.of_int n)
  in
  (key, key_len)

let char_hex n =
  Char.unsafe_chr (n + if n < 10 then Char.code '0' else Char.code 'a' - 10)

let encode_hex d =
  let len = String.length d in
  let result = Bytes.create (len * 2) in
  for i = 0 to len - 1 do
    let x = Char.code d.[i] in
    Bytes.unsafe_set result (i * 2) (char_hex (x lsr 4)) ;
    Bytes.unsafe_set result ((i * 2) + 1) (char_hex (x land 0x0f))
  done ;
  Bytes.unsafe_to_string result

module BIGSTRING = struct
  let get s pos =
    let (key_len, pos) = BIGSTRING.get_uint8 s pos in
    if key_len >= 64 then
      if key_len < hexa_point then (filename_of_key key_len, pos)
      else if key_len = int_point then
        let (n, pos) = BIGSTRING.get_uint64 s pos in
        (Int64.to_string n, pos)
      else
        let key_len = key_len - hexa_point in
        let key = Bigstring.sub_string s pos key_len in
        let pos = pos + key_len in
        let key = encode_hex key in
        (key, pos)
    else
      let key = Bigstring.sub_string s pos key_len in
      let pos = pos + key_len in
      (key, pos)
end

module STRING = struct
  let get s pos =
    let (key_len, pos) = STRING.get_uint8 s pos in
    if key_len >= 64 then
      if key_len < hexa_point then (filename_of_key key_len, pos)
      else if key_len = int_point then
        let (n, pos) = STRING.get_uint64 s pos in
        (Int64.to_string n, pos)
      else
        let key_len = key_len - hexa_point in
        let key = String.sub s pos key_len in
        let pos = pos + key_len in
        let key = encode_hex key in
        (key, pos)
    else
      let key = String.sub s pos key_len in
      let pos = pos + key_len in
      (key, pos)
end

let t = Array.make 256 0

let use x = t.(x) <- t.(x) + 1

let set_uint8 s pos len = use len ; BYTES.set_uint8 s pos len

let set s pos key =
  match key with
  | Filename key ->
      let len = String.length key in
      assert (len < 64) ;
      let pos = set_uint8 s pos len in
      Bytes.blit_string key 0 s pos len ;
      pos + len
  | Hexa key ->
      let len = String.length key in
      assert (len < 64) ;
      let pos = set_uint8 s pos (hexa_point + len) in
      Bytes.blit_string key 0 s pos len ;
      pos + len
  | Key key ->
      (* Printf.eprintf "set key at pos %d\n%!" pos; *)
      set_uint8 s pos key
  | Integer n ->
      let pos = set_uint8 s pos int_point in
      let pos = BYTES.set_uint64 s pos (Int64.of_int n) in
      pos

let stats () = t
