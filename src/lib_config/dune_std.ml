(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Tezos_data_encoding

module OP = struct
  let ( // ) = Filename.concat
end

module STRING = struct
  let split s c =
    let len = String.length s in
    let rec iter pos to_rev =
      if pos = len then List.rev ("" :: to_rev)
      else
        match
          try Some (String.index_from s pos c) with Not_found -> None
        with
        | Some pos2 ->
            if pos2 = pos then iter (pos + 1) ("" :: to_rev)
            else iter (pos2 + 1) (String.sub s pos (pos2 - pos) :: to_rev)
        | None ->
            List.rev (String.sub s pos (len - pos) :: to_rev)
    in
    iter 0 []

  let rec strneql s1 s2 len =
    len = 0
    ||
    let len = len - 1 in
    s1.[len] = s2.[len] && strneql s1 s2 len

  let starts_with s ~prefix =
    let len1 = String.length s in
    let len2 = String.length prefix in
    len2 <= len1 && strneql s prefix len2

  let cut_at s c =
    try
      let pos = String.index s c in
      let len = String.length s in
      (String.sub s 0 pos, String.sub s (pos + 1) (len - pos - 1))
    with Not_found -> (s, "")
end

module FILE = struct
  let read_json filename encoding =
    let ic = open_in filename in
    let json = Ezjsonm.from_channel ic in
    close_in ic ;
    Data_encoding.Json.destruct encoding json

  let write_json filename v encoding =
    let oc = open_out filename in
    let json = Data_encoding.Json.construct encoding v in
    Ezjsonm.value_to_channel ~minify:false oc json ;
    close_out oc

  let read filename =
    let ic = open_in filename in
    let b = Buffer.create 100 in
    let len = 100 in
    let s = Bytes.create len in
    let rec iter () =
      let nread = input ic s 0 len in
      if nread > 0 then (
        Buffer.add_subbytes b s 0 nread ;
        iter () )
    in
    iter () ; close_in ic ; Buffer.contents b

  let extension file =
    let file = Filename.basename file in
    let len = String.length file in
    let dot_pos =
      match String.rindex file '.' with
      | exception Not_found ->
          -1
      | pos ->
          pos
    in
    let ext = String.sub file (dot_pos + 1) (len - dot_pos - 1) in
    String.lowercase_ascii ext
end

module UNIX = struct
  let rec waitpid args pid =
    match Unix.waitpid args pid with
    | exception Unix.Unix_error (Unix.EINTR, "waitpid", "") ->
        waitpid args pid
    | x ->
        x
end

module JSON = struct
  let to_string ?(compact = false) encoding data =
    let ezjson =
      (module Json_repr.Ezjsonm : Json_repr.Repr
        with type value = Json_repr.ezjsonm )
    in
    Json_repr.pp
      ~compact
      ezjson
      Format.str_formatter
      (Json_encoding.construct encoding data) ;
    Format.flush_str_formatter ()
end
