(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019-2020 Origin-Labs                                       *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module StringMap = Map.Make (String)

type value = Value of string | Map of value StringMap.t ref

let rec set_path map path value =
  match path with
  | [] ->
      failwith "Bad option, no path provided"
  | dir :: path -> (
    match (StringMap.find dir !map, path) with
    | (Map _, []) | (Value _, _) ->
        Printf.kprintf failwith "conflicting options at path %S" dir
    | (Map map, path) ->
        set_path map path value
    | exception Not_found -> (
      match path with
      | [] ->
          map := StringMap.add dir (Value value) !map
      | path ->
          let inner_map = ref StringMap.empty in
          map := StringMap.add dir (Map inner_map) !map ;
          set_path inner_map path value ) )

let rec json_of_map map =
  `O
    (List.map
       (fun (dir, value) ->
         ( dir,
           match value with
           | Map inner_map ->
               json_of_map inner_map
           | Value v -> (
               let json = Printf.sprintf "{ \"value\": %s }" v in
               match
                 Tezos_data_encoding.Data_encoding.Json.from_string json
               with
               | Ok (`O [("value", json)]) ->
                   json
               | _ ->
                   Printf.eprintf "JSON: %s\n" json ;
                   Printf.kprintf failwith "cannot parse value %S" v ) ))
       (StringMap.bindings !map))

let set_options options encoding =
  let map = ref StringMap.empty in
  List.iter
    (fun option ->
      match Dune_std.STRING.cut_at option '=' with
      | (_, "") ->
          Printf.kprintf failwith "Bad option %S, no value provided" option
      | (path, value) ->
          let path = Dune_std.STRING.split path '.' in
          set_path map path value)
    options ;
  let json = json_of_map map in
  Tezos_data_encoding.Data_encoding.Json.destruct encoding json

let merge ~set_options ~set_string_options =
  set_options
  @ List.map
      (fun s ->
        let (before, after) = Dune_std.STRING.cut_at s '=' in
        Printf.sprintf "%s=\"%s\"" before after)
      set_string_options
