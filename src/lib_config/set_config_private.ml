(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* Configuration for a private version of Tezos *)

open Config_type

(* The activator for genesis block:
   Hash: tz1id1nnbxUwK1dgJwUra5NEYixK37k6hRri
   Public Key: edpkv2Gafe23nz4x4hJ1SaUM3H5hBAp5LDwsvGVjPsbMyZHNomxxQA
   Secret Key: unencrypted:edsk4LhLg3212HzL7eCXfCWWvyWFDfwUS7doL4JUSmkgTe4qwZbVYm
*)

let config =
  {
    network = "Private";
    forced_protocol_upgrades = [];
    genesis_key = "edpkv2Gafe23nz4x4hJ1SaUM3H5hBAp5LDwsvGVjPsbMyZHNomxxQA";
    genesis_time = "2019-06-30T16:07:32Z";
    genesis_block = "BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2";
    genesis_protocol = "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P";
    p2p_version = "DUNE_PRIVATE_2018-11-23T16:07:32Z";
    max_operation_data_length = 65536;
    encoding_version = 1;
    prefix_dir = "sozet";
    p2p_port = 19733;
    rpc_port = 18733;
    discovery_port = 20733;
    signer_tcp_port = 17733;
    signer_http_port = 16733;
    bootstrap_peers = [];
    cursym = "cur";
    tzcompat = false;
    protocol_revision = 0;
    only_allowed_bakers = false;
    expected_pow = 26.;
    self_amending = false;
    number_of_cycles_between_burn = 10;
    share_of_balance_to_burn = 10;
    frozen_account_cycles = 60;
    p2p_secret = None;
    nodes_list = None;
    access_control = false;
    minimal_fee_factor = 0;
    closed_network = false;
    kyc_level = Kyc_open;
  }

let config_option = Some config
