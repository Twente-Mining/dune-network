(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* We define here these types so that external tools can generate these
   configuration for the network. The file is included in the protocol,
   so that the protocol can check that it is up-to-date. *)

module MAKE_PARAMETRIC_005 (S : sig
  type period

  type tez
end) =
struct
  open S

  type parametric = {
    preserved_cycles : int;
    blocks_per_cycle : int32;
    blocks_per_commitment : int32;
    blocks_per_roll_snapshot : int32;
    blocks_per_voting_period : int32;
    time_between_blocks : period list;
    endorsers_per_block : int;
    hard_gas_limit_per_operation : Z.t;
    hard_gas_limit_per_block : Z.t;
    proof_of_work_threshold : int64;
    tokens_per_roll : tez;
    michelson_maximum_type_size : int;
    seed_nonce_revelation_tip : tez;
    origination_size : int;
    block_security_deposit : tez;
    endorsement_security_deposit : tez;
    block_reward : tez;
    endorsement_reward : tez;
    cost_per_byte : tez;
    hard_storage_limit_per_operation : Z.t;
    test_chain_duration : int64;
    (* in seconds *)
    (* Dune specifics: modify also Constants_storage if you add more *)
    hard_gas_limit_to_pay_fees : Z.t;
    max_operation_ttl : int;
    allow_collect_call : bool;
    (* more *)
    quorum_min : int32;
    quorum_max : int32;
    min_proposal_quorum : int32;
    initial_endorsers : int;
    delay_per_missing_endorsement : period;
  }
end

module MAKE_PARAMETERS_005 (S : sig
  type public_key_hash

  type public_key

  type tez

  type commitment

  type parametric

  type script
end) =
struct
  open S

  type bootstrap_account = {
    public_key_hash : public_key_hash;
    public_key : public_key option;
    amount : tez;
  }

  type bootstrap_contract = {
    delegate : public_key_hash;
    amount : tez;
    script : script;
  }

  type t = {
    bootstrap_accounts : bootstrap_account list;
    bootstrap_contracts : bootstrap_contract list;
    commitments : commitment list;
    constants : parametric;
    security_deposit_ramp_up_cycles : int option;
    no_reward_cycles : int option;
  }
end
