(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module OP : sig
  val ( // ) : string -> string -> string
end

module STRING : sig
  val split : string -> char -> string list

  val starts_with : string -> prefix:string -> bool

  val cut_at : string -> char -> string * string
end

module FILE : sig
  val read_json : string -> 'a Tezos_data_encoding.Data_encoding.encoding -> 'a

  val write_json :
    string -> 'a -> 'a Tezos_data_encoding.Data_encoding.encoding -> unit

  val read : string -> string

  val extension : string -> string
end

module UNIX : sig
  val waitpid : Unix.wait_flag list -> int -> int * Unix.process_status
end

module JSON : sig
  val to_string : ?compact:bool -> 'a Json_encoding.encoding -> 'a -> string
end
