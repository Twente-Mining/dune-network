(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Tezos_data_encoding.Data_encoding
open Config_type

let () =
  Printexc.register_printer (function
      | Json_encoding.Cannot_destruct (path, exn) ->
          Some
            (Printf.sprintf
               "%s (%s)"
               (String.concat
                  "/"
                  (List.map
                     (function
                       | `Field f ->
                           f
                       | `Index n ->
                           string_of_int n
                       | `Next ->
                           "."
                       | `Star ->
                           "*")
                     path))
               (Printexc.to_string exn))
      | _ ->
          None)

let dft x v = match x with None -> v | Some x -> x

let kyc_level_encoding =
  string_enum [("open", Kyc_open); ("source", Kyc_source); ("both", Kyc_both)]

let encoding default =
  conv
    (fun x ->
      ( ( Some x.network,
          Some x.genesis_key,
          Some x.p2p_version,
          Some x.max_operation_data_length,
          Some x.encoding_version,
          Some x.prefix_dir,
          Some x.p2p_port,
          Some x.rpc_port,
          Some x.bootstrap_peers,
          Some x.genesis_time ),
        ( ( Some x.genesis_block,
            Some x.genesis_protocol,
            Some x.forced_protocol_upgrades,
            Some x.cursym,
            Some x.discovery_port,
            Some x.signer_tcp_port,
            Some x.signer_http_port,
            Some x.protocol_revision,
            Some x.only_allowed_bakers,
            Some x.expected_pow ),
          ( Some x.self_amending,
            Some x.number_of_cycles_between_burn,
            Some x.share_of_balance_to_burn,
            Some x.frozen_account_cycles,
            Some x.p2p_secret,
            Some x.nodes_list,
            Some x.access_control,
            Some x.minimal_fee_factor,
            Some x.closed_network,
            Some x.kyc_level ) ) ))
    (fun ( ( network,
             genesis_key,
             p2p_version,
             max_operation_data_length,
             encoding_version,
             prefix_dir,
             p2p_port,
             rpc_port,
             bootstrap_peers,
             genesis_time ),
           ( ( genesis_block,
               genesis_protocol,
               forced_protocol_upgrades,
               cursym,
               discovery_port,
               signer_tcp_port,
               signer_http_port,
               protocol_revision,
               only_allowed_bakers,
               expected_pow ),
             ( self_amending,
               number_of_cycles_between_burn,
               share_of_balance_to_burn,
               frozen_account_cycles,
               p2p_secret,
               nodes_list,
               access_control,
               minimal_fee_factor,
               closed_network,
               kyc_level ) ) ) ->
      let forced_protocol_upgrades =
        dft forced_protocol_upgrades default.forced_protocol_upgrades
      in
      let genesis_key = dft genesis_key default.genesis_key in
      let p2p_version = dft p2p_version default.p2p_version in
      let max_operation_data_length =
        dft max_operation_data_length default.max_operation_data_length
      in
      let encoding_version = dft encoding_version default.encoding_version in
      let prefix_dir = dft prefix_dir default.prefix_dir in
      let p2p_port = dft p2p_port default.p2p_port in
      let rpc_port = dft rpc_port default.rpc_port in
      let bootstrap_peers = dft bootstrap_peers default.bootstrap_peers in
      let genesis_time = dft genesis_time default.genesis_time in
      let genesis_block = dft genesis_block default.genesis_block in
      let genesis_protocol = dft genesis_protocol default.genesis_protocol in
      let network = dft network default.network in
      let cursym = dft cursym default.cursym in
      let discovery_port = dft discovery_port default.discovery_port in
      let signer_tcp_port = dft signer_tcp_port default.signer_tcp_port in
      let signer_http_port = dft signer_http_port default.signer_http_port in
      let protocol_revision =
        dft protocol_revision default.protocol_revision
      in
      let only_allowed_bakers =
        dft only_allowed_bakers default.only_allowed_bakers
      in
      let expected_pow = dft expected_pow default.expected_pow in
      let self_amending = dft self_amending default.self_amending in
      let number_of_cycles_between_burn =
        dft number_of_cycles_between_burn default.number_of_cycles_between_burn
      in
      let share_of_balance_to_burn =
        dft share_of_balance_to_burn default.share_of_balance_to_burn
      in
      let frozen_account_cycles =
        dft frozen_account_cycles default.frozen_account_cycles
      in
      let p2p_secret = dft p2p_secret default.p2p_secret in
      let nodes_list = dft nodes_list default.nodes_list in
      let access_control = dft access_control default.access_control in
      let minimal_fee_factor =
        dft minimal_fee_factor default.minimal_fee_factor
      in
      let closed_network = dft closed_network default.closed_network in
      let kyc_level = dft kyc_level default.kyc_level in
      {
        network;
        genesis_key;
        p2p_version;
        max_operation_data_length;
        encoding_version;
        prefix_dir;
        p2p_port;
        rpc_port;
        bootstrap_peers;
        genesis_time;
        genesis_block;
        genesis_protocol;
        forced_protocol_upgrades;
        cursym;
        tzcompat = false;
        discovery_port;
        signer_tcp_port;
        signer_http_port;
        protocol_revision;
        only_allowed_bakers;
        expected_pow;
        self_amending;
        number_of_cycles_between_burn;
        share_of_balance_to_burn;
        frozen_account_cycles;
        p2p_secret;
        nodes_list;
        access_control;
        minimal_fee_factor;
        closed_network;
        kyc_level;
      })
    (merge_objs
       (obj10
          (opt "network" string)
          (opt "genesis_key" string)
          (opt "p2p_version" string)
          (opt "max_operation_data_length" int31)
          (opt "encoding_version" int31)
          (opt "prefix_dir" string)
          (opt "p2p_port" int31)
          (opt "rpc_port" int31)
          (opt "bootstrap_peers" (list string))
          (opt "genesis_time" string))
       (merge_objs
          (obj10
             (opt "genesis_block" string)
             (opt "genesis_protocol" string)
             (opt "forced_protocol_upgrades" (list (tup2 int32 string)))
             (opt "cursym" string)
             (opt "discovery_port" int31)
             (opt "signer_tcp_port" int31)
             (opt "signer_http_port" int31)
             (opt "protocol_revision" int31)
             (opt "only_allowed_bakers" bool)
             (opt "expected_pow" float))
          (obj10
             (opt "self_amending" bool)
             (opt "number_of_cycles_between_burn" int31)
             (opt "share_of_balance_to_burn_amending" int31)
             (opt "frozen_account_cycles" int31)
             (opt "p2p_secret" (option string))
             (opt "nodes_list" (option string))
             (opt "access_control" bool)
             (opt "minimal_fee_factor" int31)
             (opt "closed_network" bool)
             (opt "kyc_level" kyc_level_encoding))))

let variable =
  Tezos_stdlib.Environment_variable.make
    "DUNE_CONFIG"
    ~description:"Select predefined or custom configuration"
    ~allowed_values:["mainnet"; "testnet"; "private"; "<filename.json>"]
