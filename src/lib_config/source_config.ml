(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* If you change this value, you may also want to change
   P2p_version.supported, which is by default the last 2
   versions. It's ok most of the time, but sometimes, being more
   tolerant can be needed. *)
let max_revision = 4

(* 0 : Athens revision 0 *)
(* 1 : Athens revision 1 *)
(* 2 : Babylon+ revision 0 *)
(* 3 : Babylon+ revision 1 (Testnet only) *)
(* 4 : Babylon+ revision 2 *)
