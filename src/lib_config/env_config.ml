(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let tezos_compatible_mode =
  match
    Tezos_stdlib.Environment_variable.(
      get
      @@ make
           "DUNE_TZ_COMPATIBILITY"
           ~description:"(De)Activate Tezos compatibility layer"
           ~allowed_values:["true"; "y"; "Y"; "yes"; "false"; "n"; "N"; "no"])
  with
  | exception Not_found ->
      false
  | "true" | "y" | "Y" | "yes" | "" ->
      true
  | "false" | "n" | "N" | "no" ->
      false
  | _ ->
      failwith "bad DUNE_TZ_COMPATIBILITY environment variable"

let guild_navigator_mode =
  (* Undocumented on purpose : only used for simulations *)
  match Sys.getenv "DUNE_GOD_MODE" with
  | exception Not_found ->
      false
  | "y" | "Y" | "Guild Navigator" ->
      Format.eprintf
        "\n\
         \027[43m WARNING: \027[49;91m You are running a Guild Navigator\027[0m\n\
         @." ;
      true
  | "" ->
      false
  | _ ->
      failwith "bad DUNE_GOD_MODE environment variable"

let genesis_protocol_revision =
  let open Tezos_stdlib.Utils.Infix in
  match
    Tezos_stdlib.Environment_variable.(
      get
      @@ make
           "DUNE_GENESIS_PROTOCOL_REVISION"
           ~description:
             "Overrides the protocol revision used after the genesis protocol"
           ~allowed_values:
             ( ["max"; "none"]
             @ List.map string_of_int (0 -- Source_config.max_revision) ))
  with
  | exception Not_found ->
      None
  | "max" ->
      Some Source_config.max_revision
  | "" | "none" ->
      None
  | s -> (
    try
      let r = int_of_string s in
      if r > Source_config.max_revision then raise Exit ;
      Some r
    with _ ->
      failwith "bad DUNE_GENESIS_PROTOCOL_REVISION environment variable" )
