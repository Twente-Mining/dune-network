(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let genesis_time = "2019-06-24T12:00:00Z"

let config =
  Config_type.
    {
      network = "Mainnet";
      (* src/lib_base/block_header.ml *)
      forced_protocol_upgrades = [];
      (* src/proto_000_Ps9mPmXa/lib_protocol/src/data.ml *)
      genesis_key = "edpkvNMEhZWPeARxBoBHVxzkPAgHJHcNKdFXCgwZBYQGc2XGExW3K5";
      (* src/bin_node/node_run_command.ml *)
      genesis_time;
      (* Use for example:
       ./dune-client dune generate genesis hash -d 2019-06-24T12:00:00Z
    *)
      genesis_block = "BLockGenesisGenesisGenesisGenesisGenesis5e671cK5Xad";
      genesis_protocol = "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P";
      (* src/lib_shell/distributed_db_message.ml *)
      p2p_version = "DUNE_MAINNET_" ^ genesis_time;
      max_operation_data_length = 16384;
      encoding_version = 2;
      (* src/bin_node/node_config_file.ml *)
      prefix_dir = "dune";
      p2p_port = 9733;
      rpc_port = 8733;
      discovery_port = 10733;
      signer_tcp_port = 7733;
      signer_http_port = 6733;
      bootstrap_peers =
        ["boot1.mainnet.dune.network"; "boot2.mainnet.dune.network"];
      cursym = "\xC4\x91";
      tzcompat = false;
      protocol_revision = 0;
      (* Protocol revision of genesis *)
      only_allowed_bakers = false;
      expected_pow = 26.;
      self_amending = false;
      number_of_cycles_between_burn = 10;
      share_of_balance_to_burn = 10;
      frozen_account_cycles = 60;
      p2p_secret = None;
      nodes_list = None;
      access_control = false;
      minimal_fee_factor = 1;
      closed_network = false;
      kyc_level = Kyc_open;
    }

let config_option = Some config
