(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Config_type

(* If you change this time, don't forget to change the genesis_block too:
   ./dune-client dune generate genesis hash -d 2019-06-24T12:00:00Z
*)
let genesis_time = "2020-04-09T13:04:34Z"

let genesis_block = "BLockGenesisGenesisGenesisGenesisGenesis4ivHcvjLvMU"

let config =
  {
    network = "Testnet";
    (* src/lib_base/block_header.ml *)
    forced_protocol_upgrades = [];
    (* src/proto_000_Ps9mPmXa/lib_protocol/src/data.ml *)
    genesis_key = "edpkvNMEhZWPeARxBoBHVxzkPAgHJHcNKdFXCgwZBYQGc2XGExW3K5";
    (* src/bin_node/node_run_command.ml *)
    genesis_time;
    genesis_block;
    genesis_protocol = "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P";
    (* src/lib_shell/distributed_db_message.ml *)
    p2p_version = "DUNE_TESTNET_" ^ genesis_time;
    max_operation_data_length = 64_000 (* old: 16384 *);
    encoding_version = 2;
    (* src/bin_node/node_config_file.ml *)
    prefix_dir = "dune-testnet";
    p2p_port = 9734;
    rpc_port = 8734;
    discovery_port = 10734;
    signer_tcp_port = 7734;
    signer_http_port = 6734;
    bootstrap_peers = ["boot1.testnet.dune.network"];
    cursym = "\xC4\x91";
    tzcompat = false;
    protocol_revision = 2;
    (* Protocol revision of genesis *)
    only_allowed_bakers = false;
    expected_pow = 26.;
    self_amending = false;
    number_of_cycles_between_burn = 10;
    share_of_balance_to_burn = 10;
    frozen_account_cycles = 60;
    p2p_secret = None;
    nodes_list = None;
    access_control = false;
    minimal_fee_factor = 1;
    closed_network = false;
    kyc_level = Kyc_open;
  }

let config_option = Some config
