(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2018 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

include Internal_event.Legacy_logging.Make_semantic (struct
  let name = "context.dump"
end)

(* "dune-snapshot-1.0.0" is reserved for the same format as Tezos.
   For compressed snapshots, we will use dune-snapshot-1.1.0.
   We have done the following changes:
  * Every key in a Node is compressed using Storage_keys
  * We number hashes, and replace them by their number. Numbers are stored
    using Z encoding.

   Current compression state, observed on Tezos snapshots:
   1683008568 Oct 10 mainnet-irmin.full.bz2          (v 1.1 -> bz2 -30%)
   2195849770 Oct 10 mainnet-irmin.full              (v 1.1         -8%)
   2395978251 Sep 23 BLbauWxwwLMM-589825.full
   1710676003 Sep 23 BLbauWxwwLMM-589825.full.bz2    (v 1.0 -> bz2 -28%)

   107620163 Oct 10 mainnet-irmin.rolling.bz2       (v 1.1 -> bz2 -55%)
   146142981 Oct 10 mainnet-irmin.rolling           (v 1.1        -39%)
   238871681 Sep 30 BLBSXYG8ueG4-569345.rolling
   134411597 Sep 30 BLBSXYG8ueG4-569345.rolling.bz2 (v 1.0 -> bz2 -44%)
*)

type version = Version_1_0_0 | Version_1_1_0 | Version_unknown of string

let current_versions = [Version_1_1_0; Version_1_0_0]

let string_of_version = function
  | Version_1_0_0 ->
      "dune-snapshot-1.0.0"
  | Version_1_1_0 ->
      "dune-snapshot-1.1.0"
  | Version_unknown s ->
      s

let version_of_string = function
  | "dune-snapshot-1.0.0" ->
      Version_1_0_0
  | "dune-snapshot-1.1.0" ->
      Version_1_1_0
  | s ->
      Version_unknown s

(*****************************************************************************)
module type Dump_interface = sig
  type index

  type context

  type tree

  type hash

  type step = string

  type key = step list

  type commit_info

  val commit_info_encoding : commit_info Data_encoding.t

  val hash_encoding : hash Data_encoding.t

  val blob_encoding : [`Blob of Bytes.t] Data_encoding.t

  val node_encoding : [`Node of Bytes.t] Data_encoding.t

  module Block_header : sig
    type t = Block_header.t

    val to_bytes : t -> Bytes.t

    val of_bytes : Bytes.t -> t option

    val equal : t -> t -> bool

    val encoding : t Data_encoding.t
  end

  module Pruned_block : sig
    type t

    val to_bytes : t -> Bytes.t

    val of_bytes : Bytes.t -> t option

    val header : t -> Block_header.t

    val encoding : t Data_encoding.t

    val encoding2 : t Data_encoding.t
  end

  module Block_data : sig
    type t

    val to_bytes : t -> Bytes.t

    val of_bytes : Bytes.t -> t option

    val header : t -> Block_header.t

    val encoding : t Data_encoding.t
  end

  module Protocol_data : sig
    type t

    val to_bytes : t -> Bytes.t

    val of_bytes : Bytes.t -> t option

    val encoding : t Data_encoding.t
  end

  module Commit_hash : sig
    type t

    val to_bytes : t -> Bytes.t

    val of_bytes : Bytes.t -> t tzresult

    val encoding : t Data_encoding.t

    val pp : Format.formatter -> t -> unit
  end

  (* hash manipulation *)
  val hash_export : hash -> [`Node | `Blob] * Bytes.t

  val hash_import : [`Node | `Blob] -> Bytes.t -> hash tzresult

  val hash_equal : hash -> hash -> bool

  (* commit manipulation (for parents) *)
  val context_parents : context -> Commit_hash.t list Lwt.t

  (* Commit info *)
  val context_info : context -> commit_info

  val context_info_export : commit_info -> Int64.t * string * string

  val context_info_import : Int64.t * string * string -> commit_info

  (* block header manipulation *)
  val get_context : index -> Block_header.t -> context option Lwt.t

  (* Fabrice: this interface is specific to Irmin, because it assumes
     that anything is a `tree`, whereas Ironmin makes a difference
     between a blob and a tree. *)

  (* for dumping *)
  val fold_context_path :
    ?progress:(int -> unit) ->
    context ->
    ([> `Data of Bigstring.t | `Node of (string * hash) list] -> unit Lwt.t) ->
    unit Lwt.t

  (* for restoring *)
  type memcache

  val make_memcache : unit -> memcache

  val make_context : index -> context

  val set_context :
    memcache ->
    info:commit_info ->
    parents:Commit_hash.t list ->
    context ->
    Block_header.t ->
    Block_header.t option Lwt.t

  val update_context : context -> tree -> context

  val add_hash : memcache -> index -> tree -> key -> hash -> tree option Lwt.t

  val add_mbytes : memcache -> index -> Bytes.t -> unit Lwt.t

  val add_dir : memcache -> index -> (step * hash) list -> tree option Lwt.t
end

module type S = sig
  type index

  type context

  type block_header

  type block_data

  type pruned_block

  type protocol_data

  val dump_contexts_fd :
    index ->
    block_header
    * block_data
    * History_mode.t
    * (block_header ->
      (pruned_block option * protocol_data option) tzresult Lwt.t) ->
    fd:Lwt_unix.file_descr ->
    unit tzresult Lwt.t

  val restore_contexts_fd :
    index ->
    fd:Lwt_unix.file_descr ->
    ((Block_hash.t * pruned_block) list -> unit tzresult Lwt.t) ->
    (block_header option ->
    Block_hash.t ->
    pruned_block ->
    unit tzresult Lwt.t) ->
    ( block_header
    * block_data
    * History_mode.t
    * Block_header.t option
    * Block_hash.t list
    * protocol_data list )
    tzresult
    Lwt.t
end

type error += System_write_error of string

type error += Bad_hash of string * Bytes.t * Bytes.t

type error += Context_not_found of Bytes.t

type error += System_read_error of string

type error += Inconsistent_snapshot_file

type error += Inconsistent_snapshot_data

type error += Missing_snapshot_data

type error += Invalid_snapshot_version of string * string list

type error += Restore_context_failure

let () =
  let open Data_encoding in
  register_error_kind
    `Permanent
    ~id:"Writing_error"
    ~title:"Writing error"
    ~description:"Cannot write in file for context dump"
    ~pp:(fun ppf s ->
      Format.fprintf ppf "Unable to write file for context dumping: %s" s)
    (obj1 (req "context_dump_no_space" string))
    (function System_write_error s -> Some s | _ -> None)
    (fun s -> System_write_error s) ;
  register_error_kind
    `Permanent
    ~id:"Bad_hash"
    ~title:"Bad hash"
    ~description:"Wrong hash given"
    ~pp:(fun ppf (ty, his, hshould) ->
      Format.fprintf
        ppf
        "Wrong hash [%s] given: %s, should be %s"
        ty
        (Bytes.to_string his)
        (Bytes.to_string hshould))
    (obj3
       (req "hash_ty" string)
       (req "hash_is" bytes)
       (req "hash_should" bytes))
    (function
      | Bad_hash (ty, his, hshould) -> Some (ty, his, hshould) | _ -> None)
    (fun (ty, his, hshould) -> Bad_hash (ty, his, hshould)) ;
  register_error_kind
    `Permanent
    ~id:"Context_not_found"
    ~title:"Context not found"
    ~description:"Cannot find context corresponding to hash"
    ~pp:(fun ppf mb ->
      Format.fprintf ppf "No context with hash: %s" (Bytes.to_string mb))
    (obj1 (req "context_not_found" bytes))
    (function Context_not_found mb -> Some mb | _ -> None)
    (fun mb -> Context_not_found mb) ;
  register_error_kind
    `Permanent
    ~id:"System_read_error"
    ~title:"System read error"
    ~description:"Failed to read file"
    ~pp:(fun ppf uerr ->
      Format.fprintf
        ppf
        "Error while reading file for context dumping: %s"
        uerr)
    (obj1 (req "system_read_error" string))
    (function System_read_error e -> Some e | _ -> None)
    (fun e -> System_read_error e) ;
  register_error_kind
    `Permanent
    ~id:"Inconsistent_snapshot_file"
    ~title:"Inconsistent snapshot file"
    ~description:"Error while reading snapshot file"
    ~pp:(fun ppf () ->
      Format.fprintf
        ppf
        "Failed to read snapshot file. The file was corrupted or truncated.")
    empty
    (function Inconsistent_snapshot_file -> Some () | _ -> None)
    (fun () -> Inconsistent_snapshot_file) ;
  register_error_kind
    `Permanent
    ~id:"Inconsistent_snapshot_data"
    ~title:"Inconsistent snapshot data"
    ~description:"The data provided by the snapshot is inconsistent"
    ~pp:(fun ppf () ->
      Format.fprintf
        ppf
        "The data provided by the snapshot file is inconsistent (context_hash \
         does not correspond for block).")
    empty
    (function Inconsistent_snapshot_data -> Some () | _ -> None)
    (fun () -> Inconsistent_snapshot_data) ;
  register_error_kind
    `Permanent
    ~id:"Missing_snapshot_data"
    ~title:"Missing data in imported snapshot"
    ~description:"Mandatory data missing while reaching end of snapshot file."
    ~pp:(fun ppf () ->
      Format.fprintf
        ppf
        "Mandatory data is missing is the provided snapshot file.")
    empty
    (function Missing_snapshot_data -> Some () | _ -> None)
    (fun () -> Missing_snapshot_data) ;
  register_error_kind
    `Permanent
    ~id:"Invalid_snapshot_version"
    ~title:"Invalid snapshot version"
    ~description:"The version of the snapshot to import is not valid"
    ~pp:(fun ppf (found, expected) ->
      Format.fprintf
        ppf
        "The snapshot to import has version \"%s\" but \"%s\" was expected."
        found
        (String.concat "\" or \"" expected))
    (obj2 (req "found" string) (req "expected" (list string)))
    (function
      | Invalid_snapshot_version (found, expected) ->
          Some (found, expected)
      | _ ->
          None)
    (fun (found, expected) -> Invalid_snapshot_version (found, expected)) ;
  register_error_kind
    `Permanent
    ~id:"Restore_context_failure"
    ~title:"Failed to restore context"
    ~description:"Internal error while restoring the context"
    ~pp:(fun ppf () ->
      Format.fprintf ppf "Internal error while restoring the context.")
    empty
    (function Restore_context_failure -> Some () | _ -> None)
    (fun () -> Restore_context_failure)

module Make (I : Dump_interface) = struct
  type command =
    | Root of {
        block_header : I.Block_header.t;
        info : I.commit_info;
        parents : I.Commit_hash.t list;
        block_data : I.Block_data.t;
      }
    | Node of (string * I.hash) list
    | Blob of Bytes.t
    | Proot of I.Pruned_block.t
    | Loot of I.Protocol_data.t
    | End
    | End2
    | Node2 of (string * (int, I.hash) result) list
    | Proot2 of I.Pruned_block.t

  (* Command encoding. *)

  let blob_encoding =
    let open Data_encoding in
    case
      ~title:"blob"
      (Tag (Char.code 'b'))
      bytes
      (function Blob bytes -> Some bytes | _ -> None)
      (function bytes -> Blob bytes)

  let node_encoding =
    let open Data_encoding in
    case
      ~title:"node"
      (Tag (Char.code 'd'))
      (list (obj2 (req "name" string) (req "hash" I.hash_encoding)))
      (function Node x -> Some x | _ -> None)
      (function x -> Node x)

  let end_encoding =
    let open Data_encoding in
    case
      ~title:"end"
      (Tag (Char.code 'e'))
      empty
      (function End -> Some () | _ -> None)
      (fun () -> End)

  let loot_encoding =
    let open Data_encoding in
    case
      ~title:"loot"
      (Tag (Char.code 'l'))
      I.Protocol_data.encoding
      (function Loot protocol_data -> Some protocol_data | _ -> None)
      (fun protocol_data -> Loot protocol_data)

  let proot_encoding =
    let open Data_encoding in
    case
      ~title:"proot"
      (Tag (Char.code 'p'))
      (obj1 (req "pruned_block" I.Pruned_block.encoding))
      (function Proot pruned_block -> Some pruned_block | _ -> None)
      (fun pruned_block -> Proot pruned_block)

  let node_encoding =
    let open Data_encoding in
    case
      ~title:"node"
      (Tag (Char.code 'd'))
      (list (obj2 (req "name" string) (req "hash" I.hash_encoding)))
      (function Node x -> Some x | _ -> None)
      (function x -> Node x)

  let root_encoding =
    let open Data_encoding in
    case
      ~title:"root"
      (Tag (Char.code 'r'))
      (obj4
         (req "block_header" (dynamic_size I.Block_header.encoding))
         (req "info" I.commit_info_encoding)
         (req "parents" (list I.Commit_hash.encoding))
         (req "block_data" I.Block_data.encoding))
      (function
        | Root {block_header; info; parents; block_data} ->
            Some (block_header, info, parents, block_data)
        | _ ->
            None)
      (fun (block_header, info, parents, block_data) ->
        Root {block_header; info; parents; block_data})

  let node2_encoding =
    let open Data_encoding in
    case
      ~title:"node2"
      (Tag (Char.code 'D'))
      (list
         (conv
            (fun (name, hash) ->
              let name =
                let (key, key_len) = Dune_ironmin.Storage_keys.create name in
                let b = Bytes.create key_len in
                let pos = Dune_ironmin.Storage_keys.set b 0 key in
                assert (pos = key_len) ;
                Bytes.to_string b
              in
              (name, hash))
            (fun (name, hash) ->
              let name =
                let len = String.length name in
                let (name, pos) =
                  Dune_ironmin.Storage_keys.STRING.get name 0
                in
                assert (pos = len) ;
                name
              in
              (name, hash))
            (obj2
               (req "name" DUNE.string)
               (req "hash" (result DUNE.int I.hash_encoding)))))
      (function Node2 x -> Some x | _ -> None)
      (function x -> Node2 x)

  let end2_encoding =
    let open Data_encoding in
    case
      ~title:"end"
      (Tag (Char.code 'E'))
      string
      (function End2 -> Some "END TRAILER" | _ -> None)
      (fun _ -> End2)

  let proot2_encoding =
    let open Data_encoding in
    case
      ~title:"proot"
      (Tag (Char.code 'P'))
      (obj1 (req "pruned_block" I.Pruned_block.encoding2))
      (function Proot2 pruned_block -> Some pruned_block | _ -> None)
      (fun pruned_block -> Proot2 pruned_block)

  let command_encoding =
    Data_encoding.union
      ~tag_size:`Uint8
      [ blob_encoding;
        node_encoding;
        end_encoding;
        loot_encoding;
        proot_encoding;
        root_encoding;
        end2_encoding;
        node2_encoding;
        proot2_encoding ]

  (* IO toolkit. *)

  module type RBUF_SIG = sig
    type t

    val create_rbuf : Lwt_unix.file_descr -> t

    val total_rbuf : t -> int

    val get_mbytes : version -> t -> Bytes.t tzresult Lwt.t
  end

  module RBUF : RBUF_SIG = struct
    type t = {
      fd : Lwt_unix.file_descr;
      mutable buf : Bytes.t;
      mutable ofs : int;
      mutable total : int;
    }

    let create_rbuf fd = {fd; buf = Bytes.empty; ofs = 0; total = 0}

    let total_rbuf rbuf = rbuf.total

    let rec has_at_least rbuf ~len =
      if Bytes.length rbuf.buf - rbuf.ofs < len then (
        let blen = Bytes.length rbuf.buf - rbuf.ofs in
        let neu = Bytes.create (blen + 1_000_000) in
        Bytes.blit rbuf.buf rbuf.ofs neu 0 blen ;
        Lwt_unix.read rbuf.fd neu blen 1_000_000
        >>= fun bread ->
        rbuf.total <- rbuf.total + bread ;
        if bread = 0 then fail Inconsistent_snapshot_file
        else
          let neu =
            if bread <> 1_000_000 then Bytes.sub neu 0 (blen + bread) else neu
          in
          rbuf.buf <- neu ;
          rbuf.ofs <- 0 ;
          has_at_least rbuf ~len )
      else return_unit

    let read_string rbuf ~len =
      has_at_least rbuf ~len
      >>=? fun () ->
      let res = Bytes.sub_string rbuf.buf rbuf.ofs len in
      rbuf.ofs <- rbuf.ofs + len ;
      return res

    let read_mbytes rbuf b =
      read_string rbuf ~len:(Bytes.length b)
      >>=? fun string ->
      Bytes.blit_string string 0 b 0 (Bytes.length b) ;
      return ()

    let old_get_int64 rbuf =
      read_string ~len:8 rbuf
      >>=? fun s -> return @@ EndianString.BigEndian.get_int64 s 0

    let get_int64 rbuf =
      has_at_least rbuf ~len:9
      >>=? fun () ->
      let (n, pos) =
        Ocplib_ironmin.Uintvar.BYTES.get_uint64 rbuf.buf rbuf.ofs
      in
      rbuf.ofs <- pos ;
      return n

    let get_mbytes v rbuf =
      match v with
      | Version_1_0_0 ->
          old_get_int64 rbuf >>|? Int64.to_int
          >>=? fun l ->
          let b = Bytes.create l in
          read_mbytes rbuf b >>=? fun () -> return b
      | Version_1_1_0 ->
          get_int64 rbuf >>|? Int64.to_int
          >>=? fun l ->
          let b = Bytes.create l in
          read_mbytes rbuf b >>=? fun () -> return b
      | Version_unknown _ ->
          assert false
  end

  open RBUF

  let old_set_int64 buf i =
    let b = Bytes.create 8 in
    EndianBytes.BigEndian.set_int64 b 0 i ;
    Buffer.add_bytes buf b

  let set_mbytes version buf b =
    let n = Int64.of_int (Bytes.length b) in
    ( match version with
    | Version_1_0_0 ->
        old_set_int64 buf n
    | Version_1_1_0 ->
        let len = Ocplib_ironmin.Uintvar.len_uint64 n in
        let b = Bytes.create len in
        let len' = Ocplib_ironmin.Uintvar.BYTES.set_uint64 b 0 n in
        assert (len = len') ;
        Buffer.add_bytes buf b
    | Version_unknown _ ->
        assert false ) ;
    Buffer.add_bytes buf b

  let old_set_mbytes = set_mbytes Version_1_0_0

  let set_mbytes = set_mbytes Version_1_1_0

  (* Getter and setters *)

  let get_command version rbuf =
    get_mbytes version rbuf
    >>|? fun bytes -> Data_encoding.Binary.of_bytes_exn command_encoding bytes

  let set_root buf block_header info parents block_data =
    let root = Root {block_header; info; parents; block_data} in
    let bytes = Data_encoding.Binary.to_bytes_exn command_encoding root in
    set_mbytes buf bytes

  let set_node buf contents =
    let bytes =
      Data_encoding.Binary.to_bytes_exn command_encoding (Node contents)
    in
    set_mbytes buf bytes

  let set_node2 buf contents =
    let bytes =
      Data_encoding.Binary.to_bytes_exn command_encoding (Node2 contents)
    in
    set_mbytes buf bytes

  let set_blob buf data =
    let bytes =
      Data_encoding.Binary.to_bytes_exn
        command_encoding
        (Blob (Bigstring.to_bytes data))
    in
    set_mbytes buf bytes

  let set_proot buf pruned_block =
    let proot = Proot2 pruned_block in
    let bytes = Data_encoding.Binary.to_bytes_exn command_encoding proot in
    set_mbytes buf bytes

  let set_loot buf protocol_data =
    let loot = Loot protocol_data in
    let bytes = Data_encoding.Binary.to_bytes_exn command_encoding loot in
    set_mbytes buf bytes

  let set_end buf =
    let bytes = Data_encoding.Binary.to_bytes_exn command_encoding End2 in
    set_mbytes buf bytes

  (* Snapshot metadata *)

  (* TODO add more info (e.g. nb context item, nb blocks, etc.) *)
  type snapshot_metadata = {
    version : version;
    mode : Tezos_shell_services.History_mode.t;
  }

  let snapshot_metadata_encoding =
    let open Data_encoding in
    conv
      (fun {version; mode} -> (string_of_version version, mode))
      (fun (version, mode) ->
        let version = version_of_string version in
        {version; mode})
      (obj2
         (req "version" string)
         (req "mode" Tezos_shell_services.History_mode.encoding))

  let write_snapshot_metadata ~mode buf =
    let version = {version = List.hd current_versions; mode} in
    let bytes =
      Data_encoding.(Binary.to_bytes_exn snapshot_metadata_encoding version)
    in
    old_set_mbytes buf bytes

  let read_snapshot_metadata rbuf =
    get_mbytes Version_1_0_0 rbuf
    >>|? fun bytes ->
    Data_encoding.(Binary.of_bytes_exn snapshot_metadata_encoding) bytes

  let check_version v =
    match v.version with
    | Version_unknown v ->
        fail
          (Invalid_snapshot_version
             (v, List.map string_of_version current_versions))
    | _ ->
        return_unit

  let dump_contexts_fd idx data ~fd =
    (* Dumping *)
    let buf = Buffer.create 1_100_000 in
    let written = ref 0 in
    let flush () =
      let contents = Buffer.contents buf in
      Buffer.clear buf ;
      written := !written + String.length contents ;
      Lwt_utils_unix.write_string fd contents
    in
    let maybe_flush () =
      if Buffer.length buf > 1_000_000 then flush () else Lwt.return_unit
    in
    let hash_counter = ref 0 in
    let hash_to_num = Hashtbl.create 1_000_000 in
    Lwt.catch
      (fun () ->
        let (bh, block_data, mode, pruned_iterator) = data in
        write_snapshot_metadata ~mode buf ;
        I.get_context idx bh
        >>= function
        | None ->
            fail @@ Context_not_found (I.Block_header.to_bytes bh)
        | Some ctxt ->
            let progress cpt =
              Tezos_stdlib_unix.Utils.display_progress
                ~refresh_rate:(cpt, 1_000)
                "Context: %dK elements, %dMiB written%!"
                (cpt / 1_000)
                (!written / 1_048_576)
            in
            I.fold_context_path ~progress ctxt (function
                | `Data data ->
                    set_blob buf data ; maybe_flush ()
                | `Node sub_keys ->
                    let sub_keys =
                      List.map
                        (fun (key, hash) ->
                          let hash =
                            try Ok (Hashtbl.find hash_to_num hash)
                            with Not_found ->
                              let num = !hash_counter in
                              incr hash_counter ;
                              Hashtbl.add hash_to_num hash num ;
                              Error hash
                          in
                          (key, hash))
                        sub_keys
                    in
                    set_node2 buf sub_keys ; maybe_flush ())
            >>= fun () ->
            Tezos_stdlib_unix.Utils.display_progress_end () ;
            I.context_parents ctxt
            >>= fun parents ->
            List.iter
              (fun p -> Format.eprintf "parent: %a\n%!" I.Commit_hash.pp p)
              parents ;
            set_root buf bh (I.context_info ctxt) parents block_data ;
            (* Dump pruned blocks *)
            let dump_pruned cpt pruned =
              Tezos_stdlib_unix.Utils.display_progress
                ~refresh_rate:(cpt, 1_000)
                "History: %dK block, %dMiB written"
                (cpt / 1_000)
                (!written / 1_048_576) ;
              set_proot buf pruned ;
              maybe_flush ()
            in
            let rec aux cpt acc header =
              pruned_iterator header
              >>=? function
              | (None, None) ->
                  return acc (* assert false *)
              | (None, Some protocol_data) ->
                  return (protocol_data :: acc)
              | (Some pred_pruned, Some protocol_data) ->
                  dump_pruned cpt pred_pruned
                  >>= fun () ->
                  aux
                    (succ cpt)
                    (protocol_data :: acc)
                    (I.Pruned_block.header pred_pruned)
              | (Some pred_pruned, None) ->
                  dump_pruned cpt pred_pruned
                  >>= fun () ->
                  aux (succ cpt) acc (I.Pruned_block.header pred_pruned)
            in
            let starting_block_header = I.Block_data.header block_data in
            aux 0 [] starting_block_header
            >>=? fun protocol_datas ->
            (* Dump protocol data *)
            Lwt_list.iter_s
              (fun proto -> set_loot buf proto ; maybe_flush ())
              protocol_datas
            >>= fun () ->
            Tezos_stdlib_unix.Utils.display_progress_end () ;
            return_unit
            >>=? fun () ->
            set_end buf ;
            flush () >>= fun () -> return_unit)
      (function
        | Unix.Unix_error (e, _, _) ->
            fail @@ System_write_error (Unix.error_message e)
        | err ->
            Lwt.fail err)

  (* Restoring *)

  let dune_print_snapshot =
    Environment_variable.make
      ~visible:["never"]
      "DUNE_PRINT_SNAPSHOT"
      ~description:
        "Print the snapshot commands starting with the version number \
         contained in this variable"

  let restore_contexts_fd index ~fd k_store_pruned_blocks block_validation =
    let rbuf = create_rbuf fd in
    (* Editing the repository *)
    let add_blob memcache blob =
      I.add_mbytes memcache index blob >>= fun () -> return_unit
    in
    let add_dir memcache keys =
      I.add_dir memcache index keys
      >>= function
      | None -> fail Restore_context_failure | Some tree -> return tree
    in
    let hash_counter = ref 0 in
    let num_to_hash = Hashtbl.create 1_000_000 in
    let restore version =
      let memcache = I.make_memcache () in
      let rec first_pass ctxt cpt =
        Tezos_stdlib_unix.Utils.display_progress
          ~refresh_rate:(cpt, 1_000)
          "Context: %dK elements, %dMiB read"
          (cpt / 1_000)
          (total_rbuf rbuf / 1_048_576) ;
        get_command version.version rbuf
        >>=? function
        | Root {block_header; info; parents; block_data} -> (
            I.set_context memcache ~info ~parents ctxt block_header
            >>= function
            | None ->
                fail Inconsistent_snapshot_data
            | Some block_header ->
                return (block_header, block_data) )
        | Node contents ->
            add_dir memcache contents
            >>=? fun tree -> first_pass (I.update_context ctxt tree) (cpt + 1)
        | Node2 contents ->
            let contents =
              List.map
                (fun (key, hash) ->
                  let hash =
                    match hash with
                    | Ok num ->
                        Hashtbl.find num_to_hash num
                    | Error hash ->
                        let num = !hash_counter in
                        incr hash_counter ;
                        Hashtbl.add num_to_hash num hash ;
                        hash
                  in
                  (key, hash))
                contents
            in
            add_dir memcache contents
            >>=? fun tree -> first_pass (I.update_context ctxt tree) (cpt + 1)
        | Blob data ->
            add_blob memcache data >>=? fun _tree -> first_pass ctxt (cpt + 1)
        | _ ->
            fail Inconsistent_snapshot_data
      in
      let rec second_pass pred_header (rev_block_hashes, protocol_datas) todo
          cpt =
        Tezos_stdlib_unix.Utils.display_progress
          ~refresh_rate:(cpt, 1_000)
          "Store: %dK elements, %dMiB read"
          (cpt / 1_000)
          (total_rbuf rbuf / 1_048_576) ;
        get_command version.version rbuf
        >>=? function
        | Proot pruned_block | Proot2 pruned_block ->
            let header = I.Pruned_block.header pruned_block in
            let hash = Block_header.hash header in
            block_validation pred_header hash pruned_block
            >>=? fun () ->
            if (cpt + 1) mod 5_000 = 0 then
              k_store_pruned_blocks ((hash, pruned_block) :: todo)
              >>=? fun () ->
              second_pass
                (Some header)
                (hash :: rev_block_hashes, protocol_datas)
                []
                (cpt + 1)
            else
              second_pass
                (Some header)
                (hash :: rev_block_hashes, protocol_datas)
                ((hash, pruned_block) :: todo)
                (cpt + 1)
        | Loot protocol_data ->
            k_store_pruned_blocks todo
            >>=? fun () ->
            second_pass
              pred_header
              (rev_block_hashes, protocol_data :: protocol_datas)
              todo
              (cpt + 1)
        | End | End2 ->
            return (pred_header, rev_block_hashes, List.rev protocol_datas)
        | _ ->
            fail Inconsistent_snapshot_data
      in
      lwt_log_notice (fun f -> f "Starting first pass")
      >>= fun () ->
      first_pass (I.make_context index) 0
      >>=? fun (block_header, block_data) ->
      Tezos_stdlib_unix.Utils.display_progress_end () ;
      lwt_log_notice (fun f -> f "Starting second pass")
      >>= fun () ->
      second_pass None ([], []) [] 0
      >>=? fun (oldest_header_opt, rev_block_hashes, protocol_datas) ->
      Tezos_stdlib_unix.Utils.display_progress_end () ;
      lwt_log_notice (fun f -> f "finished reading snapshot")
      >>= fun () ->
      return
        ( block_header,
          block_data,
          version.mode,
          oldest_header_opt,
          rev_block_hashes,
          protocol_datas )
    in
    (* Check snapshot version *)
    read_snapshot_metadata rbuf
    >>=? fun version ->
    check_version version
    >>=? fun () ->
    Lwt.catch
      (fun () ->
        match Environment_variable.get dune_print_snapshot with
        | exception Not_found ->
            restore version
        | s ->
            let start = try int_of_string s with _ -> 0 in
            let rec iter n =
              get_command version.version rbuf
              >>=? fun c ->
              let s =
                Data_encoding.Json.to_string
                  (Data_encoding.Json.construct command_encoding c)
              in
              if n >= start then Printf.eprintf "command[%d]: %s\n%!" n s ;
              iter (n + 1)
            in
            iter 0)
      (function
        | Unix.Unix_error (e, _, _) ->
            fail @@ System_read_error (Unix.error_message e)
        | err ->
            Lwt.fail err)
end
