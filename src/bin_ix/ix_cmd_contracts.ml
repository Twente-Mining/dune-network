(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types
open Ix_common

let get c =
  Printf.eprintf "Downloading contracts into contracts.json\n%!" ;
  call
    c
    ~set_bindir
    ~cmd:"dune-client"
    ~args:
      [ "rpc";
        "get";
        "/chains/main/blocks/head/context/raw/json/contracts/index?depth=3";
        "-o";
        "contracts.json" ]

let rows () =
  Printf.eprintf "Translating contracts.json to contracts.rows\n%!" ;
  Ix_contracts.translate_to_rows "contracts.json" "contracts.rows"

let ezcmd c =
  Ix_common.ezcmd
    "contracts"
    ~action:(fun () -> get c ; rows ())
    ~doc:"Download all contracts and format them as text rows"
