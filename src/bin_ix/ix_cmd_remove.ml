(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(* TODO:
   * currently, we allow removal of a current node: either we forbid it, or
     we should prevent switching back to it.

*)

open Ix_types

let command current switch =
  let old_switch = current.current_switch in
  let (switch, nodes, clients) =
    Ix_common.parse_switch ~default_switch:old_switch switch
  in
  let current_state =
    try Ix_common.load_current_switch_state switch
    with Ix_common.SwitchDoesNotExist name ->
      Printf.eprintf "Error: switch %S does not exist.\n%!" name ;
      exit 2
  in
  let current = Ix_common.check_switch current_state in
  match (nodes, clients) with
  | ([], []) -> (
    match switch with
    | "mainnet" | "testnet" ->
        Printf.eprintf "Error: switch %S cannot be removed\n%!" switch ;
        exit 2
    | _ ->
        if old_switch = switch then (
          Printf.eprintf "Error: cannot remove current switch %S\n%!" switch ;
          exit 2 ) ;
        Ix_common.remove_dir (Ix_common.switch_dir switch) ;
        Printf.eprintf "Switch %S removed\n%!" switch )
  | _ ->
      List.iter
        (fun node ->
          if node = "dunscan" then (
            Printf.eprintf "Error: cannot remove implicit switch %S\n%!" node ;
            exit 2 ) ;
          if node = current.current_node.node_name then (
            Printf.eprintf "Error: cannot remove current node %S\n%!" node ;
            exit 2 ) ;
          if
            List.for_all
              (fun n -> n.node_name <> node)
              current.current_switch_state.switch_nodes
          then
            Printf.eprintf
              "Error: node %S does not exist in switch %S\n%!"
              node
              switch)
        nodes ;
      List.iter
        (fun client ->
          if client = "client" then (
            Printf.eprintf "Error: cannot remove default client\n%!" ;
            exit 2 ) ;
          if client = current.current_client.client_name then (
            Printf.eprintf "Error: cannot remove current client %S\n%!" client ;
            exit 2 ) ;
          if
            List.for_all
              (fun n -> n.client_name <> client)
              current.current_switch_state.switch_clients
          then
            Printf.eprintf
              "Error: client %S does not exist in switch %S\n%!"
              client
              switch)
        clients ;
      let switch_state = current.current_switch_state in
      let switch_state =
        List.fold_left
          (fun switch_state node ->
            let switch_nodes =
              List.fold_left
                (fun switch_nodes n ->
                  if n.node_name = node then (
                    ( Printf.eprintf "Removing node %s.%s\n%!" switch node ;
                      match Ix_common.node_dir current n with
                      | None ->
                          ()
                      | Some node_dir ->
                          Ix_common.remove_dir node_dir ) ;
                    switch_nodes )
                  else n :: switch_nodes)
                []
                switch_state.switch_nodes
            in
            let switch_state = {switch_state with switch_nodes} in
            Ix_common.save_switch_state switch switch_state ;
            switch_state)
          switch_state
          nodes
      in
      let switch_state =
        List.fold_left
          (fun switch_state client ->
            let switch_clients =
              List.fold_left
                (fun switch_clients c ->
                  if c.client_name = client then (
                    Printf.eprintf "Removing client %s.%s\n%!" switch client ;
                    let client_dir = Ix_common.client_dir current c in
                    Ix_common.remove_dir client_dir ;
                    switch_clients )
                  else c :: switch_clients)
                []
                switch_state.switch_clients
            in
            let switch_state = {switch_state with switch_clients} in
            Ix_common.save_switch_state switch switch_state ;
            switch_state)
          switch_state
          clients
      in
      let _ = switch_state in
      ()

let ezcmd c =
  let open Ezcmd.Modules in
  let cmd_doc = "Remove a switch/network" in
  let cmd_args =
    [ ( [],
        Arg.Anons (fun switches -> List.iter (command c) switches),
        Ezcmd.info "NETWORK Remove switch NETWORK" ) ]
  in
  let cmd_action () = () in
  {
    Arg.cmd_name = "remove";
    cmd_action;
    cmd_args;
    cmd_doc;
    cmd_man = [`P cmd_doc];
  }
