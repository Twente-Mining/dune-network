(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Tezos_data_encoding

type current_state = {switch : string; node : string; client : string}

let default_current_state =
  {switch = "mainnet"; node = "dunscan"; client = "client"}

type local_node_state = {
  mutable node_net_addr : string;
  mutable node_net_port : int;
}

type node_state = {
  node_name : string;
  mutable node_rpc_addr : string;
  mutable node_rpc_port : int;
  mutable node_local : local_node_state option;
}

type client_state = {client_name : string}

type account_state = {
  account_name : string;
  account_pkh : string;
  account_pk : string option;
  account_sk : string option;
  account_is_baker : bool;
}

type switch_state = {
  mutable switch_nodes : node_state list;
  mutable switch_clients : client_state list;
  mutable switch_accounts : account_state list;
  mutable switch_activated : bool;
}

type current = {
  current_switch : string;
  current_switch_state : switch_state;
  current_node : node_state;
  current_client : client_state;
}

module ENCODINGS = struct
  open Data_encoding

  let current_state =
    conv
      (fun x -> (x.switch, x.node, x.client))
      (fun (switch, node, client) -> {switch; node; client})
    @@ obj3 (req "switch" string) (req "node" string) (req "client" string)

  let local_node_state =
    conv
      (fun x -> (x.node_net_addr, x.node_net_port))
      (fun (node_net_addr, node_net_port) -> {node_net_addr; node_net_port})
    @@ obj2 (req "net_addr" string) (req "net_port" int31)

  let node_state =
    conv
      (fun x -> (x.node_name, x.node_local, x.node_rpc_addr, x.node_rpc_port))
      (fun (node_name, node_local, node_rpc_addr, node_rpc_port) ->
        {node_name; node_local; node_rpc_addr; node_rpc_port})
    @@ obj4
         (req "name" string)
         (opt "local" local_node_state)
         (req "rpc_addr" string)
         (req "rpc_port" int31)

  let client_state =
    conv (fun x -> x.client_name) (fun client_name -> {client_name})
    @@ obj1 (req "name" string)

  let account_state =
    conv
      (fun x ->
        ( x.account_name,
          x.account_pkh,
          x.account_pk,
          x.account_sk,
          x.account_is_baker ))
      (fun (account_name, account_pkh, account_pk, account_sk, account_is_baker)
           ->
        {account_name; account_pkh; account_pk; account_sk; account_is_baker})
    @@ obj5
         (req "name" string)
         (req "pkh" string)
         (opt "pk" string)
         (opt "sk" string)
         (dft "is_baker" bool false)

  let switch_state =
    conv
      (fun x ->
        ( x.switch_nodes,
          x.switch_clients,
          x.switch_accounts,
          x.switch_activated ))
      (fun (switch_nodes, switch_clients, switch_accounts, switch_activated) ->
        {switch_nodes; switch_clients; switch_accounts; switch_activated})
    @@ obj4
         (req "nodes" (list node_state))
         (req "clients" (list client_state))
         (dft "accounts" (list account_state) [])
         (dft "activated" bool true)
end

let set_bindir = true

let homedir =
  try Sys.getenv "HOME"
  with Not_found ->
    Printf.eprintf "Error: variable HOME not defined\n%!" ;
    exit 2

open Dune_config.Dune_std.OP

let dunedir = try Sys.getenv "IX_DIR" with Not_found -> homedir // ".dune"

let in_docker =
  match Sys.getenv "IX_IN_DOCKER" with
  | exception Not_found ->
      false
  | "n" | "no" | "N" ->
      false
  | _ ->
      true

let always_root =
  match Sys.getenv "IX_ALWAYS_ROOT" with
  | exception Not_found ->
      false
  | "n" | "no" | "N" ->
      false
  | _ ->
      true

let cwd = Sys.getcwd ()
