(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let max_files = ref 10

let rotate_file file =
  let rec iter file filebase n =
    if Sys.file_exists file then (
      if n > !max_files then (
        Printf.eprintf "ix rotate: remove %s\n%!" file ;
        Sys.remove file )
      else
        let next_file = Printf.sprintf "%s.%d" filebase n in
        iter next_file filebase (n + 1) ;
        Printf.eprintf "ix rotate: rename %s to %s\n%!" file next_file ;
        Sys.rename file next_file )
  in
  iter file file 1

open Ezcmd.Modules

let cmd_args =
  [ ( ["max"],
      Arg.Int (( := ) max_files),
      Ezcmd.info "VAL Maximal log file number" );
    ( [],
      Arg.Anons (fun list -> List.iter rotate_file list),
      Ezcmd.info "FILE rotate this file" ) ]

let ezcmd =
  {
    Arg.cmd_name = "rotate";
    cmd_action = (fun () -> ());
    cmd_args;
    cmd_man = [`P "Rename file ARG to ARG.1, ARG.1 to ARG.2, etc."];
    cmd_doc = "Rotate log files";
  }
