(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_config
open Dune_std.OP
open Ix_types

let bindir =
  ref
    ( try Sys.getenv "DUNE_BINDIR"
      with Not_found -> Filename.dirname Sys.executable_name )

let dunelink = dunedir // "CURRENT"

let duneswitches = dunedir // "switches"

let dune_client_mainnet_dir =
  if always_root then duneswitches // "mainnet/client"
  else homedir // ".dune-client"

let dune_node_mainnet_dir =
  if always_root then duneswitches // "mainnet/node"
  else homedir // ".dune-node"

let dune_client_testnet_dir =
  if always_root then duneswitches // "testnet/client"
  else homedir // ".dune-testnet-client"

let dune_node_testnet_dir =
  if always_root then duneswitches // "testnet/node"
  else homedir // ".dune-testnet-node"

let mkdir dir =
  if not (Sys.file_exists dir) then (
    try Unix.mkdir dir 0o755
    with exn ->
      Printf.eprintf
        "Error: could not create %S, exception %s\n%!"
        dir
        (Printexc.to_string exn) ;
      exit 2 )

let dunedir_must_exist () =
  mkdir dunedir ;
  mkdir duneswitches ;
  mkdir (duneswitches // "mainnet") ;
  mkdir (duneswitches // "testnet") ;
  mkdir dune_client_testnet_dir ;
  mkdir dune_client_mainnet_dir ;
  ()

let node_num node =
  if node = "node" then 0
  else
    let len = String.length node in
    int_of_string (String.sub node 4 (len - 4))

let client_num client =
  if client = "client" then 0
  else
    let len = String.length client in
    int_of_string (String.sub client 6 (len - 6))

let not_a_switch s =
  match s with
  | "dunscan" ->
      true
  | _ ->
      Dune_std.STRING.starts_with s ~prefix:"node"
      || Dune_std.STRING.starts_with s ~prefix:"client"

let parse_switch ~default_switch switch =
  let parse_words switch words =
    let switch = if switch = "_" then default_switch else switch in
    let (nodes, clients) =
      List.fold_left
        (fun (nodes, clients) word ->
          if word = "node0" then ("node" :: nodes, clients)
          else if
            ( word = "dunscan"
            && match switch with "mainnet" | "testnet" -> true | _ -> false )
            || Dune_std.STRING.starts_with word ~prefix:"node"
               &&
               try
                 ignore (node_num word) ;
                 true
               with _ -> false
          then (word :: nodes, clients)
          else if word = "client0" then (nodes, "client" :: clients)
          else if
            Dune_std.STRING.starts_with word ~prefix:"client"
            &&
            try
              ignore (client_num word) ;
              true
            with _ -> false
          then (nodes, word :: clients)
          else (
            Printf.eprintf
              "Error: cannot parse switch name attribute %S\n%!"
              word ;
            exit 2 ))
        ([], [])
        words
    in
    (switch, nodes, clients)
  in
  let words = Dune_std.STRING.split switch '.' in
  match words with
  | [] ->
      Printf.eprintf "Error: empty switch string\n%!" ;
      exit 2
  | s :: _ when not_a_switch s ->
      parse_words "_" words
  | switch :: words ->
      parse_words switch words

let mainnet_switch =
  {
    switch_nodes =
      [ {
          node_name = "dunscan";
          node_rpc_addr = "mainnet-node.dunscan.io";
          node_rpc_port = 8733;
          node_local = None;
        } ];
    switch_clients = [{client_name = "client"}];
    switch_accounts = [];
    switch_activated = true;
  }

let testnet_switch =
  {
    switch_nodes =
      [ {
          node_name = "dunscan";
          node_rpc_addr = "testnet-node.dunscan.io";
          node_rpc_port = 8734;
          node_local = None;
        } ];
    switch_clients = [{client_name = "client"}];
    switch_accounts = [];
    switch_activated = true;
  }

exception SwitchDoesNotExist of string

let switch_dir switch = duneswitches // switch

let switch_file switch = switch_dir switch // "STATE"

let load_switch_state switch =
  let filename = switch_file switch in
  if Sys.file_exists filename then (
    try Dune_std.FILE.read_json filename Ix_types.ENCODINGS.switch_state
    with _ ->
      Printf.eprintf "Error: invalid format for file %S\n" filename ;
      exit 2 )
  else
    match switch with
    | "mainnet" ->
        mainnet_switch
    | "testnet" ->
        testnet_switch
    | _ ->
        raise (SwitchDoesNotExist switch)

let save_switch_state switch switch_state =
  dunedir_must_exist () ;
  mkdir (duneswitches // switch) ;
  let filename = switch_file switch in
  Dune_std.FILE.write_json
    filename
    switch_state
    Ix_types.ENCODINGS.switch_state

let current_switch_file switch = duneswitches // switch // "CURRENT"

let load_current_switch_state switch =
  let file = current_switch_file switch in
  if Sys.file_exists file then
    Dune_std.FILE.read_json file Ix_types.ENCODINGS.current_state
  else
    match switch with
    | "mainnet" ->
        {switch = "mainnet"; node = "dunscan"; client = "client"}
    | "testnet" ->
        {switch = "testnet"; node = "dunscan"; client = "client"}
    | _ ->
        raise (SwitchDoesNotExist switch)

let load_current_state () =
  let ic = open_in dunelink in
  let switch = input_line ic in
  close_in ic ;
  try load_current_switch_state switch
  with SwitchDoesNotExist switch ->
    Printf.eprintf "Internal Error: switch %S does not exist\n%!" switch ;
    exit 2

let current_state_of_current current =
  {
    switch = current.current_switch;
    node = current.current_node.node_name;
    client = current.current_client.client_name;
  }

let save_current_state current =
  dunedir_must_exist () ;
  let current_state = current_state_of_current current in
  Dune_std.FILE.write_json
    (duneswitches // current_state.switch // "CURRENT")
    current_state
    Ix_types.ENCODINGS.current_state ;
  let oc = open_out dunelink in
  output_string oc current_state.switch ;
  close_out oc ;
  Printf.eprintf
    "Current switch: %s.%s.%s\n%!"
    current_state.switch
    current_state.node
    current_state.client

let current_state_of_switch ~current_state s =
  let (switch, nodes, clients) =
    parse_switch ~default_switch:current_state.switch s
  in
  let current_state =
    try load_current_switch_state switch
    with SwitchDoesNotExist switch ->
      Printf.eprintf "Error: switch %S does not exist\n%!" switch ;
      exit 2
  in
  let node =
    match nodes with
    | [] ->
        current_state.node
    | [node] ->
        node
    | _ ->
        Printf.eprintf "Error: %S specifies several nodes\n%!" s ;
        exit 2
  in
  let client =
    match clients with
    | [] ->
        current_state.client
    | [client] ->
        client
    | _ ->
        Printf.eprintf "Error: %S specifies several clients\n%!" s ;
        exit 2
  in
  {switch; node; client}

let check_switch current_state =
  let current_switch = current_state.switch in
  let current_switch_state =
    try load_switch_state current_switch
    with SwitchDoesNotExist switch ->
      Printf.eprintf "Error: switch %S does not exist.\n%!" switch ;
      exit 2
  in
  let current_node =
    match
      List.fold_left
        (fun acc node ->
          if node.node_name = current_state.node then Some node else acc)
        None
        current_switch_state.switch_nodes
    with
    | None ->
        Printf.eprintf
          "Error: node %S does not exist in switch %S.\n%!"
          current_state.node
          current_state.switch ;
        exit 2
    | Some node ->
        node
  in
  let current_client =
    match
      List.fold_left
        (fun acc client ->
          if client.client_name = current_state.client then Some client
          else acc)
        None
        current_switch_state.switch_clients
    with
    | None ->
        Printf.eprintf
          "Error: client %S does not exist in switch %S.\n%!"
          current_state.client
          current_state.switch ;
        exit 2
    | Some client ->
        client
  in
  {current_switch; current_switch_state; current_node; current_client}

let config_file_of_switch switch =
  match switch with
  | "mainnet" | "testnet" ->
      switch
  | switch ->
      duneswitches // switch // (switch ^ "-config.json")

let protocol_parameters_file_of_switch switch =
  match switch with
  | "mainnet" | "testnet" ->
      assert false
  | switch ->
      duneswitches // switch // (switch ^ "-parameters.json")

let node_dir c node =
  match node.node_local with
  | None ->
      None
  | Some _ ->
      Some
        ( match (c.current_switch, node.node_name) with
        | ("mainnet", "node") ->
            dune_node_mainnet_dir
        | ("testnet", "node") ->
            dune_node_testnet_dir
        | (switch, node) ->
            duneswitches // switch // node )

let client_dir c client =
  match (c.current_switch, client.client_name) with
  | ("mainnet", "client") ->
      dune_client_mainnet_dir
  | ("testnet", "client") ->
      dune_client_testnet_dir
  | (switch, client) ->
      duneswitches // switch // client

let snapshots_dir c =
  let _node_dir = node_dir c c.current_node in
  let switch_dir = switch_dir c.current_switch in
  switch_dir // "snapshots"

let get_node_args ?(local_only = false) c =
  let node_args = [] in
  let node_args =
    match node_dir c c.current_node with
    | None ->
        if local_only then (
          Printf.eprintf "Error: cannot run command on remote node\n%!" ;
          exit 2 )
        else node_args
    | Some dir ->
        "--data-dir" :: dir :: node_args
  in
  node_args

let get_client_args c =
  let node_args =
    [ "-A";
      c.current_node.node_rpc_addr;
      "-P";
      string_of_int c.current_node.node_rpc_port ]
  in
  let client_dir = client_dir c c.current_client in
  let client_args = ["-d"; client_dir] in
  node_args @ client_args

let command ?(set_bindir = false) c ~cmd ~args f =
  ( match c.current_switch with
  | "mainnet" | "testnet" ->
      Unix.putenv "DUNE_CONFIG" c.current_switch
  | switch ->
      let config = config_file_of_switch switch in
      if Sys.file_exists config then Unix.putenv "DUNE_CONFIG" config ) ;
  Unix.putenv "DUNE_CLIENT_UNSAFE_DISABLE_DISCLAIMER" "y" ;
  Unix.putenv "DUNE_CONTEXT_STORAGE" "ironmin" ;
  let before_args =
    match cmd with
    | "dune-baker-all"
    | "dune-endorser-all"
    | "dune-accuser-all"
    | "dune-baker-005-PsBabyM1"
    | "dune-endorser-005-PsBabyM1"
    | "dune-accuser-005-PsBabyM1"
    | "dune-signer"
    | "dune-admin-client"
    | "dune-client" ->
        get_client_args c
    | "local-dune-node" | "dune-node" ->
        []
    | _ ->
        []
  in
  let after_args =
    match cmd with
    | "dune-baker-all"
    | "dune-endorser-all"
    | "dune-accuser-all"
    | "dune-baker-005-PsBabyM1"
    | "dune-endorser-005-PsBabyM1"
    | "dune-accuser-005-PsBabyM1"
    | "dune-signer"
    | "dune-admin-client"
    | "dune-client" ->
        []
    | "local-dune-node" ->
        get_node_args ~local_only:true c
    | "dune-node" ->
        get_node_args c
    | _ ->
        []
  in
  let cmd = match cmd with "local-dune-node" -> "dune-node" | _ -> cmd in
  let cmd = if set_bindir then !bindir // cmd else cmd in
  let args = (cmd :: before_args) @ args @ after_args in
  Printf.eprintf "COMMAND: %s\n%!" (String.concat " " args) ;
  let args = Array.of_list args in
  f cmd args

let exec ?set_bindir c ~cmd ~args =
  command c ?set_bindir ~cmd ~args Unix.execvp

let call ?set_bindir c ~cmd ~args =
  command c ?set_bindir ~cmd ~args (fun cmd args ->
      let pid =
        Unix.create_process cmd args Unix.stdin Unix.stdout Unix.stderr
      in
      let (_pid, status) = Dune_std.UNIX.waitpid [] pid in
      match status with
      | Unix.WEXITED 0 ->
          ()
      | Unix.WEXITED n ->
          Printf.eprintf "Error: returned status %d\n%!" n ;
          exit 2
      | _ ->
          assert false)

let remove_dir dir =
  if Sys.file_exists dir then
    if Printf.kprintf Sys.command "rm -rf %s" dir <> 0 then (
      Printf.eprintf "Error: could not remove dir %S\n%!" dir ;
      exit 2 )

let ezcmd ?(action = fun () -> ()) ?args ?all ?doc ?man cmd_name =
  let open Ezcmd.Modules in
  let cmd_args = match args with Some args -> args | None -> [] in
  let cmd_args =
    match all with
    | None ->
        cmd_args
    | Some f ->
        cmd_args @ [([], Arg.Anons f, Ezcmd.info "XXX apply on XXX")]
  in
  let cmd_action = action in
  let cmd_doc =
    match doc with
    | Some doc ->
        doc
    | None ->
        Printf.sprintf "Call %s subcommand" cmd_name
  in
  let cmd_man = match man with Some man -> man | None -> [`P cmd_doc] in
  {Arg.cmd_name; cmd_action; cmd_args; cmd_doc; cmd_man}

let daemon ?set_bindir c ~log ~cmd ~args =
  command c ?set_bindir ~cmd ~args (fun cmd args ->
      let oc = open_out log in
      let fd = Unix.descr_of_out_channel oc in
      let pid = Unix.create_process cmd args Unix.stdin fd fd in
      pid)

let waitpid pid =
  let (_pid, status) = Dune_std.UNIX.waitpid [] pid in
  match status with
  | Unix.WEXITED 0 ->
      ()
  | Unix.WEXITED n ->
      Printf.eprintf "Error: returned status %d\n%!" n ;
      exit 2
  | _ ->
      assert false
