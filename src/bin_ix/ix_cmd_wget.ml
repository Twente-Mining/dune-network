(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

let ( >>= ) = Lwt.( >>= )

type r = Content of string | HttpError of int * string | Error of string

let get url =
  Printf.eprintf "Downloading %s ...\n%!" url ;
  let (waiter, finalizer) = Lwt.wait () in
  let result = ref None in
  let reply r =
    result := Some r ;
    Lwt.wakeup finalizer () ;
    Lwt.return_unit
  in
  Lwt.async (fun () ->
      Lwt.catch
        (fun () ->
          let headers = Cohttp.Header.add_list (Cohttp.Header.init ()) [] in
          Cohttp_lwt_unix.Client.get ~headers (Uri.of_string url)
          >>= fun (resp, body) ->
          let code =
            resp |> Cohttp.Response.status |> Cohttp.Code.code_of_status
          in
          Cohttp_lwt.Body.to_string body
          >>= fun body ->
          if code = 200 then reply (Content body)
          else reply (HttpError (code, body)))
        (fun exn -> reply (Error (Printexc.to_string exn)))) ;
  Lwt_main.run waiter ;
  match !result with None -> assert false | Some r -> r

let get_or_exit f ~url =
  match get url with
  | Content body ->
      f body
  | Error exn ->
      Printf.eprintf "Internal error: %s\n%!" exn ;
      exit 2
  | HttpError (code, body) ->
      Printf.eprintf "Http Error %d: %s\n%!" code body ;
      exit 2

let command ?output url =
  get_or_exit ~url (fun body ->
      match output with
      | None ->
          Printf.printf "%s\n%!" body
      | Some filename ->
          let oc = open_out filename in
          Printf.fprintf oc "%s" body ;
          close_out oc ;
          Printf.eprintf "Content saved in file %S\n%!" filename)

let ezcmd =
  let output = ref None in
  Ix_common.ezcmd
    "wget"
    ~args:
      Ezcmd.Modules.
        [ ( ["o"],
            Arg.String (fun s -> output := Some s),
            Ezcmd.info "FILE File to save result to" );
          ( [],
            Arg.Anon (0, fun arg -> command ?output:!output arg),
            Ezcmd.info "URL Download URL" ) ]
    ~doc:"Download a webpage by URL"
