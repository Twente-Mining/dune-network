(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_config
open Dune_std.OP
open Ix_types

let max_snapshot_age = 2

let command c args =
  match Ix_common.node_dir c c.current_node with
  | None ->
      Printf.eprintf "Error: not a local node\n%!" ;
      exit 2
  | Some node_dir ->
      if Sys.file_exists (node_dir // "context") then (
        Printf.eprintf
          "Error: node %S has already a storage.\n"
          c.current_node.node_name ;
        Printf.eprintf "  Use: 'ix reset' to clear node storage before\n" ;
        Printf.eprintf "%!" ;
        exit 2 )
      else
        let snapshots_dir = Ix_common.snapshots_dir c in
        let args =
          match args with
          | _ :: _ ->
              args
          | [] -> (
              Ix_common.mkdir snapshots_dir ;
              let snapshots = Sys.readdir snapshots_dir in
              try
                match snapshots with
                | [||] ->
                    raise Not_found
                | _ ->
                    Array.sort compare snapshots ;
                    let snapshot = snapshots.(Array.length snapshots - 1) in
                    if
                      match c.current_switch with
                      | "mainnet" | "testnet" -> (
                        match Dune_std.STRING.split snapshot '_' with
                        | [_network; _mode; date; _level; _block] -> (
                          match Ptime.of_rfc3339 date with
                          | Ok (date, _, _) ->
                              let diff =
                                Unix.gettimeofday () -. Ptime.to_float_s date
                              in
                              let diff = diff /. 86400. in
                              let diff = int_of_float diff in
                              Printf.eprintf "diff: %d days\n%!" diff ;
                              if diff < max_snapshot_age then true
                              else (
                                Printf.eprintf
                                  "Discarding snapshot %s, too old (%d days)\n\
                                   %!"
                                  snapshot
                                  diff ;
                                false )
                          | _ ->
                              false )
                        | _ ->
                            false )
                      | _ ->
                          true
                    then (
                      Printf.eprintf "Loading saved snapshot %s\n%!" snapshot ;
                      [snapshots_dir // snapshot] )
                    else raise Not_found
              with Not_found -> (
                try
                  match c.current_switch with
                  | "mainnet" | "testnet" -> (
                      let url =
                        Printf.sprintf
                          "https://dune.network/files/snapshots/%s"
                          c.current_switch
                      in
                      match Ix_cmd_wget.get (url // "LAST.txt") with
                      | HttpError (_, body) | Error body ->
                          Printf.eprintf
                            "Error: could not retrieve remote snapshot:\n\
                             %s\n\
                             %!"
                            body ;
                          raise Not_found
                      | Content snapshot -> (
                          let snapshot = String.trim snapshot in
                          Printf.eprintf
                            "Downloading latest snapshot %s\n%!"
                            snapshot ;
                          match Ix_cmd_wget.get (url // snapshot) with
                          | HttpError (_, body) | Error body ->
                              Printf.eprintf
                                "Error: could not retrieve remote snapshot:\n\
                                 %s\n\
                                 %!"
                                body ;
                              raise Not_found
                          | Content body ->
                              let snapshot_file = snapshots_dir // snapshot in
                              let oc = open_out_bin snapshot_file in
                              output_string oc body ;
                              close_out oc ;
                              [snapshot_file] ) )
                  | _ ->
                      raise Not_found
                with Not_found ->
                  Printf.eprintf "Error: no snapshot available\n" ;
                  Printf.eprintf "   Use: 'dun save' to create one\n" ;
                  Printf.eprintf "%!" ;
                  exit 2 ) )
        in
        Ix_common.exec
          c
          ~set_bindir
          ~cmd:"local-dune-node"
          ~args:(["snapshot"; "import"] @ args)

(*
dun wget https://dune.network/files/snapshots/mainnet/LAST.txt
dun wget https://dune.network/files/snapshots/testnet/LAST.txt
*)

let ezcmd c =
  Ix_common.ezcmd
    "restore"
    ~all:(fun args -> command c args)
    ~doc:"Restore the current node from the last snapshot"
