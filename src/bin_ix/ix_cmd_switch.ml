(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types

let command c switch =
  match switch with
  | None ->
      Printf.printf "Current switch: %s\n" c.current_switch ;
      Printf.printf "  node:   %s\n" c.current_node.node_name ;
      Printf.printf
        "     addr: %s:%d\n"
        c.current_node.node_rpc_addr
        c.current_node.node_rpc_port ;
      Printf.printf "  client: %s\n" c.current_client.client_name ;
      Printf.printf
        "  DUNE_CONFIG= %s\n"
        (Ix_common.config_file_of_switch c.current_switch) ;
      Printf.printf "  DUNE_BINDIR= %s\n" !Ix_common.bindir ;
      Printf.printf "%!"
  | Some switch ->
      let current_state = Ix_common.current_state_of_current c in
      let current_state =
        Ix_common.current_state_of_switch ~current_state switch
      in
      let current = Ix_common.check_switch current_state in
      Ix_common.save_current_state current ;
      ()

let ezcmd c =
  let switch = ref None in
  Ix_common.ezcmd
    "switch"
    ~args:
      Ezcmd.Modules.
        [ ( [],
            Arg.Anon (0, fun arg -> switch := Some arg),
            Ezcmd.info "SWITCH Switch to this resource" ) ]
    ~action:(fun () -> command c !switch)
    ~doc:"Switch to a resource network/node/client"
