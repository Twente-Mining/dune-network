(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types

let command c switch =
  let current_switch_state =
    try Ix_common.load_switch_state switch
    with Ix_common.SwitchDoesNotExist switch ->
      Printf.eprintf "Error: switch %S does not exist.\n%!" switch ;
      exit 2
  in
  Printf.printf
    "Switch %S%s:\n"
    switch
    (if c.current_switch = switch then " (current)" else "") ;
  Printf.printf "  nodes: %d\n" (List.length current_switch_state.switch_nodes) ;
  List.iter
    (fun node ->
      Printf.printf
        "    %s%s\n"
        node.node_name
        ( if
          c.current_switch = switch
          && c.current_node.node_name = node.node_name
        then " (current)"
        else "" ))
    current_switch_state.switch_nodes ;
  Printf.printf
    "  clients: %d\n"
    (List.length current_switch_state.switch_clients) ;
  List.iter
    (fun client ->
      Printf.printf
        "    %s%s\n"
        client.client_name
        ( if
          c.current_switch = switch
          && c.current_client.client_name = client.client_name
        then " (current)"
        else "" ))
    current_switch_state.switch_clients

open Ezcmd.Modules

let ezcmd c =
  let cmd_doc = "Print information on a network" in
  let networks = ref [] in
  let cmd_args =
    [ ( [],
        Arg.Anons (fun list -> networks := list),
        Ezcmd.info "NETWORK Print information on NETWORK" ) ]
  in
  let cmd_action () =
    let networks =
      match !networks with
      | [] ->
          Printf.eprintf " (use 'ix help' for help)\n%!" ;
          [c.current_switch]
      | list ->
          list
    in
    List.iter (command c) networks
  in
  {
    Arg.cmd_name = "info";
    cmd_action;
    cmd_args;
    cmd_doc;
    cmd_man = [`P cmd_doc];
  }
