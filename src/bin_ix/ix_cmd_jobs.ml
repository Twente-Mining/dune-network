(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

(*
open Dune_config
open Ix_types
*)

open Dune_config.Dune_std.OP

let command c =
  let node_lock_file = Ix_cmd_bake.node_dir c // "lock" in
  if Sys.file_exists node_lock_file then
    let pid =
      let ic = open_in node_lock_file in
      let v = input_line ic in
      close_in ic ;
      try int_of_string v
      with _ ->
        Printf.eprintf "Error: incorrect format for file %S\n%!" node_lock_file ;
        exit 2
    in
    match Unix.kill pid 0 with
    | exception _exn ->
        Printf.eprintf
          "Lock file %S exists, but node seems down. Cleaning up.\n%!"
          node_lock_file ;
        Sys.remove node_lock_file
    | _ ->
        Printf.eprintf "Node is running with pid %d\n%!" pid
  else Printf.eprintf "Node is not running\n%!"

let ezcmd c =
  Ix_common.ezcmd
    "jobs"
    ~action:(fun () -> command c)
    ~doc:"Check status of processes"
