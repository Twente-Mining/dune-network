(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_config
open Ix_types

let bindir = Ix_common.bindir

let command_args c args =
  let args = ref args in
  List.iter
    (fun node ->
      if node != c.current_node then
        match node.node_local with
        | None ->
            ()
        | Some local ->
            args :=
              [ "--peer";
                Printf.sprintf "%s:%d" local.node_net_addr local.node_net_port
              ]
              @ !args)
    c.current_switch_state.switch_nodes ;
  let args = (* "--singleprocess" :: *) !args in
  let args =
    if Dune_std.STRING.starts_with c.current_switch ~prefix:"sandbox" then
      "--bootstrap-threshold" :: "0" :: "--connections" :: "0" :: args
    else args
  in
  let cmd = "local-dune-node" in
  let args = "run" :: args in
  (cmd, args)

let command c args =
  let (cmd, args) = command_args c args in
  Ix_common.exec c ~set_bindir ~cmd ~args

let ezcmd c =
  Ix_common.ezcmd "run" ~all:(command c) ~doc:"Start the current node"
