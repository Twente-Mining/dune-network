(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_config
open Tezos_data_encoding

module Parametric = Protocol_parameters.MAKE_PARAMETRIC_005 (struct
  type period = int64

  type tez = int64
end)

module Parameters = Protocol_parameters.MAKE_PARAMETERS_005 (struct
  type public_key_hash = string

  type public_key = string

  type tez = int64

  type parametric = Parametric.parametric

  (* We do not support these parameters for now *)
  type commitment = int

  type script = int
end)

include Parametric
include Parameters

let public_key_hash_encoding = Data_encoding.string

let public_key_encoding = Data_encoding.string

let period_encoding = Data_encoding.int64

let tez_encoding = Data_encoding.int64

(* We do not support these encodings for now *)
let commitment_encoding = Data_encoding.int31

let script_encoding = Data_encoding.int31

let parametric_encoding =
  let open Data_encoding in
  conv
    (fun c ->
      ( ( c.preserved_cycles,
          c.blocks_per_cycle,
          c.blocks_per_commitment,
          c.blocks_per_roll_snapshot,
          c.blocks_per_voting_period,
          c.time_between_blocks,
          c.endorsers_per_block,
          c.hard_gas_limit_per_operation,
          c.hard_gas_limit_per_block,
          c.hard_gas_limit_to_pay_fees,
          c.max_operation_ttl,
          c.allow_collect_call ),
        ( ( c.proof_of_work_threshold,
            c.tokens_per_roll,
            c.michelson_maximum_type_size,
            c.seed_nonce_revelation_tip,
            c.origination_size,
            c.block_security_deposit,
            c.endorsement_security_deposit,
            c.block_reward ),
          ( c.endorsement_reward,
            c.cost_per_byte,
            c.hard_storage_limit_per_operation,
            c.test_chain_duration,
            c.quorum_min,
            c.quorum_max,
            c.min_proposal_quorum,
            c.initial_endorsers,
            c.delay_per_missing_endorsement ) ) ))
    (fun ( ( preserved_cycles,
             blocks_per_cycle,
             blocks_per_commitment,
             blocks_per_roll_snapshot,
             blocks_per_voting_period,
             time_between_blocks,
             endorsers_per_block,
             hard_gas_limit_per_operation,
             hard_gas_limit_per_block,
             hard_gas_limit_to_pay_fees,
             max_operation_ttl,
             allow_collect_call ),
           ( ( proof_of_work_threshold,
               tokens_per_roll,
               michelson_maximum_type_size,
               seed_nonce_revelation_tip,
               origination_size,
               block_security_deposit,
               endorsement_security_deposit,
               block_reward ),
             ( endorsement_reward,
               cost_per_byte,
               hard_storage_limit_per_operation,
               test_chain_duration,
               quorum_min,
               quorum_max,
               min_proposal_quorum,
               initial_endorsers,
               delay_per_missing_endorsement ) ) ) ->
      {
        preserved_cycles;
        blocks_per_cycle;
        blocks_per_commitment;
        blocks_per_roll_snapshot;
        blocks_per_voting_period;
        time_between_blocks;
        endorsers_per_block;
        hard_gas_limit_per_operation;
        hard_gas_limit_per_block;
        proof_of_work_threshold;
        tokens_per_roll;
        michelson_maximum_type_size;
        seed_nonce_revelation_tip;
        origination_size;
        block_security_deposit;
        endorsement_security_deposit;
        block_reward;
        endorsement_reward;
        cost_per_byte;
        hard_storage_limit_per_operation;
        test_chain_duration;
        quorum_min;
        quorum_max;
        min_proposal_quorum;
        initial_endorsers;
        delay_per_missing_endorsement;
        hard_gas_limit_to_pay_fees;
        max_operation_ttl;
        allow_collect_call;
      })
    (merge_objs
       (obj12
          (req "preserved_cycles" uint8)
          (req "blocks_per_cycle" int32)
          (req "blocks_per_commitment" int32)
          (req "blocks_per_roll_snapshot" int32)
          (req "blocks_per_voting_period" int32)
          (req "time_between_blocks" (list period_encoding))
          (req "endorsers_per_block" uint16)
          (req "hard_gas_limit_per_operation" z)
          (req "hard_gas_limit_per_block" z)
          (req "hard_gas_limit_to_pay_fees" z)
          (req "max_operation_ttl" int8)
          (req "allow_collect_call" bool))
       (merge_objs
          (obj8
             (req "proof_of_work_threshold" int64)
             (req "tokens_per_roll" tez_encoding)
             (req "michelson_maximum_type_size" uint16)
             (req "seed_nonce_revelation_tip" tez_encoding)
             (req "origination_size" int31)
             (req "block_security_deposit" tez_encoding)
             (req "endorsement_security_deposit" tez_encoding)
             (req "block_reward" tez_encoding))
          (obj9
             (req "endorsement_reward" tez_encoding)
             (req "cost_per_byte" tez_encoding)
             (req "hard_storage_limit_per_operation" z)
             (req "test_chain_duration" int64)
             (req "quorum_min" int32)
             (req "quorum_max" int32)
             (req "min_proposal_quorum" int32)
             (req "initial_endorsers" uint16)
             (req "delay_per_missing_endorsement" period_encoding))))

let bootstrap_account_encoding =
  let open Data_encoding in
  union
    [ case
        (Tag 0)
        ~title:"Public_key_known"
        (tup2 public_key_encoding tez_encoding)
        (function
          | {public_key = Some public_key; amount; _} ->
              Some (public_key, amount)
          | {public_key = None; _} ->
              None)
        (fun (public_key, amount) ->
          {public_key = Some public_key; public_key_hash = public_key; amount});
      case
        (Tag 1)
        ~title:"Public_key_unknown"
        (tup2 public_key_hash_encoding tez_encoding)
        (function
          | {public_key_hash; public_key = None; amount} ->
              Some (public_key_hash, amount)
          | {public_key = Some _; _} ->
              None)
        (fun (public_key_hash, amount) ->
          {public_key = None; public_key_hash; amount}) ]

let bootstrap_contract_encoding =
  let open Data_encoding in
  conv
    (fun {delegate; amount; script} -> (delegate, amount, script))
    (fun (delegate, amount, script) -> {delegate; amount; script})
    (obj3
       (req "delegate" public_key_hash_encoding)
       (req "amount" tez_encoding)
       (req "script" script_encoding))

let encoding =
  let open Data_encoding in
  conv
    (fun { bootstrap_accounts;
           bootstrap_contracts;
           commitments;
           constants;
           security_deposit_ramp_up_cycles;
           no_reward_cycles } ->
      ( ( bootstrap_accounts,
          bootstrap_contracts,
          commitments,
          security_deposit_ramp_up_cycles,
          no_reward_cycles ),
        constants ))
    (fun ( ( bootstrap_accounts,
             bootstrap_contracts,
             commitments,
             security_deposit_ramp_up_cycles,
             no_reward_cycles ),
           constants ) ->
      {
        bootstrap_accounts;
        bootstrap_contracts;
        commitments;
        constants;
        security_deposit_ramp_up_cycles;
        no_reward_cycles;
      })
    (merge_objs
       (obj5
          (req "bootstrap_accounts" (list bootstrap_account_encoding))
          (dft "bootstrap_contracts" (list bootstrap_contract_encoding) [])
          (dft "commitments" (list commitment_encoding) [])
          (opt "security_deposit_ramp_up_cycles" int31)
          (opt "no_reward_cycles" int31))
       parametric_encoding)

let sandbox_constants =
  {
    time_between_blocks = [2L; 3L];
    blocks_per_roll_snapshot = 4l;
    blocks_per_voting_period = 16l;
    blocks_per_cycle = 8l;
    preserved_cycles = 2;
    proof_of_work_threshold = -1L;
    blocks_per_commitment = 4l;
    endorsers_per_block = 32;
    hard_gas_limit_per_operation = Z.of_int 800000;
    hard_gas_limit_per_block = Z.of_int 8000000;
    tokens_per_roll = 8000000000L;
    michelson_maximum_type_size = 1000;
    seed_nonce_revelation_tip = 125000L;
    origination_size = 257;
    block_security_deposit = 512000000L;
    endorsement_security_deposit = 64000000L;
    block_reward = 16000000L;
    endorsement_reward = 2000000L;
    hard_storage_limit_per_operation = Z.of_int 60000;
    cost_per_byte = 1000L;
    test_chain_duration = 1966080L;
    hard_gas_limit_to_pay_fees = Z.of_int 100_000;
    max_operation_ttl = 60;
    allow_collect_call = true;
    quorum_min = 3000l;
    quorum_max = 7000l;
    min_proposal_quorum = 500l;
    initial_endorsers = 1;
    delay_per_missing_endorsement = 1L;
  }

let parameters ~accounts ~constants =
  {
    bootstrap_accounts = accounts;
    (*
      List.init 4 ~f:(fun n ->
          (Account.of_namef "bootacc-%d" n, 4_000_000_000_000L) )
    ];
*)
    bootstrap_contracts = [];
    commitments = [];
    constants;
    security_deposit_ramp_up_cycles = None;
    no_reward_cycles = None;
  }
