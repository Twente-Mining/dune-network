(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_config
open Dune_std.OP
open Ix_types

let main c =
  let call_subcommands subcommands_args =
    let subcommands =
      [ (* Switch management *)
        Ix_cmd_info.ezcmd c;
        Ix_cmd_create.ezcmd c;
        Ix_cmd_switch.ezcmd c;
        Ix_cmd_remove.ezcmd c;
        Ix_cmd_list.ezcmd c;
        (* Node management *)
        Ix_cmd_start.ezcmd c;
        Ix_cmd_stop.ezcmd c;
        Ix_cmd_jobs.ezcmd c;
        Ix_cmd_run.ezcmd c;
        Ix_cmd_activate.ezcmd c;
        Ix_cmd_bake.ezcmd c;
        (* Management of snapshots *)
        Ix_cmd_save.ezcmd c;
        Ix_cmd_reset.ezcmd c;
        Ix_cmd_restore.ezcmd c;
        (* RPCs *)
        Ix_cmd_wget.ezcmd;
        Ix_cmd_rpc.ezcmd c;
        Ix_cmd_contracts.ezcmd c;
        Ix_cmd_www.ezcmd c;
        Ix_cmd_rotate.ezcmd ]
    in
    let proxy_commands =
      List.map
        (fun (name, cmd) ->
          Ezcmd.Modules.
            {
              Arg.cmd_name = name;
              cmd_args =
                [ ( [],
                    Arg.Anons
                      (fun args -> Ix_common.exec c ~set_bindir ~cmd ~args),
                    Ezcmd.info "ARGS Call the command with these arguments" )
                ];
              cmd_doc = Printf.sprintf "Proxy command for %s" cmd;
              cmd_man =
                [ `P
                    (Printf.sprintf
                       "Call the command `%s` with options OPTION and \
                        arguments ARG. Use directly `%s --help` for the help \
                        of the command."
                       cmd
                       cmd) ];
              cmd_action = (fun () -> ());
            })
        [ ("client", "dune-client");
          ("node", "dune-node");
          ("admin", "dune-admin-client");
          ("baker", "dune-baker-all");
          ("endorser", "dune-endorser-all");
          ("accuser", "dune-accuser-all");
          ("signer", "dune-signer") ]
    in
    let subcommands = subcommands @ proxy_commands in
    let help_subcommand =
      Ezcmd.Modules.
        {
          Arg.cmd_name = "help";
          cmd_doc = "Print help on the command";
          cmd_args = [];
          cmd_action =
            (fun () ->
              Printf.printf
                "%s\n"
                (String.concat
                   "\n"
                   ( ["ix COMMAND [OPTIONS] [ARGS]"; ""; "Available commands:"]
                   @ List.map
                       (fun cmd ->
                         Printf.sprintf
                           "  ix %s : %s"
                           cmd.Arg.cmd_name
                           cmd.cmd_doc)
                       subcommands
                   @ [ "";
                       "Use `ix COMMAND --help` for help on a specific command"
                     ] )));
          cmd_man = [];
        }
    in
    let subcommands = subcommands @ [help_subcommand] in
    Ezcmd.main_with_subcommands
      ~name:"ix"
      ~version:"0.1"
      ~doc:"Helper tool for Dune Network"
      ~man:[`P "`ix` is an helper tool to work with Dune Network"]
      ~argv:(Array.of_list ("ix" :: subcommands_args))
      subcommands
  in
  let args = Sys.argv |> Array.to_list in
  let rec iter_global_args args =
    match args with
    | "--version" :: args ->
        Ix_cmd_version.command () ; iter_global_args args
    | "--bindir" :: bindir :: args ->
        Printf.eprintf "xxx\n%!" ;
        Ix_common.bindir :=
          if Filename.is_relative bindir then cwd // bindir else bindir ;
        iter_global_args args
    | _ ->
        args
  in
  match args with
  | [] ->
      assert false
  | _ :: args -> (
      let subcommands_args = iter_global_args args in
      match subcommands_args with
      | [] ->
          Ix_cmd_info.command c c.current_switch ;
          Printf.printf " (use 'ix help' for help)\n%!"
      | ["version"] ->
          Ix_cmd_version.command ()
      (* TODO: jobs management | "jobs" :: [] -> Ix_cmd_jobs.list c |
           "jobs" :: "all" :: "switches" :: [] -> Ix_cmd_jobs.list c
           ~all:true | "stop" :: "node" :: [] -> Ix_cmd_jobs.stop c |
           "stop" :: "switch" :: [] -> Ix_cmd_jobs.stop c ~allnodes:true
           | "stop" :: "switches" :: [] -> Ix_cmd_jobs.stop c
           ~allswitches:true

           TODO: account management ix add account NAME AMOUNT ix add
           baker NAME AMOUNT ix remove account NAME

           TODO: dune export NETWORK.json dune import NETWORK.json dune
           import node *)

      (* Undocumented *)
      (* Utilities for compilation *)
      | "embed" :: files ->
          Ix_cmd_embed.command files
      | cmd :: args -> (
        match cmd with
        | "client"
        | "node"
        | "admin"
        | "baker"
        | "endorser"
        | "accuser"
        | "signer" -> (
          match args with
          | [] ->
              call_subcommands subcommands_args
          | _ ->
              let cmd =
                List.assoc
                  cmd
                  [ ("client", "dune-client");
                    ("node", "dune-node");
                    ("admin", "dune-admin-client");
                    ("baker", "dune-baker-all");
                    ("endorser", "dune-endorser-all");
                    ("accuser", "dune-accuser-all");
                    ("signer", "dune-signer") ]
              in
              Ix_common.exec c ~set_bindir ~cmd ~args )
        | _ ->
            let cmd = "ix-" ^ cmd in
            if Sys.file_exists (!Ix_common.bindir // cmd) then
              Ix_common.exec c ~set_bindir ~cmd ~args
            else call_subcommands subcommands_args ) )

let current_state =
  Ix_common.dunedir_must_exist () ;
  if Sys.file_exists Ix_common.dunelink then (
    try Ix_common.load_current_state ()
    with _ ->
      Printf.eprintf "Error: invalid format for file %S\n" Ix_common.dunelink ;
      exit 2 )
  else default_current_state

let current_state =
  match Sys.getenv "IX_SWITCH" with
  | s ->
      Ix_common.current_state_of_switch ~current_state s
  | exception Not_found -> (
    match Sys.getenv "DUNE_SWITCH" with
    | s ->
        Ix_common.current_state_of_switch ~current_state s
    | exception Not_found ->
        current_state )

let current = Ix_common.check_switch current_state

let () = main current
