(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

module TYPES = struct
  type tez = int64

  type frozen_balance = {
    deposits : int64;
    rewards : int64;
    fees : int64 option;
  }

  type contract = {
    legacy2019 : bool option;
    white_list : string list option;
    nrolls : int option;
    superadmin : bool option;
    delegation_closed : bool option;
    balance : tez option;
    big_map : string list option;
    counter : int64 option;
    delegated : (string * bool) list option;
    frozen_balance : (int * frozen_balance) list option;
    manager : string option;
    spendable : bool option;
    delegatable : bool option;
    roll_list : int option;
    change : tez option;
    delegate : string option;
    delegate_desactivation : int option;
    inactive_delegate : bool option;
    paid_bytes : int64 option;
    used_bytes : int64 option;
    storage : Json_repr.ezjsonm option;
    code : Json_repr.ezjsonm option;
  }

  type context_contracts = (string * contract) list
end

open TYPES

module ENCODING = struct
  open Json_encoding

  exception DestructError

  (* Default behavior to destruct a json value *)
  let destruct encoding buf =
    try
      let json = Ezjsonm.from_string buf in
      Json_encoding.destruct encoding json
    with
    | Json_encoding.Cannot_destruct (path, exn) ->
        Format.eprintf
          "Error during destruction path %a : %s\n\n %s\n%!"
          (Json_query.print_path_as_json_path ~wildcards:true)
          path
          (Printexc.to_string exn)
          buf ;
        raise DestructError
    | Json_encoding.Unexpected_field field ->
        Format.eprintf
          "Error during destruction path, unexpected field %S %s\n%!"
          field
          buf ;
        raise DestructError

  let construct encoding data =
    let ezjson =
      (module Json_repr.Ezjsonm : Json_repr.Repr
        with type value = Json_repr.ezjsonm )
    in
    Json_repr.pp
      ~compact:true
      ezjson
      Format.str_formatter
      (Json_encoding.construct encoding data) ;
    Format.flush_str_formatter ()

  let int64 =
    union
      [ case
          int32
          (fun i ->
            let j = Int64.to_int32 i in
            if Int64.equal (Int64.of_int32 j) i then Some j else None)
          Int64.of_int32;
        case string (fun i -> Some (Int64.to_string i)) Int64.of_string ]

  let int = conv Int64.of_int Int64.to_int int64

  let tez_encoding = int64

  let frozen_balance_encoding =
    obj3
      (req "deposits" tez_encoding)
      (req "rewards" tez_encoding)
      (opt "fees" tez_encoding)

  let frozen_balance_encoding =
    conv
      (fun {deposits; rewards; fees} -> (deposits, rewards, fees))
      (fun (deposits, rewards, fees) -> {deposits; rewards; fees})
      frozen_balance_encoding

  let contract_encoding =
    merge_objs
      (obj10
         (opt "balance" tez_encoding)
         (dft "big_map" (option (list string)) (Some []))
         (opt "counter" int64)
         (dft "delegated" (option (list (tup2 string bool))) (Some []))
         (dft
            "frozen_balance"
            (option (list (tup2 int frozen_balance_encoding)))
            (Some []))
         (opt "manager" string)
         (dft "spendable" (option bool) (Some true))
         (dft "delegatable" (option bool) (Some true))
         (opt "roll_list" int)
         (dft "change" (option tez_encoding) (Some 0L)))
      (merge_objs
         (obj10
            (opt "delegate" string)
            (opt "delegate_desactivation" int)
            (opt "inactive_delegate" bool)
            (dft "paid_bytes" (option int64) (Some 0L))
            (dft "used_bytes" (option int64) (Some 0L))
            (opt "storage" any_ezjson_value)
            (opt "code" any_ezjson_value)
            (opt "2019" bool)
            (opt "white_list" (list string))
            (opt "nrolls" int))
         (obj2 (opt "superadmin" bool) (opt "delegation_closed" bool)))

  let contract_encoding =
    conv
      (fun { balance;
             big_map;
             counter;
             delegated;
             frozen_balance;
             manager;
             spendable;
             delegatable;
             roll_list;
             change;
             delegate;
             delegate_desactivation;
             inactive_delegate;
             paid_bytes;
             used_bytes;
             storage;
             code;
             legacy2019;
             white_list;
             nrolls;
             superadmin;
             delegation_closed } ->
        ( ( balance,
            big_map,
            counter,
            delegated,
            frozen_balance,
            manager,
            spendable,
            delegatable,
            roll_list,
            change ),
          ( ( delegate,
              delegate_desactivation,
              inactive_delegate,
              paid_bytes,
              used_bytes,
              storage,
              code,
              legacy2019,
              white_list,
              nrolls ),
            (superadmin, delegation_closed) ) ))
      (fun ( ( balance,
               big_map,
               counter,
               delegated,
               frozen_balance,
               manager,
               spendable,
               delegatable,
               roll_list,
               change ),
             ( ( delegate,
                 delegate_desactivation,
                 inactive_delegate,
                 paid_bytes,
                 used_bytes,
                 storage,
                 code,
                 legacy2019,
                 white_list,
                 nrolls ),
               (superadmin, delegation_closed) ) ) ->
        {
          balance;
          big_map;
          counter;
          delegated;
          frozen_balance;
          manager;
          spendable;
          delegatable;
          roll_list;
          change;
          delegate;
          delegate_desactivation;
          inactive_delegate;
          paid_bytes;
          used_bytes;
          storage;
          code;
          legacy2019;
          white_list;
          nrolls;
          superadmin;
          delegation_closed;
        })
      contract_encoding

  let context_contracts_encoding = list (tup2 string contract_encoding)
end

let translate_to_rows in_file out_file =
  let content = Dune_config.Dune_std.FILE.read in_file in
  let contracts =
    ENCODING.destruct ENCODING.context_contracts_encoding content
  in
  let oc = open_out out_file in
  Printf.fprintf
    oc
    "%s\n"
    (String.concat
       " "
       [ Printf.sprintf "%-36s" "Address";
         Printf.sprintf "%-13s" "Balance";
         Printf.sprintf "%-13s" "Deposits";
         Printf.sprintf "%-13s" "Fees";
         Printf.sprintf "%-13s" "Rewards";
         Printf.sprintf "%-55s" "Manager";
         Printf.sprintf "%-36s" "Delegate";
         Printf.sprintf "%-8s" "Kind";
         Printf.sprintf "%-7s" "Nrolls";
         Printf.sprintf "%-10s" "Burnable" ]) ;
  List.iter
    (fun (address, c) ->
      Printf.fprintf
        oc
        "%s\n"
        (String.concat
           " "
           [ address;
             Printf.sprintf
               "%13s"
               ( match c.balance with
               | None ->
                   "NO_BALANCE"
               | Some n ->
                   Printf.sprintf "%Ld" n );
             Printf.sprintf
               "%13s"
               ( match c.frozen_balance with
               | None ->
                   "NO_DEPOSITS"
               | Some l -> (
                   List.fold_left
                     (fun acc (_, f) -> Int64.add acc f.deposits)
                     0L
                     l
                   |> function
                   | 0L -> "NO_DEPOSITS" | n -> Printf.sprintf "%Ld" n ) );
             Printf.sprintf
               "%13s"
               ( match c.frozen_balance with
               | None ->
                   "NO_FEES"
               | Some l -> (
                   List.fold_left
                     (fun acc (_, f) ->
                       Int64.add
                         acc
                         (match f.fees with None -> 0L | Some n -> n))
                     0L
                     l
                   |> function 0L -> "NO_FEES" | n -> Printf.sprintf "%Ld" n )
               );
             Printf.sprintf
               "%13s"
               ( match c.frozen_balance with
               | None ->
                   "NO_REWARDS"
               | Some l -> (
                   List.fold_left
                     (fun acc (_, f) -> Int64.add acc f.rewards)
                     0L
                     l
                   |> function
                   | 0L -> "NO_REWARDS" | n -> Printf.sprintf "%Ld" n ) );
             Printf.sprintf
               "%55s"
               ( match c.manager with
               | None ->
                   "NO_MANAGER"
               | Some s ->
                   if s = address then "NOT_REVEALED" else s );
             Printf.sprintf
               "%36s"
               (match c.delegate with None -> "NO_DELEGATE" | Some s -> s);
             Printf.sprintf
               "%8s"
               ( match c.nrolls with
               | None ->
                   if address.[0] = 'd' then "ACCOUNT" else "CONTRACT"
               | Some _n ->
                   "BAKER" );
             Printf.sprintf
               "%7s"
               ( match c.nrolls with
               | None ->
                   "NO_ROLL"
               | Some n ->
                   string_of_int n );
             Printf.sprintf
               "%10s"
               ( match c.legacy2019 with
               | None ->
                   "ACTIVE"
               | Some true ->
                   "BURNABLE"
               | Some false ->
                   "ACTIVE" ) ]))
    contracts ;
  close_out oc ;
  ()
