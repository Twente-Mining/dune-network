(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types

let command_args c args =
  let protocol =
    match args with
    | [] ->
        "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
    | [proto] ->
        proto
    | _ :: x :: _ ->
        Printf.eprintf "Error: unexpected argument %S\n%!" x ;
        exit 2
  in
  let switch = c.current_switch in
  ( "dune-client",
    [ (* "-block" ; "genesis" ;*)
      "activate";
      "protocol";
      protocol;
      "with";
      "fitness";
      "1";
      "and";
      "key";
      "activator";
      "and";
      "parameters";
      Ix_common.protocol_parameters_file_of_switch switch ] )

let command c args =
  match c.current_switch with
  | "mainnet" | "testnet" ->
      Printf.eprintf "Error: cannot activate a mainnet/testnet node\n%!" ;
      exit 2
  | _ ->
      let (cmd, args) = command_args c args in
      Ix_common.exec c ~set_bindir ~cmd ~args

let ezcmd c =
  Ix_common.ezcmd
    "activate"
    ~all:(command c)
    ~doc:"Activate the most recent protocol using the current node"
