(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types
open Dune_config.Dune_std.OP (* for // *)

let remove_file file = try Sys.remove file with _ -> ()

let command c args =
  let reset_node () =
    match Ix_common.node_dir c c.current_node with
    | None ->
        Printf.eprintf "Error: node is not local\n%!" ;
        exit 2
    | Some dir ->
        Ix_common.call
          c
          ~set_bindir
          ~cmd:"local-dune-node"
          ~args:["snapshot"; "clear"; "storage"] ;
        remove_file (dir // "peers.json") ;
        remove_file (dir // "node.log")
  in
  let reset_baker () =
    let dir = Ix_common.client_dir c c.current_client in
    Printf.eprintf "Clearing baker info in %s\n%!" dir ;
    remove_file (dir // "blocks") ;
    remove_file (dir // "endorsements") ;
    ()
  in
  let reset_client () =
    let client_dir = Ix_common.client_dir c c.current_client in
    Printf.eprintf "Clearing client dir %s\n%!" client_dir ;
    Ix_common.remove_dir client_dir ;
    Ix_cmd_create.CLIENT.create_files c c.current_client
  in
  match args with
  | [] ->
      reset_node ()
  | _ ->
      List.iter
        (function
          | "node" ->
              reset_node ()
          | "client" ->
              reset_client ()
          | "baker" ->
              reset_baker ()
          | s ->
              Printf.kprintf failwith "Unexpected argument %S" s)
        args

let ezcmd c =
  let args = ref [] in
  Ix_common.ezcmd
    "reset"
    ~all:(fun s -> args := s)
    ~action:(fun () -> command c !args)
    ~doc:"Clear the storage of the current node"
