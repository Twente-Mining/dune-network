(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types

let command c args =
  let snapshots_dir = Ix_common.snapshots_dir c in
  Ix_common.mkdir snapshots_dir ;
  Ix_common.exec
    c
    ~set_bindir
    ~cmd:"local-dune-node"
    ~args:(["snapshot"; "export"; snapshots_dir] @ args)

let ezcmd c =
  Ix_common.ezcmd
    "save"
    ~all:(command c)
    ~doc:"Save a snapshot of the current node"
