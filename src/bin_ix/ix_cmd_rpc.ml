(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Ix_types

let rpc c ?output url =
  match url with
  | None ->
      let url =
        Printf.sprintf
          "http://%s:%d/"
          c.current_node.node_rpc_addr
          c.current_node.node_rpc_port
      in
      Ix_common.call c ~cmd:"xdg-open" ~args:[url]
  | Some url ->
      let url =
        Printf.sprintf
          "http://%s:%d%s%s"
          c.current_node.node_rpc_addr
          c.current_node.node_rpc_port
          (if String.length url = 0 || url.[0] = '/' then "" else "/")
          url
      in
      Ix_cmd_wget.command ?output url

let ezcmd c =
  let output = ref None in
  let get url = rpc ?output:!output c (Some url) in
  Ix_common.ezcmd
    "rpc"
    ~args:
      Ezcmd.Modules.
        [ ( ["o"],
            Arg.String (fun s -> output := Some s),
            Ezcmd.info "FILE File to save result to" );
          ( [],
            Arg.Anon
              ( 0,
                fun arg ->
                  match arg with
                  | "/" ->
                      rpc c None
                  | "head" ->
                      get "/chains/main/blocks/head"
                  | "hash" ->
                      get "/chains/main/blocks/head/hash"
                  | "header" ->
                      get "/chains/main/blocks/head/header"
                  | "level" ->
                      get "/chains/main/blocks/head/helpers/current_level"
                  | "version" ->
                      get "/dune/version"
                  | _ ->
                      get arg ),
            Ezcmd.info "URL|head|header|level call node RPC" ) ]
    ~doc:"Query the RPC server of the current node"
