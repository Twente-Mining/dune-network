(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Dune_config
open Dune_std.OP
open Tezos_crypto
open Ix_types

let genesis_time_of_now () =
  Ptime.to_rfc3339
    ~frac_s:0
    ~tz_offset_s:0
    ( match Ptime.of_float_s (Unix.gettimeofday ()) with
    | None ->
        assert false
    | Some date ->
        date )

let generate_genesis_block ~genesis_time =
  let prefix = "BLockGenesisGenesisGenesisGenesisGenesis" in
  let suffix =
    String.sub (Base58.raw_encode (Digest.string genesis_time)) 0 11
  in
  let genesis_block =
    match Base58.raw_decode (prefix ^ suffix) with
    | None ->
        assert false
    | Some p ->
        let p = String.sub p 0 (String.length p - 4) in
        Base58.safe_encode p
  in
  let _ = Block_hash.of_b58check_exn genesis_block in
  genesis_block

let commit current =
  Ix_common.save_switch_state
    current.current_switch
    current.current_switch_state ;
  ()

let parse_addr_port s =
  let len = String.length s in
  if len = 0 then ("", None)
  else if s.[0] = '[' then
    (* inline IPv6 *)
    match String.rindex_opt s ']' with
    | None ->
        invalid_arg "Utils.parse_addr_port (missing ']')"
    | Some pos ->
        let addr = String.sub s 1 (pos - 1) in
        let port =
          if pos = len - 1 then None
          else if s.[pos + 1] <> ':' then
            invalid_arg "Utils.parse_addr_port (unexpected char after ']')"
          else Some (String.sub s (pos + 2) (len - pos - 2))
        in
        (addr, port)
  else
    match String.rindex_opt s ']' with
    | Some _pos ->
        invalid_arg "Utils.parse_addr_port (unexpected char ']')"
    | None -> (
      match String.index s ':' with
      | exception _ ->
          (s, None)
      | pos -> (
        match String.index_from s (pos + 1) ':' with
        | exception _ ->
            let addr = String.sub s 0 pos in
            let port = String.sub s (pos + 1) (len - pos - 1) in
            (addr, Some port)
        | _pos ->
            invalid_arg
              "Utils.parse_addr_port: IPv6 addresses must be bracketed" ) )

let split_rpc_addr current s =
  match
    let (addr, port) = parse_addr_port s in
    ( addr,
      match port with
      | Some port ->
          int_of_string port
      | None -> (
        match current.current_switch with
        | "mainnet" ->
            8733
        | "testnet" ->
            8734
        | _ ->
            Printf.eprintf "Error: port not specified for address %S\n%!" addr ;
            exit 2 ) )
  with
  | v ->
      v
  | exception _ ->
      Printf.eprintf "Error: cannot parse address %S\n%!" s ;
      exit 2

module NODE = struct
  let create ~remote_addrs current node_name =
    Printf.eprintf "Creating node %S............\n%!" node_name ;
    let num = Ix_common.node_num node_name in
    let node =
      match !remote_addrs with
      | [] ->
          ( match current.current_switch with
          | "mainnet" | "testnet" ->
              ()
          | _ ->
              let config_file =
                Ix_common.config_file_of_switch current.current_switch
              in
              if not (Sys.file_exists config_file) then (
                Printf.eprintf
                  "Error: cannot create node without network config for %S\n%!"
                  current.current_switch ;
                exit 2 ) ) ;
          let node_local =
            {
              node_net_addr =
                ( match current.current_switch with
                | "mainnet" | "testnet" ->
                    "[::]"
                | _ ->
                    "127.0.0.1" );
              node_net_port = 9733 + num;
            }
          in
          {
            node_name;
            node_rpc_addr =
              (if Ix_types.in_docker then "0.0.0.0" else "127.0.0.1");
            node_rpc_port = 8733 + num;
            node_local = Some node_local;
          }
      | addr :: next ->
          remote_addrs := next ;
          let (node_rpc_addr, node_rpc_port) = split_rpc_addr current addr in
          {node_name; node_rpc_addr; node_rpc_port; node_local = None}
    in
    let current_switch_state = current.current_switch_state in
    let current_switch_state =
      {
        current_switch_state with
        switch_nodes = node :: current_switch_state.switch_nodes;
      }
    in
    Printf.eprintf
      "   Node RPC address: %s:%d\n%!"
      node.node_rpc_addr
      node.node_rpc_port ;
    ( match Ix_common.node_dir current node with
    | None ->
        ()
    | Some node_dir ->
        if Sys.file_exists node_dir then (
          Printf.eprintf
            "Error: directory %S for node %S already exists.\n%!"
            node_dir
            node_name ;
          Printf.eprintf "  If it is orphan, you should delete it.\n%!" ;
          exit 2 ) ) ;
    let current = {current with current_node = node; current_switch_state} in
    ( try
        let config_args =
          match node.node_local with
          | None ->
              []
          | Some node_local ->
              Ix_common.call
                current
                ~set_bindir
                ~cmd:"dune-node"
                ~args:["identity"; "generate"] ;
              [ "--net-addr";
                Printf.sprintf
                  "%s:%d"
                  node_local.node_net_addr
                  node_local.node_net_port;
                ( if Ix_types.in_docker then "--public-access"
                else "--no-public-access" ) ]
        in
        Ix_common.call
          current
          ~set_bindir
          ~cmd:"dune-node"
          ~args:
            ( "config" :: "init" :: "--rpc-addr"
            :: Printf.sprintf "%s:%d" node.node_rpc_addr node.node_rpc_port
            :: config_args )
      with exn ->
        ( match Ix_common.node_dir current node with
        | None ->
            ()
        | Some node_dir ->
            Ix_common.remove_dir node_dir ) ;
        raise exn ) ;
    current
end

module CLIENT = struct
  open Tezos_data_encoding.Data_encoding

  let public_key_hashs_encoding =
    list @@ obj2 (req "name" string) (req "value" string)

  let public_keys_encoding =
    list
    @@ obj2
         (req "name" string)
         (req "value" (obj2 (req "locator" string) (req "key" string)))

  let secret_keys_encoding =
    list @@ obj2 (req "name" string) (req "value" string)

  let create_files c client =
    let accounts = c.current_switch_state.switch_accounts in
    let public_key_hashs = ref [] in
    let public_keys = ref [] in
    let secret_keys = ref [] in
    List.iter
      (fun a ->
        public_key_hashs :=
          (a.account_name, a.account_pkh) :: !public_key_hashs ;
        ( match a.account_pk with
        | None ->
            ()
        | Some pk ->
            public_keys :=
              (a.account_name, ("unencrypted:" ^ pk, pk)) :: !public_keys ) ;
        match a.account_sk with
        | None ->
            ()
        | Some sk ->
            secret_keys :=
              (a.account_name, "unencrypted:" ^ sk) :: !secret_keys)
      accounts ;
    let client_dir = Ix_common.client_dir c client in
    Ix_common.mkdir client_dir ;
    Dune_std.FILE.write_json
      (client_dir // "public_key_hashs")
      !public_key_hashs
      public_key_hashs_encoding ;
    Dune_std.FILE.write_json
      (client_dir // "public_keys")
      !public_keys
      public_keys_encoding ;
    Dune_std.FILE.write_json
      (client_dir // "secret_keys")
      !secret_keys
      secret_keys_encoding ;
    ()

  let create current client_name =
    Printf.eprintf "Creating client %S............\n%!" client_name ;
    let current_switch_state = current.current_switch_state in
    let client = {client_name} in
    let current_switch_state =
      {
        current_switch_state with
        switch_clients = client :: current_switch_state.switch_clients;
      }
    in
    create_files current client ;
    {current with current_client = client; current_switch_state}
end

let tez n = Int64.mul (Int64.of_int n) 1_000_000L

let gen_key () =
  let (pkh, pk, sk) = Signature.generate_key () in
  ( Signature.Public_key_hash.to_b58check pkh,
    Signature.Public_key.to_b58check pk,
    Signature.Secret_key.to_b58check sk )

let sandbox_accounts =
  [ ( "bootstrap1",
      tez 4_000_000,
      ( "dn1f7ujdgCTZg7AvhTuadmVrcgUZrdrcN6jT",
        (*    "tz1id1nnbxUwK1dgJwUra5NEYixK37k6hRri", *)
        "edpkv2Gafe23nz4x4hJ1SaUM3H5hBAp5LDwsvGVjPsbMyZHNomxxQA",
        "edsk4LhLg3212HzL7eCXfCWWvyWFDfwUS7doL4JUSmkgTe4qwZbVYm" ) );
    ( "bootstrap2",
      tez 4_000_000,
      ( "dn1QHrzv66HAT2NjFdj1sEwvMasXuWtHVqJV",
        "edpkuW2GZePnF1rt3XrM533PYaRiXPwaHzCpHQSbsNzAds1SVXbhkm",
        "edsk4DDEDf3VrBYYuhVgyjfP3VC1RVX7ZJHAsCMKV511U1a7nNxJdH" ) );
    ( "bootstrap3",
      tez 4_000_000,
      ( "dn1W9Vnwqcw5kZQr4HM6nrS5qYUXAmderFyR",
        "edpku8ehu8waYZKfjvLWwJ6Pv7UuPvdKgszknNnUVj7MTyf4R9tyZA",
        "edsk2z8cbf8LGkp1LsD8k7EBDYEMjFvWFv7EY3edr3cyGxzu4fyfqk" ) );
    ( "bootstrap4",
      tez 4_000_000,
      ( "dn1QEurh3rBReQvArunakVXyTyktQX5VFSEh",
        "edpkuqBfrV371AcvKS8bUXveKENg7X8F6CUfRJomb833GjyG8GEyKt",
        "edsk3iTP2cgnxFQQatUGwP2f9aJMaPLF89xgJSUPovJeqi8ucpPGjN" ) );
    ( "activator",
      tez 100,
      ( "dn1RvpNRW6hiTL8oyRbD3TegDrM8gtb5WULi",
        "edpkvBaktu2KAXTqyZ9KMgSCsdmftwgNcFyGsfEUPWLkxTXcxcx4L9",
        "edsk4S4k7XvNZDGStr2ouVZwRMB2vghDBFgr9RY7aLEJ2Z1EocExEL" ) );
    ( "user0",
      tez 100,
      ( "dn1QudFDa1k15xwnXWYxgMRJf4RGJV1yzDym",
        "edpkv6FVz3e8dwPBXXBd6dSgkqkcRuDEQYU1GHB1yxp4M43PpZErkx",
        "edsk3B3ekV8rkhQefWsjtVHUWKkTEfPx5EDAZ3kX2cANc4CjiB6tWc" ) );
    ( "user1",
      tez 100,
      ( "dn1YJFYFw6ER8GFct3JA6ktRT1pjynV1juCy",
        "edpkvAxQRNdXKHtmpq4FaUegde9HnyDsp7PpGGmUV2xxVVZDgLsNp4",
        "edsk3db5UAazVm47udm3QhtcUaMNyF5q1HYXWjCze2aYgmwGLQQ44x" ) );
    ( "user2",
      tez 100,
      ( "dn1KhdkBRSRdPUVtXZaz9kNMX3nxd92LVBwH",
        "edpkuRmZogiSyc8hPbnyCYrK9YKaiT2gWUTF1sp7tBDNi6tTXDG4EC",
        "edsk3P769PrUP3jfiTmQQrgowg6aSQ4nmDDp7r1yFioXoT2wTUo7Uj" ) );
    ( "user3",
      tez 100,
      ( "dn1HEggbbpWYovvKzpnBfpSN82fip56fk5K2",
        "edpkuZSDuYuQsHABD9ueZtVq2GTHgk2U5SZPHx8ri2SfbUxKaGEWQ8",
        "edsk2zUC48oRHCGnPTT2cDPbpmEL1buT8W1wW6PSReKQKJdTqqwZZm" ) );
    ( "user4",
      tez 100,
      ( "dn1ZFsha5jeedydCiUxj3rLNKoKN7dmymdNg",
        "edpkvEz2MQvuM2DZhwfUFNAasVgRj6eekRR7tjUqziaA1fXmfsDhTT",
        "edsk42uzLJozidtSbkWvWppoh7pPGhkA7awjJv4e9h257zugpcXwxS" ) );
    ( "user5",
      tez 100,
      ( "dn1NL8VZb7GeWCV8RYA6b3JnEDnh9B7Z2XjT",
        "edpkuuzposdhTc45t3yKDwGzh9o77Q658JD7rWXshaLn7xz1dRa51S",
        "edsk4DRW7NuBW1kCS35QfNzcqRXW7LoNAAQamTFoRHqhzztwkBvg7T" ) );
    ( "user6",
      tez 100,
      ( "dn1Gc5pFRGzBsnTifsNogmhiQcPHvcehbX4s",
        "edpkuYnKcE7XCrG7k3nQ2BYxDxGhwgZ9YhtTg72CKvdk52fqEQLKZJ",
        "edsk4MfoYjGHnSg7paP9dkzNTkdkiFkEHgej93GUKA4GSLgpjeeKJ6" ) );
    ( "user7",
      tez 100,
      ( "dn1SVEoV68ofifURFHeXHVRuDHETmv3HedRa",
        "edpktkozcHKqQ5zmPAMjEfYqDYnmD1NTecMsn9P7hMewd9hi55iKU2",
        "edsk3QvzUrg2ZGks1mwjq8wr7H1f1F9EAbPPafgm4aP5NLStyF2mED" ) );
    ( "user8",
      tez 100,
      ( "dn1d8CXw3GEXKcZvBYzNFWgv4jDEojRXArZo",
        "edpkvLq5HBXAhhEay2sBQesFSCKFa4LN2XFMa6t6GqZwExxRE5qMyA",
        "edsk4YHMekTSDWxECJWQDYpFARsenMefEqfNdXnMfrDJ6brBQrYjGt" ) );
    ( "user9",
      tez 100,
      ( "dn1aMH2u9D53RM74UeosvYh2c3L2k49H7iQa",
        "edpku3H4VrQCy7rZKvAPDR5frz3yh2rzinyCRscWS9m11fVxpiPTnB",
        "edsk2mhS8jTeZDKJTC36HinK2i5DQkfGNeo5bD2vnAQknkm8kiueAt" ) ) ]

let accounts () =
  [ ("bootstrap1", tez 4_000_000, gen_key ());
    ("bootstrap2", tez 4_000_000, gen_key ());
    ("bootstrap3", tez 4_000_000, gen_key ());
    ("bootstrap4", tez 4_000_000, gen_key ());
    ("activator", tez 100, gen_key ());
    ("user0", tez 100, gen_key ());
    ("user1", tez 100, gen_key ());
    ("user2", tez 100, gen_key ());
    ("user3", tez 100, gen_key ());
    ("user4", tez 100, gen_key ());
    ("user5", tez 100, gen_key ());
    ("user6", tez 100, gen_key ());
    ("user7", tez 100, gen_key ());
    ("user8", tez 100, gen_key ());
    ("user9", tez 100, gen_key ()) ]

let create_switch ?network_config ~remote_addrs current switch =
  if
    switch = "" || switch = "mainnet" || switch = "testnet"
    || Dune_std.STRING.starts_with switch ~prefix:"node"
    || Dune_std.STRING.starts_with switch ~prefix:"client"
    || switch = "dunscan"
  then (
    Printf.eprintf "Error: cannot create a switch from a reserved name.\n%!" ;
    exit 2 ) ;
  ( match switch.[0] with
  | 'a' .. 'z' | 'A' .. 'Z' ->
      ()
  | _ ->
      Printf.eprintf "Error: switch must start with a letter\n%!" ;
      exit 2 ) ;
  for i = 1 to String.length switch - 1 do
    match switch.[i] with
    | 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' ->
        ()
    | c ->
        Printf.eprintf "Error: switch cannot contain char '%c'\n%!" c ;
        exit 2
  done ;
  let is_new = network_config = None in
  let (network_config, accounts) =
    match network_config with
    | Some "-" -> (
      match !remote_addrs with
      | [] ->
          Printf.eprintf
            "Error: network config can only be downloaded with --remote\n%!" ;
          exit 2
      | addr :: _ ->
          let url = Printf.sprintf "http://%s/dune/config" addr in
          (Some (Ix_cmd_wget.get_or_exit (fun s -> s) ~url), []) )
    | Some "none" ->
        (None, [])
    | Some network_config ->
        (Some (Dune_std.FILE.read network_config), [])
    | None ->
        let is_sandbox =
          Dune_std.STRING.starts_with switch ~prefix:"sandbox"
        in
        let accounts = if is_sandbox then sandbox_accounts else accounts () in
        let config = Dune_config.Set_config_mainnet.config in
        let genesis_time = genesis_time_of_now () in
        let genesis_key =
          let rec iter accounts =
            match accounts with
            | [] ->
                assert false
            | (name, _, (_pkh, genesis_key, _sk)) :: accounts ->
                if name = "activator" then genesis_key else iter accounts
          in
          iter accounts
        in
        let genesis_block = generate_genesis_block ~genesis_time in
        let network = String.capitalize_ascii switch in
        let low = String.lowercase_ascii switch in
        let upp = String.uppercase_ascii switch in
        let config =
          {
            config with
            network;
            forced_protocol_upgrades = [];
            genesis_time;
            genesis_key;
            genesis_block;
            genesis_protocol =
              "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P";
            p2p_version = Printf.sprintf "DUNE_%s_%s" upp genesis_time;
            prefix_dir = "dune-" ^ low;
            bootstrap_peers = [];
            protocol_revision = Dune_config.Source_config.max_revision;
            only_allowed_bakers = false;
            self_amending = false;
            number_of_cycles_between_burn = 10;
            share_of_balance_to_burn = 10;
            frozen_account_cycles = 60;
            p2p_secret = None;
            nodes_list = None;
            access_control = false;
            minimal_fee_factor = 1;
            closed_network = false;
            kyc_level = Kyc_open;
            expected_pow = 1.;
          }
        in
        ( Some
            (Tezos_data_encoding.Data_encoding.Json.to_string
               ~minify:false
               (Tezos_data_encoding.Data_encoding.Json.construct
                  (Dune_config.Config_encoding.encoding
                     Dune_config.Set_config_mainnet.config)
                  config)),
          accounts )
  in
  let switch_accounts =
    List.map
      (fun (account_name, _tez, (account_pkh, account_pk, account_sk)) ->
        let account_is_baker =
          Dune_std.STRING.starts_with account_name ~prefix:"bootstrap"
        in
        let account_pk = Some account_pk in
        let account_sk = Some account_sk in
        {account_name; account_pkh; account_pk; account_sk; account_is_baker})
      accounts
  in
  let switch_state =
    {
      switch_nodes = [];
      switch_clients = [];
      switch_accounts;
      switch_activated = false;
    }
  in
  let current =
    {current with current_switch = switch; current_switch_state = switch_state}
  in
  Ix_common.mkdir (Ix_common.switch_dir switch) ;
  let config_file = Ix_common.config_file_of_switch switch in
  ( match network_config with
  | None ->
      ()
  | Some config ->
      let oc = open_out config_file in
      output_string oc config ; close_out oc ) ;
  ( if is_new then
    let bootstrap_accounts =
      List.map
        (fun (account_name, amount, (public_key_hash, public_key, _sk)) ->
          let account_is_baker =
            Dune_std.STRING.starts_with account_name ~prefix:"bootstrap"
          in
          let public_key =
            if account_is_baker then Some public_key else None
          in
          {Ix_protocol.public_key_hash; public_key; amount})
        accounts
    in
    let parameters =
      Ix_protocol.parameters
        ~constants:Ix_protocol.sandbox_constants
        ~accounts:bootstrap_accounts
    in
    Dune_std.FILE.write_json
      (Ix_common.protocol_parameters_file_of_switch switch)
      parameters
      Ix_protocol.encoding ) ;
  commit current ; current

let command ?network_config ~remote_addrs current switch =
  let (switch, nodes, clients) =
    Ix_common.parse_switch ~default_switch:current.current_switch switch
  in
  let (current, created) =
    try
      let v =
        if switch = current.current_switch then (current, false)
        else
          let current_state = Ix_common.load_current_switch_state switch in
          (Ix_common.check_switch current_state, false)
      in
      match network_config with
      | None ->
          v
      | Some _ ->
          Printf.eprintf
            "Error: argument --config provided, but switch already exists\n%!" ;
          exit 2
    with Ix_common.SwitchDoesNotExist switch ->
      (create_switch ?network_config ~remote_addrs current switch, true)
  in
  ( match (nodes, clients) with
  | ([], []) ->
      if not created then (
        Printf.eprintf "Error: switch %S already exists\n%!" switch ;
        exit 2 )
  | _ ->
      (* verify first that these nodes do not exist *)
      List.iter
        (fun node ->
          List.iter
            (fun n ->
              if n.node_name = node then (
                Printf.eprintf
                  "Error: node %S already exist in switch %S\n%!"
                  node
                  switch ;
                exit 2 ))
            current.current_switch_state.switch_nodes)
        nodes ;
      (* verify first that these clients do not exist *)
      List.iter
        (fun client ->
          List.iter
            (fun cl ->
              if cl.client_name = client then (
                Printf.eprintf
                  "Error: client %S already exist in switch %S\n%!"
                  client
                  switch ;
                exit 2 ))
            current.current_switch_state.switch_clients)
        clients ) ;
  let current =
    List.fold_left
      (fun current node_name ->
        let current = NODE.create ~remote_addrs current node_name in
        commit current ; current)
      current
      nodes
  in
  let current =
    List.fold_left
      (fun current client_name ->
        let current = CLIENT.create current client_name in
        commit current ; current)
      current
      clients
  in
  let current =
    match current.current_switch_state.switch_nodes with
    | [] ->
        NODE.create ~remote_addrs current "node"
    | _ ->
        current
  in
  let current =
    match current.current_switch_state.switch_clients with
    | [] ->
        CLIENT.create current "client"
    | _ ->
        current
  in
  commit current ;
  Ix_common.save_current_state current ;
  let error = ref false in
  List.iter
    (fun addr ->
      Printf.eprintf "Error: argument --remote %S not used\n%!" addr ;
      error := true)
    !remote_addrs ;
  if !error then exit 2 ;
  ()

let ezcmd c =
  let network_config = ref None in
  let switch_name = ref None in
  let remote_addrs = ref [] in
  Ix_common.ezcmd
    "create"
    ~args:
      Ezcmd.Modules.
        [ ( [],
            Arg.Anon (0, fun arg -> switch_name := Some arg),
            Ezcmd.info "RESSOURCE Create a new network/node/client" );
          ( ["config"],
            Arg.String (fun s -> network_config := Some s),
            Ezcmd.info "NETWORK_CONFIG.json Provide a config for a new network"
          );
          ( ["remote"],
            Arg.String (fun s -> remote_addrs := !remote_addrs @ [s]),
            Ezcmd.info "ADDR:PORT Provide a remote addr for a new node" ) ]
    ~doc:"Create a new resource"
    ~action:(fun () ->
      let network_config = !network_config in
      match !switch_name with
      | None ->
          Printf.eprintf "Error: no switch/node/client name provided.\n%!" ;
          exit 2
      | Some switch_name ->
          command ?network_config ~remote_addrs c switch_name)
