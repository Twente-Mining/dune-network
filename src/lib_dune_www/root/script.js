const NBLOCKS = 20

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function monitor(path, callback) {
    return await rpcstream(path, xhr => {
        if (xhr.newResponse) {
            callback(JSON.parse(xhr.newResponse))
        }
    });
}

async function monitor_blocks(callback) {
    return await monitor("/monitor/heads/main", callback);
}

async function monitor_operations(callback) {
    return await monitor("/chains/main/mempool/monitor_operations", callback);
}


async function get_json(path) {
    var { readyState: s, response: r} = await rpcget(path);
    if (s = 4 && r) {
        return JSON.parse(r);
    }
}

async function post_json(path, data = {}) {
    var { readyState: s, response: r} =
        await rpcpost(path, JSON.stringify(data));
    if (s = 4 && r) {
        return JSON.parse(r);
    }
}

var accounts = [];
// Call once
async function init_accounts () {
    accounts = await get_json("/admin/accounts")
};

function get_alias (dn1) {
    var assoc = accounts.find(x => x.pkh == dn1);
    if (assoc) {
        return assoc.alias;
    } else {
        return dn1;
    }
}

function mk_link (name, trunc) {
    if (trunc) {
        var name_elt = document.createTextNode(name.substring(0, trunc));
    } else {
        var name_elt = document.createTextNode(name);
    }
    if (! name) { return name_elt; };
    var a = document.createElement("a");
    a.appendChild(name_elt);
    if (name.startsWith("B")) {
        a.href = "/chains/main/blocks/" + name;
    } else if (name.startsWith("dn") || name.startsWith("KT")) {
        a.href = "/chains/main/blocks/head/context/contracts/" + name + "/info";
        a.innerHTML = get_alias(name);
    } else if (name.startsWith("P")) {
        a.href = "/protocols/" + name;
    } else {
        return name_elt;
    };
    return a;
}

async function get_block(hash) {
    return await get_json("/chains/main/blocks/"+ hash);
}

async function get_block_header(hash) {
    return await get_json("/chains/main/blocks/"+ hash +"/header");
}

async function get_last_blocks(n, head) {
    return (await get_json("/chains/main/blocks?length="+ n +"&head="+head))[0];
}

var prev = null;

function set_click_raise (elt) {
    elt.onclick = function() {
        if (prev) { prev.style.zIndex = 1; }
        elt.style.zIndex = 1000;
        prev = elt;
    }
}

function set_status(status, s_class) {
    var div = document.getElementById("status");
    var span = document.createElement("div");
    span.appendChild(document.createTextNode(status));
    span.classList.add(s_class);
    div.innerHTML = ""
    div.appendChild(span);
}

function keep_rows(id, n) {
    var table = document.getElementById(id);
    var l = table.rows.length;
    for (var i = l-1; i > 0 ; i--) {
        var tr = table.rows[i];
        if (i > n) { tr.remove() };
    }
}

function keep_blocks(n) {
    keep_rows("blocks-table", n);
}

function add_block (block) {
    var header = document.getElementById("blocks-table-header");
    var tr = document.createElement("tr");
    var level_cell = document.createElement("td");
    var hash_cell = document.createElement("td");
    var ts_cell = document.createElement("td");
    tr.appendChild(level_cell);
    tr.appendChild(hash_cell);
    tr.appendChild(ts_cell);
    var level = document.createTextNode(block.level);
    level_cell.appendChild(level);
    var hash = mk_link(block.hash);
    hash_cell.appendChild(hash);
    const formatter = new JSONFormatter(block, 0);
    const json = formatter.render();
    set_click_raise(json);
    json.classList.add("inline-json-block");
    hash_cell.appendChild(json);
    var timestamp = document.createTextNode(new Date(block.timestamp).toLocaleString());
    ts_cell.appendChild(timestamp);
    header.parentNode.insertBefore(tr, header.nextSibling);
    keep_blocks(NBLOCKS)
}

async function display_blocks() {
    try {
        keep_blocks(0);
        var head = await get_block_header("head");
        var blocks = await get_last_blocks(NBLOCKS, head.hash);
        blocks.shift();
        var block_headers = await Promise.all(blocks.map(get_block_header));
        block_headers.reverse().forEach(add_block);
        await monitor_blocks(block => {
            set_status("UP", "green");
            add_block(block);
        });
        set_status("DOWN", "red");
        await sleep(10000);
        await display_blocks();
    } catch (e) {
        set_status(`DOWN (Unreachable)`, "red");
        await sleep(10000);
        await display_blocks();
    }
}

function new_div (x, cl) {
    var d = document.createElement("div");
    if (x) { d.appendChild(x) };
    if (cl) { d.classList.add(cl); }
    return d;
}

function new_inline (x, cl) {
    var d = new_div(x, cl);
    d.style.display = "inline-block";
    return d;
}

function mk_amount (str) {
    return (new Number(str) / 1000000).toLocaleString("en-US", {maximumFractionDigits: 6}) + " đ";
}

function mk_amount_elt (str) {
    return document.createTextNode(mk_amount(str));
}

function mk_kind_badge (kind) {
    var s = document.createElement("div");
    s.classList.add(kind);
    s.appendChild(document.createTextNode(kind));
    return s;
}

function mk_badge (x, kind) {
    var s = document.createElement("div");
    s.classList.add(kind);
    s.classList.add("badge");
    s.appendChild(document.createTextNode(x));
    return s;
}

function mk_transaction (t) {
    var line = new_div(null, "op-line");
    var div = new_inline(null, "tr-details");
    var src = mk_link(t.source);
    var dst = mk_link(t.destination);
    var gas = document.createTextNode(t.gas_limit + " gas");
    var storage = document.createTextNode(", " + t.storage_limit + " B");
    var fee = mk_amount_elt(t.fee);
    line.appendChild(mk_badge(mk_amount(t.amount), t.kind));
    line.appendChild(div);
    div.appendChild(document.createTextNode(" from "));
    div.appendChild(src);
    div.appendChild(document.createTextNode(" to "));
    div.appendChild(dst);
    div.appendChild(document.createTextNode(" with fee "));
    div.appendChild(fee);
    div.appendChild(document.createTextNode(" ("));
    if (t.gas_limit && t.gas_limit > 0) { div.appendChild(gas) };
    if (t.storage_limit && t.storage_limit > 0) { div.appendChild(storage) };
    div.appendChild(document.createTextNode(")"));
    return line;
}

function mk_origination (t) {
    var line = new_div(null, "op-line");
    var div = new_inline(null, "tr-details");
    var src = mk_link(t.source);
    var gas = document.createTextNode(t.gas_limit + " gas");
    var storage = document.createTextNode(", " + t.storage_limit + " B");
    var fee = mk_amount_elt(t.fee);
    line.appendChild(mk_badge(mk_amount(t.balance), t.kind));
    line.appendChild(div);
    div.appendChild(document.createTextNode(" from "));
    div.appendChild(src);
    div.appendChild(document.createTextNode(" with fee "));
    div.appendChild(fee);
    div.appendChild(document.createTextNode(" ("));
    if (t.gas_limit && t.gas_limit > 0) { div.appendChild(gas) };
    if (t.storage_limit && t.storage_limit > 0) { div.appendChild(storage) };
    div.appendChild(document.createTextNode(")"));
    return line;
}

function mk_reveal (t) {
    var line = new_div(null, "op-line");
    var div = new_inline(null, "tr-details");
    // var src = mk_link(t.source);
    line.appendChild(mk_badge(t.source, t.kind));
    line.appendChild(div);
    div.appendChild(document.createTextNode(" Public key: " + t.public_key));
    return line;
}

function mk_delegation (t) {
    var line = new_div(null, "op-line");
    var div = new_inline(null, "tr-details");
    var src = mk_link(t.source);
    line.appendChild(div);
    if (t.delegate) {
        div.appendChild(document.createTextNode(" New delegate of "));
        div.appendChild(src);
        div.appendChild(document.createTextNode(" set to "));
        div.appendChild(mk_link(t.delegate));
    } else {
        div.appendChild(document.createTextNode(" Remove delegate of "));
        div.appendChild(src);
    };
    return line;
}

function mk_endorsement (t) {
    var line = new_div(null, "op-line");
    line.appendChild(mk_badge("Level " + t.level, t.kind));
    return line;
}

function add_op(batch_nb, op) {
    var div = document.getElementById("ops");
    var op_div;
    switch(op.kind) {
    case "transaction":
        op_div = mk_transaction(op);
        break;
    case "endorsement":
        op_div = mk_endorsement(op);
        break;
    case "origination":
        op_div = mk_origination(op);
        break;
    case "reveal":
        op_div = mk_reveal(op);
        break;
    case "delegation":
        op_div = mk_delegation(op);
        break;
    default:
        op_div = new_div(null, "op-line");
    };
    op_div.insertBefore(mk_kind_badge(op.kind), op_div.firstChild);
    // op_div.classList.add(op.kind);
    if (batch_nb % 2 == 1) {
        op_div.classList.add("odd-batch");
    } else {
        op_div.classList.add("even-batch");
    }
    const formatter = new JSONFormatter(op, 0);
    const json = formatter.render();
    set_click_raise(json);
    json.classList.add("inline-json");
    op_div.appendChild(json);
    div.insertBefore(op_div, div.firstChild);
}

async function display_operations() {
    var div = document.getElementById("ops");
    div.innerHTML = "No pending operations ...";
    var first = true;
    var batch_nb = 0;
    try {
        await monitor_operations(ops => {
            console.log(ops);
            if (first && ops.length > 0) { first = false; div.innerHTML = ""}
            ops.reverse().forEach(op => {
                batch_nb ++
                if (op.contents) {
                    op.contents.forEach(op => add_op(batch_nb, op));
                }
            })
        });
        await display_operations();
    } catch(e) {
        console.log("Error in monitor_operations");
        await sleep(10000);
        await display_operations();
    }
}

async function get_self() {
    return await get_json("/network/self");
}

async function get_chain_id() {
    return await get_json("/chains/main/chain_id");
}


async function get_chechkpoint() {
    return await get_json("/chains/main/checkpoint");
}

async function get_network_version() {
    return await get_json("/network/version");
}

function set_at_id(id, v){ document.getElementById(id).innerHTML = ""+v; }

async function start_front_monitor() {

    async function once()
    {
        var self = await get_self();
        var chain_id = await get_chain_id();
        var net = await get_network_version();
        set_at_id("node_id", self);
        set_at_id("chain_id", chain_id);
        set_at_id("network", net.chain_name);
    }

    async function update_10s(){
        var daemons = await get_daemons_running();
        function doit(kind) {
            var nb = daemons.filter(d => d.kind == kind).length;
            var str = nb + " " + kind;
            if (nb > 1) {
                str = str + "s"
            };
            var eid = "nb-"+kind+"s";
            set_at_id(eid, str);
            var cl = document.getElementById(eid).classList;
            if (nb == 0) {
                cl.add("nb-daemon-zero");
            } else {
                cl.remove("nb-daemon-zero");
            }
        }
        doit("baker");
        doit("endorser");
        doit("accuser");
    }

    async function update_120s(){
        var checkpoint = await get_chechkpoint();
        set_at_id("checkpoint_level", checkpoint.block.level);
    }

    await init_accounts();
    once();
    update_120s();
    update_10s();

    setInterval(update_120s, 120000);
    setInterval(update_10s, 10000);

    display_blocks();
    display_operations();
}

async function get_daemons () {
    return await get_json("/admin/daemons");
}

async function get_daemons_running () {
    return await get_json("/admin/daemons?running");
}

function mk_action_btn(name, cell, path, classes) {
    var btn = document.createElement("button");
    btn.innerHTML=name;
    cell.appendChild(btn);
    if (classes) {
        classes.forEach(btn.classList.add);
    };
    btn.onclick = async () => {
        await post_json(path);
        await update_daemons();
        await sleep(2000);
        await update_daemons();
        return false;
    };
}

var logging_map = new Map();

function mk_daemon(d) {
    var table = document.getElementById(d.kind + "-table");
    var tr = document.createElement("tr");
    var id_cell = document.createElement("td");
    var del_cell = document.createElement("td");
    var proto_cell = document.createElement("td");
    var status_cell = document.createElement("td");
    var ping_cell = document.createElement("td");
    var log_cell = document.createElement("td");
    var act_cell = document.createElement("td");
    tr.appendChild(id_cell);
    if (d.kind != "accuser") {
        tr.appendChild(del_cell);
    };
    tr.appendChild(proto_cell);
    tr.appendChild(status_cell);
    tr.appendChild(ping_cell);
    tr.appendChild(log_cell);
    tr.appendChild(act_cell);

    id_cell.appendChild(document.createTextNode(d.id));
    if (d.kind != "accuser") {
        d.delegates.forEach(del => {
            var div = document.createElement("div");
            div.appendChild(mk_link(del))
            del_cell.appendChild(div);
        })
    };
    d.protocols.forEach(p => {
        var div = document.createElement("div");
        div.appendChild(mk_link(p, 8))
        proto_cell.appendChild(div);
    });
    status_cell.appendChild(document.createTextNode(d.state.state));
    if (d.state.code != undefined) {
        status_cell.appendChild(document.createTextNode(
            " (" + d.state.code + ")"));
    };
    var ping = document.createTextNode(new Date(d.last_ping).toLocaleString());
    ping_cell.appendChild(ping);
    if (d.spawned) {
        if (d.state.state == "running") {
            mk_action_btn("Stop", act_cell,
                          "/admin/daemon/" + d.id + "/kill");

        } else  {
            mk_action_btn("Start", act_cell,
                          "/admin/daemon/" + d.id + "/start");
        }
    };
    tr.classList.add("daemon-" + d.state.state);

    if (logging_map.has(d.id)) {
        log_cell.appendChild(logging_map.get(d.id).a);
    } else {
        var log_env = { showing : false };
        logging_map.set(d.id, log_env);
        log_env.a = document.createElement("a");
        log_env.a.innerHTML = "Show";
        log_env.a.href = "#";
        log_env.a.onclick = () => {
            toggle_show_log (d, log_env);
            return false;
        };
        log_cell.appendChild(log_env.a);
    };
    table.appendChild(tr);
}

function hide_log(log_env) {
    clearInterval(log_env.interval);
    log_env.a.innerHTML = "Show";
    log_env.showing = false;
    if (log_env.pre) {
        log_env.pre.remove();
    }
}

async function show_log(daemon, log_env) {
    log_env.pre = document.createElement("pre");
    log_env.a.innerHTML = "Hide";
    log_env.showing = true;
    document.getElementById("logs-div").appendChild(log_env.pre);
    logging_map.forEach((l, id) => {
        if (id != daemon.id) { hide_log(l) }
    });
    update_log(daemon, log_env.pre);
    log_env.interval =
        setInterval(() => update_log(daemon, log_env.pre),
                    2000);
}

async function toggle_show_log (daemon, log_env) {
    if (log_env.showing) {
        hide_log(log_env);
    } else {
        show_log(daemon, log_env);
    }
}

async function get_daemon_log (id) {
    return await get_json("/admin/daemon/" + id + "/log");
}

async function update_log(daemon, pre) {
    var log = await get_daemon_log(daemon.id);
    if (typeof log != "string") {
        log = JSON.stringify(log, null, 2);
    }
    var end_first_line = log.search(/\n/g);
    if(end_first_line != -1) {
        log = log.substring(end_first_line + 1);
    }
    pre.innerHTML = log;
    pre.scrollTop = pre.scrollHeight;
}

async function update_daemons () {
    var daemons = await get_daemons ();
    keep_rows("baker-table", 0);
    keep_rows("endorser-table", 0);
    keep_rows("accuser-table", 0);
    daemons.forEach(mk_daemon);
}


async function start_daemons_manager() {

    async function update_10s(){
        await update_daemons ()
    }

    await init_accounts();

    update_10s();
    setInterval(update_10s, 10000);
}
