
function rpcget(path) {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onload = () => { resolve(xhr) };
        xhr.onerror = () => { reject(new Error("Unreachable " + path)) };
        xhr.open("GET", path);
        xhr.send();
    })
}

function rpcpost(path, data = "{}") {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onload = () => { resolve(xhr) };
        xhr.onerror = () => { reject(new Error("Unreachable " + path)) };
        xhr.open("POST", path);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(data);
    })
}

function rpcstream(path, callback) {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.position = 0;
        xhr.onprogress = () => {
            xhr.newResponse = xhr.responseText.substr(xhr.position);
            xhr.position = xhr.responseText.length;
            callback(xhr);
        };
        xhr.onload = () => { resolve(xhr) };
        xhr.onerror = () => { reject(new Error("Unreachable stream " + path)) };
        xhr.open("GET", path);
        xhr.send();
    })
}
