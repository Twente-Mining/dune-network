(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

(* let () = Ocplib_mapfile.Mapfile.test () *)

open Ocplib_ironmin.Ironmin

let () =
  let handler _id = raise Exit in
  ignore (Sys.signal Sys.sigint (Sys.Signal_handle handler)) ;
  ignore (Sys.signal Sys.sigterm (Sys.Signal_handle handler)) ;
  ()

module Hash = struct
  type t = Context_hash.t

  let to_string h = Context_hash.to_string h

  let of_string h =
    match Context_hash.of_string h with Ok s -> s | _ -> assert false

  let hash_string l =
    let hash = Context_hash.hash_string l in
    if Dune_ironmin.GitStores.debug_iron then
      Printf.eprintf
        "%S = hash_iron %S\n%!"
        (to_string hash)
        (String.concat "" l) ;
    hash
end

module Storage = Ocplib_ironmin.Ironmin_lmdb

module X = Ocplib_ironmin.Ironmin.MakeConcrete (struct
  module Hash = Hash
  module Storage = Storage
  module Key = Dune_ironmin.Storage_keys

  type config = Storage.config

  let create_storage = Storage.create_storage
end)

open X

let string_cut_at s c =
  match String.index s c with
  | exception Not_found ->
      (s, "")
  | pos ->
      (String.sub s 0 pos, String.sub s (pos + 1) (String.length s - pos - 1))

let fake_mode = ref true

let commit_or_abort storage =
  if !fake_mode then Storage.abort storage else Storage.commit storage

let puts = ref 0

let new_put storage =
  incr puts ;
  if !puts mod 1000 = 0 then commit_or_abort storage

let migrate_to_ironmin from_db to_db =
  Printf.eprintf "Upgrading...\n%!" ;
  let others = ref 0 in
  let contents = ref 0 in
  let nodes = ref 0 in
  let commits = ref 0 in
  let add_direct_contents = ref 0 in
  let small_contents = ref 0 in
  let items = ref 0 in
  Storage.fold
    from_db
    (fun ~key value _acc ->
      incr items ;
      if !items mod 10_000 = 0 then Printf.eprintf "%d items\n%!" !items ;
      let len = Bigstring.length value in
      (*      Printf.eprintf "key=%S value=str[%d]\n%!" key len; *)
      let (key_prefix, key_value) = string_cut_at key '/' in
      if key_prefix = IrminMarshaller.contents_prefix then (
        (*        Printf.eprintf "irmin content\n%!"; *)
        incr contents ;
        if len < 32 then incr small_contents
        else
          (*          Printf.eprintf "upgrading node...\n%!"; *)
          let inode = new_inode ~inode_content:(Content value) () in
          OptimizedStorage.put_inode ~storage:to_db ~key:key_value inode ;
          new_put to_db )
      else if key_prefix = IrminMarshaller.node_prefix then (
        incr nodes ;
        (*        Printf.eprintf "irmin node\n%!"; *)
        match
          IrminMarshaller.unmarshal_inode_content
            ~inode_kind:Kind_tree
            from_db
            value
        with
        | Content _ ->
            assert false
        | Tree {tree_files} ->
            let tree_files =
              StringMap.map
                (fun tree_content ->
                  if use_direct_content then
                    match tree_content with
                    | DirectContent _ ->
                        assert false
                    | Inode inode -> (
                      match inode.inode_kind with
                      | Kind_tree ->
                          tree_content
                      | Kind_content -> (
                        match IrminStorage.get_inode from_db inode with
                        | Tree _ ->
                            assert false
                        | Content c ->
                            if Bigstring.length c < 32 then (
                              incr add_direct_contents ;
                              DirectContent (Bigstring.to_string c) )
                            else tree_content ) )
                  else tree_content)
                tree_files
            in
            (*            Printf.eprintf "upgrading node...\n%!"; *)
            let inode =
              new_inode
                ~inode_content:(Tree {tree_files; tree_diff = NoDiff})
                ()
            in
            OptimizedStorage.put_inode ~storage:to_db ~key:key_value inode ;
            new_put to_db ;
            () )
      else if key_prefix = IrminMarshaller.commit_prefix then (
        incr commits ;
        (*        Printf.eprintf "irmin commit\n%!"; *)
        let raw_commit = IrminMarshaller.unmarshal_raw_commit value in
        let _hash : Hash.t =
          OptimizedStorage.put_commit
            ~storage:to_db
            ~hash:(Hash.of_string key_value)
            raw_commit
        in
        new_put to_db )
      else (Printf.eprintf "other\n%!" ; incr others))
    () ;
  Printf.eprintf
    "%d contents/ %d nodes / %d commits\n%!"
    !contents
    !nodes
    !commits ;
  Printf.eprintf "Small contents: %d occurrences\n%!" !small_contents ;
  Printf.eprintf "Small contents: %d added\n%!" !add_direct_contents ;
  Printf.eprintf "Others: %d\n%!" !others ;
  Printf.eprintf "Puts: %d\n%!" !puts ;
  commit_or_abort to_db ;
  Printf.eprintf "Upgraded !\n%!" ;
  ()

module Table = struct
  type t = (int, int ref) Hashtbl.t

  let create () = Hashtbl.create 100

  let add t x =
    try incr (Hashtbl.find t x) with Not_found -> Hashtbl.add t x (ref 1)

  let list_key_value t =
    let list = ref [] in
    Hashtbl.iter (fun key value -> list := (key, !value) :: !list) t ;
    let array = Array.of_list !list in
    Array.sort compare array ; array

  let list_value_key t =
    let list = ref [] in
    Hashtbl.iter (fun key value -> list := (!value, key) :: !list) t ;
    let array = Array.of_list !list in
    Array.sort compare array ; array

  let print oc table ~key ~value t =
    let list = list_value_key t in
    Array.iter
      (fun (value_n, key_n) ->
        Printf.fprintf oc "%s: %d %s of %s %d\n" table value_n value key key_n)
      list

  let print2 oc table ~key ~value t =
    let list = list_key_value t in
    Array.iter
      (fun (key_n, value_n) ->
        Printf.fprintf oc "%s: %d %s of %s %d\n" table value_n value key key_n)
      list
end

module HierarchicalTable = struct
  type t = {
    max_table_size : int;
    mutable table_size : int;
    base_table : (string, int ref) Hashtbl.t;
    top_table : (string, int ref) Hashtbl.t;
  }

  let merge t =
    Hashtbl.iter
      (fun key value ->
        try
          let r = Hashtbl.find t.base_table key in
          r := !r + !value
        with Not_found -> Hashtbl.add t.base_table key value)
      t.top_table ;
    Hashtbl.clear t.top_table ;
    let list = ref [] in
    Hashtbl.iter (fun key value -> list := (!value, key) :: !list) t.base_table ;
    let array = Array.of_list !list in
    Array.sort compare array ;
    let len = Array.length array in
    let keep = min 1000 len in
    for i = 1 to keep do
      let x = array.(len - i) in
      Hashtbl.add t.top_table (snd x) (ref (fst x))
    done ;
    Hashtbl.clear t.base_table ;
    t.table_size <- 0

  let create max_table_size =
    {
      max_table_size;
      table_size = 0;
      base_table = Hashtbl.create 10000;
      top_table = Hashtbl.create 10000;
    }

  let add t x =
    try incr (Hashtbl.find t.base_table x)
    with Not_found ->
      Hashtbl.add t.base_table x (ref 1) ;
      t.table_size <- t.table_size + String.length x ;
      if t.table_size > t.max_table_size then merge t

  let list_key_value t =
    merge t ;
    let list = ref [] in
    Hashtbl.iter (fun key value -> list := (key, !value) :: !list) t.top_table ;
    let array = Array.of_list !list in
    Array.sort compare array ; array

  let list_value_key t =
    merge t ;
    let list = ref [] in
    Hashtbl.iter (fun key value -> list := (!value, key) :: !list) t.top_table ;
    let array = Array.of_list !list in
    Array.sort compare array ; array

  let print oc table ~key ~value t =
    let list = list_value_key t in
    Array.iter
      (fun (value_n, key_n) ->
        Printf.fprintf oc "%s: %d %s of %s %S\n" table value_n value key key_n)
      list

  let print2 oc table ~key ~value t =
    let list = list_key_value t in
    Array.iter
      (fun (key_n, value_n) ->
        Printf.fprintf oc "%s: %d %s of %s %S\n" table value_n value key key_n)
      list
end

let compute_stats storage stats_filename =
  Printf.eprintf "Computing stats...\n%!" ;
  let value_sizes = Table.create () in
  let content_sizes = Table.create () in
  let node_sizes = Table.create () in
  let node_fans = Table.create () in
  let links = HierarchicalTable.create 100_000_000 in
  let direct_contents = HierarchicalTable.create 100_000_000 in
  let irmin_contents = ref 0 in
  let irmin_nodes = ref 0 in
  let irmin_commits = ref 0 in
  let irmin_small_contents = ref 0 in
  let ironmin_contents = ref 0 in
  let ironmin_nodes = ref 0 in
  let ironmin_commits = ref 0 in
  let ironmin_small_contents = ref 0 in
  let unknown = ref 0 in
  let items = ref 0 in
  let total_content_size = ref 0 in
  let total_key_size = ref 0 in
  let ctrl_c = ref false in
  let last_ctrl_c = ref (-100_000_000) in
  let () =
    let handler _id = ctrl_c := true in
    ignore (Sys.signal Sys.sigint (Sys.Signal_handle handler)) ;
    ignore (Sys.signal Sys.sigterm (Sys.Signal_handle handler)) ;
    ()
  in
  let save () =
    let oc = open_out stats_filename in
    Printf.fprintf oc "Total: %d key-value pairs\n%!" !items ;
    Printf.fprintf oc "Irmin:\n" ;
    Printf.fprintf
      oc
      "  %d contents/ %d nodes / %d commits\n"
      !irmin_contents
      !irmin_nodes
      !irmin_commits ;
    Printf.fprintf
      oc
      "  Small contents: %d occurrences\n%!"
      !irmin_small_contents ;
    Printf.fprintf oc "Ironmin:\n" ;
    Printf.fprintf
      oc
      "  %d contents/ %d nodes / %d commits\n"
      !ironmin_contents
      !ironmin_nodes
      !ironmin_commits ;
    Printf.fprintf
      oc
      "  Small contents: %d occurrences\n%!"
      !ironmin_small_contents ;
    Printf.fprintf oc "Unknown: %d\n%!" !unknown ;
    Printf.fprintf oc "Total contents size: %d\n%!" !total_content_size ;
    Printf.fprintf oc "Total key size: %d\n%!" !total_key_size ;
    Table.print oc "vs" ~key:"size" ~value:"values" value_sizes ;
    Table.print oc "cs" ~key:"size" ~value:"contents" content_sizes ;
    Table.print oc "ns" ~key:"size" ~value:"nodes" node_sizes ;
    Table.print oc "nf" ~key:"fan" ~value:"nodes" node_fans ;
    Table.print2 oc "vs2" ~key:"size" ~value:"values" value_sizes ;
    Table.print2 oc "cs2" ~key:"size" ~value:"contents" content_sizes ;
    Table.print2 oc "ns2" ~key:"size" ~value:"nodes" node_sizes ;
    Table.print2 oc "nf2" ~key:"fan" ~value:"nodes" node_fans ;
    HierarchicalTable.print oc "links" ~key:"refs" ~value:"link" links ;
    HierarchicalTable.print
      oc
      "direct"
      ~key:"refs"
      ~value:"content"
      direct_contents ;
    close_out oc ;
    Printf.eprintf "stats saved to '%s'\n%!" stats_filename
  in
  let iter () =
    Storage.fold
      storage
      (fun ~key value _acc ->
        if !ctrl_c then (
          save () ;
          ctrl_c := false ;
          if !items - !last_ctrl_c < 20_000 then exit 2 ;
          last_ctrl_c := !items ) ;
        incr items ;
        if !items mod 10_000 = 0 then Printf.eprintf "%d items\n%!" !items ;
        let len = Bigstring.length value in
        total_content_size := !total_content_size + len ;
        total_key_size := !total_key_size + String.length key ;
        Table.add value_sizes len ;
        (*      Printf.eprintf "key=%S value=str[%d]\n%!" key len; *)
        let (key_prefix, _key_value) = string_cut_at key '/' in
        if key_prefix = IrminMarshaller.contents_prefix then (
          incr irmin_contents ;
          Table.add content_sizes len ;
          if len < 32 then incr irmin_small_contents )
        else if key_prefix = IrminMarshaller.node_prefix then (
          incr irmin_nodes ;
          let inode_content =
            IrminMarshaller.unmarshal_inode_content
              ~inode_kind:Kind_tree
              storage
              value
          in
          Table.add node_sizes len ;
          match inode_content with
          | Tree {tree_files} ->
              Table.add node_fans (StringMap.cardinal tree_files) ;
              StringMap.iter
                (fun _key inode ->
                  match inode with
                  | DirectContent _ ->
                      ()
                  | Inode inode -> (
                    match inode.inode_hash with
                    | None ->
                        assert false
                    | Some hash ->
                        HierarchicalTable.add links hash ))
                tree_files
          | _ ->
              assert false )
        else if key_prefix = IrminMarshaller.commit_prefix then
          incr irmin_commits
        else if key_prefix = OptimizedMarshaller.contents_prefix then (
          incr ironmin_contents ;
          Table.add content_sizes len )
        else if key_prefix = OptimizedMarshaller.node_prefix then (
          incr ironmin_nodes ;
          let inode_content =
            OptimizedMarshaller.unmarshal_inode_content
              ~inode_kind:Kind_tree
              storage
              value
          in
          Table.add node_sizes len ;
          match inode_content with
          | Tree {tree_files} ->
              Table.add node_fans (StringMap.cardinal tree_files) ;
              StringMap.iter
                (fun _key inode ->
                  match inode with
                  | DirectContent c ->
                      HierarchicalTable.add direct_contents c ;
                      Table.add content_sizes len
                  | Inode inode -> (
                    match inode.inode_hash with
                    | None ->
                        assert false
                    | Some hash ->
                        HierarchicalTable.add links hash ))
                tree_files
          | _ ->
              assert false )
        else if key_prefix = OptimizedMarshaller.commit_prefix then
          incr ironmin_commits
        else incr unknown)
      ()
  in
  ( try iter ()
    with exn -> Printf.eprintf "Exception %s\n%!" (Printexc.to_string exn) ) ;
  save ()

type task =
  | Stats
  | MigrateToIronmin
  | MigrateToIrmin
  | PlayIrmin of string
  | PlayIrminCompat of string
  | PlayIronmin of string
  | PrintSteps of string
  | CopyDump

let open_storage ?(readonly = true) filename =
  Printf.eprintf "With %s:\n" filename ;
  let storage =
    Storage.create_storage
      (Ocplib_ironmin.Ironmin_lmdb.create_config
         ~mapsize:400_960_000_000L
         ~readonly
         filename)
  in
  Storage.stats storage ; storage

let open_rw_storage filename = open_storage ~readonly:false filename

let () =
  let databases = ref [] in
  let task = ref None in
  let max_steps = ref None in
  let max_tree_diff = ref None in
  let reads_also = ref None in
  let no_progress = ref None in
  let record_size = ref None in
  let switch = ref None in
  let play ?(finish = fun () -> Lwt.return_unit)
      ~(player : Dune_ironmin.GitStores.player) ~trace ~db () =
    Lwt_main.run
      ( player
          ?record_size:!record_size
          ?reads_also:!reads_also
          ?max_tree_diff:!max_tree_diff
          ?no_progress:!no_progress
          ?max_steps:!max_steps
          ?switch:!switch
          ~trace
          ~db
          ()
      >>= finish )
  in
  let arg_anon root = databases := !databases @ [root] in
  let arg_usage = "tezos-ironmin [OPTIONS] database-dir" in
  let arg_list =
    [ ( "--2irmin",
        Arg.Unit (fun () -> task := Some MigrateToIrmin),
        " Migrate database to Irmin format" );
      ( "--2ironmin",
        Arg.Unit (fun () -> task := Some MigrateToIronmin),
        " Migrate database to Ironmin format" );
      ( "--commit",
        Arg.Clear fake_mode,
        " Commit changes (default is fake mode !)" );
      ( "--stats",
        Arg.Unit (fun () -> task := Some Stats),
        " Iter on database to compute stats" );
      ( "--play-irmin",
        Arg.String (fun dump -> task := Some (PlayIrmin dump)),
        "FILE.dump play with irmin format" );
      ( "--play-irmin-compat",
        Arg.String (fun dump -> task := Some (PlayIrminCompat dump)),
        "FILE.dump play with irmin format" );
      ( "--play-ironmin",
        Arg.String (fun dump -> task := Some (PlayIronmin dump)),
        "FILE.dump play with ironmin format" );
      ( "--max-steps",
        Arg.Int (fun n -> max_steps := Some n),
        "NSTEPS number of operations to play" );
      ( "--print-steps",
        Arg.String (fun dump -> task := Some (PrintSteps dump)),
        "FILE.dump print steps of dump" );
      ( "--reads-also",
        Arg.Unit (fun () -> reads_also := Some true),
        " Also play find/mem/list" );
      ( "--no-progress",
        Arg.Unit (fun () -> no_progress := Some true),
        " Don't print progress every 10 kops" );
      ( "--filter",
        Arg.Unit (fun () -> task := Some CopyDump),
        "SRC_DST Copy records from one source to another" );
      ( "--sizes",
        Arg.Unit (fun () -> record_size := Some true),
        " Output sizes of db" );
      ( "--switch-to-ironmin",
        Arg.Int
          (fun n ->
            switch :=
              Some
                ( n,
                  fun () ->
                    Printf.eprintf "Setting storage to Optimized\n%!" ;
                    storage_config := Optimized )),
        "NOPS Switch to Ironmin after NOPS operations" ) ]
  in
  Arg.parse arg_list arg_anon arg_usage ;
  match (!task, !databases) with
  | (None, databases) ->
      List.iter (fun filename -> ignore (open_storage filename)) databases ;
      Printf.eprintf "%!"
  | (Some Stats, databases) ->
      List.iter
        (fun filename ->
          compute_stats (open_storage filename) (filename ^ ".stats"))
        databases ;
      Printf.eprintf "%!"
  | (Some MigrateToIronmin, [from_db; to_db]) ->
      fake_mode := false ;
      let from_db = open_storage from_db in
      let to_db = open_rw_storage to_db in
      migrate_to_ironmin from_db to_db
  | (Some MigrateToIronmin, [filename]) ->
      let storage = open_rw_storage filename in
      if !fake_mode then (
        Printf.eprintf "Warning: no changes will be committed !\n" ;
        Printf.eprintf "   Use --commit for permanent changes\n%!" ) ;
      migrate_to_ironmin storage storage
  | (Some MigrateToIronmin, _) ->
      Printf.eprintf "Error: --2ironmin expects one or two databases only.\n%!" ;
      exit 2
  | (Some MigrateToIrmin, _) ->
      Printf.eprintf "Error: --2irmin expects one or two databases only.\n%!" ;
      exit 2
  | (Some (PlayIrmin dump_file), [filename]) ->
      let module UseRecorder =
        Dune_ironmin.GitStores.Record (Dune_ironmin.UseIrmin) in
      storage_config := Irmin ;
      play ~player:UseRecorder.play ~trace:dump_file ~db:filename ()
  | (Some (PlayIrmin _), _) ->
      Printf.eprintf "Error: --play-irmin expects one database, only.\n%!" ;
      exit 2
  | (Some (PlayIrminCompat dump_file), [filename]) ->
      let module UseRecorder =
        Dune_ironmin.GitStores.Record (Dune_ironmin.UseIronmin) in
      storage_config := IrminCompat ;
      play ~player:UseRecorder.play ~trace:dump_file ~db:filename ()
  | (Some (PlayIrminCompat _), _) ->
      Printf.eprintf "Error: --play-irmin expects one database, only.\n%!" ;
      exit 2
  | (Some (PlayIronmin dump_file), [filename]) ->
      let module UseRecorder =
        Dune_ironmin.GitStores.Record (Dune_ironmin.UseIronmin) in
      storage_config := Optimized ;
      let finish () =
        let keys = Dune_ironmin.Storage_keys.stats () in
        Array.iteri (fun i n -> Printf.eprintf "keys[%d] : %d\n" i n) keys ;
        Printf.eprintf "%!" ;
        Lwt.return_unit
      in
      play ~player:UseRecorder.play ~trace:dump_file ~db:filename ~finish ()
  | (Some (PlayIronmin _), _) ->
      Printf.eprintf "Error: --play-ironmin expects one database, only.\n%!" ;
      exit 2
  | (Some (PrintSteps dump_file), []) ->
      let module UseRecorder =
        Dune_ironmin.GitStores.Record (Dune_ironmin.UseIronmin) in
      storage_config := Optimized ;
      let _n = UseRecorder.print ~trace:dump_file in
      ()
  | (Some (PrintSteps _), _) ->
      Printf.eprintf "Error: --print-steps expects no database.\n%!" ;
      exit 2
  | (Some CopyDump, [src; dst]) ->
      let module UseRecorder =
        Dune_ironmin.GitStores.Record (Dune_ironmin.UseIronmin) in
      storage_config := Optimized ;
      UseRecorder.copy ?reads_also:!reads_also ?max_steps:!max_steps ~src ~dst ;
      ()
  | (Some CopyDump, _) ->
      Printf.eprintf "Error: --copy expects two databases, only.\n%!" ;
      exit 2
