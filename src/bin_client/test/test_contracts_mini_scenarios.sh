#!/bin/bash

set -e
set -o pipefail

test_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)")"
source $test_dir/test_lib.inc.sh "$@"

start_node 1
activate_alpha

$client -w none config update

bake

key1=foo
key2=bar

$client gen keys $key1
$client gen keys $key2

printf "\n\n"

# Assert all contracts typecheck
if [ ! $NO_TYPECHECK ] ; then
    for contract in `ls $contract_scenarios_dir/*.tz`; do
        printf "[Type-checking %s]\n" "$contract";
        ${client} typecheck script "$contract";
    done
    printf "All contracts are well typed\n\n"
fi

# Assert deprecated contracts fail type-check
if [ ! $NO_TYPECHECK ] ; then
    for contract in `ls $contract_deprecated_dir/*.tz`; do
        printf "[Expecting type-check fail %s]\n" "$contract";
        assert_fails ${client} typecheck script "$contract";
    done
    printf "All deprecated contracts don't type-check\n\n"
fi

# FORMAT: assert_output contract_file storage input expected_result

# TODO add tests for the following contracts
# lockup, originator, parameterized_multisig, reservoir, scrutable_reservoir,
# weather_insurance, xcat_dapp, xcat
# NB: hardlimit.tz is tested in test_basic.sh

# Test replay prevention
init_with_transfer $contract_scenarios_dir/replay.tz Unit 0 bootstrap1
assert_fails $client transfer 0 from bootstrap1 to replay --burn-cap 10

# Creates a contract, transfers data to it and stores the data
init_with_transfer $contract_scenarios_dir/create_contract.tz Unit 1,000 bootstrap1
assert_balance create_contract "1000 ꜩ"
created_contract=\
`$client transfer 0 from bootstrap1 to create_contract -arg 'None' --burn-cap 10 \
| grep 'New contract' \
| sed -E 's/.*(KT1[a-zA-Z0-9]+).*/\1/' \
| head -1`
bake
assert_storage_contains $created_contract '"abcdefg"'
assert_balance $created_contract "100 ꜩ"
assert_balance create_contract "900 ꜩ"

# Test IMPLICIT_ACCOUNT
init_with_transfer $contract_scenarios_dir/default_account.tz \
				   Unit 1,000 bootstrap1
bake_after $client transfer 0 from bootstrap1 to default_account  -arg "\"$BOOTSTRAP4_IDENTITY\"" --burn-cap 10
assert_balance $BOOTSTRAP4_IDENTITY "4000100 ꜩ"
account=tz1SuakBpFdG9b4twyfrSMqZzruxhpMeSrE5
bake_after $client transfer 0 from bootstrap1 to default_account  -arg "\"$account\"" --burn-cap 10
assert_balance $account "100 ꜩ"

# Test bytes, SHA256, CHECK_SIGNATURE
init_with_transfer $contract_scenarios_dir/reveal_signed_preimage.tz \
				   '(Pair 0x9995c2ef7bcc7ae3bd15bdd9b02dc6e877c27b26732340d641a4cbc6524813bb "p2pk66uq221795tFxT7jfNmXtBMdjMf6RAaxRTwv1dbuSHbH6yfqGwz")' 1,000 bootstrap1
assert_fails $client transfer 0 from bootstrap1 to reveal_signed_preimage -arg \
             '(Pair 0x050100000027566f756c657a2d766f757320636f75636865722061766563206d6f692c20636520736f6972 "p2sigvgDSBnN1bUsfwyMvqpJA1cFhE5s5oi7SetJVQ6LJsbFrU2idPvnvwJhf5v9DhM9ZTX1euS9DgWozVw6BTHiK9VcQVpAU8")'  --burn-cap 10
assert_fails $client transfer 0 from bootstrap1 to reveal_signed_preimage -arg \
             '(Pair 0x050100000027566f756c657a2d766f757320636f75636865722061766563206d6f692c20636520736f6972203f "p2sigvgDSBnN1bUsfwyMvqpJA1cFhE5s5oi7SetJVQ6LJsbFrU2idPvnvwJhf5v9DhM9ZTX1euS9DgWozVw6BTHiK9VcQVpAU8")'  --burn-cap 10
assert_success $client transfer 0 from bootstrap1 to reveal_signed_preimage -arg \
               '(Pair 0x050100000027566f756c657a2d766f757320636f75636865722061766563206d6f692c20636520736f6972203f "p2sigsceCzcDw2AeYDzUonj4JT341WC9Px4wdhHBxbZcG1FhfqFVuG7f2fGCzrEHSAZgrsrQWpxduDPk9qZRgrpzwJnSHC3gZJ")'  --burn-cap 10
bake

# Test SET_DELEGATE
b2='tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN'
b3='tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU'
b4='tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv'
b5='tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv'
init_with_transfer $contract_scenarios_dir/vote_for_delegate.tz \
				   "(Pair (Pair \"$b3\" None) (Pair \"$b4\" None))" 1,000 bootstrap1
$client get delegate for vote_for_delegate | assert_in_output none

assert_fails $client transfer 0 from bootstrap1 to vote_for_delegate -arg None --burn-cap 10
assert_fails $client transfer 0 from bootstrap2 to vote_for_delegate -arg None --burn-cap 10
bake_after $client transfer 0 from bootstrap3 to vote_for_delegate -arg "(Some \"$b5\")" --burn-cap 10
assert_storage_contains vote_for_delegate "\"$b5\""
$client get delegate for vote_for_delegate | assert_in_output none
bake_after $client transfer 0 from bootstrap4 to vote_for_delegate -arg "(Some \"$b2\")" --burn-cap 10
assert_storage_contains vote_for_delegate "\"$b2\""
$client get delegate for vote_for_delegate | assert_in_output none
bake_after $client transfer 0 from bootstrap4 to vote_for_delegate -arg "(Some \"$b5\")" --burn-cap 10
$client get delegate for vote_for_delegate | assert_in_output "$b5"

# Test votes, fee code
echo "Test votes, fee code"

b1='tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'
b2='tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN'
b3='tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU'
b4='tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv'
b5='tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv'

echo "Deploy votes"
init_with_transfer $contract_scenarios_dir/votes.tz \
		   "Pair {} (Pair { Elt \"1\" 0 ; Elt \"2\" 0 ; Elt \"3\" 0 ; Elt \"4\" 0 ; Elt \"5\" 0 } { Elt \"1\" \"$b1\" ; Elt \"2\" \"$b2\" ; Elt \"3\" \"$b3\" ; Elt \"4\" \"$b4\" ; Elt \"5\" \"$b5\" })" 1,000 bootstrap1

echo "Re-Deploy votes (code-hash)"
init_with_transfer $contract_scenarios_dir/votes2.tz \
		   "Pair {} (Pair { Elt \"1\" 0 ; Elt \"2\" 0 ; Elt \"3\" 0 ; Elt \"4\" 0 ; Elt \"5\" 0 } { Elt \"1\" \"$b1\" ; Elt \"2\" \"$b2\" ; Elt \"3\" \"$b3\" ; Elt \"4\" \"$b4\" ; Elt \"5\" \"$b5\" })" 1,000 bootstrap1

echo "Testing collect fees with too high fee"
assert_fails $client transfer 5 from bootstrap1 to votes --arg 'Left "1"' --collect-call --fee 0.010006
echo "Testing votes with too low amount"
assert_fails $client transfer 4 from bootstrap1 to votes --arg 'Left "1"'
echo "Testing votes with too low collect fee gas"
assert_fails $client transfer 5 from bootstrap1 to votes --arg 'Left "1"' --collect-call-fee-gas 100
echo "Testing votes with collect fee gas higher than hard limit"
assert_fails $client transfer 5 from bootstrap1 to votes --arg 'Left "1"' --collect-call-fee-gas 40001
echo "Testing votes with collect fee gas > gas"
assert_fails $client transfer 5 from bootstrap1 to votes --arg 'Left "1"' --collect-call-fee-gas 10000 -G 9999

echo "Testing collect call 1, should succeed"
$client transfer 5 from bootstrap1 to votes --arg 'Left "1"' --collect-call -G 97300
echo "Testing vote with already voted sender"
assert_fails $client transfer 5 from bootstrap1 to votes --arg 'Left "1"'
bake

echo "Testing collect call 2, fee gas = 13000"
$client transfer 5 from bootstrap2 to votes --arg 'Left "1"' --collect-call-fee-gas 13000
echo "Testing collect call 3"
$client transfer 5 from bootstrap3 to votes --arg 'Left "2"' --collect-call
echo "Testing call 4"
$client transfer 5 from bootstrap4 to votes --arg 'Left "1"' --burn-cap 0.068
echo "Testing collect call 5"
$client transfer 5 from bootstrap5 to votes --arg 'Left "1"' --collect-call
bake

balance_bootstrap5="$($client get balance for bootstrap5)"
echo "Testing collect call 5 close vote"
$client transfer 0 from bootstrap5 to votes --arg 'Right Unit' --collect-call
bake

echo "Balance should be unchanged, no fees were payed by $b5"
assert_balance bootstrap5 "$balance_bootstrap5" # no fees payed
echo "Balance of votes should be 0"
assert_balance votes "0 ꜩ" # everything transfered to b1

# DEPRECATION tests

# Tests create_account fails - uses deprecated instruction 'CREATE_ACCOUNT'
assert_fails_init_with_transfer $contract_scenarios_dir/create_account.tz None 1,000 bootstrap1


printf "\nEnd of test\n"

show_logs="no"
