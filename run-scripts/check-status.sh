#!/bin/sh

# exit 0 => command is running
# exit 1 => command is not running
# exit 2 => script error


if test -z "$PIDFILE"; then
    echo "check-status.sh should be called from another script."
    exit 2
fi

if test -f $PIDFILE; then
    PID=$(cat $PIDFILE)
    if kill -0 $PID; then
	echo "$COMMAND_NAME is running ! (pid=$PID)"
	exit 0
    else
	echo "$COMMAND_NAME is down. Removing pid file."
	rm -f $PIDFILE
	exit 1
    fi
else
    echo $COMMAND_NAME is not running
    exit 1
fi
