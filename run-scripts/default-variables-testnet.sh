DUNE_EXECDIR=$HOME/dune-testnet-2019-07-08
DUNE_NET_PORT=9734

# RPC accessible only on this computer
DUNE_RPC_ADDR=127.0.0.1
DUNE_RPC_PORT=8734

DUNE_HISTORY_MODE=full

# Set this one to 0 if you are alone
DUNE_BOOTSTRAP_THRESHOLD=1
