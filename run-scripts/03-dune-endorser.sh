#!/bin/bash

LOG_PREFIX="[Dune Node]"
COMMAND_NAME=dune-endorser-all

DUNE_SRCDIR=$(dirname $(cd $(dirname $0); pwd))
SCRIPTS_DIR=$DUNE_SRCDIR/run-scripts
source $SCRIPTS_DIR/set-variables.sh

stop_dune_endorser () {
    $SCRIPTS_DIR/stop-process.sh
}

status_dune_endorser () {
    $SCRIPTS_DIR/check-status.sh
}

start_dune_endorser () {

    $SCRIPTS_DIR/check-status.sh

    if [ $? -eq 1 ]; then

	DUNE_ROTATE=$DUNE_SRCDIR/dune-rotate
	export DUNE_ROTATE

	mkdir -p $DUNE_LOGDIR
	mkdir -p $DUNE_RUNDIR
	mkdir -p $DUNE_CMDDIR

	cp -f $DUNE_SRCDIR/$COMMAND_NAME $DUNE_CMDDIR/

	$DUNE_ROTATE $LOGFILE

	if [ -f $GENESIS_DIR/genesis-env.sh ]; then
	    source $GENESIS_DIR/genesis-env.sh
	    mkdir -p $DUNE_CLIDIR

	    sed \
		-e "s/NAME/baker/" \
		-e "s/KEYHASH/$BAKER_KEYHASH/" \
		$SCRIPTS_DIR/client/public_key_hashs \
		> $DUNE_CLIDIR/public_key_hashs
	    sed \
		-e "s/NAME/baker/" \
		-e "s/PUBKEY/$BAKER_PUBKEY/" \
		$SCRIPTS_DIR/client/public_keys \
		> $DUNE_CLIDIR/public_keys
	    sed \
		-e "s/NAME/baker/" \
		-e "s/PRIVKEY/$BAKER_PRIVKEY/" \
		$SCRIPTS_DIR/client/secret_keys \
		> $DUNE_CLIDIR/secret_keys

	    PROTO_SHORT_HASH=$(echo $PROTO_HASH | cut -b 1-8)

	    $DUNE_CMDDIR/$COMMAND_NAME --base-dir $DUNE_CLIDIR  -A $DUNE_RPC_ADDR -P $DUNE_RPC_PORT run baker >& $LOGFILE &
	    echo $! > $PIDFILE
	    echo "Dune endorser started..."
	    sleep 1
	    $SCRIPTS_DIR/check-status.sh
	    echo "You can watch the log:"
	    echo tail -f $LOGFILE

	else
	    echo You have to set GENESIS_DIR in '$HOME/dune-env.sh'. Example:
	    echo GENESIS_DIR='$HOME/GIT/dune-genesis/testnet'
	fi


    fi
}

case "$1" in
    start)
        echo "$LOG_PREFIX == Starting =="
        start_dune_endorser
	;;

    debug)
        echo "$LOG_PREFIX == Starting =="
        DUNE_LOG='* -> debug'
        export DUNE_LOG
        start_dune_endorser
	;;

    stop)
        echo "$LOG_PREFIX == Stopping =="
        stop_dune_endorser
        ;;

    restart)
        echo "$LOG_PREFIX == Restarting =="
        echo "$LOG_PREFIX == Stopping =="
        stop_dune_endorser
        echo "$LOG_PREFIX == Starting =="
        start_dune_endorser
        ;;

    status)
        echo "$LOG_PREFIX == Status == "
        status_dune_endorser
        ;;

    head)
        echo "$LOG_PREFIX == HEAD == "
	head_dune_endorser
        ;;

    *)
        echo "Usage: ./dune-node.sh {start|stop|restart|status|head}"
        exit 1
        ;;

esac

exit 0
