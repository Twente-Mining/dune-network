(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Ocplib_mapfile

let debug_lmdb = Ironmin.if_var_exists "IRONMIN_DEBUG"

module Lmdb = struct

  include Lmdb

  let put_string t db key value =
    if debug_lmdb then
      Printf.eprintf "Iron.put_string %S : %S\n%!" key value;
    put_string t db key value

end

module KeyValueStore : sig
  include Ironmin.IronminKeyValue

  val create : ?mapsize:int64 -> ?readonly:bool -> string -> t

end = struct

  (* How many time a read transaction can be used before being
     aborted.  It avoids keeping very old read transactions,
     preventing LMDB from reusing space. *)
  let max_counter = 100

  type t = {
    lmdb : Lmdb.t ;
    mutable wtxn: (Lmdb.rw Lmdb.txn * Lmdb.db) option;
    mutable rtxn: (Lmdb.ro Lmdb.txn * Lmdb.db * int ref) option;
  }

  let ok =
    function
    | Ok x -> x
    | Error error ->
        Printf.eprintf "Lmdb Error %s\n%!" (Lmdb.string_of_error error);
        failwith "lmdb-error"

  let get_wtxn t =
    match t.wtxn with
    | None ->
        begin
          match t.rtxn with
          | None -> ()
          | Some ( txn, _db, _counter ) ->
              ok ( Lmdb.commit_txn txn );
              t.rtxn <- None
        end;
        let txn = ok ( Lmdb.create_rw_txn t.lmdb ) in
        let db = ok ( Lmdb.opendb txn ) in
        t.wtxn <- Some ( txn, db ) ;
        ( txn, db )
    | Some ( txn, db ) -> ( txn, db )

  let check_update t =
    match t.rtxn with
    | None -> ()
    | Some (txn, _db, _counter) ->
        t.rtxn <- None;
        Lmdb.abort_txn txn;
        ()

  let rec get_rtxn t =
    match t.rtxn with
    | None ->
        let txn = ok ( Lmdb.create_ro_txn t.lmdb ) in
        let db = ok ( Lmdb.opendb txn ) in
        t.rtxn <- Some ( txn, db, ref 0 ) ;
        ( txn, db )
    | Some ( txn, db, counter ) ->
        incr counter;
        if !counter = max_counter then begin
          t.rtxn <- None;
          Lmdb.abort_txn txn;
          get_rtxn t
        end
        else
          ( txn, db )

  let mem t ~key =
    match t.wtxn with
    | None ->
        let ( txn, db ) = get_rtxn t in
        ok ( Lmdb.mem txn db key )
    | Some ( txn, db ) ->
        ok ( Lmdb.mem txn db key )

  let get t ~key =
    match
      match t.wtxn with
      | None ->
          let ( txn, db ) = get_rtxn t in
          Lmdb.get txn db key
      | Some ( txn, db ) ->
          Lmdb.get txn db key
    with
    | Error _ -> None
    | Ok buffer -> Some buffer
  (*      Some ( Cstruct.to_string (Cstruct.of_bigarray buffer) ) *)

  let put t ~key value =
    let ( txn, db ) = get_wtxn t in
    ok ( Lmdb.put_string txn db key value )

  let remove t ~key =
    let ( txn, db ) = get_wtxn t in
    ok ( Lmdb.del txn db key )

  let create ?mapsize ?readonly dir =
    let flags =
      if readonly = Some true then [ Lmdb.RdOnly ] else [] in
    if not (Sys.file_exists dir) then Unix.mkdir dir 0o755 ;
    let flags = Lmdb.NoRdAhead :: Lmdb.NoTLS :: flags in
    let lmdb = ok ( Lmdb.opendir
                      ~flags
                      ?mapsize
                      dir 0o644 )
    in
    let wtxn = None in
    let rtxn = None in
    { lmdb ; wtxn ; rtxn }


  let commit t =
    begin
      match t.wtxn with
      | None -> ()
      | Some ( txn, _db ) ->
          t.wtxn <- None;
          ok ( Lmdb.commit_txn txn )
    end;
    begin
      match t.rtxn with
      | None -> ()
      | Some ( txn, _db, _counter ) ->
          t.rtxn <- None;
          ok (Lmdb.commit_txn txn)
    end;
    ()

  let abort t =
    begin
      match t.wtxn with
      | None -> ()
      | Some ( txn, _db ) ->
          t.wtxn <- None;
          Lmdb.abort_txn txn
    end;
    begin
      match t.rtxn with
      | None -> ()
      | Some ( txn, _db, _counter ) ->
          t.rtxn <- None;
          Lmdb.abort_txn txn
    end;
    ()

  let close t =
    abort t;
    Lmdb.closedir t.lmdb

  let fold t f acc =
    ok (Lmdb.with_ro_db t.lmdb ~f:(fun txn db ->
        Lmdb.with_cursor txn db
          ~f:(fun cursor ->
              ok (Lmdb.cursor_first cursor);
              (* For some reason, cursor_fold fails if the cursor is not
                 yet set, for example to first. *)
              Lmdb.cursor_fold_left
                ~init:acc
                ~f:(fun acc (key, value) ->
                    let key = Bigstring.to_string key in
                    Ok (f ~key value acc))
                cursor)))

  let stats t =
    let s = Lmdb.stat t.lmdb in
    let i = Lmdb.envinfo t.lmdb in
    Printf.eprintf "mapsize: %d\n" i.Lmdb.mapsize ;
    Printf.eprintf "psize: %d\n" s.Lmdb.psize ;
    Printf.eprintf "depth: %d\n" s.Lmdb.depth ;
    Printf.eprintf "branch_pages: %d\n" s.Lmdb.branch_pages ;
    Printf.eprintf "leaf_pages: %d\n" s.Lmdb.leaf_pages ;
    Printf.eprintf "overflow_pages: %d\n" s.Lmdb.overflow_pages ;
    Printf.eprintf "entries: %d\n" s.Lmdb.entries ;
    Printf.eprintf "%!"

  let length t =
    let s = Lmdb.stat t.lmdb in
    s.Lmdb.entries

end

type t = {
  dir : string ;
  mapsize : int64 option;
  keys : KeyValueStore.t ;
  mutable saved : int option ;
  mutable off : int ;
  store_name : string ;
  store : Mapfile.t ;
}

let length t = KeyValueStore.length t.keys
let fold t = KeyValueStore.fold t.keys
let get t ~key = KeyValueStore.get t.keys ~key
let put t ~key s = KeyValueStore.put t.keys ~key s
let remove t ~key = KeyValueStore.remove t.keys ~key
let mem t ~key = KeyValueStore.mem t.keys ~key
let check_update t = KeyValueStore.check_update t.keys

let position_key = String.make 16 '\000'

let put_store_offset t =
  let s = Bytes.create 8 in
  EndianString.BigEndian.set_int64 s 0 (Int64.of_int t.off);
  let key = position_key in
  KeyValueStore.put t.keys ~key (Bytes.to_string s)


let revert t =
  Mapfile.truncate t.store 0;
  t.off <- 0;
  t.saved <- None;
  put_store_offset t;
  Mapfile.close t.store;
  Sys.remove t.store_name

let close t =
  KeyValueStore.close t.keys;
  Mapfile.close t.store

type config = {
  mapsize : int64 option ;
  readonly : bool option ;
  root : string ;
}

let create_config ?mapsize ?readonly root =
  { mapsize ; readonly ; root }

let create_storage { mapsize ; readonly ; root } =
  let keys = KeyValueStore.create ?mapsize ?readonly root in
  let saved = None in
  let store_name = Filename.concat root "mapfile.bin" in
  let store =
    let mapsize = match mapsize with
      | Some mapsize -> mapsize
      | None -> 400_000_000_000L
    in
    Mapfile.openfile ?readonly ~mapsize store_name
  in
  let off = 0 in
  let t =   { dir = root ; mapsize ; keys ;
              saved ; off ; store_name ; store } in
  begin
    let key = position_key in
    match KeyValueStore.get t.keys ~key with
    | Some s ->
        t.off <-
          Int64.to_int ( EndianBigstring.BigEndian.get_int64 s 0 )
    | None ->
        if readonly = Some true then failwith "Store not initialized";
        put_store_offset t
  end;
  t

let commit t =
  begin
    match t.saved with
    | None -> ()
    | Some _ ->
        put_store_offset t;
        t.saved <- None;
  end;
  KeyValueStore.commit t.keys

let abort t =
  KeyValueStore.abort t.keys;
  begin
    match t.saved with
    | None -> ()
    | Some off ->
        t.off <- off;
        t.saved <- None;
  end;
  ()

let at t ~off =
  if off >= t.off then begin
    let key = position_key in
    match KeyValueStore.get t.keys ~key with
    | Some s ->
        t.off <-
          Int64.to_int ( EndianBigstring.BigEndian.get_int64 s 0 )
    | None -> assert false
  end;
  assert (off < t.off);
  Mapfile.read t.store ~off

let added = ref 0

let store t s =
  let off = t.off in
  begin match t.saved with
    | None -> t.saved <- Some off
    | Some _ -> ()
  end;
  t.off <- Mapfile.write_string t.store ~off s ~pos:0 ~len:(String.length s);
  if t.off - off < 8 then
    added := !added + (8 - off + t.off);
  off

let iter t ?off f =
  Mapfile.iter t.store ?off ~max_off:t.off f

let dir t = t.dir

(* TODO: remove dir if it already exists *)
let clone ( t : t ) dir =
  (*  assert (not (Sys.file_exists dir)); *)
  create_storage { root = dir ; mapsize = t.mapsize ; readonly = None }

let stats t =
  KeyValueStore.stats t.keys;
  Printf.eprintf "added: %d\n%!" !added;
  Printf.eprintf "total: %d\n%!" t.off;
  ()
