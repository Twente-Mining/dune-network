(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2019 Origin Labs                                          *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)


(* TODO: We could remove this dependency, and move it to tezos-ironmin. *)
(* Our IrminStorage is only compatible with Irmin for a node in
   archive mode.  Indeed, the semantics of `tree` is different, our
   `tree` is a map between basenames and files, whereas, in Irmin,
   `tree` can be a map, a hash (the hash of the tree in the db), or a
   pair map+hash. The only place where it makes a difference is in
   full/rolling modes, where some contexts are "pruned", i.e. the
   "data" subtree is actually non-existing, but still has a key in the
   top tree. Tree.find "data" will fail with IrminStorage, but not
   with Irmin.
   TODO: we could probably change this behavior using FakeTree. *)
open Lwt


let hits = ref 0

let load_irmin_contents = ref 0
let save_irmin_contents = ref 0
let load_irmin_trees = ref 0
let save_irmin_trees = ref 0

let load_ironmin_contents = ref 0
let save_ironmin_contents = ref 0
let load_ironmin_trees = ref 0
let save_ironmin_trees = ref 0

let use_direct_content = true

let if_var_exists var =
  try
    ignore (Sys.getenv var); true
  with Not_found -> false

let int_var_or var dft =
  try
    int_of_string (Sys.getenv var)
  with Not_found ->
    dft



(* Beware, the cache cannot be used if the data migrates in the
   database.  That's the case if we keep Bigstring pointers to the
   LMDB database that are not cleared during commits. *)
let no_cache = if_var_exists "IRONMIN_NO_CACHE"
let debug_api = if_var_exists "IRONMIN_DEBUG_API"

(* Our experimentations show that:
   40-50 is equivalent, 50 is slower, less than 40 takes a little more
   space, about 5% *)
let direct_content_max_size = int_var_or "IRONMIN_DIRECT_CONTENT" 40


(* Setting IRONMIN_MAX_DIFFS to 0 means no diff on diff *)
let max_diffs = int_var_or "IRONMIN_MAX_DIFFS" 0

module type Int64Marshaller = sig

  val len_uint64 : int64 -> int
  val set_uint64 : bytes -> int -> int64 -> int
  module BIGSTRING : sig
    val get_uint64 : Bigstring.t -> int -> int64 * int
  end
end

type storage_config =
  | Irmin       (* Keep using Irmin *)
  | IrminCompat (* Replace Irmin only *)
  | Optimized   (* Tezos compatibility with optimized storage, even
                   with former database *)

let storage_config = ref Optimized

module StringMap = Map.Make(String)


module type IronminKeyValue = sig
  type t

  (* key x value *)
  val mem : t -> key:string -> bool
  val get : t -> key:string -> Bigstring.t option
  val check_update : t -> unit
  val put : t -> key:string -> string -> unit
  val remove : t -> key:string -> unit
  val commit : t -> unit
  val abort : t -> unit
  val fold : t -> (key:string -> Bigstring.t -> 'a -> 'a) -> 'a -> 'a
  val length : t -> int
  val stats : t -> unit

  val close : t -> unit

end

module type IronminStorage = sig

  (* key x value *)
  include IronminKeyValue

  (* append-only *)
  val at : t -> off:int -> Bigstring.t
  val store : t -> string -> int
  val iter : t -> ?off:int -> (off:int -> Bigstring.t -> unit) -> unit
  val revert : t -> unit

  val dir : t -> string
  val clone : t -> string -> t
end

module type Key = sig
  type t
  val create : string -> t * int
  val set : Bytes.t -> int -> t -> int
  module BIGSTRING : sig
    val get : Bigstring.t -> int -> string * int
  end
end

let hash_len = 32

module Make(S : sig

    module Hash : sig
      type t
      val hash_string : string list -> t
      val to_string : t -> string
      val of_string : string -> t
    end

    module Storage : IronminStorage
    module Key : Key

    type config
    val create_storage : config -> Storage.t

  end ) = struct

  let init () = ()

  module Storage = S.Storage
  module Key = S.Key
  module Hash = struct
    include S.Hash

    let () =
      assert (String.length
                (to_string (hash_string [])) = hash_len)

    let hash_string _msg s =
      let h = hash_string s in
      (*      Printf.eprintf "%S = hash_string{%s} [ %S ]\n%!"
              (to_string h) msg (String.concat "" s); *)
      h

  end
  type metadata = unit
  type unit = metadata
  type config = S.config
  type info = { date : int64 ; author : string ; message : string }
  type inode_kind = Kind_content | Kind_tree

  (*  type position = int *)

  type raw_commit = {
    tree_hash : string ;
    tree_off : int option ; (* only used by Irontez *)
    parents : string list ;
    info : info;
  }

  type inode_storage =
    | IrminStorage of Storage.t
    | IronminStorage of Storage.t

  type commit = {
    repo : repo ;
    tree : inode ;
    raw : raw_commit ;
    hash : Hash.t ;
  }
  (* inode_kind is necessary to be able to store the content directly.
     It tells us how to unmarshal a block *)
  and inode = {
    mutable inode_storage : inode_storage option ;
    mutable inode_hash : string option ;
    mutable inode_off : int option ;
    inode_kind : inode_kind ;
    mutable inode_content : inode_content option ;
  }
  and inode_content =
    | Content of Bigstring.t
    | Tree of tree
  and file =
    | DirectContent of string
    | Inode of inode

  and tree_op =
    | AddKey of string * file
    | RemoveKey of string

  and tree_diff =
    | NoDiff
    | Diff of inode * tree_op list
    | FakeTree of string (* hash *)
  (* We introduce the notion of FakeTree, because that's the way
       contexts for pruned blocks are built, using an empty
       tree under "data", but with a hash. *)
  and tree = {
    tree_files : file StringMap.t ;
    tree_diff : tree_diff ;
  }
  and repo = {
    mutable storage : Storage.t Lazy.t;
    cache : (Hash.t, commit) FifoCache.t;
  }

  let storage_mem storage ~key = Storage.mem storage ~key
  let storage_put storage ~key value =
    if not ( Storage.mem storage ~key ) then
      Storage.put storage ~key value

  let storage_remove storage ~key =
    assert ( Storage.mem storage ~key ) ;
    Storage.remove storage ~key

  let storage_get storage ~key =
    Storage.get storage ~key

  let storage_check_update storage =
    Storage.check_update storage

  let new_inode
      ?inode_off ?inode_storage ?inode_kind ?inode_content ?inode_hash () =
    let inode_kind = match inode_kind with
      | Some inode_kind -> inode_kind
      | None -> match inode_content with
        | None -> assert false
        | Some (Content _) -> Kind_content
        | Some (Tree _) -> Kind_tree
    in
    begin
      match inode_hash, inode_off, inode_storage with
      | Some _, Some _, Some _ -> ()
      | Some _, None, Some _ -> ()
      | None, Some _, Some _ -> ()
      | None, None, None -> ()
      | None, None, Some _ -> assert false
      | Some _, None, None -> assert false
      | Some _, Some _, None -> assert false
      | None, Some _, None -> assert false
    end;
    {
      inode_storage;
      inode_content ;
      inode_off ;
      inode_kind ;
      inode_hash ;
    }

  module type Encoding = sig

    (* raise Not_found if not present *)
    val get_inode : Storage.t -> inode -> inode_content
    val get_commit : Storage.t -> hash:Hash.t -> raw_commit

    val put_inode : storage:Storage.t -> ?key:string -> inode -> unit
    val put_commit : storage:Storage.t -> ?hash:Hash.t -> raw_commit -> Hash.t
    val remove_commit : storage:Storage.t -> hash:Hash.t -> unit

    val hash_content: inode_content -> string
    val hash_content_to_hash: inode_content -> Hash.t
    val hash_commit : raw_commit -> Hash.t

  end

  module type StorageMarshaller = sig

    val contents_prefix : string
    val node_prefix : string
    val commit_prefix : string

    val key_of_key : prefix:string -> string -> string

    val unmarshal_raw_commit : EndianBigstring.bigstring -> raw_commit
    val unmarshal_inode_content :
      inode_kind:inode_kind -> Storage.t -> Bigstring.t -> inode_content

    val marshal_commit_to_hash : ?to_store:string -> raw_commit -> string
    val marshal_commit_to_store : ?to_hash:string -> raw_commit -> string

    val marshal_content_to_hash : ?to_store:string -> inode_content -> string
    val marshal_content_to_store : ?to_hash:string -> inode_content -> string

  end

  module IrminInt64 : Int64Marshaller = struct

    module LE = EndianString.BigEndian
    module BLE = EndianBigstring.BigEndian

    let len_uint64 _v = 8
    let set_uint64 s pos v = LE.set_int64 s pos v; pos + len_uint64 v
    module BIGSTRING = struct
      let get_uint64 s pos = BLE.get_int64 s pos , pos + 8
    end
  end

  module IrminMarshaller : StorageMarshaller =
  struct

    let contents_prefix = "contents"
    let node_prefix = "node"
    let commit_prefix = "commit"

    let key_of_key ~prefix key =
      Printf.sprintf "%s/%s" prefix key

    open IrminInt64


    (* Encoding and hashing of Irmin:
       string: len[8 bytes] + content[len bytes]
       bool: true=\255, false=\000
       list: len[8 bytes] + items

       Content: string (8 + len)
       Node:
       nitems[8 bytes]
       each item:
        is_file[bool]
        name[string]
        hash[string]

       Same as lexicographical order, except that for common prefixes,
       nodes are supposed to end with '/' whereas files end with '\000'.

    *)

    let len_int v = len_uint64 (Int64.of_int v)
    let len_string s =
      let len = String.length s in
      len_int len + len
    let len_list len_item list =
      let rec iter len_item len nitems list =
        match list with
          [] -> len + len_int nitems
        | item :: items ->
            let len = len + len_item item in
            iter len_item len (nitems+1) items
      in
      iter len_item 0 0 list

    let set_int s pos v = set_uint64 s pos (Int64.of_int v)
    let get_int s pos =
      let i, pos = BIGSTRING.get_uint64 s pos in
      Int64.to_int i, pos
    let set_bool s pos v =
      Uintvar.BYTES.set_uint8 s pos (if v then 255 else 0)
    let get_bool s pos =
      let b, pos = Uintvar.BIGSTRING.get_uint8 s pos in
      match b with
      | 0 -> false, pos
      | 255 -> true, pos
      | _ ->
          let s = Bigstring.to_string s in
          let len = String.length s in
          Printf.eprintf "get_bool %d/%d %S\n%!" pos len s;
          Printf.eprintf "at %S\n%!" (String.sub s pos (len-pos));
          assert false
    let set_string s pos str =
      let len = String.length str in
      let pos = set_int s pos len in
      Bytes.blit_string str 0 s pos len;
      pos+len
    let get_string s pos =
      let len, pos = get_int s pos in
      let s = Bigstring.sub_string s pos len in
      let pos = pos + len in
      ( s, pos )
    let set_list set_item s pos list =
      let nitems = List.length list in
      let pos = set_int s pos nitems in
      let rec iter set_item s pos list =
        match list with
          [] -> pos
        | item :: items ->
            let pos = set_item s pos item in
            iter set_item s pos items
      in
      iter set_item s pos list
    let get_list get_item s pos =
      let nitems, pos = get_int s pos in
      let rec iter get_item nitems s pos =
        if nitems = 0 then
          [], pos
        else
          let item, pos = get_item s pos in
          let items, pos = iter get_item (nitems-1) s pos in
          item :: items, pos
      in
      iter get_item nitems s pos

    (* We return two strings, because Irmin does not store the content
       of a file as it is hashed, but without its length (nodes are
       stored as they are hashed, however). So there is a string for
       hash computation, and a string for storage *)

    (* irmin version *)
    let marshal_content_to_hash ?to_store content =
      match content with
      | Content content ->
          let len = Bigstring.length content in
          let s = Bytes.create (len_int len + len) in
          let pos = set_int s 0 len in
          Bigstring.blit_to_bytes content 0 s pos len;
          Bytes.to_string s

      | Tree tree ->
          match to_store with
          | Some to_store -> to_store
          | None ->
              (* TODO: Irmin uses a different strategy to order files, as
                 files with chars with ASCII code less than '/' should be
                 reordered. In Tezos, we assume it never happens. *)
              let len = ref 0 in
              let nitems = ref 0 in
              let list = ref [] in
              StringMap.iter (fun key inode ->
                  let hash =
                    match inode with
                    | Inode inode -> begin
                        match inode.inode_hash with
                        | None -> assert false
                        | Some inode_hash ->
                            list := ( inode.inode_kind,
                                      key,
                                      inode_hash ) :: !list ;
                            inode_hash
                      end
                    | DirectContent content ->
                        let to_hash =
                          let len = String.length content in
                          let s = Bytes.create (len_int len + len) in
                          let pos = set_int s 0 len in
                          Bytes.blit_string content 0 s pos len;
                          Bytes.to_string s
                        in
                        let hash = Hash.hash_string
                            key
                            [to_hash] in
                        let hash = Hash.to_string hash in
                        list := ( Kind_content,
                                  key,
                                  hash ) :: !list ;
                        hash
                  in
                  incr nitems ;
                  len := !len +
                         1 +
                         len_string key +
                         len_string hash
                ) tree.tree_files ;
              let len = !len + len_int !nitems in
              let s = Bytes.create len in
              let pos = set_int s 0 !nitems in
              let len = ref pos in
              List.iter (fun (kind, key, hash) ->
                  let pos = !len in
                  let pos =
                    set_bool s pos (match kind with
                        | Kind_tree -> false
                        | Kind_content -> true) in
                  let pos = set_string s pos key in
                  let pos = set_string s pos hash in
                  len := pos
                ) (List.rev !list) ;
              assert ( !len = Bytes.length s );
              Bytes.to_string s

    (* irmin version *)
    let marshal_content_to_store ?to_hash content =
      match content with
      | Content content ->
          incr save_irmin_contents;
          Bigstring.to_string content
      | Tree _ ->
          incr save_irmin_trees;
          match to_hash with
          | None -> marshal_content_to_hash content
          | Some s -> s

    (* irmin version *)
    let unmarshal_inode_content ~inode_kind inode_storage ( s : Bigstring.t ) =
      match inode_kind with
      | Kind_content ->
          incr load_irmin_contents;
          Content s

      | Kind_tree ->
          incr load_irmin_trees;
          let nitems, pos = get_int s 0 in
          let len = ref pos in
          let files = ref StringMap.empty in
          for _i = 1 to nitems do
            let pos = !len in
            let is_file, pos = get_bool s pos in
            let key, pos = get_string s pos in
            let inode_hash, pos = get_string s pos in
            len := pos;
            let inode_kind = if is_file then Kind_content else Kind_tree in
            files := StringMap.add key
                ( Inode
                    (new_inode
                       ~inode_storage:(IrminStorage inode_storage)
                       ~inode_hash ~inode_kind () ))
                !files
          done;
          let tree_files = !files in
          Tree {
            tree_files ;
            tree_diff = NoDiff ;
          }

(*
Commit:
  node_hash[string]
  parents[string list]
  date[8 bytes]
  author[string]
  message[string]
*)

    (* irmin version *)
    let marshal_commit_to_store ?to_hash raw =
      match to_hash with
      | Some to_hash -> to_hash
      | None ->
          let info = raw.info in
          let len =
            len_string raw.tree_hash +
            len_list len_string raw.parents +
            len_uint64 info.date +
            len_string info.author +
            len_string info.message
          in
          let s = Bytes.create len in
          let pos = 0 in
          let pos = set_string s pos raw.tree_hash in
          let pos = set_list set_string s pos raw.parents in
          let pos = set_uint64 s pos info.date in
          let pos = set_string s pos info.author in
          let pos = set_string s pos info.message in
          assert ( len = pos );
          Bytes.to_string s

    let marshal_commit_to_hash ?to_store raw =
      marshal_commit_to_store ?to_hash:to_store raw

    (* irmin version *)
    let unmarshal_raw_commit s =
      let pos = 0 in
      let tree_hash, pos = get_string s pos in
      let parents, pos = get_list get_string s pos in
      let date, pos = BIGSTRING.get_uint64 s pos in
      let author, pos = get_string s pos in
      let message, pos = get_string s pos in
      if pos <> Bigstring.length s then begin
        Printf.eprintf "%d <> %d\n%!" pos (Bigstring.length s);
        Printf.eprintf "commit = %S\n%!" (Bigstring.to_string s);
        exit 2
      end;
      let info = {
        date ; author ; message
      } in
      let tree_off = None in
      { tree_hash ; tree_off ; parents ; info }

  end (* IrminMarshaller *)

  module IrminStorage : Encoding = struct
    module S = IrminMarshaller

    (* irmin *)
    let get_inode storage inode =
      match inode.inode_hash with
      | None -> assert false
      | Some key ->
          let prefix = match inode.inode_kind with
            | Kind_tree -> S.node_prefix
            | Kind_content -> S.contents_prefix
          in
          let shortkey = S.key_of_key ~prefix key in
          match storage_get storage ~key:shortkey with
          | None -> raise Not_found
          | Some raw_content ->
              S.unmarshal_inode_content ~inode_kind:inode.inode_kind
                storage raw_content

    (* irmin *)
    let get_commit storage ~hash =
      let key = Hash.to_string hash in
      let prefix = S.commit_prefix in
      let shortkey = S.key_of_key ~prefix key in
      storage_check_update storage;
      match storage_get storage ~key:shortkey with
      | None -> raise Not_found
      | Some raw_commit ->
          S.unmarshal_raw_commit raw_commit

    (* irmin *)
    let hash_content_to_hash inode_content =
      match inode_content with
      | Tree { tree_diff = FakeTree hash ; _ } -> Hash.of_string hash
      | _ ->
          let content_to_hash = S.marshal_content_to_hash inode_content in
          Hash.hash_string "hash_content" [ content_to_hash ]

    let hash_content inode_content =
      Hash.to_string (hash_content_to_hash inode_content)

    (* irmin *)
    let put_inode ~storage ?key inode =
      match inode.inode_content with
      | None -> assert false
      | Some inode_content ->
          let to_store = S.marshal_content_to_store inode_content in
          let key =
            match key with
            | Some key -> key
            | None ->
                let to_hash = S.marshal_content_to_hash ~to_store inode_content in
                let hash = Hash.hash_string "put_content" [ to_hash ] in
                Hash.to_string hash
          in
          begin
            let prefix = match inode.inode_kind with
              | Kind_tree  -> S.node_prefix
              | Kind_content -> S.contents_prefix
            in
            let shortkey = S.key_of_key ~prefix key in
            storage_put storage ~key:shortkey to_store;
          end;
          begin
            match inode.inode_hash with
            | None -> inode.inode_hash <- Some key
            | Some hash -> assert (key = hash)
          end

    (* irmin *)
    let hash_commit raw =
      let raw_commit = S.marshal_commit_to_hash raw in
      Hash.hash_string "put_commit" [ raw_commit ]

    (* irmin *)
    let put_commit ~storage ?hash raw =
      let to_store = S.marshal_commit_to_store raw in
      let hash = match hash with
        | Some hash -> hash
        | None ->
            let to_hash = S.marshal_commit_to_hash ~to_store raw in
            Hash.hash_string "put_commit" [ to_hash ]
      in
      let key = Hash.to_string hash in
      let prefix = S.commit_prefix in
      let key = S.key_of_key ~prefix key in
      storage_put storage ~key to_store;
      hash

    let remove_commit ~storage ~hash =
      let key = Hash.to_string hash in
      let prefix = S.commit_prefix in
      let shortkey = S.key_of_key ~prefix key in
      storage_remove storage ~key:shortkey

  end (* IrminStorage *)


  module OptimizedMarshaller : StorageMarshaller = struct

    let contents_prefix = "c"
    let node_prefix = "n"
    let commit_prefix = "C"

    let key_size = int_var_or "IRONMIN_KEY_SIZE" 16

    let key_of_key ~prefix key =
      let key = String.sub key 0 key_size in
      Printf.sprintf "%s/%s" prefix key

    open Uintvar

    let len_int v = len_uint64 (Int64.of_int v)
    let len_string s =
      let len = String.length s in
      len_int len + len
    let len_list len_item list =
      let rec iter len_item len nitems list =
        match list with
          [] -> len + len_int nitems
        | item :: items ->
            let len = len + len_item item in
            iter len_item len (nitems+1) items
      in
      iter len_item 0 0 list

    let set_int s pos v = BYTES.set_uint64 s pos (Int64.of_int v)
    let get_int s pos =
      let i, pos = BIGSTRING.get_uint64 s pos in
      Int64.to_int i, pos
    let set_stringN s pos str len =
      Bytes.blit_string str 0 s pos len;
      pos+len
    let set_string s pos str =
      let len = String.length str in
      let pos = set_int s pos len in
      set_stringN s pos str len
    let get_stringN s pos len =
      let s = Bigstring.sub_string s pos len in
      ( s, pos + len )
    let get_string s pos =
      let len, pos = get_int s pos in
      get_stringN s pos len
    let _get_bigstring s pos =
      let len, pos = get_int s pos in
      let s = Bigstring.sub s pos len in
      let pos = pos + len in
      ( s, pos )
    let set_list set_item s pos list =
      let nitems = List.length list in
      let pos = set_int s pos nitems in
      let rec iter set_item s pos list =
        match list with
          [] -> pos
        | item :: items ->
            let pos = set_item s pos item in
            iter set_item s pos items
      in
      iter set_item s pos list
    let get_list get_item s pos =
      let nitems, pos = get_int s pos in
      let rec iter get_item nitems s pos =
        if nitems = 0 then
          [], pos
        else
          let item, pos = get_item s pos in
          let items, pos = iter get_item (nitems-1) s pos in
          item :: items, pos
      in
      iter get_item nitems s pos

    let info_of_inode inode =
      let hash =
        match inode.inode_hash with
        | Some inode_hash -> inode_hash
        | None -> assert false
      in
      let kind =
        match inode.inode_kind, inode.inode_off with
        | _, None -> assert false
        | Kind_tree, Some _ -> 0
        | Kind_content, Some _ -> 255
      in
      (kind, Some hash, inode.inode_off)

    let info_of_direct_content content =
      if content = "inited" then
        (1, None, None)
      else
        let rec iter content pos len =
          if pos < 3 && pos < len && content.[pos] = '\000' then
            iter content (pos+1) len
          else
            let content_len = len - pos in
            (* with content_len < 32 && pos < 7, kind
               maximal value is 250 *)
            let kind = 2 + pos + (content_len lsl 2) in
            let content =
              if pos <> 0 then
                String.sub content pos content_len
              else content
            in
            (kind, Some content, None)
        in
        let len = String.length content in
        iter content 0 len

    let info_of_file = function
      | DirectContent content ->
          info_of_direct_content content
      | Inode inode ->
          info_of_inode inode

    let info_len (_kind, content, off) =
      1 +
      (match off with
       | None -> 0
       | Some off -> len_int off) +
      (match content with
       | None -> 0
       | Some content -> String.length content)

    let set_info s pos (kind, content, off) =
      let pos = Uintvar.BYTES.set_uint8 s pos kind in
      let pos = match off with
        | None -> pos
        | Some off ->
            assert (content <> None);
            (* Printf.eprintf "set_int s[%d] %d\n%!" pos off; *)
            set_int s pos off
      in
      match content with
      | None -> pos
      | Some content ->
          let content_len = String.length content in
          Bytes.blit_string content 0 s pos content_len;
          pos + content_len

    (* formats:
       0: content
       1: tree
       2 : commit
       3 : fake tree
       20-119 : diff to Ironmin node
    *)

    let max_diff_size = 100
    let ironmin_diff =   20
    let ironmin_diff_end = ironmin_diff + max_diff_size - 1

    type info =
      | InfoAddKey of Key.t *
                      (int * string option * int option)
      | InfoRemoveKey of Key.t

    (* ironmin version *)
    let marshal_content_to_store ?to_hash:_ ( content : inode_content ) =
      match content with
      | Content content ->
          incr save_ironmin_contents;
          let len = Bigstring.length content in
          let b = Bytes.create (1 + len) in
          let _pos = BYTES.set_uint8 b 0 0 in
          Bigstring.blit_to_bytes content 0 b 1 len;
          Bytes.to_string b

      | Tree tree ->
          incr save_ironmin_trees;
          match tree.tree_diff with
          | Diff (subtree_inode, tree_ops) ->
              let len = match subtree_inode.inode_off with
                | None -> 1 + hash_len (* irmin *)
                | Some pos -> 1 + len_int pos (* ironmin *)
              in
              let len = ref len in
              let infos = ref [] in
              (* tree_files = StringMap.empty, it means we have only
                 loaded the inode, and not computed `tree_files`, so
                 we should not try to simplify the diff. *)
              let add_all_diffs = tree.tree_files = StringMap.empty in
              let removed_keys = ref StringMap.empty in
              List.iter (function
                  | AddKey (key, file) ->
                      let add_diff =
                        add_all_diffs ||
                        (* is it still in the tree *)
                        match StringMap.find key tree.tree_files with
                        | exception Not_found -> false
                        | file2 -> file == file2
                      in
                      if add_diff then
                        let (key, key_len) = Key.create key in
                        let info = info_of_file file in
                        let op_len = 1 + key_len + info_len info in
                        len := !len + op_len;
                        infos := (InfoAddKey (key, info)) :: !infos
                  | RemoveKey key ->
                      if
                        add_all_diffs ||
                        ( not ( StringMap.mem key tree.tree_files ) &&
                          not ( StringMap.mem key !removed_keys ) )
                      then begin
                        removed_keys := StringMap.add key () !removed_keys;
                        let (key, key_len) = Key.create key in
                        let op_len = 1 + key_len in
                        len := !len + op_len;
                        infos := (InfoRemoveKey key) :: !infos;
                      end
                      (* else
                         Printf.printf "!%!"
                         else
                         Printf.printf "?%!" *)
                ) tree_ops;
              let infos = List.rev !infos in
              let len = !len in
              let b = Bytes.create len in
              let pos =
                match subtree_inode.inode_off with
                | None -> assert false
                | Some off ->
                    let pos = BYTES.set_uint8 b 0
                        (ironmin_diff + List.length infos - 1) in
                    set_int b pos off
              in
              let pos = ref pos in
              List.iter (function
                  | InfoAddKey (key, info) ->
                      pos :=
                        (let pos = !pos in
                         let pos = BYTES.set_uint8 b pos 0 in (* AddKey *)
                         let pos = Key.set b pos key in
                         (* TODO: add an op here *)
                         let pos = set_info b pos info in
                         pos)
                  | InfoRemoveKey key ->
                      pos :=
                        (let pos = !pos in
                         let pos = BYTES.set_uint8 b pos 1 in (* RemoveKey *)
                         let pos = Key.set b pos key in
                         pos)
                ) infos;
              let pos = !pos in
              assert (pos = len);
              Bytes.to_string b

          | NoDiff ->
              let len = ref 0 in
              let nitems = ref 0 in
              let list = ref [] in (* key, kind, content *)
              StringMap.iter (fun key file ->
                  incr nitems ;
                  let (key, key_len) = Key.create key in
                  let info = info_of_file file in
                  (* assert (String.length hash = 32); *)
                  len := !len +
                         key_len +
                         info_len info;
                  list := (key, info) :: !list
                ) tree.tree_files ;
              let len = 1 + !len + len_int !nitems in
              (*            Printf.eprintf "%d items\n%!" !nitems;*)
              let s = Bytes.create len in
              let pos = BYTES.set_uint8 s 0 1 in (* format 1 *)
              let pos = set_int s pos !nitems in
              let len = ref pos in
              List.iter (fun ( key, info ) ->
                  let pos = !len in
                  let pos = Key.set s pos key in
                  let pos = set_info s pos info in
                  len := pos
                ) (List.rev !list) ;
              assert ( !len = Bytes.length s );
              let s = Bytes.to_string s in
              (* Printf.eprintf "Marshaled [%d items]: %S\n%!" !nitems s; *)
              s
          | FakeTree hash ->
              let len = 1 + hash_len in
              let s = Bytes.create len in
              let pos = BYTES.set_uint8 s 0 3 in (* format 3 *)
              let content_len = String.length hash in
              assert ( content_len = hash_len );
              Bytes.blit_string hash 0 s pos content_len;
              Bytes.to_string s


    (* Use IrminMarshaler, (1) it should be faster and (2) we cannot
       use our own direct format, because it contains the positions in
       the mapfile, which is dependent on the local history. *)
    let marshal_content_to_hash ?to_store:_ content =
      IrminMarshaller.marshal_content_to_hash content

    let get_file ~inode_storage s pos =
      let kind, pos = Uintvar.BIGSTRING.get_uint8 s pos in
      match kind with
      | 0 | 255 ->
          let inode_off, pos = get_int s pos in
          let inode_hash = Bigstring.sub_string s pos hash_len in
          let pos = pos + hash_len in
          let inode_kind =
            if kind = 0 then Kind_tree else Kind_content
          in
          Inode (
            new_inode
              ~inode_storage:(IronminStorage inode_storage)
              ~inode_off ~inode_hash ~inode_kind ()
          ), pos

      (* all other ones are DirectContent *)
      | 1 ->
          DirectContent "inited", pos
      | _ ->
          let kind = kind - 2 in
          let zeroes = kind land 3 in
          let content_len = kind lsr 2 in
          let content =
            String.make zeroes '\000' ^
            Bigstring.sub_string s pos content_len in
          let pos = pos + content_len in
          DirectContent content, pos

    (* ironmin version *)
    let unmarshal_inode_content ~debug ~inode_kind inode_storage s =
      match inode_kind with
      | Kind_content ->
          incr load_ironmin_contents;
          let len = Bigstring.length s in
          Content (Bigstring.sub s 1 (len-1))

      | Kind_tree ->
          incr load_ironmin_trees;
          let format, pos = Uintvar.BIGSTRING.get_uint8 s 0 in
          match format with

          | 1 ->
              let nitems, pos = get_int s pos in
              let len = ref pos in
              let files = ref StringMap.empty in
              for _i = 1 to nitems do
                let pos = !len in
                if debug then Printf.eprintf "pos: %d\n%!" pos;
                let key, pos = Key.BIGSTRING.get s pos in
                if debug then Printf.eprintf "key: %S\n%!" key;
                let file, pos = get_file ~inode_storage s pos in
                len := pos;
                files := StringMap.add key file !files
              done;
              let tree_files = !files in
              Tree {
                tree_files ;
                tree_diff = NoDiff ;
              }
          | 2 -> assert false (* Commit *)
          | 3 -> (* Fake tree *)
              let hash, _pos = get_stringN s pos hash_len in
              Tree { tree_files = StringMap.empty ;
                     tree_diff = FakeTree hash }

          | _ -> (* format 10..19 & 20..29 *)
              let subtree_inode, nops, pos =
                if format >= ironmin_diff && format <= ironmin_diff_end then
                  let inode_off, pos = get_int s pos in
                  let inode_kind = Kind_tree in
                  let subtree_inode =
                    new_inode ~inode_off ~inode_kind
                      ~inode_storage:(IronminStorage inode_storage)
                      ()
                  in
                  subtree_inode, format-ironmin_diff+1, pos
                else
                  assert false
              in
              let tree_ops = ref [] in
              let pos = ref pos in
              for _i = 1 to nops do
                pos := (
                  let pos = !pos in
                  let opcode, pos = Uintvar.BIGSTRING.get_uint8 s pos in
                  let tree_op, pos =
                    match opcode with
                    | 0 ->
                        let key, pos = Key.BIGSTRING.get s pos in
                        let file, pos = get_file ~inode_storage s pos in
                        AddKey( key, file ), pos
                    | 1 ->
                        let key, pos = Key.BIGSTRING.get s pos in
                        RemoveKey key, pos
                    | _ -> assert false
                  in
                  tree_ops := tree_op :: !tree_ops;
                  pos)
              done;
              let pos = !pos in
              let tree_ops = List.rev !tree_ops in
              assert (pos = Bigstring.length s);
              Tree {
                tree_files = StringMap.empty ;
                tree_diff = Diff (subtree_inode, tree_ops);
              }


    let unmarshal_inode_content ~inode_kind inode_storage s =
      try
        unmarshal_inode_content ~debug:false ~inode_kind inode_storage s
      with _exn ->
        Printf.eprintf "unmarshal_inode_content: %S\n%!"
          (Bigstring.to_string s);
        unmarshal_inode_content ~debug:true ~inode_kind inode_storage s

    (* ironmin version *)
    let marshal_commit_to_store ?to_hash:_ raw =
      let info = raw.info in
      let tree_off = match raw.tree_off with
        | None -> assert false
        | Some tree_off -> tree_off
      in
      let len =
        1 +
        len_string raw.tree_hash +
        len_int tree_off +
        len_list len_string raw.parents +
        len_uint64 info.date +
        (*        len_string info.author + *)
        len_string info.message
      in
      let s = Bytes.create len in
      let pos = 0 in
      let pos = BYTES.set_uint8 s pos 2 in
      let pos = set_string s pos raw.tree_hash in
      let pos = set_int s pos tree_off in
      let pos = set_list set_string s pos raw.parents in
      let pos = BYTES.set_uint64 s pos info.date in
      (*      let pos = set_string s pos info.author in *)
      let pos = set_string s pos info.message in
      assert ( len = pos );
      Bytes.to_string s

    let marshal_commit_to_hash ?to_store:_ raw =
      IrminMarshaller.marshal_commit_to_hash raw

    (* ironmin version *)
    let unmarshal_raw_commit s =
      let pos = 0 in
      let opcode, pos = Uintvar.BIGSTRING.get_uint8 s pos in
      assert (opcode = 2);
      let tree_hash, pos = get_string s pos in
      let tree_off, pos = get_int s pos in
      let parents, pos = get_list get_string s pos in
      let date, pos = BIGSTRING.get_uint64 s pos in
      (*      let author, pos = get_string s pos in *)
      let author = "Tezos" in
      let message, pos = get_string s pos in
      assert ( pos = Bigstring.length s );
      let info = {
        date ; author ; message
      } in
      let tree_off = Some tree_off in
      { tree_hash ; tree_off ; parents ; info }

  end (* OptimizedMarshaller *)

  module OptimizedStorage : Encoding = struct
    module S =  OptimizedMarshaller

    (* optimized *)
    let get_inode storage inode =
      match inode.inode_off with
      | None -> raise Not_found
      | Some off ->
          let raw_content = Storage.at storage ~off in
          (* Printf.eprintf "loaded at %d\n%!" off; *)
          S.unmarshal_inode_content ~inode_kind:inode.inode_kind
            storage raw_content

    (* optimized *)
    let hash_content_to_hash inode_content =
      let to_hash = S.marshal_content_to_hash inode_content in
      Hash.hash_string "hash_content" [ to_hash ]

    let hash_content inode_content =
      Hash.to_string (hash_content_to_hash inode_content)

    let off_of_bigstring s =
      let off,_ = Uintvar.BIGSTRING.get_uint64 s 0 in
      Int64.to_int off

    let string_of_off off =
      let offL = Int64.of_int off in
      let len = Uintvar.len_uint64 offL in
      let b = Bytes.create len in
      let _pos = Uintvar.BYTES.set_uint64 b 0 offL in
      Bytes.to_string b

    (* optimized *)
    let put_inode ~storage ?key inode =
      assert (inode.inode_off = None);
      match inode.inode_content with
      | None -> assert false
      | Some inode_content ->
          let key =
            match key with
            | Some key -> key
            | None ->
                let to_hash = S.marshal_content_to_hash inode_content in
                let hash = Hash.hash_string "put_content" [ to_hash ] in
                Hash.to_string hash
          in
          let to_store = S.marshal_content_to_store inode_content in
          let off = Storage.store storage to_store in
          inode.inode_off <- Some off;
          begin
            match inode.inode_hash with
            | None -> inode.inode_hash <- Some key
            | Some hash -> assert (key = hash)
          end


    (* optimized *)
    let hash_commit raw =
      let raw_commit = S.marshal_commit_to_hash raw in
      Hash.hash_string "put_commit" [ raw_commit ]


    (* optimized *)
    let get_commit storage ~hash =
      let key = Hash.to_string hash in
      let prefix = S.commit_prefix in
      let shortkey = S.key_of_key ~prefix key in
      storage_check_update storage;
      match storage_get storage ~key:shortkey with
      | None -> raise Not_found
      | Some s ->
          let off = off_of_bigstring s in
          let raw_commit = Storage.at storage ~off in
          S.unmarshal_raw_commit raw_commit

    (* optimized *)
    let put_commit ~storage ?hash raw =
      let to_store = S.marshal_commit_to_store raw in
      let hash = match hash with
        | None ->
            let to_hash = S.marshal_commit_to_hash ~to_store raw in
            Hash.hash_string "put_commit" [ to_hash ]
        | Some hash -> hash
      in
      let off = Storage.store storage to_store in
      let key = Hash.to_string hash in
      let prefix = S.commit_prefix in
      let shortkey = S.key_of_key ~prefix key in
      if storage_mem storage ~key:shortkey then
        Printf.eprintf "put_commit: already in storage\n%!";
      storage_put storage ~key:shortkey (string_of_off off);
      hash

    let remove_commit ~storage ~hash =
      let key = Hash.to_string hash in
      let prefix = S.commit_prefix in
      let shortkey = S.key_of_key ~prefix key in
      storage_remove storage ~key:shortkey

  end (* OptimizedStorage *)

  let rec get_inode inode =
    match inode.inode_storage with
    | None -> assert false
    | Some (IrminStorage storage) ->
        begin
          match IrminStorage.get_inode storage inode with
          | exception Not_found ->
              assert ( inode.inode_kind = Kind_tree );
              begin
                match inode.inode_hash with
                | None -> assert false
                | Some inode_hash ->
                    Tree { tree_files = StringMap.empty ;
                           tree_diff = FakeTree inode_hash }
              end
          | v -> v
        end
    | Some (IronminStorage storage) ->
        let inode_content =
          OptimizedStorage.get_inode storage inode
        in
        match inode_content with
        | Content _ -> inode_content
        | Tree {
            tree_diff =
              (Diff ( subtree_inode, tree_ops )) as tree_diff ;
            _
          } ->
            let tree_files =
              get_files_from_diff subtree_inode tree_ops
            in
            Tree { tree_files ; tree_diff }
        | Tree { tree_diff = NoDiff | FakeTree _ ; _ } ->
            inode_content

  and get_files_from_diff subtree_inode tree_ops =
    (*                Printf.printf ".%!"; *)
    match get_inode_content subtree_inode with
    | Content _ -> assert false
    | Tree { tree_files ; _ } ->
        let tree_files = ref tree_files in
        List.iter (function
            | AddKey (key, file) ->
                tree_files := StringMap.add key file !tree_files
            | RemoveKey key ->
                tree_files := StringMap.remove key !tree_files
          ) tree_ops;
        let tree_files = !tree_files in
        (* is it possible for the subtree_inode to be used again ?
           We hope no, so let's clear its content to save memory.
        *)
        subtree_inode.inode_content <- None;
        tree_files

  and get_inode_content inode =
    match inode.inode_content with
    | Some content -> content
    | None ->
        let content = get_inode inode in
        inode.inode_content <- Some content ;
        content

  let get_commit storage ~hash =
    match !storage_config with
    | Irmin -> assert false
    | IrminCompat ->
        IrminStorage.get_commit storage ~hash, IrminStorage storage
    | Optimized ->
        try
          OptimizedStorage.get_commit storage ~hash, IronminStorage storage
        with Not_found ->
          IrminStorage.get_commit storage ~hash, IrminStorage storage

  let hash_content content =
    match !storage_config with
    | Irmin -> assert false
    | IrminCompat
    | Optimized ->
        IrminStorage.hash_content content

  let hash_content_to_hash content =
    match !storage_config with
    | Irmin -> assert false
    | IrminCompat
    | Optimized ->
        IrminStorage.hash_content_to_hash content

  let put_inode ~storage inode =
    match !storage_config with
    | Irmin -> assert false
    | IrminCompat ->
        IrminStorage.put_inode ~storage inode;
        IrminStorage storage
    | Optimized ->
        let key =
          match inode.inode_hash with
          | Some hash -> hash
          | None ->
              match inode.inode_content with
              | None -> assert false
              | Some content ->
                  IrminStorage.hash_content content
        in
        OptimizedStorage.put_inode ~storage ~key inode;
        IronminStorage storage

  let hash_commit raw =
    match !storage_config with
    | Irmin -> assert false
    | IrminCompat
    | Optimized ->
        IrminStorage.hash_commit raw

  let put_commit ~storage raw =
    match !storage_config with
    | Irmin -> assert false
    | IrminCompat ->
        IrminStorage.put_commit ~storage raw
    | Optimized ->
        let hash = IrminStorage.hash_commit raw in
        OptimizedStorage.put_commit ~storage ~hash raw


  let rec put_inode_maybe_copy repo inode =
    match inode.inode_storage with
    (* In Optimized mode, we copy all the reachable nodes in
       IrminStorage to OptimizedStorage for every new commit to be
       stored in OptimizedStorage. That way, we do not care about
       links between the two storages.

       This operation can cause a heavy load in memory when loading
       the full context in memory before saving it. So, we try to prevent
       it by unloaded any content that was not already loaded.

       TODO: cleanup the node, replacing short-content by DirectContent.
       We cannot do it easily, as we unload the content so we are
       not able to check if it is a short-content.

       If we are converting Irmin to Ironmin, we might find that some
       trees are fake (because of pruned contexts). TODO: handle such cases!
    *)
    | Some (IrminStorage _storage) when !storage_config = Optimized ->
        let empty_content = inode.inode_content = None in
        let (_ : inode_content) =  get_inode_content inode in
        inode.inode_storage <- None;
        put_inode_maybe_copy repo inode;
        if empty_content then inode.inode_content <- None
    | Some (IronminStorage _) ->
        () (* OK *)
    | Some (IrminStorage _) ->
        () (* OK *)
    | None ->

        match inode.inode_content with
        | None -> assert false
        | Some inode_content ->

            begin (* recurse first on memory-mapped trees *)
              match inode_content with
              | Tree tree  ->
                  put_tree_in_storage repo tree
              |  Content _ -> ()
            end;
            let storage = Lazy.force repo.storage in
            let inode_storage = put_inode ~storage inode in
            inode.inode_storage <- Some inode_storage

  and put_tree_in_storage repo tree =
    StringMap.iter (fun _key inode ->
        match inode with
        | DirectContent _ -> ()
        | Inode inode -> put_inode_maybe_copy repo inode)
      tree.tree_files

  let rec hash_inode_in_mem inode =
    match inode.inode_hash with
    | Some _ -> ()
    | None ->

        match inode.inode_content with
        | None -> assert false
        | Some inode_content ->

            begin (* recurse first on memory-mapped trees *)
              match inode_content with
              | Tree tree  ->
                  hash_tree_inodes_in_mem tree
              |  Content _ -> ()
            end;

            let key = hash_content inode_content in
            inode.inode_hash <- Some key

  and hash_tree_inodes_in_mem tree =
    StringMap.iter (fun _key inode ->
        match inode with
        | DirectContent _ -> ()
        | Inode inode ->
            hash_inode_in_mem inode)
      tree.tree_files

  (* if repo is specified, the tree content is stored in the repository.
     otherwise, the computation is done in memory *)
  let hash_tree ?repo tree =
    match tree.tree_diff with
    | FakeTree hash -> Hash.of_string hash
    | _ ->
        begin
          match repo with
          | None -> hash_tree_inodes_in_mem tree
          | Some repo -> put_tree_in_storage repo tree
        end;
        let inode_content = Tree tree in
        let to_hash =
          OptimizedMarshaller.marshal_content_to_hash inode_content in
        Hash.hash_string "hash_tree" [ to_hash ]

  module Tree = struct
    type hash =
      [ `Contents of Hash.t * metadata | `Node of Hash.t ]

    let hash _repo tree =
      Lwt.return ( `Node ( hash_tree tree ) )

    let empty () = {
      tree_files = StringMap.empty ;
      tree_diff = NoDiff ;
    }

    let fake hash = {
      tree_files = StringMap.empty ;
      tree_diff = FakeTree hash ;
    }

    let mem tree v =
      if debug_api then
        Printf.eprintf "Ironmin: mem %s\n%!"
          (String.escaped (String.concat " . " v));
      let rec iter tree path =
        match path with
        | [] -> false
        | [ v ] ->
            begin
              match StringMap.find v tree.tree_files with
              | exception Not_found -> false
              | DirectContent _ -> true
              | Inode inode ->
                  match get_inode_content inode with
                  | Tree _tree ->  false
                  | Content _ -> true
            end
        | v :: path ->
            begin
              match StringMap.find v tree.tree_files with
              | exception Not_found -> false
              | DirectContent _ -> false
              | Inode inode ->
                  match get_inode_content inode with
                  | Tree tree ->  iter tree path
                  | Content _ -> false
            end
      in
      Lwt.return (iter tree v)

    let mem_tree tree v =
      if debug_api then
        Printf.eprintf "Ironmin: mem_tree %s\n%!"
          (String.escaped (String.concat " . " v));
      let rec iter tree path =
        match path with
        | [] -> true
        | v :: path ->
            begin
              match StringMap.find v tree.tree_files with
              | exception Not_found -> false
              | DirectContent _ -> false
              | Inode inode ->
                  match get_inode_content inode with
                  | Tree tree ->  iter tree path
                  | Content _ -> false
            end
      in
      Lwt.return (iter tree v)

    let find tree v =
      if debug_api then
        Printf.eprintf "Ironmin: find %s\n%!"
          (String.escaped (String.concat " . " v));

      let rec iter tree path =
        match path with
        | [] -> None
        | [ v ] ->
            begin
              match StringMap.find v tree.tree_files with
              | exception Not_found -> None
              | DirectContent content ->
                  Some (Bigstring.of_string content)
              | Inode inode ->
                  match get_inode_content inode with
                  | Tree _tree ->  None
                  | Content content -> Some content
            end
        | v :: path ->
            begin
              match StringMap.find v tree.tree_files with
              | exception Not_found -> None
              | DirectContent _ -> None
              | Inode inode ->
                  match get_inode_content inode with
                  | Tree tree ->  iter tree path
                  | Content _ -> None
            end
      in
      let tree = try
          iter tree v
        with exn ->
          let bt = Printexc.get_raw_backtrace () in
          Printf.eprintf "exception in find %s: %s\n%s\n%!"
            (String.concat ":" v)
            (Printexc.to_string exn)
            (Printexc.raw_backtrace_to_string bt);
          raise exn
      in
      Lwt.return tree

    let find_tree_no_lwt tree v =
      if debug_api then
        Printf.eprintf "Ironmin: find_tree %s\n%!"
          (String.escaped (String.concat " . " v));
      let rec iter tree path =
        match path with
        | [] -> Some tree
        | v :: path ->
            begin
              match StringMap.find v tree.tree_files with
              | exception Not_found -> None
              | DirectContent _ -> None
              | Inode inode ->
                  match get_inode_content inode with
                  | Tree tree ->  iter tree path
                  | Content _ -> None
            end
      in
      iter tree v

    let find_tree tree v =
      Lwt.return ( find_tree_no_lwt tree v )

    let tree_diff ?use_diff tree_files tree_op =
      let tree_diff =
        match use_diff with
        | None -> NoDiff
        | Some inode ->
            match inode.inode_content with
            | None -> NoDiff
            | Some (Content _) -> assert false
            | Some (Tree tree) ->
                match inode.inode_storage, tree.tree_diff with
                (* Never make a diff on an Irmin-stored node *)
                | Some (IrminStorage _), _ -> NoDiff
                (* Never make a diff on a non-stored node *)
                | None, ( NoDiff | FakeTree _ ) -> NoDiff
                | _ ->
                    let size = StringMap.cardinal tree_files in
                    if size = 1 then
                      NoDiff
                    else
                      match tree.tree_diff with
                      | NoDiff | FakeTree _ ->
                          if inode.inode_storage == None then
                            NoDiff
                          else
                            Diff (inode, [tree_op])
                      | Diff (inode, tree_ops) ->
                          assert (inode.inode_storage != None);
                          let nops = 1 + List.length tree_ops in
                          if (nops < 10 && nops < size) ||
                             nops < 100 && nops < size/2 then begin
                            (*                        Printf.eprintf "%d%!" nops; *)
                            Diff (inode, tree_ops @ [tree_op])
                          end else begin
                            (* if nops > 100 then Printf.printf "_%!"; *)
                            NoDiff
                          end
      in
      { tree_files ; tree_diff }

    let tree_diff ?use_diff tree_files tree_op =
      if max_diffs = 0 then
        tree_diff ?use_diff tree_files tree_op
      else
        let tree_diff =
          match use_diff with
          | None -> NoDiff
          | Some inode ->
              match inode.inode_content with
              | None -> NoDiff
              | Some (Content _) -> assert false
              | Some (Tree tree) ->
                  match inode.inode_storage with
                  | Some _ when Random.int max_diffs > 0 ->
                      Diff (inode, [tree_op])
                  | Some _
                  | None ->
                      match tree.tree_diff with
                      | NoDiff | FakeTree _ -> NoDiff
                      | Diff (inode, tree_ops) ->
                          let size = StringMap.cardinal tree_files in
                          if size = 1 then
                            NoDiff
                          else
                            let nops = 1 + List.length tree_ops in
                            assert (inode.inode_storage != None);
                            if (nops < 10 && nops < size) ||
                               nops < 100 && nops < size/2 then begin
                              (* Printf.eprintf "%d%!" nops; *)
                              Diff (inode, tree_ops @ [tree_op])
                            end else begin
                              (* if nops > 100 then Printf.printf "_%!"; *)
                              NoDiff
                            end
        in
        { tree_files ; tree_diff }

    let add_no_lwt tree path content =
      if debug_api then
        Printf.eprintf "Ironmin: add %s\n%!"
          (String.escaped (String.concat " . " path));

      let rec iter tree ?diff:use_diff path content =
        match path with
        | [] -> assert false
        | [ v ] ->
            let file =
              match !storage_config with
              | Irmin -> assert false
              | IrminCompat ->
                  Inode ( new_inode ~inode_content:(Content content) () )
              | Optimized ->
                  if use_direct_content &&
                     Bigstring.length content <= direct_content_max_size
                  then begin
                    let content = Bigstring.to_string content in
                    (* Printf.eprintf "add direct content %S\n%!"
                       content; *)
                    DirectContent content
                  end
                  else
                    Inode ( new_inode ~inode_content:(Content content) () )
            in
            let tree_files = StringMap.add v file tree.tree_files in
            tree_diff ?use_diff tree_files (AddKey (v, file))
        | v :: path ->
            let subtree, diff =
              match StringMap.find v tree.tree_files with
              | exception Not_found -> empty (), None
              | DirectContent _ -> empty (), None
              | Inode inode ->
                  match get_inode_content inode with
                  | Tree tree -> tree, Some inode
                  | Content _ -> empty (), None
            in
            let subtree = iter subtree ?diff path content in
            let inode_content = Tree subtree in
            let file = Inode (new_inode ~inode_content ()) in
            let tree_files = StringMap.add v file tree.tree_files in
            tree_diff ?use_diff tree_files (AddKey (v, file))
      in
      iter tree path content

    let add tree path ?metadata:_ content =
      Lwt.return ( add_no_lwt tree path content )


    let add_tree_no_lwt tree path tree_to_add =
      if debug_api then
        Printf.eprintf "Ironmin: add_tree %s\n%!"
          (String.escaped (String.concat " . " path));

      let rec iter tree ?diff:use_diff path tree_to_add =
        match path with
        | [] -> assert false
        | [ v ] ->
            let inode_content = Tree tree_to_add in
            let file = Inode ( new_inode ~inode_content () ) in
            let tree_files = StringMap.add v file tree.tree_files in
            tree_diff ?use_diff tree_files (AddKey (v, file))
        | v :: path ->
            let subtree, diff =
              match StringMap.find v tree.tree_files with
              | exception Not_found -> empty (), None
              | DirectContent _ -> empty (), None
              | Inode inode ->
                  match get_inode_content inode with
                  | Tree tree -> tree, Some inode
                  | Content _ -> empty (), None
            in
            let subtree = iter ?diff subtree path tree_to_add in
            let inode_content = Tree subtree in
            let file = Inode ( new_inode ~inode_content () ) in
            let tree_files = StringMap.add v file tree.tree_files in
            tree_diff ?use_diff tree_files (AddKey (v, file))
      in
      iter tree path tree_to_add

    let add_tree tree path tree_to_add =
      Lwt.return ( add_tree_no_lwt tree path tree_to_add )

    let remove tree path =
      if debug_api then
        Printf.eprintf "Ironmin: remove %s\n%!"
          (String.escaped (String.concat " . " path));

      let rec iter tree ?diff:use_diff path =
        match path with
        | [] -> assert false
        | [ v ] ->
            let tree_files = StringMap.remove v tree.tree_files in
            if tree_files = StringMap.empty then
              None
            else
              Some ( tree_diff ?use_diff tree_files (RemoveKey v) )
        | v :: path ->
            match StringMap.find v tree.tree_files with
            | exception Not_found -> Some tree
            | DirectContent _ -> assert false
            | Inode inode ->
                match get_inode_content inode with
                | Content _ -> assert false
                | Tree subtree ->
                    match iter subtree ~diff:inode path with
                    | None -> (* subtree is now empty ! *)
                        let tree_files = StringMap.remove v tree.tree_files in
                        if tree_files = StringMap.empty then
                          None
                        else
                          Some ( tree_diff ?use_diff tree_files (RemoveKey v) )
                    | Some subtree ->
                        let inode_content = Tree subtree in
                        let file = Inode ( new_inode ~inode_content ()) in
                        let tree_files = StringMap.add v file tree.tree_files in
                        Some (tree_diff ?use_diff tree_files (AddKey (v,file)) )
      in
      let tree = match iter tree path with
        | None -> empty ()
        | Some tree -> tree
      in
      Lwt.return tree

    let list_no_lwt tree path =
      if debug_api then
        Printf.eprintf "Ironmin: list %s\n%!"
          (String.escaped (String.concat " . " path));

      match find_tree_no_lwt tree path with
      | None -> []
      | Some tree ->
          let list = ref [] in
          StringMap.iter (fun file inode ->
              let kind =
                match inode with
                | DirectContent _ -> `Contents
                | Inode inode ->
                    match get_inode_content inode with
                    | Content _ -> `Contents
                    | Tree _ -> `Node
              in
              list := ( file , kind ) :: !list
            ) tree.tree_files;
          List.rev !list

    let list tree path =
      Lwt.return ( list_no_lwt tree path )


  end

  module Commit = struct

    type t = commit

    let info t = t.raw.info

    let tree c =
      if debug_api then
        Printf.eprintf "Ironmin: Commit.tree %s\n%!"
          (String.escaped (Hash.to_string c.hash));

      match get_inode_content c.tree with
      | Tree tree -> Lwt.return tree
      | _ -> assert false

    let of_hash_no_lwt repo hash =
      if debug_api then
        Printf.eprintf "Ironmin: Commit.of_hash %s\n%!"
          (String.escaped (Hash.to_string hash));

      let commit =
        match
          if no_cache || !storage_config = IrminCompat then raise Not_found;
          FifoCache.find repo.cache hash with
        | commit ->
            assert (commit.hash = hash);
            assert (commit.tree.inode_hash <> None);
            Some commit
        | exception Not_found ->
            let storage = Lazy.force repo.storage in
            match get_commit storage ~hash with
            | exception Not_found -> None
            | raw, inode_storage ->
                let inode = new_inode
                    ~inode_storage
                    ~inode_kind:Kind_tree
                    ~inode_hash:raw.tree_hash
                    ?inode_off:raw.tree_off
                    ()
                in
                let commit = {
                  repo ;
                  raw ;
                  tree = inode ;
                  hash
                } in
                if not ( no_cache || !storage_config = IrminCompat ) then
                  FifoCache.add repo.cache hash commit;
                Some commit
      in
      commit

    let of_hash repo hash =
      Lwt.return (of_hash_no_lwt repo hash)

    let hash commit = commit.hash


    let v0 repo ~info ~parents_hashes tree =
      if debug_api then
        Printf.eprintf "Ironmin: Commit.v\n%!";

      let parents = List.sort compare (
          List.map Hash.to_string parents_hashes
        ) in
      let inode_content = Tree tree in
      let inode = new_inode ~inode_content () in
      put_inode_maybe_copy repo inode ;
      let tree_hash = match inode.inode_hash with
          None -> assert false
        | Some hash -> hash in
      let tree_off = inode.inode_off in
      let raw = { tree_hash ; tree_off ; parents ; info } in
      let storage = Lazy.force repo.storage in
      let hash = put_commit ~storage raw
      in
      Storage.commit storage ;
      let commit = {
        repo ;
        raw ;
        tree = inode ;
        hash
      } in
      if not ( no_cache || !storage_config = IrminCompat ) then begin
        if FifoCache.mem repo.cache hash then
          Printf.eprintf "Cache: new commit already in cache!\n%!"
        else
          FifoCache.add repo.cache hash commit;
      end;
      Lwt.return commit

    let v repo ~info ~parents tree =
      let parents_hashes =
        List.map (fun c -> c.hash) parents
      in
      v0 repo ~info ~parents_hashes tree

    let to_hash0 ~info ~parents_hashes tree =
      if debug_api then
        Printf.eprintf "Ironmin: Commit.to_hash\n%!";

      let inode_content = Tree tree in
      let inode = new_inode ~inode_content () in
      hash_inode_in_mem inode ;
      let tree_hash = match inode.inode_hash with
          None -> assert false
        | Some hash -> hash in
      (*      Printf.eprintf "tree_hash %S\n%!" tree_hash; *)
      let parents =
        List.sort compare (
          List.map Hash.to_string parents_hashes
        )
      in
      let tree_off = inode.inode_off in
      hash_commit { tree_hash ; tree_off ; parents ; info }

    let to_hash ~info ~parents tree =
      let parents_hashes = List.map (fun c -> c.hash) parents in
      to_hash0 ~info ~parents_hashes tree
  end

  module Branch = struct

    let set _repo _branch_name _commit =
      (* Useless ?
         repo.branches <- StringMap.add branch_name commit repo.branches;*)
      Lwt.return_unit

    let remove _repo _branch_name =
      (* Useless ?
         repo.branches <- StringMap.remove branch_name repo.branches; *)
      Lwt.return_unit

    let master = "master"

  end

  module Info = struct

    type t = info
    let v ~date ~author message =
      (*
      Printf.eprintf "author = %S\n%!" author;
      Printf.eprintf "message = %S\n%!" message;
author = "Tezos"
message = "lvl 1197, fit 1197, prio 0, 1 ops"
author = "Tezos"
message = "lvl 1198, fit 1198, prio 0, 2 ops"
author = "Tezos"
message = "lvl 1198, fit 1198, prio 0, 2 ops"
author = "Tezos"
message = "lvl 1199, fit 1199, prio 0, 1 ops"

*)
      { date ; author ; message }
    let date t = t.date
    let author t = t.author
    let message t = t.message
  end

  module Repo = struct

    type t = repo

    let v (config : config) =
      let storage = lazy ( S.create_storage config ) in
      let repo = {
        storage ;
        cache = FifoCache.create
            ~clear_every:1500 (* clear once a day *)
            10;
        (* Useless ?        branches = StringMap.empty ; *)
      } in

      Lwt.return repo

  end

  module TOPLEVEL = struct

    let storage_dir repo =
      let storage = Lazy.force repo.storage in
      Storage.dir storage

    let clear_stats () =
      hits := 0;
      load_irmin_contents := 0;
      load_irmin_trees := 0;
      save_irmin_contents := 0;
      save_irmin_trees := 0;
      load_ironmin_contents := 0;
      load_ironmin_trees := 0;
      save_ironmin_contents := 0;
      save_ironmin_trees := 0;
      ()

    let print_stats repo =
      Printf.eprintf "Stats:\n%!";
      if !hits > 0 then Printf.eprintf "Cache hits: %d\n%!" !hits;
      List.iter (fun (name, r) ->
          if !r > 0 then
            Printf.eprintf "%s: %d\n%!" name !r;
        )
        [
          "load_irmin_contents", load_irmin_contents ;
          "load_irmin_trees",    load_irmin_trees ;
          "save_irmin_contents", save_irmin_contents ;
          "save_irmin_trees",    save_irmin_trees ;
          "load_ironmin_contents", load_ironmin_contents ;
          "load_ironmin_trees",    load_ironmin_trees ;
          "save_ironmin_contents", save_ironmin_contents ;
          "save_ironmin_trees",    save_ironmin_trees ;
        ];
      let storage = Lazy.force repo.storage in
      Storage.stats storage

    let hex_of_string hash =
      let `Hex s = Hex.of_string hash in
      s

    let rec remove_rec dir =
      let st = Unix.stat dir in
      match st.Unix.st_kind with
      | Unix.S_DIR ->
          let files = Sys.readdir dir in
          Array.iter (fun file ->
              remove_rec (Filename.concat dir file)) files;
          Unix.rmdir dir
      | _ ->
          Sys.remove dir


    (* During a GC, nobody should try to use the former database. For example,
       bakers should be disconnected before starting the GC, and restarted once
       the GC is completed. For now. TODO: add a way for bakers to know
       that the database is out-of-date.
       Let's implement several versions:
       1) Everything in memory (requires minimal disk space, but big memory)
       2) Modify the current Mapfile to store the offsets
             (requires minimal disk space, minimal memory, but corrupts the
              current Mapfile)
       3) Use an intermediate Mapfile to store the offsets
             (use more Disk Space)
       8 bytes/entry => 7*8 bits
         4_398_046_511_104 (4 TB)
       7 bytes/entry => 35 bits
         34_359_738_368 (35 GB)
    *)


    (* Using this does not improve memory consumption.

       module Hashtbl = struct
       let create size = FlatHashtbl.create size
       let mem t x = FlatHashtbl.mem t (x+1)
       let add t x v =
        FlatHashtbl.add t (x+1)
          (match v with
           | None -> 0
           | Some v -> 1 + v)
       let replace t x v =
        FlatHashtbl.replace t (x+1)
          (match v with
           | None -> 0
           | Some v -> 1 + v)
       let find t x =
        match FlatHashtbl.find t (x+1) with
        | 0 -> None
        | v -> Some (v-1)
       end

       end_of_cycle: 49 c935d5f1db79bb16a07198224a45d3129e05e54b259f668e4b99abcd4b27a4b7
       Garbage collecting cycle 41
       Starting a GC, keeping e9b242e3cf7027343c31cad06f3b2a349f6804a586a3442c03292adb6d449b96 as the oldest commit
       Everything after 853114197 is live
       Scan done in 20.63s
       1: "listings"
       2: "listings_size"
       1: "listings"
       2: "listings_size"
       Scanned 14888553 objects done in 30.45s
       live_commits:   32769
       live_contents: 136587
       live_trees:   8318815
                8 500 000 objets
       8 m x 16 x 3 = 8 x 48 = 500 m Not so much.

       Currently, we move the full context into memory. TODO: cut it.
    *)


    (* GC full memory : Hashtbl to store content *)

    let gc repo ~genesis ~current =
      match !storage_config with
      | IrminCompat
      | Irmin ->
          Printf.eprintf "No garbage collection in Irmin mode\n%!";
          false
      | Optimized ->
          Printf.eprintf "Starting a GC, keeping %s as the oldest commit\n%!"
            (hex_of_string (Hash.to_string current));
          let t0 = Unix.gettimeofday () in
          match Commit.of_hash_no_lwt repo current with
          | None ->
              Printf.eprintf
                "No garbage collection of already garbage collected context\n%!";
              false
          | Some commit ->
              let tree = commit.tree in
              match tree.inode_storage with
              | None -> assert false
              | Some (IrminStorage _) -> false
              (* do nothing, we cannot GC Irmin, it will disappear by itself *)
              | Some (IronminStorage storage) ->
                  (* Good ! *)
                  match tree.inode_off with
                  | None -> assert false
                  | Some live_off ->
                      Printf.eprintf
                        "Everything after %d is live\n%!" live_off;

                      let live_offsets = Hashtbl.create 100_000 in

                      let mark_inode inode =
                        match inode.inode_off with
                        | None ->
                            Printf.eprintf
                              "Error: reaching node outside of Irontez!\n%!";
                            assert false
                        | Some inode_off ->
                            if not (Hashtbl.mem live_offsets inode_off)
                            then
                              Hashtbl.add live_offsets inode_off None
                      in
                      let rec iter_inode inode =
                        mark_inode inode;
                        match get_inode_content inode with
                        | Content _ ->
                            inode.inode_content <- None
                        | Tree { tree_files ; tree_diff } ->
                            inode.inode_content <- None;
                            StringMap.iter (fun _ file ->
                                iter_file file
                              ) tree_files;
                            match tree_diff with
                            | NoDiff | FakeTree _ -> ()
                            | Diff (subtree_inode, tree_ops) ->
                                iter_inode subtree_inode;
                                List.iter (fun diff ->
                                    match diff with
                                    | AddKey (_, file) -> iter_file file
                                    | RemoveKey _ -> ()
                                  ) tree_ops
                      and iter_file file =
                        match file with
                        | DirectContent _ -> ()
                        | Inode inode -> iter_inode inode
                      in

                      let ( genesis_raw_commit, genesis_tree ) =
                        match Commit.of_hash_no_lwt repo genesis with
                        | None ->
                            Printf.eprintf
                              "Error in GC: could not find the genesis block\n%!";
                            assert false
                        | Some { tree ; raw ; _ } ->
                            match raw.tree_off with
                            | Some _ -> raw, tree (* genesis already in Irontez *)
                            | None ->
                                Printf.eprintf
                                  "Copying genesis context to Irontez\n%!";
                                put_inode_maybe_copy repo tree ;
                                { raw with tree_off = tree.inode_off }, tree
                      in
                      Printf.eprintf "Scanning genesis...\n%!";
                      iter_inode genesis_tree;

                      Printf.eprintf "Scanning oldest kept...\n%!";
                      iter_inode tree;

                      let t1 = Unix.gettimeofday () in
                      Printf.eprintf "Scan done in %.2fs\n%!" (t1 -. t0);

                      let old_storage_dir = Storage.dir storage in
                      let new_storage_dir = old_storage_dir ^ ".gc" in
                      if Sys.file_exists new_storage_dir then
                        remove_rec new_storage_dir;

                      let new_storage = Storage.clone storage new_storage_dir in

                      (* TODO: use another LMDB database !!! *)
                      let n = ref 0 in
                      let live_commits = ref 0 in
                      let live_contents = ref 0 in
                      let live_trees = ref 0 in
                      let update_inode inode =
                        match inode.inode_off with
                        | None -> assert false
                        | Some inode_off ->
                            let new_inode_off =
                              Hashtbl.find live_offsets inode_off
                            in
                            assert (new_inode_off != None);
                            inode.inode_off <- new_inode_off
                      in
                      let update_file file =
                        match file with
                        | DirectContent _ -> ()
                        | Inode inode -> update_inode inode
                      in
                      let update_commit raw_commit =
                        let raw_commit =
                          let tree_off = match raw_commit.tree_off with
                            | None -> assert false
                            | Some tree_off ->
                                let new_tree_off =
                                  Hashtbl.find live_offsets tree_off
                                in
                                assert (new_tree_off != None);
                                new_tree_off
                          in
                          { raw_commit with tree_off }
                        in
                        let (_hash : Hash.t) =
                          put_commit ~storage:new_storage raw_commit
                        in
                        ()
                      in
                      Storage.iter storage (fun ~off:old_off s ->
                          incr n;
                          if old_off >= live_off ||
                             Hashtbl.mem live_offsets old_off
                          then
                            let opcode, _pos =
                              Uintvar.BIGSTRING.get_uint8 s 0 in
                            match opcode with
                            | 2 -> (* commit *)
                                incr live_commits;
                                let raw_commit =
                                  OptimizedMarshaller.unmarshal_raw_commit s in
                                update_commit raw_commit
                            | 0 ->
                                incr live_contents;
                                let off = Storage.store new_storage
                                    (Bigstring.to_string s) in
                                Hashtbl.replace live_offsets old_off (Some off)
                            | _ ->
                                incr live_trees;
                                match
                                  OptimizedMarshaller.unmarshal_inode_content
                                    ~inode_kind:Kind_tree
                                    storage
                                    s
                                with
                                | Content _ -> assert false
                                | Tree tree ->
                                    StringMap.iter (fun _ file ->
                                        update_file file
                                      ) tree.tree_files;
                                    begin
                                      match tree.tree_diff with
                                      | NoDiff | FakeTree _ -> ()
                                      | Diff (subtree_inode, tree_ops) ->
                                          update_inode subtree_inode;
                                          List.iter (fun diff ->
                                              match diff with
                                              | AddKey (_, file) -> update_file file
                                              | RemoveKey _ -> ()
                                            ) tree_ops
                                    end;
                                    let s =
                                      OptimizedMarshaller.marshal_content_to_store
                                        ( Tree tree )
                                    in
                                    let off = Storage.store new_storage s in
                                    Hashtbl.replace live_offsets old_off (Some off)
                        );
                      update_commit genesis_raw_commit;
                      Storage.commit new_storage ;

                      let t2 = Unix.gettimeofday () in
                      Printf.eprintf "Scanned %d objects done in %.2fs\n%!" !n (t2 -. t1);
                      Printf.eprintf "  live_commits: %d\n%!" !live_commits;
                      Printf.eprintf "  live_contents: %d\n%!" !live_contents;
                      Printf.eprintf "  live_trees: %d\n%!" !live_trees;
                      Storage.close storage;
                      Storage.close new_storage;

                      (* All this code is Ironmin_lmdb specific, it should be moved
                         there *)
                      let backup_storage_dir = old_storage_dir ^ ".backup" in
                      if Sys.file_exists backup_storage_dir then
                        remove_rec backup_storage_dir;
                      Sys.rename old_storage_dir backup_storage_dir;
                      Sys.rename new_storage_dir old_storage_dir;

                      let moved_storage = Storage.clone storage old_storage_dir in
                      repo.storage <- lazy moved_storage;
                      FifoCache.clear repo.cache;
                      true

    (* Revert storage from Ironmin to Irmin. It must be the last time
       you use IronTez, otherwise it will switch the storage back to
       Ironmin at the next restart. *)
    let revert repo =
      let t0 = Unix.gettimeofday () in
      let n = ref 0 in
      let live_commits = ref 0 in
      let live_contents = ref 0 in
      let live_trees = ref 0 in
      (* From now on, use IrminCompat *)
      storage_config := IrminCompat;
      FifoCache.clear repo.cache;
      let storage = Lazy.force repo.storage in
      Storage.iter storage (fun ~off:_ s ->
          incr n;
          let opcode, _pos = Uintvar.BIGSTRING.get_uint8 s 0 in
          match opcode with
          | 2 -> (* commit *)
              incr live_commits;
              let raw_commit =
                OptimizedMarshaller.unmarshal_raw_commit s in
              let (hash : Hash.t) =
                put_commit ~storage raw_commit
              in
              (* remove the commit hash from LMDB in IronTez format *)
              OptimizedStorage.remove_commit ~storage ~hash;
              Storage.commit storage ;
              ()
          | 3 -> (* fake tree: no need to copy *) ()
          | 0 ->
              incr live_contents;
              let inode_content =
                OptimizedMarshaller.unmarshal_inode_content
                  ~inode_kind:Kind_content
                  storage
                  s
              in
              let inode = new_inode
                  ~inode_kind:Kind_content
                  ~inode_content
                  ()
              in
              let (_ : inode_storage) = put_inode ~storage inode in
              ()
          | _ ->
              incr live_trees;
              let inode_content =
                OptimizedMarshaller.unmarshal_inode_content
                  ~inode_kind:Kind_tree
                  storage
                  s
              in
              let tree_files = match inode_content with
                | Content _ -> assert false
                | Tree { tree_diff = ( NoDiff | FakeTree _ ) ;
                         tree_files } -> tree_files
                | Tree { tree_diff = Diff (subtree_inode, tree_ops) ; _ } ->
                    get_files_from_diff subtree_inode tree_ops
              in
              let tree_files = StringMap.map (fun file ->
                  match file with
                  | Inode _ -> file
                  | DirectContent c ->
                      let inode_content = Content (Bigstring.of_string c) in
                      let inode = new_inode
                          ~inode_kind:Kind_content
                          ~inode_content
                          ()
                      in
                      let (_ : inode_storage) = put_inode ~storage inode in
                      Inode inode
                ) tree_files in
              let inode_content =
                Tree { tree_files ; tree_diff = NoDiff }
              in
              let inode = new_inode
                  ~inode_kind:Kind_tree
                  ~inode_content
                  ()
              in
              let (_ : inode_storage) = put_inode ~storage inode in
              ()
        );
      (* Clear the mapfile and its size *)
      Storage.revert storage;
      Storage.commit storage ;
      let t1 = Unix.gettimeofday () in
      Printf.eprintf "Scanned %d objects done in %.2fs\n%!" !n (t1 -. t0);
      Printf.eprintf "  live_commits: %d\n%!" !live_commits;
      Printf.eprintf "  live_contents: %d\n%!" !live_contents;
      Printf.eprintf "  live_trees: %d\n%!" !live_trees;
      ()

    type tree_hash =
      [ `Contents of Hash.t * metadata | `Node of Hash.t ]

    module MEMCACHE = struct

      type memcache = {
        table : ( string, inode_content) Hashtbl.t; (* hash -> content *)
        revtable : (Bigstring.t, unit) Hashtbl.t; (* content -> hash *)
      }
      let new_memcache () =
        {
          table = Hashtbl.create 5_000_000;
          revtable = Hashtbl.create 100_000;
        }

      let add_mbytes memcache _repo blob =
        if not (Hashtbl.mem memcache.revtable blob) then begin
          let content = Content blob in
          let h = hash_content content in
          Hashtbl.add memcache.table h content ;
          Hashtbl.add memcache.revtable blob ();
        end;
        Lwt.return_unit

      let add_hash memcache _repo tree path file =
        let hash = match file with
            `Contents (hash, _) -> hash
          | `Node hash -> hash
        in
        let hash = Hash.to_string hash in
        let inode_content = try
            Hashtbl.find memcache.table hash
          with Not_found ->
            failwith "Ironmin: hash not found in the memcache";
        in
        let tree =
          match file, inode_content with
          | `Contents _, Content content ->
              Tree.add_no_lwt tree path content
          | `Node _, Tree sub_tree ->
              Tree.add_tree_no_lwt tree path sub_tree
          | _ ->
              failwith "Inconsistent use of hashes"
        in
        Lwt.return_some tree

      let add_tree memcache repo tree =
        let h = Hash.to_string ( hash_tree ~repo tree ) in
        match Hashtbl.find memcache.table h with
        | Tree _ -> Lwt.return_unit
        | Content _ -> assert false
        | exception Not_found ->
            Hashtbl.add memcache.table h (Tree tree) ;
            Lwt.return_unit


      let set_context memcache repo ~info ~parents:parents_hashes ~node  =
        match Hashtbl.find memcache.table ( Hash.to_string node ) with
        | exception Not_found ->
            failwith "context not found in memcache"
        | Tree tree ->
            Commit.v0 repo ~info ~parents_hashes tree >>= fun commit ->
            Lwt.return ( Commit.hash commit )
        | Content _ ->
            failwith "Content as context in memcache"

    end

    (* We do very bad things here: when we load a snapshot, we do not
       compute the context for every block in the past. Instead, we
       create a fake commit, with a partial tree, where the "data"
       sub-tree is actually empty. Idem for the parents of the
       commits, they are not real commits. TODO: We must take care of
       this in the GC.  *)

    let fake_commit ~repo ~info ~hash ~inode =
      let raw_commit = {
        tree_hash = Hash.to_string hash ;
        tree_off = None ;
        parents = [] ;
        info ;
      } in
      {
        repo ;
        tree = inode ;
        raw = raw_commit ;
        hash ;
      }

    let compute_context_hash repo tree ~info ~parents_hashes data_hash =
      let data_tree = Tree.fake ( Hash.to_string data_hash ) in
      let new_tree = Tree.add_tree_no_lwt tree ["data"] data_tree in
      match StringMap.find "data" new_tree.tree_files with
      | DirectContent _ -> assert false
      | Inode inode ->
          let commit_hash = Commit.to_hash0 ~info ~parents_hashes new_tree in
          ( commit_hash ,
            List.map (fun hash ->
                fake_commit ~repo ~info ~hash ~inode
              ) parents_hashes ,
            data_tree )

    let fold_tree_path ?(progress = (fun _ -> ())) _repo tree f =
      (* Noting the visited hashes *)
      let visited_inodes = Hashtbl.create 1_000_000 in
      let visited_contents = Hashtbl.create 1_000_000 in
      let cpt = ref 0 in
      let rec fold_tree_path tree =
        let keys = StringMap.bindings tree.tree_files in
        Lwt_list.map_s
          begin fun (name, file) ->
            match file with
            | DirectContent s ->
                begin
                  match Hashtbl.find visited_contents s with
                  | tree_hash -> Lwt.return ( name, tree_hash )
                  | exception Not_found ->
                      progress !cpt;
                      incr cpt ;
                      let content = Bigstring.of_string s in
                      f ( `Data content ) >>= fun () ->
                      let hash = hash_content_to_hash ( Content content ) in
                      let tree_hash = `Contents ( hash , () ) in
                      Hashtbl.add visited_contents s tree_hash;
                      Lwt.return ( name, ( tree_hash : tree_hash ) )
                end
            | Inode inode ->
                let hash = match inode.inode_hash with
                  | None -> assert false
                  | Some hash -> hash
                in
                match Hashtbl.find visited_inodes hash with
                | tree_hash -> Lwt.return ( name, tree_hash )
                | exception Not_found ->
                    progress !cpt;
                    incr cpt ;
                    begin
                      let hash = Hash.of_string hash in
                      match get_inode_content inode with
                      | Content s ->
                          f ( `Data s ) >>= fun () ->
                          Lwt.return ( `Contents ( hash , () ) )
                      | Tree tree ->
                          fold_tree_path tree >>= fun () ->
                          Lwt.return ( `Node hash )
                    end >>= fun tree_hash ->
                    Hashtbl.add visited_inodes hash tree_hash;
                    Lwt.return ( name, ( tree_hash : tree_hash ) )
          end keys >>= fun sub_keys ->
        f (`Node sub_keys)
      in
      fold_tree_path tree

    let context_parents _repo commit =
      let parents = commit.raw.parents in
      let parents = List.sort compare parents in
      let parents = List.map Hash.of_string parents in
      Lwt.return parents

  end

end

module MakeConcrete = Make
