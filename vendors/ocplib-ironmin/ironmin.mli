(************************************************************************)
(*                                Ironmin                               *)
(*                                                                      *)
(*  Copyright 2019 Origin-Labs                                          *)
(*  Copyright 2018-2019 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  Ironmin is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

type storage_config =
  | Irmin       (* Keep using Irmin *)
  | IrminCompat (* Replace Irmin only *)
  | Optimized   (* Tezos compatibility with optimized storage, even
                   with formet database *)

val storage_config : storage_config ref

module type IronminKeyValue = sig
  type t

  (* key x value *)
  val mem : t -> key:string -> bool
  val get : t -> key:string -> Bigstring.t option
  val check_update : t -> unit
  val put : t -> key:string -> string -> unit
  val remove : t -> key:string -> unit
  val commit : t -> unit
  val abort : t -> unit
  val fold : t -> (key:string -> Bigstring.t -> 'a -> 'a) -> 'a -> 'a
  val length : t -> int
  val stats : t -> unit

  val close : t -> unit

end

module type Key = sig
  type t
  val create : string -> t * int
  val set : Bytes.t -> int -> t -> int
  module BIGSTRING : sig
    val get : Bigstring.t -> int -> string * int
  end
end

module type IronminStorage = sig

  (* key x value *)
  include IronminKeyValue

  (* append-only *)
  val at : t -> off:int -> Bigstring.t
  val store : t -> string -> int
  val iter : t -> ?off:int -> (off:int -> Bigstring.t -> unit) -> unit
  val revert : t -> unit

  val dir : t -> string
  val clone : t -> string -> t
end

module Make(S : sig

    module Hash : sig
      type t
      val hash_string : string list -> t
      val to_string : t -> string
      val of_string : string -> t
    end

    module Storage : IronminStorage
    module Key : Key

    type config
    val create_storage : config -> Storage.t

  end ) : sig

  type metadata = unit
  type config = S.config
  type tree

  val init : unit -> unit

  module Info : sig
    type t
    val v : date:int64 -> author:string -> string -> t
    val date : t -> int64
    val author : t -> string
    val message : t -> string
  end

  module Repo : sig
    type t
    val v : config -> t Lwt.t
  end

  module Tree : sig
    type hash =
      [ `Contents of S.Hash.t * metadata | `Node of S.Hash.t ]
    val hash : Repo.t -> tree -> hash Lwt.t
    val empty : unit -> tree
    val mem : tree -> string list -> bool Lwt.t
    val mem_tree : tree -> string list -> bool Lwt.t
    val find : tree -> string list -> Bigstring.t option Lwt.t
    val remove : tree -> string list -> tree Lwt.t
    val add : tree -> string list -> ?metadata:metadata -> Bigstring.t -> tree Lwt.t
    val find_tree : tree -> string list -> tree option Lwt.t
    val add_tree : tree -> string list -> tree -> tree Lwt.t
    val list : tree -> string list -> ( string * [`Contents | `Node] ) list Lwt.t
  end

  module Commit : sig
    type t
    val tree : t -> tree Lwt.t
    val of_hash : Repo.t -> S.Hash.t -> t option Lwt.t
    val v : Repo.t -> info:Info.t -> parents:t list -> tree -> t Lwt.t
    val hash : t -> S.Hash.t
    val to_hash : info:Info.t -> parents:t list -> tree -> S.Hash.t
    val info : t -> Info.t
  end
  module Branch : sig
    val set : Repo.t -> string -> Commit.t -> unit Lwt.t
    val remove : Repo.t -> string -> unit Lwt.t
    val master : string
  end

  module TOPLEVEL : sig
    val gc : Repo.t -> genesis:S.Hash.t -> current:S.Hash.t -> bool
    val revert : Repo.t -> unit

    val clear_stats : unit -> unit
    val print_stats : Repo.t -> unit
    val storage_dir : Repo.t -> string

    type tree_hash =
      [ `Contents of S.Hash.t * metadata | `Node of S.Hash.t ]

    module MEMCACHE : sig

      type memcache
      val new_memcache : unit -> memcache

      (* Register some content by hash in the memcache *)
      val add_mbytes :
        memcache -> Repo.t -> Bigstring.t -> unit Lwt.t
      (* Add some sub_tree/content by hash inside a tree, without saving the
         tree in the memcache *)
      val add_hash :
        memcache ->
        Repo.t ->
        tree ->
        string list -> tree_hash ->
        tree option Lwt.t
      (* Add a tree in the memcache using its name *)
      val add_tree :
        memcache ->
        Repo.t ->
        tree ->
        unit Lwt.t
      (* Commit everything and return the hash of the commit *)
      val set_context :
        memcache ->
        Repo.t ->
        info:Info.t ->
        parents:S.Hash.t list -> node:S.Hash.t -> S.Hash.t Lwt.t
    end

    val compute_context_hash :
      Repo.t ->
      tree ->
      info:Info.t ->
      parents_hashes:S.Hash.t list ->
      S.Hash.t ->
      S.Hash.t * Commit.t list * tree

    val fold_tree_path :
      ?progress:(int -> unit) ->
      Repo.t ->
      tree ->
      ([> `Data of Bigstring.t
       | `Node of (string * tree_hash) list ] ->
       unit Lwt.t) ->
      unit Lwt.t

    val context_parents :
      Repo.t -> Commit.t -> S.Hash.t list Lwt.t


  end
end


(*  Concrete version    *)

val use_direct_content : bool

val if_var_exists : string -> bool
val int_var_or : string -> int -> int

module StringMap : Map.S with type key = string

module MakeConcrete(S : sig

    module Hash : sig
      type t
      val hash_string : string list -> t
      val to_string : t -> string
      val of_string : string -> t
    end

    module Storage : IronminStorage
    module Key : Key

    type config
    val create_storage : config -> Storage.t

  end ) : sig

  open S
  type metadata
  type config
  type info = { date : int64 ; author : string ; message : string }
  type inode_kind = Kind_content | Kind_tree

  type raw_commit = {
    tree_hash : string ;
    tree_off : int option ;
    parents : string list ;
    info : info;
  }


  type inode_storage =
    | IrminStorage of Storage.t
    | IronminStorage of Storage.t


  type commit = {
    repo : repo ;
    tree : inode ;
    raw : raw_commit ;
    hash : Hash.t ;
  }
  (* inode_kind is necessary to be able to store the content directly.
     It tells us how to unmarshal a block *)
  and inode = {
    mutable inode_storage : inode_storage option ;
    mutable inode_hash : string option ;
    mutable inode_off : int option ;
    inode_kind : inode_kind ;
    mutable inode_content : inode_content option ;
  }
  and inode_content =
    | Content of Bigstring.t
    | Tree of tree
  and file =
    | DirectContent of string
    | Inode of inode

  and tree_op =
    | AddKey of string * file
    | RemoveKey of string

  and tree_diff =
    | NoDiff
    | Diff of inode * tree_op list
    | FakeTree of string (* hash *)

  and tree = {
    tree_files : file StringMap.t ;
    tree_diff : tree_diff ;
  }
  and repo = {
    mutable storage : Storage.t Lazy.t;
    cache : (Hash.t, commit) FifoCache.t;
  }

  module Info : sig
    type t = info
    val v : date:int64 -> author:string -> string -> t
    val date : t -> int64
    val author : t -> string
    val message : t -> string
  end

  module Repo : sig
    type t = repo
    val v : config -> t Lwt.t
  end

  module Tree : sig
    type hash =
      [ `Contents of Hash.t * metadata | `Node of Hash.t ]
    val hash : Repo.t -> tree -> hash Lwt.t
    val empty : unit -> tree
    val mem : tree -> string list -> bool Lwt.t
    val mem_tree : tree -> string list -> bool Lwt.t
    val find : tree -> string list -> Bigstring.t option Lwt.t
    val remove : tree -> string list -> tree Lwt.t
    val add : tree -> string list -> ?metadata:metadata -> Bigstring.t -> tree Lwt.t
    val find_tree : tree -> string list -> tree option Lwt.t
    val add_tree : tree -> string list -> tree -> tree Lwt.t
    val list : tree -> string list -> ( string * [`Contents | `Node] ) list Lwt.t
  end
  module Commit : sig
    type t = commit
    val tree : t -> tree Lwt.t
    val of_hash : Repo.t -> S.Hash.t -> t option Lwt.t
    val v : Repo.t -> info:Info.t -> parents:t list -> tree -> t Lwt.t
    val hash : t -> S.Hash.t
    val to_hash : info:Info.t -> parents:t list -> tree -> S.Hash.t
    val info : t -> Info.t
  end
  module Branch : sig
    val set : Repo.t -> string -> Commit.t -> unit Lwt.t
    val remove : Repo.t -> string -> unit Lwt.t
    val master : string
  end



  module type StorageMarshaller = sig

    val contents_prefix : string
    val node_prefix : string
    val commit_prefix : string

    val key_of_key : prefix:string -> string -> string

    val unmarshal_raw_commit : EndianBigstring.bigstring -> raw_commit
    val unmarshal_inode_content :
      inode_kind:inode_kind -> Storage.t -> Bigstring.t -> inode_content

    val marshal_commit_to_hash : ?to_store:string -> raw_commit -> string
    val marshal_commit_to_store : ?to_hash:string -> raw_commit -> string

    val marshal_content_to_hash : ?to_store:string -> inode_content -> string
    val marshal_content_to_store : ?to_hash:string -> inode_content -> string

  end

  module IrminMarshaller : StorageMarshaller
  module OptimizedMarshaller : StorageMarshaller

  module type Encoding = sig

    (* raise Not_found if not present *)
    val get_inode : Storage.t -> inode -> inode_content
    val get_commit : Storage.t -> hash:Hash.t -> raw_commit

    val put_inode : storage:Storage.t -> ?key:string -> inode -> unit
    val put_commit : storage:Storage.t -> ?hash:Hash.t -> raw_commit -> Hash.t
    val remove_commit : storage:Storage.t -> hash:Hash.t -> unit

    val hash_content: inode_content -> string
    val hash_content_to_hash: inode_content -> Hash.t
    val hash_commit : raw_commit -> Hash.t

  end

  module IrminStorage : Encoding
  module OptimizedStorage : Encoding

  val new_inode :
    ?inode_off:int ->
    ?inode_storage:inode_storage ->
    ?inode_kind:inode_kind ->
    ?inode_content:inode_content ->
    ?inode_hash:string -> unit -> inode

  module TOPLEVEL : sig
    val clear_stats : unit -> unit
    val print_stats : repo -> unit
  end
end
