include Makefile-dune.definitions

PACKAGES:=$(patsubst %.opam,%,$(notdir $(shell find src vendors -name \*.opam -print)))

active_protocol_versions := $(shell cat active_protocol_versions)
active_protocol_directories := $(shell (sed '/^all$$/d' | tr -- - _) < active_protocol_versions)

current_opam_version := $(shell opam --version)
include scripts/version.sh

ifeq ($(filter ${opam_version}.%,${current_opam_version}),)
$(error Unexpected opam version (found: ${current_opam_version}, expected: ${opam_version}.*))
endif

current_ocaml_version := $(shell opam exec -- ocamlc -version)

all: generate_dune
ifneq (${current_ocaml_version},${ocaml_version})
	$(error Unexpected ocaml version (found: ${current_ocaml_version}, expected: ${ocaml_version}))
endif
	@dune build \
		${DUNE_TARGETS} \
		src/bin_node/main.exe \
		src/bin_validation/main_validator.exe \
		src/bin_client/main_client.exe \
		src/bin_client/main_admin.exe \
		src/bin_signer/main_signer.exe \
		src/bin_codec/codec.exe \
		src/lib_protocol_compiler/main_native.exe \
		$(foreach p, $(active_protocol_directories), src/proto_$(p)/bin_baker/main_baker_$(p).exe) \
		$(foreach p, $(active_protocol_directories), src/proto_$(p)/bin_endorser/main_endorser_$(p).exe) \
		$(foreach p, $(active_protocol_directories), src/proto_$(p)/bin_accuser/main_accuser_$(p).exe) \
		$(foreach p, $(active_protocol_directories), src/proto_$(p)/lib_parameters/sandbox-parameters.json)
	@cp -fp _build/default/src/bin_node/main.exe dune-node
	@cp -fp _build/default/src/bin_validation/main_validator.exe dune-validator
	@cp -fp _build/default/src/bin_client/main_client.exe dune-client
	@cp -fp _build/default/src/bin_client/main_admin.exe dune-admin-client
	@cp -fp _build/default/src/bin_signer/main_signer.exe dune-signer
	@cp -fp _build/default/src/bin_codec/codec.exe dune-codec
	@cp -fp _build/default/src/lib_protocol_compiler/main_native.exe dune-protocol-compiler
	@for p in $(active_protocol_directories) ; do \
	   cp -fp _build/default/src/proto_$$p/bin_baker/main_baker_$$p.exe dune-baker-`echo $$p | tr -- _ -` ; \
	   cp -fp _build/default/src/proto_$$p/bin_endorser/main_endorser_$$p.exe dune-endorser-`echo $$p | tr -- _ -` ; \
	   cp -fp _build/default/src/proto_$$p/bin_accuser/main_accuser_$$p.exe dune-accuser-`echo $$p | tr -- _ -` ; \
	   cp -fp _build/default/src/proto_$$p/lib_parameters/sandbox-parameters.json sandbox-parameters.json ; \
	 done
	@$(MAKE) dune-copy-targets

#PROTOCOLS := 000_Ps9mPmXa 001_PtCJ7pwo 002_PsYLVpVv 003_PsddFKi3 004_Pt24m4xi demo_noops
PROTOCOLS := 000_Ps9mPmXa 004_Pt24m4xi 005_PsBabyM1  demo_noops
DUNE_INCS=$(patsubst %,src/proto_%/lib_protocol/dune.inc, ${PROTOCOLS})

# all.pkg: generate_dune
# 	@dune build \
# 	    $(patsubst %.opam,%.install, $(shell find src vendors -name \*.opam -print))

# $(addsuffix .pkg,${PACKAGES}): %.pkg:
# 	@dune build \
# 	    $(patsubst %.opam,%.install, $(shell find src vendors -name $*.opam -print))

# $(addsuffix .test,${PACKAGES}): %.test:
# 	@dune build \
# 	    @$(patsubst %/$*.opam,%,$(shell find src vendors -name $*.opam))/runtest

DOC_PROTOCOL=PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS
DOC_PROTO=005-PsBabyM1

doc-html: all
	@dune build @doc
	@./dune-client -protocol $(DOC_PROTOCOL) man -verbosity 3 -format html | sed "s#${HOME}#\$$HOME#g" > docs/api/dune-client.html
	@./dune-admin-client man -verbosity 3 -format html | sed "s#${HOME}#\$$HOME#g" > docs/api/dune-admin-client.html
	@./dune-signer man -verbosity 3 -format html | sed "s#${HOME}#\$$HOME#g" > docs/api/dune-signer.html
	@./dune-baker-all man -verbosity 3 -format html | sed "s#${HOME}#\$$HOME#g" > docs/api/dune-baker.html
	@./dune-endorser-all man -verbosity 3 -format html | sed "s#${HOME}#\$$HOME#g" > docs/api/dune-endorser.html
	@./dune-accuser-all man -verbosity 3 -format html | sed "s#${HOME}#\$$HOME#g" > docs/api/dune-accuser.html
	@mkdir -p $$(pwd)/docs/_build/api/odoc
	@rm -rf $$(pwd)/docs/_build/api/odoc/*
	@cp -r $$(pwd)/_build/default/_doc/* $$(pwd)/docs/_build/api/odoc/
	@${MAKE} -C docs html
	@echo '@media (min-width: 745px) {.content {margin-left: 4ex}}' >> $$(pwd)/docs/_build/api/odoc/_html/odoc.css
	@sed -e 's/@media only screen and (max-width: 95ex) {/@media only screen and (max-width: 744px) {/' $$(pwd)/docs/_build/api/odoc/_html/odoc.css > $$(pwd)/docs/_build/api/odoc/_html/odoc.css2
	@mv $$(pwd)/docs/_build/api/odoc/_html/odoc.css2  $$(pwd)/docs/_build/api/odoc/_html/odoc.css

doc-html-and-linkcheck: doc-html
	@${MAKE} -C docs all

build-sandbox:
	@dune build src/bin_sandbox/main.exe
	@cp _build/default/src/bin_sandbox/main.exe dune-sandbox

build-test: build-sandbox
	@dune build @buildtest

test:
	@DUNE_TZ_COMPATIBILITY=y DUNE_CLIENT_UNSAFE_DISABLE_DISCLAIMER=y dune runtest
	@DUNE_TZ_COMPATIBILITY=y DUNE_CLIENT_UNSAFE_DISABLE_DISCLAIMER=y ./scripts/check_opam_test.sh
	@./scripts/check_docker_network_test.sh

test-lint:
	@dune build @runtest_lint
	make -C tests_python lint_all

fmt:
	@src/tooling/lint.sh format

build-deps:
	@./scripts/install_build_deps.sh

build-dev-deps:
	@./scripts/install_build_deps.sh --dev

docker-image:
	@./scripts/create_docker_image.sh

# install:
# 	@dune build @install
# 	@dune install

# uninstall:
# 	@dune uninstall

clean: dune-clean
	@-dune clean
	@-find . -name dune-project -delete
	@-rm -f \
		dune-node \
		dune-validator \
		dune-client \
		dune-signer \
		dune-admin-client \
		dune-codec \
		dune-protocol-compiler \
		dune-sandbox \
		ix \
	  $(foreach p, $(active_protocol_versions), dune-baker-$(p) dune-endorser-$(p) dune-accuser-$(p))
	@-${MAKE} -C docs clean
	@-rm -f docs/api/dune-{baker,endorser,accuser}.html docs/api/dune-{admin-,}client.html docs/api/dune-signer.html

include Makefile-dune.rules

.PHONY: all test build-deps docker-image clean
