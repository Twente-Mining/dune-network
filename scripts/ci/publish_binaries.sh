#! /bin/sh

set -e

ci_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
script_dir="$(dirname "$ci_dir")"
src_dir="$(dirname "$script_dir")"
cd "$src_dir"

. "$script_dir"/version.sh

build_image="${1:-registry.gitlab.com/dune-network/opam-repository:${opam_repository_tag}}"

cleanup () {
    set +e
    echo Cleaning up...
    rm -rf "$tmp_dir"
    if ! [ -z "$container" ]; then docker rm $container; fi
}
trap cleanup EXIT INT

container=$(docker create $build_image)
archive="${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}.zip"

echo
echo "### Building binary zip archive $archive ..."
echo
touch nothing
zip $archive nothing
zip -d $archive nothing
rm -f nothing
versioned_daemons="$(sed "s/^\(.*\)$/dune-baker-\1 dune-endorser-\1 dune-accuser-\1/g" "active_protocol_versions")"
for bin in dune-client dune-admin-client dune-node $versioned_daemons dune-signer ix; do
    docker cp -L $container:/home/tezos/dune-network/$bin ./
    zip -ur $archive ./$bin
done
zip -ur $archive active_protocol_versions
zip -ur $archive scripts/mainnet.sh

if ! [ -z "${CI_COMMIT_TAG}" ] ; then

    echo
    echo "### Publishing link on Gitlab"
    echo

    ## Try to create release if it does not exist
    curl --request POST \
             --header 'Content-Type:application/json' \
             --header "PRIVATE-TOKEN:$CI_BINARY_PRIVATE_TOKEN" \
             --data '{"name":"'$CI_COMMIT_TAG'","tag_name":"'$CI_COMMIT_TAG'","description":"Version '$CI_COMMIT_TAG'"}' \
             "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases"
    echo

    curl --request POST \
         --header "PRIVATE-TOKEN:${CI_BINARY_PRIVATE_TOKEN}" \
         --data name="Linux binaries for ${CI_PROJECT_NAME} ${CI_COMMIT_REF_NAME}" \
         --data url="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}/artifacts/$archive" \
         "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links"
    echo
fi
