#! /usr/bin/env bash

# Script to use `ix` within Docker

# Binaries are installed in /usr/local/bin. To use your own binaries,
# you should use the environment variable DUNIX_BINDIR
# (=$SRCDIR). Your binaries must be compiled using Docker (see
# scripts/image/README.txt for a tutorial). You also need to
# copy scripts/docker/entrypoint.* to your binaries directory.

set -e

if ! which docker > /dev/null 2>&1 ; then
    echo "Docker does not seem to be installed."
    exit 1
fi

if ! which docker-compose > /dev/null 2>&1 ; then
    echo "Docker-compose does not seem to be installed."
    exit 1
fi

docker_version="$(docker version -f "{{ .Server.Version }}")"
docker_major="$(echo "$docker_version" | cut -d . -f 1)"
docker_minor="$(echo "$docker_version" | cut -d . -f 2)"

docker_repo="registry.gitlab.com/dune-network/dune-network"

if ([ "$docker_major" -gt 1 ] ||
    ( [ "$docker_major" -eq 1 ] && [ "$docker_minor" -ge 13 ] )) ; then
    docker_1_13=true
else
    docker_1_13=false
fi

current_dir="$(pwd -P)"
src_dir="$(cd "$(dirname "$0")" && echo "$current_dir/")"
cd "$src_dir"

dunix_dir="${HOME}/.dunix"

update_compose_file() {

    update_active_protocol_version
    set_internal_rpc_port

    mkdir -p "${dunix_dir}"
    
    if [ "$#" -ge 2 ] && [ "$1" = "--rpc-port" ] ; then
        export_rpc="
      - \"$2:$internal_rpc_port\""
        shift 2
    fi

    if ! [ "$DUNIX_BINDIR" = "" ]; then
        extra_volume="
      - $DUNIX_BINDIR:/usr/local/bin"
    fi

    cat > "$docker_compose_yml" <<EOF
version: "2"

volumes:
  node_data:
  client_data:

services:

  dunix:
    image: $docker_image
    user: 1000:1000
    hostname: node
    environment:
      - HOME=${dunix_dir}
    command: dune server --port 9000 --root /
    ports:
      - 9000:9000
      - 8733:8733
      - 8734:8734
      - 8735:8735
      - 8736:8736
      - 8737:8737
      - 8738:8738
      - 8739:8739
      - 8740:8740
      - 8741:8741
    volumes:
      - ${dunix_dir}:${dunix_dir}${extra_volume}
      - client_data:/var/run/dune-network/client
      - node_data:/var/run/dune-network/node
    restart: on-failure

EOF

}

call_docker_compose() {
    echo docker-compose -f "$docker_compose_yml" -p "$docker_compose_name" "$@"
    docker-compose -f "$docker_compose_yml" -p "$docker_compose_name" "$@"
}

exec_docker() {
    if [ -t 0 ] && [ -t 1 ] && [ -t 2 ] && [ -z "$TESTNET_EMACS" ]; then
        local interactive_flags="-i"
    else
        local interactive_flags=""
    fi
    local node_container="$(container_name "$docker_node_container")"
    declare -a container_args=();
    tmpdir="/tmp"
    for arg in "$@"; do
        if [[ "$arg" == 'container:'* ]]; then
            local_path="${arg#container:}"
            if [[ "$local_path" != '/'* ]]; then
                local_path="$current_dir/$local_path"
            fi
            file_name=$(basename "${local_path}")
            docker_path="$tmpdir/$file_name"
            docker cp "${local_path}" "$node_container:${docker_path}"
            docker exec $interactive_flags "$node_container" sudo chown tezos "${docker_path}"
            container_args+=("file:$docker_path");
        else
            container_args+=("${arg}");
        fi
    done
    docker exec $interactive_flags \
           -u ${userid}:${groupid} \
           -e "HOME=${dunix_dir}" \
           -e 'DUNE_CLIENT_UNSAFE_DISABLE_DISCLAIMER' \
           -e "IX_DIR=${dunix_dir}/ix" \
           -e "IX_IN_DOCKER=y" \
           -e 'IX_ALWAYS_ROOT=y' \
           "$node_container" "${container_args[@]}"
}

## Container ###############################################################

update_active_protocol_version() {
    docker run --rm --entrypoint /bin/cat "$docker_preimage" \
           /usr/local/share/dune-network/active_protocol_versions > "$active_protocol_versions"
}

may_update_active_protocol_version() {
    if [ ! -f "$active_protocol_versions" ] ; then
        update_active_protocol_version
    fi
}

pull_image() {
    if [ "$DUNE_TESTNET_DO_NOT_PULL" = "yes" ] \
           || [ "$TESTNET_EMACS" ] \
           || [ "$docker_preimage" = "$(echo $docker_preimage | tr -d '/')" ] ; then
        return ;
    fi
    docker pull "$docker_preimage"
    update_active_protocol_version
    
    cat > "$docker_dir"/Dockerfile <<EOF
FROM $docker_preimage
USER 0:0
RUN addgroup $USER -g $groupid 
RUN mkdir -p ${dunix_dir}
RUN chown $userid:$groupid ${dunix_dir}
RUN adduser $USER -u $userid -G $USER -D -h ${dunix_dir}
USER $userid:$groupid
WORKDIR ${dunix_dir}
ENTRYPOINT ["/usr/local/bin/dune-client"]
EOF
    docker build -t "${docker_image}" "$docker_dir"
    date "+%s" > "$docker_pull_timestamp"
}

may_pull_image() {
    if [ ! -f "$docker_pull_timestamp" ] \
         || [ 3600 -le $(($(date "+%s") - $(cat $docker_pull_timestamp))) ]; then
        pull_image
    fi
}

uptodate_container() {
    running_image=$(docker inspect \
                           --format="{{ .Image }}" \
                           --type=container "$(container_name "$1")")
    latest_image=$(docker inspect \
                          --format="{{ .Id }}" \
                          --type=image "$docker_image")
    [ "$latest_image" = "$running_image" ]
}

uptodate_containers() {
    container=$1
    if [ ! -z "$container" ]; then
        shift 1
        uptodate_container $container && uptodate_containers $@
    fi
}

assert_container() {
    call_docker_compose up --no-start
}

container_name() {
    local name="$(docker ps --filter "name=$1" --format "{{.Names}}")"
    if [ -n "$name" ]; then echo "$name"; else echo "$1"; fi
}

set_internal_rpc_port() {
    if ! $(docker run --rm -i -v="$docker_node_volume":/tmp/data busybox \
                  test -f /tmp/data/data/config.json) ; then
        internal_rpc_port=8733
        return
    fi

    if ! which jq > /dev/null 2>&1 ; then
        echo "jq does not seem to be installed."
        exit 1
    fi

    if ! which awk > /dev/null 2>&1 ; then
        echo "awk does not seem to be installed."
        exit 1
    fi

    local port=$(docker run --rm -i -v="$docker_node_volume":/tmp/data busybox \
                        cat /tmp/data/data/config.json \
                     | jq -r '.rpc."listen-addr"' \
                     | awk -F ":" '{print $NF}')
    if [ "$port" == "null" ] ; then
        internal_rpc_port=8733
    else
        internal_rpc_port="$port"
    fi
}

## Node ####################################################################

check_node_volume() {
    docker volume inspect "$docker_node_volume" > /dev/null 2>&1
}

clear_node_volume() {
    if check_node; then
        echo -e "\033[31mCannot clear data while the node is running.\033[0m"
        exit 1
    fi
    if check_node_volume ; then
        docker volume rm "$docker_node_volume" > /dev/null
        echo -e "\033[32mThe chain data has been removed from the disk.\033[0m"
    else
        echo -e "\033[32mNo remaining data to be removed from the disk.\033[0m"
    fi
}

migrate_volume() {
    local docker_volume="$1"
    local old_volume=${docker_volume#"dune"}
    if ! $(docker volume inspect "$old_volume" > /dev/null 2>&1) ; then
        echo -e "\033[31mNo volume $old_volume to migrate!\033[0m"
    elif [ "$old_volume" != "$docker_volume" ] ; then
        if ! $(docker volume inspect "$docker_volume" > /dev/null 2>&1) ; then
            echo "Creating destination volume $docker_volume..."
            docker volume create --name $docker_volume
        fi
        docker run --rm -i \
               -v="$old_volume":/"$old_volume" \
               -v="$docker_volume":/"$docker_volume" \
               busybox sh -c "cp -fav /$old_volume/. /$docker_volume/"
        if [ "$?" = "0" ] ; then
            echo -e "\033[32mThe $docker_volume has been migrated to new volume $docker_volume.\033[0m"
            echo "You can issue \"docker volume rm $old_volume\" when you have confirmed data has been imported."
            # docker volume rm "$old_volume" > /dev/null
            # echo -e "\033[32mOld volume $old_volume has been removed.\033[0m"
        fi
    fi
}

migrate_volumes() {
    if check_node; then
        echo -e "\033[31mCannot migate data while the node is running.\033[0m"
        exit 1
    fi
    migrate_volume "$docker_node_volume"
    migrate_volume "$docker_client_volume"
}

check_node() {
    res=$(docker inspect \
                 --format="{{ .State.Running }}" \
                 --type=container "$(container_name "$docker_node_container")" 2>/dev/null || echo false)
    [ "$res" = "true" ]
}

assert_node() {
    if ! check_node; then
        echo -e "\033[31mNode is not running!\033[0m"
        exit 0
    fi
}

warn_node_uptodate() {
    if ! uptodate_container "$docker_node_container"; then
        echo -e "\033[33mThe current node is not the latest available.\033[0m"
    fi
}

assert_node_uptodate() {
    assert_node
    if ! uptodate_container "$docker_node_container"; then
        echo -e "\033[33mThe current node is not the latest available.\033[0m"
        exit 1
    fi
}

status_node() {
    if check_node; then
        echo -e "\033[32mNode is running\033[0m"
        warn_node_uptodate
    else
        echo -e "\033[33mNode is not running\033[0m"
    fi
}

start_dunix() {
    if check_node; then
        echo -e "\033[31mNode is already running\033[0m"
        exit 1
    fi
    update_compose_file "$@"
    call_docker_compose up --no-start
    call_docker_compose start dunix
    echo -e "\033[32mThe node is now running.\033[0m"
}

log_dunix() {
    assert_node_uptodate
    call_docker_compose logs --tail=100 -f dunix
}

stop_dunix() {
    if ! check_node; then
        echo -e "\033[31mNo node to kill!\033[0m"
        exit 1
    fi
    echo -e "\033[32mStopping the node...\033[0m"
    call_docker_compose stop dunix
}


## Misc ####################################################################

run_client() {
    assert_node_uptodate
    exec_docker "dune-client" "$@"
}

run_ix() {
    exec_docker "ix" "$@"
}

run_admin_client() {
    assert_node_uptodate
    exec_docker "dune-admin-client" "$@"
}

run_shell() {
    assert_node_uptodate
    if [ $# -eq 0 ]; then
        exec_docker /bin/sh
    else
        exec_docker /bin/sh -c "$@"
    fi
}

display_head_stat() {
    local head="$@"
    local timestamp_UTC="$(jq -r .timestamp <<< $head)"
    local timestamp_local="$(date --date=$timestamp_UTC)"
    local timestamp_s="$(date --date=$timestamp_UTC '+%s')"
    local now="$(date '+%s')"
    local delay=$(( $now - timestamp_s))
    local status="\033[41mSynchronizing ($((delay / 3600)) hours late)\033[0m"
    local levelcolor=31
    if [ $delay -lt 60 ] ; then
        status="\033[42mSynchronized\033[0m                           "
        levelcolor=32
    elif [ $delay -lt 120 ] ; then
        status="\033[43mSynchronizing\033[0m                          "
        levelcolor=33
    fi
    echo -e "Status:\t\t$status"
    echo -e "Level:\t\t\033[$levelcolor;1m$(jq .level <<< $head)\033[0m"
    echo -e "Hash:\t\t\033[35m$(jq -r .hash <<< $head)\033[0m"
    echo -e "Timestamp:\t\033[36m$(date --date=`jq -r .timestamp <<< $head`)\033[0m"
}

display_head() {
    assert_node_uptodate
    display_head_stat "$(exec_docker dune-client rpc get /chains/main/blocks/head/header)"
}

install_curl() {
    if ! exec_docker "which" "curl" > /dev/null ; then
        run_shell "sudo apk add curl"
    fi
}

monitor_head() {
    warn_node_uptodate
    install_curl
    set_internal_rpc_port
    echo; echo; echo; echo
    tput cuu 4
    tput sc
    trap "kill 0" EXIT
    run_shell "curl -N -s http://localhost:$internal_rpc_port/monitor/heads/main" | \
        while read -r head; do
            if [ ! -z "$PID" ]; then
                kill "$PID" &> /dev/null
            fi
            while true ; do
                tput rc
                tput sc
                display_head_stat $head
                sleep 5
            done &
            local PID=$!
        done
    wait
}

## Main ####################################################################

start() {
    update_compose_file "$@"
    call_docker_compose up -d --remove-orphans
    warn_script_uptodate
}

stop() {
    call_docker_compose down
}

kill_() {
    call_docker_compose kill
    stop
}

status() {
    status_node
    warn_script_uptodate verbose
}

snapshot_import() {
    pull_image
    local_snapshot_path="$1"
    update_compose_file
    call_docker_compose up importer
    warn_script_uptodate
}

warn_script_uptodate() {
    if [[ $TESTNET_EMACS ]]; then
       return
    fi
    docker run --rm --entrypoint /bin/cat "$docker_preimage" \
       "/usr/local/share/dune-network/dunix.sh" > ".dunix.sh.new"
    if ! diff .dunix.sh.new  "$0" >/dev/null 2>&1 ; then
        echo -e "\033[33mWarning: the container contains a new version of 'dunix.sh'.\033[0m"
        echo -e "\033[33mYou might run '$0 update_script' to synchronize.\033[0m"
    elif [ "$1" = "verbose" ] ; then
        echo -e "\033[32mThe script is up to date.\033[0m"
    fi
    rm .dunix.sh.new
}

update_script() {
    docker run --rm --entrypoint /bin/cat "$docker_preimage" \
       "/usr/local/share/dune-network/dunix.sh" > ".dunix.sh.new"
    if ! diff .dunix.sh.new  "$0" >/dev/null 2>&1 ; then
        mv .dunix.sh.new "$0"
        chmod +x "$0"
        echo -e "\033[32mThe script has been updated.\033[0m"
    else
        rm .dunix.sh.new
        echo -e "\033[32mThe script is up to date.\033[0m"
    fi
}

upgrade_node_storage() {
    pull_image
    local_snapshot_path="$1"
    update_compose_file
    call_docker_compose up upgrader
    warn_script_uptodate
}

do_cp(){
        local node_container="$(container_name "$docker_node_container")"
        echo docker cp "$@" "$node_container:/usr/local/bin/"
        docker cp "$@" "$node_container:/usr/local/bin/"
}

usage() {
    echo -e "Usage: $0 [GLOBAL_OPTIONS] <command> [OPTIONS]"
    echo -e "  Main commands:"
    echo -e "    \e[1m$0 \e[32mcontainer init\e[39m\e[0m"
    echo -e "       Download or update the Docker-ix container."
    echo -e "    \e[1m$0 \e[92mcontainer start\e[39m\e[0m"
    echo -e "       Launch the Docker-ix container"
    echo -e "    \e[1m$0 <\e[31mcontainer stop\e[39m|\e[91mkill\e[39m>\e[0m"
    echo -e "       Friendly or brutally stop the container."
    echo -e "    \e[1m$0 \e[31mcontainer shell\e[39m\e[0m"
    echo -e "       Start a shell in the container"
    echo -e "    \e[1m$0 container update_script\e[0m"
    echo -e "       Replace '$0' with the one found in the docker image."
    echo -e "    \e[1m$0 \e[32mCOMMAND\e[39m\e[0m [ARGUMENTS]"
    echo -e "       Call 'ix \e[32mCOMMAND\e[39m\e[0m [ARGUMENTS]' within the container"
    echo -e "  Example commands:"
    echo -e "    $0 \e[94mcreate sandbox1\e[39m"
    echo -e "    $0 \e[94mstart --lazy\e[39m"
    echo -e "    $0 \e[94mstop\e[39m"
    echo -e "    $0 \e[94mreset baker\e[39m"
    echo -e "    $0 \e[94mremove sandbox1\e[39m"
    echo -e "    $0 \e[94mswitch mainnet\e[39m"
    echo -e "    $0 \e[94mrpc header\e[39m"
    echo -e "Files: ${dunix_dir}"
    echo -e "Image: ${docker_image}"
    echo -e "Container prefix:"
    echo -e "    container:<FILE>"
    echo -e "      can be used anywhere 'file:<FILE>' is permitted in client commands."
    echo -e "      It will cause the referenced file to be copied into the docker conainer."
    echo -e "      Files will be renamed, which may make errors difficult to read"
}

## Dispatch ################################################################

if [ "$#" -ge 2 ] && [ "$1" = "--port" ] ; then
    port="$2"
    shift 2
fi

command="$1"

script_basename=$(basename $0)
script_name=${script_basename%.*}
docker_base_dir="$HOME/.dune-$script_name"
docker_preimage="$docker_repo:next"
docker_image="dunix:next"
docker_compose_base_name="dune$script_name"
default_port=19733

docker_dir="$docker_base_dir"
docker_compose_yml="$docker_dir/docker-compose.yml"
docker_pull_timestamp="$docker_dir/docker_pull.timestamp"
active_protocol_versions="$docker_dir/active_protocol_versions"
docker_compose_name="$docker_compose_base_name"

docker_node_container=${docker_compose_name}_dunix_1

docker_node_volume=${docker_compose_name}_node_data
docker_client_volume=${docker_compose_name}_client_data

userid=$(id -u $USER)
groupid=$(id -g $USER)

if [ "$#" -eq 0 ] ; then usage ; exit 1;  else shift ; fi

if [ -n "$port" ] ; then
    mkdir -p "$docker_base_dir"
    echo "$port" > "$docker_base_dir/default_port"
elif [ -f "$docker_base_dir/default_port" ]; then
    port=$(cat "$docker_base_dir/default_port")
else
    port=$default_port
fi

mkdir -p "$docker_dir"

case "$command" in

    container)
        subcommand="$1"
        if [ "$#" -eq 0 ] ; then usage ; exit 1;  else shift ; fi
        case "$subcommand" in
            init)
                pull_image
                ;;
            start)
                start "$@"
                ;;
            log)
                log_dunix
                ;;
            update)
                pull_image
                ;;
            restart)
                stop
                update_script
                export DUNE_TESTNET_DO_NOT_PULL=yes
                exec "$0" start "$@"
                ;;
            status)
                status
                ;;
            stop)
                stop
                ;;
            kill)
                kill_
                ;;
            shell)
                run_shell "$@"
                ;;
            update_script)
                update_script
                ;;
        esac
        ;;
    *)
        run_ix "$command" "$@"
        ;;

esac

