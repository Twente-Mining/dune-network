#! /bin/sh

set -e

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"

. "$script_dir/opam-pin.sh"

tmp=$(mktemp)

sed -z 's/^\(.*##BEGIN_OPAM##\n\).*\(\n##END_OPAM##.*\)$/\1/' "$src_dir/.gitlab-ci.yml" > $tmp

cpt=0
for package in $packages; do
    num=$(printf "%03d" $cpt)
    cpt=$((cpt+1))
    case $package in
        tezos-protocol-004-Pt24m4xi-tests|tezos-protocol-005-PsBabyM1-tests)
            cat >> $tmp <<EOF
opam:$num:$package:
  <<: *opam_definition
  variables:
    DUNE_TZ_COMPATIBILITY: "y"
    DUNE_GENESIS_PROTOCOL_REVISION: "max"
    package: $package

EOF
            ;;
        *)
            cat >> $tmp <<EOF
opam:$num:$package:
  <<: *opam_definition
  variables:
    package: $package

EOF
            ;;
    esac
done

sed -z 's/^\(.*##BEGIN_OPAM##\n\).*\(\n##END_OPAM##.*\)$/\2/' "$src_dir/.gitlab-ci.yml" >> $tmp

mv $tmp "$src_dir/.gitlab-ci.yml"

