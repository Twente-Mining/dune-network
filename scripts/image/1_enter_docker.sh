#!/bin/sh

set -e

script_dir="$(cd "$(dirname "$0")" && cd .. && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"
cd "$src_dir"

if [ -e "$src_dir/_opam" ]; then
    echo Renaming _opam to _opam_old
    mv "$src_dir/_opam" "$src_dir/_opam_old"
fi


# for opam_repository_tag=9335e123cb92f6fbb2dc7d1844634d20ca2c8b11
. "$script_dir"/version.sh

image_name=dune-network
image_version=current
build_deps_image_name=registry.gitlab.com/dune-network/opam-repository
build_deps_image_version=$opam_repository_tag
build_image_name="dune-network_build"
base_image="$build_deps_image_name:$build_deps_image_version"
container="${image_name}:${image_version}"

tmp_dir=$(mktemp -dt dune-network.opam.dune-network.XXXXXXXX)

userid=$(id -u $USER)
groupid=$(id -g $USER)

cat <<EOF > "$tmp_dir"/Dockerfile
FROM $base_image
USER 0:0
RUN addgroup $USER -g $groupid 
RUN adduser $USER -u $userid -G $USER -D
RUN chown $userid:$groupid /home/$USER
USER $userid:$groupid
RUN mkdir /home/$USER/.opam
RUN cp /home/tezos/.opam/config /home/$USER/.opam/
RUN chown -R $userid:$groupid /home/$USER
RUN ln -s /home/tezos/.opam/ocaml-base-compiler.4.07.1/ /home/$USER/.opam/ocaml-base-compiler.4.07.1
WORKDIR /home/$USER
ENTRYPOINT /bin/bash
EOF

echo Generated Dockerfile:
cat "$tmp_dir"/Dockerfile

docker build -t ${container} "$tmp_dir"

