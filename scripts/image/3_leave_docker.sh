#!/bin/sh

set -e

script_dir="$(cd "$(dirname "$0")" && cd .. && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"
cd "$src_dir"

if [ -e "$src_dir/_opam_old" ]; then
    echo Restoring local opam switch
    mv "$src_dir/_opam_old" "$src_dir/_opam"
fi

