#!/bin/sh

set -e

script_dir="$(cd "$(dirname "$0")" && cd .. && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"
cd "$src_dir"

# for opam_repository_tag=9335e123cb92f6fbb2dc7d1844634d20ca2c8b11
. "$script_dir"/version.sh

image_name=dune-network
image_version=current
build_deps_image_name=registry.gitlab.com/dune-network/opam-repository
build_deps_image_version=$opam_repository_tag
build_image_name="dune-network_build"
base_image="$build_deps_image_name:$build_deps_image_version"
container="${image_name}:${image_version}"

userid=$(id -u $USER)
groupid=$(id -g $USER)

docker run -it -v $src_dir:/home/$USER/dune-network  -u `id -u $USER`:`id -g $USER` -e "HOME=/home/$USER" ${container} -i  
